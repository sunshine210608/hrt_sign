;/**************************************************************************//**
; * @file     startup_ARMCM4.s
; * @brief    CMSIS Core Device Startup File for
; *           ARMCM4 Device
; * @version  V5.4.0
; * @date     12. December 2018
; ******************************************************************************/
;/*
; * Copyright (c) 2009-2018 Arm Limited. All rights reserved.
; *
; * SPDX-License-Identifier: Apache-2.0
; *
; * Licensed under the Apache License, Version 2.0 (the License); you may
; * not use this file except in compliance with the License.
; * You may obtain a copy of the License at
; *
; * www.apache.org/licenses/LICENSE-2.0
; *
; * Unless required by applicable law or agreed to in writing, software
; * distributed under the License is distributed on an AS IS BASIS, WITHOUT
; * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; * See the License for the specific language governing permissions and
; * limitations under the License.
; */

;//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------


;<h> Stack Configuration
;  <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
;</h>

;Stack_Size      EQU      0x0000B000
Stack_Size      EQU      0x00010000
;Stack_Size      EQU      0x0000D000
	
                AREA     STACK, NOINIT, READWRITE, ALIGN=3
__stack_limit
Stack_Mem       SPACE    Stack_Size
__initial_sp


;<h> Heap Configuration
;  <o> Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
;</h>

Heap_Size       EQU      0x00060000
;Heap_Size       EQU      0x0006B000
;Heap_Size       EQU      0x0006C000
;Heap_Size       EQU      0x000A000

                IF       Heap_Size != 0                      ; Heap is provided
                AREA     HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE    Heap_Size
__heap_limit
                ENDIF


                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset

                AREA     RESET, DATA, READONLY
                EXPORT   __Vectors
                EXPORT   __Vectors_End
                EXPORT   __Vectors_Size

__Vectors       DCD      __initial_sp                        ;     Top of Stack
                DCD      Reset_Handler                       ;     Reset Handler
                DCD      NMI_Handler                         ; -14 NMI Handler
                DCD      HardFault_Handler                   ; -13 Hard Fault Handler
                DCD      MemManage_Handler                   ; -12 MPU Fault Handler
                DCD      BusFault_Handler                    ; -11 Bus Fault Handler
                DCD      UsageFault_Handler                  ; -10 Usage Fault Handler
                DCD      0                                   ;     Reserved
                DCD      0                                   ;     Reserved
                DCD      0                                   ;     Reserved
                DCD      0                                   ;     Reserved
                DCD      SVC_Handler                         ;  -5 SVCall Handler
                DCD      DebugMon_Handler                    ;  -4 Debug Monitor Handler
                DCD      0                                   ;     Reserved
                DCD      PendSV_Handler                      ;  -2 PendSV Handler
                DCD      SysTick_Handler                     ;  -1 SysTick Handler

                ; External Interrupts
                DCD     DMA0_IRQHandler
				DCD     USB_IRQHandler
                DCD     USBDMA_IRQHandler
                DCD     LCD_IRQHandler
                DCD     SCI0_IRQHandler
                DCD     UART0_IRQHandler
                DCD     UART1_IRQHandler
                DCD 	SPI0_IRQHandler
                DCD		CRYPT0_IRQHandler
                DCD		TIM0_0_IRQHandler
                DCD		TIM0_1_IRQHandler
                DCD		TIM0_2_IRQHandler
                DCD		TIM0_3_IRQHandler
                DCD		EXTI0_IRQHandler
                DCD		EXTI1_IRQHandler
                DCD		EXTI2_IRQHandler
                DCD		RTC_IRQHandler
                DCD		SENSOR_IRQHandler
                DCD		TRNG_IRQHandler
                DCD		ADC0_IRQHandler
				DCD		SSC_IRQHandler
				DCD		TIM0_4_IRQHandler
				DCD		TIM0_5_IRQHandler
				DCD		KEYBOARD_IRQHandler
				DCD		MSR_IRQHandler
				DCD		EXTI3_IRQHandler
				DCD		SPI1_IRQHandler
				DCD		SPI2_IRQHandler
				DCD     SCI1_IRQHandler
                DCD     SCI2_IRQHandler
                DCD		SPI3_IRQHandler
                DCD		SPI4_IRQHandler
                DCD     UART2_IRQHandler
                DCD     UART3_IRQHandler
                DCD     0
                DCD     QSPI_IRQHandler
                DCD     I2C0_IRQHandler
                DCD     EXTI4_IRQHandler
                DCD     EXTI5_IRQHandler
				DCD		TIM0_6_IRQHandler
				DCD		TIM0_7_IRQHandler
				DCD		CSI2_IRQHandler
				DCD     DCMI_IRQHandler
                DCD     EXTI6_IRQHandler
                DCD     EXTI7_IRQHandler
				DCD		SDIOM_IRQHandler
				DCD		QR_IRQHandler
			    DCD     GPU_IRQHandler
				DCD     0	
				DCD     AWD_IRQHandler
				DCD     DAC_IRQHandler	
__Vectors_End
__Vectors_Size  EQU      __Vectors_End - __Vectors


                AREA     |.text|, CODE, READONLY

; Reset Handler

Reset_Handler   PROC
                EXPORT   Reset_Handler             [WEAK]
                IMPORT   SystemInit
                IMPORT   __main

                LDR      R0, =SystemInit
                BLX      R0
                LDR      R0, =__main
                BX       R0
                ENDP


; Macro to define default exception/interrupt handlers.
; Default handler are weak symbols with an endless loop.
; They can be overwritten by real handlers.
                MACRO
                Set_Default_Handler  $Handler_Name
$Handler_Name   PROC
                EXPORT   $Handler_Name             [WEAK]
                B        .
                ENDP
                MEND

				Set_Default_Handler  NMI_Handler
                Set_Default_Handler  HardFault_Handler
                Set_Default_Handler  MemManage_Handler
                Set_Default_Handler  BusFault_Handler
                Set_Default_Handler  UsageFault_Handler
                Set_Default_Handler  SVC_Handler
                Set_Default_Handler  DebugMon_Handler
                Set_Default_Handler  PendSV_Handler
                Set_Default_Handler  SysTick_Handler

Default_Handler PROC
; ToDo:  Add here the export definition for the device specific external interrupts handler
                EXPORT		DMA0_IRQHandler			[WEAK]
				EXPORT		USB_IRQHandler			[WEAK]
				EXPORT		USBDMA_IRQHandler		[WEAK]
				EXPORT		LCD_IRQHandler			[WEAK]
				EXPORT		SCI0_IRQHandler			[WEAK]
				EXPORT		UART0_IRQHandler		[WEAK]
				EXPORT		UART1_IRQHandler		[WEAK]
				EXPORT 		SPI0_IRQHandler			[WEAK]
				EXPORT		CRYPT0_IRQHandler		[WEAK]
				EXPORT		TIM0_0_IRQHandler		[WEAK]
				EXPORT		TIM0_1_IRQHandler		[WEAK]
				EXPORT		TIM0_2_IRQHandler		[WEAK]
				EXPORT		TIM0_3_IRQHandler		[WEAK]
				EXPORT		EXTI0_IRQHandler		[WEAK]
				EXPORT		EXTI1_IRQHandler		[WEAK]
				EXPORT		EXTI2_IRQHandler		[WEAK]
				EXPORT		RTC_IRQHandler			[WEAK]
				EXPORT		SENSOR_IRQHandler		[WEAK]
				EXPORT		TRNG_IRQHandler			[WEAK]
				EXPORT		ADC0_IRQHandler			[WEAK]
				EXPORT		SSC_IRQHandler			[WEAK]
				EXPORT		TIM0_4_IRQHandler		[WEAK]
				EXPORT		TIM0_5_IRQHandler		[WEAK]	
				EXPORT		KEYBOARD_IRQHandler	    [WEAK]	
				EXPORT		MSR_IRQHandler			[WEAK]	
				EXPORT		EXTI3_IRQHandler		[WEAK]	
				EXPORT 		SPI1_IRQHandler			[WEAK]
				EXPORT 		SPI2_IRQHandler			[WEAK]
				EXPORT 		SCI1_IRQHandler         [WEAK]
                EXPORT      SCI2_IRQHandler         [WEAK]
                EXPORT		SPI3_IRQHandler         [WEAK]
                EXPORT		SPI4_IRQHandler         [WEAK]
                EXPORT      UART2_IRQHandler        [WEAK]
                EXPORT      UART3_IRQHandler        [WEAK]
                EXPORT      QSPI_IRQHandler         [WEAK]
                EXPORT      I2C0_IRQHandler         [WEAK]
                EXPORT      EXTI4_IRQHandler        [WEAK]
                EXPORT      EXTI5_IRQHandler        [WEAK]
				EXPORT		TIM0_6_IRQHandler		[WEAK]
				EXPORT		TIM0_7_IRQHandler		[WEAK]
				EXPORT 		CSI2_IRQHandler			[WEAK]	
				EXPORT      DCMI_IRQHandler         [WEAK]
				EXPORT      EXTI6_IRQHandler        [WEAK]
				EXPORT      EXTI7_IRQHandler        [WEAK]  
				EXPORT		SDIOM_IRQHandler		[WEAK]
				EXPORT		QR_IRQHandler			[WEAK]	
			    EXPORT      GPU_IRQHandler          [WEAK]
				EXPORT		AWD_IRQHandler	    [WEAK]
				EXPORT	 	DAC_IRQHandler      [WEAK]
; ToDo:  Add here the names for the device specific external interrupts handler
DMA0_IRQHandler			
USB_IRQHandler			
USBDMA_IRQHandler	
LCD_IRQHandler		
SCI0_IRQHandler			
UART0_IRQHandler		
UART1_IRQHandler	
SPI0_IRQHandler		
CRYPT0_IRQHandler	
TIM0_0_IRQHandler		
TIM0_1_IRQHandler	
TIM0_2_IRQHandler	
TIM0_3_IRQHandler		
EXTI0_IRQHandler		
EXTI1_IRQHandler		
EXTI2_IRQHandler		
RTC_IRQHandler		
SENSOR_IRQHandler		
TRNG_IRQHandler		
ADC0_IRQHandler			
SSC_IRQHandler			
TIM0_4_IRQHandler	
TIM0_5_IRQHandler		
KEYBOARD_IRQHandler	  
MSR_IRQHandler		
EXTI3_IRQHandler		
SPI1_IRQHandler			
SPI2_IRQHandler			
SCI1_IRQHandler       
SCI2_IRQHandler      
SPI3_IRQHandler       
SPI4_IRQHandler        
UART2_IRQHandler       
UART3_IRQHandler             
QSPI_IRQHandler         
I2C0_IRQHandler         
EXTI4_IRQHandler       
EXTI5_IRQHandler 
TIM0_6_IRQHandler
TIM0_7_IRQHandler
CSI2_IRQHandler
DCMI_IRQHandler
EXTI6_IRQHandler
EXTI7_IRQHandler
SDIOM_IRQHandler
QR_IRQHandler	
GPU_IRQHandler   
AWD_IRQHandler	  
DAC_IRQHandler

                B       .
                ENDP
					
					
					
                ALIGN


; User setup Stack & Heap

                IF       :LNOT::DEF:__MICROLIB
                IMPORT   __use_two_region_memory
                ENDIF

                EXPORT   __stack_limit
                EXPORT   __initial_sp
                IF       Heap_Size != 0                      ; Heap is provided
                EXPORT   __heap_base
                EXPORT   __heap_limit
                ENDIF

                END
