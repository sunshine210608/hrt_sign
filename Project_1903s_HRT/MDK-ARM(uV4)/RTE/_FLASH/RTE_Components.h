
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'Project' 
 * Target:  'FLASH' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "ARMCM4_FP.h"

/*  ARM.FreeRTOS::RTOS:Config:FreeRTOS:9.0.0 */
#define RTE_RTOS_FreeRTOS_CONFIG        /* RTOS FreeRTOS Config for FreeRTOS API */
/*  ARM.FreeRTOS::RTOS:Core:9.0.0 */
#define RTE_RTOS_FreeRTOS_CORE          /* RTOS FreeRTOS Core */
/*  ARM.FreeRTOS::RTOS:Event Groups:9.0.0 */
#define RTE_RTOS_FreeRTOS_EVENTGROUPS   /* RTOS FreeRTOS Event Groups */
/*  ARM.FreeRTOS::RTOS:Heap:Heap_4:9.0.0 */
#define RTE_RTOS_FreeRTOS_HEAP_4        /* RTOS FreeRTOS Heap 4 */
/*  ARM.FreeRTOS::RTOS:Timers:9.0.0 */
#define RTE_RTOS_FreeRTOS_TIMERS        /* RTOS FreeRTOS Timers */


#endif /* RTE_COMPONENTS_H */
