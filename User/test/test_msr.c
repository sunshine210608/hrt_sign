#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "msr.h"
#include "hardwaretest.h"
#include "sysTick.h"


int test_msr(void){
    int ret;
    tick tks=get_tick();
    track_data msr[MSR_MAX_TRACK];
    char strbuf[32]={0};
    uint16_t key;
    
    FUNCIN;
    memset((unsigned char*)&msr[0],0,sizeof(msr));
    LcdClean(C_WHITE);
    enterTestItem("MSR Test");
    LcdDisplayMessageCenterC("wait swip msr card");
    while(!is_timeout(tks, DEFAULT_TIMEOUTMS)){

        KeyBoardRead(&key);
        if(key==KEY_CANCEL)
            return NG;
        
        ret=readMSRData(&msr);
        if(ret) continue;
        dbg("%d %d %d\n",msr[0].len,msr[1].len,msr[2].len);

        LcdClean(C_WHITE);
        enterTestItem("MSR Test");

        memset(strbuf,0,sizeof(strbuf));
        sprintf(strbuf,"Track1:%d bytes",msr[0].len);
        LcdDisplayMessageL(strbuf,5);

        memset(strbuf,0,sizeof(strbuf));
        sprintf(strbuf,"Track2:%d bytes",msr[1].len);
        LcdDisplayMessageL(strbuf,6);

        memset(strbuf,0,sizeof(strbuf));
        sprintf(strbuf,"Track3:%d bytes",msr[2].len);
        LcdDisplayMessageL(strbuf,7);
        
        mdelay(1500);
        if(msr[0].len && msr[1].len && msr[2].len){
            return OK;
        }else
            return NG;
    }
    LcdDisplayMessageCenterC("MSR Test Timeout");
    return NG;
}


int TestMSR(void*m){
    return test_msr();
}
