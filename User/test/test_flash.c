/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : test_flash.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-11-08
 * 
 * 
 * @note History:
 * @note        : Jay 2019-11-08
 * @note        : 
 *   Modification: Created file

********************************************************************************/
#include "hardwaretest.h"



/*
Test flash erase read and write
*/
int TestFlash(void*m){
    LcdClear();
    if(((SMenuItem *)m)->name!=NULL)
        LcdPutsc( ((SMenuItem *)m)->name, 0);
    
    return flashmain();
}

