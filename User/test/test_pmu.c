#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "hardwaretest.h"

#include "pmu.h"
#include "debug.h"

static int gTstflag = 0;

int TestIdleMode(void*m){

    gTstflag = get_tick();
    dbg("gTstflag=%d\n",gTstflag);
    mdelay(1000);
    
    EnterSleepMode();

    mdelay(1000);
    dbg("now gTstflag=%d\n",gTstflag);
    return OK;
}

int TestSleepMode(void*m){
    gTstflag = get_tick();
    dbg("gTstflag=%d\n",gTstflag);
    mdelay(1000);
    
    EnterDeepSleepMode();

    mdelay(1000);
    dbg("now gTstflag=%d\n",gTstflag);
    return OK;
}

int TestMCUReset(void*m){
    
    return OK;
}
