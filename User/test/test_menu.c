#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "hardwaretest.h"
#include "displaybuf.h"
#include "pmu.h"
#include "update.h"

//#define dbg


SMenu menu_manual_test;

SMenu *MAINMENU= &menu_manual_test;



static void Display_Menu(SMenu *m)
{
	int i;
	char str[32];	
    SMenuItem *si;
	char num= 0;
	

    for(i=m->cur-1;i<MAX_MENUITEM_NUMBER;i++)
    {
        si = &m->items[i];
        if(si->name==NULL){ 
            dbg("last one item\n");
            break;
        }
        if(++num >=MAX_NUM_ONEPAGE) 
            break;
    }
    dbg("menu num=%d, cur=%d\n",num, m->cur);
	
	for(i=0; i<num; i++)
	{
		dbg("%d name:[%s]\n", i+m->cur-1, m->items[i+m->cur-1].name);
		sprintf(str, " %d.%s\n", (i+1), m->items[i+m->cur-1].name/*,Ir->pass,Ir->failed*/);//m->items[i].result->pass,m->items[i].result->failed
		if(i<m->num)
			LcdPutsl(str,i+1);
		else
			LcdPutsc("                            ",i+1);
	}
    // TODO:
	//flush_lcd();
	
}


// Print the menu into a display buffer, which may not be the internal
// display buffer.
int MenuToBuffer(displaybuf_t *db,SMenu *m)
{
	LcdClear();
    LcdPutsc(m->name,0);
	Display_Menu(m);
	return 0;
}


// Print the menu on the screen, and return the pointer of menu item selected.
SMenuItem* MenuDisplay(SMenu *m)
{
    FUNCIN;
    uint16_t keypad;
	//FIXME:
	//int bytes_per_line = 320/16;//pfont->height * (plcd->pixelx / 8);

	//indecates the current selected menu item.
	char cursor = 0;
	
	//xf20200111 add start.
	if(m->num==0)
	{
		int i;
		for(i=0;i<MAX_MENUITEM_NUMBER && m->items[i].name!=NULL;i++)
			;
		m->num=i;
	}
	//xf20200111 add end.
	
	MenuToBuffer(NULL, m);

	KeyBoardClean();
	while(1)
	{
        FeedDog();
        PMUModeCheck();
        
		if(KeyBoardRead(&keypad))
			continue;

		if(keypad == KEY_CANCEL)
		{
			dbg("cancel\n");
			return NULL;
		}
        dbg("key val=0x%x\n",keypad);
        if(keypad == KEY_1) cursor = 1;
        else if(keypad == KEY_2) cursor = 2;
        else if(keypad == KEY_3) cursor = 3;
        else if(keypad == KEY_4) cursor = 4;
        else if(keypad == KEY_5) cursor = 5;
        else if(keypad == KEY_6) cursor = 6;
        else if(keypad == KEY_7) cursor = 7;
        else if(keypad == KEY_8) cursor = 8;
        else if(keypad == KEY_9) cursor = 9;
        else if(keypad == KEY_F1 || keypad == KEY_F2){
            if(m->num>MAX_NUM_ONEPAGE){
                m->cur = ((m->cur-1)/MAX_NUM_ONEPAGE + 1) * MAX_NUM_ONEPAGE + 1;
                if(m->cur > m->num)
                    m->cur = 1;
            }
            
            dbg("change page to %d\n",m->cur);
            MenuToBuffer(NULL, m);
            continue;
        }else
            continue;
        cursor += m->cur - 1;
        
        if(cursor > m->num)
            continue;
		dbg("cur=%d max=%d\n",cursor,m->num);
        break;
	}
	
	return &(m->items[cursor-1]);
}


int TestManual(void *m)
{
	SMenuItem *item = NULL;
	SMenu *Mainmenu=MAINMENU,*TopMenu=NULL;
	int num=0;
	int ret;

	//get numbers of items
	for(item=Mainmenu->items;item->name!=NULL;item++){
		num++;
	}
	Mainmenu->num=num;
	dbg("items=%d\n",Mainmenu->num);
	TestResult_Init(Mainmenu,0);
	while(1)
	{	
		item = MenuDisplay(Mainmenu);
		if(item == NULL){
            if(TopMenu){
                dbg("Cancel key,return from sub\n");
                //use the top menu.
                Mainmenu = TopMenu;
                TopMenu  = NULL;
                continue;
            }
            dbg("Exit Main Menu,but not exit,free result\n");
            //ShowTestResult();
			break;
		}
        dbg("item:%s\n",item->name);
        if(item->submenu != NULL){
            dbg("has submenu\n");
            //save the top menu point
            TopMenu = Mainmenu;
            Mainmenu = item->submenu;
        }else{
		    ret = item->func((void*)item);
            dbg("Result = %d\n",ret);
            RecordResult(item,ret);
        }
	}

    TestResult_DeInit();
    return 0;
}


int EnterTestMenu(SMenu * Mainmenu)
{
	SMenuItem *item = NULL;
	SMenu *TopMenu=NULL;
	int num=0;
	int ret;

	//get numbers of items
	for(item=Mainmenu->items;item->name!=NULL;item++){
		num++;
	}
	Mainmenu->num=num;
	dbg("items=%d\n",Mainmenu->num);
	TestResult_Init(Mainmenu,0);
	while(1)
	{	
		mdelay(10);
		item = MenuDisplay(Mainmenu);
		if(item == NULL){
            if(Mainmenu->topmenu){
                dbg("Cancel key,return from sub\n");
                //use the top menu.
                Mainmenu = Mainmenu->topmenu;
                //TopMenu  = NULL;
                continue;
            }
            dbg("Exit Main Menu,but not exit,free result\n");
            //ShowTestResult();
			break;
		}
        dbg("item:%s\n",item->name);
        if(item->submenu != NULL){
            dbg("has submenu\n");
            //save the top menu point
            //TopMenu = Mainmenu;
			item->submenu->topmenu=Mainmenu;
            Mainmenu = item->submenu;
        }else{
		    ret = item->func((void*)item);
            dbg("Result = %d\n",ret);
            RecordResult(item,ret);
        }
	}

    TestResult_DeInit();
    return 0;
}

int selectExitmenu(void){
    uint16_t key;
    LcdClear();
    LcdPutscc("Exit Or Test Next?");
    LcdPutsb(" EXIT         NEXT ");
    KeyBoardClean();

    key = 0;
    while(1){
        KeyBoardRead(&key);
        if(key==KEY_CANCEL)
            return EXIT;
        else if(key == KEY_OK)
            return NG;
    }
}

/**
 * @fn       int EnterAutoTestItems(SMenu * Mainmenu)
 * @brief    auto test all items in Mainmenu
 * 
 * @param[in]          SMenu * Mainmenu  
 * @return         int
 */
int EnterAutoTestItems(SMenu * Mainmenu){
    SMenuItem *item = NULL;
	int i=0;
	int ret;
    TestResult_Init(Mainmenu,1);

    //register double cancel to exit loop test or skip current test item
    registerKeyboardEvent(KB_EVT_DOUBLECANCEL);
    //only support one level menu
    while(1){	
        LcdClear();
        ////Line;
    	item = &Mainmenu->items[i++];
    	if(item->name == NULL){
            //last test item
    		break;
    	}
	    ret = item->func((void*)item);
        //exit test or continue next one item?
        if(ret == EXIT){
            ret = selectExitmenu();
            if(ret==EXIT)
                break;
        }
        RecordResult(item,ret);
    }
    unregisterKeyboardEvent(KB_EVT_DOUBLECANCEL);
    
    TestResult_DeInit();
    ////Line;
    return 0;
}

SMenu menu_testLCD =
{"Test LCD", 3, 1, 0,
	{
		{"LCD Color", TestLCDColor, NULL},
		{"LCD BackLight",TestLCDBacklight, NULL},
   #ifdef TESTSHOWPIC
        {"Show Picture",TestLCDPicture,NULL},
   #endif
        NULL
	}
};



SMenu menu_testTP =
{"Test TouchPanel", 3, 1, 0,
	{
		{"TouchPanel Adjust", touchpanel_testAdjust, NULL},
		{"TP Test Point",touchpanel_test, NULL},
        {"TP Test Sign",touchpanel_testsign, NULL},
        NULL
	}
};


SMenu menu_testPrinter =
{"Test LCD", 2, 1, 0,
	{
		{"Normal Test", TestPrinter_normal, NULL},
		{"High Temperature Test",TestPrinter_hightemp, NULL},
       
        NULL
	}
};

SMenu menu_testPMU =
{"Test PMU", 3, 1, 0,
	{
		{"Idle Mode", TestIdleMode, NULL},
		{"Sleep Mode",TestSleepMode, NULL},
        {"Reset",TestMCUReset, NULL},
        NULL
	}
};

SMenu menu_testSensor =
{"Sensor Test", 3, 1, 0,
	{
		{"Sensor Test", TestSensor},
		{"Sensor Enable",TestSensorEnable},
        {"Sensor Disable",TestSensorDisable},
        NULL
	}
};


int lfs_test(void);

SMenu menu_manual_test =
{"Manual Test", 0, 1, 0,    
	{
        //{"WIFI",TestWifi},
        //{"Update",lfs_test},
        {"PMU",NULL,&menu_testPMU},
            
        {"Charge",TestCharge},
        {"RTC",TestRTC},	
		{"LCD",NULL,&menu_testLCD},		
		{"ICC",TestICC},
        {"SAM",TestSAM},
        {"KeyBoard",TestKeyBoard},
        {"MSR",TestMSR},
        {"NFC",TestNFC},
        {"Buzzer",TestBuzzer},
		{"TouchPanel",NULL,&menu_testTP},
        {"Printer",NULL,&menu_testPrinter},
        {"WIFI",TestWifi},
        {"GPRS",TestGPRS},
        {"Sensor",NULL,&menu_testSensor},
        {"Flash",TestFlash},
        {"PMU",NULL,&menu_testPMU},
        //{"Update",updateFirmware},
        {"Result",TestResult},
        {"About",TestAbout},
		NULL
	}
};

SMenu menu_factory_autotest =
{"Factory Auto Test", 0, 1, 0,    
	{            
        //{"Charge",TestCharge},
        //{"RTC",TestRTC},	
		//{"LCD Color", TestLCDColor},
		//{"LCD BackLight",TestLCDBacklight},		
        
//		{"ICC",TestICC},
//        {"SAM",TestSAM},
//        {"KeyBoard",TestKeyBoard},
//        {"MSR",TestMSR},
//        {"NFC",TestNFC},
//        {"Buzzer",TestBuzzer},
//          {"TouchPanel Adjust", touchpanel_testAdjust},

          {"Printer Test", TestPrinter_normal},
//          {"Battery Test",TestBattery},
//        {"WIFI",TestWifi},
//        {"GPRS",TestGPRS},
        {"Sensor",TestSensor},
//        {"Flash",TestFlash},
//        //{"PMU",NULL,&menu_testPMU},
        {"Result",TestResult},
		NULL
	}
};


SMenu menu_StressTest =
{"Stress Test", 0, 1, 0,
	{
		{"MSR Stress Test", TestMSRStress},
		{"KeyBoard Stress Test",TestKeyBoard_StressTest},
        {"ICC Stress Test",TestICC_StressTest},
        NULL
	}
};

void HardwareTestManual(void){
    EnterTestMenu(&menu_manual_test);
}

void HardwareTest_StressTest(void){
    EnterTestMenu(&menu_StressTest);
}

void HardwareTest_FactoryAutoTest(void){
    EnterAutoTestItems(&menu_factory_autotest);
}

int waitkey(void)
{
	uint16_t key;
	while(KeyBoardRead(&key))
		;
	return key;
}

int func1_1_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_1_2", 2);
	waitkey();
	return 0;
}
int func1_1_3(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_1_3", 2);
	waitkey();
	return 0;
}
int func1_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_2", 2);
	waitkey();
	return 0;
}
int func1_3(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_3", 2);
	waitkey();
	return 0;
}
int func2_1_1(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_1_1", 2);
	waitkey();
	return 0;
}
int func2_1_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_1_2", 2);
	waitkey();
	return 0;
}
int func2_1_3(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_1_3", 2);
	waitkey();
	return 0;
}
int func2_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_2", 2);
	waitkey();
	return 0;
}
int func2_3(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_3", 2);
	waitkey();
	return 0;
}
int func1_1_1_1(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_1_1_1", 2);
	waitkey();
	return 0;
}

int func1_1_1_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_1_1_2", 2);
	waitkey();
	return 0;
}
int func3(void *p)
{
	LcdClear();
	LcdPutsl("this is func3", 2);
	waitkey();
	return 0;
}


SMenu menu_SubMemu1_1_1 =
{"Sub Menu1_1_1", 0, 1, 0,
	{
		{"Menu1_1_1_1-F", func1_1_1_1},
		{"Menu1_1_1_2-F", func1_1_1_2},
        NULL
	}
};


//二级子菜单1_1
SMenu menu_SubMemu1_1 =
{"Sub Menu1_1", 0, 1, 0,
	{
		{"Menu1_1_1-M", 0, &menu_SubMemu1_1_1},
		{"Menu1_1_2-F", func1_1_2},
        {"Menu1_1_3-F", func1_1_3},
        NULL
	}
};

//一级子菜单1
SMenu menu_SubMemu1 =
{"Sub Menu1", 0, 1, 0,
	{
		{"Menu1_1-M", 0, &menu_SubMemu1_1},
		{"Menu1_2-F", func1_2},
        {"Menu1_3-F", func1_3},
        NULL
	}
};

//二级子菜单2-1
SMenu menu_SubMemu2_1 =
{"Sub Menu1_1", 0, 1, 0,
	{
		{"Menu2_1_1-F", func2_1_1},
		{"Menu2_1_2-F", func2_1_2},
        {"Menu2_1_3-F", func2_1_3},
        NULL
	}
};

//一级子菜单2
SMenu menu_SubMemu2 =
{"Sub Menu2", 0, 1, 0,
	{
		{"Menu2_1-M", 0, &menu_SubMemu2_1},
		{"Menu2_2-F", func2_2},
        {"Menu2_3-F", func2_3},
        NULL
	}
};

//主菜单
SMenu menu_MainMemu =
{"Main Menu", 0, 1, 0,
	{
		{"Menu1-M", 0, 		&menu_SubMemu1, 0},
		{"Menu2-M", 0, 		&menu_SubMemu2, 0},
        {"Menu3-F", func3, 	0, 				0},
        NULL
	}
};

void testmenu(void){
    EnterTestMenu(&menu_MainMemu);
}
