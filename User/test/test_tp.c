#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "mhscpu.h"
#include "msr.h"
#include "systick.h"
#include "hardwaretest.h"

void test_tp(void){
    uint8_t x,y,mx=0,my=0,lx=0xff,ly=0xff;
    tick tks=get_tick();
    int ret,cnt=0;
    char strtmp[32];
    FUNCIN;
    enterTestItem("TP Test");
    //while(1) ts_test();
    LcdDisplayMessageCenterC("Please touch TP");
    do{
        ret=waitTPPressIrq(1000);
        if(ret) continue;
        ret=tp_readXY(&x,&y);
        cnt++;
        //if(x/10!=lastx/10 || y/10!=lasty/10)
        {
            memset(strtmp,0,sizeof(strtmp));
            sprintf(strtmp,"x y : %d %d",x,y);
            
            LcdDisplayMessageCenterC(strtmp);
        }
        if(mx<x){ mx=x;dbg("maxy %d:%d\n minxy %d:%d\n",mx,my,lx,ly);}
        if(my<y){ my=y;dbg("maxy %d:%d\n minxy %d:%d\n",mx,my,lx,ly);}
        if(lx>x){ lx=x;dbg("maxy %d:%d\n minxy %d:%d\n",mx,my,lx,ly);}
        if(ly>y){ ly=y;dbg("maxy %d:%d\n minxy %d:%d\n",mx,my,lx,ly);}
    }while(!is_timeout(tks, 30*1000) && cnt < 5);
}

#define TP_DEVVIATION  10 
int tpxytest(int x,int y){
    int ret,tpx,tpy;
    char buf[32];
    
    put_cross(x, y);
	ret = tpReadXY(&tpx, &tpy);
    if(ret) return -1;
    if(abs(x-tpx)<TP_DEVVIATION && abs(y-tpy)<TP_DEVVIATION){
        dbg("good point\n");
        return 0;
    }
    sprintf(buf,"Err:LCD:%d %d,TP:%d %d\n",x,y,tpx,tpy);
    LcdDisplayMessageBottom(buf);
    mdelay(1500);
    return -2;
}

int touchpanel_test(void*m)
{
	int ret;

    LcdClear();
    LcdPutsc(((SMenuItem*)m)->name, 0);
    ret=tpxytest(80,80);
    if(ret) return NG;

    ret=tpxytest(100,150);
    if(ret) return NG;

    ret=tpxytest(200,200);
    if(ret) return NG;

    ret=tpxytest(160,120);
    if(ret) return NG;

    ret=tpxytest(300,200);
    if(ret) return NG;

    LcdClear();
    LcdDisplayMessageCenterC("All TP Point Test PASS!");mdelay(1500);
    dbg("all tp point test pass\n");
    return OK;
}


int touchpanel_testAdjust(void*m){
    int ret;
    ret=touchpanel_adjust();
    return ret?NG:OK;
}
