/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : hardwaretest.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-10-28
 * 
 * 
 * @note History:
 * @note        : Jay 2019-10-28
 * @note        : 
 *   Modification: Created file

********************************************************************************/
#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "_compileinfo.h"
#include "hardwaretest.h"

/*
line top anc center
*/
void showTestItemName(char *message){
	int line,len=strlen(message);
	UG_FontSelect(&FONT_12X16);
	UG_SetBackcolor(C_WHITE);
    UG_SetForecolor(C_BLACK);
	line = len*FONT_12X16.char_width;
	if(line > DISPLAY_WIDTH)
		line = 0;
	else
		line = (DISPLAY_WIDTH-line)/2;

	UG_PutString(line,0,message);
}


void showMainMenu(void){
    
}

void enterTestItem(char*itemname){
    LcdClean(C_WHITE);
    showTestItemName(itemname);
}


static char gAutoTest_flag = 0;
void setAutoTest(char autoflag){
    gAutoTest_flag = autoflag?1:0;
}

char getAutoTest(void){
    return gAutoTest_flag;
}

void AutoHardwareTest(void){
    int ret;
    uint16_t key;
    char buf[32];

    FUNCIN;
    LcdClean(C_WHITE);
    sprintf(buf,"v%s",FWVERSION);
    LcdDisplayMessageCenterC("CY20 Hardware Test");
    LcdDisplayMessageCenter(buf, 9);

    mdelay(500);
    KeyBoardClean();

#ifdef CFG_SENSOR_ENABLE
    do{
        state = getSensorState();
        if(state){
            showSensorMessage();
        }else{
            dbg("All sensor done\n");
            LcdClean(C_WHITE);
            LcdDisplayMessageCenterC("All Sensor OK");
        }
        mdelay(1000);
        
    }while(1);
    
#endif

#ifndef UNIT_TEST
    KeyBoardClean();
    while(1){
        ret = KeyBoardReadTimeout(&key,2000);
        if(ret<0) continue;
        if(KEYTYPE(key)==KEY_PUSH) break;
    };
#else

    dbg("Unit Test Mode\n");    
    while(1){
        LcdDisplayMessageCenterC("Press any key start");
        KeyBoardClean();
        ret = KeyBoardReadTimeout(&key,2000);
        if(ret<0) continue;
        LcdClean(C_WHITE);
        //test_qrcode();
        //test_printerSpeed(PRINT_1DST);
        //test_printerSpeed(PRINT_FEED);
        test_printer();
        //ts_test();
        //printouttestresult();
    }
#endif
        
    while(1){
        TestPrinter_normal(NULL);
        KeyBoardRead(&key);
        if(key==KEY_CANCEL) break;
    }
}
