/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : test_keyboard.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-10-28
 * 
 * 
 * @note History:
 * @note        : Jay 2019-10-28
 * @note        : 
 *   Modification: Created file

********************************************************************************/
#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "systick.h"
#include "hardwaretest.h"
#if 0
enum{
    KEY_POWER_IDX,KEY_F1_IDX,KEY_F2_IDX,
    KEY_1_IDX,KEY_2_IDX,KEY_3_IDX,
    KEY_4_IDX,KEY_5_IDX,KEY_6_IDX,
    KEY_7_IDX,KEY_8_IDX,KEY_9_IDX,
    KEY_L_IDX,KEY_0_IDX,KEY_OK_IDX,
    KEY_CANCEL_IDX,KEY_CLR_IDX
};
#else    
char KeyBoard[]={
    KEY_POWER,KEY_F1,KEY_F2,
    KEY_1,KEY_2,KEY_3,
    KEY_4,KEY_5,KEY_6,
    KEY_7,KEY_8,KEY_9,
    KEY_L,KEY_0,KEY_OK,
    KEY_CANCEL,KEY_CLR
};
const char * KeyBoard_KeyNames[]={
    "POWER","F1","F2",
    "1","2","3",
    "4","5","6",
    "7","8","9",
    "L","0","OK",
    "CANCEL","CLR"
};
const char  KeyBoard_chars[]={
    'P','U','D',
    '1','2','3',
    '4','5','6',
    '7','8','9',
    'L','0','S',
    'C','R'
};

#endif

static unsigned int keypush=0,keyrel=0;


char indexofkey(char keyvale){
    char i;
    for(i=0;i<KEY_MAXNUM;i++){
        if(KeyBoard[i]==keyvale)
            return i;
    }
		return 0;
}
/*
default color is black
red if just detect relese
blue during press
green after press and release
*/
void showKeyBoardTestUI(void){
    short xs = 50,ys = 50,xi=80,yi=25,ln=0;
    char i,j,idx,c;
    uint16_t color = C_BLACK;
    //KEY_MAXNUM
    for(i=0;i<6;i++){
        for(j=0;j<3;j++){
            idx = i*3+j;
            if(idx >= KEY_MAXNUM)
                break;
            
            c = KeyBoard_chars[idx];
            
            if(keypush&(1<<idx) && keyrel&(1<<idx))
                color = C_GREEN;
            else{
                if(keypush&(1<<idx))
                    color = C_BLUE;
                else if( keyrel&(1<<idx))
                    color = C_RED;
                else
                    color = C_BLACK;
            }
            UG_PutChar(c, xs+xi*j, ys + ln*yi, color, C_WHITE);
        }
        ln++;
    }

}

int TestKeyBoard(void *m){
    uint16_t keyram;
    int ret;
    char keyidx=0,cancle=0;
    char strtmp[32]={0};
    keypush = keyrel = 0;
    KeyBoardClean();
    LcdClear();
    showKeyBoardTestUI();
    while(1){
        ret=KeyBoardRead(&keyram);
        if(ret) continue;
                
        keyidx = indexofkey(KEYVALUE(keyram));

        if(KEYTYPE(keyram)==KEY_PUSH){
            sprintf(strtmp,"%c Press",KeyBoard[keyidx]);
            keypush |= 1<<keyidx;
        }
        else{
            sprintf(strtmp,"%c Release",KeyBoard[keyidx]);
            keyrel |= 1<<keyidx;
            if(keyram==KEY_CANCEL){
                if(cancle)
                    break;
                cancle = 1;
            }else{
                cancle = 0;
            }
            
        }
        dbg(strtmp);  
        showKeyBoardTestUI();

        if(keypush == 0x1FFFF && keyrel == 0x1FFFF){
            dbg("all key ok\n");
            return OK;
        }
    }

    return NG;
}

