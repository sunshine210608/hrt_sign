#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "test_wifi.h"
#include "wifi.h"
#include "wifi_at.h"
#include "debug.h"
#include "hardwaretest.h"

int wifi_at_test()
{
	unsigned char tmp[1024];
	int ret;
	
	wifi_init();
	
	wifi_hardware_poweron();

	dbg("After power on\n");
	ret = wifi_read(tmp, 1024, 10*1000);
	dbg("After power on wifi_read[%d] = [%s]\n", ret, tmp);
	hexdump(tmp, ret);
    
	if (wifi_at_DisableEcho() != 0)
	{
		dbg("wifi_at_DisableEcho error\n");
		ret = -1;
        goto out;
	}

	if (wifi_at_SetStationMode() != 0)
	{
		dbg("wifi_at_SetStationMode error\n");
		ret = -2;
        goto out;
	}

	if (wifi_at_ConnectAp() != 0)
	{
		dbg("wifi_at_ConnectAp error\n");
		ret =  -3;
        goto out;
	}
    ret = 0;
out:
    wifi_hardware_poweroff();
    
	return ret;
}


int TestWifi(void*m){
    int ret;
    char buf[32];
    enterTestItem("WIFI Test");LcdDisplayMessageCenterC("WIFI Auto Test...");
    ret=wifi_at_test();
    if(ret){
        memset(buf,0,sizeof(buf));
        sprintf(buf,"WIFI Test FAIL,code=%d",ret);
        LcdDisplayMessageCenterC(buf);
    }else{
        LcdDisplayMessageCenterC("WIFI Test PASS");
    }
    mdelay(1500);
    return ret?NG:OK;
}

