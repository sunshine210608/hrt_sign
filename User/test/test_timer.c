/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : test_timer.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-10-08
 * 
 * 
 * @note History:
 * @note        : Jay 2019-10-08
 * @note        : 
 *   Modification: Created file

********************************************************************************/

#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "systick.h"
#if 0
void test_udelay(void){
    int i;
    GPIO_InitTypeDef GPIO_InitStruct;
    
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStruct.GPIO_Remap = GPIO_Remap_1;
    GPIO_Init(GPIOA, &GPIO_InitStruct);

    dbg("%s\n",__FUNCTION__);
    
    while(1){
        GPIO_SetBits(GPIOA, GPIO_Pin_4);
        udelay(100);
        GPIO_ResetBits(GPIOA, GPIO_Pin_4);
        udelay(100);
    }
}



/*
test 2 different timer at same time
*/
void test_two_timer(void){
    /*struct tm *pgettm;
    rtc_gettime(&pgettm);
    dbg("RTC time:%d %d %d %d:%d:%d\n",pgettm->tm_year+1900,pgettm->tm_mon+1,pgettm->tm_mday,pgettm->tm_hour,pgettm->tm_min,pgettm->tm_sec);
    */
    tick ts,tdiff;
    FUNCIN;
    test_buzzer();
    while(1){
        dbg("udelay 3000ms\n");
        ts=get_tick();
        udelay(3000*1000);
        tdiff=get_diff_tick(get_tick(), ts);
        dbg("udelay 3000ms done %dms\n",tdiff);
        
        dbg("mdelay 5000ms\n");
        ts=get_tick();
        mdelay(5000);
        tdiff=get_diff_tick(get_tick(), ts);
        dbg("mdelay 5000ms done %dms\n",tdiff);
    }
}
#endif

