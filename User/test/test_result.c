#include <stdio.h>
#include <mhscpu.h>
#include <debug.h>
#include <time.h>
#include "hardwaretest.h"
#include "lcdfont.h"
#include "printer.h"
#include "_compileinfo.h"

extern SMenu *MAINMENU;

ItemResult *HWResults=NULL;


int GetResult(char*item,ItemResult **target)
{
	int i;
	ItemResult *presults;
	presults=HWResults;

	if(!item)
	{
		dbg("item name %s error\n",item);
		return -1;
	}
	for(i=0;i<MAX_MENUITEM_NUMBER;i++,presults++)
	{
		if(!strlen(presults->name)){
			*target=presults;
			return 1;	
		}
		if(!strncmp(item,presults->name,strlen(item)))
			break;
	}
	if(i==MAX_MENUITEM_NUMBER)
	{
		dbg("Items full\n");
		return -2;
	}
	*target=presults;
	return 0;
}

int TestResult_Init(int autotest)
{
	
	int i,ret;	
    // TODO:read from flash		
	HWResults=(ItemResult*)malloc(sizeof(ItemResult)*MAX_MENUITEM_NUMBER);
	if(HWResults==NULL)
	{
		dbg("HWResults mmap failed\n");
		return -1;
	}
    memset((uint8_t*)HWResults,0,sizeof(ItemResult)*MAX_MENUITEM_NUMBER);
    
	//init items
	for(i=0;i<(sizeof(MAINMENU->items)/sizeof(SMenuItem));i++)
	{
		if(!MAINMENU->items[i].name){
			break;
		}

        //submenu
        if(MAINMENU->items[i].submenu != NULL){
            /*add sub menu item result*/
            SMenuItem *sitem = MAINMENU->items[i].submenu->items;
            if(sitem->name ==NULL) continue;
            //dbg("%s has submenu\n",MAINMENU->items[i].name);
            do{
                //dbg("subtestitem:%s\n",sitem->name);
                ret=GetResult(sitem->name,&sitem->result);
                if(ret>0)
        		{
        			strcpy(sitem->result->name,sitem->name);
        			sitem->result->pass=sitem->result->failed=0;
                    sitem->result->autotest=autotest;
                    sitem->result->lastresult = NG;
        		}else if(ret<0){
        			dbg("Warning :it's full\n");
                    return -2;
                }

                sitem++;
            }while(sitem->name != NULL);
        }else{
            /*add new item result*/
            //dbg("testitem:%s\n",MAINMENU->items[i].name);
    		ret=GetResult(MAINMENU->items[i].name,&(MAINMENU->items[i].result));
    		if(ret>0)
    		{
    			strcpy(MAINMENU->items[i].result->name,MAINMENU->items[i].name);
    			MAINMENU->items[i].result->pass=MAINMENU->items[i].result->failed=0;
                MAINMENU->items[i].result->autotest=autotest;
                MAINMENU->items[i].result->lastresult = NG;
    		}else if(ret<0){
    			dbg("Warning :it's full\n");
                return -2;
            }
        }		
	}
	dbg("Test Mod :%d\n",autotest);
	return 0;
}


int TestResult_DeInit(void){
    if(HWResults){
        dbg("free HWResults memory\n");
        free(HWResults);
    }
		return 0;
}

void ShowTestResult(void){
    ItemResult * presults=HWResults;
    if(presults==NULL) return;
    while(strlen(presults->name)){
        dbg("%s lastresult:%d,pass:%d,fail:%d,total:%d\n",presults->name,presults->lastresult,presults->pass,presults->failed,presults->total);   
        presults++;
    }
}


int RecordResult(void*m,char ok)
{
	char buf[32]={0};
	SMenuItem *si=(SMenuItem*)m;

	ItemResult *target=NULL;

	if(si->name==NULL)
		return -1;
	
	if(GetResult(si->name,&target))
	{
		dbg("GetResult error\n");
		return -2;
	}

	if(ok != OK && ok != NG)
	{
        uint16_t key;
        KeyBoardClean();
		sprintf(buf,"%s OK?",si->name);
		LcdClear();
		LcdPutsc(buf, 3);
		LcdPutsc(" NG              OK ", 10);
		
		do{
			KeyBoardRead(&key);
		}while(key != KEY_OK && key!=KEY_CANCEL);		
		//change
		if(key==KEY_OK){
			target->pass++;
            target->lastresult = OK;
        }
		else{
			target->failed++;
            target->lastresult = NG;
        }
	}else{
		if(ok==OK)
			target->pass++;
		else if(ok==NG)
			target->failed++;
        target->lastresult = ok;
	}
	target->total++;
    
    return 0;
}


/*
print out all the last test result.
*/
int TestResult(void*m){
    ItemResult * presults=HWResults;
    int ret;
    char buf[48];
    struct font_desc *font_p = (struct font_desc *)&font_16x32;
    struct tm gettm;
    
    if(presults==NULL) return NG;
    ret= PrintOpen();
    if (ret < 0) {
        dbg("printer open error %d",ret);
        return NG;
    }
    PrintSetDepth(1500);

    PrintLoadFont(font_p);
    
    PrintPutsc("Hardware Test Result");
    PrintPutsc(" ");
    rtc_gettime(&gettm);
    sprintf(buf,"Date:%d %d %d %d:%d:%d\n",gettm.tm_year+1900,gettm.tm_mon+1,gettm.tm_mday,gettm.tm_hour,gettm.tm_min,gettm.tm_sec);
    PrintPutsc(buf);

    //version
    sprintf(buf,"Version:%s\n",FWVERSION);
    PrintPutsc(buf);
    
    while(strlen(presults->name)){
        dbg("%s lastresult:%d,pass:%d,fail:%d,total:%d\n",presults->name,presults->lastresult,presults->pass,presults->failed,presults->total);   
        sprintf(buf,"%s:%s\n",presults->name,presults->lastresult==OK?"PASS":"FAIL");
        if(!strncmp("Result",presults->name,strlen(presults->name)) || !strncmp("About",presults->name,strlen(presults->name)))
            dbg("ignore it\n");
        else
            PrintPutsc(buf);
        presults++;
    }
    PrintPutsc(" ");
    PrintPutsc(" ");
    PrintPutsc(" ");
    
    ret = PrintWrite();
    PrintClose();
    return OK;
}
