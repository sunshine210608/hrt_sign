#ifndef __GPRS_AT_H__
#define __GPRS_AT_H__

//文档为1500,此处定义比文档略小
#define GPRS_AT_TCP_SND_BUFFER_MAX_SIZE	1450
#define GPRS_AT_TCP_RCV_BUFFER_MAX_SIZE	1450

#define GPRS_AT_OFFLINE_ON	0
#define GPRS_AT_OFFLINE_OFF	1

//#define GPRS_AT_DEBUG_ON_LCD

void gprs_lcdDebug(char *szSnd, char *szRcv);

int gprs_at_poweron(void);

int gprs_at_MoudleIsActive(int num);

int gprs_at_CheckSIM(void);

int gprs_at_GetSignalQuality(void);

int gprs_at_CheckRegister(void);

int gprs_at_CheckGprsAttachment(void);

int gprs_at_SetAPN(char *apn, char *user, char *pwd);

int gprs_at_SetHead(void);

int gprs_at_SetRecvMode(void);

int gprs_at_BuildGprsLink(void);

int gprs_at_CloseGprs(void);

int gprs_at_TcpConnectHost(char *hostIp, char *hostPort);

int gprs_at_TcpSnd(unsigned char *sndData, int sndLen);

int gprs_at_TcpRcv(unsigned char *rcvData, int expRcvLen, int timeoutMs);

int gprs_at_TcpDisconnectHost(void);

int gprs_at_OfflineOnOff(int offlineOnOff);

int gprs_at_DisableEcho(void);

int gprs_at_PowerOff(void);

int gprs_at_GetAutoAnswer(void);

int gprs_at_SetAutoAnswer(void);

int gprs_at_save(void);

int gprs_at_info(void);

int gprs_at_Config_Cur_Context(void);

int gprs_at_InitPDP(void);

int gprs_at_actgprs(char preChkStat, int loopMax);

int gprs_at_Tcp_RcvBuf_OnOff(int OnOff);

int gprs_at_location(char *szLocation);

int gprs_at_get_cclk(struct tm * tmDateTime);

int gprs_at_iccid(char *pszICCID);

int gprs_at_imei(char *pszIMEI);

int gprs_at_GetLocationFlow(char *szLocation);

int gprs_at_connState(char *pszState);

int gprs_at_test(void);

int gprs_at_domain(char *pszHostName, char *pszHostIp);

//输出: MCC,MNC,LAC/TAC(16进制),CI(16进制)
int gprs_at_GetCellInfo(char *pszCellInfo);

int gprs_at_IMSI(char *pszIMSI);

int gprs_at_QueryActStatus(void);

int gprs_at_QFENG(char* msg);

#endif
