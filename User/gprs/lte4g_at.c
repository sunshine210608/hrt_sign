#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <mhscpu.h>
#include "serial.h"
#include "gprs.h"
#include "debug.h"
#include "gprs_at.h"
#include "systick.h"
#include "common.h"
#include "user_projectconfig.h"

/**********************************
2022-11-10 支持九联模块(id为6,兼容处理:<1>不支持AT+CTZU=3 <2>NTP和CCLK返回UTC时间,需加时区处理 <3>AT+QISTATE=1,0改AT+QISTATE)
2022-11-10 NTP服务器从阿里云NTP1改为国家授时服务cn.ntp.org.cn
2022-12-10 骐俊160R模块适应性修改(增加基站信息指令等待时间)
2022-12-15 九联模块上电RDY适应性修改
2022-12-21 增加MCU变频处理(后测试发现变频后对显示和串口可能有影响,停用)
2022-12-23 增加版本
2022-12-26 获取IP地址兼容同时返回IP4和IP6的情况
2023-02-08 高低波切换时根据波特率更改主频
2023-02-22 <1>变频后重新计算tick时间 <2>稍微延长透传+++挂断等待时间 <3>设置底层串口字符间隔为20ms <4>设置清串口等待时间30ms
**********************************/

//4g通讯程序,为与gprs兼容,函数名相同
#ifdef USE_LTE_4G

#define COMM_CALLBACK
//#define TEST_4G_MODULE

//#define XinWei_MODULE           //信位008之前的版本设置波特率无返回,读取波特率返回值有误

//#define USE_HIGH_BAUDRATE         //改为user_projectconfig.h中定义
#ifndef HIGH_BAUDRATE
//#define HIGH_BAUDRATE   921600
#define HIGH_BAUDRATE   460800
//#define HIGH_BAUDRATE   230400
#define LOW_BAUDRATE    115200
#endif

//单独屏蔽日志
#ifndef FOR_JSD_TEST
#undef  dbg
#define dbg(x...)		do{}while(0)
#endif
    
#define GPRS_AT_RSP_ERR1 "ERROR\r"
#define GPRS_AT_RSP_ERR2 "ERROR:"

extern void (*gl_CommCallbackFunc)(void);

static char sg_cModuleType=0;
int gprs_at_ModuleInfo(char* msg);

static int sg_iTcpTAMode=0;          //Transparent Access Mode
void vSetTcpTAMode(int mode);
int  iGetTcpTAMode(void);
int gprs_at_TcpConnectHostTA(char *hostIp, char *hostPort, int timeoutMs);
int gprs_at_TcpSndTA(unsigned char *sndData, int sndLen);
int gprs_at_TcpRcvTA(unsigned char *rcvData, int expRcvLen, int timeoutMs);
int gprs_at_TcpDisconnectHostTA(void);
int gprs_at_cmd_sndrcv(char *pszReq, char *pszCheckRsp, int iTimeOut, int icrflag, int iclearflag, char *pszOutRsp, int iOutSize);
int iMcusetclk(uint32_t baud);

char *pszGet4GVer(char *pszVer)
{
    const char *ver="V23022201";
    if(pszVer)
        strcpy(pszVer, ver);
    return (char*)ver;
}

//1-移远 2-骐俊AM/DM 3-爱联 4-信位 5-骐俊160R 6-九联
char get4gmoduletype(void)
{
    return sg_cModuleType;    
}

//取通讯模块信息,格式: 厂商名称\r + 型号\r + Revision: <revision>\r\r\r + OK\r
//(版本字段骐俊没有“Revision: ”开头,与文档不符)
/*
Cheerzing
ML160
ML160_1.4.13.1362_21011814_R

OK
*/
int gprs_at_ModuleInfo(char* msg)
{
	int ret;
	char recv[600];
    int loop=0;
    
    do{
        memset(recv,0,sizeof(recv));
        ret = gprs_at_cmd_sndrcv("ATI\r", "OK\r", 1500, 1, 1, recv, sizeof(recv));
        if(ret==0 && strstr(recv, "Cheerzing")==NULL && strstr(recv, "Quectel")==NULL 
            && strstr(recv, "Ai-Link")==NULL && strstr(recv, "simware")==NULL && strstr(recv, "UNIONMAN")==NULL)
        {
            ret=-1;
        }
        if(ret && ret!=-2)
            mdelay(200);
    }while(ret!=0 && ++loop<3);

    if(ret==0)
    {
        sg_cModuleType=0;
        
        if(strstr(recv, "Quectel"))             //移远模块
            sg_cModuleType=1;
        else if(strstr(recv, "Cheerzing"))      //骐俊模块
        {
            sg_cModuleType=2;
            if(strstr(recv, "ML160R"))
                sg_cModuleType=5;
        }else if(strstr(recv, "Ai-Link"))        //爱联模块
            sg_cModuleType=3;
        else if(strstr(recv, "simware"))         //信位模块
            sg_cModuleType=4;
        else if(strstr(recv, "UNIONMAN"))        //九联模块
            sg_cModuleType=6;
        
        dbg("4G ModuleType=%d\n", sg_cModuleType);
        
        if(msg)
            strcpy(msg, recv);
                
#if 0//def FOR_JSD_TEST
        if(sg_cModuleType==6)
        {
            ret=gprs_at_cmd_sndrcv("at+ecpcfg?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv));
            if(strstr(recv, "\"pwrKeyMode\":0,")!=NULL)
                ret=gprs_at_cmd_sndrcv("at+ecpcfg=\"pwrKeyMode\",2\r", "OK\r", 1000, 1, 1, recv, sizeof(recv));
        }
#endif        
        //gprs_at_cmd_sndrcv("AT+IPR?\r", "OK", 800, 1, 1, NULL, 0);
        //gprs_at_cmd_sndrcv("AT+QCCID\r", "OK\r", 500, 1, 1, NULL, 0);
#if 0//def JTAG_DEBUG
        if(sg_cModuleType==3)
        {
            //gprs_at_cmd_sndrcv("AT+CGMR\r", "OK\r", 500, 1, 1, NULL, 0);
			//查看当前休眠模式
            gprs_at_cmd_sndrcv("AT+QSCLK?\r", "OK\r", 500, 1, 1, NULL, 0);
        }
#endif
    }
	return ret;
}

void gprs_lcdDebug(char *szSnd, char *szRcv)
{
#ifdef GPRS_AT_DEBUG_ON_LCD
	LcdClear();

	LcdPutsl("List GPRS Snd Buffer", 1);
	LcdPutsl(szSnd, 2);

	LcdPutsl("List GPRS Rcv Buffer", 5);
	LcdPutsl(szRcv, 6);
#endif
}

//清gprs串口接收缓存
void gprs_clear(void)
{
    uint8_t ch;
#ifdef FOR_JSD_TEST
	int flag = 1; // -1:不打印clear日志；1:打印日志(有clear数据的时候才打印起始终止行)

	while (gprs_read((char *)&ch, 1, 30) > 0)
	{
        
		if (flag >= 0)
		{
			if (flag == 1)
			{
				flag = 0;
				dbg("gprs at clear...\n");
			}
			dbg("clr ch:%c=%02X\n", ch, ch);
		}
	}
	if (flag == 0)
		dbg("gprs at clear end.\n");
#else
    while (gprs_read((char *)&ch, 1, 30) > 0);
#endif
}

//发送gprs at指令和接收响应（tcp数据接收不适用)
//	pszReq	发送的指令
//  pszCheckRsp		指令执行成功判断字
//  iTimeOut		超时时间(ms),若为0则默认30*1000ms
//  icrflag			是否按行读取判断,少部分指令需设为0
//  iclearflag		发送指令前是否清空串口
//	pszOutRsp		输出数据缓冲区,为NULL则不输出数据
//  iOutSize		输出数据缓冲区大小,为0则不输出数据
// ret:  0:成功  -1:error   -2:timeout
int gprs_at_cmd_sndrcv(char *pszReq, char *pszCheckRsp, int iTimeOut, int icrflag, int iclearflag, char *pszOutRsp, int iOutSize)
{
	int ret, ret2;
	char *pszOut;
	int iMaxLen = (iOutSize > 0) ? iOutSize : 200;
	char szRspTmp[iMaxLen + 1];
	int iReTry = 1;
	tick tmEnd;
	//	char *pTmp;

	if (iclearflag)
		gprs_clear();

	if (iTimeOut <= 0)
		iTimeOut = 15 * 1000;

	ret2 = -2;    
	szRspTmp[0] = 0;
    //memset(szRspTmp, 0, sizeof(szRspTmp));
	while (iReTry)
	{
        mdelay(1);
		iReTry = 0;
        if(pszReq)
        {
            ret = gprs_write(pszReq, strlen(pszReq));
            dbg("gprs_write[%d] = [%s]\n", ret, pszReq);
        }
		pszOut = szRspTmp;
		tmEnd = get_tick()+ iTimeOut;
		while (get_tick() < tmEnd )
		{            
            #ifdef COMM_CALLBACK
            if(gl_CommCallbackFunc)
            {
                //dbg("exec commcallbackfunc\n");
                gl_CommCallbackFunc();
            }
            #endif
            
            if(pszOut>szRspTmp+sizeof(szRspTmp)-1)
            {
                //超过最大接收长度
                ret2=-3;
                break;
            }
            
			ret = gprs_read(pszOut, 1, 100);
			if (ret <= 0)
			{
				continue;
			}
            
			//dbg("rcv ch:%c=%02X\n", pszOut[0], pszOut[0]);
			if (icrflag && pszOut[0] != 0x0A) //判断\r\n的0A
			{
				++pszOut;
				continue;
			}
			pszOut[1] = 0x00;
			++pszOut;

			//接收到一行响应，开始判断
			if (strstr((char *)szRspTmp, (char *)pszCheckRsp))
			{
				ret2 = 0;
				break;
			}

			if (strstr((char *)szRspTmp, "+QIRDI: 0,1,0"))
			{
				dbg("rcv:[%s], retry to recv...\n", szRspTmp);
				gprs_clear();
				memset(szRspTmp, 0, sizeof(szRspTmp));
				iReTry = 1;
				break;
			}

			/*
			if(strstr((char*)szRspTmp, WIFI_AT_RSP_BUSYP) || strstr((char*)szRspTmp, WIFI_AT_RSP_BUSYS))
			{
				dbg("gprs busy... retry\n");
				szRspTmp[0]=0;
				pszOut=szRspTmp;
				mdelay(500);
				gprs_clear();
				iReTry=1;
				break;
			}
			*/
			if (strstr((char *)szRspTmp, GPRS_AT_RSP_ERR1) || strstr((char *)szRspTmp, GPRS_AT_RSP_ERR2))
			{
				ret2 = -1;
				break;
			}

			ret = -2;
			continue;
		}
	}
	if (pszOutRsp && iOutSize > 0)
    {
		strncpy((char *)pszOutRsp, (char *)szRspTmp, iOutSize-1);
        pszOutRsp[iOutSize-1]=0;        //0结尾
    }
    
	dbg("gprs_read [%d] = [%s], ret2=%d\n", strlen(szRspTmp), szRspTmp, ret2);
	//hexdump(pszOut, ret);

	return ret2;
}


#define POWERON_RDY     "RDY\r"        //ok for yiyuan
#define POWERDOWN       "POWERED DOWN"

extern void LcdPutscc(char *message);
extern void lcdflush(void);

/**
 * @fn          int gprs_poweron_readycheck(void)
 * @brief       check 4g module if power on success.
                have to call after gprs_poweron or gprs_modulepin_reset
 * 
 * @param[in]   void  
 * @return      int
                0 : power on success
                -1: fail
 */
int gprs_poweron_readycheck(void)
{
    int ret;
    int loop = 0;
    char buf[128]={0};
    int tks = get_tick();
    int i;
    int iOkNum=0;
    
    /*
    read the data auto reported after the module been poweron.
    max timeout is 3000 ms.
    */
    do{
        memset(buf,0,sizeof(buf));
        ret = gprs_read(buf, sizeof(buf), 1*1000);
        if(ret > 0)
        {
            dbg("Get Data From Module ret=%d,strlen=%d,data=[%s]\n",ret,strlen(buf),buf);
            if(strstr(buf,POWERON_RDY) && strstr(buf,POWERDOWN) == NULL)
            {
                dbg("Got RDY Success\n");
                break;
            }
            if(strlen(buf)<ret-4) //九联字符串中间有\0,需要按字符来比较
            {
                for(i=strlen(buf)+1; i<ret-3; i++)
                {
                    if(memcmp(buf+i, "RDY", 3)==0)
                    {
                        dbg("Got RDY Success\n");
                        break;
                    }
                }
                if(memcmp(buf+i, "RDY", 3)==0)  //跳出do-while
                    break;
            }
        }
    }while(!is_timeout(tks, 10*1000));
    
    //gprs_at_MoudleIsActive(5);

    /*
    check ATE1 commmand respinse.
    */

	//改为:ATE1连续两次正常返回才算上电成功--20221220
    do{
        memset(buf,0,sizeof(buf));
        ret = gprs_at_cmd_sndrcv("ATE1\r", "OK\r", 300, 1, 1, buf, sizeof(buf));
        if(ret == 0)
        {
            if(strstr(buf, "ATE1") != NULL)
            {
                ++iOkNum;
                dbg("check ATE1 ok %d.\n", iOkNum);
                mdelay(50);
                continue;
            }
            iOkNum=0;
            ret = -1;
            mdelay(50);
        }else
        {
            iOkNum=0;
            mdelay(200);
        }
        dbg("ret=%d,buf=[%s],iOkNum=%d\n",ret,buf,iOkNum);
    }while (iOkNum<2 && ++loop <= 4);
            
    return ret;
}

static int powerontime=0;
//注意:内部有调用gprs_poweron_readycheck可能执行了ATE1(或者模块上电后默认是ATE1)，本函数调用后需先执行ATE0
int gprs_at_poweron(void)
{
	int ret;
      
    serialsetchartimeout(20);		//设置底层字符串间隔判定时间
    
    ret=gprs_poweron();
    if(ret==1)
        powerontime=get_tick();
/*
	if (gprs_at_MoudleIsActive(30) == 0)
	{
        dbg("Need gprs_modulepin_reset()\n");
        gprs_poweroff();
        mdelay(200);
        gprs_poweron();
        if (gprs_at_MoudleIsActive(30) == 0)
        {
            dbg("gprs_modulepin_reset() can't work\n");
            //gprs_lcdDebug("AT", "No response");
            return -99;
        }
        mdelay(1000);
    }
*/
    if(ret==0 && gprs_at_MoudleIsActive(1))
    {
        return 0;
    }
	if (gprs_poweron_readycheck())
	{
        dbg("ready check fail,we should reset?\n");   
        gprs_poweroff();  // power off vcc of 4g
        return -1;
    }
#if 0//def USE_HIGH_BAUDRATE    
    if(gprs_at_setbaud(HIGH_BAUDRATE))
    {
        gprs_poweroff();  // power off vcc of 4g
        return -1;
    }
#endif    
	return 0;
}

/*
RECOMMEND reset API.
reset by reset pin.
*/
int gprs_rstpin_reset(void)
{
	gprs_modulepin_reset();
    
	if (gprs_poweron_readycheck())
	{
        dbg("ready check fail,we should reset?\n");
        return -1;
    }
	return 0;
}

/*
power off and then power on again.
NOT RECOMMEND
*/
//注意:内部有调用gprs_poweron_readycheck可能执行了ATE1(或者模块上电后默认是ATE1)，本函数调用后需先执行ATE0
int gprs_hardware_reset(void)
{
/*    
	gprs_poweroff();
    mdelay(1000);
    gprs_poweron();
    
	if (gprs_poweron_readycheck())
	{
        dbg("ready check fail,we should reset?\n");
        return -1;
    }
	return 0;
*/  
    if(gprs_rstpin_reset()==0)
        return 0;
    mdelay(1000);
    
    if(get_tick()-powerontime<60*1000)
        return -1;
    
    gprs_poweroff();
    mdelay(3000);
    if(gprs_poweron() < 0)
	{
        return -1;
    }
    powerontime=get_tick();
	if (gprs_poweron_readycheck())
	{
        dbg("ready check fail,we should reset?\n");
        gprs_poweroff();  // power off vcc of 4g
        return -1;
    }
#if 0//def USE_HIGH_BAUDRATE    
    if(gprs_at_setbaud(HIGH_BAUDRATE))
    {
        gprs_poweroff();  // power off vcc of 4g
        return -1;
    }
#endif
	return 0;
}

int gprs_at_MoudleIsActive(int num)
{  
	int ret=0;
    int loop=0;

    if(num<=0)
        num=1;
    
    do
    {
        //vDispVarArg(3, "at %d", loop);
        if(ret && ret!=-2)
			mdelay(200);        
        ret = gprs_at_cmd_sndrcv("AT\r", "OK\r", 300, 1, 1, NULL, 0);
    }while(ret && ++loop<num);
    ret = (ret == 0 ? 1 : 0);
	dbg("gprs_AT_MoudleIsActive return %d\n", ret);
	return ret;
}

int gprs_at_CheckSIM(void)
{
	char recv[100];
	int ret=0;
    int loop=0;
    
    do{
		if(ret && ret!=-2)
        	mdelay(500);        
		memset(recv, 0, sizeof(recv));
        ret = gprs_at_cmd_sndrcv("AT+CPIN?\r", "OK\r", 3000, 1, 1, recv, sizeof(recv));
        if((ret==-1 && loop>1))      //-1:error
		{
            //上电后, sim卡没ready之前, 移远返回14(busy)，骐俊可能返回53/13, 爱联返回13
            if(!strstr(recv, "+CME ERROR: 14") && !strstr(recv, "+CME ERROR: 53") && !strstr(recv, "+CME ERROR: 13"))
                break;
		}
    }while (ret != 0 && ++loop <= 50);
    dbg("gprs_at_CheckSIM return %d\n", ret);
	if (ret == 0)
	{
		if(strstr(recv, "+CPIN: READY"))
			return 0;        
		if(strstr(recv, "+CPIN: NO SIM") || strstr(recv, "+CME ERROR: 10") || strstr(recv, "NOT INSERTED"))
                return -100;
		return -2;
	}else
	{
        /*
        {
            char *p;
            p=recv;
            while(*p)
            {
                if(*p==0x0d)
                    *p='d';
                if(*p==0x0a)
                    *p='a';
                p++;
            }
            vDispVarArg(2, "ret=%d,recv:%d", ret, strlen(recv));
            _vDisp(3, recv);
            if(strlen(recv)>26)
                _vDisp(4, recv+26);
            mdelay(2000);
        }
        */       
        
		if(strstr(recv, "+CPIN: NO SIM") || strstr(recv, "+CME ERROR: 10") || strstr(recv, "NOT INSERTED"))
			return -100;
		return -1;
	}
}

int gprs_at_CheckSIMQuick(void)
{
	char recv[100];
	int ret;

    ret = gprs_at_cmd_sndrcv("AT+CPIN?\r", "OK\r", 2000, 1, 1, recv, sizeof(recv));
	if (ret == 0)
	{
		if(strstr(recv, "+CPIN: READY"))
			return 0;
		if(strstr(recv, "+CPIN: NO SIM") || strstr(recv, "+CME ERROR: 10") || strstr(recv, "NOT INSERTED"))
			return -100;
		return -2;
	}else
	{
		if(strstr(recv, "+CPIN: NO SIM") || strstr(recv, "+CME ERROR: 10") || strstr(recv, "NOT INSERTED"))
			return -100;
		return -1;
	}
}

int gprs_at_CheckSIM2(char *siminfo)
{
	char recv[100];
	int ret=0;
    int loop=0;
    
	siminfo[0]=0;
    do{
		if(ret && ret!=-2)
        	mdelay(500);        
        ret = gprs_at_cmd_sndrcv("AT+CPIN?\r", "OK\r", 2000, 1, 1, recv, sizeof(recv));
        if((ret==-1 && loop>2))      //-1:error
		{
            break;
		}
    }while (ret != 0 && ++loop <= 10);
    dbg("gprs_at_CheckSIM return %d\n", ret);
	if (ret == 0)
	{
		char *p, *p1;
		p=strstr(recv, "+CPIN:");
		if(p)
		{
			p1=strstr(p, "\r");
			if(p1)
				*p1=0;
			strncpy(siminfo, p, 15);
		}
		return 0;
	}else
	{
		if(strstr(recv, "+CPIN: NO SIM") || strstr(recv, "+CME ERROR: 10") || strstr(recv, "NOT INSERTED"))
			return -100;
		return -1;
	}
}

int gprs_at_GetSignalQuality(void)
{
	char recv[500];
	int ret=0;
	char *pTmp;
	int signalQuality;
	int loop=0;
	char *p;
	int i;
	int rsrp, sinr;

    if(sg_cModuleType<1 || sg_cModuleType>6)
    {
        gprs_at_ModuleInfo(NULL);
    }
    
    ret=0;
    if(sg_cModuleType!=1)
    {
        do{
            if(ret && ret!=-2)
                mdelay(200);            
            ret = gprs_at_cmd_sndrcv("AT+CESQ\r", "OK\r", 500, 1, 1, recv, sizeof(recv));
        }while (ret != 0 && ++loop <= 2);
        if (ret == 0 && (pTmp = strstr(recv, "+CESQ:")) != NULL)
        {
            //sscanf(pTmp, "%*s%d,%*d", &signalQuality);
            //dbg("signalQuality = %d\n", signalQuality);     //<rssi> 0：-110db   1-30：……     31：-48db 
            rsrp=0;
            for(i=0; i<5; i++)
            {
                p=strchr(pTmp, ',');
                if(p==NULL)
                    return 0;
                if(i==4)	//第5个','后面跟rsrp
                    rsrp=atoi(p+1);
                pTmp=p+1;
            }
            
            signalQuality=0;
            if(rsrp<255)            //255：not detectable
            {
            /*
            0 rsrp < -140 dBm
            1 -140 dBm ≤ rsrp < -139 dBm
            2 -139 dBm ≤ rsrp < -138 dBm
            ……
            95 -46 dBm ≤ rsrp < -45 dBm
            96 -45 dBm ≤ rsrp < -44 dBm
            97 -44 dBm ≤ rsrp
            255 not known or not detectable
            */
            signalQuality = rsrp;
            }
            dbg("gprs_at_CheckSignalQuality return %d\n", signalQuality);
            return signalQuality;
        }
    }else
    {
        do{
            if(ret && ret!=-2)
                mdelay(500);
            ret = gprs_at_cmd_sndrcv("AT+QENG=\"servingcell\"\r", "OK\r", 2000, 1, 1, recv, sizeof(recv));
        }while (ret != 0 && ++loop <= 2);
        if (ret == 0 && (pTmp = strstr(recv, "LTE")) != NULL)
        {
            //+QENG:"servingcell",<state>,"LTE",<is_tdd>,<mcc>,<mnc>,<cellid>,<pcid>,<earfcn>,<freq_band_ind>,<ul_bandwidth>,<dl_bandwidth>,<tac>,<rsrp>,<rsrq>,<rssi>,<sinr>,<srxlev>
            //+QENG: "servingcell","NOCONN","LTE","TDD",460,00,CBB7281,15,38950,40,5,5,2638,-88,-6,-85,28,38
            p=strchr(pTmp, '\r');
            if(p==NULL)
                return 0;
            sinr=0;
            rsrp=0;
            *p=0;
            for(i=0; i<5; i++)
            {
                p=strrchr(pTmp, ',');
                if(p==NULL)
                    return 0;
                if(i==1)	//倒数第2个','后面跟sinr
                    sinr=atoi(p+1);
                if(i==4)	//倒数第5个','后面跟rsrp
                    rsrp=atoi(p+1);
                *p=0;
            }
            dbg("rsrp(-44~-140)=%d, sinr(0~31)=%d, rsrp->%d\n", rsrp, sinr, rsrp+141);
            signalQuality=0;
            if(rsrp<0)
            {
                /*
                极好点： RSRP>-85dBm； SINR>25
                好点： RSRP=-85～-95dBm；SINR:16-25
                中点： RSRP=-95～-105dBm；SINR:11-15
                差点： RSRP=-105～-115dBm；SINR:3-10
                极差点： RSRP<-115dB；SINR<3
                */
                //signalQuality=(rsrp+140)*0.9;
                signalQuality=rsrp+141;
            }
            dbg("gprs_at_CheckSignalQuality return %d\n", signalQuality);
            return signalQuality;
        }
    }
 (void)sinr;
	dbg("gprs_at_CheckSignalQuality return -1\n");
	return 0;
}

int gprs_at_CheckRegister(void)
{
	char recv[100];
	int ret;
	int loop = 0;
#if 0    
    gprs_at_cmd_sndrcv("AT+CREG=0\r", "OK\r", 500, 1, 1, NULL, 0);
    
	do
	{
		ret = gprs_at_cmd_sndrcv("AT+CREG?\r", "OK\r", 800, 1, 1, recv, sizeof(recv));	
		if (ret == 0 && (strstr(recv, "+CREG: 0,1")||strstr(recv, "+CREG: 0,5")))		//1或5表示已注册,1本地 5漫游
			ret = 0;
		else
		{
			if(ret!=-2)
				mdelay(500);
			ret = -1;
		}
	} while (ret < 0 && ++loop <= 30);

	dbg("gprs_at_CheckRegister return %d, loop=%d\n", ret, loop);
	if(ret)
		return ret;
#endif
    gprs_at_cmd_sndrcv("AT+CEREG=0\r", "OK\r", 500, 1, 1, NULL, 0);
    loop=0;
    ret=0;
	do
	{
        if(ret && ret!=-2)
            mdelay(500);
		ret = gprs_at_cmd_sndrcv("AT+CEREG?\r", "OK\r", 800, 1, 1, recv, sizeof(recv));	
		if (ret == 0)		//1或5表示已注册,1本地 5漫游
        {
            if(strstr(recv, "+CEREG: 0,1")||strstr(recv, "+CEREG: 0,5"))
                ret = 0;
            else
                ret = -1;
        }
	} while (ret < 0 && ++loop <= 30);

	dbg("gprs_at_CheckRegister2 return %d\n", ret);	
	return ret;
}

//输出: MCC,MNC,LAC/TAC(16进制),CI(16进制)
int gprs_at_GetCellInfo2(char *pszIMSI, char *pszCellInfo)
{
	char recv[300];
	int ret;
	char *p0, *p1;
	char imsi[30],lac[10], ci[10];
    
	pszCellInfo[0]=0;
	
	imsi[0]=0;
	lac[0]=0;
	ci[0]=0;
	
	gprs_at_IMSI(imsi);
	if(imsi[0]==0)
		return -1;
    
    if(pszIMSI)
        strcpy(pszIMSI, imsi);
    
#if 0	
	//设置格式
	ret = gprs_at_cmd_sndrcv("AT+CREG=2\r", "OK\r", 500, 1, 1, NULL, 0);	
	if (ret)
		return ret;
	//网络注册信息
	ret = gprs_at_cmd_sndrcv("AT+CREG?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv));
	//还原设置
	gprs_at_cmd_sndrcv("AT+CREG=0\r", "OK\r", 500, 1, 1, NULL, 0);

	if(ret==0)
	{
		ret=1;
		p0=recv;
		p1=strstr(p0, "+CREG: 2,");
		if(p1)
		{
			//格式: +CREG: <n>,<stat>,<lac>,<cellid>[,<act>]
			//ex:   +CREG: 2,1,"2638","CBB7281",7
			p0=p1+strlen("+CREG: ");
			
			sscanf(p0, "%*d,%*d,\"%[^\"]\",\"%[^\"]\"", lac, ci);
			if(lac[0]==0 || ci[0]==0)
				return -2;
			
			//imei的前5位为mcc[3]+mnc[2]
			sprintf(pszCellInfo, "%.3s,%.2s,%s,%s", imsi, imsi+3, lac, ci);
			dbg("cellinfo=[%s]\n", pszCellInfo);
			
			ret=0;
		}
	}
#else
	//设置格式
	ret = gprs_at_cmd_sndrcv("AT+CEREG=2\r", "OK\r", 1200, 1, 1, NULL, 0);	    //160R响应慢,加大时间
	if (ret)
    {
		return ret;
    }
	//网络注册信息
	ret = gprs_at_cmd_sndrcv("AT+CEREG?\r", "OK\r", 1200, 1, 1, recv, sizeof(recv));
	//还原设置
	gprs_at_cmd_sndrcv("AT+CEREG=0\r", "OK\r", 800, 1, 1, NULL, 0);

	if(ret==0)
	{
		ret=1;
		p0=recv;
		p1=strstr(p0, "+CEREG: 2,");
		if(p1)
		{
			//格式: +CREG: <n>,<stat>,<lac>,<cellid>[,<act>]
			//ex:   +CREG: 2,1,"2638","CBB7281",7
			p0=p1+strlen("+CEREG: ");
			
			sscanf(p0, "%*d,%*d,\"%[^\"]\",\"%[^\"]\"", lac, ci);
			if(lac[0]==0 || ci[0]==0)
				return -2;
			
			//imei的前5位为mcc[3]+mnc[2]
			sprintf(pszCellInfo, "%.3s,%.2s,%s,%s", imsi, imsi+3, lac, ci);
			dbg("cellinfo=[%s]\n", pszCellInfo);
			
			ret=0;
		}
	}
#endif
	return ret;
}

int gprs_at_GetCellInfo(char *pszCellInfo)
{
    return gprs_at_GetCellInfo2(NULL, pszCellInfo);
}

// 14.6 将可读的16进制表示串压缩成其一半长度的二进制串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
// Attention: 源串必须为合法的十六进制表示，大小写均可
//            长度如果为奇数，函数会靠近到比它大一位的偶数
static void vTwoOne(char *psIn, int iLength, unsigned char *psOut)
{
    unsigned char ucTmp;
    int i;

    for (i = 0; i < iLength; i += 2)
    {
        ucTmp = psIn[i];
        if (ucTmp > '9')
            ucTmp = toupper(ucTmp) - 'A' + 0x0a;
        else
            ucTmp &= 0x0f;
        psOut[i / 2] = ucTmp << 4;

        ucTmp = psIn[i + 1];
        if (ucTmp > '9')
            ucTmp = toupper(ucTmp) - 'A' + 0x0a;
        else
            ucTmp &= 0x0f;
        psOut[i / 2] += ucTmp;
    } // for(i=0; i<uiLength; i+=2) {
}

//取基站信息,输出固定为5字节长的10进制数,不足位数前补0(本接口已废弃)
int gprs_at_GetLACandCI(char *pszLAC, char *pszCellId)
{
	char recv[200];
	int ret;
	char *p0, *p1;
    
	pszLAC[0]=0;
	pszCellId[0]=0;

	//开工程模式
	ret = gprs_at_cmd_sndrcv("AT+QENG=1,0\r", "OK\r", 500, 1, 1, NULL, 0);	
	if (ret)
		return ret;
	//取工程模式信息
	ret = gprs_at_cmd_sndrcv("AT+QENG?\r", "OK\r", 2000, 1, 1, recv, sizeof(recv));
	//还原设置
	gprs_at_cmd_sndrcv("AT+QENG=0\r", "OK\r", 500, 1, 1, NULL, 0);

	if(ret==0)
	{
		ret=1;
		p0=recv;
		p1=strstr(p0, "+QENG: 0,");		//基站列表第一条
		if(p1)
		{
			//格式: +QENG: 0,<mcc>,<mnc>,<lac>,<cellid>,<bcch>,<bsic>,<dbm>......
			//ex:   +QENG: 0,460,00,2623,e25,49,14,-68,40,40,5,6,x,x,x,x,x,x,x
			//取第四个和第五个逗号前的信息
			p0=p1+strlen("+QENG: 0,");

			//第二逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			p0=p1+1;

			//第三逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			p0=p1+1;

			//第四逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			*p1=0;
			strcpy(pszLAC, p0);
			p0=p1+1;
			
			//第五逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			*p1=0;
			strcpy(pszCellId, p0);
			p0=p1+1;

			dbg("lac=[%s], cellid=[%s]\n", pszLAC, pszCellId);
			
			ret=0;
		}
	}

#if 0
	//先设置2要求返回小区信息
	ret = gprs_at_cmd_sndrcv("AT+CGREG=2\r", "OK\r", 500, 1, 1, NULL, 0);	
	if (ret)
		return ret;
	//取基站信息
	ret = gprs_at_cmd_sndrcv("AT+CGREG?\r", "+CGREG:", 2000, 1, 1, recv, sizeof(recv));
	//还原设置
	gprs_at_cmd_sndrcv("AT+CGREG=0\r", "OK\r", 500, 1, 1, NULL, 0);

	if(ret==0)
	{
		ret=1;

		//格式:+CGREG: mode,state,lac,cellid.  ex: +CGREG: 2,1,"2623","E25"
		p0=recv;
		p1=strstr(p0, "+CGREG: 2,");
		if(p1)
		{
			p0=p1+strlen("+CGREG: 2,");
			p1=strchr(p0, 0x0d);
			if(p1)
				*p1=0;
			
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			p0=p1+1;

			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			*p1=0;
			dbg("lac=[%s]", p0);
			p0=p1+1;

			p1=strchr(p0, ',');
			if(p1)
			{
				*p1=0;
			}
			dbg("cellid=[%s]", p0);

			ret=0;
		}
	}
#endif

	//4位长16进制字符串,转为5位长的10进制数(最大65535)
	if(ret==0)
	{
		unsigned char tmp[10];
		if(strlen(pszLAC)<4)
		{
			strcpy(recv, "0000");
			strcpy(recv+4-strlen(pszLAC), pszLAC);
		}else
			strcpy(recv, pszLAC);
		vTwoOne(recv, 4, tmp);
		sprintf(pszLAC, "%05u", tmp[0]*256+tmp[1]);

		if(strlen(pszCellId)<4)
		{
			strcpy(recv, "0000");
			strcpy(recv+4-strlen(pszCellId), pszCellId);
		}else
			strcpy(recv, pszCellId);
		vTwoOne(recv, 4, tmp);
		sprintf(pszCellId, "%05u", tmp[0]*256+tmp[1]);		
	}
	dbg("out lac=[%s], cellid=[%s]\n", pszLAC, pszCellId);
	return ret;
}

int gprs_at_CheckGprsAttachment(void)
{
	char recv[100];
	int ret;
	int loop = 0;

    ret=0;
	do
	{
        if(ret && ret!=-2)
            mdelay(500);
		ret = gprs_at_cmd_sndrcv("AT+CGATT?\r", "OK\r", 5*1000, 1, 1, recv, sizeof(recv));	
		if (ret == 0)
        {
            if( strstr(recv, "+CGATT: 1")==NULL )
                ret = -1;
        }
	} while (ret < 0 && ++loop <= 20);
	dbg("gprs_at_CheckGprsAttachment return %d\n", ret);
	return ret;
}

//设置apn
//必须在Act前调用；若已act再调用设置apn,需重启或deact或切换CFUN后，新apn才生效。切换CFUN：AT+CFUN=0再AT+CFUN=1
int gprs_at_SetAPN(char *apn, char *user, char *pwd)
{
	int ret=0;
#if 0	//当前客户不使用apn，没有apn卡无法调试，暂时屏蔽不用	
    char send[100];
	char recv[100];
	char szApn[16+1], szUser[16+1], szPwd[16+1];
	int loop = 0;
	char *p;

	if(apn==NULL || apn[0]==0)
		return 0;
	szUser[0]=0;
	szPwd[0]=0;
	strcpy(szApn, apn);
	if(user)
		strcpy(szUser, user);
	if(pwd)
		strcpy(szPwd, pwd);

	sprintf(send, "AT+QICSGP=1,1,\"%s\",\"%s\",\"%s\",1\r", szApn, szUser, szPwd);
	//sprintf(send, "AT+QICSGP=1,1,\"%s\",\"%s\",\"%s\",1\r", "apntest", "456", szPwd);		//test
	
	//查询apn设置
	ret = gprs_at_cmd_sndrcv("AT+QICSGP=1\r", "OK\r", 500, 1, 1, recv, sizeof(recv));
	if(ret==0 && (p=strstr(recv, "+QICSGP: "))!=NULL)
	{
		p=strchr(p, ',');
		if(p)
		{
			if(memcmp(send+14, p+1, strlen(send)-14-3)==0)		//比较"apn","user","pwd",一致则直接退出,不重复设置apn(奇骏激活后再设置apn会失败)
			{
				dbg("apn is same, quick return 0\n");
				return 0;
			}
		}
	}
	
	//查询是否已激活,若已激活则断开,设置apn后重新激活?
    if(gprs_at_QueryActStatus())
    {
        gprs_at_CloseGprs();
    }
    
	do
	{    
        //ret = gprs_at_cmd_sndrcv("AT+QICSGP=1,1,\"UNINET\"\r", "OK\r", 2000, 1, 1, NULL, 0);
        //ret = gprs_at_cmd_sndrcv("AT+QICSGP=1,1,\"CMIOT\"\r", "OK\r", 2000, 1, 1, NULL, 0);                 //移动物联网
        ret = gprs_at_cmd_sndrcv(send, "OK\r", 2000, 1, 1, NULL, 0);   //移动物联网
        if(ret && ret!=-2)
            mdelay(200);
    } while (ret < 0 && ++loop <= 3);

	dbg("gprs_at_SetAPN return %d\n", ret);

#if 1
	//移远模块设置apn后必须AT+CFUN或重启
	ret=gprs_at_cmd_sndrcv("AT+CFUN=0\r", "OK\r", 5*1000, 1, 1, NULL, 0);
	if(ret==0)
		ret=gprs_at_cmd_sndrcv("AT+CFUN=1\r", "OK\r", 5*1000, 1, 1, NULL, 0);
	dbg("gprs_at_SetAPN AT+CFUN return %d\n", ret);
    if(ret==0)
        ret=998;
#else	
    //重启
    if(ret==0)
    {
        dbg("********set apn, need reset module.***********\n");
        ret=999;        //重启
    }
#endif
#endif
	return ret;
}

int gprs_at_SetHead(void)
{
	/*
	int ret;
	ret = gprs_at_cmd_sndrcv("AT+QIHEAD=1\r", "OK\r", 500, 1, 1, NULL, 0);
	dbg("gprs_at_SetHead return %d\n", ret);
	return ret;
	*/
	return 0;
}
int gprs_at_SetRecvMode(void)
{
	/*
	int ret;
	ret = gprs_at_cmd_sndrcv("AT+QINDI=1\r", "OK\r", 500, 1, 1, NULL, 0);
	dbg("gprs_at_SetRecvMode return %d\n", ret);
	return ret;
	*/
	return 0;
}

int gprs_at_BuildGprsLink(void)
{
	/*
	int ret;
	ret = gprs_at_cmd_sndrcv("AT+CIICR\r", "OK\r", 1000, 1, 1, NULL, 0);
	dbg("gprs_at_BuildGprsLink return %d\n", ret);
	return ret;
	*/
	return 0;
}

int gprs_at_CloseGprs(void)
{
    if(sg_cModuleType==4)
        gprs_at_cmd_sndrcv("AT+MIPCALL=0\r", "+MIPCALL: ", 5*1000, 1, 1, NULL, 0);
    else
        gprs_at_cmd_sndrcv("AT+QIDEACT=1\r", "OK\r", 5*1000, 1, 1, NULL, 0);
	return 0;
}

int gprs_at_TcpConnectHostEx(char *hostIp, char *hostPort, int timeoutMs)
{
	char send[100];
	char recv[100];
	int ret;

    //部分模块联网超时时间较长。超时过短可能后续操作失败,一般应>=30秒,默认为60秒。
    if(timeoutMs<30*1000)
        timeoutMs=60*1000;
    
    if(iGetTcpTAMode())
    {
        return gprs_at_TcpConnectHostTA(hostIp, hostPort, timeoutMs);
    }
    
    if(sg_cModuleType==4)
    {
        sprintf(send, "AT+MIPOPEN=1,,\"%s\",%s,0\r", hostIp, hostPort);
        ret = gprs_at_cmd_sndrcv(send, "+MIP", timeoutMs, 1, 1, recv, sizeof(recv));
        if(ret==0 && strstr(recv, "+MIPOPEN: 1,1"))
            ret=0;
        else
        {
            ret=-1;
            if(strstr(recv, "+MIPSTAT: 1,"))
            {
                gprs_at_TcpDisconnectHost();
            }
        }
    }else
    {
        sprintf(send, "AT+QIOPEN=1,0,\"TCP\",\"%s\",%s,0,0\r", hostIp, hostPort);
        ret = gprs_at_cmd_sndrcv(send, "+QIOPEN: ", 10 * 1000, 1, 1, recv, sizeof(recv));
        if (ret == 0 && strstr(recv, "+QIOPEN: 0,0"))
        {
            ret = 0;
            gprs_at_cmd_sndrcv("AT+QISDE=0\r", "OK\r", 500, 1, 1, NULL, 0);	//设置不回显发送的数据
        }else
        {
            //ret=-2:若ip或端口有错,连接失败返回时间很长,需主动关闭tcp连接
            //err=563:连接id重复
            if(ret == -2 || strstr(recv, "+QIOPEN: 0,563"))
            {
                gprs_at_TcpDisconnectHost();
                ret = -1;
            }else
                ret = -1;
        }
    }
	dbg("gprs_at_TcpConnectHost return %d\n", ret);
    
    //gprs_at_connState(recv);    //for test at cmd
    
	return ret;
}

int gprs_at_TcpConnectHost(char *hostIp, char *hostPort)
{
    return gprs_at_TcpConnectHostEx(hostIp, hostPort, 60*1000);
}

int gprs_at_TcpSnd(unsigned char *sndData, int sndLen)
{
	char send[100];
	int ret;

	if (sndLen > GPRS_AT_TCP_SND_BUFFER_MAX_SIZE || NULL == sndData || sndLen <= 0)
	{
		return -3;
	}
    
    if(iGetTcpTAMode())
    {
        return gprs_at_TcpSndTA(sndData, sndLen);
    }
    
    if(sg_cModuleType==4)
        sprintf(send, "AT+MIPSENDEX=1,%d\r", sndLen);
    else
        sprintf(send, "AT+QISEND=0,%d\r", sndLen);
    
	ret = gprs_at_cmd_sndrcv(send, ">", 1500, 0, 1, NULL, 0);
	if (ret != 0)
	{
		dbg("gprs_at_cmd_sndrcv err, ret=%d", ret);
		return ret;
	}

	ret = gprs_write((char *)sndData, sndLen);
	if (ret <= 0)
	{
		dbg("gprs_write err, ret=%d", ret);
		return -1;
	}

	{
		char szRspTmp[100];
		char *pszOut;
		tick tmstart;
		int iTimeOut = 3 * 1000;

		ret = -2;
		tmstart = get_tick();
		pszOut = szRspTmp;
		while (get_tick() < tmstart + iTimeOut && (pszOut - szRspTmp < sizeof(szRspTmp) - 1))
		{
			ret = gprs_read(pszOut, 1, 100);
			if (ret <= 0)
				continue;
			dbg("ch:%c=%02X\n", pszOut[0], pszOut[0]);
			if (pszOut[0] != 0x0A) //判断\r\n的0A
			{
				++pszOut;
				continue;
			}
			pszOut[1] = 0x00;
			++pszOut;

			//接收到一行响应，开始判断
            if(sg_cModuleType==4)
            {
                if (strstr(szRspTmp, "+MIPSENDEX: 1,0"))
                {
                    ret = gprs_at_cmd_sndrcv("AT+MIPPUSH=1\r", "+MIPPUSH: 1", 1500, 1, 1, szRspTmp, sizeof(szRspTmp));
                    if(ret==0 && strstr(szRspTmp, "+MIPPUSH: 1,0"))
                        ret = sndLen;
                    else
                        ret=-3;
                    break;
                }
                if (strstr(szRspTmp, "+MIPSENDEX: 1,1") || strstr(szRspTmp, "ERROR"))
                {
                    ret = -1;
                    break;
                }
            }else
            {
                if (strstr(szRspTmp, "SEND OK"))
                {
                    ret = sndLen;
                    break;
                }
                if (strstr(szRspTmp, "SEND FAIL") || strstr(szRspTmp, "ERROR"))
                {
                    ret = -1;
                    break;
                }
            }
			ret = -2;
		}
	}

	dbg("gprs_at_TcpSnd return %d\n", ret);
	return ret;
}

int sg_iRcvQuick=0;
void vSetQuickRcv(int flag)
{
    sg_iRcvQuick=flag;
}
int iGetQueckRcv(void)
{
    return sg_iRcvQuick;
}

int gprs_at_TcpRcv(unsigned char *rcvData, int expRcvLen, int timeoutMs)
{
	char send[100];
	char recv[100];
	int ret, rcvLen = 0, len = 0;
	char *p0;
    tick tmstart;
    char szRespHead[20];

	if (timeoutMs <= 0)
		timeoutMs = 20 * 1000;
    
    if(iGetTcpTAMode())
    {
        return gprs_at_TcpRcvTA(rcvData, expRcvLen, timeoutMs);
    }

    if(sg_cModuleType==4)
        strcpy(szRespHead, "+MIPREAD: 1,");
    else
        strcpy(szRespHead, "+QIRD: ");
        
    if(sg_iRcvQuick==0)
    {   
        tmstart=get_tick();
        while (get_tick() < tmstart + timeoutMs)
        {
            if(sg_cModuleType==4)
                ret=gprs_at_cmd_sndrcv("AT+MIPREAD?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv));
            else
                ret=gprs_at_cmd_sndrcv("AT+QIRD=0,0\r", "OK\r", 1000, 1, 1, recv, sizeof(recv));
            if(ret==0 && (p0=strstr(recv, szRespHead))!=0)
            {
                p0+=strlen(szRespHead);
                if(sg_cModuleType==4)
                    sscanf(p0, "%d", &len);
                else
                    sscanf(p0, "%*d,%*d,%d", &len);
                if(len>=expRcvLen)
                    break;
            }else if(ret==-1)
            {
                return -1;
            }
            
            #ifdef COMM_CALLBACK
            if(gl_CommCallbackFunc)
            {
                //dbg("exec commcallbackfunc\n");
                gl_CommCallbackFunc();
            }
            #endif
            
            if(ret!=-2)
                mdelay(80);
            
            continue;
        }
        if(len==0)
        {
            dbg("gprs_at_TcpRcv timeout, recv 0 data. ret:-1\n");
            return -1;
        }
        dbg("len=%d\n", len);
    }
    
    if(sg_cModuleType==4)
    {
        sprintf(send, "AT+MIPREAD=1,%d\r", expRcvLen);
        strcpy(szRespHead,"+MIPDATA: 1,");
    }else
        sprintf(send, "AT+QIRD=0,%d\r", expRcvLen);
    
    if(sg_iRcvQuick!=2)
        timeoutMs=1000;

	ret = gprs_at_cmd_sndrcv(send, szRespHead, timeoutMs, 1, 1, recv, sizeof(recv));
	if (ret != 0)
	{
		//dbg("recv [%02X %02X]\n", recv[0], recv[1]);
		dbg("gprs_at_cmd_sndrcv error:%d\n", ret);
		return -1;
	}

	p0 = strstr(recv, szRespHead);
	if (p0 == NULL)
	{
		dbg("gprs recv err.\n");
		return -1;
	}
    p0+=strlen(szRespHead);
	rcvLen = atoi(p0);
    dbg("rcvLen=%d\n", rcvLen);
	if (rcvLen > expRcvLen)
	{
		dbg("revLen > expRcvLen, err.\n");
		return -1;
	}
    
	p0 = (char *)rcvData;
	len = rcvLen;
	//收取包内数据
	while (len > 0)
	{
		ret = gprs_read(p0, len, 2000);
		if (ret <= 0)
		{
			dbg("gprs_read error:ret=%d, len=%d\n", ret, len);
			return -1;
		}
		p0 += ret;
		len -= ret;
	}

	dbg("gprs_at_TcpRcv return %d\n", rcvLen);
	return rcvLen;
}

int gprs_at_TcpDisconnectHost()
{
	int ret;

    if(iGetTcpTAMode())
    {
        return gprs_at_TcpDisconnectHostTA();
    }
    
    if(sg_cModuleType==4)
    {
        ret = gprs_at_cmd_sndrcv("AT+MIPCLOSE=1,2\r", "+MIPCLOSE:", 1000, 1, 1, NULL, 0);
    }else
    {
        ret = gprs_at_cmd_sndrcv("AT+QICLOSE=0\r", "OK\r", 1000, 1, 1, NULL, 0);
    }
	dbg("gprs_at_TcpDisconnectHost return %d\n", ret);
	return ret;
}

int gprs_at_OfflineOnOff(int offlineOnOff)
{
	char send[100];
	char recv[100];
	char correctRsp[100];
	int ret;
	int loop = 0;

	if (offlineOnOff == GPRS_AT_OFFLINE_ON)
	{
		strcpy(send, "AT+CIFUN=0\r");
	}
	else
	{
		strcpy(send, "AT+CIFUN=1\r");
	}
	strcpy(correctRsp, "OK");

	while (loop++ < 1)
	{
		ret = gprs_write(send, strlen(send));
		dbg("gprs_write[%d] = [%s]\n", ret, send);
		ret = gprs_read(recv, 100, 600);
		recv[ret] = 0x00;
		dbg("gprs_read [%d] = [%s]\n", ret, recv);
		if (strstr(recv, correctRsp) != NULL)
		{
			dbg("gprs_at_OfflineOnOff return 0\n");
			return 0;
		}
	}
	dbg("gprs_at_OfflineOnOff return -1\n");
	return -1;
}

int gprs_at_SetErrFormat(void)
{
	int ret;
	int loop;
	
    ret=0;
	do{
        if(ret && ret!=2)
			mdelay(100);
		ret = gprs_at_cmd_sndrcv("AT+CMEE=1\r", "OK", 500, 1, 1, NULL, 0);
	}while(ret!=0 && ++loop<3);
	
	dbg("gprs_at_SetErrFormat return %d\n", ret);
	return ret;
}

int gprs_at_DisableEcho(void)
{
	int ret=0;
    int loop=0;
    char recv[100];
    
#if 0    
    do{
        ret = gprs_at_cmd_sndrcv("ATE1\r", "OK", 800, 1, 1, recv, sizeof(recv));
        if(ret==0 && strstr(recv, "ATE1")==NULL)
            ret=-1;
        if(ret && ret!=-2)
            mdelay(300);
    }while (ret != 0 && ++loop <= 5);
    
    ret = gprs_at_cmd_sndrcv("ATE0\r", "OK", 1000, 1, 1, recv, sizeof(recv));
 #else   
    do{
        if(ret && ret!=-2)
            mdelay(200);        
        ret = gprs_at_cmd_sndrcv("ATE0\r", "OK", 800, 1, 1, recv, sizeof(recv));
    }while (ret != 0 && ++loop <= 3);
  #endif  
    if(ret==0)
    {
        gprs_at_ModuleInfo(NULL);
        gprs_at_SetErrFormat();
    }
    
	dbg("gprs_at_DisableEcho return %d\n", ret);
	return ret;
}

int gprs_at_PowerOff()
{
	int ret;

	ret = gprs_at_cmd_sndrcv("AT+QPOWD=1\r", "NORMAL POWER DOWN", 500, 1, 1, NULL, 0);

	dbg("gprs_at_PowerOff return %d\n", ret);
	return ret;
}

int gprs_at_Config_Cur_Context()
{
	return 0;
}

int gprs_at_InitPDP(void)
{
	int ret = 0;
	//int loop;
    //char szTmp[100];

	//loop = 0;	
	ret=gprs_at_actgprs(1, 10);
	if (ret)
	{
		return ret;
	}
    
    /*
	ret = gprs_at_cmd_sndrcv("AT+QILOCIP\r", ".", 0, 1, 1, NULL, 0);
	if (ret)
	{
		dbg("AT+QILOCIP error:%d\n", ret);
		//return ret;
	}
    */
    
	return ret;
}

int gprs_at_RegApp(void)
{
	int ret = 0;
	int loop;
    char szTmp[100];

	loop = 0;
	do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+QIREGAPP\r", "OK", 1000, 1, 1, NULL, 0);
        if(ret==-1 && loop>=1)
        {
            //重复regapp会返回“+CME ERROR: 3”， ret=-1表明收到ERROR,需检查stat
            szTmp[0]=0;
            gprs_at_connState(szTmp);
            if(szTmp[0] && strstr(szTmp, "INITIAL")==0) //非INITIAL为已注册
            {
                ret=0;
                break;
            }
        }
	} while (ret < 0 && ++loop <= 10);
	if (ret)
	{
		dbg("AT+QIREGAPP error:%d\n", ret);
	}
    
	return ret;
}

int gprs_at_QueryActStatus(void)
{
	char szTmp[100];
	int ret;

	szTmp[0]=0;
    if(sg_cModuleType==4)
    {
        ret=gprs_at_cmd_sndrcv("AT+MIPCALL?\r", "OK\r", 1000, 1, 1, szTmp, sizeof(szTmp));
        if(ret==0 && strstr(szTmp, "+MIPCALL: 1"))
        {
            //已act,不需要QIACT指令
            return 1;
        }
        return 0;
    }
    
	ret = gprs_at_cmd_sndrcv("AT+QIACT?\r", "OK\r", 1000, 1, 1, szTmp, sizeof(szTmp));
	if(ret==0 && (strstr(szTmp, "+QIACT: 1,1") || strstr(szTmp, "+QIACT:1,1")))
	{
		//已act,不需要QIACT指令
		return 1;
	}
	return 0;
}

int gprs_at_actgprs(char preChkStat, int loopMax)
{
	int ret = 0;
	int loop;

	if(loopMax<=0)
		loopMax=10;

	if(preChkStat)
	{
		if(gprs_at_QueryActStatus()==1)
			return 0;
	}

	loop = 0;
	do
	{
        if(sg_cModuleType==4)
            ret = gprs_at_cmd_sndrcv("AT+MIPCALL=1\r", "OK\r", 10*1000, 1, 1, NULL, 0);
        else
            ret = gprs_at_cmd_sndrcv("AT+QIACT=1\r", "OK\r", 10*1000, 1, 1, NULL, 0);
		if(ret!=-2)
        {
            if(gprs_at_QueryActStatus()==1)
				return 0;
			ret=-1;
			mdelay(800);
        }
	} while (ret < 0 && ++loop <= loopMax);
	if (ret)
		dbg("AT+QIACT error:%d\n", ret);
	return ret;
}

int gprs_at_Tcp_RcvBuf_OnOff(int OnOff)
{
	char send[100];
	char recv[120];
	char correctRsp[100];
	int ret;
	int loop = 0;

	sprintf(send, "AT+QINDI=%d\r", OnOff);
	strcpy(correctRsp, "OK");

	while (loop++ < 10)
	{
		ret = gprs_write(send, strlen(send));
		dbg("gprs_write[%d] = [%s]\n", ret, send);
		ret = gprs_read(recv, 100, 5 * 1000);
		recv[ret] = 0x00;
		dbg("gprs_read [%d] = [%s]\n", ret, recv);
		if (strstr(recv, correctRsp) != NULL)
		{
			dbg("gprs_at_Config_Cur_Context return 0\n");
			return 0;
		}
	}

	dbg("gprs_at_Config_Cur_Context return -1\n");
	return -1;
}

int gprs_at_location(char *szLocation)
{
	return -1;		//当前模块不支持GNSS
#if 0	
	char recv[120];
	char correctRsp[30];
	char *pTmp, *pTmp2;
	int ret;

	//gprs_at_cmd_sndrcv("AT+QGPS?\r", "OK", 500, 1, 1, NULL, 0);

	ret = gprs_at_cmd_sndrcv("AT+QGPS=1\r", "OK", 800, 1, 1, recv, sizeof(recv));	//打开定位
	if (ret)
	{
		dbg("AT+QGPS error:%d\n", ret);
		return ret;
	}

	ret = gprs_at_cmd_sndrcv("AT+QGPSLOC?\r", "OK", 10*1000, 1, 1, recv, sizeof(recv));	//获取定位信息
	gprs_at_cmd_sndrcv("AT+QGPSEND\r", "OK", 800, 1, 1, NULL, sizeof(recv));			//关闭定位
	if (ret)
	{
		dbg("AT+QGPSLOC error:%d\n", ret);
		return ret;
	}
	dbg("loc:[%s]\n", recv);
	return 0;
	/*
	strcpy(correctRsp, "+QCELLLOC: ");
	if ((pTmp = strstr(recv, correctRsp)) != NULL)
	{
		pTmp += strlen(correctRsp);
		pTmp2 = strstr(pTmp, "\r");
		if (pTmp2 != NULL)
		{
			*pTmp2 = 0;
			if (szLocation != NULL)
			{
				strcpy(szLocation, pTmp);
			}
			dbg("gprs_at_location return 0, Location=[%s]\n", pTmp);
			return 0;
		}
	}
	*/
	//dbg("gprs_at_location return -1\n");
	//return -1;
#endif	
}

int rtcStrToStructTm(char *szDateTime, struct tm *tmDateTime)
{
	int i;
    char *p;
    
	i = 0;
    p=strchr(szDateTime, '/');
    if(p && (p-szDateTime==4))      //九联的年是4位,做兼容处理
        i+=2;
    tmDateTime->tm_year = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0') + 2000 - 1900; //1900 年开始计数
	
    i += 3;
	tmDateTime->tm_mon = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0') - 1; //0~11表示1~12个月
	i += 3;
	tmDateTime->tm_mday = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0');
	i += 3;
	tmDateTime->tm_hour = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0');
	i += 3;
	tmDateTime->tm_min = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0');
	i += 3;
	tmDateTime->tm_sec = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0');

	dbg("tmDateTime:%d, %d, %d, %d, %d, %d\n",
		tmDateTime->tm_year, tmDateTime->tm_mon, tmDateTime->tm_mday,
		tmDateTime->tm_hour, tmDateTime->tm_min, tmDateTime->tm_sec);
	return 0;
}

static int cclk_first=1;
int gprs_at_get_cclk2(struct tm *tmDateTime)
{
	char recv[100];
	char *pRtcStartStr;
	int ret;
	int loop;
	char *p;
    
#if 1
    //if(cclk_first && sg_cModuleType!=4 && sg_cModuleType!=6)
    if(cclk_first && sg_cModuleType==1)
    {
        /*
        loop = 0;
        do
        {
            ret = gprs_at_cmd_sndrcv("AT+QNITZ=1\r", "OK", 3 * 1000, 1, 1, NULL, 0);
        } while (ret != 0 && ++loop <= 5);
        if (ret)
        {
            dbg("AT+QNITZ=1 error:%d\n", ret);
            //return ret;
        }
        */

        loop = 0;
        ret = 0;
        do
        {
            if(ret && ret!=-2)
				mdelay(300);
            ret = gprs_at_cmd_sndrcv("AT+CTZU=3\r", "OK", 500, 1, 1, NULL, 0);
        } while (ret != 0 && ++loop <= 3);
        if (ret)
        {
            dbg("AT+CTZU=3 error:%d\n", ret);
            return ret;
        }else
            cclk_first=0;
    }
#endif
	loop = 0;
    ret=0;
	do
	{
        if(ret && ret!=-2)
			mdelay(300);
		ret = gprs_at_cmd_sndrcv("AT+CCLK?\r", "OK", 500, 1, 1, recv, sizeof(recv));
	} while (ret != 0 && ++loop <= 3);
	if (ret)
	{
		dbg("AT+CCLK? error:%d\n", ret);
		return ret;
	}
	if ((p=strstr(recv, "+CCLK: ")) != NULL)
	{
		pRtcStartStr = strchr(p, '"');
        if(pRtcStartStr==NULL)
            return -1;
        p=strchr(pRtcStartStr, 0x0D);
        if(p)
            *p=0;
		dbg("pRtcStartStr = [%s]\n", pRtcStartStr);        
		rtcStrToStructTm(pRtcStartStr + 1, tmDateTime);
		if(tmDateTime->tm_year<120)
            return -1;
        if(sg_cModuleType==4 || sg_cModuleType==6)   //信位模块需要计算时区
        {
            struct tm *ptm;
            time_t tma;
            
            tma=mktime(tmDateTime);
            p=strchr(pRtcStartStr, '+');
            if(p==NULL)
                p=strchr(pRtcStartStr, '-');
            if(p)
            {
                if(*p=='+')
                {
                    dbg("++\n");
                    tma=tma+atoi(p+1)/4*60*60;
                }else if(*p=='-')
                {
                    dbg("--\n");
                    tma=tma-atoi(p+1)/4*60*60;
                }
                ptm = localtime(&tma);
                memcpy(tmDateTime, ptm, sizeof(struct tm));
            }
            
            dbg("tmDateTime2:%d, %d, %d, %d, %d, %d\n",
                tmDateTime->tm_year, tmDateTime->tm_mon, tmDateTime->tm_mday,
                tmDateTime->tm_hour, tmDateTime->tm_min, tmDateTime->tm_sec);
        }
		return 0;
	}

	dbg("gprs_at_cclk return -1\n");
	return -1;
}

int gprs_at_get_ntptime(struct tm *tmDateTime)
{
	int ret;
	char recv[100];
	char *p, *p1;
	//char date[20];
	struct tm stm, *ptm;
	time_t tm;

    if(sg_cModuleType<1 || sg_cModuleType>6)
    {
        gprs_at_ModuleInfo(NULL);
    }
    
    //ret = gprs_at_cmd_sndrcv("AT+QNTP=1,\"58.220.133.132\"\r", "+QNTP:", 2000, 1, 1, NULL, 0);	//中国 NTP 快速授时服务 cn.ntp.org.cn
    //ret = gprs_at_cmd_sndrcv("AT+QNTP=1,\"120.25.115.20\"\r", "+QNTP:", 2000, 1, 1, NULL, 0);   //阿里云公共NTP1 ntp1.aliyun.com
    //ret = gprs_at_cmd_sndrcv("AT+QNTP=1,\"203.107.6.88\"\r", "+QNTP:", 2000, 1, 1, NULL, 0);    //阿里云公共NTP2 ntp2.aliyun.com
    //ret = gprs_at_cmd_sndrcv("AT+QNTP=1,\"202.120.2.101\"\r", "+QNTP:", 2000, 1, 1, NULL, 0);     //上海交大NTP

    memset(recv, 0, sizeof(recv));
	//ret = gprs_at_cmd_sndrcv("AT+QNTP=1,\"120.25.115.20\"\r", "+QNTP:", 3000, 1, 1, recv, sizeof(recv));	//阿里云公共NTP1 ntp1.aliyun.com
	//if(ret)
	//	ret = gprs_at_cmd_sndrcv("AT+QNTP=1,\"ntp2.aliyun.com\"\r", "+QNTP:", 3000, 1, 1, recv, sizeof(recv));	//阿里云公共NTP2
    ret = gprs_at_cmd_sndrcv("AT+QNTP=1,\"cn.ntp.org.cn\"\r", "+QNTP:", 3000, 1, 1, recv, sizeof(recv));        //中国 NTP 快速授时服务
	if(ret==0)
	{
		//+QNTP: 0,"2021/01/25,08:49:37+32"
		p=strstr(recv, "+QNTP:");
		if(p==NULL)
			return -1;
        p+=strlen("+QNTP:");
		if(p[0]==' ')
			p++;
        if(sg_cModuleType!=4)
        {
            if(atoi(p)!=0)
                return -1;
            p1=strchr(p, '"');
            if(p1==NULL)
            {
                p1=strstr(p, "0,");     //爱联模块QNTP的时间没有双引号,因此搜索0,
                if(p1==NULL)
                    return -1;
                p1++;
            }
            p=p1+1;
        }
        dbg("time=[%s]\n", p);
		stm.tm_year=atoi(p)-1900;  
		stm.tm_mon=atoi(p+5)-1;  
		stm.tm_mday=atoi(p+8);  
		stm.tm_hour=atoi(p+11);  
		stm.tm_min=atoi(p+14);  
		stm.tm_sec=atoi(p+17);
        
        if(sg_cModuleType==1 || sg_cModuleType==6)   //移远模块需要计算时区
        {
            tm=mktime(&stm);
            if(p[19]=='+' || p[19]=='-')
            {
                if(p[19]=='+')
                {
                    dbg("++\n");
                    tm=tm+atoi(p+20)/4*60*60;
                }else if(p[19]=='-')
                {
                    dbg("--\n");
                    tm=tm-atoi(p+20)/4*60*60;
                }
            }else
            {
                dbg("++--\n");
                tm=tm+8*60*60;
            }

            ptm = localtime(&tm);
            memcpy(tmDateTime, ptm, sizeof(struct tm));
        }else
        {
            memcpy(tmDateTime, &stm, sizeof(struct tm));
        }
        dbg("time:%d-%d-%d %02d:%02d:%02d\n",
            tmDateTime->tm_year, tmDateTime->tm_mon, tmDateTime->tm_mday, tmDateTime->tm_hour, tmDateTime->tm_min, tmDateTime->tm_sec);
        
        if(tmDateTime->tm_year<120)
            return -1;
		return 0;
	}
    return -1;
}
static int sg_timemode=0;	//1-启动后默认cclk校时 0-启动后默认ntp校时
int gprs_at_get_cclk(struct tm *tmDateTime)
{
    int ret;
    
#if defined(TEST_4G_MODULE) //&& defined(JTAG_DEBUG)
    //for debug
	ret=gprs_at_get_ntptime(tmDateTime);
    ret=gprs_at_get_cclk2(tmDateTime);
#else    
	//sg_timemode=0;	//固定为先ntp
    if(sg_timemode)
        ret=gprs_at_get_cclk2(tmDateTime);
    else
        ret=gprs_at_get_ntptime(tmDateTime);
    if(ret)
    {
        sg_timemode=(sg_timemode+1)%2;
        if(sg_timemode)
            ret=gprs_at_get_cclk2(tmDateTime);
        else
            ret=gprs_at_get_ntptime(tmDateTime);
    }
#endif
    if( tmDateTime->tm_year>=138 )    //信位模块偶现2038年问题,暂不知原因
        ret=-1;
    return ret;
}

/*
int gprs_at_GetLocationFlow(char *szLocation)
{	
	int ret;
	//char rcvData[1024];
	struct tm rtcTm;
	int loop;

	gprs_poweron();
  	
	if (gprs_at_MoudleIsActive() == 0)
	{
		dbg("Need gprs_modulepin_reset()\n");
		gprs_modulepin_reset();
		if (gprs_at_MoudleIsActive() == 0)
		{
			dbg("Need gprs_modulepin_reset2()\n");
			gprs_modulepin_reset();
			if (gprs_at_MoudleIsActive() == 0)
			{
				dbg("gprs_modulepin_reset() can't work\n");
				gprs_lcdDebug("AT", "No response");
				return -9;
			}
		}
		mdelay(5*1000);
	}else
	{
		dbg("No Need gprs_hardware_reset()\n");
	}

	ret = gprs_at_DisableEcho();
	if (ret < 0)
	{
		dbg("gprs_at_DisableEcho error\n");
		return -1;
	}
	
	ret = gprs_at_CheckSIM();
	if (ret < 0)
	{
		dbg("gprs_at_CheckSIM error\n");
		return -1;
	}
	
	ret = gprs_at_GetSignalQuality();
	if (ret < 0)
	{
		dbg("gprs_at_GetSignalQuality error\n");
		return -2;
	}

	loop=0;
	do{
		ret = gprs_at_CheckRegister();
		if(ret)
			mdelay(200);
	}while(ret!=0 && ++loop<=10);
	if (ret < 0)
	{
		dbg("gprs_at_CheckRegister error\n");
		return -3;
	}

	loop=0;
	do{
		ret = gprs_at_CheckGprsAttachment();
		if(ret)
			mdelay(200);
	}while(ret!=0 && ++loop<=10);
	if (ret < 0)
	{
		dbg("gprs_at_CheckGprsAttachment error\n");
		return -4;
	}

	ret = gprs_at_SetAPN(NULL, NULL, NULL);
	if (ret < 0)
	{
		dbg("gprs_at_SetAPN error\n");
		return -5;
	}

	ret = gprs_at_InitPDP();
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
		return -6;
	}

	ret = gprs_at_get_cclk(&rtcTm);
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
		return -6;
	}

	ret = gprs_at_location(szLocation);
	if (ret < 0)
	{
		dbg("gprs_at_location error\n");
		return -6;
	}
	
	ret = gprs_at_CloseGprs();
	if (ret < 0)
	{
		dbg("gprs_at_CloseGprs error\n");
		return -8;
	}

	return 0;
}
*/

int gprs_at_test(void)
{
	int ret;
	unsigned char rcvData[1050 + 1];
	//	struct tm rtcTm;
	int loop, len;

	dbg("gprs_at_test\n");

	if(gprs_poweron() < 0)
	{
        return -1;
    }

    if (gprs_at_MoudleIsActive(30) == 0)
	{
        dbg("Need gprs_modulepin_reset()\n");
        gprs_poweroff();
        mdelay(200);
        if(gprs_poweron() < 0)
    	{
            return -1;
        }
        if (gprs_at_MoudleIsActive(30) == 0)
        {
            dbg("gprs_modulepin_reset() can't work\n");
            //gprs_lcdDebug("AT", "No response");
            return -99;
        }
        //mdelay(5 * 1000);
        //mdelay(1 * 1000);
    }

	ret = gprs_at_DisableEcho();
	if (ret < 0)
		return -1;

	ret = gprs_at_CheckSIM();
	if (ret < 0)
    {
		if(ret==-100)       //无SIM卡
            return -100;
        return -1;
    }
    
	ret = gprs_at_GetSignalQuality();
	if (ret < 0)
		return -2;

	ret = gprs_at_CheckRegister();
	if (ret < 0)
		return -3;

	ret = gprs_at_CheckGprsAttachment();
	if (ret < 0)
		return -4;

	/*
	ret = gprs_at_SetAPN(NULL, NULL, NULL);
	if (ret < 0)
	{
		dbg("gprs_at_SetAPN error\n");
		return -5;
	}
*/
	ret = gprs_at_InitPDP();
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
		return -6;
	}
	/*
	ret = gprs_at_get_cclk(&rtcTm);
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
		return -6;
	}


	ret = gprs_at_location(NULL);
	if (ret < 0)
	{
		dbg("gprs_at_location error\n");
		return -6;
	}
*/
	gprs_at_SetRecvMode();
	gprs_at_SetHead();

	ret = gprs_at_TcpConnectHost("47.106.13.245", "6666");
	if (ret < 0)
	{
		dbg("gprs_at_TcpConnectHost error\n");
		return -7;
	}

	//连续通讯3次
	for (loop = 0; loop < 3; loop++)
	{
        //格式:STX+cmd[1]+len[2]+data[n]+ETX+CRC[1]
        //cmd A0为测试tcp收发。请求的data前两字节为要求返回报文的数据长度n，响应的data第一个字节为rspcode[1],后面填充n个0xAB
		memcpy(rcvData, "\x02\xA0\x00\x02\x01\x00\x03\xA2", 8);     //\x01\x00要求返回256字节的数据
		ret = gprs_at_TcpSnd(rcvData, 8);
		if (ret < 0)
		{
			dbg("gprs_at_TcpSnd error:%d\n", ret);
			break;
		}

		ret = gprs_at_TcpRcv(rcvData, 4, 5 * 1000);
		if (ret < 0)
		{
			dbg("gprs_at_TcpRcv error\n");
			break;
		}
		len = ((unsigned char)rcvData[2]) * 256 + (unsigned char)rcvData[3];
		if (len + 4 + 2 > sizeof(rcvData))
		{
			dbg("recv too long.\n");
			break;
		}
		ret = gprs_at_TcpRcv(rcvData + 4, len + 2, 500);
		if (ret < 0)
		{
			dbg("gprs_at_TcpRcv2 error\n");
			break;
		}
		mdelay(200);
	}

	if(ret>0)
	{
		hexdump((unsigned char *)rcvData, 4 + len + 2);
	}

	gprs_at_TcpDisconnectHost();

	ret = gprs_at_CloseGprs();
	if (ret < 0)
	{
		dbg("gprs_at_CloseGprs error\n");
		return -8;
	}

	return 0;
}

int gprs_at_domain(char *pszHostName, char *pszHostIp)
{
	char send[100];
	char resp[300];
	int ret;
	char *p, *p1;
	//int flag;
	
    pszHostIp[0]=0;
    
    
	//gprs解析域名,先有OK，后一行才出ip,不方便指定特定字符做结束符
    if(sg_cModuleType==4)
    {
        sprintf(send, "AT+MIPDNS=\"%s\"\r", pszHostName);
        //+MIPDNS: "baidu.com",220.181.38.148
        ret=gprs_at_cmd_sndrcv(send, "OK\r", 10*1000, 1, 1, resp, sizeof(resp));
        if(ret)
            return ret;
        p=strstr(resp, "\",");
        if(p==NULL)
            return -3;
        p+=2;
        p1=strchr(p, 0x0d);
        if(p1)
            *p1=0;
        if(strlen(p)>15)
            return -4;
        strcpy(pszHostIp, p);
        
        dbg("domain=[%s], ip=[%s]\n", pszHostName, pszHostIp);
        return 0;
    }
    
	sprintf(send, "AT+QIDNSGIP=1,\"%.80s\"\r", pszHostName);
#if 1
	//按行读取到行内有'.'时认为读取成功
	//ret=gprs_at_cmd_sndrcv(send, ".", 10*1000, 1, 1, resp, sizeof(resp));
    ret=gprs_at_cmd_sndrcv(send, "+QIURC: \"dnsgip\",\"", 10*1000, 1, 1, resp, sizeof(resp));
	if(ret)
		return ret;

    p=strstr(resp, "+QIURC: \"dnsgip\",\"");
    if(p)
    {
        p+=strlen("+QIURC: \"dnsgip\",\"");
        p1=strchr(p, '"');
        if(p1 && p1-p<=15)
        {
            memcpy(pszHostIp, p, p1-p);
            pszHostIp[p1-p]=0;
            
            dbg("domain=[%s], ip=[%s]\n", pszHostName, pszHostIp);
            return 0;
        }
    }
    return 1;
/*
	flag=0;
	p=resp+strlen(resp)-1;
	while(p>=resp)
	{
		if(*p=='.' || (*p>='0' && *p<='9'))
		{
			flag=1;
		}else
		{
			if(flag)
				break;
			else
				*p=0;
		}
		p--;
		continue;
	}
	if(*p<'0' || *p>'9')
		p++;
	pszHostIp[0]=0;
	ret=strlen(p);
	if(flag && ret>=7 && ret<=15)
	{
		strcpy(pszHostIp, p);	
		return 0;
	}else
		return 1;
    */
#else
	//按行读取到“OK”时认为执行成功，然后再按行读取判断ip
	ret=gprs_at_cmd_sndrcv(send, "OK", 0, 1, 1, resp, sizeof(resp));
	if(ret)
		return ret;

	len=0;
	while(len<sizeof(resp)-1)
	{
		if(gprs_read(resp+len, 1, 3000)<=0)
		{
			dbg("gprs_at_domain err.\n");
			return -1;
		}
		if(resp[len]=='\r' || resp[len]=='\n')
		{
			resp[len]=0;
			if(strchr(resp, '.'))
			{
				strcpy(pszHostIp, resp);
				return 0;
			}else
			{
				len=0;
				continue;
			}
		}
		len++;
		continue;
	}
#endif
}

int gprs_at_iccid(char *pszICCID)
{
	char resp[100];
	int ret;
	char *p, *p1;
	int loop;
	
    pszICCID[0]=0;
    loop = 0;
    ret=0;
	do
	{
        if(ret && ret!=-2)
			mdelay(300);
		ret=gprs_at_cmd_sndrcv("AT+QCCID\r", "OK\r", 500, 1, 1, resp, sizeof(resp));
	} while (ret != 0 && ++loop <= 2);
    
	if(ret)
    {
        dbg("AT+QCCID err:%d\n", ret);
		return ret;
    }

	p=strstr(resp, "+QCCID: ");
	if(p)
	{
		p+=strlen("+QCCID: ");
		p1=strchr(p, '\r');
		if(p1)
			*p1=0;
		strncpy(pszICCID, p, 20);
        pszICCID[20]=0;
		return 0;
	}
    return -1;
}

int gprs_at_imei(char *pszIMEI)
{
	char resp[100];
	int ret;
	char *p, *p1;

    pszIMEI[0]=0;
	ret=gprs_at_cmd_sndrcv("AT+GSN\r", "OK\r", 500, 1, 1, resp, sizeof(resp));
	//if(ret)
    //    ret=gprs_at_cmd_sndrcv("AT+QGSN\r", "OK\r", 500, 1, 1, resp, sizeof(resp));
    if(ret)
    {
        dbg("AT+GSN err:%d\n", ret);
		return ret;
    }
    
	p=strstr(resp, "GSN: ");      //移远“+GSN: IMEI”
	if(p)
	{
		p+=strlen("GSN: ");
    }else
    {
        p=strstr(resp, "86");       //骐俊直接返回IMEI
    }
    if(p==NULL)
        return -1;
    p1=strchr(p, '\r');
    if(p1)
        *p1=0;
    if(p[strlen(p)-1]=='"')     //爱联iemi有双引号,去除末尾的双引号
        p[strlen(p)-1]=0;
    strncpy(pszIMEI, p, 17);
    pszIMEI[17]=0;
    return 0;
}

int gprs_at_connState(char *pszState)
{
	char resp[200];
	int ret;
	char *p, *p1;
	int iState=-1;
    
/*
    ret=gprs_at_CheckSIMQuick();
    if(ret)
    {
        return ret;
    }
*/
    
    pszState[0]=0;
    memset(resp, 0, sizeof(resp));
    if(sg_cModuleType!=4)
    {
        if(sg_cModuleType==6)
            ret=gprs_at_cmd_sndrcv("AT+QISTATE\r", "OK\r", 1*1000, 1, 1, resp, sizeof(resp));
        else
            ret=gprs_at_cmd_sndrcv("AT+QISTATE=1,0\r", "OK\r", 1*1000, 1, 1, resp, sizeof(resp));
        if(ret)
        {
            dbg("AT+QISTATE err:%d\n", ret);
            return ret;
        }
    }
	p=strstr(resp, "+QISTATE: ");
	if(p)
	{
		p+=strlen("+QISTATE: ");
		p1=strchr(p, '\r');
		if(p1)
			*p1=0;
		//+QISTATE: 0,"TCP","39.156.66.18",80,6305,4,1,0,0,"uart1"
        sscanf(p, "%*d,%*[^,],%*[^,],%*d,%*d,%d", &iState);
        dbg("s=[%s],iState=%d\n", p, iState);
		if(iState<0)
			return 2;
		/*
		0 "Initial": connection has not been established  
		1 "Opening": client is connecting or server is trying to listen  
		2 "Connected": client/incoming connection has been established  
		3 "Listening": server is listening  
		4 "Closing": connection is closing 
		*/
		//sprintf(pszState, "%d", iState);
		//为兼容gprs,转为gprs指令的对应字符串
		switch(iState)
		{
			case 0:
				strcpy(pszState, "IP INITIAL");
				break;
			case 1:
				strcpy(pszState, "TCP CONNECTING");
				break;
			case 2:
				strcpy(pszState, "CONNECT OK");
				break;
			case 3:
				strcpy(pszState, "Listening");
				break;
			case 4:
				gprs_at_TcpDisconnectHost();		//返回4需要主动关闭tcp连接
				strcpy(pszState, "IP CLOSE");
				break;
		}
		return 0;
	}else
	{
		//检查act和reg
		ret=gprs_at_actgprs(1, 1);
		if(ret==0)
		{
			strcpy(pszState, "GPRSACT");
			return 0;
		}
	}
    return 1;
}

int gprs_at_gmr(char *pszGprsVer, int iOutSize)
{
	//两条指令返回相同
	//gprs_at_cmd_sndrcv("AT+CGMR\r", "OK", 0, 1, 1, pszGprsVer, iOutSize);
	return gprs_at_cmd_sndrcv("AT+GMR\r", "OK", 300, 1, 1, pszGprsVer, iOutSize);
}

int gprs_at_MobileId(char* pszMobileId)
{
	char recv[200];
	int ret;
	int loop = 0;
	char *p, *p1;

	if(pszMobileId)
		pszMobileId[0]=0;

	//do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+CNUM\r", "+CNUM:", 500, 1, 1, recv, sizeof(recv));	
		if (ret == 0 && strstr(recv, "+CNUM:"))
		{
			ret=-1;
			p=strstr(recv, "+CNUM:");
			//_vDisp(2, p);
			p+=strlen("+CNUM:");
			p1=strchr(p, ',');
			if(p1)
			{
				p=p1+1;
				p1=strchr(p, ',');
				if(p1)
					*p1=0;
				else
				{
					p[20]=0;
				}
				if(p[0]=='"')
					p++;
				if(p[strlen(p)-1]=='"')
					p[strlen(p)-1]=0;
				//_vDisp(3, p);
				if(pszMobileId)
					strcpy(pszMobileId, p);
				ret = 0;
			}
		}else
			ret = -1;
	} //while (ret < 0 && ++loop <= 15);
	dbg("gprs_at_MobileId return %d\n", ret);
	return ret;
}

int gprs_at_IMSI(char* pszIMSI)
{
	char recv[100];
	int ret;
	int loop = 0;
	char *p, *p1;

	if(pszIMSI)
		pszIMSI[0]=0;

	//do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+CIMI\r", "OK\r", 500, 1, 1, recv, sizeof(recv));	
		if (ret == 0)
		{
			ret=-1;
			
            p=strstr(recv, "CIMI");
			if(p)
				p+=strlen("CIMI");
			else
				p=recv;
			
			while(*p)
			{
				if(*p>='0' && *p<='9')
					break;
				p++;
			}
            if(*p==0)
                ret=-1;
            else
            {
                p1=strchr(p, '\r');
                if(p1)
                    *p1=0;
                //_vDisp(3, p);
                if(pszIMSI)
                    strcpy(pszIMSI, p);
                ret = 0;
            }
		}else
			ret = -1;
	} //while (ret < 0 && ++loop <= 15);
	dbg("gprs_at_IMSI return %d\n", ret);
	return ret;
}


int gprs_at_QFENG(char* msg)
{
#if 1
	char recv[500]={0};
    int ret;

    if(sg_cModuleType==1 || sg_cModuleType==2 || sg_cModuleType==5 || sg_cModuleType==6)
    {
        ret=gprs_at_cmd_sndrcv("AT+QENG=\"servingcell\"\r", "OK\r", 3000, 1, 1, recv, sizeof(recv));
        dbg("recv=[%s]\n", recv);
        if(ret==0 && recv[0] && msg)
        {
            strncpy(msg, recv, 199);
            recv[199]=0;
        }            
    }else
        dbg("AT+QENG cmd not support!\n");
#endif    
	return 0;
}

//mode: 1-sleep  0-wakeup
int gprs_at_sleep(char mode)
{
    char send[50];
    int  ret;
    
    if(sg_cModuleType == 3)
    {
        //ali module does not need at command.
        return 0;
    }
    
#if 0//def TEST_4G_SLEEP    
    if(mode==0)
    {
        sprintf(send, "mode=%d, type=%d", mode, sg_cModuleType);
        _vDisp(5, send);
        _vDelay(150);
    }
#endif
    
    if(sg_cModuleType==4)
    {
        if(mode)
            strcpy(send, "ATS24=1\r");
        else
            strcpy(send, "ATS24=0\r");
        //ret = gprs_write(send, strlen(send));
        //dbg("gprs_write[%d] = [%s]\n", ret, send);
        //接收响应
        ret=gprs_at_cmd_sndrcv(send, "OK\r", 500, 1, 1, NULL, 0);
        ret=0;
    }else
    {
        if(mode)
        {
            mode=(sg_cModuleType==2 || sg_cModuleType==5)?2:1;
        }
        
        sprintf(send, "AT+QSCLK=%d\r", mode);
        
        ret=gprs_at_cmd_sndrcv(send, "OK\r", 300, 1, 1, NULL, 0);
    }
    return ret;
}

#ifdef USE_HIGH_BAUDRATE
/*修改gprs初始化配置.
    通讯模块上电和复位后默认115200,只有在以下步骤之后才会更改为高波特率.
    * at指令发送AT+IPR=波特率,以修改模块的波特率.
    * 调用gprs_setbaud(波特率) 更改与通讯模块串口的波特率(gprs_setbaud需要修改main函数来支持).
*/
int gprs_at_setbaud2(uint32_t baud)
{
    char buf[128],*p;
    int ret;
    int i;
    uint32_t readbaud = 0;
    
    ret = gprs_at_cmd_sndrcv("ATE0\r", "OK", 800, 1, 1, NULL, 0);
    
    //1.send at command
    sprintf(buf,"AT+IPR=%d\r",baud);
    ret = gprs_at_cmd_sndrcv(buf, "OK", 800, 1, 1, NULL, 0);
	 //1.1 changes mcu clk
    iMcusetclk(baud);
    
    if(ret)
    {
        dbg("AT+IPR=%u err:%d\n", baud, ret);
        if(ret!=-2)
            return -1;
        #ifndef XinWei_MODULE
        return -1;
        #endif
    }

    //2.change gprs baud
    ret=gprs_setbaud(baud);
    if(ret)
    {
        dbg("gprs_setbaud(%u) err:%d\n", baud, ret);
        return -2;
    }
    mdelay(100);
    
    //3.read back new baud from at command
    for(i=0;i<5;i++)
    {
        // AT+IPR?
        memset(buf,0,sizeof(buf));
        ret = gprs_at_cmd_sndrcv("AT+IPR?\r", "OK", 800, 1, 1, buf, sizeof(buf));
        if(ret == 0)
        {
            break;
        }
        mdelay(100);
    }
    if(ret)
    {
        dbg("readbaud fail:%d\n", ret);
        return -4;
    }
    p=strstr(buf, "+IPR:");
    if(p==NULL)
    {
        dbg("readbaud err.\n");
        return -5;
    }
    readbaud=99;
    ret = sscanf(p,"+IPR: %d",&readbaud);
    dbg("ret=%d.setbaud=%d,readbaud=%d,[%s]\n", ret, baud, readbaud, p);
    
    #if 0   //能正常读取数据则认为设置成功
    if(readbaud != baud && readbaud!=0)
    {
        return -6;
    }
    #endif
    
    return 0;
}

int gprs_at_setbaud(uint32_t baud)
{
    int ret;
    
    ret=gprs_at_setbaud2(baud);
    if(ret && baud>=HIGH_BAUDRATE)
    {
        dbg("gprs_at_setbaud HIGH_BAUDRATE fail=%d, set LOW_BAUDRATE\n", ret);
        //高波设置失败,恢复低波(主频)
        gprs_at_setbaud2(LOW_BAUDRATE);     //iMcusetclk(LOW_BAUDRATE);
    }
    return ret;
}

#endif

//TA模式(TransparentAccess Mode)
int gprs_at_TcpConnectHostTA(char *hostIp, char *hostPort, int timems)
{
	char send[100];
	char recv[100];
	int ret;

    if(sg_cModuleType==4)
    {
        sprintf(send, "AT+MIPOPEN=1,,\"%s\",%s,0\r", hostIp, hostPort);
        ret = gprs_at_cmd_sndrcv(send, "+MIP", timems, 1, 1, recv, sizeof(recv));
        if(ret==0 && strstr(recv, "+MIPOPEN: 1,1"))
            ret=0;
        else
        {
            ret=-1;
            if(strstr(recv, "+MIPSTAT: 1,"))
            {
                gprs_at_TcpDisconnectHost();
            }
        }
    }else
    {
        //gprs_at_cmd_sndrcv("AT+QISDE=0\r", "OK\r", 500, 1, 1, NULL, 0);	//设置不回显发送的数据
        
        sprintf(send, "AT+QIOPEN=1,0,\"TCP\",\"%s\",%s,0,2\r", hostIp, hostPort);
        ret = gprs_at_cmd_sndrcv(send, "CONNECT\r\n", 10 * 1000, 1, 1, recv, sizeof(recv));
        if (ret)
        {
            //ret=-2:若ip或端口有错,连接失败返回时间很长,需主动关闭tcp连接
            //err=563:连接id重复
            if(ret == -2)
            {
                gprs_at_TcpDisconnectHost();
                ret = -1;
            }else
                ret = -1;
        }
    }
	dbg("gprs_at_TcpConnectHost return %d\n", ret);
	return ret;
}

int gprs_at_TcpSndTA(unsigned char *sndData, int sndLen)
{
	//char send[100];
	int ret;

	if (sndLen > GPRS_AT_TCP_SND_BUFFER_MAX_SIZE || NULL == sndData || sndLen <= 0)
	{
		return -3;
	}
    
	ret = gprs_write((char *)sndData, sndLen);
	if (ret <= 0)
	{
		dbg("gprs_write err, ret=%d", ret);
		return -1;
	}

	dbg("gprs_at_TcpSnd return %d\n", ret);
	return ret;
}

int gprs_at_TcpRcvTA(unsigned char *rcvData, int expRcvLen, int timeoutMs)
{
	//char send[100];
	//char recv[100];
	int ret, rcvLen = 0, len = 0;
	char *p0;
    tick tmstart;
    //char szRespHead[20];

	if (timeoutMs <= 0)
		timeoutMs = 20 * 1000;

	p0 = (char *)rcvData;
	len = expRcvLen;
	//收取包内数据
	tmstart=get_tick();
    while (get_tick() < tmstart + timeoutMs && len>0)
	{
		ret = gprs_read(p0, len, 2000);
		if (ret > 0)
		{
			p0 += ret;
            len -= ret;
            continue;
		}
        
        if(len!=expRcvLen)
            break;
        
        #ifdef COMM_CALLBACK
        if(gl_CommCallbackFunc)
        {
            //dbg("exec commcallbackfunc\n");
            gl_CommCallbackFunc();
        }
        #endif
		//mdelay(10);
	}
    
    rcvLen=p0-(char *)rcvData;
	//dbg("gprs_at_TcpRcv return %d\n", rcvLen);
	return rcvLen;
}

int gprs_at_TcpDisconnectHostTA(void)
{
	int ret;

    mdelay(1200);
    ret = gprs_at_cmd_sndrcv("+++", "OK", 1500, 1, 0, NULL, 0);		//+++前后至少要间隔1秒
    if(ret && ret!=-2)
        mdelay(1200);
    
    if(sg_cModuleType==4)
    {
        ret = gprs_at_cmd_sndrcv("AT+MIPCLOSE=1,2\r", "+MIPCLOSE:", 1000, 1, 1, NULL, 0);
    }else
    {
        ret = gprs_at_cmd_sndrcv("AT+QICLOSE=0\r", "OK\r", 1000, 1, 1, NULL, 0);
    }
	dbg("gprs_at_TcpDisconnectHost return %d\n", ret);
    
    //关闭tcp后自动关闭TA模式
    vSetTcpTAMode(0);
	return ret;
}


void vSetTcpTAMode(int mode)
{
    dbg("vSetTcpTAMode:%d\n", mode);
    sg_iTcpTAMode=mode;
}
int iGetTcpTAMode(void)
{
    return sg_iTcpTAMode;
}


//4G模块取本地IP
int gprs_at_localip(char *ip)
{
    char recv[200];
    int ret;
    char *p0, *p1;

    ip[0]=0;

    if(sg_cModuleType==4)
    {
        //信位模块ip无引号，格式 +MIPCALL: 1,10.136.252.148
        ret = gprs_at_cmd_sndrcv("AT+MIPCALL?\r", "+MIPCALL:", 1000, 1, 1, recv, sizeof(recv));
        if(ret)
            return -1;
        p1=strstr(recv, "+MIPCALL: 1,");
        if(p1==NULL)
            return -1;
        p0=p1+strlen("+MIPCALL: 1,");
        p1=strchr(p0, '\r');
        if(p1)
            *p1=0;
        if(strlen(p0)>15)
            return -1;
        strcpy(ip, p0);
    }else
    {
        //格式 +QIACT: 1,1,1,"10.217.107.60"
        ret = gprs_at_cmd_sndrcv("AT+QIACT?\r", "+QIACT:", 1000, 1, 1, recv, sizeof(recv));
        if(ret)
            return -1;
        p1=strstr(recv, "+QIACT:");
        if(p1==NULL)
            return -1;
        p0=strchr(p1, '"');
        if(p0==NULL)
            return -1;
        p0++;
        p1=strchr(p0, '"');
        //有骐俊模块IP4和IP6一起返回并同在双引号中 +QIACT: 1,1,3,"10.6.8.130  2409:8D80:4002:1EA0::1"
        #if 0
        if(p1==NULL || p1-p0>15)    
            return -1;
        #else
        if(p1==NULL)
            return -1;
        if(p1-p0>15)
        {
            for(int i=0; i<p1-p0; i++)
            {
                if((p0[i]<'0' || p0[i]>'9') && p0[i]!='.')
                {
                    p1=&p0[i];
                    break;
                }
            }
            if(p1-p0>15)
                return -1;
        }
        #endif
        *p1=0;
        strcpy(ip, p0);
    }
    return 0;
}

int iGetCellInfoVal(char *pszInfo, char *pszKey, char *pszVal, int size, char **pNext)
{
    char *p0, *p1;
    int len;
    
    pszVal[0]=0;
    if(pNext)
        *pNext=NULL;
    p0=strstr(pszInfo, pszKey);
    if(p0)
    {
        p0+=strlen(pszKey);
        p1=strchr(p0, ',');
        if(p1)
        {
            len=p1-p0;
        }else
            len=strlen(p0);
        if(size && len>size)
            len=size;
        memcpy(pszVal, p0, len);
        pszVal[len]=0;
        
        if(pNext && p1)
            *pNext=p1+1;
        return 0;
    }
    return 1;
}

extern unsigned long ulA2L(const unsigned char *psString, int iLength);
extern void vLongToHex0(unsigned long ulLongNumber, int iLength, unsigned char *psHexString);
int gprs_at_NeighbourCell(char *cell1, char *cell2, char *cell3)
{
    int ret;
    //char send[100];
    char recv[1024];
    char *p0;
    char *p, *pTmp, *pCell;
    int i, k;
    char tmp[30];
    char lac[10];
    
    if(cell1==NULL)
        return -1;
    cell1[0]=0;
    if(cell2)
        cell2[0]=0;
    if(cell3)
        cell3[0]=0;
    if(sg_cModuleType==1)
    {
        ret = gprs_at_cmd_sndrcv("AT+QCELL?\r", "OK", 6000, 1, 1, recv, sizeof(recv));
        if(ret)
            return ret;

        //+QCELL: "servingcell","LTE",460,00,2638,cbb7281,15,63
        //+QCELL: "neighbourcell inter","LTE",460,00,2638,d228941,152,37
        p0=recv;
        //pTmp=strstr(p0, "+QCELL: ");
        pTmp=strstr(p0, "neighbourcell");
        if(pTmp==NULL)
            return 1;
        
        for(i=0; i<6; i++)
        {
            p=strchr(pTmp, ',');
            if(p==NULL)
                return 2;
            pTmp=p+1;
            if(i==1)
                p0=p+1;
            if(i==5)
            {
                *p=0;                
                break;
            }
        }
        strcpy(cell1, p0);
        strcat(cell1, ",0");       //移远模块无信号值,置0
        
        if(cell2)
        {
            p0=p+1;
            pTmp=strstr(p0, "+QCELL: ");
            if(pTmp==NULL)
                return 0;
            for(i=0; i<6; i++)
            {
                p=strchr(pTmp, ',');
                if(p==NULL)
                    return 0;
                pTmp=p+1;
                if(i==1)
                    p0=p+1;
                if(i==5)
                {
                    *p=0;
                    
                    break;
                }
            }
            strcpy(cell2, p0);
            strcat(cell2, ",0");       //移远模块无信号值,置0
        }
        
        if(cell3)
        {
            p0=p+1;
            pTmp=strstr(p0, "+QCELL: ");
            if(pTmp==NULL)
                return 0;
            for(i=0; i<6; i++)
            {
                p=strchr(pTmp, ',');
                if(p==NULL)
                    return 0;
                pTmp=p+1;
                if(i==1)
                    p0=p+1;
                if(i==5)
                {
                    *p=0;
                    
                    break;
                }
            }
            strcpy(cell3, p0);
            strcat(cell3, ",0");       //移远模块无信号值,置0
        }
        
        return 0;
    }
    if(sg_cModuleType==2)
    {
        ret = gprs_at_cmd_sndrcv("AT+QENG=\"neighbourcell\"\r", "OK", 6000, 1, 1, recv, sizeof(recv));
        if(ret)
            return ret;

        //+QENG: "neighbourcell","LTE",460,00,D602350,7A,3590,2638,34,14,0,0,0,0,0,0
        //+QENG: "neighbourcell","LTE",460,00,9D3242,EE,3590,2638,30,7,0,0,0,0,0,0
        p0=recv;
        for(k=0; k<3; k++)
        {
            if(k==0)
                pCell=cell1;
            if(k==1)
                pCell=cell2;
            if(k==2)
                pCell=cell3;
            if(pCell==NULL)
                break;
            
            pTmp=strstr(p0, "+QENG: ");
            if(pTmp==NULL)
            {
                if(k)
                    return 0;
                else
                    return 2;
            }
            for(i=0; i<9; i++)
            {
                p=strchr(pTmp, ',');
                if(p==NULL)
                {
                    if(k)
                        return 0;
                    else
                        return 2;
                }
                pTmp=p+1;
                if(i==1)
                    p0=p+1;
                if(i==3)
                {
                    *p=0;
                    strcpy(tmp, p0);
                    p0=p+1;
                }
                if(i==4)
                {
                    *p=0;
                    strncpy(lac, p0, 8);
                    p0=p+1;
                }
                if(i==6)
                    p0=p+1;
                if(i==7)
                {
                    *p=0;
                    sprintf(tmp+strlen(tmp), ",%s,%s,", p0, lac);
                    p0=p+1;
                }
                if(i==8)
                {
                    *p=0;
                    strcat(tmp, p0);
                    break;
                }
            }
            strcpy(pCell, tmp);
            p0=p+1;
        }
        return 0;
    }
    if(sg_cModuleType==4)
    {
        ret = gprs_at_cmd_sndrcv("AT+MCELL=0,26\r", "OK", 6000, 1, 1, recv, sizeof(recv));
        if(ret)
            return ret;
        //MCC:460,MNC:00,LAC:9784,Cell ID:213611137,BSIC:0,(P)BCCH ARFCN:38950,RxLev:68,RxDbm:-72
        //MCC:460,MNC:00,LAC:9784,Cell ID:220367169,BSIC:0,(P)BCCH ARFCN:40936,RxLev:32,RxDbm:-108
        p0=recv;
        for(k=0; k<3; k++)
        {
            if(k==0)
                pCell=cell1;
            if(k==1)
                pCell=cell2;
            if(k==2)
                pCell=cell3;
            if(pCell==NULL)
                break;
                
            ret=iGetCellInfoVal(p0, "MCC:", pCell+strlen(pCell), 3, NULL);
            if(ret)
                break;
            strcat(pCell, ",");
            ret=iGetCellInfoVal(p0, "MNC:", pCell+strlen(pCell), 2, NULL);
            strcat(pCell, ",");
            iGetCellInfoVal(p0, "LAC:", tmp, 5, NULL);
            {
                //数字串转16进制可见字符串
                vLongToHex0(ulA2L((unsigned char*)tmp, strlen(tmp)), 4, (unsigned char*)tmp);
                strcat(pCell, tmp);
                strcat(pCell, ",");
            }
            iGetCellInfoVal(p0, "Cell ID:", tmp, 11, NULL);
            {
                //数字串转16进制可见字符串
                vLongToHex0(ulA2L((unsigned char*)tmp, strlen(tmp)), 8, (unsigned char*)tmp);
                strcat(pCell, tmp);
                strcat(pCell, ",");
            }
            iGetCellInfoVal(p0, "RxLev:", pCell+strlen(pCell), 3, &pTmp);
            
            if(pTmp==NULL)
                break;
            p0=strchr(pTmp, '\r');
            if(p0==NULL)
				p0=strchr(pTmp, '\n');
			if(p0==NULL)
                break;
            p0++;
            continue;
        }
        if(cell1[0])
            return 0;
        else
            return 1;
    }
    if(sg_cModuleType==5 || sg_cModuleType==6)
    {
        ret = gprs_at_cmd_sndrcv("AT+QCELL?\r", "OK", 6000, 1, 1, recv, sizeof(recv));
        if(ret)
            return ret;

        //+QCELL: "servingcell","LTE",460,00,2638,CBB7281,15,3207
        //+QCELL: "neighbourcell inter","LTE",460,00,2638,D228941,152,1700
        //+QCELL: "neighbourcell inter","LTE",460,00,2638,D60234C,366,1762
        p0=recv;
        pTmp=strstr(p0, "neighbourcell");
        if(pTmp==NULL)
            return 1;
        
        p0=pTmp;
        for(k=0; k<3; k++)
        {
            if(k==0)
                pCell=cell1;
            if(k==1)
                pCell=cell2;
            if(k==2)
                pCell=cell3;
            if(pCell==NULL)
                break;
            
            pTmp=strstr(p0, "\"LTE\",");
            if(pTmp==NULL)
                break;
            pTmp+=strlen("\"LTE\",");
			p0=pTmp;
            for(i=0; i<5; i++)
            {
                p=strchr(pTmp, ',');
                if(p==NULL)
                    return 2;
                pTmp=p+1;
                if(i==3)
                {
                    if(p-pCell>3+1+2+1+4+1+8)
                        return 2;
                    *p=0;
					strcpy(pCell, p0);
                }
                if(i==4)
                {
                    pTmp=++p;       //指向信号
                    while(*p>='0' && *p<='9')
                        ++p;
                    *p=0;
                    if(sg_cModuleType==5)
                        sprintf(pCell+strlen(pCell), ",%d", atoi(pTmp)/64);
                    else
                        sprintf(pCell+strlen(pCell), ",%d", atoi(pTmp));
                    break;
                }
            }
            p0=p+1;
        }
        if(cell1[0])
            return 0;
        else
            return 1;
    }
    return 3;
}

#ifdef USE_HIGH_BAUDRATE
int gprs_at_ResetBaud(void)
{
    int ret;
    
    dbg("gprs_poweroff...\n");
    gprs_poweroff();
    
    dbg("mdelay 2000...\n");
    mdelay(2000);
        
    dbg("gprs_poweron...\n");
    gprs_poweron();
    
    dbg("mdelay 10*1000...\n");
    mdelay(10*1000);
    
    ret=gprs_at_MoudleIsActive(10);
    gprs_at_cmd_sndrcv("ATI\r", "OK\r", 1500, 1, 1, NULL, 0);
    if(ret==1)
        return 0;
    
    dbg("high baud test...");
    dbg("gprs_poweroff 2...\n");
    gprs_poweroff();
    
    dbg("mdelay2 2000...\n");
    mdelay(2000);
        
    dbg("gprs_poweron 2...\n");
    gprs_poweron();
    
    dbg("mdelay2 10*1000...\n");
    mdelay(10*1000);
    
    dbg("set baud 921600.\n");
    gprs_setbaud(921600);
    
    ret=gprs_at_MoudleIsActive(10);
    gprs_at_cmd_sndrcv("ATI\r", "OK\r", 1500, 1, 1, NULL, 0);
    if(ret==1)
    {
        //恢复默认波特率115200
        ret=gprs_at_setbaud(115200);
        if(ret==0)
            gprs_at_cmd_sndrcv("AT&W\r", "OK\r", 1500, 1, 1, NULL, 0);
        gprs_at_cmd_sndrcv("ATI\r", "OK\r", 1500, 1, 1, NULL, 0);
        return 1;
    }
    return -1;
}
#endif


#ifdef USE_MQTT
//当前MQTT只支持骐俊160DM模块
static char sg_szMqttBak[800];      //因变量长度限制从串口读取到的最后一条msg可能不完整,保存下来供下次读取时使用

//==================================================================
//功能描述：MQTT设置参数
//输入参数：svr			服务器地址或域名
//          port		服务器端口
//			clientId	客户ID
//			user		用户名, 可为NULL
//			pwd			密码, 可为NULL
//			topic		订阅的主题(自动订阅模式),为NULL则不进行自动订阅,需在conn后sub
//			qos			0-2
//返 回 值：0-成功 !0-失败
//==================================================================
int mqtt_param(char *svr, char *port, char *clientId, char *user, char *pwd, char *topic, int qos)
{
    char send[100];
    int  ret;
    
    if(svr==NULL || port==NULL || clientId==NULL)
        return -1;

    do{
        sprintf(send, "AT+CZMQTTSERVPORT=0,\"%s\",%s\r", svr, port);
        ret=gprs_at_cmd_sndrcv(send, "OK\r", 800, 1, 1, NULL, 0);
        if(ret)
            break;
        
        //client ID
        sprintf(send, "AT+CZMQTTID=0,\"%s\"\r", clientId);
        ret=gprs_at_cmd_sndrcv(send, "OK\r", 800, 1, 1, NULL, 0);
        if(ret)
            break;
        
        //用户名密码
        if(user && user[0])
        {
            sprintf(send, "AT+CZMQTTUNPW=0,\"%s\",\"%s\"\r", user, (pwd==NULL)?"":pwd);
            ret=gprs_at_cmd_sndrcv(send, "OK\r", 800, 1, 1, NULL, 0);
            if(ret)
                break;
        }
        
        //关clean session(阿里云mqtt，当qos=1且clean为0时才有离线消息)
        ret=gprs_at_cmd_sndrcv("AT+CZMQTTPARA=0,\"CLEAN\",0\r", "OK\r", 800, 1, 1, NULL, 0);
        if(ret)
            break;
        
        //自动订阅
        if(topic && topic[0])
        {
            if(qos<0 || qos>2)
                qos=1;
            sprintf(send, "AT+CZMQTTAUTOSUB=0,0,\"%s\",%d\r", topic, qos);
            ret=gprs_at_cmd_sndrcv(send, "OK\r", 800, 1, 1, NULL, 0);
            if(ret)
                break;
        }

        //查询参数值,for debug
        //gprs_at_cmd_sndrcv("AT+CZMQTTPARA=0,\"CLEAN\"\r", "OK\r", 800, 1, 1, NULL, 0);
        //gprs_at_cmd_sndrcv("AT+CZMQTTPARA=0,\"RECONNECT\"\r", "OK\r", 800, 1, 1, NULL, 0);
        
        /*
        if(save)
        {
            //保存参数(掉电不丢失)
            ret=gprs_at_cmd_sndrcv("AT+CZMQTTSCONT=0\r", "OK\r", 800, 1, 1, NULL, 0);
            if(ret)         //参数保存失败不影响使用
                ret=0;
        }
        */
        
        break;
    }while(1);
        
    return ret;
}

//==================================================================
//功能描述：MQTT建立连接
//输入参数：automode 1-自动订阅模式(conn前已设置了订阅主题参数) 0-非自动订阅模式(conn后需设置订阅主题)
//返 回 值：0-成功 !0-失败
//==================================================================
int mqtt_conn(int automode)
{
    char recv[100];
    int ret;
        
    sg_szMqttBak[0]=0;
    ret=gprs_at_cmd_sndrcv("AT+CZMQTTCONN=0\r", "CONNECT", 5000, 1, 1, recv, sizeof(recv));
    if(ret==0 && (strstr(recv, "CONNECT OK") || strstr(recv, "ALREADY CONNECT")))
    {
        if(automode)
        {
            //设置了自动订阅,mqtt连接后会返回+CZMQTTAUTOSUB OK: 0,0
            gprs_at_cmd_sndrcv(NULL, "+CZMQTTAUTOSUB OK:", 800, 1, 0, NULL, 0);
        }
        return 0;
    }
    return -1;
}

//==================================================================
//功能描述：MQTT断开连接
//返 回 值：0-成功 !0-失败
//==================================================================
int mqtt_disconn(void)
{    
    //return gprs_at_cmd_sndrcv("AT+CZMQTTDISCONN=0\r", "OK\r", 1500, 1, 0, NULL, 0);
    char snd[100];
    strcpy(snd, "AT+CZMQTTDISCONN=0\r");
    gprs_write(snd, strlen(snd));
    mdelay(500);
    return 0;
}

//==================================================================
//功能描述：MQTT读取消息
//输入参数：clientID	mqtt连接时的客户id,此处可为NULL
//          msgsize  	输出参数msg的size
//输出参数: msg         消息内容,格式为消息1长度[3]+消息1内容+消息2长度[3]+消息2内容+...
//返 回 值：>0:读取到的消息条数 0:无消息 <0:mqtt已断开
//==================================================================
int mqtt_readMsg(char *clientID, char *msg, int msgsize)
{
    char recv[10*1024];
    char *p, *p1;
    char *pBak;
    int num=0;
    int len;
    
    msg[0]=0;
    //模块返回的订阅消息数据格式如下，其中topic为带clientid完整路径的topic, msglen后有0D0A, msg和topic有加双引号, "msg"后有0D0A:
    /*
    [
    +CZMQTTRCVPUB: idx,"topic",msglen
    "msg"
    ]
    */
#if 1
    p=recv;
    p1=recv+sizeof(recv)-1;
    
    if(sg_szMqttBak[0])
    {
        strcpy(recv, sg_szMqttBak);
        sg_szMqttBak[0]=0;
        
        p=recv+strlen(recv);
    }
    while(p<p1 && gprs_read(p, 1, 150) > 0)
    {
        p++;
    }
    if(p<recv+10)
    {
        /*
        extern void dbgHex(unsigned char *head, unsigned char *in, unsigned int len);
        
        if(p>recv)
            dbgHex("recv data err", recv, p-recv);
        */
        return 0;
    }
    *p=0;
#else
    strcpy(recv, 
    "\x0D\x0A\x2B\x43\x5A\x4D\x51\x54\x54\x52\x43\x56\x50\x55\x42\x3A"
    "\x20\x30\x2C\x22\x7A\x77\x6D\x5F\x78\x69\x6E\x6C\x69\x61\x6E\x66"
    "\x75\x2F\x70\x32\x70\x2F\x47\x49\x44\x5F\x5A\x57\x4D\x50\x41\x59"
    "\x40\x40\x40\x30\x30\x30\x30\x37\x33\x30\x32\x31\x39\x39\x39\x39"
    "\x39\x30\x30\x30\x30\x36\x30\x2F\x22\x2C\x31\x39\x37\x0D\x0A\x22"
    "\x7B\x22\x70\x61\x79\x61\x62\x6C\x65\x41\x6D\x74\x22\x3A\x22\x35"
    "\x2E\x32\x22\x2C\x22\x6F\x72\x64\x65\x72\x4E\x6F\x22\x3A\x22\x32"
    "\x30\x32\x32\x30\x32\x32\x34\x31\x34\x30\x31\x34\x31\x32\x39\x34"
    "\x37\x32\x30\x35\x37\x22\x2C\x22\x74\x72\x61\x6E\x73\x44\x61\x74"
    "\x65\x22\x3A\x22\x32\x30\x32\x32\x2D\x30\x32\x2D\x32\x34\x20\x31"
    "\x34\x3A\x30\x31\x3A\x34\x31\x22\x2C\x22\x6D\x65\x72\x4E\x61\x6D"
    "\x65\x22\x3A\x22\xE5\x8D\x97\xE9\xA3\x8E\xE5\xB0\x8F\xE5\x90\x83"
    "\xE5\x9F\x8E\xEF\xBC\x88\xE4\xB8\xB4\xE7\x8C\x97\xE5\xBA\x97\xEF"
    "\xBC\x89\x22\x2C\x22\x6D\x69\x64\x22\x3A\x22\x38\x36\x34\x30\x30"
    "\x31\x31\x35\x38\x31\x32\x32\x35\x30\x33\x22\x2C\x22\x70\x61\x79"
    "\x4E\x61\x6D\x65\x22\x3A\x22\xE6\x94\xAF\xE4\xBB\x98\xE5\xAE\x9D"
    "\x22\x2C\x22\x74\x69\x64\x22\x3A\x22\x38\x38\x31\x38\x30\x39\x34"
    "\x31\x32\x34\x22\x7D\x22\x0D\x0A");
#endif
    //dbgHex("recv", recv, strlen(recv));
    
    num=0;
    p=recv;
    while(msgsize>strlen(msg)+10)   //假设至少需10字节剩余空间存放下一条消息
    {
        p1=strstr(p, "+CZMQTTRCVPUB:");
        if(p1==NULL && num==0)
        {
            if(strstr(p, "+CZMQTTDISCONN:"))
            {
                num=-1;
            }
            break;
        }
        
        p=p1;
        pBak=p;
        if(clientID && clientID[0])
        {
            p1=strstr(p, clientID);
            if(p1)
                p=p1+strlen(clientID);
        }
        p1=strstr(p, "\",");            //搜索clientID后面的",
        if(p1==NULL)
            break;
        p=p1+strlen("\",");
        
        if(p[0]<'0' || p[0]>'9')
            break;
        len=atoi(p);
        
        if(strlen(msg)+3+len>=msgsize)      //msg size不足
        {
            if(strlen(pBak)<sizeof(sg_szMqttBak))
                strcpy(sg_szMqttBak, pBak);
            break;
        }
        
        p1=strstr(p, "\x0D\x0A\x22");
        if(p1==NULL)
            break;
        
        if(p1+3+len>recv+sizeof(recv)-1)    //接收不完整
        {
            if(strlen(pBak)<sizeof(sg_szMqttBak))
                strcpy(sg_szMqttBak, pBak);
            break;
        }
        
        //按长度收取(也可按\x0D\x0A\x22和\x22\x0D\x0A来收取中间数据)
        sprintf(msg+strlen(msg), "%03d", len);
        p=msg+strlen(msg);
        memcpy(p, p1+3, len);
        p[len]=0;
        
        #if 0
        dbgHex("msg", msg, strlen(msg));
        {
            char tmp[2048]={0};
            extern int Utf8ToGB(unsigned char *pIn, unsigned int len, unsigned char *pOut);
            Utf8ToGB(msg, strlen(msg), tmp);
            dbg("msg=[%s]\n", tmp);
        }
        #endif
        
        num++;
        p=p1+3+len;
    }
    
    dbg("readMsg ret:%d\n", num);
    return num;
}

//==================================================================
//功能描述：MQTT订阅主题,非自动订阅模式时使用,在mqtt_conn后调用
//			topic		订阅的主题
//			qos			0-2
//返 回 值：0-成功 !0-失败
//==================================================================
int mqtt_sub(char *topic, int qos)
{
    char send[300];
    char recv[100];
    int ret;

    if(topic==NULL || topic[0]==0)
        return -1;
    
    if(qos<0 || qos>2)
        qos=1;
    sprintf(send, "AT+CZMQTTSUB=0,\"%s\",%d\r", topic, qos);
    //ret=gprs_at_cmd_sndrcv(send, "OK\r", 800, 1, 1, recv, sizeof(recv));
    ret=gprs_at_cmd_sndrcv(send, "+CZMQTTSUB:", 800, 1, 1, recv, sizeof(recv));
    if(ret==0)
    {
        if(strstr(recv, "+CZMQTTSUB: 0,0") || strstr(recv, "+CZMQTTSUB:0,0"))
            ret=0;
        else
            ret=1;
    }
    return ret;
}

//==================================================================
//功能描述：mqtt获取连接状态
//返 回 值：1:已连接 0:未连接 -1:查询失败
//==================================================================
int mqtt_state(void)
{
    char recv[100];
    int ret;

    ret=gprs_at_cmd_sndrcv("AT+CZMQTTSTATE=0\r", "+CZMQTTSTATE:", 800, 1, 1, recv, sizeof(recv));
    if(ret==0)
    {
        //收取指令响应信息末尾的OK\r
        gprs_at_cmd_sndrcv(NULL, "OK\r", 200, 1, 0, NULL, 0);

        if(strstr(recv, "+CZMQTTSTATE: 0,1")!=NULL || strstr(recv, "+CZMQTTSTATE:0,1")!=NULL)
            return 1;
        else
            return 0;
    }
    return -1;
}

#endif      //#ifdef USE_MQTT



int iMcusetclk(uint32_t baud)
{  
#ifdef CHANGE_MCU_FREQ
	extern void gprs_reset_clk(SYSCTRL_PLL_TypeDef clk);
	extern uint32_t HALGetPLLCLK(void);

	if(baud == 115200 && HALGetPLLCLK() != SYSCTRL_PLL_168MHz)
	{
        gprs_reset_clk(SYSCTRL_PLL_168MHz);
	}
    if(baud == 921600 && HALGetPLLCLK() != SYSCTRL_PLL_180MHz)
	{
        gprs_reset_clk(SYSCTRL_PLL_180MHz);
    }
    if(baud == 460800 && HALGetPLLCLK() != SYSCTRL_PLL_204MHz)
    {
        gprs_reset_clk(SYSCTRL_PLL_204MHz);
    }
    SysTick_Configuration();		//变频后重新计算tick时间
    dbg("baud=[%u], new clk=[0x%02X]\n", baud, HALGetPLLCLK());
#endif
    return 0;
}

#endif      //#ifdef USE_LTE_4G

/*
中国移动测试要求(RSRP为信号强度/接收功率(取值0~97,实际值=取值-140),SINR为信号质量/信噪比(取值0~31))
极好点： RSRP>-85dBm； SINR>25
好点： RSRP=-85～-95dBm；SINR:16-25
中点： RSRP=-95～-105dBm；SINR:11-15
差点： RSRP=-105～-115dBm；SINR:3-10
极差点： RSRP<-115dB；SINR<3
*/
