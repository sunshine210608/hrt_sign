#ifndef __WIFI_AT_H__
#define __WIFI_AT_H__

//文档为2048,此处定义比文档略小
#define WIFI_TCP_SND_BUFFER_MAX_SIZE	2000
#define WIFI_TCP_RCV_BUFFER_MAX_SIZE	2000

#define WIFI_TCP_PACKSIZE               2048

#define GPRS_WIFI_TCP_SND_BUFFER_MAX_SIZE	1460
#define GPRS_WIFI_TCP_RCV_BUFFER_MAX_SIZE	1460

#define GPRS_AT_OFFLINE_ON	0
#define GPRS_AT_OFFLINE_OFF	1

typedef struct {
	char ssid[30+1];	//wifi名称
	char ecn;			//加密模式0-4，0:OPEN 1:WEP 2:WPA_PSK 3:WPA2_PSK 4:WPA_WPA2_PSK 
	short rssi;			//信号强度
} AP_INFO;

int wifi_appLayer_Init(void);

int wifi_at_ConnectAp(void);
int wifi_at_ConnectAp2(char *ssid, char *pwd);
int wifi_at_DisconnAp(void);
int wifi_at_QueryAPConnStatus(char *ssid, int *rssi);
int wifi_at_get_localIP(char *pszIp, char *pszMac);

int wifi_at_SetStationMode(void);
int wifi_at_DisableEcho(void);

int wifi_at_tcp_connect(char *szIpHost, int port);
//type: 0-tcp 1-udp
int wifi_at_tcpudp_connect(int type, char *szIpHost, int port);
int wifi_at_DisconnTcp(void);

int wifi_at_tcp_send(unsigned char *sndData, int sndLen);
int wifi_at_tcp_recv(unsigned char *rcvData, int expRcvLen, int timeoutMs);
int wifi_at_tcp_recvPack(unsigned char *rcvData, int expRcvLen, int timeoutMs);

int wifi_at_ListAP(AP_INFO *pstAPInfos, int size);

int wifi_at_MoudleIsActive(void);

void wifi_statusClear(void);

int wifi_at_domain(char *pszHostName, char *pszHostIp);

int wifi_at_gmr(char *pszWifiVer, int iOutSize);

int wifi_at_connStatus(void);

int  wifi_at_poweroff(void);

#endif
