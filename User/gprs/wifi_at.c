#include <string.h>
#include <stdio.h>
#include <mhscpu.h>
#include <time.h>
#include "serial.h"
#include "wifi.h"
#include "user_projectconfig.h"
#include "debug.h"
//#include "gprs_at.h"
#include "SysTick.h"
#include "wifi_at.h"
#include "common.h"


#if !defined(DISABLE_WIFI) && !defined(NO_WIFI)

#define ATBM_WIFI_MODEL         //ATBM模块

#ifndef ATBM_WIFI_MODEL
#define WIFI_RECV_MODE_PASV		//被动接收模式,终端发起接收数据指令后，wifi模块才发送数据
#endif

#define WIFI_AT_RSP_ERR		"ERROR"
#define WIFI_AT_RSP_ERR2    "+ERR\r"
#define WIFI_AT_RSP_BUSYS	"busy s..."
#define WIFI_AT_RSP_BUSYP	"busy p..."


#define WIFI_TCP_SND_BUFF_MAXSIZE	WIFI_TCP_SND_BUFFER_MAX_SIZE
//wifi模块限制单次最多只能读取2048字节数据
#define WIFI_TCP_RCV_BUFF_MAXSIZE	WIFI_TCP_RCV_BUFFER_MAX_SIZE

#ifndef WIFI_RECV_MODE_PASV
//主动模式下才使用以下三个变量
static int sg_iWifiRcvLen, sg_iWifiRcvOff;	
static uint8_t sg_sWifiRcvBuff[WIFI_TCP_RCV_BUFF_MAXSIZE+200];
#endif

extern void (*gl_CommCallbackFunc)(void);

static int sg_iWifiRecvMode=0;

//清wifi 串口接收缓存
void wifi_clear(void)
{
	uint8_t ch;
	int flag = 1; // -1:不打印clear日志；1:打印日志(有clear数据的时候才打印起始终止行)

	while (wifi_read((char *)&ch, 1, 80) > 0)
	{
		if (flag >= 0)
		{
			if (flag == 1)
			{
				flag = 0;
				dbg("wifi at clear...\n");
			}
			dbg("clr ch:%c=%02X\n", ch, ch);
		}
	}
	if (flag == 0)
		dbg("wifi at clear end.\n");

}

//发送wifi at指令和接收响应（tcp数据接收不适用)
//	pszReq	发送的指令
//  pszCheckRsp		指令执行成功判断字
//  iTimeOut		超时时间(ms),若为0则默认30*1000ms
//  icrflag			是否按行读取判断,少部分指令需设为0
//  iclearflag		发送指令前是否清空串口
//	pszOutRsp		输出数据缓冲区,为NULL则不输出数据
//  iOutSize		输出数据缓冲区大小,为0则不输出数据
// ret:  0:成功  -1:error   -2:timeout
int wifi_at_cmd_sndrcv(char *pszReq, char *pszCheckRsp, int iTimeOut, int icrflag, int iclearflag, char *pszOutRsp, int iOutSize)
{
	int ret, ret2;
	char *pszOut;
	int iMaxLen=(iOutSize>0)?iOutSize:100;
	char szRspTmp[iMaxLen+1];
	int iReTry=1;
	tick tmstart;
	//char *pTmp;
	
	if(iclearflag)
		wifi_clear();
	
	if(iTimeOut<=0)
		iTimeOut=15*1000;
	
	ret2=-2;
	szRspTmp[0]=0;
	while(iReTry)
	{
		mdelay(1);
		iReTry=0;
		if(pszReq)
        {
			ret = wifi_write((uint8_t*)pszReq, strlen(pszReq));
			dbg("wifi_write[%d] = %.100s\n", ret, pszReq);
		}
		pszOut=szRspTmp;
		tmstart=get_tick();
		while(get_tick()<tmstart+iTimeOut)
		{		
            if(gl_CommCallbackFunc)
                gl_CommCallbackFunc();

			ret = wifi_read((uint8_t*)pszOut, 1, 100);
			if(ret<=0)
			{
				/*
				//读取数据指令收到"+IPD"时,退出重发指令
				if(memcmp(pszReq, "AT+CIPRECVDATA=", 15)==0 && strstr(szRspTmp, "+IPD,"))
				{
					dbg("rcv:[%.100s], retry to recv...\n", szRspTmp); 
					iReTry=1;
					//wifi_clear();
					break;
				}else */
					continue;
			}
			//dbg("rcv ch:%c=%02X\n", pszOut[0], pszOut[0]); 
			if(icrflag && pszOut[0]!=0x0A)			//判断\r\n的0A
			{
				++pszOut;
				continue;
			}
			pszOut[1] = 0x00;
			++pszOut;
			
			//接收到一行响应，开始判断			
			if(strstr((char*)szRspTmp, (char*)pszCheckRsp))
			{
				ret2=0;
				break;
			}
			
			if(memcmp(pszReq, "AT+CIPRECVDATA=", 15)==0 && strstr(szRspTmp, "+IPD,"))
			{
				dbg("rcv:[%s], retry to recv...\n", szRspTmp);
				wifi_clear();
				memset(szRspTmp, 0, sizeof(szRspTmp));
				iReTry = 1;
				break;
			}

			/*			
			if(strstr((char*)szRspTmp, WIFI_AT_RSP_BUSYP) || strstr((char*)szRspTmp, WIFI_AT_RSP_BUSYS))
			{
				dbg("wifi busy... retry\n");
				szRspTmp[0]=0;
				pszOut=szRspTmp;
				mdelay(500);
				wifi_clear();
				iReTry=1;
				break;
			}
			*/
			if( strstr(szRspTmp, WIFI_AT_RSP_ERR) || 
#ifdef ATBM_WIFI_MODEL
                strstr(szRspTmp, WIFI_AT_RSP_ERR2) || 
#endif            
                (strstr(szRspTmp, "FAIL") && pszReq && strstr(pszReq, "AT+CWJAP=")) )
			{
				ret2=-1;
				break;
			}
			
			ret = -2;
			continue;
		}
	}
    
    if (pszOutRsp && iOutSize > 0)
    {
		strncpy((char *)pszOutRsp, (char *)szRspTmp, iOutSize);
        pszOutRsp[iOutSize]=0;
    }

	dbg("wifi_read [%d] = [%s], ret2=%d\n", strlen(szRspTmp), szRspTmp, ret2);
	//hexdump(pszOut, ret);

	return ret2;
}

int wifi_at_DisableEcho(void)
{
    int ret;
    int loop=0;
    
    //return wifi_at_cmd_sndrcv("ATE0\r\n", "OK", 300, 1, 1, NULL, 0);
    
    ret = 0;
    do{
        if(ret && ret!=-2)
            mdelay(200);        
        ret = wifi_at_cmd_sndrcv("ATE0\r\n", "OK", 300, 1, 1, NULL, 0);
    }while (ret != 0 && ++loop <= 3);
    
    return ret;
}

//ret: 1-已连接指定AP 0-未连接AP -1:已连接其它AP -2:取信号强度错 -9:查询失败
int wifi_at_QueryAPConnStatus(char *ssid, int *rssi)
{
	char res[200];
	char tmp[100];
	char *p0, *p1;

	int ret=wifi_at_cmd_sndrcv("AT+CWJAP?\r\n", "OK", 1200, 1, 1, res, sizeof(res)-1);
	if(ret==0)
	{
        if(strstr(res, "No AP"))
            return 0;
		if(ssid)
			sprintf(tmp, "+CWJAP:\"%s\",", ssid);
		else
			strcpy(tmp, "+CWJAP:");
		if(strstr(res, tmp)==NULL)
			return -1;
		
		if(rssi)
		{
			//取信号强度
			p0=res;
			p1=strchr(p0, ',');
			if(p1==NULL)
				return -2;
			
			p0=p1+1;
			p1=strchr(p0, ',');
			if(p1==NULL)
				return -2;
			
			p0=p1+1;
			p1=strchr(p0, ',');
			if(p1==NULL)
				return -2;
			
			p0=p1+1;
			p1=strstr(p0, "\r\nOK");
			if(p1==NULL)
				return -2;
			*p1=0;
			
			*rssi=atoi(p0)+150;             //>-75db为满格，+150后也就是>75为满格
		}
		return 1;
	}
	return -9;
}

int wifi_at_setRecvMode(int iMode)
{
	char send[100];
	int ret;
	
	if(iMode!=0 && iMode!=1)
		return -1;
	
#if 0		//查询当前模式
	strcpy(send, "AT+CIPRECVMODE?\r\n");
	ret=wifi_at_cmd_sndrcv(send, "OK", 0, 1, 1, NULL, 0);
#endif
	
	sprintf(send, "AT+CIPRECVMODE=%d\r\n", iMode);
	ret=wifi_at_cmd_sndrcv(send, "OK", 500, 1, 1, NULL, 0);
	if(ret==0)
		sg_iWifiRecvMode=iMode;
	return ret;
}


int wifi_at_SetStationMode(void)
{
	return wifi_at_cmd_sndrcv("AT+CWMODE=1\r\n", "OK", 500, 1, 1, NULL, 0);
}

int wifi_at_ConnectAp2(char *ssid, char *pwd)
{
	char send[100];
	//char recv[200];
	//char tmp[100];
	int ret;
    
	//连接AP前先查询是否已连接
	ret=wifi_at_QueryAPConnStatus(ssid, NULL);
	if(ret==1)
		return 0;
	if(ret==-1)
		wifi_at_DisconnAp();
	
	sprintf(send, "AT+CWJAP=\"%s\",\"%s\"\r\n", ssid, pwd);		
	ret=wifi_at_cmd_sndrcv(send, "OK", 20*1000, 1, 1, NULL, 0);
    if(ret)
    {
        //失败则再查询一次
        mdelay(100);
        ret=wifi_at_QueryAPConnStatus(ssid, NULL);
        if(ret==1)
            return 0;
        else
            ret=-1;
        //ret = wifi_at_cmd_sndrcv(send, "OK", 10*1000, 1, 1, NULL, 0);
    }
    return ret;
}

int wifi_at_ConnectAp(void)
{
    int ret;
	ret = wifi_at_ConnectAp2("JSD_test", "23609898");
	return ret;
}

int wifi_at_get_localIP(char *pszIp, char *pszMac)
{
	char res[200];
	
	int ret=wifi_at_cmd_sndrcv("AT+CIFSR\r\n", "OK", 5*1000, 1, 1, res, sizeof(res)-1);
	if(ret==0)
	{
		char *p0, *p1;
		
		if(pszIp)
		{
			p0=strstr(res, ":STAIP,\"");
			if(p0==NULL)
			{
				return -1;
			}
			p0+=8;
			p1=strchr(p0+1, '\"');
			if(p1==NULL || p1-p0>15)
			{
				return -1;
			}
			memcpy(pszIp, p0, p1-p0);
			pszIp[p1-p0]=0;
            dbg("ip:[%s]\n", pszIp);
		}
		
		if(pszMac)
		{
			p0=strstr(res, ":STAMAC,\"");
			if(p0==NULL)
			{
				return -1;
			}
			p0+=9;
			p1=strchr(p0+1, '\"');
			if(p1==NULL || p1-p0>17)
			{
				return -1;
			}
			memcpy(pszMac, p0, p1-p0);
			pszMac[p1-p0]=0;
            dbg("mac:[%s]\n", pszMac);
		}
		return 0;
	}
	return ret;
}

int wifi_at_tcpudp_connect(int type, char *szIpHost, int port)
{
	char send[100];
    char recv[100];
    int ret;
	
    if(type==0)
        sprintf(send, "AT+CIPSTART=\"TCP\",\"%s\",%d\r\n", szIpHost, port);
    else
        sprintf(send, "AT+CIPSTART=\"UDP\",\"%s\",%d\r\n", szIpHost, port);
	ret=wifi_at_cmd_sndrcv(send, "OK", 10*1000, 1, 1, recv, sizeof(recv)-1);
    if(ret && strstr(recv, "ALREADY CONNECTED"))
    {
        wifi_at_DisconnTcp();
        mdelay(300);
        ret=wifi_at_cmd_sndrcv(send, "OK", 10*1000, 1, 1, NULL, 0);
    }
    return ret;
}

int wifi_at_tcp_connect(char *szIpHost, int port)
{
	return wifi_at_tcpudp_connect(0, szIpHost, port);
}

int wifi_at_DisconnTcp(void)
{
	return wifi_at_cmd_sndrcv("AT+CIPCLOSE\r\n", "OK", 500, 1, 1, NULL, 0);
}

int wifi_at_DisconnAp(void)
{
	return wifi_at_cmd_sndrcv("AT+CWQAP\r\n", "OK", 500, 1, 1, NULL, 0);
}

int wifi_at_MoudleIsActiveNum(int num)
{
	int ret;
    int loop=0;

    ret=0;
    do{
        if(ret && ret!=-2)
            mdelay(200);
        ret = wifi_at_cmd_sndrcv("AT\r\n", "OK\r", 300, 1, 1, NULL, 0);
    }while(ret && ++loop<num);
    
    ret=(ret==0?1:0);
    
	dbg("wifi_at_MoudleIsActive return %d\n", ret);
	return ret;
}

int wifi_at_MoudleIsActive(void)
{
	return wifi_at_MoudleIsActiveNum(1);
}

int wifi_at_tcp_send(unsigned char *sndData, int sndLen)
{
	char send[100];
	int ret;
	//int loop = 0;
	int hadSndLen;

	dbg("send start. tick=%u\n", get_tick());
	
	if(sndLen<=0)
		return -2;
	if (sndLen > WIFI_TCP_SND_BUFFER_MAX_SIZE || NULL == sndData)
	{
		dbg("wifi_at_tcpip_snd too long:%d\n", sndLen);
		return -2;
	}
	
#ifdef WIFI_RECV_MODE_PASV	
	if(sg_iWifiRecvMode==0)
		wifi_at_setRecvMode(1);
#else
	sg_iWifiRcvLen=0;
	sg_iWifiRcvOff=0;
#endif
	
	dbg("send data:\n");
	//hexdump(sndData, sndLen);
		
	sprintf(send, "AT+CIPSEND=%d\r\n", sndLen);
	//ATBM模块使用">"作为返回值判断；
	//乐鑫8266模块使用 "> "作为返回值判断(多了空格)
	//if(wifi_at_cmd_sndrcv(send, "> ", 500, 0, 1, NULL, 0))
	if(wifi_at_cmd_sndrcv(send, ">", 500, 0, 1, NULL, 0))
		return -1;

	//if (strstr(recv, correctRsp)!= NULL)
	{
		hadSndLen=0;
		while(sndLen>0)
		{
			ret = wifi_write(sndData+hadSndLen, sndLen);
			//dbg("wifi_write[%d] = %s\n", ret, sndData+hadSndLen);
			if (ret >= 0)
			{
				sndLen-=ret;
				hadSndLen+=ret;
				
				dbg("wifi_at_tcpip_snd return = %d. sndLen=0, hadSndLen=%d\n", ret, sndLen, hadSndLen);
				continue;
			}else
			{
				dbg("wifi_at_TcpSnd return -1\n");
				return -1;
			}
		}
	}
	
	{
		char szRspTmp[100];
		char *pszOut;
		tick tmstart;
		int iTimeOut=5*1000;

		//dbg("get send rsp. tick=%u\n", get_tick());
		
		tmstart=get_tick();
		pszOut=szRspTmp;
		while(get_tick()<tmstart+iTimeOut && (pszOut-szRspTmp<sizeof(szRspTmp)-1))
		{		
			ret = wifi_read((uint8_t*)pszOut, 1, 100);
			if(ret<=0)
				continue;
			//dbg("ch:%c=%02X\n", pszOut[0], pszOut[0]);
			if(pszOut[0]!=0x0A)			//判断\n\r的0A
			{
				++pszOut;
				continue;
			}
			pszOut[1] = 0x00;
			++pszOut;
			
			//接收到一行响应，开始判断			
			if(strstr((char*)szRspTmp, "OK\r\n"))
			{
				break;
			}
		}
	}
	dbg("send return. tick=%u\n", get_tick());
	
	return hadSndLen;
}

#ifdef WIFI_RECV_MODE_PASV
int wifi_at_tcp_recv(unsigned char *rcvData, int expRcvLen, int timeoutMs)
{
	char send[100];
	char recv[100];
	int ret, revLen=0, len=0;
	char *p0, *p1;

	dbg("recv start. tick=%u\n", get_tick());
	dbg("WIFI_RECV_MODE_PASV.\n");
	
	if(sg_iWifiRecvMode==0)
	{
		dbg("wifi模块未设置数据接收被动模式");
		return -99;
	}
    
    if(gl_CommCallbackFunc)
        gl_CommCallbackFunc();
    
	if(expRcvLen>WIFI_TCP_RCV_BUFF_MAXSIZE)
	{
		dbg("warnning!!! expRcvLen is too long!");
		expRcvLen=WIFI_TCP_RCV_BUFF_MAXSIZE;
	}
	
	sprintf(send, "AT+CIPRECVDATA=%d\r\n", expRcvLen);
	ret=wifi_at_cmd_sndrcv(send, ":", timeoutMs, 0, 1, recv, sizeof(recv)-1);
	//if(ret==-2 && strstr(recv, "+IPD,"))
	//	ret=wifi_at_cmd_sndrcv(send, ":", timeoutMs, 0, 1, recv, sizeof(recv)-1);
	if(ret)
	{	
		dbg("wifi_at_cmd_sndrcv error\n");
		return ret;
	}
	p0=strstr(recv, "+CIPRECVDATA,");
	if(p0==NULL)
	{
		dbg("p0==NULL\n");
		return -1;
	}
	
	p0+=strlen("+CIPRECVDATA,");
	p1=strchr(p0, ':');
	if(p1==NULL || p1==p0)
	{
		dbg("p1==NULL || p1==p0\n");
		return -1;
	}
	*p1=0;
	revLen=atoi(p0);
	if(revLen>expRcvLen)
	{
		dbg("revLen > expRcvLen\n");
		return -1;
	}
	
	p0=(char*)rcvData;
	len=revLen;
	//收取包内数据
	while(len>0)
	{
		ret =wifi_read((unsigned char*)p0, len, 1000);
		if(ret<=0)
		{
			dbg("wifi_read error\n");
			return -1; 
		}
		//dbg("wifi_read ret=%d\n", ret);
		p0+=ret;
		len-=ret;
	}
	
	dbg("recv data return, len=[%d].tick=%u\n", revLen, get_tick());
	//hexdump(rcvData, revLen);
	
	return revLen;
}

#else

//以下为主动模式收取数据,不建议使用主动模式

//从wifi模块接收一包数据, 格式为 +IPD,<len>:<data>
//ret: <=0失败  >0收取的数据长度
int wifi_at_tcp_recvpack(unsigned char *rcvData, int size, int timeoutMs)
{
	uint8_t tmp[100];
	int ret;
	int len, outLen;
	//uint8_t needFirst=1;
	tick tk;
	uint8_t *ps;
	
	if(timeoutMs<0)
		return 0;

	tk=get_tick();	
	//收取包头"+IPD,"
	while(get_tick()-tk<=timeoutMs)		//判断是否超时
	{
#if 0		
		//第一字节'+'
		if(needFirst)
		{
			ret = wifi_read(tmp, 1, 500);
			if(ret<=0)
				continue;
			dbg("recv start %c\n", tmp[0]);
			if(tmp[0]!='+')
				continue;
		}
		needFirst=1;	//预置1
		
		//第二字节'I'
		ret = wifi_read(tmp+1, 1, 100);
		if(ret<=0)	//"+IPD,"为连续5字节，如果接收失败则认为不是包的开始
		{	
			continue;
		}
		dbg("recv %c\n", tmp[1]);
		if(tmp[1]=='+')
		{
			//收到'+'重新开始收取第二字节
			needFirst=0;
			continue;
		}
		if(tmp[1]!='I')
			continue;
		
		//第三字节'P'
		ret = wifi_read(tmp+2, 1, 100);
		if(ret<=0)	//"+IPD,"为连续5字节，如果接收失败则认为不是包的开始
		{	
			continue;
		}
		dbg("recv %c\n", tmp[2]);
		if(tmp[2]=='+')
		{
			needFirst=0;
			continue;
		}
		if(tmp[2]!='P')
			continue;
		
		//第四字节'D'
		ret = wifi_read(tmp+3, 1, 100);
		if(ret<=0)	//"+IPD,"为连续5字节，如果接收失败则认为不是包的开始
		{	
			continue;
		}
		dbg("recv %c\n", tmp[3]);
		if(tmp[3]=='+')
		{
			needFirst=0;
			continue;
		}
		if(tmp[3]!='D')
			continue;
		
		//第五字节','
		ret = wifi_read(tmp+4, 1, 100);
		if(ret<=0)	//"+IPD,"为连续5字节，如果接收失败则认为不是包的开始
		{	
			continue;
		}
		dbg("recv %c\n", tmp[4]);
		if(tmp[4]=='+')
		{
			needFirst=0;
			continue;
		}
		if(tmp[4]!=',')
			continue;
		break;
#else
		ret = wifi_read(tmp, 1, 500);
		if(ret<=0)	//"+IPD,"为连续5字节，如果接收失败则认为不是包的开始
		{	
			continue;
		}
		//dbg("recv %c\n", tmp[0]);
		if(tmp[0]==',')
			break;
#endif
	}

	dbg("recv len...\n");
	//收取长度
	len=0;
	while(1)
	{
		ret = wifi_read(tmp+len, 1, 100);
		if(ret<=0)
		{
			dbg("recv len err.\n");
			return -1;
		}
		if(tmp[len]==':')
			break;
		if(len>=5 || tmp[len]<'0' || tmp[len]>'9')
		{
			dbg("recv len err2.\n");
			return -1;
		}
		++len;
	}
	tmp[len]=0;
	len=atoi(tmp);
	outLen=len;
    dbg("pack data len=%d\n", len);
	if(size<=outLen)
	{
		dbg("rcvpack buff size is too short.size=%d,len=%d.\n", size, outLen);
		return -1;
	}

	ps=rcvData;
	//收取包内数据
	while(len>0)
	{
		ret = wifi_read(ps, len, 500);
		if(ret<=0)
		{
			return -1;
		}
		ps+=ret;
		len-=ret;
	}
	
	dbg("return pack data,len=%d\n", outLen);
	
	return outLen;
}

int wifi_at_tcp_recvmanypack(unsigned char *rcvData, int size, int timeoutMs)
{
	tick tk=get_tick();
	int len=wifi_at_tcp_recvpack(rcvData, size, timeoutMs);
	if(len<=0)
		return 0;
	if(timeoutMs>500)
		timeoutMs=500;
	return len+wifi_at_tcp_recvmanypack(rcvData+len, size-len, timeoutMs-(get_tick()-tk));
}


int wifi_at_tcp_recv(unsigned char *rcvData, int expRcvLen, int timeoutMs)
{
	int ret, revLen=0, len=0;

	//tick tk=get_tick();	
	
	if(expRcvLen>sizeof(sg_sWifiRcvBuff))
	{
		dbg("warn!!! expRcvLen is too long!");
		expRcvLen=sizeof(sg_sWifiRcvBuff);
	}
	
	dbg("wifi_at_tcp_recv, sg_iWifiRcvLen=%d, tk=%u\n", sg_iWifiRcvLen, get_tick());
	
	if(sg_iWifiRcvLen)
	{
		len=sg_iWifiRcvLen>=expRcvLen?expRcvLen:sg_iWifiRcvLen;

		memcpy(rcvData, sg_sWifiRcvBuff+sg_iWifiRcvOff, len);	
		sg_iWifiRcvOff+=len;
		sg_iWifiRcvLen-=len;
		revLen+=len;
		expRcvLen-=len;

		if(sg_iWifiRcvLen==0)
			sg_iWifiRcvOff=0;

		//当前收取数据较多时,尝试一并读取下一包数据
		if(revLen>300 && sg_iWifiRcvOff+sg_iWifiRcvLen<sizeof(sg_sWifiRcvBuff)/2)
		{
			dbg("retry read next pack, sg_iWifiRcvOff=%d, sg_iWifiRcvLen=%d, revLen=%d\n", sg_iWifiRcvOff, sg_iWifiRcvLen, revLen);
			ret=wifi_at_tcp_recvpack(sg_sWifiRcvBuff+sg_iWifiRcvOff+sg_iWifiRcvLen, sizeof(sg_sWifiRcvBuff)-sg_iWifiRcvOff-sg_iWifiRcvLen-1, 300);
			if(ret<=0)
			{
				return revLen;
			}
			sg_iWifiRcvLen+=ret;
		}
		
		if(expRcvLen==0)
			return revLen;
	}
	
	if(timeoutMs<=0)
		return revLen;
	/*
	if(sg_iWifiRcvOff+sg_iWifiRcvLen>=WIFI_TCP_RCV_BUFF_MAXSIZE)
	{
		memmove(sg_sWifiRcvBuff, sg_sWifiRcvBuff+sg_iWifiRcvOff, sg_iWifiRcvLen);
		sg_iWifiRcvOff=0;
	}
	*/
	
	
	dbg("wifi_at_tcp_recv, expRcvLen=%d, revLen=%d, timeout=%d\n", expRcvLen, revLen, timeoutMs);
	//ret=wifi_at_tcp_recvmanypack(sg_sWifiRcvBuff, WIFI_TCP_RCV_BUFF_MAXSIZE, timeoutMs);		//一次尽量收取多包数据
	//ret=wifi_at_tcp_recvpack(sg_sWifiRcvBuff, WIFI_TCP_RCV_BUFF_MAXSIZE, timeoutMs);
    ret=wifi_at_tcp_recvpack(sg_sWifiRcvBuff, sizeof(sg_sWifiRcvBuff), timeoutMs);
	if(ret<=0)
	{
		return revLen;
	}
	sg_iWifiRcvLen=ret;
	
	dbg("recv pack data[%d]:\n", sg_iWifiRcvLen);
	//hexdump(sg_sWifiRcvBuff, sg_iWifiRcvLen);
    //dbgHex("recv pack data[%d]", sg_iWifiRcvLen);
	
	len=sg_iWifiRcvLen>=expRcvLen?expRcvLen:sg_iWifiRcvLen;
	memcpy(rcvData+revLen, sg_sWifiRcvBuff, len);
	sg_iWifiRcvOff=len;
	sg_iWifiRcvLen-=len;
	revLen+=len;
	expRcvLen-=len;
	
	/*
	//尝试读取下一包数据
	if(0)
	{
		dbg("retry read next pack, sg_iWifiRcvOff=%d, sg_iWifiRcvLen=%d, revLen=%d\n", sg_iWifiRcvOff, sg_iWifiRcvLen, revLen);
		ret=wifi_at_tcp_recvpack(sg_sWifiRcvBuff+sg_iWifiRcvOff+sg_iWifiRcvLen, WIFI_TCP_RCV_BUFF_MAXSIZE-sg_iWifiRcvOff-sg_iWifiRcvLen-1, 200);
		if(ret<=0)
		{
			return revLen;
		}
		sg_iWifiRcvLen+=ret;
	}
	*/

    dbg("recv data return, len=[%d].tick=%u\n", revLen, get_tick());
    
	return revLen;
}

//按整包收取数据,下载大文件时使用
int wifi_at_tcp_recvPack(unsigned char *rcvData, int expRcvLen, int timeoutMs)
{
	int ret, revLen=0, len=0;		
	dbg("wifi_at_tcp_recv, sg_iWifiRcvLen=%d, tk=%u\n", sg_iWifiRcvLen, get_tick());
	
	if(sg_iWifiRcvLen)
	{
		len=sg_iWifiRcvLen>=expRcvLen?expRcvLen:sg_iWifiRcvLen;

		memcpy(rcvData, sg_sWifiRcvBuff+sg_iWifiRcvOff, len);	
		sg_iWifiRcvOff+=len;
		sg_iWifiRcvLen-=len;

		if(sg_iWifiRcvLen==0)
			sg_iWifiRcvOff=0;
		
        return len;
	}
		
	dbg("wifi_at_tcp_recv, expRcvLen=%d, revLen=%d, timeout=%d\n", expRcvLen, revLen, timeoutMs);
    ret=wifi_at_tcp_recvpack(rcvData, expRcvLen, timeoutMs);
	if(ret<=0)
	{
		return 0;
	}
	revLen=ret;
    dbg("recv data return, len=[%d].tick=%u\n", revLen, get_tick());
    
	return revLen;
}

#endif	//#ifdef WIFI_RECV_MODE_PASV

void wifi_rts_init(void){
    GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Remap = GPIO_Remap_1;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
    GPIO_ResetBits(GPIOA, GPIO_Pin_3);

    //pa2 input
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    
    dbg("set rts pa3 low ,pa2 input\n");
	
	sg_iWifiRecvMode=0;		//wifi_rts_init函数在wifi模块上电后调用,wifi模块重新上电后接收模式为0
}

int wifi_at_gmr(char *pszWifiVer, int iOutSize)
{
    //#ifdef ATBM_WIFI_MODEL
    //return wifi_at_cmd_sndrcv("AT+GET_VER\r\n", "OK", 200, 1, 1, pszWifiVer, iOutSize);
    //#else
	return wifi_at_cmd_sndrcv("AT+GMR\r\n", "OK", 200, 1, 1, pszWifiVer, iOutSize);
    //#endif
}

int wifi_at_ping(char *host)
{
	char send[100];
	char correctRsp[100];

	sprintf(send, "AT+PING=\"%s\"\r\n", host);
	strcpy(correctRsp, "\r\nOK\r\n");

	return wifi_at_cmd_sndrcv(send, correctRsp, 5*1000, 1, 1, NULL, 0);
}

/*
int wifi_at_test()
{
	char szSendData[1024]; 
	char szRcvData[1024];
	int ret;
	unsigned char tmp[1024];

	dbg("Before wifi_init \n");
	wifi_init();
	dbg("Before wifi_hardware_poweron \n");
	wifi_hardware_poweron();
	dbg("After wifi_hardware_poweron \n");

	dbg("After power on\n");
	ret = wifi_read(tmp, 1024, 5*1000);
	dbg("After power on wifi_read[%d] = [%s]\n", ret, tmp);
	//hexdump(tmp, ret);

	wifi_at_DisableEcho();

	wifi_at_gms();

	if (wifi_at_SetStationMode() != 0)
	{
		dbg("wifi_at_SetStationMode error\n");
		return -2;
	}

	if (wifi_at_ConnectAp() != 0)
	{
		dbg("wifi_at_ConnectAp error\n");
		return -3;
	}

	wifi_at_check_localIP();

	//wifi_at_ping("www.baidu.com");
	//wifi_at_ping("192.168.2.144");

	if (wifi_at_tcpip_connect("192.168.2.144", 8080) != 0)
	{
		dbg("wifi_at_tcpip_connect error\n");
		return -4;
	}

	while(1)
	{
		strcpy(szSendData, "12345");
		if (wifi_at_tcpip_snd(szSendData, strlen(szSendData)) != strlen(szSendData))
		{
			dbg("wifi_at_tcpip_snd error\n");
			//return -5;
		}

		
		ret = wifi_at_tcpip_rcv(szRcvData, 100, 10*1000);
	}
}
*/

static int wifiInitFlag=0;
void wifi_statusClear(void)
{
    wifiInitFlag=0;
    sg_iWifiRecvMode=0;
    return;
}

int wifi_at_poweroff(void)
{
    wifi_poweroff();
    wifi_statusClear();
    return 0;
}

int wifi_appLayer_Init(void)
{
	//unsigned char tmp[1024];
	int ret;
	tick t1;
	//char szLocalIp[20], szMac[20];
	//int retry;
	
	t1=get_tick();
    
    if(wifiInitFlag==0x55 && wifi_at_MoudleIsActive()==0)
    {
        wifi_at_poweroff();
    }
	
	if(wifiInitFlag!=0x55)
	{
		//wifi_init();
		dbg("Before power on\n");
		wifi_poweron();

        #if 0
		dbg("After power on\n");
		ret = wifi_read(tmp, 1024, 1000);
		dbg("After power on wifi_read[%d] = [%s]\n", ret, tmp);
		//wifi_at_ctsrts();
		#else
        wifi_at_MoudleIsActiveNum(10);
        #endif
        
		if (wifi_at_DisableEcho() != 0)
		{
			dbg("wifi_at_DisableEcho error\n");
			ret = -1;
			//return ret;
		}

		if (wifi_at_SetStationMode() != 0)
		{
			dbg("wifi_at_SetStationMode error\n");
			ret = -2;
			//return ret;
		}
		
		wifiInitFlag=0x55;
		ret=0;

		dbg("wifi power state = %d\n", wifi_getpowerstate());
	}else
	{
		ret=0;
	}

	dbg("**** wifi start time=%u ms\n", get_tick()-t1);
	
	return ret;
}
#if 0
void BarcodeScanner_NoCommandTest()
{
	int ret;
	unsigned char pcRcvBuf[1024];
	unsigned char pcSndBuf[2010];
	char timeoutStr[3];
	int iTimeout;
	int ret2;

	dbg("Enter BarcodeScanner_Test, buffer 2010()\n");

	memset(pcRcvBuf, 0x00, sizeof(pcRcvBuf));

	LcdClear();
	LcdPutscc("wait for command");
	while(1)
	{
	/*
		ret = serialread(UARTUSB, pcRcvBuf, 1, 10*60*1000);
		dbg("serialread UARTUSB ret = %d\n", ret);
		if (ret == 1 && (pcRcvBuf[0] == '1'))
		{
			dbg("serialread '1'\n");
			ret = serialread(UARTUSB, pcRcvBuf + 1, 7, 1000);
			if (ret == 7 && (memcmp(pcRcvBuf, "101099", strlen("101099")) == 0))
			{
				dbg("serialread 7 ok\n");
				memcpy(timeoutStr, pcRcvBuf + 6, 2);
				timeoutStr[2] = 0x00;
				iTimeout = atoi(timeoutStr);
			}else
			{
				dbg("serialread 7 error\n");
				continue;
			}
		}else
		{
			//dbg("Timeout\n");
			continue;
		}

		LcdPutscc("Received Command");
		dbg("BarcodeScanner_Test: serialread = %s\n", pcRcvBuf);

		//For test
		ret = serialwrite(UARTUSB, "1234", 4);
		dbg("BarcodeScanner_Test: serialwrite ret = %d\n", ret);
		
		LcdPutscc("Test response sent");
		//continue;
		//For test end
		*/
		
		//ret = wifi_write(pcRcvBuf, 8);
		ret2 = serialread(UARTUSB, pcRcvBuf, 1024, 200);
		dbg("USB Serial read = %d\n", ret2);
		

	
		ret = wifi_write("10109910", 8);
		dbg("wifi_write return = %d\n", ret);
		if (ret != 8)
		{
			dbg("wifi_write err\n");
			continue;
		}

		ret = wifi_read(pcSndBuf, sizeof(pcSndBuf), 10*1000 + 1*1000);
		dbg("wifi_read return = %d\n", ret);
		
		if (ret > 0)
		{
			//dbg("serialwrite\n");
			//ret2 = serialwrite(UARTUSB, pcSndBuf, ret);
			//dbg("serialwrite return = %d\n", ret2);
		}

/*
		ret = serialread(UARTUSB, pcRcvBuf, 1024, 200);
		dbg("USB Serial read = %d\n", ret);
		hexdumpEx("USB Serial read List", pcRcvBuf, ret);
*/
		if (ret2 > 0)
		{
			dbg("USB Serial try to write data len = [%d]\n", ret2);
			serialwrite(UARTUSB, pcRcvBuf, ret2);
		}
		
		LcdPutscc("Wait for command");
	}
}


void BarcodeScanner_Test()
{
	int ret;
	unsigned char pcRcvBuf[1024];
	unsigned char pcSndBuf[2010];
	char timeoutStr[3];
	int iTimeout;
	int ret2;

	dbg("Enter BarcodeScanner_Test, buffer 2010()\n");

	memset(pcRcvBuf, 0x00, sizeof(pcRcvBuf));

	LcdClear();
	LcdPutscc("wait for command");
	while(1)
	{
	
		ret = serialread(UARTUSB, pcRcvBuf, 1, 10*60*1000);
		dbg("serialread UARTUSB ret = %d\n", ret);
		if (ret == 1 && (pcRcvBuf[0] == '1'))
		{
			dbg("serialread '1'\n");
			ret = serialread(UARTUSB, pcRcvBuf + 1, 7, 1000);
			if (ret == 7 && (memcmp(pcRcvBuf, "101099", strlen("101099")) == 0))
			{
				dbg("serialread 7 ok\n");
				memcpy(timeoutStr, pcRcvBuf + 6, 2);
				timeoutStr[2] = 0x00;
				iTimeout = atoi(timeoutStr);
				dbg("iTimeout = %d\n", iTimeout);
			}else
			{
				dbg("serialread 7 error\n");
				continue;
			}
		}else
		{
			//dbg("Timeout\n");
			continue;
		}

		LcdPutscc("Received Command");
		

		ret = wifi_write(pcRcvBuf, 8);
		
		ret = wifi_read(pcSndBuf, sizeof(pcSndBuf), iTimeout*1000 + 1*1000);
		dbg("wifi_read return = %d\n", ret);
		
		if (ret > 0)
		{
			
			ret2 = serialwrite(UARTUSB, pcSndBuf, ret);
			dbg("UARTUSB serialwrite return = %d\n", ret2);
		}

		
		LcdPutscc("Wait for command");
	}
}
#endif
int wifi_at_ListAP(AP_INFO *pstAPInfos, int size)
{
	//char send[30];
	char resp[2048];
	int ret;
	char *p0, *p1, *p2;
	int i;
	
	//设置wifi列表格式,每个ap只返回esn,ssid,rssi三列数据
	ret=wifi_at_cmd_sndrcv("AT+CWLAPOPT=1,7\r\n", "OK", 500, 1, 1, NULL, 0);
	if(ret)
		return ret;

	//查询AP列表
	//ret=wifi_at_cmd_sndrcv("AT+CWLAP\r\n", "\r\nOK\r\n", 0, 1, 1, resp, sizeof(resp)-1);
    ret=wifi_at_cmd_sndrcv("AT+CWLAP\r\n", "K\r\n", 30*1000, 1, 1, resp, sizeof(resp)-1);
	if(ret)
		return ret;
	resp[sizeof(resp)-1]=0;
	
	memset(pstAPInfos, 0, sizeof(AP_INFO)*size);
	
	p0=resp;
	for(i=0; i<size;)
	{
		p1=strstr(p0, "+CWLAP:(");
		if(p1==NULL)
			break;
		p0=p1+8;
		p1=strstr(p0, ")\r\n");
		if(p1==NULL)
			break;
		*p1=0;
		
		p2=p1+3;
		
		p1=strchr(p0, ','); 
		if(p1==NULL)
			break;
		*p1=0;
		pstAPInfos[i].ecn=atoi(p0);
		if(0 && pstAPInfos[i].ecn<=1)		//bctc要求过滤ecn为0和1的wifi
		{
			*p1=',';		//恢复原值打印日志
			dbg("Ignore wifi:[%s]\n", p0);
			p0=p2;
			continue;
		}
		
		p0=p1+1;
		p1=strchr(p0, ','); 
		if(p1==NULL)
			break;
		*p1=0;
		if(*p0=='"' && *(p1-1)=='"')
		{
			//去除ssid首尾双引号
			p0++;
			*(p1-1)=0;
		}
		if(*p0==0)		//ssid为空的暂不支持
		{
			p0=p2;
			continue;
		}
		
		strncpy(pstAPInfos[i].ssid, p0, sizeof(pstAPInfos[i].ssid)-1);
		
		p0=p1+1;
		//p1=strchr(p0, ',');
		//if(p1==NULL)
		//	break;
		pstAPInfos[i].rssi=atoi(p0);
		
		p0=p2;
		
		i++;
		continue;
	}
	
	/*
	for(i=0; i<size && pstAPInfos[i].ssid[0]!=0; i++)
	{
		dbg("%d. ssid:%s, %d, %d\n", i+1, pstAPInfos[i].ssid, pstAPInfos[i].ecn, pstAPInfos[i].rssi);
		if(memcmp(pstAPInfos[i].ssid, "xfwifi", 4)==0)
		{
			dbg("print ssid...\n");
			for(int n=0; n<strlen(pstAPInfos[i].ssid); n++)
			{
				dbg("%02X %c\n", pstAPInfos[i].ssid[n], pstAPInfos[i].ssid[n]);
			}
		}
	}
	dbg("---end---\n");
	*/
	
	if(i==0)
		return 1;
	return 0;
}

//域名解析
int wifi_at_domain(char *pszHostName, char *pszHostIp)
{
	char send[100];
	char resp[100];
	int ret;
	char *p0, *p1;
	
	sprintf(send, "AT+CIPDOMAIN=\"%.80s\"\r\n", pszHostName);
	ret=wifi_at_cmd_sndrcv(send, "OK", 2*1000, 1, 1, resp, sizeof(resp)-1);
	if(ret)
		return ret;
	p0=strstr(resp, "+CIPDOMAIN:");
	if(p0==NULL)
		return -1;
	p0+=strlen("+CIPDOMAIN:");
	p1=strstr(p0, "\r\n");
	if(p1)
		*p1=0;
    
    //ATBM返回的IP地址有引号
    if(p0[0]=='"')
    {
        p0++;
        p1=strchr(p0, '"');
        if(p1)
            *p1=0;
    }
    
	strcpy(pszHostIp, p0);
	return 0;
}

int wifi_at_time(char *pszDateTime)
{
	int ret;
    
    if(pszDateTime==NULL)
        return -1;
    pszDateTime[0]=0;
    
#ifdef 	ATBM_WIFI_MODEL
    /*    
    //设置时区
    strcpy(send, "AT+SNTP_SET ZONE 8\r\n");
	ret=wifi_at_cmd_sndrcv(send, "OK", 1000, 1, 1, resp, sizeof(resp)-1);
	//if(ret)
	//	return ret;
    
    //取时间
	strcpy(send, "AT+SNTP_GET\r\n");
	ret=wifi_at_cmd_sndrcv(send, "OK", 1000, 1, 1, resp, sizeof(resp)-1);
	if(ret)
		return ret;
    */
    
    //转换int类型大小端
    #define ENDIAN_SWAP32(data)  	((data >> 24) | /* right shift 3 bytes */ \
                    ((data & 0x00ff0000) >> 8) | /* right shift 1 byte */ \
                    ((data & 0x0000ff00) << 8) | /* left shift 1 byte */ \
                    ((data & 0x000000ff) << 24)) /* left shift 3 bytes */
                    
    //1900-1970的秒数,时间格式转换时使用
    #define NTP_TIMESTAMP_DELTA 2208988800ull
    
    typedef struct
    {
        uint8_t li_vn_mode;      // Eight bits. li, vn, and mode.
                                 // li.   Two bits.   Leap indicator.
                                 // vn.   Three bits. Version number of the protocol.
                                 // mode. Three bits. Client will pick mode 3 for client.

        uint8_t stratum;         // Eight bits. Stratum level of the local clock.
        uint8_t poll;            // Eight bits. Maximum interval between successive messages.
        uint8_t precision;       // Eight bits. Precision of the local clock.

        uint32_t rootDelay;      // 32 bits. Total round trip delay time.
        uint32_t rootDispersion; // 32 bits. Max error aloud from primary clock source.
        uint32_t refId;          // 32 bits. Reference clock identifier.

        uint32_t refTm_s;        // 32 bits. Reference time-stamp seconds.
        uint32_t refTm_f;        // 32 bits. Reference time-stamp fraction of a second.

        uint32_t origTm_s;       // 32 bits. Originate time-stamp seconds.
        uint32_t origTm_f;       // 32 bits. Originate time-stamp fraction of a second.

        uint32_t rxTm_s;         // 32 bits. Received time-stamp seconds.
        uint32_t rxTm_f;         // 32 bits. Received time-stamp fraction of a second.

        uint32_t txTm_s;         // 32 bits and the most important field the client cares about. Transmit time-stamp seconds.
        uint32_t txTm_f;         // 32 bits. Transmit time-stamp fraction of a second.
    } ntp_packet; 
    
    ntp_packet packet;  // = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    memset( &packet, 0, sizeof( ntp_packet ) );
    packet.li_vn_mode=0x1B;         //或0xE3
    
    dbg("ntp size:%d\n", sizeof(packet));
	ret=wifi_at_tcpudp_connect(1, "120.25.115.20", 123);
    if(ret==0)
    {
        ret=wifi_at_tcp_send((unsigned char*)&packet, sizeof(packet));
        if(ret==sizeof(packet))
        {
            ret=wifi_at_tcp_recv((unsigned char*)&packet, sizeof(packet), 1500);
            //dbg("wifi_at_tcp_recv ret:%d\n", ret);
        }
        wifi_at_DisconnTcp();
        if(ret==sizeof(packet))
        {
            packet.txTm_s = ENDIAN_SWAP32( packet.txTm_s ); // Time-stamp seconds.
            packet.txTm_f = ENDIAN_SWAP32( packet.txTm_f ); // Time-stamp fraction of a second.
            time_t txTm = ( time_t ) ( packet.txTm_s - NTP_TIMESTAMP_DELTA + 28800);    //28800:东八区8*60*60

            //dbg( "Time: %s", ctime( ( const time_t* ) &txTm ) );
            struct tm *now;
            now = localtime(&txTm);
            sprintf(pszDateTime, "%04d%02d%02d%02d%02d%02d", now->tm_year+1900, now->tm_mon+1, now->tm_mday, 
                    now->tm_hour, now->tm_min, now->tm_sec);
            ret=0;
        }else
            ret=-1;
        
    }
    
    return ret;
#else
	char send[100];    
	char resp[100];    
    char *p0, *p1;
	int loop=0;
    
	//strcpy(send, "AT+CIPSNTPCFG=1,8\r\n");
	//ret=wifi_at_cmd_sndrcv(send, "OK", 1000, 1, 1, resp, sizeof(resp)-1);
	//if(ret)
	//	return ret;

	while(++loop<=2)
	{
		strcpy(send, "AT+CIPSNTPTIME?\r\n");
		ret=wifi_at_cmd_sndrcv(send, "OK", 1000, 1, 1, resp, sizeof(resp)-1);
		if(ret)
			return ret;
		p0=strstr(resp, "+CIPSNTPTIME:");
		if(p0==NULL)
			return -1;
		p0+=strlen("+CIPSNTPTIME:");
		p1=strstr(p0, "\r\n");
		if(p1)
			*p1=0;
		//解析取时间
		{
			char arr[10][40];
			char months[]="Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec ";
			int year=-1, month=-1, date=-1, hour=-1, min, sec;
			int n;

			if(*p0==' ')
				p0++;
			memset(arr, 0, sizeof(arr));
			//按空格拆分
			sscanf(p0, "%s%s%s%s%s%s%s%s%s%s", arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9]);
			for(n=0; n<10 && arr[n]; n++)
			{
				//月日
				if(month<0 && strlen(arr[n])==3 && (p1=strstr(months, arr[n]))!=NULL)
				{
					month=(p1-months)/4+1;
									
					date=atoi(arr[++n]);	//日期跟在月份后
					continue;
				}

				//年
				if(year<0 && strlen(arr[n])==4 && arr[n][0]=='2' && arr[n][1]=='0')
				{
					year=atoi(arr[n]);
					continue;
				}

				//时间
				if(hour<0 && strlen(arr[n])>=7 && strchr(arr[n], ':'))
				{
					sscanf(arr[n], "%d:%d:%d", &hour, &min, &sec);
					continue;
				}
			}
			if(year<0 || month<0 || hour<0)		//有时候第一次返回OK但没有时间信息,重发指令再取一次
				continue;
			sprintf(pszDateTime, "%04d%02d%02d%02d%02d%02d", year, month, date, hour, min, sec);
			if(strlen(pszDateTime)!=14 || memcmp(pszDateTime, "20", 2)!=0)
			{
				pszDateTime[0]=0;
				return 1;
			}
			dbg("datetime:%s\n", pszDateTime);
			return 0;
		}
	}
	return 1;
#endif    
}

//ret:  
//     -1: err
//      2：ESP8266 Station 已连接AP并获得本地IP地址
//      3：ESP8266 Station 已建力TCP 或 UDP 传输
//      4：ESP8266 Station 断开TCP/UDP网络连接
//      5：ESP8266 Station 未连接 AP 
int wifi_at_connStatus(void)
{
	//char send[100];
	int ret;
    int status=-1;
    
#ifndef ATBM_WIFI_MODEL  
	char resp[200];
    char *p0;    
/*
    响应:	
    STATUS:<stat>
    +CIPSTATUS:<link ID>,<type>,<remote	IP>,<remote	port>,<local port>,<tetype>
*/
	ret=wifi_at_cmd_sndrcv("AT+CIPSTATUS\r\n", "OK", 1*1000, 1, 1, resp, sizeof(resp)-1);
	if(ret)
		return ret;
	p0=strstr(resp, "STATUS:");
	if(p0==NULL)
		return -1;
	p0+=strlen("STATUS:");
	//p1=strstr(p0, "\r\n");
	//if(p1)
	//	*p1=0;
    status=atoi(p0);
    dbg("wifi status:%d\n", status);
#else
/*
wifi_read [176] = [[STA mode] 
mac	:[b4:fb:e3:77:19:2a] 
status	:[connect]
ssid	:[JSD] 
bssid	:[54:75:95:50:9f:ff]
channel :6 
rssi    :-32 
pwd     :[23609898] 
enctype :WPA_WPA2_PSK  
]
*/
    //ret=wifi_at_cmd_sndrcv("AT+WIFI_STATUS\r\n", "OK", 2*1000, 1, 1, resp, sizeof(resp)-1);
	//if(ret)
	//	return ret;
    
    //ret=wifi_at_cmd_sndrcv("AT+SOCKETSTATUS\r\n", "\"+SOCKETSTATUS\":", 500, 1, 1, resp, sizeof(resp)-1);
	//if(ret)
	//	return ret;
    ret=wifi_at_QueryAPConnStatus(NULL, NULL);
    switch(ret)
    {
        //1-已连接指定AP 0-未连接AP -1:已连接其它AP -2:取信号强度错 -9:查询失败
        case 0:
            status=5;
            break;
        case 1:
            status=2;
            break;
        default:
            status=-1;
    }
#endif
	return status;
}

#endif

