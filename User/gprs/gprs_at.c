#include <string.h>
#include <stdio.h>
#include <time.h>
#include <mhscpu.h>
#include "serial.h"
#include "gprs.h"
#include "debug.h"
#include "gprs_at.h"
#include "systick.h"
#include "ProjectConfig.h"
//#define dbg(x...)		0
#ifndef USE_LTE_4G
#define GPRS_AT_RSP_ERR "ERROR"

void gprs_lcdDebug(char *szSnd, char *szRcv)
{
#ifdef GPRS_AT_DEBUG_ON_LCD
	LcdClear();

	LcdPutsl("List GPRS Snd Buffer", 1);
	LcdPutsl(szSnd, 2);

	LcdPutsl("List GPRS Rcv Buffer", 5);
	LcdPutsl(szRcv, 6);
#endif
}

//清gprs串口接收缓存
void gprs_clear(void)
{
	uint8_t ch;
	int flag = 1; // -1:不打印clear日志；1:打印日志(有clear数据的时候才打印起始终止行)

	while (gprs_read((char *)&ch, 1, 80) > 0)
	{
		if (flag >= 0)
		{
			if (flag == 1)
			{
				flag = 0;
				dbg("gprs at clear...\n");
			}
			dbg("clr ch:%c=%02X\n", ch, ch);
		}
	}
	if (flag == 0)
		dbg("gprs at clear end.\n");
}

//发送gprs at指令和接收响应（tcp数据接收不适用)
//	pszReq	发送的指令
//  pszCheckRsp		指令执行成功判断字
//  iTimeOut		超时时间(ms),若为0则默认30*1000ms
//  icrflag			是否按行读取判断,少部分指令需设为0
//  iclearflag		发送指令前是否清空串口
//	pszOutRsp		输出数据缓冲区,为NULL则不输出数据
//  iOutSize		输出数据缓冲区大小,为0则不输出数据
// ret:  0:成功  -1:error   -2:timeout
int gprs_at_cmd_sndrcv(char *pszReq, char *pszCheckRsp, int iTimeOut, int icrflag, int iclearflag, char *pszOutRsp, int iOutSize)
{
	int ret, ret2;
	char *pszOut;
	int iMaxLen = (iOutSize > 0) ? iOutSize : 100;
	char szRspTmp[iMaxLen + 1];
	int iReTry = 1;
	tick tmstart;
	//	char *pTmp;

	if (iclearflag)
		gprs_clear();

	if (iTimeOut <= 0)
		iTimeOut = 15 * 1000;

	ret2 = -2;
	szRspTmp[0] = 0;
	while (iReTry)
	{
        mdelay(1);
		iReTry = 0;
		ret = gprs_write(pszReq, strlen(pszReq));
		dbg("gprs_write[%d] = %s\n", ret, pszReq);

		pszOut = szRspTmp;
		tmstart = get_tick();
		while (get_tick() < tmstart + iTimeOut)
		{
			ret = gprs_read(pszOut, 1, 100);
			if (ret <= 0)
			{
				/*
				//读取数据指令收到"+IPD"时,退出重发指令
				if(memcmp(pszReq, "AT+CIPRECVDATA=", 15)==0 && strstr(szRspTmp, "+IPD,"))
				{
					dbg("rcv:[%.100s], retry to recv...\n", szRspTmp); 
					iReTry=1;
					break;
				}else */
				continue;
			}
			//dbg("rcv ch:%c=%02X\n", pszOut[0], pszOut[0]);
			if (icrflag && pszOut[0] != 0x0A) //判断\r\n的0A
			{
				++pszOut;
				continue;
			}
			pszOut[1] = 0x00;
			++pszOut;

			//接收到一行响应，开始判断
			if (strstr((char *)szRspTmp, (char *)pszCheckRsp))
			{
				ret2 = 0;
				break;
			}

			if (strstr((char *)szRspTmp, "+QIRDI: 0,1,0"))
			{
				dbg("rcv:[%s], retry to recv...\n", szRspTmp);
				gprs_clear();
				memset(szRspTmp, 0, sizeof(szRspTmp));
				iReTry = 1;
				break;
			}

			/*
			if(strstr((char*)szRspTmp, WIFI_AT_RSP_BUSYP) || strstr((char*)szRspTmp, WIFI_AT_RSP_BUSYS))
			{
				dbg("gprs busy... retry\n");
				szRspTmp[0]=0;
				pszOut=szRspTmp;
				mdelay(500);
				gprs_clear();
				iReTry=1;
				break;
			}
			*/
			if (strstr((char *)szRspTmp, GPRS_AT_RSP_ERR))
			{
				ret2 = -1;
				break;
			}

			ret = -2;
			continue;
		}
	}
	if (pszOutRsp && iOutSize > 0)
		strcpy((char *)pszOutRsp, (char *)szRspTmp);

	dbg("gprs_read [%d] = [%s], ret2=%d\n", strlen(szRspTmp), szRspTmp, ret2);
	//hexdump(pszOut, ret);

	return ret2;
}

int gprs_at_poweron(void)
{
	gprs_poweron();

	if (gprs_at_MoudleIsActive() == 0)
	{
		dbg("Need gprs_modulepin_reset()\n");
		gprs_modulepin_reset();
		if (gprs_at_MoudleIsActive() == 0)
		{
			dbg("Need gprs_modulepin_reset()\n");
			gprs_modulepin_reset();
			if (gprs_at_MoudleIsActive() == 0)
			{
				dbg("gprs_modulepin_reset() can't work\n");
				//gprs_lcdDebug("AT", "No response");
				return -99;
			}
		}
		mdelay(5 * 1000);
	}
	else
	{
		dbg("No Need gprs_hardware_reset()\n");
	}
	return 0;
}

int gprs_at_MoudleIsActive(void)
{
	int ret;

    ret = gprs_at_cmd_sndrcv("AT\r", "OK\r", 300, 1, 1, NULL, 0);
	ret = (ret == 0) ? 1 : 0;

	dbg("gprs_AT_MoudleIsActive return %d\n", ret);
	return ret;
}

int gprs_at_CheckSIM(void)
{
	char recv[100];
	int ret;
    int loop=0;
    
    do{
        if(loop)
            mdelay(100);
        ret = gprs_at_cmd_sndrcv("AT+CPIN?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv) - 1);
        if((ret==-1 && loop>3))      //-1:error
		{
            break;
		}
    }while (ret != 0 && ++loop <= 10);
    dbg("gprs_at_CheckSIM return %d\n", ret);
	if (ret == 0)
	{
		if(strstr(recv, "+CPIN: READY"))
			return 0;
		if(strstr(recv, "+CPIN: NO SIM"))
			return -100;
		return -2;
	}else
		return -1;
}

int gprs_at_CheckSIMQuick(void)
{
	char recv[100];
	int ret;

    ret = gprs_at_cmd_sndrcv("AT+CPIN?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv) - 1);
	if (ret == 0)
	{
		if(strstr(recv, "+CPIN: READY"))
			return 0;
		if(strstr(recv, "+CPIN: NO SIM") || strstr(recv, "+CME ERROR: 10"))
			return -100;
		return -2;
	}else
	{
		if(strstr(recv, "+CME ERROR: 10"))
			return -100;
		return -1;
	}
}

int gprs_at_CheckSIM2(char *siminfo)
{
	char recv[100];
	int ret;
    int loop=0;
    
	siminfo[0]=0;
    do{
        if(loop)
            mdelay(100);
        ret = gprs_at_cmd_sndrcv("AT+CPIN?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv) - 1);
        if((ret==-1 && loop>2))      //-1:error
		{
            break;
		}
    }while (ret != 0 && ++loop <= 10);
    dbg("gprs_at_CheckSIM return %d\n", ret);
	if (ret == 0)
	{
		char *p, *p1;
		p=strstr(recv, "+CPIN:");
		if(p)
		{
			p1=strstr(p, "\r");
			if(p1)
				*p1=0;
			strncpy(siminfo, p, 15);
		}
		return 0;
	}else
		return -1;
}


int gprs_at_GetSignalQuality(void)
{
    char recv[500];
    int ret;
    char *pTmp;
    int signalQuality;
    int loop=0;
    char *p;
    int i;
    int rsrp, sinr;

#if 0
    do{
        if(loop)
            mdelay(100);
    ret = gprs_at_cmd_sndrcv("AT+CSQ\r", "OK\r", 500, 1, 1, recv, sizeof(recv) - 1);
    }while (ret != 0 && ++loop <= 2);
    if (ret == 0 && (pTmp = strstr(recv, "+CSQ: ")) != NULL)
    {
    sscanf(pTmp, "%*s%d,%*d", &signalQuality);
    dbg("signalQuality = %d\n", signalQuality);     //<rssi> 0：-110db   1-30：……     31：-48db 
    if(signalQuality>=99)
    signalQuality=0;
    return signalQuality*100/31;            //转为百分比，<16:1格 16-32:2格 32-40:3格 >=40:full     
    }
#else
    do{
    ret = gprs_at_cmd_sndrcv("AT+QENG=\"servingcell\"\r", "OK\r", 2000, 1, 1, recv, sizeof(recv) - 1);
    if(ret && ret!=-2)
    mdelay(500);
    }while (ret != 0 && ++loop <= 2);
    if (ret == 0 && (pTmp = strstr(recv, "LTE")) != NULL)
    {
    //+QENG:"servingcell",<state>,"LTE",<is_tdd>,<mcc>,<mnc>,<cellid>,<pcid>,<earfcn>,<freq_band_ind>,<ul_bandwidth>,<dl_bandwidth>,<tac>,<rsrp>,<rsrq>,<rssi>,<sinr>,<srxlev>
    //+QENG: "servingcell","NOCONN","LTE","TDD",460,00,CBB7281,15,38950,40,5,5,2638,-88,-6,-85,28,38
    p=strchr(pTmp, '\r');
    if(p==NULL)
    return 0;
    sinr=0;
    rsrp=0;
    *p=0;
    for(i=0; i<5; i++)
    {
        p=strrchr(pTmp, ',');
        if(p==NULL)
        return 0;
        if(i==1) //倒数第2个','后面跟sinr
        sinr=atoi(p+1);
        if(i==4) //倒数第5个','后面跟rsrp
        rsrp=atoi(p+1);
        *p=0;
    }
    signalQuality=0;
    if(rsrp<0)
    {
        /*
        极好点： RSRP>-85dBm； SINR>25
        好点： RSRP=-85～-95dBm；SINR:16-25
        中点： RSRP=-95～-105dBm；SINR:11-15
        差点： RSRP=-105～-115dBm；SINR:3-10
        极差点： RSRP<-115dB；SINR<3
        */
        //转为百分比，<16:1格 16-32:2格 32-40:3格 >=40:full
        signalQuality=(rsrp+140)*0.9;
    }
    return signalQuality;
    }
#endif
    dbg("gprs_at_CheckSignalQuality return -1\n");
    return 0;
}


int gprs_at_CheckRegister(void)
{
	char recv[100];
	int ret;
	int loop = 0;
    
    gprs_at_cmd_sndrcv("AT+CREG=0\r", "OK\r", 500, 1, 1, NULL, 0);
    
	do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+CREG?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv) - 1);	
		if (ret == 0 && (strstr(recv, "+CREG: 0,1")||strstr(recv, "+CREG: 0,5")))		//1或5表示已注册,1本地 5漫游
			ret = 0;
		else
			ret = -1;
	} while (ret < 0 && ++loop <= 15);
	dbg("gprs_at_CheckRegister return %d\n", ret);
	return ret;
}

//输出: MCC,MNC,LAC/TAC(16进制),CI(16进制)
int gprs_at_GetCellInfo(char *pszCellInfo)
{
	char recv[300];
	int ret;
	char *p0, *p1, *p2;
    
	pszCellInfo[0]=0;

	//开工程模式
	ret = gprs_at_cmd_sndrcv("AT+QENG=1,0\r", "OK\r", 500, 1, 1, NULL, 0);	
	if (ret)
		return ret;
	//取工程模式信息
	ret = gprs_at_cmd_sndrcv("AT+QENG?\r", "OK\r", 2000, 1, 1, recv, sizeof(recv) - 1);
	//还原设置
	gprs_at_cmd_sndrcv("AT+QENG=0\r", "OK\r", 500, 1, 1, NULL, 0);

	if(ret==0)
	{
		ret=1;
		p0=recv;
		p1=strstr(p0, "+QENG: 0,");		//基站列表第一条
		if(p1)
		{
			//格式: +QENG: 0,<mcc>,<mnc>,<lac>,<cellid>,<bcch>,<bsic>,<dbm>......
			//ex:   +QENG: 0,460,00,2623,e25,49,14,-68,40,40,5,6,x,x,x,x,x,x,x
			//取第四个和第五个逗号前的信息
			p0=p1+strlen("+QENG: 0,");
			p2=p0;		//起始位置

			//第二逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			p0=p1+1;

			//第三逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			p0=p1+1;

			//第四逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			p0=p1+1;
			
			//第五逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			*p1=0;			//截止位置
			strcpy(pszCellInfo, p2);

			dbg("cellinfo=[%s]\n", pszCellInfo);
			
			ret=0;
		}
	}
	return ret;
}
// 14.4 将二进制源串分解成双倍长度可读的16进制串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
static void vOneTwo(const unsigned char *psIn, int iLength, unsigned char *psOut)
{
    static const unsigned char aucHexToChar[17] = "0123456789ABCDEF";
    int iCounter;

    for (iCounter = 0; iCounter < iLength; iCounter++)
    {
        psOut[2 * iCounter] = aucHexToChar[(psIn[iCounter] >> 4)];
        psOut[2 * iCounter + 1] = aucHexToChar[(psIn[iCounter] & 0x0F)];
    }
}

// 14.5 将二进制源串分解成双倍长度可读的16进制串, 并在末尾添'\0'
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : pszOut   : 目标串
static void vOneTwo0(const unsigned char *psIn, int iLength, unsigned char *pszOut)
{
    vOneTwo(psIn, iLength, pszOut);
    if (iLength < 0)
        iLength = 0;
    pszOut[2 * iLength] = 0;
}
/*
static unsigned char toupper(unsigned char c)
{
    if (c >= 'a' && c <= 'z')
        c -= 0x20;
    return (c);
}
*/

// 14.6 将可读的16进制表示串压缩成其一半长度的二进制串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
// Attention: 源串必须为合法的十六进制表示，大小写均可
//            长度如果为奇数，函数会靠近到比它大一位的偶数
static void vTwoOne(const unsigned char *psIn, int iLength, unsigned char *psOut)
{
    unsigned char ucTmp;
    int i;

    for (i = 0; i < iLength; i += 2)
    {
        ucTmp = psIn[i];
        if (ucTmp > '9')
            ucTmp = toupper(ucTmp) - 'A' + 0x0a;
        else
            ucTmp &= 0x0f;
        psOut[i / 2] = ucTmp << 4;

        ucTmp = psIn[i + 1];
        if (ucTmp > '9')
            ucTmp = toupper(ucTmp) - 'A' + 0x0a;
        else
            ucTmp &= 0x0f;
        psOut[i / 2] += ucTmp;
    } // for(i=0; i<uiLength; i+=2) {
}

//取基站信息,输出固定为5字节长的10进制数,不足位数前补0
int gprs_at_GetLACandCI(char *pszLAC, char *pszCellId)
{
	char recv[200];
	int ret;
	char *p0, *p1;
    
	pszLAC[0]=0;
	pszCellId[0]=0;

	//开工程模式
	ret = gprs_at_cmd_sndrcv("AT+QENG=1,0\r", "OK\r", 500, 1, 1, NULL, 0);	
	if (ret)
		return ret;
	//取工程模式信息
	ret = gprs_at_cmd_sndrcv("AT+QENG?\r", "OK\r", 2000, 1, 1, recv, sizeof(recv) - 1);
	//还原设置
	gprs_at_cmd_sndrcv("AT+QENG=0\r", "OK\r", 500, 1, 1, NULL, 0);

	if(ret==0)
	{
		ret=1;
		p0=recv;
		p1=strstr(p0, "+QENG: 0,");		//基站列表第一条
		if(p1)
		{
			//格式: +QENG: 0,<mcc>,<mnc>,<lac>,<cellid>,<bcch>,<bsic>,<dbm>......
			//ex:   +QENG: 0,460,00,2623,e25,49,14,-68,40,40,5,6,x,x,x,x,x,x,x
			//取第四个和第五个逗号前的信息
			p0=p1+strlen("+QENG: 0,");

			//第二逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			p0=p1+1;

			//第三逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			p0=p1+1;

			//第四逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			*p1=0;
			strcpy(pszLAC, p0);
			p0=p1+1;
			
			//第五逗号
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			*p1=0;
			strcpy(pszCellId, p0);
			p0=p1+1;

			dbg("lac=[%s], cellid=[%s]\n", pszLAC, pszCellId);
			
			ret=0;
		}
	}

#if 0
	//先设置2要求返回小区信息
	ret = gprs_at_cmd_sndrcv("AT+CGREG=2\r", "OK\r", 500, 1, 1, NULL, 0);	
	if (ret)
		return ret;
	//取基站信息
	ret = gprs_at_cmd_sndrcv("AT+CGREG?\r", "+CGREG:", 2000, 1, 1, recv, sizeof(recv) - 1);
	//还原设置
	gprs_at_cmd_sndrcv("AT+CGREG=0\r", "OK\r", 500, 1, 1, NULL, 0);

	if(ret==0)
	{
		ret=1;

		//格式:+CGREG: mode,state,lac,cellid.  ex: +CGREG: 2,1,"2623","E25"
		p0=recv;
		p1=strstr(p0, "+CGREG: 2,");
		if(p1)
		{
			p0=p1+strlen("+CGREG: 2,");
			p1=strchr(p0, 0x0d);
			if(p1)
				*p1=0;
			
			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			p0=p1+1;

			p1=strchr(p0, ',');
			if(p1==NULL)
				return 2;
			*p1=0;
			dbg("lac=[%s]", p0);
			p0=p1+1;

			p1=strchr(p0, ',');
			if(p1)
			{
				*p1=0;
			}
			dbg("cellid=[%s]", p0);

			ret=0;
		}
	}
#endif

	//4位长16进制字符串,转为5位长的10进制数(最大65535)
	if(ret==0)
	{
		unsigned char tmp[10];
		if(strlen(pszLAC)<4)
		{
			strcpy(recv, "0000");
			strcpy(recv+4-strlen(pszLAC), pszLAC);
		}else
			strcpy(recv, pszLAC);
		vTwoOne(recv, 4, tmp);
		sprintf(pszLAC, "%05u", tmp[0]*256+tmp[1]);

		if(strlen(pszCellId)<4)
		{
			strcpy(recv, "0000");
			strcpy(recv+4-strlen(pszCellId), pszCellId);
		}else
			strcpy(recv, pszCellId);
		vTwoOne(recv, 4, tmp);
		sprintf(pszCellId, "%05u", tmp[0]*256+tmp[1]);		
	}
	dbg("out lac=[%s], cellid=[%s]\n", pszLAC, pszCellId);
	return ret;
}

int gprs_at_CheckGprsAttachment(void)
{
	char recv[100];
	int ret;
	int loop = 0;

	do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+CGATT?\r", "OK\r", 2000, 1, 1, recv, sizeof(recv) - 1);	
		if (ret == 0 && strstr(recv, "+CGATT: 1"))
			ret = 0;
		else
			ret = -1;
	} while (ret < 0 && ++loop <= 20);
	dbg("gprs_at_CheckGprsAttachment return %d\n", ret);
	return ret;
}

//设置apn：可三个参数同时为NULL(默认CMNET);若有一个非NULL,则另两个参数不可为NULL(可为"")
int gprs_at_SetAPN(char *apn, char *user, char *pwd)
{
    char send[50];
	int ret;
	int loop = 0;

    if((apn==NULL || apn[0]==0) && (user==NULL || user[0]==0) && (pwd==NULL || pwd[0]==0))
        sprintf(send, "AT+QICSGP=1,\"%s\"\r", "CMNET");
    else
        sprintf(send, "AT+QICSGP=1,\"%s\",\"%s\",\"%s\"\r", apn, user, pwd);
	do
	{    
        //ret = gprs_at_cmd_sndrcv("AT+QICSGP=1,\"UNINET\"\r", "OK\r", 2000, 1, 1, NULL, 0);
        //ret = gprs_at_cmd_sndrcv("AT+QICSGP=1,\"CMMTM\"\r", "OK\r", 2000, 1, 1, NULL, 0);                 //2G移动物联网
        ret = gprs_at_cmd_sndrcv(send, "OK\r", 2000, 1, 1, NULL, 0);
        if(ret==-2)
            mdelay(200);
    } while (ret < 0 && ++loop <= 3);
	dbg("gprs_at_SetAPN return %d\n", ret);
	return ret;
}

int gprs_at_SetHead(void)
{
	int ret;
	ret = gprs_at_cmd_sndrcv("AT+QIHEAD=1\r", "OK\r", 500, 1, 1, NULL, 0);
	dbg("gprs_at_SetHead return %d\n", ret);
	return ret;
}
int gprs_at_SetRecvMode(void)
{
	int ret;
	ret = gprs_at_cmd_sndrcv("AT+QINDI=1\r", "OK\r", 500, 1, 1, NULL, 0);
	dbg("gprs_at_SetRecvMode return %d\n", ret);
	return ret;
}

int gprs_at_BuildGprsLink(void)
{
	int ret;
	ret = gprs_at_cmd_sndrcv("AT+CIICR\r", "OK\r", 1000, 1, 1, NULL, 0);
	dbg("gprs_at_BuildGprsLink return %d\n", ret);
	return ret;
}

int gprs_at_CloseGprs(void)
{
	int ret;
	ret = gprs_at_cmd_sndrcv("AT+QIDEACT\r", "DEACT OK", 10*1000, 1, 1, NULL, 0);
	dbg("gprs_at_CloseGprs return %d\n", ret);
	return 0;
}

int gprs_at_TcpConnectHost(char *hostIp, char *hostPort)
{
	char send[100];
	char recv[100];
	int ret;

	sprintf(send, "%s\"%s\",%s\r", "AT+QIOPEN=\"TCP\",", hostIp, hostPort);
	ret = gprs_at_cmd_sndrcv(send, "CONNECT ", 10 * 1000, 1, 1, recv, sizeof(recv) - 1);
	if (ret == 0 && strstr(recv, "CONNECT OK"))
		ret = 0;
	else
		ret = -1;
	dbg("gprs_at_TcpConnectHost return %d\n", ret);
	return ret;
}

int gprs_at_TcpSnd(unsigned char *sndData, int sndLen)
{
	char send[100];
	int ret;

	if (sndLen > GPRS_AT_TCP_SND_BUFFER_MAX_SIZE || NULL == sndData || sndLen <= 0)
	{
		return -3;
	}

	sprintf(send, "%s%d\r", "AT+QISEND=", sndLen);
	ret = gprs_at_cmd_sndrcv(send, ">", 1500, 0, 1, NULL, 0);
	if (ret != 0)
	{
		dbg("gprs_at_cmd_sndrcv err, ret=%d", ret);
		return ret;
	}

	ret = gprs_write((char *)sndData, sndLen);
	if (ret <= 0)
	{
		dbg("gprs_write err, ret=%d", ret);
		return -1;
	}

	{
		char szRspTmp[100];
		char *pszOut;
		tick tmstart;
		int iTimeOut = 3 * 1000;

		ret = -2;
		tmstart = get_tick();
		pszOut = szRspTmp;
		while (get_tick() < tmstart + iTimeOut && (pszOut - szRspTmp < sizeof(szRspTmp) - 1))
		{
			ret = gprs_read(pszOut, 1, 100);
			if (ret <= 0)
				continue;
			dbg("ch:%c=%02X\n", pszOut[0], pszOut[0]);
			if (pszOut[0] != 0x0A) //判断\r\n的0A
			{
				++pszOut;
				continue;
			}
			pszOut[1] = 0x00;
			++pszOut;

			//接收到一行响应，开始判断
			if (strstr(szRspTmp, "SEND OK"))
			{
				ret = sndLen;
				break;
			}
			if (strstr(szRspTmp, "SEND FAIL") || strstr(szRspTmp, "ERROR"))
			{
				ret = -1;
				break;
			}
			ret = -2;
		}
	}

	dbg("gprs_at_TcpSnd return %d\n", ret);
	return ret;
}

int gprs_at_TcpRcv(unsigned char *rcvData, int expRcvLen, int timeoutMs)
{
	char send[100];
	char recv[100];
	int ret, rcvLen = 0, len = 0;
	char *p0;

	if (timeoutMs <= 0)
		timeoutMs = 20 * 1000;

	sprintf(send, "%s=0,1,0,%d\r", "AT+QIRD", expRcvLen);

	ret = gprs_at_cmd_sndrcv(send, "+QIRD:", timeoutMs, 1, 1, recv, sizeof(recv) - 1);
	if (ret != 0)
	{
		//dbg("recv [%02X %02X]\n", recv[0], recv[1]);
		dbg("gprs_at_cmd_sndrcv error:%d\n", ret);
		return -1;
	}

	p0 = strrchr(recv, ',');
	if (p0 == NULL)
	{
		dbg("gprs recv err.\n");
		return -1;
	}

	rcvLen = atoi(p0 + 1);
	if (rcvLen > expRcvLen)
	{
		dbg("revLen > expRcvLen, err.\n");
		return -1;
	}

	p0 = (char *)rcvData;
	len = rcvLen;
	//收取包内数据
	while (len > 0)
	{
		ret = gprs_read(p0, len, 800);
		if (ret <= 0)
		{
			dbg("gprs_read error:%d\n", ret);
			return -1;
		}
		p0 += ret;
		len -= ret;
	}

	dbg("gprs_at_TcpRcv return %d\n", rcvLen);
	return rcvLen;
}

int gprs_at_TcpDisconnectHost()
{
	int ret;

	ret = gprs_at_cmd_sndrcv("AT+QICLOSE\r", "CLOSE OK", 500, 1, 1, NULL, 0);

	dbg("gprs_at_TcpDisconnectHost return %d\n", ret);
	return ret;
}

int gprs_at_OfflineOnOff(int offlineOnOff)
{
	char send[100];
	char recv[100];
	char correctRsp[100];
	int ret;
	int loop = 0;

	if (offlineOnOff == GPRS_AT_OFFLINE_ON)
	{
		strcpy(send, "AT+CIFUN=0\r");
	}
	else
	{
		strcpy(send, "AT+CIFUN=1\r");
	}
	strcpy(correctRsp, "OK");

	while (loop++ < 1)
	{
		ret = gprs_write(send, strlen(send));
		dbg("gprs_write[%d] = %s\n", ret, send);
		ret = gprs_read(recv, 100, 600);
		recv[ret] = 0x00;
		dbg("gprs_read [%d] = [%s]\n", ret, recv);
		if (strstr(recv, correctRsp) != NULL)
		{
			dbg("gprs_at_OfflineOnOff return 0\n");
			return 0;
		}
	}
	dbg("gprs_at_OfflineOnOff return -1\n");
	return -1;
}

int gprs_at_DisableEcho(void)
{
	int ret;

	ret = gprs_at_cmd_sndrcv("ATE0\r", "OK", 300, 1, 1, NULL, 0);

	dbg("gprs_at_DisableEcho return %d\n", ret);
	return ret;
}

int gprs_at_PowerOff()
{
	int ret;

	ret = gprs_at_cmd_sndrcv("AT+QPOWD=1\r", "NORMAL POWER DOWN", 500, 1, 1, NULL, 0);

	dbg("gprs_at_PowerOff return %d\n", ret);
	return ret;
}

int gprs_at_GetAutoAnswer(void)
{
	char send[100];
	char recv[120];
	char correctRsp[100];
	int ret;
	int loop = 0;

	strcpy(send, "ATS0?\r");
	strcpy(correctRsp, "OK");

	while (loop++ < 10)
	{
		ret = gprs_write(send, strlen(send));
		dbg("gprs_write[%d] = %s\n", ret, send);
		ret = gprs_read(recv, 100, 5 * 1000);
		recv[ret] = 0x00;
		dbg("gprs_read [%d] = [%s]\n", ret, recv);
		if (strstr(recv, correctRsp) != NULL)
		{
			dbg("gprs_at_GetAutoAnswer return 0\n");
			return 0;
		}
	}
	dbg("gprs_at_GetAutoAnswer return -1\n");
	return -1;
}

int gprs_at_SetAutoAnswer(void)
{
	char send[100];
	char recv[120];
	char correctRsp[100];
	int ret;
	int loop = 0;

	strcpy(send, "ATS0=1\r");
	strcpy(correctRsp, "OK");

	while (loop++ < 10)
	{
		ret = gprs_write(send, strlen(send));
		dbg("gprs_write[%d] = %s\n", ret, send);
		ret = gprs_read(recv, 100, 5 * 1000);
		recv[ret] = 0x00;
		dbg("gprs_read [%d] = [%s]\n", ret, recv);
		if (strstr(recv, correctRsp) != NULL)
		{
			dbg("gprs_at_SetAutoAnswer return 0\n");
			return 0;
		}
	}
	dbg("gprs_at_SetAutoAnswer return -1\n");
	return -1;
}

int gprs_at_save(void)
{
	char send[100];
	char recv[120];
	char correctRsp[100];
	int ret;
	int loop = 0;

	strcpy(send, "AT&W\r");
	strcpy(correctRsp, "OK");

	while (loop++ < 10)
	{
		ret = gprs_write(send, strlen(send));
		dbg("gprs_write[%d] = %s\n", ret, send);
		ret = gprs_read(recv, 100, 5 * 1000);
		recv[ret] = 0x00;
		dbg("gprs_read [%d] = [%s]\n", ret, recv);
		if (strstr(recv, correctRsp) != NULL)
		{
			dbg("gprs_at_PowerOff return 0\n");
			return 0;
		}
	}
	dbg("gprs_at_PowerOff return -1\n");
	return -1;
}

int gprs_at_info(void)
{
	char send[100];
	char recv[120];
	char correctRsp[100];
	int ret;
	int loop = 0;

	strcpy(send, "ATI\r");
	strcpy(correctRsp, "OK");

	while (loop++ < 10)
	{
		ret = gprs_write(send, strlen(send));
		dbg("gprs_write[%d] = %s\n", ret, send);
		ret = gprs_read(recv, 100, 5 * 1000);
		recv[ret] = 0x00;
		dbg("gprs_read [%d] = [%s]\n", ret, recv);
		if (strstr(recv, correctRsp) != NULL)
		{
			dbg("gprs_at_PowerOff return 0\n");
			return 0;
		}
	}

	gprs_lcdDebug(send, recv);
	dbg("gprs_at_PowerOff return -1\n");
	return -1;
}

int gprs_at_Config_Cur_Context()
{
	int ret;

	ret = gprs_at_cmd_sndrcv("AT+QIFGCNT=0\r", "OK", 1000, 1, 1, NULL, 0);

	dbg("gprs_at_Config_Cur_Context return %d\n", ret);
	return ret;
}

int gprs_at_InitPDP(void)
{
	int ret = 0;
	int loop;
    char szTmp[100];

	loop = 0;
	do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+QIREGAPP\r", "OK", 1000, 1, 1, NULL, 0);
        if(ret==-1 && loop>=1)
        {
            //重复regapp会返回“+CME ERROR: 3”， ret=-1表明收到ERROR,需检查stat
            szTmp[0]=0;
            gprs_at_connState(szTmp);
            if(szTmp[0] && strstr(szTmp, "INITIAL")==0) //非INITIAL为已注册
            {
                ret=0;
                break;
            }
        }
	} while (ret < 0 && ++loop <= 10);
	if (ret)
	{
		dbg("AT+QIREGAPP error:%d\n", ret);
		return ret;
	}
	
	ret=gprs_at_actgprs(1, 10);
	if (ret)
	{
		return ret;
	}
    
    /*
	ret = gprs_at_cmd_sndrcv("AT+QILOCIP\r", ".", 0, 1, 1, NULL, 0);
	if (ret)
	{
		dbg("AT+QILOCIP error:%d\n", ret);
		//return ret;
	}
    */
    
	return ret;
}

int gprs_at_RegApp(void)
{
	int ret = 0;
	int loop;
    char szTmp[100];

	loop = 0;
	do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+QIREGAPP\r", "OK", 1000, 1, 1, NULL, 0);
        if(ret==-1 && loop>=1)
        {
            //重复regapp会返回“+CME ERROR: 3”， ret=-1表明收到ERROR,需检查stat
            szTmp[0]=0;
            gprs_at_connState(szTmp);
            if(szTmp[0] && strstr(szTmp, "INITIAL")==0) //非INITIAL为已注册
            {
                ret=0;
                break;
            }
        }
	} while (ret < 0 && ++loop <= 10);
	if (ret)
	{
		dbg("AT+QIREGAPP error:%d\n", ret);
	}
    
	return ret;
}

int gprs_at_actgprs(char preChkStat, int loopMax)
{
	int ret = 0;
	int loop;
    char szTmp[100];

	if(loopMax<=0)
		loopMax=10;

	if(preChkStat)
	{
		szTmp[0]=0;
		gprs_at_connState(szTmp);
		if(strstr(szTmp, "GPRSACT") || strstr(szTmp, "IP CLOSE") || strstr(szTmp, "IP CONFIG") || strstr(szTmp, "IP STATUS"))
		{
			//已act,不需要QIACT指令
			return 0;
		}
	}

	loop = 0;
	do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+QIACT\r", "OK", 2000, 1, 1, NULL, 0);
		if(ret==-1 && loop>=2)
        {
            //重复regapp会返回“+CME ERROR: 3”， ret=-1表明收到ERROR,需检查stat
            szTmp[0]=0;
            gprs_at_connState(szTmp);			
			if(strstr(szTmp, "GPRSACT") || strstr(szTmp, "IP CLOSE") || strstr(szTmp, "IP CONFIG") || strstr(szTmp, "IP STATUS"))
			{
				//已act,不需要QIACT指令
                ret=0;
                break;
            }
        }
	} while (ret < 0 && ++loop <= loopMax);
	if (ret)
		dbg("AT+QIACT error:%d\n", ret);
	return ret;
}

int gprs_at_Tcp_RcvBuf_OnOff(int OnOff)
{
	char send[100];
	char recv[120];
	char correctRsp[100];
	int ret;
	int loop = 0;

	sprintf(send, "AT+QINDI=%d\r", OnOff);
	strcpy(correctRsp, "OK");

	while (loop++ < 10)
	{
		ret = gprs_write(send, strlen(send));
		dbg("gprs_write[%d] = %s\n", ret, send);
		ret = gprs_read(recv, 100, 5 * 1000);
		recv[ret] = 0x00;
		dbg("gprs_read [%d] = [%s]\n", ret, recv);
		if (strstr(recv, correctRsp) != NULL)
		{
			dbg("gprs_at_Config_Cur_Context return 0\n");
			return 0;
		}
	}

	dbg("gprs_at_Config_Cur_Context return -1\n");
	return -1;
}

int gprs_at_location(char *szLocation)
{
	char recv[120];
	char correctRsp[30];
	char *pTmp, *pTmp2;
	int ret;

	ret = gprs_at_cmd_sndrcv("AT+QCELLLOC=1\r", "OK", 10*1000, 1, 1, recv, sizeof(recv) - 1);
	if (ret)
	{
		dbg("AT+QCELLLOC error:%d\n", ret);
		return ret;
	}

	strcpy(correctRsp, "+QCELLLOC: ");
	if ((pTmp = strstr(recv, correctRsp)) != NULL)
	{
		pTmp += strlen(correctRsp);
		pTmp2 = strstr(pTmp, "\r");
		if (pTmp2 != NULL)
		{
			*pTmp2 = 0;
			if (szLocation != NULL)
			{
				strcpy(szLocation, pTmp);
			}
			dbg("gprs_at_location return 0, Location=[%s]\n", pTmp);
			return 0;
		}
	}

	dbg("gprs_at_location return -1\n");
	return -1;
}

int rtcStrToStructTm(char *szDateTime, struct tm *tmDateTime)
{
	int i;
	i = 0;
	tmDateTime->tm_year = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0') + 2000 - 1900; //1900 年开始计数
	i += 3;
	tmDateTime->tm_mon = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0') - 1; //0~11表示1~12个月
	i += 3;
	tmDateTime->tm_mday = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0');
	i += 3;
	tmDateTime->tm_hour = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0');
	i += 3;
	tmDateTime->tm_min = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0');
	i += 3;
	tmDateTime->tm_sec = (szDateTime[i] - '0') * 10 + (szDateTime[i + 1] - '0');

	dbg("tmDateTime:%d, %d, %d, %d, %d, %d\n",
		tmDateTime->tm_year, tmDateTime->tm_mon, tmDateTime->tm_mday,
		tmDateTime->tm_hour, tmDateTime->tm_min, tmDateTime->tm_sec);
	return 0;
}

static int cclk_first=1;
int gprs_at_get_cclk(struct tm *tmDateTime)
{
	char recv[100];
	char *pRtcStartStr;
	int ret;
	int loop;
    
#if 1
    if(cclk_first)
    {
        loop = 0;
        do
        {
            ret = gprs_at_cmd_sndrcv("AT+QNITZ=1\r", "OK", 3 * 1000, 1, 1, NULL, 0);
        } while (ret != 0 && ++loop <= 5);
        if (ret)
        {
            dbg("AT+QNITZ=1 error:%d\n", ret);
            return ret;
        }

        loop = 0;
        do
        {
            ret = gprs_at_cmd_sndrcv("AT+CTZU=3\r", "OK", 3 * 1000, 1, 1, NULL, 0);
        } while (ret != 0 && ++loop <= 5);
        if (ret)
        {
            dbg("AT+CTZU=3 error:%d\n", ret);
            return ret;
        }else
            cclk_first=0;
    }
#endif
	loop = 0;
	do
	{
		ret = gprs_at_cmd_sndrcv("AT+CCLK?\r", "OK", 3 * 1000, 1, 1, recv, sizeof(recv) - 1);
	} while (ret != 0 && ++loop <= 5);
	if (ret)
	{
		dbg("AT+CCLK? error:%d\n", ret);
		return ret;
	}
	if (strstr(recv, "+CCLK: ") != NULL)
	{
		pRtcStartStr = strstr(recv, "\"");
		dbg("pRtcStartStr = [%s]\n", pRtcStartStr);
		rtcStrToStructTm(pRtcStartStr + 1, tmDateTime);
		return 0;
	}

	dbg("gprs_at_cclk return -1\n");
	return -1;
}

/*
int gprs_at_GetLocationFlow(char *szLocation)
{	
	int ret;
	//char rcvData[1024];
	struct tm rtcTm;
	int loop;

	gprs_poweron();
  	
	if (gprs_at_MoudleIsActive() == 0)
	{
		dbg("Need gprs_modulepin_reset()\n");
		gprs_modulepin_reset();
		if (gprs_at_MoudleIsActive() == 0)
		{
			dbg("Need gprs_modulepin_reset2()\n");
			gprs_modulepin_reset();
			if (gprs_at_MoudleIsActive() == 0)
			{
				dbg("gprs_modulepin_reset() can't work\n");
				gprs_lcdDebug("AT", "No response");
				return -9;
			}
		}
		mdelay(5*1000);
	}else
	{
		dbg("No Need gprs_hardware_reset()\n");
	}

	ret = gprs_at_DisableEcho();
	if (ret < 0)
	{
		dbg("gprs_at_DisableEcho error\n");
		return -1;
	}
	
	ret = gprs_at_CheckSIM();
	if (ret < 0)
	{
		dbg("gprs_at_CheckSIM error\n");
		return -1;
	}
	
	ret = gprs_at_GetSignalQuality();
	if (ret < 0)
	{
		dbg("gprs_at_GetSignalQuality error\n");
		return -2;
	}

	loop=0;
	do{
		ret = gprs_at_CheckRegister();
		if(ret)
			mdelay(200);
	}while(ret!=0 && ++loop<=10);
	if (ret < 0)
	{
		dbg("gprs_at_CheckRegister error\n");
		return -3;
	}

	loop=0;
	do{
		ret = gprs_at_CheckGprsAttachment();
		if(ret)
			mdelay(200);
	}while(ret!=0 && ++loop<=10);
	if (ret < 0)
	{
		dbg("gprs_at_CheckGprsAttachment error\n");
		return -4;
	}

	ret = gprs_at_SetAPN(NULL, NULL, NULL);
	if (ret < 0)
	{
		dbg("gprs_at_SetAPN error\n");
		return -5;
	}

	ret = gprs_at_InitPDP();
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
		return -6;
	}

	ret = gprs_at_get_cclk(&rtcTm);
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
		return -6;
	}

	ret = gprs_at_location(szLocation);
	if (ret < 0)
	{
		dbg("gprs_at_location error\n");
		return -6;
	}
	
	ret = gprs_at_CloseGprs();
	if (ret < 0)
	{
		dbg("gprs_at_CloseGprs error\n");
		return -8;
	}

	return 0;
}
*/

int gprs_at_test(void)
{
	int ret;
	unsigned char rcvData[1050 + 1];
	//	struct tm rtcTm;
	int loop, len;

	dbg("gprs_at_test\n");

	gprs_poweron();

	if (gprs_at_MoudleIsActive() == 0)
	{
		dbg("Need gprs_at_MoudleIsActive()\n");
		gprs_modulepin_reset();
		if (gprs_at_MoudleIsActive() == 0)
		{
			dbg("Need gprs_at_MoudleIsActive()\n");
			gprs_modulepin_reset();
			if (gprs_at_MoudleIsActive() == 0)
			{
				dbg("gprs_at_MoudleIsActive() can't work\n");
				gprs_lcdDebug("AT", "No response");
				return -9;
			}
		}

		mdelay(5 * 1000);
	}
	else
	{
		dbg("No Need gprs_modulepin_reset()\n");
	}

	ret = gprs_at_DisableEcho();
	if (ret < 0)
		return -1;

	ret = gprs_at_CheckSIM();
	if (ret < 0)
    {
		if(ret==-100)       //无SIM卡
            return -100;
        return -1;
    }
    
	ret = gprs_at_GetSignalQuality();
	if (ret < 0)
		return -2;

	ret = gprs_at_CheckRegister();
	if (ret < 0)
		return -3;

	ret = gprs_at_CheckGprsAttachment();
	if (ret < 0)
		return -4;

	/*
	ret = gprs_at_SetAPN(NULL, NULL, NULL);
	if (ret < 0)
	{
		dbg("gprs_at_SetAPN error\n");
		return -5;
	}
*/
	ret = gprs_at_InitPDP();
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
		return -6;
	}
	/*
	ret = gprs_at_get_cclk(&rtcTm);
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
		return -6;
	}


	ret = gprs_at_location(NULL);
	if (ret < 0)
	{
		dbg("gprs_at_location error\n");
		return -6;
	}
*/
	gprs_at_SetRecvMode();
	gprs_at_SetHead();

	ret = gprs_at_TcpConnectHost("47.106.13.245", "6666");
	if (ret < 0)
	{
		dbg("gprs_at_TcpConnectHost error\n");
		return -7;
	}

	//连续通讯3次
	for (loop = 0; loop < 3; loop++)
	{
		memcpy(rcvData, "\x02\xA0\x00\x01\x00\x03\xA1", 7);
		ret = gprs_at_TcpSnd(rcvData, 7);
		if (ret < 0)
		{
			dbg("gprs_at_TcpSnd error:%d\n", ret);
			break;
		}

		ret = gprs_at_TcpRcv(rcvData, 4, 5 * 1000);
		if (ret < 0)
		{
			dbg("gprs_at_TcpRcv error\n");
			break;
		}
		len = ((unsigned char)rcvData[2]) * 256 + (unsigned char)rcvData[3];
		if (len + 4 + 2 > sizeof(rcvData))
		{
			dbg("recv too long.\n");
			break;
		}
		ret = gprs_at_TcpRcv(rcvData + 4, len + 2, 500);
		if (ret < 0)
		{
			dbg("gprs_at_TcpRcv2 error\n");
			break;
		}
		mdelay(200);
	}

	if(ret>0)
	{
		//char szTmp[2100+1];
		//vOneTwo0(rcvData, 4+len+2, szTmp);
		//dbg("rcvdata:[%.200s]\n", szTmp);
		hexdump((unsigned char *)rcvData, 4 + len + 2);
	}

	gprs_at_TcpDisconnectHost();

	ret = gprs_at_CloseGprs();
	if (ret < 0)
	{
		dbg("gprs_at_CloseGprs error\n");
		return -8;
	}

	return 0;
}

int gprs_at_domain(char *pszHostName, char *pszHostIp)
{
	char send[100];
	char resp[100];
	int ret;
	char *p;
	int flag;
	
	//gprs解析域名,先有OK，后一行才出ip,不方便指定特定字符做结束符
	sprintf(send, "AT+QIDNSGIP=\"%.80s\"\r\n", pszHostName);
#if 1
	//按行读取到行内有'.'时认为读取成功
	ret=gprs_at_cmd_sndrcv(send, ".", 6*1000, 1, 1, resp, sizeof(resp)-1);
	if(ret)
		return ret;
	if(strstr(resp, "OK")==0)
		return -1;

	flag=0;
	p=resp+strlen(resp)-1;
	while(p>=resp)
	{
		if(*p=='.' || (*p>='0' && *p<='9'))
		{
			flag=1;
		}else
		{
			if(flag)
				break;
			else
				*p=0;
		}
		p--;
		continue;
	}
	if(*p<'0' || *p>'9')
		p++;
	pszHostIp[0]=0;
	ret=strlen(p);
	if(flag && ret>=7 && ret<=15)
	{
		strcpy(pszHostIp, p);	
		return 0;
	}else
		return 1;
#else
	//按行读取到“OK”时认为执行成功，然后再按行读取判断ip
	ret=gprs_at_cmd_sndrcv(send, "OK", 0, 1, 1, resp, sizeof(resp)-1);
	if(ret)
		return ret;

	len=0;
	while(len<sizeof(resp)-1)
	{
		if(gprs_read(resp+len, 1, 3000)<=0)
		{
			dbg("gprs_at_domain err.\n");
			return -1;
		}
		if(resp[len]=='\r' || resp[len]=='\n')
		{
			resp[len]=0;
			if(strchr(resp, '.'))
			{
				strcpy(pszHostIp, resp);
				return 0;
			}else
			{
				len=0;
				continue;
			}
		}
		len++;
		continue;
	}
#endif
}

int gprs_at_iccid(char *pszICCID)
{
	char resp[100];
	int ret;
	char *p, *p1;
	int loop;
	
    pszICCID[0]=0;
    loop = 0;
	do
	{
		ret=gprs_at_cmd_sndrcv("AT+QCCID\r\n", "OK\r", 1*1000, 1, 1, resp, sizeof(resp)-1);
	} while (ret != 0 && ++loop <= 2);
    
	if(ret)
    {
        dbg("AT+QCCID err:%d\n", ret);
		return ret;
    }

    p=resp;
    while(*p=='\r' || *p=='\n')
        p++;
    
    if(memcmp(p, "89", 2))
        return -1;
    p1=strchr(p, '\r');
    if(p1)
        *p1=0;
    if(strlen(p)!=20)
        return -2;
    strcpy(pszICCID, p);
    return 0;
}

int gprs_at_imei(char *pszIMEI)
{
	char resp[100];
	int ret;
	char *p, *p1;

    pszIMEI[0]=0;
	ret=gprs_at_cmd_sndrcv("AT+QGSN\r\n", "OK\r", 1*1000, 1, 1, resp, sizeof(resp)-1);
	if(ret)
    {
        dbg("AT+QGSN err:%d\n", ret);
		return ret;
    }
    
	p=strstr(resp, "+QGSN: ");
	if(p)
	{
		p+=strlen("+QGSN: ");
		p1=strchr(p, '\r');
		if(p1)
			*p1=0;
		strcpy(pszIMEI, p);
		return 0;
	}
    return -1;
}

int gprs_at_connState(char *pszState)
{
	char resp[100];
	int ret;
	char *p, *p1;

/*
    ret=gprs_at_CheckSIMQuick();
    if(ret)
    {
        if(ret!=-100)
            ret=-20;
        return ret;
    }
*/
    
    ret=gprs_at_cmd_sndrcv("AT+QISTATE\r\n", ":", 1*1000, 1, 1, resp, sizeof(resp)-1);
	if(ret)
    {
        dbg("AT+QISTATE err:%d\n", ret);
		return ret;
    }

	p=strstr(resp, "STATE: ");
	if(p)
	{
		p+=strlen("STATE: ");
		p1=strchr(p, '\r');
		if(p1)
			*p1=0;
		strcpy(pszState, p);
		return 0;
	}
    return -1;
}

int gprs_at_gmr(char *pszGprsVer, int iOutSize)
{
	//两条指令返回相同
	//gprs_at_cmd_sndrcv("AT+CGMR\r", "OK", 0, 1, 1, pszGprsVer, iOutSize);
	return gprs_at_cmd_sndrcv("AT+GMR\r", "OK", 0, 1, 1, pszGprsVer, iOutSize);
}

int gprs_at_MobileId(char* pszMobileId)
{
	char recv[200];
	int ret;
	int loop = 0;
	char *p, *p1;

	if(pszMobileId)
		pszMobileId[0]=0;

	//do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+CNUM\r", "+CNUM:", 1000, 1, 1, recv, sizeof(recv) - 1);	
		if (ret == 0 && strstr(recv, "+CNUM:"))
		{
			ret=-1;
			p=strstr(recv, "+CNUM:");
			//_vDisp(2, p);
			p+=strlen("+CNUM:");
			p1=strchr(p, ',');
			if(p1)
			{
				p=p1+1;
				p1=strchr(p, ',');
				if(p1)
					*p1=0;
				else
				{
					p[20]=0;
				}
				if(p[0]=='"')
					p++;
				if(p[strlen(p)-1]=='"')
					p[strlen(p)-1]=0;
				//_vDisp(3, p);
				if(pszMobileId)
					strcpy(pszMobileId, p);
				ret = 0;
			}
		}else
			ret = -1;
	} //while (ret < 0 && ++loop <= 15);
	dbg("gprs_at_MobileId return %d\n", ret);
	return ret;
}

int gprs_at_IMSI(char* pszIMSI)
{
	char recv[100];
	int ret;
	int loop = 0;
	char *p, *p1;

	if(pszIMSI)
		pszIMSI[0]=0;

	//do
	{
		if (loop)
			mdelay(200);
		ret = gprs_at_cmd_sndrcv("AT+CIMI\r", "OK\r", 1000, 1, 1, recv, sizeof(recv) - 1);	
		if (ret == 0)
		{
			ret=-1;
			
            p=strstr(recv, "CIMI");
			if(p)
				p+=strlen("CIMI");
			else
				p=recv;
			
			while(*p)
			{
				if(*p>='0' && *p<='9')
					break;
				p++;
			}
            if(*p==0)
                ret=-1;
            else
            {
                p1=strchr(p, '\r');
                if(p1)
                    *p1=0;
                //_vDisp(3, p);
                if(pszIMSI)
                    strcpy(pszIMSI, p);
                ret = 0;
            }
		}else
			ret = -1;
	} //while (ret < 0 && ++loop <= 15);
	dbg("gprs_at_IMSI return %d\n", ret);
	return ret;
}

#endif      //#ifndef USE_LTE_4G

