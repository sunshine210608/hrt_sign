#ifndef _MANAGE_H
#define _MANAGE_H

// 界面有关函数返回值定义, 用于上层函数判断界面走向
#define FUNCRET_OK				0 // 函数正常退出
#define FUNCRET_CANCEL			1 // 函数被取消退出
#define FUNCRET_TIMEOUT			2 // 函数超时退出
#define FUNCRET_ERR_RETURN		3 // 函数出错, 返回主界面
#define FUNCRET_ERR_CONTINUE	4 // 函数出错, 不返回主界面
#define FUNCRET_MENU_CANCEL		5 // 菜单选择时取消退出

// 终端初始化
void vTermInit(void);

// 修改主管员密码
int iMainOperChgPwd(void);

// 操作员解除暂退
int iDoOperLeavePause(void);

// 修改操作员密码
int iDoChangeOperPwd(void);

// 操作员签到
int iDoOperLogin(void);

void vInputTMK(void);

int iViewTransList(void);

int iSetPosParam(void);

int iSetTransParam(void);

//sheet-第几联  0-无 1-商户联 2-持卡人 3-收单机构
//dup - 重打标志 0-正常 1-重打
//QrRec - 交易记录
//pszReference - 备注信息,8583的63.2
int iPrintTransSheet(int sheet, int dup, stTransRec *Rec, uchar *pszReference);

//sheet-第几联  0-无 1-商户联 2-持卡人 3-收单机构
//dup - 重打标志 0-正常 1-重打
//QrRec - 交易记录
//pszReference - 备注信息
int iPrintQrTransSheet(int sheet, int dup, stQrTransRec *QrRec);

//dup - 重打标志
//pszReference - 备注信息
int iPrintTrans(stTransRec *rec, int dup, uchar *pszReference);

int iPrintQrTrans(stQrTransRec *QrRec, int dup);

int iPrintTransByTTC(void);

int iPrintAll(void);

int iPrintSum(void);

int iPrintSettle(stSettleInfo *sett,uchar ucPrintSettleFlag);

void vGenTransSum(int *piSaleNum, ulong *pulSaleAmt, int *piSaleVoidNum, ulong *pulSaleVoidAmt, 
        int *piAuthCmpNum, ulong *pulAuthCmpAmt, int *piAuthCmpVoidNum, ulong *pulAuthCmpVoidAmt,
        int *piRefundNum, ulong *pulRefundAmt,uchar flag);

void vGenQrTransSum(int *piScanSaleNum, ulong *pulScanSaleAmt, int *piQrSaleNum, ulong *pulQrSaleAmt, int *piSaleVoidNum, ulong *pulSaleVoidAmt, 
        int *piSaleRefundNum, ulong *pulSaleRefundAmt, uchar PayWay,uchar flag);
void vGenQrPreauthTransSum(int *piScanPreauthNum, ulong *pulScanPreauthAmt, int *piQrPreauthNum, ulong *pulQrPreauthAmt, int *piPreauthCompNum, ulong *pulPreauthCompAmt, 
        int *piPreauthVoidNum, ulong *pulPreauthVoidAmt, uchar PayWay,uchar flag);
int iViewTransSum(void);

int iViewTransByTTC(void);


#endif
