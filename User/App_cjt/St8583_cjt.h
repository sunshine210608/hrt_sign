#ifndef _st8583_H
#define _st8583_H

#include "Pack8583.h"

#define Msg0_01Len 4

#define Data0_BitMapLen 8
#define Data0_02Len 19
#define Data0_03Len 6
#define Data0_04Len 12
#define Data0_05Len 100
#define Data0_06Len 64
#define Data0_07Len 64
#define Data0_11Len 6
#define Data0_12Len 6
#define Data0_13Len 4
#define Data0_14Len 4
#define Data0_15Len 4
#define Data0_22Len 3
#define Data0_23Len 3
#define Data0_24Len 3
#define Data0_25Len 2
#define Data0_26Len 2
#define Data0_27Len 2
#define Data0_28Len 8
#define Data0_29Len 15
#define Data0_30Len 40

//#define Data0_28Len 1
#define Data0_32Len 11
#define Data0_35Len 37
#define Data0_36Len 104
#define Data0_37Len 12
#define Data0_38Len 6
#define Data0_39Len 2
#define Data0_40Len 40
#define Data0_41Len 8
#define Data0_42Len 15
#define Data0_43Len 40
#define Data0_44Len 25
#define Data0_45Len 38
#define Data0_47Len 999
#define Data0_48Len 322
#define Data0_49Len 4
#define Data0_51Len 300			//vip收款标识,mpos专用
#define Data0_52Len 8
#define Data0_53Len 16
#define Data0_54Len 120
#define Data0_55Len 255
#define Data0_56Len 512
#define Data0_57Len 100
#define Data0_58Len 260
#define Data0_59Len 600
#define Data0_60Len 150
#define Data0_61Len 395
#define Data0_62Len 999       //上送签字扩到999字节
#define Data0_63Len 163
#define Data0_64Len 8

//域必须与Data0中一致
typedef struct
{
   unsigned char Msg01[Msg0_01Len + 2];
   unsigned char BitMap[2 * Data0_BitMapLen];
   unsigned char Field02[Data0_02Len + 2];
   unsigned char Field03[Data0_03Len + 2];
   unsigned char Field04[Data0_04Len + 2];
   unsigned char Field05[Data0_05Len + 2];
   unsigned char Field06[Data0_06Len + 2];
#if 0     
   unsigned char Field07[Data0_06Len + 2];
#endif
   unsigned char Field11[Data0_11Len + 2];
   unsigned char Field12[Data0_12Len + 2];
   unsigned char Field13[Data0_13Len + 2];
   unsigned char Field14[Data0_14Len + 2];
   unsigned char Field15[Data0_15Len + 2];
   unsigned char Field22[Data0_22Len + 2];
   unsigned char Field23[Data0_23Len + 2];
   unsigned char Field24[Data0_24Len + 2];
   unsigned char Field25[Data0_25Len + 2];
   unsigned char Field26[Data0_26Len + 2];
   unsigned char Field27[Data0_27Len + 2];
   unsigned char Field28[Data0_28Len + 2];
   unsigned char Field29[Data0_29Len + 2];
   unsigned char Field30[Data0_30Len + 2];
   unsigned char Field32[Data0_32Len + 2];
   unsigned char Field35[Data0_35Len + 2];
   unsigned char Field36[Data0_36Len + 2];
   unsigned char Field37[Data0_37Len + 2];
   unsigned char Field38[Data0_38Len + 2];
   unsigned char Field39[Data0_39Len + 2];
   unsigned char Field40[Data0_40Len + 2];
   unsigned char Field41[Data0_41Len + 2];
   unsigned char Field42[Data0_42Len + 2];
   unsigned char Field43[Data0_43Len + 2];
   unsigned char Field44[Data0_44Len + 2];
   unsigned char Field45[Data0_45Len + 2];
   unsigned char Field47[Data0_47Len + 2];
   unsigned char Field48[Data0_48Len + 2];
   unsigned char Field49[Data0_49Len + 2];
//   unsigned char Field51[Data0_51Len + 2];
   unsigned char Field52[Data0_52Len + 2];
   unsigned char Field53[Data0_53Len + 2];
   unsigned char Field54[Data0_54Len + 2];
   unsigned char Field55[Data0_55Len + 2];
   unsigned char Field56[Data0_56Len + 2];
   unsigned char Field57[Data0_57Len + 2];
   unsigned char Field58[Data0_58Len + 2];
   unsigned char Field59[Data0_59Len + 2];
   unsigned char Field60[Data0_60Len + 2];
   unsigned char Field61[Data0_61Len + 2];
   unsigned char Field62[Data0_62Len + 2];
   unsigned char Field63[Data0_63Len + 2];
   unsigned char Field64[Data0_64Len + 2];
} st8583;

extern const FIELD_ATTR Msg0[];
extern const FIELD_ATTR CjtData0[];
#endif
