#ifndef _QRTRANS_H
#define _QRTRANS_H


#define B_posTophone					1		//被扫(机器扫手机)
#define Z_phoneTopos					2		//主扫(手机扫机器)

#define MODE_UNIONPAY				0x31
#define MODE_WETCHAT				0x32		
#define MODE_ALIPAY					0x33	
#define MODE_HUABEI	                                0x34 


#define TRADE_FIELD44_QR_TYPE_WETCHAT   "002"
#define TRADE_FIELD44_QR_TYPE_ALIPAY    "003"
#define TRADE_FIELD44_QR_TYPE_UNIONPAY  "004"

unsigned char iQRCodePay(u8 nMode);
	
#endif
