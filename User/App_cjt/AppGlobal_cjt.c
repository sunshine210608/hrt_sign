#include "VposFace.h"
#include "AppGlobal_cjt.h"

stSysInfo		gl_SysInfo;								// 系统信息，设定的参数
stSysData		gl_SysData;								// 系统数据
stTransRec		gl_TransRec;							// 交易记录      
//stRunTimeData   gl_RTD;									// 运行时数据

st8583			gl_Send8583;							// 发送8583结构
st8583			gl_Recv8583;							// 接收8583结构
uchar           gl_ucNFCPinFlag;     //闪付凭密标志
uchar           gl_uc9f6c[2+1];
uchar           gl_ucOutsideCardFlag;
uchar           gl_ucStandbyCardFlag; //在待机界面读卡了就不需要再读卡 1.待机界面IC 2.待机界面NFC  3.待机界面MAG  0.菜单进入
uchar          gl_ucSPan[19+1]; //手输卡号
uchar          gl_ucTimeOutFlag; //99 00 是否超时退出
uchar          gl_ucProcReqFlag;//判断报文头是否有处理要求
uchar          gl_ucSignuploadFlag; //电子签名上送交易标识
uchar          gl_ucQrTransFlag; //扫码交易标识
uchar          gl_ucMobileTransFlag; //手机pay交易标识
uchar          gl_ucAppUpdate;        //TMS应用下载更新标志
char            gl_baseStation[100];  //wifi情况下获取基站定位
stCardBinb        gl_CardBinb;  
stCardclblack        gl_Cardclblack;  
//test: TMK_KEK = 9AE9E33AECD8966C
