#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "printer.h"
#include "VposFace.h"
#include "Pub.h"
#include "Oper.h"
#include "test_menu.h"
#include "debug.h"
#include "tag.h"
#include "user_projectconfig.h"
#include "func.h"
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "QrTrans_cjt.h"
#include "Manage_cjt.h"
#include "pinpad.h"

extern int LoadTMK(uchar *TMK);
extern int iDoMainOperFunc(void);
extern int iDoSystemMenu(void);
extern int iPosLogin(uchar ucDispMsg);
extern int iPrintSignFile(uchar *pszJbgFile);
extern int iOnlineSettle(void);
extern int iOfflineSettle(void);
extern int iDownloadCAPK(void);
extern int iDownloadEmvParam(void);
extern int iPrintQrCode(void);
extern void vSetExitToMainFlag(char flag);
extern uint uiMemManaGetSettleInfo(stSettleInfo *pSettle);
extern int iGetStringGBK(unsigned char *pszStr, int iStrLen);
extern void  _uiSetSpaceCols(int col,uchar *OutData);
extern int iDoHardWareTest(void);
extern int iCheckTranStat(uint uiTransType, uchar ucMode);
extern int iPosUpdateParam(void);
extern int iDownMK(void);
extern void vDispRight(uint uiLine, char *pszMsg);
	
void GetBankName(char *cmpbuf,char * text);
void  get_bankno(byte *str,uchar *OutData);
int iSystemChgPwd(void);
int iSafeChgPwd(void);
int iMainOperResetPwd(void);

extern char ucExitToMainFlag;        //1-退到主界面

// 终端初始化
void vTermInit(void)
{
	//uchar szBuf[20];
	//int i;
	uint uiRet;
	gl_SysInfo.ulInitFlag = 0;
	
    	uiRet = uiMemManaSetEnv();
	dbg("gl_SysInfo.ulInitFlag=%04X\n", gl_SysInfo.ulInitFlag);
	
	dbgHex("mainkey", gl_SysInfo.sMasterKey,16);
	dbgHex("sPinKey", gl_SysData.sPinKey,16);
	dbgHex("sMacKey", gl_SysData.sMacKey,16);
	dbgHex("sMagKey", gl_SysData.sMagKey,16);  
#if 1	
    if(uiRet==0 && memcmp(gl_SysInfo.szSysVer, "HRT_R_V1.0.0", 12)!=0)
    {
        //1.0.0之前的版本未对szSysVer赋值，本处对1.0.0之前的版本赋值初始化标志
        gl_SysInfo.ulInitFlag = 0;
    }
#endif    

	if ( uiRet || gl_SysInfo.ulInitFlag != 0x55AA5AA5)
	{
		dbg("app first init...\n");
        
		// 这是第一次启动终端, 初始化
		memset(&gl_SysInfo, 0, sizeof(gl_SysInfo));
		memset(&gl_SysData, 0, sizeof(gl_SysData));
		memset(&gl_Cardclblack,0,sizeof(gl_Cardclblack));

        //生产环境        
        //TMS      
            strcpy(gl_SysInfo.szTmsIP1, "47.106.13.245");
	    gl_SysInfo.uiTmsHostPort=8686;
//        strcpy((char*)gl_SysInfo.szTmsIP1,    "119.254.93.36");
//	    strcpy((char*)gl_SysInfo.szTmsIP2, "119.254.93.36");
//        strcpy((char*)gl_SysInfo.szTmsHost,    "tms.ruiyinxin.com");
//	    strcpy((char*)gl_SysInfo.szTmsHostBak, "tmsbak.ruiyinxin.com");
//	    gl_SysInfo.uiTmsHostPort=10010;
//	    gl_SysInfo.uiTmsHostPortBak=10010;
//	    gl_SysInfo.ucTmsDomainOrIp = 0x01;	

	//签名
//	strcpy(gl_SysInfo.szESignSvrIp, "124.126.12.68");
//	gl_SysInfo.uiESignSvrPort=37520;
//	strcpy(gl_SysInfo.szESignBakSvrIp, gl_SysInfo.szESignSvrIp);
//	gl_SysInfo.uiESignBakSvrPort=gl_SysInfo.uiESignSvrPort;
        
        strcpy(gl_SysInfo.szSysVer, "HRT_R_V1.0.0");        //格式: 厂商[3]_R/D[1](R正式版/D调试版)_XX.XX.XX(版本号)    
	
        gl_SysInfo.iCommType = VPOSCOMM_WIFI;//VPOSCOMM_WIFI;   //盛迪嘉要求初始通讯方式为gprs
//    gl_SysInfo.ucAmtInPutMode=0x01;            // 默认值:0-分 1-元
        strcpy(gl_SysInfo.szParamVer,"V2021102601");
#ifdef ENABLE_PRINTER
	strcpy(gl_SysInfo.ucPrintTitle,"POS签购单");
#endif
        gl_SysInfo.ucFailSignMax = 50;      // 失败电签存储笔数,默认50
        gl_SysInfo.ucKeyBeep = 0x01;

	//以下为TMS参数

        //交易
        //测试环境
	//strcpy(gl_SysInfo.szSvrIp, "123.125.127.115");
	//gl_SysInfo.uiSvrPort=1900;
	//strcpy(gl_SysInfo.szBakSvrIp, "123.125.127.115");
	//gl_SysInfo.uiBakSvrPort=1900;
	
	//生产环境
	strcpy(gl_SysInfo.szSvrIp, "123.125.99.84");
	gl_SysInfo.uiSvrPort=1900;
	strcpy(gl_SysInfo.szBakSvrIp, "58.83.189.53");
	gl_SysInfo.uiBakSvrPort=1900;
	
	memcpy(gl_SysInfo.sTPDU, "\x60\x00\x04\x00\x00", 5);
        memcpy(gl_SysInfo.sQrTPDU, "\x60\x00\x02\x00\x00", 5);
		
	// 其它参数
	gl_SysInfo.uiCommTimeout= 60;	// 通讯超时时间
        gl_SysInfo.uiConnectTimeout = 10;
	gl_SysInfo.uiUiTimeOut = 60;
	gl_SysInfo.uiCommTimeout = 60;     
	
	gl_SysData.ulTTC = 1;		// 初始TTC
	gl_SysData.ulBatchNo = 1;	// 交易批次号
#ifdef ENABLE_PRINTER 
        gl_SysInfo.ucPrinterAttr=2; //打印两联
	gl_SysInfo.ucPrintIssuer = 0x01;
        gl_SysInfo.ucPrtNegative = 0x01;
	gl_SysInfo.ucPrintEN = 0x01;	
#endif
	
       	gl_SysInfo.ucSaleSwitch = 0x01;
	gl_SysInfo.ucVoidSwitch = 0x01;
	gl_SysInfo.ucRefundSwitch = 0x01;
	gl_SysInfo.ucBalanceSwitch = 0x01;
	gl_SysInfo.ucAuthSwitch = 0x01;
	gl_SysInfo.ucAuthCancelSwitch = 0x01;
	gl_SysInfo.ucAuthCompleteSwitch = 0x01;
	gl_SysInfo.ucAuthCompletenoticeSwitch = 0x01;
	gl_SysInfo.ucAuthCompleteVoidSwitch = 0x01;
	gl_SysInfo.ucSignNameSwitch = 0x01;
	gl_SysInfo.ucQrSaleSwitch = 0x01;
	gl_SysInfo.ucQrRefundSwitch = 0x01;
	gl_SysInfo.ucQrVoidSwitch = 0x01;
	gl_SysInfo.ucQrPreAuthSwitch = 0x01;
        gl_SysInfo.ucQrPreAuthCancelSwitch = 0x01;
	gl_SysInfo.ucQrAuthCompleteSwitch = 0x01;

	gl_SysInfo.ucSaleVoidPinFlag = 0x01;		
	gl_SysInfo.ucAuthVoidPinFlag = 0x01;	
	gl_SysInfo.ucAuthCompVoidPinFlag = 0x01;		
	gl_SysInfo.ucAuthCompPinFlag = 0x01;

        gl_SysInfo.ucSaleVoidCardFlag = 0x01;			
	gl_SysInfo.ucAuthCompleteVoidCardFlag = 0x01;			

	gl_SysInfo.ucAutoLogoutFlag = 0x01;
	gl_SysInfo.ucSettlePrintDetail = 0x01;

	gl_SysInfo.ucMainOperPwd = 0x01;
	gl_SysInfo.ucHandCard = 0x01;
		
        gl_SysInfo.ucDefaultTrans = 0x01;
	   
	gl_SysInfo.ucMaxResendCount = 3;
	gl_SysInfo.ucMaxTradeCnt = 300;	
	gl_SysInfo.ulRefund_limit = 10000000;//精确到分
	//gl_SysInfo.ucNoPinFlag = 0x01;
	gl_SysInfo.ulNoPinLimit = 100000;
	//gl_SysInfo.ucNoSignFlag = 0x01;
	gl_SysInfo.ulNoSignLimit = 100000;        
	gl_SysInfo.ucSupportESign = 0x01;
	gl_SysInfo.uiESignSendCount = 3;
	gl_SysInfo.uiESignTimeout = 180;
	
	// 	    
	gl_SysInfo.ucCommRetryCount = 3;		
		
        strcpy(gl_SysInfo.szSysOperPwd,"12345678");// 系统管理员缺省密码
	strcpy(gl_SysInfo.szMainOperPwd,"123456");   // 主管员缺省密码     
	strcpy(gl_SysInfo.szSafeSetPwd,"517033");	

        gl_SysInfo.ucSmFlag = 0x01;                           //	0 3des		1 国密
//        gl_SysInfo.magEncrypt = 0x01;
	gl_SysInfo.ucQrPreViewFlag = 0x01;	

	gl_SysInfo.ucByPass = 0x01;
	gl_SysInfo.ucDoReverseFlag = 0x01;
	gl_SysInfo.ucCleanReverseFlag = 0x01;
	
		//gl_SysInfo.ulInitFlag = 0x55AA5AA5; // 结构初始化完成

        uiRet = uiMemManaInit();					// 首次扩展内存管理完全初始化
	if(uiRet)
	{
		dbg("uiMemManaInit fail!!!\n");
		_vCls();
		vDispMid(2, "初始化系统数据失败");
		//_vPosClose();
	    while(1)
	        ;
	}
	//uiMemManaPutSysData();

	vOperInit();						// 初始化操作员
        
        gl_SysInfo.ulInitFlag = 0x55AA5AA5; // 结构初始化完成,必须放初始化操作最后
        uiMemManaPutSysInfo();		
	}
     	_vSetKeyVoice(gl_SysInfo.ucKeyBeep);     //设置按键音
  
    
#if 0
	while(gl_SysInfo.szPosId[0]==0 || gl_SysInfo.szSvrIp[0]==0 || memcmp(gl_SysInfo.sMasterKey, "\x00\x00\x00", 3)==0)
	{
		iDoSystemMenu();
	}
#endif
}

// 输入新密码
// in  : pszPrompt : 提示信息
//       iPinLen   : 密码长度
//       pszNewPin : 输入的新密码
// ret : FUNCRET_OK				: 函数正常退出
//       FUNCRET_CANCEL			: 函数被取消退出
//       FUNCRET_TIMEOUT		: 函数超时退出
//       FUNCRET_ERR_RETURN		: 函数出错
int iGetNewPin(uchar *pszPrompt, int iPinLen, uchar *pszNewPin)
{
	uchar szPin1[13], szPin2[13];
	int iRet;
        uchar ucPinPos;
		
	if (iPinLen > sizeof(szPin1) - 1)
		return (FUNCRET_ERR_RETURN);
	if (pszPrompt && pszPrompt[0])
		vDispCenter(1, (char *)pszPrompt, DISP_REVERSE);
	
	ucPinPos = _uiGetVCols();
	for (;;)
	{
		vClearLines(2);
		_vDisp(2, "请输入新密码:");		
		iRet = iInput(INPUT_PIN|INPUT_LEFT, 4, ucPinPos, szPin1, (uchar)iPinLen, (uchar)iPinLen, (uchar)iPinLen, gl_SysInfo.uiUiTimeOut);
		if (iRet == -1)
			return (FUNCRET_CANCEL);
		if (iRet == -3)
			return (FUNCRET_TIMEOUT);
		if (iRet < iPinLen)
			return (FUNCRET_ERR_RETURN);
		
		vClearLines(2);
		_vDisp(2, "请确认新密码:");
		iRet = iInput(INPUT_PIN|INPUT_LEFT, 4, ucPinPos, szPin2, (uchar)iPinLen, (uchar)iPinLen, (uchar)iPinLen, gl_SysInfo.uiUiTimeOut);
		if (iRet == -1)
			return (FUNCRET_CANCEL);
		if (iRet == -3)
			return (FUNCRET_TIMEOUT);
		if (iRet < iPinLen)
			return (FUNCRET_ERR_RETURN);
		if (strcmp((char *)szPin1, (char *)szPin2) == 0)
			break;
		vMessage("两次输入不一致");
	} // for(;;
	strcpy((char *)pszNewPin, (char *)szPin1);
	return (FUNCRET_OK);
}

int iDoSystemPwd(void *p)
{    
	iSystemChgPwd();
	return 0;
}

int iDoSafePwd(void *p)
{    
	iSafeChgPwd();
	return 0;
}

// 重置主管员密码
int iDoResetMainOperPwd(void *p)
{
	iMainOperResetPwd();
	return 0;
}


/*
// 清除暂退
static int iDoResetPauseFlag(void)
{
	int iRet;
	_vCls();
	_vDisp(2, "确定解除暂退?");
	iRet = iGetKeyWithTimeout(gl_SysInfo.uiUiTimeOut);
	if (iRet < 0)
		return (FUNCRET_TIMEOUT);
	if (iRet != _KEY_ENTER)
		return (FUNCRET_CANCEL);
	gl_SysData.ucOperPauseMode = 0;
	gl_SysData.szCurOper[0] = 0; // 解除暂退需要退出操作员登录
	uiMemManaPutSysData();
	return (FUNCRET_OK);
}
*/

// 修改主管员密码
int iMainOperChgPwd(void)
{
#if 1
	int iRet;
	uchar szBuf[32];
        uchar ucPinPos;
		
	vDispCenter(1, "修改主管员密码", DISP_REVERSE);
	_vDisp(2, "请输入原密码:");
	vClearLines(3);
	ucPinPos = _uiGetVCols();
	iRet = iInput(INPUT_PIN|INPUT_LEFT, 3, ucPinPos, szBuf, 6, 6, 6, gl_SysInfo.uiUiTimeOut);
	if (iRet == -1)
		return (FUNCRET_CANCEL);
	if (iRet == -3)
		return (FUNCRET_TIMEOUT);
	if (iRet < 0)
		return (FUNCRET_ERR_RETURN);
	if (strcmp((char *)gl_SysInfo.szMainOperPwd, (char *)szBuf) != 0)
	{
		vMessage("密码错误");
		return (FUNCRET_ERR_CONTINUE);
	}

	iRet = iGetNewPin((uchar *)"修改主管员密码", 6, szBuf);
	if (iRet != FUNCRET_OK)
		return (iRet);
	strcpy((char *)gl_SysInfo.szMainOperPwd, (char *)szBuf);
	uiMemManaPutSysInfo();
	vMessage("密码修改成功");
	return (FUNCRET_OK);
#else
{

extern void vUpDateAppfile(void);
vUpDateAppfile();
}
#endif
}


// 修改管理员密码
int iSystemChgPwd(void)
{ 
	int iRet;
	uchar szBuf[32];
        uchar ucPinPos;

	_vCls();
	vDispCenter(1, "修改管理员密码", DISP_REVERSE);
	_vDisp(2, "请输入原密码:");
	ucPinPos = _uiGetVCols();
	iRet = iInput(INPUT_PIN|INPUT_LEFT, 3, ucPinPos, szBuf, 8, 8, 8, gl_SysInfo.uiUiTimeOut);
	if (iRet == -1)
		return (FUNCRET_CANCEL);
	if (iRet == -3)
		return (FUNCRET_TIMEOUT);
	if (iRet < 0)
		return (FUNCRET_ERR_RETURN);
	if (strcmp((char *)gl_SysInfo.szSysOperPwd, (char *)szBuf) != 0)
	{
		vMessage("密码错误");
		return (FUNCRET_ERR_CONTINUE);
	}

	iRet = iGetNewPin((uchar *)"修改管理员密码", 8, szBuf);
	if (iRet != FUNCRET_OK)
		return (iRet);
	strcpy((char *)gl_SysInfo.szSysOperPwd, (char *)szBuf);
	uiMemManaPutSysInfo();
	vMessage("密码修改成功");
	return (FUNCRET_OK);
 }

// 修改安全密码
int iSafeChgPwd(void)
{ 
	int iRet;
	uchar szBuf[32];
        uchar ucPinPos;

	_vCls();	
	vDispCenter(1, "修改安全密码", DISP_REVERSE);
	_vDisp(2, "请输入原密码:");
	ucPinPos = _uiGetVCols();
	iRet = iInput(INPUT_PIN|INPUT_LEFT, 3, ucPinPos, szBuf, 6, 6, 6, gl_SysInfo.uiUiTimeOut);
	if (iRet == -1)
		return (FUNCRET_CANCEL);
	if (iRet == -3)
		return (FUNCRET_TIMEOUT);
	if (iRet < 0)
		return (FUNCRET_ERR_RETURN);
	if (strcmp((char *)gl_SysInfo.szSafeSetPwd, (char *)szBuf) != 0)
	{
		vMessage("密码错误");
		return (FUNCRET_ERR_CONTINUE);
	}

	iRet = iGetNewPin((uchar *)"修改安全密码", 6, szBuf);
	if (iRet != FUNCRET_OK)
		return (iRet);
	strcpy((char *)gl_SysInfo.szSafeSetPwd, (char *)szBuf);
	uiMemManaPutSysInfo();
	vMessage("密码修改成功");
	return (FUNCRET_OK); 
}

int iMainOperResetPwd(void)
{
	int iRet;
	_vCls();
	vDispMid(2, "重置主管密码");
	vDispMid(3, "确定?");
	iRet = iGetKeyWithTimeout(gl_SysInfo.uiUiTimeOut);
	if (iRet < 0)
		return (FUNCRET_TIMEOUT);
	if (iRet != _KEY_ENTER)
		return (FUNCRET_CANCEL);
	strcpy((char*)gl_SysInfo.szMainOperPwd, "123456");
	uiMemManaPutSysInfo();
	vMessage("重置成功");
	return (FUNCRET_OK);
}

// 操作员暂退
int iDoOperPause(void *p)
{
    extern void vSetExitToMainFlag(char flag);
	uint uiRet;
	_vCls();
	//vDispCenter(1, "操作员暂退", DISP_REVERSE);
    vDispCenter(1, "终端锁定", DISP_REVERSE);
	gl_SysData.ucOperPauseMode = 2;
	uiRet = uiMemManaPutSysData();
	if (!uiRet)
    {
		//vMessage("操作员暂退成功");
        vMessage("终端锁定成功");
    }
    vSetExitToMainFlag(1);
	return (FUNCRET_ERR_RETURN); // 返回主界面
}

// 操作员解除暂退
int iDoOperLeavePause(void)
{
	int iRet;
	uchar szOperNo[OPER_NO_LEN+1],szPin[10];
	uchar ucPinPos;
	
    /*
    if(gl_SysData.ucOperPauseMode==0)
    {
        _vCls();
        vMessage("终端未锁定,无需解锁");
        return 0;
    }
    */
	//vDispCenter(1, "操作员解除暂退", DISP_REVERSE);
	_vCls();
	vDispCenter(1, "终端解锁", DISP_REVERSE);
	vDispVarArg(2, "当前操作员%s", gl_SysData.szCurOper);
	ucPinPos = _uiGetVCols();
    while(1)
    {
		vDispVarArg(3, "请输入操作员号:%.*s", OPER_NO_LEN, "    ");
		iRet = iInput(INPUT_NORMAL|INPUT_LEFT, 4, ucPinPos, szOperNo, OPER_NO_LEN, OPER_NO_LEN, OPER_NO_LEN, gl_SysInfo.uiUiTimeOut);
		if (iRet == -1)
		{
			vSetExitToMainFlag(1);
			return (FUNCRET_CANCEL);
		}	
		if (iRet == -3)
		{
			vSetExitToMainFlag(1);
			return (FUNCRET_TIMEOUT);
		}	
		if (memcmp(szOperNo, "00", OPER_NO_LEN) == 0)
		{
			//主管员登录
			vSetExitToMainFlag(0);			
		//	iDoMainOperFunc();		
		        vClearLines(2);
			_vDisp(2, "请输入操作员密码:");
			ucPinPos = _uiGetVCols();
			iRet = iInput(INPUT_PIN | INPUT_LEFT, 4, ucPinPos, szPin, 6, 6, 6, gl_SysInfo.uiUiTimeOut);
			if (iRet <0)
				return -1;

		       	if (strcmp((char *)szPin, (char *)gl_SysInfo.szMainOperPwd)  != 0)
			{
				vDispMid(4,"密码错误"); //
				iGetKeyWithTimeout(3);
			}
			else
			{
				gl_SysData.ucPosLoginFlag=0;
				gl_SysData.ucOperPauseMode=0;
				strcpy((char *)gl_SysData.szCurOper, (char *)"00");
				uiMemManaPutSysData();
			}
			
			vSetExitToMainFlag(1);
			return (FUNCRET_ERR_RETURN); // 回到主界面
		}
		if (memcmp(szOperNo, "99", OPER_NO_LEN) == 0)
		{
			//管理员登录
			vSetExitToMainFlag(0);
            		iDoSystemMenu();       
			vSetExitToMainFlag(1);
			return (FUNCRET_ERR_RETURN); // 回到主界面
		}
		
	        if(strcmp((char*)szOperNo, (char*)gl_SysData.szCurOper)==0)
	            break;
		vMessageLine(4,"操作员不符"); //
	//	iGetKeyWithTimeout(3);
		vClearLines(4);
    }

	vClearLines(3);
   	 _vDisp(3, "请输入操作员密码:");	 
	ucPinPos = _uiGetVCols();
	iRet = iInput(INPUT_PIN | INPUT_LEFT, 4, ucPinPos, szPin, 4, 4, 4, gl_SysInfo.uiUiTimeOut);
	if (iRet == -1)
	{
		vSetExitToMainFlag(1);
		return (FUNCRET_CANCEL);
	}	
	if (iRet == -3)
	{
		vSetExitToMainFlag(1);
		return (FUNCRET_TIMEOUT);	
	}	
	if (uiOperCheckPwd(gl_SysData.szCurOper, szPin) != 0)
	{
		vSetExitToMainFlag(1);
		vMessage("密码错误");
		return (FUNCRET_ERR_CONTINUE);
	}
	vSetExitToMainFlag(1);
	gl_SysData.ucOperPauseMode = 0;
	uiMemManaPutSysData();
	return (FUNCRET_ERR_RETURN); // 返回主界面
}

uint iResetOperPwd(void)
{
	uint iIndex;
	uchar sBuf[OPER_REC_SIZE];
        int iRet;
	uchar szOperNo[10] = {0};
	uchar ucPinPos;
	uint uiKey;
	
	_vCls();
	vDispCenter(1, "操作员密码重置", DISP_REVERSE);	

        ucPinPos = _uiGetVCols();
	for (;;)
	{
		vDispVarArg(2, "请输入操作员号:%.*s", OPER_NO_LEN, "    ");
		iRet = iInput(INPUT_NORMAL|INPUT_LEFT, 4, ucPinPos, szOperNo, OPER_NO_LEN, OPER_NO_LEN, OPER_NO_LEN, gl_SysInfo.uiUiTimeOut);
		if (iRet == -1)
			return 1;
		if (iRet == -3)
			return 1;		
	        if (ucOperExist(szOperNo))
	        	break;
			vDispMid(4,"无此操作员"); //
			 iGetKeyWithTimeout(3);
			 vClearLines(4);
	} // for(;;

      
	vDispMid(4, "确认重置");
	_vDisp(5,"[确认]重置 退出[取消]"); 

	_vFlushKey();
	while(1)
	{				
		if(_uiKeyPressed())
		{
			uiKey =_uiGetKey();
			if(uiKey ==_KEY_ENTER)
			 	break;
			else if(uiKey ==_KEY_CANCEL)						    
			{
				return (FUNCRET_CANCEL);	
			}
		}	
	}
	
	for (iIndex= 0; iIndex < MAX_OPER_NUM; iIndex++)
	{
		uiMemManaGetOper(iIndex, sBuf);
                if (memcmp(szOperNo, sBuf, OPER_NO_LEN) == 0)
			break;
	}

	memset(sBuf + OPER_NO_LEN, '0', OPER_PIN_LEN);	
	uiMemManaPutOper(iIndex, sBuf);
	vClearLines(2);
        vMessage("重置成功");
	return (FUNCRET_OK);
}

#if 0
// 操作员功能
int iDoOperFunc(void)
{
	/*	
	//主菜单
	SMenu menu_OperMemu =
	{"操作员功能", 0, 1, 0,
		{
			{"操作员改密", iDoChangeOperPwd},
			{"操作员暂退", iDoOperPause},
			{"操作员签退", iDoOperlogout},
			NULL
		}
	};

	EnterTestMenu(&menu_OperMemu);
*/
	uint uiKey;

	_vCls();
	while (1)
	{
		vDispCenter(1, "操作员功能", 1);
		_vDisp(2, "1. 操作员改密");
		_vDisp(3, "2. 操作员暂退");
		_vDisp(4, "3. 操作员签退");
		while (1)
		{
			uiKey = _uiGetKey();
			if (uiKey == _KEY_ESC)
				return 0;
			if (uiKey >= _KEY_1 && uiKey <= _KEY_3)
				break;
		}
		if (uiKey == _KEY_1)
			iDoChangeOperPwd(NULL);
		if (uiKey == _KEY_2)
			iDoOperPause(NULL);
		if (uiKey == _KEY_3)
			iDoOperlogout(NULL);
		if (gl_SysData.szCurOper[0] == 0 || gl_SysData.ucOperPauseMode)
			break;
	}
	return (0);
}
#endif

//管理-签到
int iDoPosLogin(void)
{
	uchar szOperNo[10] = {0};
	int iRet;
	char szDateTime[14+1];
	
#if 1
	gl_SysData.ucOperPauseMode = 0;
	uiMemManaPutSysData();

	strcpy(szOperNo,"01");		
         vSetExitToMainFlag(1);	

//        if(!gl_SysInfo.szPosId[0] && !gl_SysInfo.szMerchId[0] && (gl_SysInfo.iCommType == VPOSCOMM_GPRS))
//		 return (FUNCRET_ERR_RETURN); 

	#ifdef TMS_PROC
	_vGetTime(szDateTime);
	 if(memcmp(szDateTime, gl_SysInfo.ucTimeTms, 8))
	{
		memcpy(gl_SysInfo.ucTimeTms,szDateTime,8);
		uiMemManaPutSysInfo();
		iTmsProc(0, 1);
	}	
	#endif
			
         iSetConnStatus(1, NULL);    //开长链接
         
	//参数下载
	iRet = iPosUpdateParam();	
	if(iRet != 0)
	{
		iSetConnStatus(0, NULL);    //关长链接	
		return (FUNCRET_ERR_RETURN); 
		
	}else
	{
		;
	}

	//主密钥下载
//        if(gl_SysInfo.ucMKFlag == 0)
	{
		iRet = iDownMK();
		if(iRet != 0)
		{
			iSetConnStatus(0, NULL);    //关长链接	
			return (FUNCRET_ERR_RETURN); 
		}	
        }	
#endif

        _vCls();
	vDispCenter(1, "签到", DISP_REVERSE);		
 
//	if(gl_SysData.ucPosLoginFlag == 0)
	{
		//签到
		iRet = iPosLogin(1);
		if(iRet == 0)
		{
			strcpy((char *)gl_SysData.szCurOper, (char *)szOperNo);
			uiMemManaPutSysData();
			return (FUNCRET_OK); // 回到主界面
		}
		else 
		{
			gl_SysData.ucOperPauseMode=0;
			return (FUNCRET_ERR_RETURN); 
		}	
	}	
}

// 操作员签到
int iDoOperLogin(void)
{
	uchar szOperNo[10] = {0}, szPin[10] = {0};
	int iRet;
	uchar ucPinPos;

        _vCls();
	vDispCenter(1, "签到", DISP_REVERSE);		
	ucPinPos = _uiGetVCols();
	
	gl_SysData.ucOperPauseMode = 1;
	uiMemManaPutSysData();

	for (;;)
	{
		vDispVarArg(2, "请输入操作员号:%.*s", OPER_NO_LEN, "    ");
		iRet = iInput(INPUT_NORMAL|INPUT_LEFT|INPUT_FACTORY, 4, ucPinPos, szOperNo, OPER_NO_LEN, OPER_NO_LEN, OPER_NO_LEN, gl_SysInfo.uiUiTimeOut);
		if (iRet == -1)
		{
		        vSetExitToMainFlag(1);
			return (FUNCRET_CANCEL);
		}	
		if (iRet == -3)
		{
			vSetExitToMainFlag(1);
			return (FUNCRET_TIMEOUT);
		}
		if(iRet == -4)
		{
			vSetExitToMainFlag(0);
			iDoHardWareTest();
			vSetExitToMainFlag(1);
			return (FUNCRET_ERR_RETURN); // 回到主界面
		}
		if (memcmp(szOperNo, "00", OPER_NO_LEN) == 0)
		{
			//主管员登录
			vSetExitToMainFlag(0);
			strcpy((char *)gl_SysData.szCurOper, (char *)szOperNo);
			iDoMainOperFunc();
			vSetExitToMainFlag(1);
			return (FUNCRET_ERR_RETURN); // 回到主界面
		}
		if (memcmp(szOperNo, "99", OPER_NO_LEN) == 0)
		{
			//管理员登录
			vSetExitToMainFlag(0);
            		iDoSystemMenu();   
			vSetExitToMainFlag(1);
			return (FUNCRET_ERR_RETURN); // 回到主界面
		}
		
	        if (ucOperExist(szOperNo))
	        	break;
			vDispMid(4,"无此操作员"); //
			 iGetKeyWithTimeout(3);
			 vClearLines(4);
	} // for(;;

	vClearLines(2);
   	 _vDisp(2, "请输入操作员密码:");	 
	ucPinPos = _uiGetVCols();
	iRet = iInput(INPUT_PIN | INPUT_LEFT, 4, ucPinPos, szPin, 4, 4, 4, gl_SysInfo.uiUiTimeOut);
	if (iRet == -1)
	{
	        vSetExitToMainFlag(1);
		return (FUNCRET_CANCEL);
	}	
	if (iRet == -3)
	{
	        vSetExitToMainFlag(1);
		return (FUNCRET_TIMEOUT);
	}	
	dbg("szPin:%s\n",szPin);
		
	if (uiOperCheckPwd(szOperNo, szPin) == 0)
	{	
	         vSetExitToMainFlag(1);			 
	 
		if(gl_SysData.ucPosLoginFlag == 0)
		{
			//签到
			iRet = iPosLogin(1);
			if(iRet == 0)
			{
				strcpy((char *)gl_SysData.szCurOper, (char *)szOperNo);
				gl_SysData.ucOperPauseMode=0;
				uiMemManaPutSysData();
				return (FUNCRET_OK); // 回到主界面
			}
			else 
				return (FUNCRET_ERR_RETURN); 
		}	

	        strcpy((char *)gl_SysData.szCurOper, (char *)szOperNo);
		gl_SysData.ucOperPauseMode=0;
		uiMemManaPutSysData();
		return FUNCRET_OK;
	}

        vSetExitToMainFlag(1);
	vClearLines(3);
	vMessage("密码错误");
	return (FUNCRET_ERR_RETURN);
}

void vInputTMK(void)
{
	int ret;
	char buf[50];
	uchar ucPinPos;

	char *apszAlphaTable[10] = {
		"0",
		"1",
		"2ABC",
		"3DEF",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9"};

	vSetAlphaTable((uchar **)apszAlphaTable);

	_vCls();
	vDispCenter(1, "手输主密钥", 1);
#ifndef KEY_TEST	
	_vDisp(2, "请输入前16位:");
	ucPinPos = _uiGetVCols();
	ret = iInput(INPUT_ALPHA|INPUT_LEFT, 3, ucPinPos, buf, 16, 16, 16, 60);
	if( ret<=0 )
		return;
	
	vClearLines(2);	
	_vDisp(2, "请输入后16位:");
	ucPinPos = _uiGetVCols();
	ret = iInput(INPUT_ALPHA|INPUT_LEFT, 3, ucPinPos, buf+16, 16, 16, 16, 60);
	if( ret<=0 )
		return;
#else
	if(gl_SysInfo.ucSmFlag == 0)
       	//	strcpy(buf,"4923E95101F28CDCCE836E384CE5B686");   //TUSN: 31
       		strcpy(buf,"E3EFBF1C4A107C0DD35E0D5413A70D9D"); //TUSN: 32
	else if(gl_SysInfo.ucSmFlag == 1)
       	//	strcpy(buf,"6157DD350FC37FB1DC3E95CFFFB32BBE"); //TUSN: 30
       		strcpy(buf,"1AFD88909EE8D623AB1E4231677D632E");//TUSN:33
#endif
	vTwoOne(buf, strlen(buf), gl_SysInfo.sMasterKey);
	dbgHex("input TMK", gl_SysInfo.sMasterKey, 16);
	uiMemManaPutSysInfo();	
#ifndef NO_SEC_FORDEMO
    ret=SaveTMK(gl_SysInfo.sMasterKey);
    #if 0
    {
        uchar key[16],tmp[50];
        
        dbg("SaveTMK ret:%d\n", ret);
        ret=LoadTMK(key);
        dbg("LoadTMK ret:%d\n", ret);
        //if(memcmp(gl_SysInfo.sMasterKey, key, 16))
        {
            vOneTwo0(key, 8, tmp);
            _vDisp(3, tmp);
            vOneTwo0(key+8, 8, tmp);
            _vDisp(4, tmp);
            _uiGetKey();
        }
    }
    #endif    
#endif

        vClearLines(2);
	vMessage("主密钥设置成功");
	return;
}

int iViewTransList(void)
{
	int idxcard,  idxnum;
    char buf[50], tmp[50];
    stTransRec rec;
	ulong ulTimer;
	uint uiKey,uiRet;
	char ucSign;
	int line,page;
	
	_vCls();
	vDispCenter(1, "交易明细", 1);
	
	    if(gl_SysData.uiTransNum==0)
	    {
	        vMessage("没有交易");
	        return 0;
	    }
	
	idxnum = 0;
	idxcard = 0;	
	page = 1;	
    while(idxcard<gl_SysData.uiTransNum)
    {
        uiRet=uiMemManaGetTransRec(gl_SysData.uiTransNum-idxcard-1, &rec);	//倒序
	if(uiRet)
		return 1;
		
		buf[0]=0;
		ucSign=0;
		dbg("rec.uiTransType:%04x\n",rec.uiTransType);
		dbg("idxcard:%d\n",idxcard);
		dbg("ulTTC:%06lu\n",rec.ulTTC);
		dbg("ulAmount:%lu\n",rec.ulAmount);
		switch (rec.uiTransType)
		{
		case TRANS_TYPE_SALE:
			strcpy(buf, "消费");
			if(rec.ucVoidFlag)
				strcat(buf, "(已撤)");
			break;			
		case  TRANS_TYPE_DAIRY_SALE:
			strcpy(buf, "日结消费");
			if(rec.ucVoidFlag)
				strcat(buf, "(已撤)");
			break;
		case TRANS_TYPE_SALEVOID:
			strcpy(buf, "消费撤销");
			ucSign=1;
			break;
		case TRANS_TYPE_PREAUTH:
			strcpy(buf, "预授权");
			break;
		case TRANS_TYPE_PREAUTHVOID:
			strcpy(buf, "预授权撤销");
			ucSign=1;
			break;
		case TRANS_TYPE_PREAUTH_COMP:
			strcpy(buf, "预授权完成");
			if(rec.ucVoidFlag)
				strcat(buf, "(撤)");
			break;
		case TRANS_TYPE_PREAUTH_COMPVOID:
			strcpy(buf, "预授权完成撤销");
			ucSign=1;
			break;
		case TRANS_TYPE_REFUND:
			strcpy(buf, "退货");
			ucSign=1;
			break;
		default:
			strcpy(buf, "未知交易");
			break;
		}
    #if  defined(ST7567)    || defined(LCD_GC9106)	
        
        vDispCenterEx(1, buf, 1, 1, idxnum+1, gl_SysData.uiTransNum);
        
        if(page == 1)
        {
                line=2;
                sprintf(buf, "金额:%s%lu.%02lu", ucSign?"-":"", rec.ulAmount/100, rec.ulAmount%100);
                _vDisp(line++, buf);
                sprintf(buf, "凭证号:%06lu", rec.ulTTC);
                _vDisp(line++, buf);

                vUnpackPan(rec.sPan, buf);
                if(rec.uiTransType != TRANS_TYPE_PREAUTH || (rec.uiTransType == TRANS_TYPE_PREAUTH  && gl_SysInfo.ucAuthCardMask == 1))
                    memcpy(buf+6, "*********", strlen(buf)-6-4);		//显示卡号首6位末4位，中间屏蔽(预授权交易不屏蔽)
                if(strlen(buf)<=_uiGetVCols()-5)
                    vDispVarArg(line++, "卡号:%s", buf);
                else
                    vDispVarArg(line++, "卡%s", buf);
    	}

	if(page == 2)
	{
		line=2;
                vDispVarArg(line++, "参考号:%.12s", rec.sReferenceNo);
                vOneTwo0(rec.sDateTime, 6, buf);
                if(_uiGetVCols()>=5+19)
                    vDispVarArg(line++, "时间:20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", buf, buf+2, buf+4, buf+6, buf+8, buf+10);
                else
                    vDispVarArg(line++, "20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", buf, buf+2, buf+4, buf+6, buf+8, buf+10);
                buf[0]=0;
                
                if(TRANS_TYPE_SALEVOID==rec.uiTransType || TRANS_TYPE_PREAUTH_COMPVOID==rec.uiTransType)
                    vDispVarArg(line++, "原凭证号:%06lu", rec.ulVoidTTC);
                else if(TRANS_TYPE_PREAUTH==rec.uiTransType || TRANS_TYPE_PREAUTH_COMP==rec.uiTransType || TRANS_TYPE_PREAUTHVOID==rec.uiTransType)
                    vDispVarArg(line++, "授权码:%.6s", rec.sAuthCode);
                else if(TRANS_TYPE_REFUND==rec.uiTransType)
                {
                    vOneTwo0(rec.sVoidInfo+6, 2, tmp);
                    vDispVarArg(line++, "原交易日期:%.4s", tmp);
                }
	}
        
    #else     

	#endif
        
        if(line<_uiGetVLines())
            vClearLines(line);

#ifdef LCD_GC9106
	if(page == 1)
		vDispCenter(_uiGetVLines(), "[取消]退出[确认]更多", 0);
	else if(page == 2)
		vDispCenter(_uiGetVLines(), "[取消]退出[↑↓]翻页", 0);
#else
	if(page == 1)
		vDispCenter(_uiGetVLines(), "[取消]退出 [确认]更多", 0);
	else if(page == 2)
		vDispCenter(_uiGetVLines(), "[取消]退出 [↑↓]翻页", 0);
#endif

		_vFlushKey();
		_vSetTimer(&ulTimer, 60*100);
		while(1)
		{
			if(_uiKeyPressed())
			{
			       _vSetTimer(&ulTimer, 60*100);
				uiKey=_uiGetKey();
				if(uiKey==_KEY_ESC)
					return 0;
				if(uiKey==_KEY_DOWN) 
				{
					if(page == 1)
					{
					  	if( idxnum != gl_SysData.uiTransNum -1)
					  	{
							idxcard++;
						        idxnum++;
					  	}
						else 
							continue;
					}
					if(page == 2)
						page = 1;
					break;
				}	
				if(uiKey==_KEY_UP)
				{
				        if(page == 1)
					{
					         if(idxnum > 0)
						 {
						 	idxcard--;
							 idxnum--;
					         }
						else
							continue;
				        }
					if(page == 2)
						page = 1;
					break;
				}
				if(uiKey==_KEY_ENTER)
				{
					page = 2;
					break;
				}
			}
			if(_uiTestTimer(ulTimer))
				return 0;
		}
    }
		
	return 0;
}

int iSetPosParam(void)
{
	int ret;
	char buf[50];
	uchar ucPinPos;
        uchar  szDateTime[14 + 1];
	char szBuf[40];
	int len;
		    char *apszAlphaTable[10] = {
		"0 ",
		"1qzQZ",
		"2abcABC",
		"3defDEF",
		"4ghiGHI",
		"5jklJKL",
		"6mnoMNO",
		"7pqrsPQRS",
		"8tuvTUV",
		"9wxyzWXYZ"};

	vSetAlphaTable((uchar **)apszAlphaTable);
	
	_vCls();
	vDispCenter(1, "商户参数设置", 1);
	_vDisp(2, "商户编号:");	
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%.15s", gl_SysInfo.szMerchId);  
	ucPinPos = _uiGetVCols() - strlen(buf);
	ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT,3, ucPinPos, buf, 15, 15, 15, NULL);
	if(ret < 0)
		return -1;
	if(memcmp(gl_SysInfo.szMerchId,buf,15) != 0)
	{
	    vClearLines(2);
		
	     if((gl_SysData.uiTransNum + gl_SysData.uiQrTransNum) > 0)
	    {
	        //有未结算的交易,自动结算
	        vDispCenter(2, "有未结算的交易",0 );
	        vDispCenter(3, "需先结算现有交易", 0);
	        iGetKeyWithTimeout(2);
	        if(gl_SysData.uiTransNum || gl_SysData.uiQrTransNum)
	            return -1;
	    }
           
		_vDisp(2, "请输入安全密码:");    
		ucPinPos = _uiGetVCols();
		memset(szBuf, 0x00, sizeof(szBuf));
		vDispCenter(_uiGetVLines(), "输错请按[清除]",0);
		szBuf[0]=0;
		if(iInput(INPUT_PIN|INPUT_LEFT, 3, ucPinPos, szBuf, 6, 6, 6, 30)!=6)
		return -1;
		if(strcmp(gl_SysInfo.szSafeSetPwd, szBuf))
		{
			vMessage("密码错");
			return -1;
		}			
		memcpy(gl_SysInfo.szMerchId,buf,15);
		uiMemManaPutSysInfo();
	}

	vClearLines(2);
	_vDisp(2, "终端编号:");	
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%.8s", gl_SysInfo.szPosId);  
	ucPinPos = _uiGetVCols() - strlen(buf);
       	ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT,3, ucPinPos, buf, 8, 8, 8, NULL);
	if(ret < 0)
		return -1;
	if(memcmp(gl_SysInfo.szPosId,buf,8) != 0)
	{
	    vClearLines(2);
		
            if((gl_SysData.uiTransNum + gl_SysData.uiQrTransNum) > 0)
	    {
	        //有未结算的交易,自动结算
	        vDispCenter(2, "有未结算的交易",0 );
	        vDispCenter(3, "需先结算现有交易", 0);
	        iGetKeyWithTimeout(2);
	        if(gl_SysData.uiTransNum || gl_SysData.uiQrTransNum)
	            return -1;
	    }
               
		_vDisp(2, "请输入安全密码:");    
		ucPinPos = _uiGetVCols();
		memset(szBuf, 0x00, sizeof(ucPinPos));
		vDispCenter(_uiGetVLines(), "输错请按[清除]",0);
		szBuf[0]=0;		
		if(iInput(INPUT_PIN|INPUT_LEFT, 3, ucPinPos, szBuf, 6, 6, 6, 30)!=6)
		return -1;
		if(strcmp(gl_SysInfo.szSafeSetPwd, szBuf))
		{
			vMessage("密码错");
			return -1;
		}	
		memcpy(gl_SysInfo.szPosId,buf,8);
		uiMemManaPutSysInfo();
	}
	      
	vClearLines(2);
	_vDisp(2, "商户中文名称:");
#ifdef REMOTE_GBK
    {
        char gbkstr[100];
 //       strcpy((char*)gl_SysInfo.szMerchName, "新乡餐饮（测试商户）荺鋆");
//        strcpy((char*)gl_SysInfo.szMerchName,"新乡餐饮深圳的地址是在南山");
 //	strcpy((char*)gl_SysInfo.szMerchName,"12345678901234567890abcdefghijklmn");
        strcpy(gbkstr, (char*)gl_SysInfo.szMerchName);        
        iGetStringGBK((uchar*)gbkstr, -1);		

    }
#endif 

#if 0
       	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%s",gl_SysInfo.szMerchName);
	dbgHex("gl_SysInfo.szMerchName:",gl_SysInfo.szMerchName,strlen(gl_SysInfo.szMerchName));
	if(strlen((char *)gl_SysInfo.szMerchName)>_uiGetVCols())
		ucPinPos = 0;
	else
		ucPinPos = _uiGetVCols() - strlen(gl_SysInfo.szMerchName);
	ret=iInput(INPUT_TWO_LINE|INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT|INPUT_NULL, 3, ucPinPos, buf, 40, 0, 40, NULL);	
	if(ret < 0)
		return -1;
	if( ret > 0 && memcmp(gl_SysInfo.szMerchName,buf,ret) != 0)
	{
		memcpy(gl_SysInfo.szMerchName,buf,ret);
		uiMemManaPutSysInfo();
	}
#endif	

	dbgHex("gl_SysInfo.szMerchName:",gl_SysInfo.szMerchName,strlen(gl_SysInfo.szMerchName));
	if(strlen((char *)gl_SysInfo.szMerchName)>_uiGetVCols())
	{
               strcpy(szBuf, gl_SysInfo.szMerchName);
                len=iSplitGBStr(szBuf, _uiGetVCols());
                szBuf[len]=0;
                _vDisp(3, szBuf);
                _vDisp(4, gl_SysInfo.szMerchName+len);
	}				
	else
	{
		ucPinPos = _uiGetVCols() - strlen(gl_SysInfo.szMerchName);
               _vDispAt(3, ucPinPos, gl_SysInfo.szMerchName);
	}
	if(!bOK())
            return -1;
			
       	vClearLines(2);
	_vDisp(2, "当前年份: ");		
    	_vGetTime(szDateTime);		
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%.4s",szDateTime); 
	ucPinPos = _uiGetVCols() - strlen(buf);
	ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT|INPUT_NULL,3, ucPinPos, buf, 4, 4, 4, NULL);
	if(ret<0)
		return -1;
    	return 0;
}

int iSetTransParam(void)
{  
	char buf[50];
	int ret, curr;
	long num;
	uchar ucPinPos;
	uchar Nbuff[50];
	uchar ucKeyIn;                                    // 接受输入的字符
        uchar nMode;
	int InputCnt;
       ulong ulAmount;
	
	    char *apszAlphaTable[10] = {
		"0 ",
		"1qzQZ",
		"2abcABC",
		"3defDEF",
		"4ghiGHI",
		"5jklJKL",
		"6mnoMNO",
		"7pqrsPQRS",
		"8tuvTUV",
		"9wxyzWXYZ"};

	vSetAlphaTable((uchar **)apszAlphaTable);
	
	_vCls();
	vDispCenter(1, "系统参数设置", 1);	
	_vDisp(2, "当前流水号: ");		
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%06lu", gl_SysData.ulTTC);  
	ucPinPos = _uiGetVCols() - 6;
	ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT,3, ucPinPos, buf, 6, 6, 6, 30);
	if(ret<0)
		return -1;
	num=atol(buf);
	if(num!=gl_SysData.ulTTC && num>0)
	{
		gl_SysData.ulTTC=num;
		uiMemManaPutSysData();
	}

	vClearLines(2);	
	_vDisp(2, "当前批次号:");		
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%06lu", gl_SysData.ulBatchNo);
	ucPinPos = _uiGetVCols() - 6;
	ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT,3,ucPinPos, buf, 6, 6, 6, 30);
	if(ret<0)
		return -1;
	num=atol(buf);
	if(num!=gl_SysData.ulBatchNo && num>0)
	{
			//批次号不同,判断当前是否有交易,有则先结算再改批次号
		if((gl_SysData.uiTransNum + gl_SysData.uiQrTransNum) > 0)
		{
			_vCls();
			vDispCenter(2, "修改批次号",0 );
			vDispCenter(3, "需先结算现有交易", 0);
			iGetKeyWithTimeout(2);
			if((gl_SysData.uiTransNum + gl_SysData.uiQrTransNum) > 0)
				return 0;
			vSetExitToMainFlag(0);

			//恢复当前页面内容
			_vCls();
			vDispCenter(1, "交易参数设置", 1);
			vDispVarArg(2, "流水号:%06lu", gl_SysData.ulTTC);        //ttc在结算时变了,以当前实际为准
			vDispVarArg(3, "批次号:%06lu", num);
		}

		gl_SysData.ulBatchNo=num;
		uiMemManaPutSysData();
	}

        vClearLines(2);
	curr= (gl_SysInfo.ucSmFlag== 1)? 1:2;
	ret=iSelectOne("是否支持国密", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if( ret!= curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucSmFlag=ret;
	    uiMemManaPutSysInfo();
	}

        vClearLines(2);
	curr= (gl_SysInfo.magEncrypt== 1)? 1:2;
	ret=iSelectOne("磁道加密设置", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if( ret!= curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.magEncrypt=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucAmtInPutMode== 1)? 1:2;
	ret=iSelectOne("金额输入单位", "1.元", "2.分", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if( ret!= curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucAmtInPutMode=ret;
	    uiMemManaPutSysInfo();
	}
	
#ifdef ENABLE_PRINTER 
	vClearLines(2);
	curr= (gl_SysInfo.ucPrintAcquirer == 1)? 1:2;
	ret=iSelectOne("是否打印中文收单行", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
		return -1;
	else if(ret!=curr)
	{
	       ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucPrintAcquirer=ret;
		uiMemManaPutSysInfo();
	}

        vClearLines(2);
	curr= (gl_SysInfo.ucPrintIssuer == 1)? 1:2;
	ret=iSelectOne("是否打印中文发卡行", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if( ret!= curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucPrintIssuer=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	_vDisp(2, "签购单设置:");	
#ifdef REMOTE_GBK
    {
        char gbkstr[100];
  //      strcpy((char*)gl_SysInfo.ucPrintTitle,"新乡餐饮深圳的地址是在鹮昇");
 //	strcpy((char*)gl_SysInfo.ucPrintTitle,"12345678901234567890abcdefghijklmn");
        strcpy(gbkstr, (char*)gl_SysInfo.ucPrintTitle);        
        iGetStringGBK((uchar*)gbkstr, -1);	
    }
#endif	

#if 0
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%s",gl_SysInfo.ucPrintTitle);	
	if(strlen((char *)gl_SysInfo.ucPrintTitle)>_uiGetVCols())
		ucPinPos = 0;
	else
		ucPinPos = _uiGetVCols() - strlen(gl_SysInfo.ucPrintTitle);
	ret=iInput(INPUT_TWO_LINE|INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT|INPUT_NULL,3, ucPinPos,buf, 40, 0, 40, 30);	
	if(ret<0)
        	return -1;
	if( ret > 0 && strcmp(buf,gl_SysInfo.ucPrintTitle))
	{        
	        memset(gl_SysInfo.ucPrintTitle,0x00,sizeof(gl_SysInfo.ucPrintTitle));
		memcpy(gl_SysInfo.ucPrintTitle,buf,ret);
		uiMemManaPutSysInfo();
	}
#endif

       	dbgHex("gl_SysInfo.ucPrintTitle:",gl_SysInfo.ucPrintTitle,strlen(gl_SysInfo.ucPrintTitle));
	if(strlen((char *)gl_SysInfo.ucPrintTitle)>_uiGetVCols())
	{
               strcpy(szBuf, gl_SysInfo.ucPrintTitle);
                len=iSplitGBStr(szBuf, _uiGetVCols());
                szBuf[len]=0;
                _vDisp(3, szBuf);
                _vDisp(4, gl_SysInfo.ucPrintTitle+len);
	}				
	else
	{
		ucPinPos = _uiGetVCols() - strlen(gl_SysInfo.ucPrintTitle);
               _vDispAt(3, ucPinPos, gl_SysInfo.ucPrintTitle);
	}
	if(!bOK())
            return -1;

	vClearLines(2);
	_vDisp(2, "打印张数设置(1-3):");	
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%d", gl_SysInfo.ucPrinterAttr);
	ucPinPos = _uiGetVCols() - 1;	
	while(1)
	{
		ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT,3,ucPinPos, buf, 1, 0, 1, NULL);
		if(ret<0)
			return -1;
		if(ret == 0 ) break;		
		if(buf[0] < '1' || buf[0] > '3') continue;		
		if( gl_SysInfo.ucPrinterAttr  != (buf[0] - '0'))
		{      
			gl_SysInfo.ucPrinterAttr = buf[0] - '0';
			uiMemManaPutSysInfo();
			break;
		}
		break;
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucPrintEN == 1)? 1:2;
	ret=iSelectOne("签购单是否打印英文", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if(ret != curr)
	{
		ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucPrintEN=ret;
		uiMemManaPutSysInfo();
	}
#endif

       	vClearLines(2);
	curr= (gl_SysInfo.ucDoReverseFlag== 1)? 1:2;
	ret=iSelectOne("是否支持立即冲正", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if( ret!= curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucDoReverseFlag=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	_vDisp(2, "失败电签存储笔数: ");
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%d", gl_SysInfo.ucFailSignMax);  
	ucPinPos = _uiGetVCols() - strlen(buf);		
	ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 2, 1, 2, 30);
	if(ret<0)
		return -1;
	num=atol(buf);
	if(num!=gl_SysInfo.ucFailSignMax && num>0)
	{
		gl_SysInfo.ucFailSignMax = num;
		uiMemManaPutSysInfo();
	}
	
	
	vClearLines(2);
	_vDisp(2, "最大冲正次数(1-3):");	
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%d", gl_SysInfo.ucMaxResendCount);
	ucPinPos = _uiGetVCols() - 1;
	while(1)
	{
		ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT,3,ucPinPos, buf, 1, 0, 1, NULL);
		if(ret<0)
			return -1;	
		if(ret == 0 ) break;	
		num=atol(buf);
		if(num < 1 || num> 3) continue;	
		if(num!= gl_SysInfo.ucMaxResendCount && num>0)
		{
			gl_SysInfo.ucMaxResendCount=num;
			uiMemManaPutSysInfo();
			break;
		}
		break;
	}	

        vClearLines(2);
	curr= (gl_SysInfo.ucCleanReverseFlag== 1)? 1:2;
	ret=iSelectOne("是否开启隔日删除冲正", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if( ret!= curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucCleanReverseFlag=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	_vDisp(2, "隔日删除冲正时间设置:");
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%.4s", gl_SysInfo.ucTimeCleanReverse);  
	ucPinPos = _uiGetVCols() - strlen(buf);		
	ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 4, 4, 4, 30);
	if(ret<0)
		return -1;
	if (strcmp((char *)gl_SysInfo.ucTimeCleanReverse, (char *)buf) != 0)
	{
		strcpy(gl_SysInfo.ucTimeCleanReverse,buf);
		uiMemManaPutSysInfo();
	}
	
	vClearLines(2);
	_vDisp(2, "最大交易笔数设置:");
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%d", gl_SysInfo.ucMaxTradeCnt);  
	ucPinPos = _uiGetVCols() - strlen(buf);		
	ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 3, 0, 3, 30);
	if(ret<0)
		return -1;
	num=atol(buf);
	if(num!=gl_SysInfo.ucMaxTradeCnt && num>0)
	{
		gl_SysInfo.ucMaxTradeCnt = num;
		uiMemManaPutSysInfo();
	}
	
   	vClearLines(2);
   	_vDisp(2, "最大退货金额设置:");	
	memset(Nbuff,0x00,sizeof(Nbuff));
	sprintf(Nbuff, "%lu.%02lu", gl_SysInfo.ulRefund_limit/100, gl_SysInfo.ulRefund_limit%100);
         ucPinPos =_uiGetVCols()-strlen(Nbuff);
	_vDispAt(3,ucPinPos,Nbuff);
	
	_vFlushKey();
	nMode = 0x00;
	ulAmount = gl_SysInfo.ulRefund_limit;
	InputCnt = 0;
        while(1)
    	{
    		if(nMode)
		{
			nMode=0x00;		
			memset(Nbuff,0x00,sizeof(Nbuff));
			sprintf(Nbuff,"%.2f", ulAmount/100.00); 					
			ucPinPos =_uiGetVCols()-strlen(Nbuff);
			_vDisp(3," ");
			_vDispAt(3,ucPinPos,Nbuff);
		}
    		if(_uiKeyPressed())
		{
			ucKeyIn=_uiGetKey();
			if(_KEY_ENTER== ucKeyIn)
			{			      
				break;	
			}
			else if(_KEY_CANCEL== ucKeyIn)
			{
				return 0;
			}
			else if((ucKeyIn >= _KEY_0)&&(ucKeyIn <= _KEY_9)&&(InputCnt<9))
			{
				ulAmount = ulAmount * 10 + (ucKeyIn - _KEY_0);
				nMode=0xaa;
				InputCnt++;
			}
			else if((_KEY_BKSP== ucKeyIn)&& (ulAmount > 0))
			{
				ulAmount =ulAmount / 10;
				nMode=0xaa;				
				if(InputCnt)
					InputCnt--;
			}
			else
			{
				nMode=0x00;
			}
		}
    	}
	if(ulAmount != gl_SysInfo.ulRefund_limit && ulAmount >0)
	{
		gl_SysInfo.ulRefund_limit = ulAmount;
		uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucDefaultTrans == 1)? 1:2;
	ret=iSelectOne("默认交易设置", "1.消费", "2.预授权", "", curr, 30, 1);
	if(ret<0)
		return -1;
	if( ret != curr)
	{
		ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucDefaultTrans=ret;
		uiMemManaPutSysInfo();
	}

#ifdef ENABLE_PRINTER 
	vClearLines(2);
	curr= (gl_SysInfo.ucPrtNegative == 1)? 1:2;
	ret=iSelectOne("撤销类交易打负号", "1.打印", "2.不打印", "", curr, 30, 1);
	if(ret<0)
	    	return -1;
	if( ret != curr)
	{
		ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucPrtNegative=ret;
		uiMemManaPutSysInfo();
	}
#endif

	vClearLines(2);
	curr= (gl_SysInfo.ucAuthCardMask == 1)? 1:2;
	ret=iSelectOne( "预授权屏蔽卡号", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    	return -1;
	if(ret != curr)
	{
		ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucAuthCardMask=ret;
		uiMemManaPutSysInfo();
	}

	vClearLines(2);
	gl_SysInfo.ucKeyBeep=_cGetKeyVoice();
	dbg("gl_SysInfo.ucKeyBeep111:%d\n",gl_SysInfo.ucKeyBeep);
	curr= (gl_SysInfo.ucKeyBeep == 1)? 1:2;
	ret=iSelectOne("设置按键音:", "1.开", "2.关", "", curr, 30, 1);
	if(ret<0)
	    	return -1;
	if(curr != ret)
	{
		ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucKeyBeep=ret;
		uiMemManaPutSysInfo();
		dbg("gl_SysInfo.ucKeyBeep222:%d\n",gl_SysInfo.ucKeyBeep);
		_vSetKeyVoice(gl_SysInfo.ucKeyBeep);        
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucSupportESign == 1)? 1:2;
	ret=iSelectOne("支持电子签名", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    	return -1;
	if(curr != ret)
	{		
	        ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucSupportESign=ret;
		uiMemManaPutSysInfo(); 
	}

	vClearLines(2);
	_vDisp(2, "签名重发次数(1-3):");	
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%d", gl_SysInfo.uiESignSendCount);
	ucPinPos = _uiGetVCols() - 1;
	while(1)
	{
		ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT,3,ucPinPos, buf, 1, 0, 1, NULL);
		if(ret<0)
			return -1;	
		if(ret == 0 ) break;	
		num=atol(buf);
		if(num < 1 || num> 3) continue;	
		if(num!= gl_SysInfo.uiESignSendCount && num>0)
		{
			gl_SysInfo.uiESignSendCount=num;
			uiMemManaPutSysInfo();
			break;
		}
		break;
	}	
	
	vClearLines(2);
	curr= (gl_SysInfo.ucNFCTransPrior == 1)? 1:2;
	ret=iSelectOne("优先挥卡交易", "1.打开挥卡优先", "2.关闭挥卡优先", "", curr, 30, 1);
	if(ret<0)
	    	return -1;
	if(ret  !=  curr)
	{
		 ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucNFCTransPrior=ret;
		uiMemManaPutSysInfo();
	}

	vClearLines(2);	
	curr= (gl_SysInfo.ucOnlinefirst == 1)? 1:2;
	ret=iSelectOne( "非接交易通道开关", "1.脱机优先", "2.联机优先", "", curr, 30, 1);
	if(ret<0)
		return -1;
	if(ret!=curr)
	{
	         ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucOnlinefirst=ret;
		uiMemManaPutSysInfo();
	}

	vClearLines(2);
	_vDisp(2, "闪卡当笔重刷处理时间:");
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%lu", gl_SysInfo.ulFlashProcPerTransTime);		
	ucPinPos = _uiGetVCols() - strlen(buf);
	ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 3, 0, 3, 30);
	if(ret<0)
		return -1;
	num=atol(buf);
	if(num!=gl_SysInfo.ulFlashProcPerTransTime && num>0)
	{
		gl_SysInfo.ulFlashProcPerTransTime = num;
		uiMemManaPutSysInfo();
	}

	vClearLines(2);
	_vDisp(2, "闪卡记录可处理时间:");
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%lu", gl_SysInfo.ulFlashExpire);	
	ucPinPos = _uiGetVCols() - strlen(buf);
	ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 3, 0, 3, 30);
	if(ret<0)
		return -1;
	num=atol(buf);
	if(num!=gl_SysInfo.ulFlashExpire && num>0)
	{
		gl_SysInfo.ulFlashExpire = num;
		uiMemManaPutSysInfo();
	}

          vClearLines(2);
   	_vDisp(2, "免密限额:");	
	memset(Nbuff,0x00,sizeof(Nbuff));
	sprintf(Nbuff, "%lu.%02lu", gl_SysInfo.ulNoPinLimit/100, gl_SysInfo.ulNoPinLimit%100);
         ucPinPos =_uiGetVCols()-strlen(Nbuff);
	_vDispAt(3,ucPinPos,Nbuff);
	
	_vFlushKey();
	nMode = 0x00;
	ulAmount = gl_SysInfo.ulNoPinLimit;
	InputCnt = 0;
        while(1)
    	{
    		if(nMode)
		{
			nMode=0x00;		
			memset(Nbuff,0x00,sizeof(Nbuff));
			sprintf(Nbuff,"%.2f", ulAmount/100.00); 					
			ucPinPos =_uiGetVCols()-strlen(Nbuff);
			_vDisp(3," ");
			_vDispAt(3,ucPinPos,Nbuff);
		}
    		if(_uiKeyPressed())
		{
			ucKeyIn=_uiGetKey();
			if(_KEY_ENTER== ucKeyIn)
			{			      
				break;	
			}
			else if(_KEY_CANCEL== ucKeyIn)
			{
				return 0;
			}
			else if((ucKeyIn >= _KEY_0)&&(ucKeyIn <= _KEY_9)&&(InputCnt<9))
			{
				ulAmount = ulAmount * 10 + (ucKeyIn - _KEY_0);
				nMode=0xaa;
				InputCnt++;
			}
			else if((_KEY_BKSP== ucKeyIn)&& (ulAmount > 0))
			{
				ulAmount =ulAmount / 10;
				nMode=0xaa;				
				if(InputCnt)
					InputCnt--;
			}
			else
			{
				nMode=0x00;
			}
		}
    	}
	if(ulAmount != gl_SysInfo.ulNoPinLimit && ulAmount >0)
	{
		gl_SysInfo.ulNoPinLimit = ulAmount;
		uiMemManaPutSysInfo();
	}

	
	vClearLines(2);
	curr= (gl_SysInfo.ucNoPinFlag == 1)? 1:2;
	ret=iSelectOne("非接快速业务标识", "1.启用", "2.关闭", "", curr, 30, 1);
	if(ret<0)
		return -1;
	if(ret!=curr)
	{
	        ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucNoPinFlag=ret;
		uiMemManaPutSysData();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucBINAFlag == 1)? 1:2;
	ret=iSelectOne( "BIN表A标识", "1.启用", "2.关闭", "", curr, 30, 1);
	if(ret<0)
		return -1;
	if(ret != curr)
	{
	         ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucBINAFlag=ret;
		uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucBINBFlag == 1)? 1:2;
	ret=iSelectOne( "BIN表B标识", "1.启用", "2.关闭", "", curr, 30, 1);
	if(ret<0)
		return -1;
	if(ret!=curr)
	{
	       ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucBINBFlag=ret;
		uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucCDCVMFlag == 1)? 1:2;
	ret=iSelectOne("CDCVM标识", "1.启用", "2.关闭", "", curr, 30, 1);
	if(ret<0)
		return -1;
	if(ret != curr)
	{
		ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucCDCVMFlag=ret;
		uiMemManaPutSysInfo();
	}

        vClearLines(2);
   	_vDisp(2, "免签限额:");	
	memset(Nbuff,0x00,sizeof(Nbuff));
	sprintf(Nbuff, "%lu.%02lu", gl_SysInfo.ulNoSignLimit/100, gl_SysInfo.ulNoSignLimit%100);
         ucPinPos =_uiGetVCols()-strlen(Nbuff);
	_vDispAt(3,ucPinPos,Nbuff);
	
	_vFlushKey();
	nMode = 0x00;
	ulAmount = gl_SysInfo.ulNoSignLimit;
	InputCnt = 0;
        while(1)
    	{
    		if(nMode)
		{
			nMode=0x00;		
			memset(Nbuff,0x00,sizeof(Nbuff));
			sprintf(Nbuff,"%.2f", ulAmount/100.00); 					
			ucPinPos =_uiGetVCols()-strlen(Nbuff);
			_vDisp(3," ");
			_vDispAt(3,ucPinPos,Nbuff);
		}
    		if(_uiKeyPressed())
		{
			ucKeyIn=_uiGetKey();
			if(_KEY_ENTER== ucKeyIn)
			{			      
				break;	
			}
			else if(_KEY_CANCEL== ucKeyIn)
			{
				return 0;
			}
			else if((ucKeyIn >= _KEY_0)&&(ucKeyIn <= _KEY_9)&&(InputCnt<9))
			{
				ulAmount = ulAmount * 10 + (ucKeyIn - _KEY_0);
				nMode=0xaa;
				InputCnt++;
			}
			else if((_KEY_BKSP== ucKeyIn)&& (ulAmount > 0))
			{
				ulAmount =ulAmount / 10;
				nMode=0xaa;				
				if(InputCnt)
					InputCnt--;
			}
			else
			{
				nMode=0x00;
			}
		}
    	}
	if(ulAmount != gl_SysInfo.ulNoSignLimit && ulAmount >0)
	{
		gl_SysInfo.ulNoSignLimit = ulAmount;
		uiMemManaPutSysInfo();
	}

   	vClearLines(2);
	curr= (gl_SysInfo.ucNoSignFlag == 1)? 1:2;
	ret=iSelectOne("免签标识", "1.启用", "2.关闭", "", curr, 30, 1);
	if(ret<0)
		return -1;
	if(ret!= curr)
	{
		ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucNoSignFlag=ret;
		uiMemManaPutSysData();
	}

	return 0;
}


//sheet-第几联  0-无 1-商户联  2-收单机构3-持卡人
//dup - 重打标志 0-正常 1-重打
//QrRec - 交易记录
//pszReference - 备注信息,8583的63.2
int iPrintTransSheet(int sheet, int dup, stTransRec *Rec, uchar *pszReference)
{   
    char szBuf[100], szTmp[100];
    int uiEntryMode;
    int len;
    int iPrtRet;
    uchar text[10]={0};
    int ucI,ucJ;
   
    if(_uiPrintOpen())
        return 1;
    
    _uiPrintSetFont(1);
    if(strlen(gl_SysInfo.ucPrintTitle ) > 0)
		_uiPrintEx(2,gl_SysInfo.ucPrintTitle, 0);
    else
    		_uiPrintEx(2, "POS签购单", 0);	

    _uiPrintSetFont(2);		
    memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    if(sheet>=1 && sheet<=3)
    {
        if(sheet==1)
       {       
       		strcpy(szTmp, "商户存根");
		if(gl_SysInfo.ucPrintEN== 1) strcat(szTmp, "(MERCHANT COPY)");			
        }		
	   	
        else if(sheet==2)
        {
            strcpy(szTmp, "银行存根");
	    if(gl_SysInfo.ucPrintEN== 1) strcat(szTmp, "(BANK COPY)");	
    	}
        else		
        {
            strcpy(szTmp, "持卡人存根");
	    if(gl_SysInfo.ucPrintEN== 1) strcat(szTmp, "(CARDHOLDER COPY)");	
    	}		
        len=strlen(szTmp);
        memcpy(szBuf+(strlen(szBuf)-len)/2, szTmp, len);
    }
    _uiPrintEx(1, szBuf, 0);    

    
    if(Rec->szPrtMerchName[0])		
    	strcpy(szBuf, Rec->szPrtMerchName);
    else
	strcpy(szBuf, gl_SysInfo.szMerchName);
    
    #if 0
    strcpy(szTmp, "商户名称:");
    if(strlen(szBuf)<_uiGetVLines())
    {
        if(9+strlen(szBuf)<=_uiGetVLines())
        {
            strcat(szTmp, szBuf);
            _uiPrintEx(1, szTmp, 0);
        }else
        {
            _uiPrintEx(1, szTmp, 0);
            _uiPrintEx(2, szBuf, 0);
        }
    }else
    {
        strcat(szTmp, szBuf);
        len=iSplitGBStr(szTmp, _uiGetPCols());
        strcpy(szBuf, szTmp+len);
        szTmp[len]=0;
        _uiPrintEx(1, szTmp, 0);
        _uiPrintEx(2, szBuf, 0);
    }
    #else
    if(gl_SysInfo.ucPrintEN== 1) 
	        _uiPrintEx(1, "商户名称(MERCHANT NAME):", 0);
    else 
		_uiPrintEx(1, "商户名称:", 0);
   
   _uiPrintEx(11, szBuf,0);  
   
    #endif

    if(gl_SysInfo.ucPrintEN== 1) 
		_uiPrintEx(1, "商户编号(MERCHANT NO.):", 0);
    else
		_uiPrintEx(1, "商户编号:", 0);
    if(Rec->sPrtMerchId[0])
     {

	 	_uiPrintEx(1, Rec->sPrtMerchId,0);
    	}	 
    else
    {
		 _uiPrintEx(1,  gl_SysInfo.szMerchId,0);
    }	

     if(gl_SysInfo.ucPrintEN== 1) 
    {
	    	if(Rec->sPrtPosId[0])
			 	sprintf(szBuf, "终端编号(TERMINAL NO.):%s", Rec->sPrtPosId);
		else	
				sprintf(szBuf, "终端编号(TERMINAL NO.):%s", gl_SysInfo.szPosId);
     }	
    else
    {
    	if(Rec->sPrtPosId[0])
		sprintf(szBuf, "终端编号:%s", Rec->sPrtPosId);	
        else
    		sprintf(szBuf, "终端编号:%s", gl_SysInfo.szPosId);
    }
	
    _uiPrintEx(1, szBuf, 0);	

      vOneTwo0(Rec->sOperNo, 1, szTmp);
    if(gl_SysInfo.ucPrintEN== 1) 
		sprintf(szBuf, "操作员号(OPERATOR NO.):%2.2s", szTmp);
    else
		sprintf(szBuf,"操作员号:%2.2s", szTmp);     	
    _uiPrintEx(1, szBuf, 0);	 

     if(gl_SysInfo.ucPrintEN== 1) 
      {
               if(gl_SysInfo.ucPrintIssuer == 1)
               	{             dbgHex("Rec->issuerBankId", Rec->issuerBankId, 8);
               			memset(text,0x00,sizeof(text));
				GetBankName(Rec->issuerBankId,text);
				sprintf(szBuf, "发卡行(ISSUER):%s", text);
				_uiPrintEx(1, szBuf, 0);
               	}
      		else 
		{
				sprintf(szBuf, "发卡行(ISSUER):%s", Rec->issuerBankId);
				_uiPrintEx(1, szBuf, 0);
      		}		

                 if(gl_SysInfo.ucPrintAcquirer == 1)
               	{              dbgHex("Rec->recvBankId", Rec->recvBankId, 8);
               			memset(text,0x00,sizeof(text));
				get_bankno(Rec->recvBankId,text);
				sprintf(szBuf, "收单行(ACQUIRER):%s", text);
				_uiPrintEx(1, szBuf, 0);
               	}
      		else 
		{
			sprintf(szBuf, "收单行(ACQUIRER):%s", Rec->recvBankId);
			_uiPrintEx(1, szBuf, 0);
      		}
      	}
	else 
	{
      		 if(gl_SysInfo.ucPrintIssuer == 1)
               	{
               			memset(text,0x00,sizeof(text));
				GetBankName(Rec->issuerBankId,text);
				sprintf(szBuf, "发卡行:%s", text);
				_uiPrintEx(1, szBuf, 0);
               	}
      		else 
		{
				sprintf(szBuf, "发卡行:%s", Rec->issuerBankId);
				_uiPrintEx(1, szBuf, 0);
      		}		

                 if(gl_SysInfo.ucPrintAcquirer == 1)
               	{
               			memset(text,0x00,sizeof(text));
				get_bankno(Rec->recvBankId,text);
				sprintf(szBuf, "收单行:%s", text);
				_uiPrintEx(1, szBuf, 0);
               	}
      		else 
		{
			sprintf(szBuf, "收单行:%s", Rec->recvBankId);
			_uiPrintEx(1, szBuf, 0);
      		}
      	}	

#if 0
    szBuf[0]=0;
    if(Rec->ucCardClass<=3)
    {
        if(gl_SysInfo.ucPrintEN== 1) 
       	 	strcpy(szBuf, "卡类型(CARD TYPE):");
	else
		strcpy(szBuf, "卡类型:");
        switch(Rec->ucCardClass)
        {
            case 0:
                strcat(szBuf, "境内借记卡 ");
                break;
            case 1:
                strcat(szBuf, "境内贷记卡 ");
                break;
            default:
                strcat(szBuf, "           ");
                break;
        }
    }
   _uiPrintEx(1, szBuf, 0);
#endif
   
   if(gl_SysInfo.ucPrintEN== 1) 
   	_uiPrintEx(1, "卡  号(CARD NO.):", 0);      //切换字体前需打印之前的缓存
   else	
    	_uiPrintEx(1, "卡  号:", 0);      //切换字体前需打印之前的缓存
    vUnpackPan(Rec->sPan, szBuf);
		dbg("szBuf:%s\n",szBuf);
    if(Rec->uiTransType != TRANS_TYPE_PREAUTH || (Rec->uiTransType == TRANS_TYPE_PREAUTH  && gl_SysInfo.ucAuthCardMask == 1))
   {   	
   	memset(szBuf+6, '*', strlen(szBuf)-6-4);
    }	
	memset(szTmp, ' ', sizeof(szTmp));
	for(ucI= 0,ucJ = 0;ucI < strlen(szBuf);ucI++,ucJ++)
	{
		if((!(ucI%4)) && (ucI != 0))
		{
			szTmp[ucJ] = ' ';
			++ucJ;
		}
		szTmp[ucJ] = szBuf[ucI];
	}

    uiEntryMode=(Rec->uiEntryMode%100)/10;
    if(uiEntryMode==5)
    {
        strcpy(&szTmp[ucJ], "/I");        //IC
    }else if(uiEntryMode==7)
        strcpy(&szTmp[ucJ], "/C");        //非接
    else if(uiEntryMode==2)
        strcpy(&szTmp[ucJ], "/S");        //磁条
    else
        strcpy(&szTmp[ucJ], "/M");        //手输
    _uiPrintSetFont(1);
    _uiPrintEx(1, szTmp, 0);    //卡号用大号字体
    if(strlen(szTmp) > _uiGetPCols())	 
		_uiPrintEx(1, szTmp+_uiGetPCols(), 0);
    _uiPrintSetFont(2);

   if(Rec->uiTransType == TRANS_TYPE_PREAUTH) 
   {
	    szBuf[0]=0;
	    if(Rec->sCardExpire[0] && Rec->sCardExpire[1])
	    {
	          vOneTwo0(Rec->sCardExpire, 2, szTmp);
	    	 if(gl_SysInfo.ucPrintEN== 1)        	 	
	       		 sprintf(szBuf, "有效期(EXP DATE):%.2s/%.2s", szTmp, szTmp+2);
		else
			 sprintf(szBuf, "有效期:%.2s/%.2s", szTmp, szTmp+2);
	    }	
	    if(szBuf[0])
	        _uiPrintEx(1, szBuf, 0);
   }

    if(gl_SysInfo.ucPrintEN== 1)
	_uiPrintEx(1, "交易类型(TRANS TYPE):", 0);	
    else		
    	_uiPrintEx(1, "交易类型:", 0);
    switch (Rec->uiTransType)
    {
    case TRANS_TYPE_SALE:
    case TRANS_TYPE_DAIRY_SALE:		
        strcpy(szBuf, "消费");
	if(gl_SysInfo.ucPrintEN== 1) strcat(szBuf, "(SALE)");	
        break;
//   case TRANS_TYPE_DAIRY_SALE:
//	strcpy(szBuf, "日结消费");
//	if(gl_SysInfo.ucPrintEN== 1) strcat(szBuf, "(DAIRY SALE)");	
//	break;		
    case TRANS_TYPE_SALEVOID:
        strcpy(szBuf, "消费撤销");
	if(gl_SysInfo.ucPrintEN== 1) strcat(szBuf, "(VOID)");
        break;
    case TRANS_TYPE_PREAUTH:
        strcpy(szBuf, "预授权");
       if(gl_SysInfo.ucPrintEN== 1) strcat(szBuf, "(AUTH)");
        break;
    case TRANS_TYPE_PREAUTHVOID:
        strcpy(szBuf, "预授权撤销");
       if(gl_SysInfo.ucPrintEN== 1) strcat(szBuf, "(CANCEL)");
        break;
    case TRANS_TYPE_PREAUTH_COMP:
        strcpy(szBuf, "预授权完成");
        if(gl_SysInfo.ucPrintEN== 1) strcat(szBuf, "(AUTH COMP)");
        break;
    case TRANS_TYPE_PREAUTH_COMPVOID:
        strcpy(szBuf, "预授权完成撤销");
	 if(gl_SysInfo.ucPrintEN== 1) strcat(szBuf, "(COMPLETE VOID)");	
        break;
    case TRANS_TYPE_REFUND:
        strcpy(szBuf, "退货");
	if(gl_SysInfo.ucPrintEN== 1) strcat(szBuf, "(REFUND)");	
        break;    
    default:
        strcpy(szBuf, "未知交易");
	if(gl_SysInfo.ucPrintEN== 1) strcat(szBuf, "(UNKNOWN)");		
        break;
    }
    _uiPrintSetFont(1);
    _uiPrintEx(11, szBuf, 0);    //交易类型用大号字体
    _uiPrintSetFont(2);

    memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    _uiPrintEx(1, szBuf, 0);

   if(gl_SysInfo.ucPrintEN== 1) 
   {
   	sprintf(szBuf, "批次号(BATCH NO.):%06lu", gl_SysData.ulBatchNo);
	 _uiPrintEx(1, szBuf, 0);
	 sprintf(szBuf, "凭证号(VOUCHER NO.):%06lu", Rec->ulTTC);
	  _uiPrintEx(1, szBuf, 0);
   }
   else
   {
   	sprintf(szBuf, "批次号:%06lu", gl_SysData.ulBatchNo);
	 _uiPrintEx(1, szBuf, 0);
	 sprintf(szBuf, "凭证号:%06lu", Rec->ulTTC);
	  _uiPrintEx(1, szBuf, 0);
   }
   
    vOneTwo0(Rec->sDateTime, 6, szTmp);
   if(gl_SysInfo.ucPrintEN== 1) 
   {
	   if(Rec->uiTransType == TRANS_TYPE_PREAUTH)
	  {
		_uiPrintEx(1, "授权码(AUTH NO.):", 0);
		sprintf(szBuf, "%6.6s", Rec->sAuthCode);
	  	_uiPrintSetFont(1);	  
	  	  _uiPrintEx(1, szBuf, 0); 
	   	 _uiPrintSetFont(2); 	
	   }else
	   {
		sprintf(szBuf, "授权码(AUTH NO.):%6.6s", Rec->sAuthCode);
	   	_uiPrintEx(1, szBuf, 0); 
	   }
	   
    sprintf(szBuf, "参考号(REFER NO.):%.12s", Rec->sReferenceNo);
      _uiPrintEx(1, szBuf, 0);  
    _uiPrintEx(1, "日期时间(DATE/TIME):", 0);
    sprintf(szBuf, "         20%.2s/%.2s/%.2s %.2s:%.2s:%.2s", szTmp, szTmp+2, szTmp+4, szTmp+6, szTmp+8, szTmp+10);
    _uiPrintEx(1, szBuf, 0);	
    _uiPrintEx(1, "交易金额(AMOUNT):", 0);
   }
   else	
   {
	    if(Rec->uiTransType == TRANS_TYPE_PREAUTH)
	    {
			_uiPrintEx(1, "授权码:", 0);
			sprintf(szBuf, "%6.6s", Rec->sAuthCode);	
		    _uiPrintSetFont(1);
		    _uiPrintEx(1, szBuf, 0); 
		    _uiPrintSetFont(2);	 
	    }else
	    {
		sprintf(szBuf, "授权码:%6.6s", Rec->sAuthCode);	
	    	 _uiPrintEx(1, szBuf, 0); 
	    }
		
    sprintf(szBuf, "参考号:%.12s", Rec->sReferenceNo);
    _uiPrintEx(1, szBuf, 0);    
    _uiPrintEx(1, "日期时间:", 0);
    sprintf(szBuf, "         20%.2s/%.2s/%.2s %.2s:%.2s:%.2s", szTmp, szTmp+2, szTmp+4, szTmp+6, szTmp+8, szTmp+10);
    _uiPrintEx(1, szBuf, 0);
    _uiPrintEx(1, "交易金额:", 0);
}
   if( gl_SysInfo.ucPrtNegative == 1   && (Rec->uiTransType == TRANS_TYPE_SALEVOID || Rec->uiTransType == TRANS_TYPE_PREAUTHVOID || Rec->uiTransType == TRANS_TYPE_PREAUTH_COMPVOID||Rec->uiTransType == TRANS_TYPE_REFUND ))
        sprintf(szBuf, "RMB:-%lu.%02lu", Rec->ulAmount/100, Rec->ulAmount%100);
   else
   	 sprintf(szBuf, "RMB:%lu.%02lu", Rec->ulAmount/100, Rec->ulAmount%100);
    _uiPrintSetFont(1);
    _uiPrintEx(1, szBuf, 0);    //金额用大号字体
    _uiPrintSetFont(2);

    memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    _uiPrintEx(1, szBuf, 0);

 if(gl_SysInfo.ucPrintEN== 1) 
  	_uiPrintEx(1,"备注(REFERENCE):",0);
 else
    	_uiPrintEx(1, "备注:", 0);
/*        
1、	原交易凭证信息：撤销交易，打印原交易凭证号；退货交易，打印原交易参考号和原交易日期。
2、	预授权码：预授权撤销、预授权完成（请求）、预授权完成（请求）撤销时输入的原预授权码。
3、	重打印标志：重打印功能印出的凭证，必须在备注栏打印该标志。
4、	收单方需要持卡人了解的信息（域63.2.3）。
*/
    if(Rec->uiTransType==TRANS_TYPE_SALEVOID)
    {
         sprintf(szBuf, "原交易凭证号:%06lu", Rec->ulVoidTTC);
        _uiPrintEx(1, szBuf, 0);
    }
    if(Rec->uiTransType==TRANS_TYPE_REFUND)
    {
        vOneTwo0(Rec->sVoidInfo, 8, szTmp);
        sprintf(szBuf, "原参考号:%.12s", szTmp);
        _uiPrintEx(1, szBuf, 0);
        sprintf(szBuf, "原交易日期:%.4s", szTmp+12);
        _uiPrintEx(1, szBuf, 0);
    }
    if(Rec->uiTransType==TRANS_TYPE_PREAUTHVOID || Rec->uiTransType==TRANS_TYPE_PREAUTH_COMP
            || Rec->uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
    {
        sprintf(szBuf, "原授权码:%.6s", Rec->sVoidInfo);
        _uiPrintEx(1, szBuf, 0);
    }
#if 0
    if(pszReference && pszReference[0])     //Field63.2
    {
        /*
        63.2该域为一定长域，长度值由右靠的BCD码表示，最大到120个字节
        该域由四个子域构成，具体描述如下：
        ——	63.2.1      发卡方保留域			ANS20
        ——	63.2.2      中国银联保留域			ANS20
        ——	63.2.3      受理机构保留域			ANS20
        ——	63.2.4      POS终端保留域			ANS60
        */
       uchar *p;
       int k;

        dbg("Field63.2:len=[%d],[%s]\n", strlen(pszReference), pszReference);
       //63.2.1
       p=pszReference;
       for(k=0; k<4 && p[0] && k*20<strlen(pszReference); k++)
       {
           if(k==3)
                vMemcpy0(szBuf, p, 60);
            else
                vMemcpy0(szBuf, p, 20);
            rtrim(szBuf);
            if(szBuf[0])
                _uiPrintEx(1, szBuf, 0);
            p+=20;
       }
    }
#endif	
    if(dup)
    {
        _uiPrintSetFont(1);
	if(gl_SysInfo.ucPrintEN== 1) 
		_uiPrintEx(2, "重打印/DUPLICATED", 0); 
	else	
        	_uiPrintEx(2, "重打印", 0);
        _uiPrintSetFont(2);
    }

    if(uiEntryMode==5 || uiEntryMode==7)
    {
        //sdj aid参数没有9F09,但有9F08,需将9F08(TAG_ICC_APP_VERSION_NUMBER)替换为9F09(TAG_APP_VERSION_NUMBER)
//        byte tmp[50];
        int tagBinLen;
        TagList tmpTagList;
        Tag3ByteList tmpTag3ByteList;
//        uint16 tmpTLVBufLen;
        ushort tag;

        InitTagList(&tmpTagList);
        InitTag3ByteList(&tmpTag3ByteList);
        
        BuildTagListOneLevelBctc(Rec->sIcData, Rec->uiIcDataLen, &tmpTagList, &tmpTag3ByteList);

        tag=0x9F26;
        if(GetTagValueSize(&tmpTagList, tag))
        {
            strcpy(szBuf, "ARQC:");
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
            vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
            _uiPrint(szBuf, 0);
        }
        
        tag=0x4F;
        if(GetTagValueSize(&tmpTagList, tag))
        {
            strcpy(szBuf, "AID:");
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
            vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
            _uiPrint(szBuf, 0);
        }
			
        if(Rec->ucUploadFlag!=0xFF)
        {
            //脱机交易
            szBuf[0]=0;
    #if 0
            tag=0x5F34;
            if(GetTagValueSize(&tmpTagList, tag))
            {
                strcpy(szBuf, "CSN:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
                strcat(szBuf, "    ");
                //_uiPrint(szBuf, 0);
            }
    #else
            if(Rec->ucPanSerNo!=0xFF)
            {
                sprintf(szBuf, "CSN:%03d    ", Rec->ucPanSerNo);
            }
    #endif        
            tag=0x95;
            if(GetTagValueSize(&tmpTagList, tag))
            {
                strcpy(szBuf+strlen(szBuf), "TVR:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
                //_uiPrint(szBuf, 0);
            }
            if(szBuf[0])
                _uiPrint(szBuf, 0);
            
            szBuf[0]=0;
            tag=0x9F36;
            if(GetTagValueSize(&tmpTagList, tag))
            {
                strcpy(szBuf, "ATC:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
               //_uiPrint(szBuf, 0);
                strcat(szBuf, "   ");
            }
            tag=0x9B;
            if(GetTagValueSize(&tmpTagList, tag))
            {            
                strcpy(szBuf+strlen(szBuf), "TSI:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
                //_uiPrint(szTmp, 0);
            }
            if(szBuf[0])
                _uiPrint(szBuf, 0);
            
            
            tag=0x50;   //应用标签AppLable
            if(GetTagValueSize(&tmpTagList, tag))
            {
                strcpy(szBuf, "AppLab:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                //vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, );
                vMemcpy0(szBuf+strlen(szBuf), GetTagValue(&tmpTagList, tag), tagBinLen);
                _uiPrint(szBuf, 0);
            }
            //tag=0x9F12; //应用首选名称AppName
            
            tag=0x9F37;
            if(GetTagValueSize(&tmpTagList, tag))
            {
                strcpy(szBuf, "UNPR_NUM:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
                _uiPrint(szBuf, 0);
            }
            
            szBuf[0]=0;
            tag=0x82;
            if(GetTagValueSize(&tmpTagList, tag))
            {
                strcpy(szBuf, "AIP:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
                //_uiPrint(szBuf, 0);
                strcat(szBuf, "   ");
            }
            tag=0x9F34;
            if(GetTagValueSize(&tmpTagList, tag))
            {
                strcpy(szBuf+strlen(szBuf), "CVMR:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
                //_uiPrint(szBuf, 0);
            }
            if(szBuf[0])
                _uiPrint(szBuf, 0);

            tag=0x9F10;
            if(GetTagValueSize(&tmpTagList, tag))
            {
                strcpy(szBuf, "IAD:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
                _uiPrint(szBuf, 0);
                if(strlen(szBuf)>_uiGetPCols())
                    _uiPrint(szBuf+_uiGetPCols(), 0);
            }
            tag=0x9F33;
            if(GetTagValueSize(&tmpTagList, tag))
            {
                strcpy(szBuf, "TermCap:");
                tagBinLen=GetTagValueSize(&tmpTagList, tag);	
                vOneTwo0(GetTagValue(&tmpTagList, tag), tagBinLen, szBuf+strlen(szBuf));
                _uiPrint(szBuf, 0);
            }	    		
        }
        FreeTagList(&tmpTagList);

        _uiPrint(NULL, 0);
    }
    memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    _uiPrintEx(1, szBuf, 0);                
     if(sheet==1 || sheet==2)    //商户联才打印签字
    {  
        if(Rec->ucTransAttr & 0x02)     //需要签字
        {
            
            if((Rec->ucTransAttr&0x04)==0)   //电签
            {
                char file[30];
                vOneTwo0(Rec->sDateTime+1, 2, file);
                sprintf((char*)file+4, "_%06lu.jbg", Rec->ulTTC);
                 if(gl_SysInfo.ucPrintEN== 1) 
			iPrtRet=_uiPrintEx(1, "持卡人签名(CARDHOLDER SIGNATURE):", 1);	 	
	         else
              	  	iPrtRet=_uiPrintEx(1, "持卡人签名:", 1);         //打印图片之前先打印
                if(iPrtRet==0)
                {
                    iPrtRet=iPrintSignFile(file);
                    if(iPrtRet>0)
                    {
                        _uiPrint("", 0);
                        _uiPrint("", 0);
                        _uiPrint("", 0);
                        iPrtRet=0;
                    }
                }
            }else
            {
                if(gl_SysInfo.ucPrintEN== 1) 
                	 _uiPrintEx(1, "持卡人签名(CARDHOLDER SIGNATURE):", 1);	 
		else		 
                	_uiPrintEx(1, "持卡人签名:", 0);
                _uiPrint("", 0);
                _uiPrint("", 0);
                _uiPrint("", 0);
            }

	    if(gl_TransRec.uiEntryMode/10 == 7 && (Rec->uiTransType == TRANS_TYPE_SALE ||Rec->uiTransType == TRANS_TYPE_DAIRY_SALE || Rec->uiTransType == TRANS_TYPE_PREAUTH))		
	    {
	    	if(!gl_SysInfo.ucNeedPin)
	            {
	                sprintf(szBuf, "交易金额未超%lu.%02lu元，无需输密", gl_SysInfo.ulNoPinLimit/100, gl_SysInfo.ulNoPinLimit%100);
	                _uiPrintEx(1, szBuf, 0);
	            }
	    }
		
            memset(szBuf, '-', _uiGetPCols());
            szBuf[_uiGetPCols()]=0;
            _uiPrintEx(1, szBuf, 0);

            if(strlen(rtrim(gl_TransRec.gl_uc5f20)) >0) 
	   	_uiPrintEx(1,gl_TransRec.gl_uc5f20,0);
	   
	      if(gl_SysInfo.ucPrintEN== 1) 
	      	{
		    _uiPrintEx(11, "本人确认以上交易，同意将其记入本卡账户", 0);
    		   _uiPrintEx(11, "I ACKNOWLEDGE SATISFACTORY RECEIPT OF RELATIVE GOODS/SERVICES", 0);
	      	}
	     else	  	
	     	{
		_uiPrintEx(11, "本人确认以上交易，同意将其记入本卡账户", 0);
        	}
        }else
       {
            _uiPrint("", 0);
	   if(gl_TransRec.uiEntryMode/10 == 7 && (Rec->uiTransType == TRANS_TYPE_SALE ||Rec->uiTransType == TRANS_TYPE_DAIRY_SALE || Rec->uiTransType == TRANS_TYPE_PREAUTH))	
	   {
	   	
	            if(gl_SysInfo.ucNoSignFlag  &&  Rec->ulAmount <= gl_SysInfo.ulNoSignLimit && gl_SysInfo.ucNeedPin)
	            {
	                sprintf(szBuf, "交易金额未超%lu.%02lu元，无需签名", gl_SysInfo.ulNoSignLimit/100, gl_SysInfo.ulNoSignLimit%100);
	                _uiPrintEx(1, szBuf, 0);
	            }

		    if( gl_SysInfo.ucNoSignFlag  &&  Rec->ulAmount <= gl_SysInfo.ulNoSignLimit && !gl_SysInfo.ucNeedPin)
	            {
	                sprintf(szBuf, "交易金额未超%lu.%02lu元，免密免签", gl_SysInfo.ulNoPinLimit/100, gl_SysInfo.ulNoPinLimit%100);
	                _uiPrintEx(1, szBuf, 0);
	            }

			memset(szBuf, '-', _uiGetPCols());
			szBuf[_uiGetPCols()]=0;
			_uiPrintEx(1, szBuf, 0);	
          }		

	if(strlen(rtrim(gl_TransRec.gl_uc5f20)) >0) 
       		  _uiPrintEx(1,gl_TransRec.gl_uc5f20,0);
	
  	 if(gl_SysInfo.ucPrintEN== 1) 
      	{
		    _uiPrintEx(11, "本人确认以上交易，同意将其记入本卡账户", 0);
    		   _uiPrintEx(11, "I ACKNOWLEDGE SATISFACTORY RECEIPT OF RELATIVE GOODS/SERVICES", 0);
      	}
    	 else	  	
     	{
		_uiPrintEx(11, "本人确认以上交易，同意将其记入本卡账户", 0);
    	} 		   
            memset(szBuf, '-', _uiGetPCols());
            szBuf[_uiGetPCols()]=0;
            _uiPrintEx(1, szBuf, 0);
        }

	   _uiPrint("", 0);
	     //服务热线
           sprintf(szBuf,"服务热线:%s",gl_SysInfo.ucServiceHotline);
           _uiPrintEx(1,szBuf,0);
  
           //二维码
           if(gl_SysInfo.ucPrintMerchantQrFlag)
           {
	  	 _uiPrint(NULL, 1);
	   //	iPrintQrCodeStr(gl_SysInfo.ucPrintMerchantQrFlag);
           }		
 
           //广告词
           if(gl_SysInfo.ucPrintMerchantMsgFlag)
          {
          	_uiPrintEx(11,gl_SysInfo.ucPrintMerchantMsg,0);
           }	
    }else 
    {
   	    if(strlen(rtrim(gl_TransRec.gl_uc5f20)) >0) 
            		_uiPrintEx(1,gl_TransRec.gl_uc5f20,0);
		
	    if(gl_SysInfo.ucPrintEN== 1) 
      	   {
	    	_uiPrintEx(11, "本人确认以上交易，同意将其记入本卡账户", 0);
		   _uiPrintEx(11, "I ACKNOWLEDGE SATISFACTORY RECEIPT OF RELATIVE GOODS/SERVICES", 0);
      		}
    	 else	  	
     		{
		_uiPrintEx(11, "本人确认以上交易，同意将其记入本卡账户", 0);
    		}
		 
           _uiPrint("", 0);
	    //服务热线
           sprintf(szBuf,"服务热线:%s",gl_SysInfo.ucServiceHotline);
           _uiPrintEx(1,szBuf,0);

           //二维码
           if(gl_SysInfo.ucPrintCardholderQrFlag)
           {
	  	 _uiPrint(NULL, 1);
//	   	iPrintQrCodeStr(gl_SysInfo.ucPrintCardholderQrFlag);
           }		
   
           //广告词
           if(gl_SysInfo.ucPrintCardholderMsgFlag)
          {
          	_uiPrintEx(11,gl_SysInfo.ucPrintCardholderMsg,0);
           }	 
    }	 
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 1);
    
    _uiPrintClose();

    return 0;
}


//sheet-第几联  0-无 1-商户联  2-收单机构3-持卡人
//dup - 重打标志 0-正常 1-重打
//QrRec - 交易记录
//pszReference - 备注信息
int iPrintQrTransSheet(int sheet, int dup, stQrTransRec *QrRec)
{ 
    char szBuf[100], szTmp[100];
    int len;
	
    if(_uiPrintOpen())
        return 1;
    
    _uiPrintSetFont(1);
    if(strlen(gl_SysInfo.ucPrintTitle ) > 0)
	_uiPrintEx(2,gl_SysInfo.ucPrintTitle, 0);
    else
    	_uiPrintEx(2, "POS签购单", 0);

   _uiPrintSetFont(2);
    memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    if(sheet>=1 && sheet<=3)
    {
        if(sheet==1)
       {
       		strcpy(szTmp, "商户存根");		
        }		
	   	
        else if(sheet==2)
        {
            strcpy(szTmp, "银行存根");
    	}
        else		
        {
            strcpy(szTmp, "持卡人存根");
    	}
        len=strlen(szTmp);
        memcpy(szBuf+(strlen(szBuf)-len)/2, szTmp, len);
    }
    _uiPrintEx(1, szBuf, 0);    

    if(QrRec->szPrtMerchName[0])
	sprintf(szBuf, "商户名称:%s", QrRec->szPrtMerchName);
    else
	sprintf(szBuf, "商户名称:%s", gl_SysInfo.szMerchName);	
	 _uiPrintEx(11, szBuf,0);    

      if(QrRec->sPrtMerchId[0])
     {

	 	sprintf(szBuf, "商户编号:%s",QrRec->sPrtMerchId);
    }	 
    else
    {
		 sprintf(szBuf, "商户编号:%s",gl_SysInfo.szMerchId);
    }	   
    _uiPrintEx(1, szBuf, 0);

       vOneTwo0(QrRec->sOperNo, 1, szTmp);
    	if(QrRec->sPrtPosId[0])
		sprintf(szBuf, "终端编号:%s    操作员号:%2.2s", QrRec->sPrtPosId,szTmp);	
        else
    		sprintf(szBuf, "终端编号:%s    操作员号:%2.2s", gl_SysInfo.szPosId,szTmp);
	
    _uiPrintEx(1, szBuf, 0);
	
    szBuf[0]= 0;
    strcpy(szBuf, "交易类型:");	
     switch (QrRec->uiTransType)
    {
   	 case QRTRANS_TYPE_SALE:
	 	if(QrRec->ucPayMode == Z_phoneTopos)
        		strcat(szBuf, "用户扫码");
		else if(QrRec->ucPayMode == B_posTophone)
        		strcat(szBuf, "商户扫码");
        	break;
   	 case QRTRANS_TYPE_VOID:
       	 	strcat(szBuf, "扫码撤销");
        	break; 
	 case QRTRANS_TYPE_REFUND:
       	 	strcat(szBuf, "扫码退款");
        	break; 
	 case SCAN_TRANS_TYPE_PREAUTH:
	 	if(QrRec->ucPayMode == Z_phoneTopos)
        	        strcat(szBuf, "用户扫码预授权");
		else if(QrRec->ucPayMode == B_posTophone)
        	         strcat(szBuf, "商户扫码预授权");
        	break;
	 case SCAN_TRANS_TYPE_PREAUTHVOID:	
	 	strcat(szBuf, "扫码预授权撤销");
		break;
	 case SCAN_TRANS_TYPE_PREAUTH_COMP:	
	 	strcat(szBuf, "扫码预授权完成");
		break;	
    	default:
        	strcat(szBuf, "(未知交易)");
        	break;
    }
    _uiPrintEx(11, szBuf, 0);    //交易类型用大号字体

   memset( szBuf,0x00,sizeof(szBuf));
    strcpy(szBuf, "支付类型:");
    if(QrRec->ucPayWay==MODE_WETCHAT)
    {
	 strcat(szBuf, "微信支付");
    }else if(QrRec->ucPayWay==MODE_ALIPAY)
    {	
        strcat(szBuf, "支付宝支付");
    }else if(QrRec->ucPayWay==MODE_UNIONPAY)
    {
        strcat(szBuf, "银联支付");
    }
   _uiPrintEx(1, szBuf, 0);  

     if(QrRec->uiTransType == QRTRANS_TYPE_SALE)
      {
      		sprintf(szTmp, "扫码订单号:%s", QrRec->orderId);
		_uiPrintEx(11, szTmp, 0);	
     }		
     else if(QrRec->uiTransType == QRTRANS_TYPE_VOID)	
	{
		sprintf(szTmp, "扫码订单号:%s", QrRec->szVoidOrderId);
		_uiPrintEx(11, szTmp, 0);
     	}
     else if(QrRec->uiTransType == QRTRANS_TYPE_REFUND && QrRec->szVoidOrderId[0])	
	{
		sprintf(szTmp, "扫码订单号:%s", QrRec->szVoidOrderId);	 
	       _uiPrintEx(11, szTmp, 0);
     	}	   
	else if(QrRec->uiTransType == SCAN_TRANS_TYPE_PREAUTH)	
	{
		sprintf(szTmp, "扫码订单号:%s", QrRec->orderId);	 
	      _uiPrintEx(11, szTmp, 0);
	}	  
	else if(QrRec->uiTransType == SCAN_TRANS_TYPE_PREAUTHVOID && QrRec->szVoidOrderId[0])	
	{
		sprintf(szTmp, "扫码订单号:%s", QrRec->szVoidOrderId);	 
		_uiPrintEx(11, szTmp, 0);
	}
	else if(QrRec->uiTransType == SCAN_TRANS_TYPE_PREAUTH_COMP && QrRec->szVoidOrderId[0])	
	{
		sprintf(szTmp, "扫码订单号:%s", QrRec->szVoidOrderId);	 
		_uiPrintEx(11, szTmp, 0);
	}	
   	
   	sprintf(szBuf, "批次号:%06lu      凭证号:%06lu", gl_SysData.ulBatchNo,QrRec->ulTTC);
	  _uiPrintEx(1, szBuf, 0);
	
   sprintf(szBuf, "参考号:%.12s", QrRec->sReferenceNo);
    _uiPrintEx(1, szBuf, 0);    

    vOneTwo0(QrRec->sDateTime, 6, szTmp);	
    sprintf(szBuf, "日期/时间:20%.2s/%.2s/%.2s %.2s:%.2s:%.2s", szTmp, szTmp+2, szTmp+4, szTmp+6, szTmp+8, szTmp+10);
    _uiPrintEx(1, szBuf, 0);

    sprintf(szBuf, "金额:RMB %lu.%02lu", QrRec->ulAmount/100, QrRec->ulAmount%100);
    _uiPrintSetFont(1);
    _uiPrintEx(1,szBuf, 0);    //金额用大号字体			
    _uiPrintSetFont(2);   
	
    memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    _uiPrintEx(1, szBuf, 0);

      _uiPrintEx(1, "备注:", 0);

    if(QrRec->uiTransType == QRTRANS_TYPE_VOID ||QrRec->uiTransType == QRTRANS_TYPE_REFUND
		||QrRec->uiTransType == SCAN_TRANS_TYPE_PREAUTHVOID ||QrRec->uiTransType == SCAN_TRANS_TYPE_PREAUTH_COMP)	
   {	
	sprintf(szBuf, "原凭证号:%06lu", QrRec->ulVoidTTC);		
	_uiPrintEx(1, szBuf, 0);
   }
	
    if(dup)
    {
        _uiPrintSetFont(1);	
        _uiPrintEx(2, "重打印", 0);
    }

    _uiPrintSetFont(3); 
     memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    strcpy(szTmp, "本人确认以上交易，同意将其记入本卡账户");
    len=strlen(szTmp);
    memcpy(szBuf+(strlen(szBuf)-len)/2, szTmp, len);
     _uiPrintEx(1, szBuf, 0);			

	_uiPrintSetFont(2);	
	if(sheet==1 || sheet==2)
	{
		    if(gl_SysInfo.ucPrintMerchantQrFlag)
	           {
		   	 _uiPrint(NULL, 1);
//		   	iPrintQrCodeStr(gl_SysInfo.ucPrintMerchantQrFlag);
	           }		
	 
	           //广告词
	           if(gl_SysInfo.ucPrintMerchantMsgFlag)
	          {
	          	_uiPrintEx(11,gl_SysInfo.ucPrintMerchantMsg,0);
	           }	

	}else
	{				//二维码
	       if(gl_SysInfo.ucPrintCardholderQrFlag)
	       {
		   	 _uiPrint(NULL, 1);
//		   	iPrintQrCodeStr(gl_SysInfo.ucPrintCardholderQrFlag);
	       }		
   
	       //广告词
	       if(gl_SysInfo.ucPrintCardholderMsgFlag)
	      {
	      	_uiPrintEx(11,gl_SysInfo.ucPrintCardholderMsg,0);
	       }
	}
		
	 //服务热线
       sprintf(szBuf,"服务热线:%s",gl_SysInfo.ucServiceHotline);
       _uiPrintEx(1,szBuf,0);

	_uiPrintSetFont(3);	   
	memset(szTmp,0x00,sizeof(szTmp));
        _pszGetAppVer(szTmp);		
	sprintf(szBuf, "%s      %s", _pszGetModelName(NULL),szTmp); 
	_uiPrintEx(1, szBuf, 0);	
	_uiPrintSetFont(2);	
	
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 1);    
    _uiPrintClose();
    return 0;
}

//dup - 重打标志
//pszReference - 备注信息
int iPrintTrans(stTransRec *rec, int dup, uchar *pszReference)
{    
    if(_uiGetPCols()==0)
        return 0;
	
    if(gl_SysInfo.ucPrinterAttr==0)
    {
        if(dup)
            vMessage("当前设置为不打印单据");
        return 0;
    }

    gl_SysData.ucPrintErr = 1;
    uiMemManaPutSysData();	
	
    if(gl_SysInfo.ucPrinterAttr==2 || gl_SysInfo.ucPrinterAttr==3)
    {
        _vDisp(3, "打印第一联");
        iPrintTransSheet(1, dup, rec, pszReference);
        _vBuzzer();
	if(gl_SysInfo.ucPrinterAttr == 2)	
       {
       		_vDisp(3, "准备打印第二联...");
        	vDispCenter(_uiGetVLines(), "[确认]打印,[取消]退出", 0);
        	_vFlushKey();
	        if(iOK(5)==0)
	        {
			gl_SysData.ucPrintErr = 0;
			uiMemManaPutSysData();	
	        	return 0;
	        }		
	        _vDisp(_uiGetVLines(), "");
	        _vDisp(3, "打印第二联");
	        iPrintTransSheet(3, dup, rec, pszReference);
	}	
        if(gl_SysInfo.ucPrinterAttr==3)
        {
        	_vDisp(3, "准备打印第二联...");
        	vDispCenter(_uiGetVLines(), "[确认]打印,[取消]退出", 0);
        	_vFlushKey();
	        if(iOK(5)==0)
	        {
			gl_SysData.ucPrintErr = 0;
			uiMemManaPutSysData();	
	        	return 0;
	        }		
	        _vDisp(_uiGetVLines(), "");
	        _vDisp(3, "打印第二联");
	        iPrintTransSheet(2, dup, rec, pszReference);
			
           	 _vDisp(3, "准备打印第三联...");
	         vDispCenter(_uiGetVLines(), "[确认]打印,[取消]退出", 0);
        	_vFlushKey();
	        if(iOK(5)==0)
	       {
			gl_SysData.ucPrintErr = 0;
			uiMemManaPutSysData();	
	       		return 0;
	        }		
		_vDisp(_uiGetVLines(), "");	
           	 _vDisp(3, "打印第三联");
           	 iPrintTransSheet(3, dup, rec, pszReference);         
        }
        _vDisp(3, "");
    }else if(gl_SysInfo.ucPrinterAttr)
    {
        iPrintTransSheet(1, dup, rec, pszReference);
        _vBuzzer();
    }

    gl_SysData.ucPrintErr = 0;
    uiMemManaPutSysData();	
	
    return 0;
}

int iPrintQrTrans(stQrTransRec *QrRec, int dup)
{ 
    if(_uiGetPCols()==0)
        return 0;
	
    if(gl_SysInfo.ucPrinterAttr==0)
    {
        if(dup)
            vMessage("当前设置为不打印单据");
        return 0;
    }

    gl_SysData.ucPrintErr = 2;
    uiMemManaPutSysData();	
	
    if(gl_SysInfo.ucPrinterAttr==2 || gl_SysInfo.ucPrinterAttr==3)
    {
        _vDisp(3, "打印第一联");
        iPrintQrTransSheet(1, dup, QrRec);
        _vBuzzer();
	if(gl_SysInfo.ucPrinterAttr == 2)		
	{
		_vDisp(3, "准备打印第二联...");
	        vDispCenter(_uiGetVLines(), "[确认]打印,[取消]退出", 0);
	        _vFlushKey();
	        if(iOK(5)==0)
	        {
	        	gl_SysData.ucPrintErr = 0;
			uiMemManaPutSysData();	
	        	return 0;
	        }		
	        _vDisp(_uiGetVLines(), "");
	        _vDisp(3, "打印第二联");
	        iPrintQrTransSheet(3, dup, QrRec);
    	}		
        if(gl_SysInfo.ucPrinterAttr==3)
        {
        	_vDisp(3, "准备打印第二联...");
	        vDispCenter(_uiGetVLines(), "[确认]打印,[取消]退出", 0);
	        _vFlushKey();
	        if(iOK(5)==0)
	        {
			gl_SysData.ucPrintErr = 0;
			uiMemManaPutSysData();	
	        	return 0;
	        }		
	        _vDisp(_uiGetVLines(), "");
	        _vDisp(3, "打印第二联");
	        iPrintQrTransSheet(2, dup, QrRec);
			
            	_vDisp(3, "准备打印第三联...");
		vDispCenter(_uiGetVLines(), "[确认]打印,[取消]退出", 0);
        	_vFlushKey();
	        if(iOK(5)==0)
	       {
			gl_SysData.ucPrintErr = 0;
			uiMemManaPutSysData();	
	       		return 0;
	        }		
		_vDisp(_uiGetVLines(), "");	
           	 _vDisp(3, "打印第三联");
           	 iPrintQrTransSheet(3, dup, QrRec);            
        }
        _vDisp(3, "");
    }else if(gl_SysInfo.ucPrinterAttr)
    {
        iPrintQrTransSheet(1, dup, QrRec);
        _vBuzzer();
    }

    gl_SysData.ucPrintErr = 0;
    uiMemManaPutSysData();	
		
    return 0;
}

int iPrintTransByTTC(void)
{
    int iRet, i,j;
    char buf[50];
    ulong ulTTC;
    stTransRec rec;
    stQrTransRec QRrec;	
   uchar ucPinPos;
   
    _vCls();
    vDispCenter(1, "按凭证号打印单据", 1);

     if(gl_SysData.uiTransNum==0 && gl_SysData.uiQrTransNum==0)
    {
        vMessage("没有交易");
        return 0;
    }
		
    _vDisp(2, "请输入凭证号:");
    ucPinPos = _uiGetVCols();
    iRet=iInput(INPUT_NORMAL|INPUT_LEFT, 3, ucPinPos, buf, 6, 1, 6, 30);
    if(iRet<0)
        return -1;
    vClearLines(2);
    ulTTC=atol(buf);
    if(ulTTC==0)
    {
        vMessage("无效凭证号");
        return -1;
    }

     if(gl_SysData.uiTransNum > 0)
     {
	    for(i = 0; i < gl_SysData.uiTransNum; i ++)
	    {
	        uiMemManaGetTransRec(i, &rec);	        
	        if(rec.ulTTC==ulTTC)
	        {
	            iPrintTrans(&rec, 1, NULL);
	           return 1;
	        }
	    }
     }  

    if(gl_SysData.uiQrTransNum > 0)
     {
	    for(j = 0; j < gl_SysData.uiQrTransNum; j++)
	    {
	        uiMemManaGetQrTransRec(j, &QRrec);	        
	        if(QRrec.ulTTC==ulTTC &&  ((QRrec.uiTransType == QRTRANS_TYPE_SALE && QRrec.ucProType == 0x01) || QRrec.uiTransType == QRTRANS_TYPE_VOID))
	        {
	            iPrintQrTrans(&QRrec, 1);
	           return 1;
	        }
	    }	   
     } 

     if(i >= gl_SysData.uiTransNum  &&  j >= gl_SysData.uiQrTransNum)
    {
        vMessage("无此交易");
        return 1;
    } 
	 
    return 0;
}

int iPrintAll(void)
{ 
    char szBuf[100], szTmp[100];
    //int len;
    stTransRec rec;
    stQrTransRec QRrec;
    int i,n;
    char szType[1+1];

    _vCls();
    vDispCenter(1, "打印交易明细单", 1);

    if(_uiPrintOpen())
        return 1;
    
    _uiPrintSetFont(1);
    _uiPrintEx(2, "交易明细单", 0);
    _uiPrintSetFont(2);

    sprintf(szBuf, "商户名:%s", gl_SysInfo.szMerchName);
    _uiPrintEx(11, szBuf, 0);
    sprintf(szBuf, "商户号:%s", gl_SysInfo.szMerchId);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "终端号:%s    操作员:%.2s", gl_SysInfo.szPosId, gl_SysData.szCurOper);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "批次号:%06lu", gl_SysData.ulBatchNo);
    _uiPrintEx(1, szBuf, 0);
    _vGetTime(szTmp);
    sprintf(szBuf, "时  间:%.4s/%.2s/%.2s %.2s:%.2s:%.2s", szTmp, szTmp+4, szTmp+6, szTmp+8, szTmp+10, szTmp+12);
    _uiPrintEx(1, szBuf, 0);

    memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    _uiPrintEx(1, szBuf, 0);
	
    _uiPrintSetFont(3);
    //只打印消费，预授权完成和退货。预授权，撤销交易和已被撤销的交易不打印
    strcpy(szBuf, "凭证号  类型       卡号/订单号            金额    授权码");
    _uiPrintEx(1, szBuf, 0);

     _uiPrintSetFont(3);	
    szType[1]=0;
    for(i=0; i<gl_SysData.uiTransNum; i++)
    {
        uiMemManaGetTransRec(i, &rec);
        if(rec.uiTransType!=TRANS_TYPE_SALE && rec.uiTransType!=TRANS_TYPE_DAIRY_SALE && rec.uiTransType!=TRANS_TYPE_PREAUTH_COMP && rec.uiTransType!=TRANS_TYPE_REFUND)
            continue;
        if(rec.ucVoidFlag)
            continue;
        if(rec.uiTransType==TRANS_TYPE_SALE || rec.uiTransType==TRANS_TYPE_DAIRY_SALE)
            szType[0]='S';
        else if(rec.uiTransType==TRANS_TYPE_PREAUTH_COMP)
            szType[0]='C';
        else
            szType[0]='R';
        vUnpackPan(rec.sPan, szTmp);
	#ifdef JTAG_DEBUG        
       //		if(i==0 && rec.sAuthCode[0]==0)
       //     		memcpy(rec.sAuthCode, "876543", 6); 
	#endif       
        #if 0
        sprintf(szBuf, "%06lu %.1s %.3s*%.3s %5lu.%02lu %.6s", rec.ulTTC, szType, szTmp, szTmp+strlen(szTmp)-3, 
                    rec.ulAmount/100, rec.ulAmount%100, rec.sAuthCode);   
        #else
        if(strlen(szTmp)>10)
            memset(szTmp+6, '*', strlen(szTmp)-6-4);
        sprintf(szBuf, "%06lu   %.1s    %19.19s   %7lu.%02lu   %.6s", rec.ulTTC, szType, szTmp, 
                    rec.ulAmount/100, rec.ulAmount%100, rec.sAuthCode); 
        #endif
//        _uiPrintEx(1, szBuf, 0);
        ++n;
	 _uiPrintEx(1, szBuf, (i>0&&n%50==0)?1:0);
    }
	
    szType[1]=0;
    for(i=0; i<gl_SysData.uiQrTransNum; i++)
    {
        uiMemManaGetQrTransRec(i, &QRrec);
	if(QRrec.uiTransType != QRTRANS_TYPE_SALE && QRrec.uiTransType != SCAN_TRANS_TYPE_PREAUTH
		&& QRrec.uiTransType != SCAN_TRANS_TYPE_PREAUTHVOID && QRrec.uiTransType != SCAN_TRANS_TYPE_PREAUTH_COMP)
		continue;
        if(QRrec.ucVoidFlag ||QRrec.ucProType == 0x02)
            continue;
        if(QRrec.uiTransType == QRTRANS_TYPE_SALE ||QRrec.uiTransType == SCAN_TRANS_TYPE_PREAUTH 
			||QRrec.uiTransType == SCAN_TRANS_TYPE_PREAUTHVOID||QRrec.uiTransType == SCAN_TRANS_TYPE_PREAUTH_COMP)
        {
        	if(QRrec.ucPayWay == MODE_WETCHAT)
			szType[0]='W';
		if(QRrec.ucPayWay == MODE_ALIPAY)
			szType[0]='Z';	
		if(QRrec.ucPayWay == MODE_UNIONPAY)
			szType[0]='U';
        } 

        sprintf(szTmp,"%.5s*****%.5s",QRrec.orderId,&QRrec.orderId[strlen(QRrec.orderId)-5]);
		
        sprintf(szBuf, "%06lu   %.1s       %15.15s    %7lu.%02lu", QRrec.ulTTC, szType, szTmp, QRrec.ulAmount/100, QRrec.ulAmount%100);
//        _uiPrintEx(1, szBuf, 0);
        ++n;
	_uiPrintEx(1, szBuf, (i>0&&n%50==0)?1:0);
    }

   _uiPrintSetFont(2);	
    memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    _uiPrintEx(1, szBuf, 0);
    _uiPrintEx(1, "类型: S-消费 R-退货 C-预授权完成", 0);	
    _uiPrintEx(1, "          W-微信 Z-支付宝 U-银联", 0);

	memset(szBuf, '-', _uiGetPCols());
	szBuf[_uiGetPCols()]=0;
	_uiPrintEx(1, szBuf, 0);
	
	 //服务热线
	   sprintf(szBuf,"服务热线:%s",gl_SysInfo.ucServiceHotline);
	   _uiPrintEx(1,szBuf,0);
    
     _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 1);   
    _uiPrintClose();   

    return 0;
}

int iPrintSum(void)
{
    int iSaleNum=0, iSaleVoidNum=0, iAuthCmpNum=0, iAuthCmpVoidNum=0, iRefundNum=0;
    ulong ulSaleAmt=0L, ulSaleVoidAmt=0L, ulAuthCmpAmt=0L, ulAuthCmpVoidAmt=0L, ulRefundAmt=0L;
    
 int iWetchatScanSaleNum=0, iWetchatQrSaleNum=0,iWetchatSaleVoidNum=0,iWetchatSaleRefundNum=0,iWetchatScanPreauthNum=0, iWetchatQrPreauthNum=0,iWetchatPreauthCompNum=0,iWetchatPreauthVoidNum=0;
      int iAlipayScanSaleNum=0, iAlipayQrSaleNum=0,iAlipaySaleVoidNum=0,iAlipaySaleRefundNum=0,iAlipayScanPreauthNum=0, iAlipayQrPreauthNum=0,iAlipayPreauthCompNum=0,iAlipayPreauthVoidNum=0;	  
      int iUnionpayScanSaleNum=0, iUnionpayQrSaleNum=0,iUnionpaySaleVoidNum=0,iUnionpaySaleRefundNum=0,iUnionpayScanPreauthNum=0, iUnionpayQrPreauthNum=0,iUnionpayPreauthCompNum=0,iUnionpayPreauthVoidNum=0;	  

     ulong ulWetchatScanSaleAmt=0L, ulWetchatQrSaleAmt=0L, ulWetchatSaleVoidAmt=0L, ulWetchatSaleRefundAmt=0L, ulWetchatScanPreauthAmt=0L, ulWetchatQrPreauthAmt=0L, ulWetchatPreauthCompAmt=0L, ulWetchatPreauthVoidAmt=0L;
     ulong ulAlipayScanSaleAmt=0L, ulAlipayQrSaleAmt=0L, ulAlipaySaleVoidAmt=0L, ulAlipaySaleRefundAmt=0L, ulAlipayScanPreauthAmt=0L, ulAlipayQrPreauthAmt=0L, ulAlipayPreauthCompAmt=0L, ulAlipayPreauthVoidAmt=0L;
     ulong ulUnionpayScanSaleAmt=0L, ulUnionpayQrSaleAmt=0L, ulUnionpaySaleVoidAmt=0L, ulUnionpaySaleRefundAmt=0L, ulUnionpayScanPreauthAmt=0L, ulUnionpayQrPreauthAmt=0L, ulUnionpayPreauthCompAmt=0L, ulUnionpayPreauthVoidAmt=0L;

	
    char szBuf[100], szTmp[100];

    _vCls();
    vDispCenter(1, "打印交易汇总", 1);

    vGenTransSum(&iSaleNum, &ulSaleAmt, &iSaleVoidNum, &ulSaleVoidAmt, 
                &iAuthCmpNum, &ulAuthCmpAmt, &iAuthCmpVoidNum, &ulAuthCmpVoidAmt,
                &iRefundNum, &ulRefundAmt,0);
	
   vGenQrTransSum(&iWetchatScanSaleNum, &ulWetchatScanSaleAmt, &iWetchatQrSaleNum, &ulWetchatQrSaleAmt, 
   	        &iWetchatSaleVoidNum,&ulWetchatSaleVoidAmt,&iWetchatSaleRefundNum,&ulWetchatSaleRefundAmt,MODE_WETCHAT,0);
   
    vGenQrPreauthTransSum(&iWetchatScanPreauthNum, &ulWetchatScanPreauthAmt, &iWetchatQrPreauthNum, &ulWetchatQrPreauthAmt, 
   	        &iWetchatPreauthCompNum,&ulWetchatPreauthCompAmt,&iWetchatPreauthVoidNum,&ulWetchatPreauthVoidAmt,MODE_WETCHAT,0);
	
  vGenQrTransSum(&iAlipayScanSaleNum, &ulAlipayScanSaleAmt, &iAlipayQrSaleNum, &ulAlipayQrSaleAmt, 
   	        &iAlipaySaleVoidNum,&ulAlipaySaleVoidAmt,&iAlipaySaleRefundNum,&ulAlipaySaleRefundAmt,MODE_ALIPAY,0);
   
    vGenQrPreauthTransSum(&iAlipayScanPreauthNum, &ulAlipayScanPreauthAmt, &iAlipayQrPreauthNum, &ulAlipayQrPreauthAmt, 
   	        &iAlipayPreauthCompNum,&ulAlipayPreauthCompAmt,&iAlipayPreauthVoidNum,&ulAlipayPreauthVoidAmt,MODE_ALIPAY,0);

   vGenQrTransSum(&iUnionpayScanSaleNum, &ulUnionpayScanSaleAmt, &iUnionpayQrSaleNum, &ulUnionpayQrSaleAmt, 
   	        &iUnionpaySaleVoidNum,&ulUnionpaySaleVoidAmt,&iUnionpaySaleRefundNum,&ulUnionpaySaleRefundAmt,MODE_UNIONPAY,0);
   
    vGenQrPreauthTransSum(&iUnionpayScanPreauthNum, &ulUnionpayScanPreauthAmt, &iUnionpayQrPreauthNum, &ulUnionpayQrPreauthAmt, 
   	        &iUnionpayPreauthCompNum,&ulUnionpayPreauthCompAmt,&iUnionpayPreauthVoidNum,&ulUnionpayPreauthVoidAmt,MODE_UNIONPAY,0);
    /*
    _vDisp(2, "银行卡:");
    vDispVarArg(3, " 借记数:%3d 金额:%5lu.%02lu", iDebitNum, ulDebitAmt/100, ulDebitAmt%100);
    vDispVarArg(4, " 贷记数:%3d 金额:%5lu.%02lu", iCreditNum, ulCreditAmt/100, ulCreditAmt%100);
    _vDisp(5, "扫一扫:");
    vDispVarArg(6, " 借记数:%3d 金额:%5lu.%02lu", iQrDebitNum, ulQrDebitAmt/100, ulQrDebitAmt%100);
    vDispVarArg(7, " 贷记数:%3d 金额:%5lu.%02lu", iQrCreditNum, ulQrCreditAmt/100, ulQrCreditAmt%100);
    vDispCenter(_uiGetVLines(), "[取消]退出,[确认]打印", 0);
    if(iOK(15)==0)
        return 0;
    */
     if(_uiPrintOpen())
         return 1;
    _uiPrintSetFont(1);
    _uiPrintEx(2, "交易汇总单", 0);
    _uiPrintSetFont(2);

    sprintf(szBuf, "商户名:%s", gl_SysInfo.szMerchName);
    _uiPrintEx(11, szBuf, 0);
    sprintf(szBuf, "商户号:%s", gl_SysInfo.szMerchId);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "终端号:%s", gl_SysInfo.szPosId);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "批次号:%06lu  操作员:%.2s", gl_SysData.ulBatchNo, gl_SysData.szCurOper);
    _uiPrintEx(1, szBuf, 0);
    _vGetTime(szTmp);
    sprintf(szBuf, "时  间:%.4s/%.2s/%.2s %.2s:%.2s:%.2s", szTmp, szTmp+4, szTmp+6, szTmp+8, szTmp+10, szTmp+12);
    _uiPrintEx(1, szBuf, 0);

    memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    _uiPrintEx(1, szBuf, 0);
    
    _uiPrintEx(1, "  交易类型   总笔数      总金额", 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "消费", iSaleNum, ulSaleAmt/100, ulSaleAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "消费撤销", iSaleVoidNum, ulSaleVoidAmt/100, ulSaleVoidAmt%100);
    _uiPrintEx(1, szBuf, 0);

    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "预授权完成", iAuthCmpNum, ulAuthCmpAmt/100, ulAuthCmpAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "预授权完成撤销", iAuthCmpVoidNum, ulAuthCmpVoidAmt/100, ulAuthCmpVoidAmt%100);
    _uiPrintEx(1, szBuf, 0);
    
	sprintf(szBuf, "%-14s %4d %8lu.%02lu", "退货", iRefundNum, ulRefundAmt/100, ulRefundAmt%100);
	_uiPrintEx(1, szBuf, 0);    
		
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信扫码付", iWetchatScanSaleNum, ulWetchatScanSaleAmt/100, ulWetchatScanSaleAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信付款码", iWetchatQrSaleNum, ulWetchatQrSaleAmt/100, ulWetchatQrSaleAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信撤销", iWetchatSaleVoidNum, ulWetchatSaleVoidAmt/100, ulWetchatSaleVoidAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信退货", iWetchatSaleRefundNum, ulWetchatSaleRefundAmt/100, ulWetchatSaleRefundAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微扫预授权", iWetchatScanPreauthNum, ulWetchatScanPreauthAmt/100, ulWetchatScanPreauthAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微码预授权", iWetchatQrPreauthNum, ulWetchatQrPreauthAmt/100, ulWetchatQrPreauthAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微预授权完成", iWetchatPreauthCompNum, ulWetchatPreauthCompAmt/100, ulWetchatPreauthCompAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微预授权撤销", iWetchatPreauthVoidNum, ulWetchatPreauthCompAmt/100, ulWetchatPreauthCompAmt%100);
    _uiPrintEx(1, szBuf, 0);
	
       sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝扫码付", iAlipayScanSaleNum, ulAlipayScanSaleAmt/100, ulAlipayScanSaleAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝付款码", iAlipayQrSaleNum, ulAlipayQrSaleAmt/100, ulAlipayQrSaleAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝撤销", iAlipaySaleVoidNum, ulAlipaySaleVoidAmt/100, ulAlipaySaleVoidAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝退货", iAlipaySaleRefundNum, ulAlipaySaleRefundAmt/100, ulAlipaySaleRefundAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支扫预授权", iAlipayScanPreauthNum, ulAlipayScanPreauthAmt/100, ulAlipayScanPreauthAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支码预授权", iAlipayQrPreauthNum, ulAlipayQrPreauthAmt/100, ulAlipayQrPreauthAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权完成", iAlipayPreauthCompNum, ulAlipayPreauthCompAmt/100, ulAlipayPreauthCompAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权撤销", iAlipayPreauthVoidNum, ulAlipayPreauthCompAmt/100, ulAlipayPreauthCompAmt%100);	

    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联扫码付", iUnionpayScanSaleNum, ulUnionpayScanSaleAmt/100, ulUnionpayScanSaleAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联付款码", iUnionpayQrSaleNum, ulUnionpayQrSaleAmt/100, ulUnionpayQrSaleAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联撤销", iUnionpaySaleVoidNum, ulUnionpaySaleVoidAmt/100, ulUnionpaySaleVoidAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联退货", iUnionpaySaleRefundNum, ulUnionpaySaleRefundAmt/100, ulUnionpaySaleRefundAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银扫预授权", iUnionpayScanPreauthNum, ulUnionpayScanPreauthAmt/100, ulUnionpayScanPreauthAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银码预授权", iUnionpayQrPreauthNum, ulUnionpayQrPreauthAmt/100, ulUnionpayQrPreauthAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银预授权完成", iUnionpayPreauthCompNum, ulUnionpayPreauthCompAmt/100, ulUnionpayPreauthCompAmt%100);
    _uiPrintEx(1, szBuf, 0);
    sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银预授权撤销", iUnionpayPreauthVoidNum, ulUnionpayPreauthCompAmt/100, ulUnionpayPreauthCompAmt%100);	
    
     memset(szBuf, '-', _uiGetPCols());
    szBuf[_uiGetPCols()]=0;
    _uiPrintEx(1, szBuf, 0);
    
    sprintf(szBuf, "%s_%s 服务热线:%s", _pszGetModelName(NULL), _pszGetAppVer(NULL), gl_SysInfo.ucServiceHotline);
    _uiPrintEx(11, szBuf, 0);

    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 1);
    
    _uiPrintClose();       
    
    return 0;
}

int iPrintSettle(stSettleInfo *sett,uchar ucPrintSettleFlag)
{   
    stSettleInfo settTmp;	
    char szBuf[100], szTmp[100];
	
    if(ucPrintSettleFlag == 1)	
    {
		_vCls();
		vDispCenter(1, "重打印结算单", 1);
		if(uiMemManaGetSettleInfo(&settTmp))
		{
			vMessage("没有上笔结算单数据");
			 _uiPrintClose();       
			return 0;
		}
		if(settTmp.ulBatchNo==0)
		{
			vMessage("没有上笔结算单数据");
			 _uiPrintClose();       
			return 0;
		}

		sett=&settTmp;
		
		if(_uiPrintOpen())
		    	return 1;
		_uiPrintSetFont(1);
		_uiPrintEx(2, "结算单", 0);
		_uiPrintSetFont(2);

		sprintf(szBuf, "商户名:%s", gl_SysInfo.szMerchName);
		_uiPrintEx(11, szBuf, 0);
		sprintf(szBuf, "商户号:%s", gl_SysInfo.szMerchId);
		_uiPrintEx(1, szBuf, 0);
	
		vOneTwo0(sett->sOper, 1, szTmp);
		sprintf(szBuf, "终端号:%s    操作员:%.2s", gl_SysInfo.szPosId, szTmp);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "批次号:%06lu", sett->ulBatchNo);
		_uiPrintEx(1, szBuf, 0);

		vOneTwo0(sett->sDateTime, 6, szTmp+2);
		sprintf(szBuf, "时  间:20%.2s/%.2s/%.2s %.2s:%.2s:%.2s", szTmp+2, szTmp+4, szTmp+6, szTmp+8, szTmp+10, szTmp+12);
		_uiPrintEx(1, szBuf, 0);			

		memset(szBuf, '-', _uiGetPCols());
		szBuf[_uiGetPCols()]=0;
		_uiPrintEx(1, szBuf, 0);	             
				
		_uiPrintEx(1, "银行卡交易:", 0);
		_uiPrintEx(1, "  交易类型   总笔数      总金额", 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "消费", sett->iSaleNum, sett->ulSaleAmt/100, sett->ulSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);

		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "预授权完成", sett->iAuthCmpNum, sett->ulAuthCmpAmt/100, sett->ulAuthCmpAmt%100);
		_uiPrintEx(1, szBuf, 0);
    
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "退货", sett->iRefundNum, sett->ulRefundAmt/100, sett->ulRefundAmt%100);
		_uiPrintEx(1, szBuf, 0); 

		memset(szBuf, '-', _uiGetPCols());
		szBuf[_uiGetPCols()]=0;
		_uiPrintEx(1, szBuf, 0);
	
		_uiPrintEx(1, "扫码交易:", 0);
		_uiPrintEx(1, "  交易类型   总笔数      总金额", 0);
 		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信扫码付",sett->iWetchatScanSaleNum, sett->ulWetchatScanSaleAmt/100, sett->ulWetchatScanSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信付款码", sett->iWetchatQrSaleNum, sett->ulWetchatQrSaleAmt/100, sett->ulWetchatQrSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信撤销",sett->iWetchatSaleVoidNum, sett->ulWetchatSaleVoidAmt/100, sett->ulWetchatSaleVoidAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信退货", sett->iWetchatSaleRefundNum, sett->ulWetchatSaleRefundAmt/100, sett->ulWetchatSaleRefundAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微扫预授权", sett->iWetchatScanPreauthNum, sett->ulWetchatScanPreauthAmt/100, sett->ulWetchatScanPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微码预授权", sett->iWetchatQrPreauthNum, sett->ulWetchatQrPreauthAmt/100, sett->ulWetchatQrPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微预授权完成", sett->iWetchatPreauthCompNum, sett->ulWetchatPreauthCompAmt/100, sett->ulWetchatPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微预授权撤销",sett->iWetchatPreauthVoidNum, sett->ulWetchatPreauthCompAmt/100, sett->ulWetchatPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
		
		 sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝扫码付",sett->iAlipayScanSaleNum, sett->ulAlipayScanSaleAmt/100, sett->ulAlipayScanSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝付款码", sett->iAlipayQrSaleNum, sett->ulAlipayQrSaleAmt/100, sett->ulAlipayQrSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝撤销",sett->iAlipaySaleVoidNum, sett->ulAlipaySaleVoidAmt/100, sett->ulAlipaySaleVoidAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝退货", sett->iAlipaySaleRefundNum, sett->ulAlipaySaleRefundAmt/100, sett->ulAlipaySaleRefundAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权", sett->iAlipayScanPreauthNum, sett->ulAlipayScanPreauthAmt/100, sett->ulAlipayScanPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权", sett->iAlipayQrPreauthNum, sett->ulAlipayQrPreauthAmt/100, sett->ulAlipayQrPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权完成", sett->iAlipayPreauthCompNum, sett->ulAlipayPreauthCompAmt/100, sett->ulAlipayPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权撤销",sett->iAlipayPreauthVoidNum, sett->ulAlipayPreauthCompAmt/100, sett->ulAlipayPreauthCompAmt%100);
               _uiPrintEx(1, szBuf, 0);
			   
                 sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联扫码付",sett->iUnionpayScanSaleNum, sett->ulUnionpayScanSaleAmt/100, sett->ulUnionpayScanSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联付款码", sett->iUnionpayQrSaleNum, sett->ulUnionpayQrSaleAmt/100, sett->ulUnionpayQrSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联撤销",sett->iUnionpaySaleVoidNum, sett->ulUnionpaySaleVoidAmt/100, sett->ulUnionpaySaleVoidAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联退货", sett->iUnionpaySaleRefundNum, sett->ulUnionpaySaleRefundAmt/100, sett->ulUnionpaySaleRefundAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银扫预授权", sett->iUnionpayScanPreauthNum, sett->ulUnionpayScanPreauthAmt/100, sett->ulUnionpayScanPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银码预授权", sett->iUnionpayQrPreauthNum, sett->ulUnionpayQrPreauthAmt/100, sett->ulUnionpayQrPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银预授权完成", sett->iUnionpayPreauthCompNum, sett->ulUnionpayPreauthCompAmt/100, sett->ulUnionpayPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银预授权撤销",sett->iUnionpayPreauthVoidNum, sett->ulUnionpayPreauthCompAmt/100, sett->ulUnionpayPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
 
    } else if(ucPrintSettleFlag == 0)
    { 
               	if(_uiPrintOpen())
		    	return 1;
		_uiPrintSetFont(1);
		_uiPrintEx(2, "结算单", 0);
		_uiPrintSetFont(2);

		sprintf(szBuf, "商户名:%s", gl_SysInfo.szMerchName);
		_uiPrintEx(11, szBuf, 0);
		sprintf(szBuf, "商户号:%s", gl_SysInfo.szMerchId);
		_uiPrintEx(1, szBuf, 0);

		vOneTwo0(sett->sOper, 1, szTmp);
		sprintf(szBuf, "终端号:%s    操作员:%.2s", gl_SysInfo.szPosId, szTmp);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "批次号:%06lu", sett->ulBatchNo);
		_uiPrintEx(1, szBuf, 0);
		
		vOneTwo0(sett->sDateTime, 6, szTmp+2);
		sprintf(szBuf, "时  间:20%.2s/%.2s/%.2s %.2s:%.2s:%.2s", szTmp+2, szTmp+4, szTmp+6, szTmp+8, szTmp+10, szTmp+12);
		_uiPrintEx(1, szBuf, 0);	

		memset(szBuf, '-', _uiGetPCols());
		szBuf[_uiGetPCols()]=0;
		_uiPrintEx(1, szBuf, 0);	
		
		_uiPrintEx(1, "银行卡交易:", 0);
		_uiPrintEx(1, "  交易类型   总笔数      总金额", 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "消费", sett->iSaleNum, sett->ulSaleAmt/100, sett->ulSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);

		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "预授权完成", sett->iAuthCmpNum, sett->ulAuthCmpAmt/100, sett->ulAuthCmpAmt%100);
		_uiPrintEx(1, szBuf, 0);
    
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "退货", sett->iRefundNum, sett->ulRefundAmt/100, sett->ulRefundAmt%100);
		_uiPrintEx(1, szBuf, 0); 

		memset(szBuf, '-', _uiGetPCols());
		szBuf[_uiGetPCols()]=0;
		_uiPrintEx(1, szBuf, 0);
	
		_uiPrintEx(1, "扫码交易:", 0);
		_uiPrintEx(1, "  交易类型   总笔数      总金额", 0);
		 sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信扫码付",sett->iWetchatScanSaleNum, sett->ulWetchatScanSaleAmt/100, sett->ulWetchatScanSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信付款码", sett->iWetchatQrSaleNum, sett->ulWetchatQrSaleAmt/100, sett->ulWetchatQrSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信撤销",sett->iWetchatSaleVoidNum, sett->ulWetchatSaleVoidAmt/100, sett->ulWetchatSaleVoidAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微信退货", sett->iWetchatSaleRefundNum, sett->ulWetchatSaleRefundAmt/100, sett->ulWetchatSaleRefundAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微扫预授权", sett->iWetchatScanPreauthNum, sett->ulWetchatScanPreauthAmt/100, sett->ulWetchatScanPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微码预授权", sett->iWetchatQrPreauthNum, sett->ulWetchatQrPreauthAmt/100, sett->ulWetchatQrPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微预授权完成", sett->iWetchatPreauthCompNum, sett->ulWetchatPreauthCompAmt/100, sett->ulWetchatPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "微预授权撤销",sett->iWetchatPreauthVoidNum, sett->ulWetchatPreauthCompAmt/100, sett->ulWetchatPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
		
		 sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝扫码付",sett->iAlipayScanSaleNum, sett->ulAlipayScanSaleAmt/100, sett->ulAlipayScanSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝付款码", sett->iAlipayQrSaleNum, sett->ulAlipayQrSaleAmt/100, sett->ulAlipayQrSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝撤销",sett->iAlipaySaleVoidNum, sett->ulAlipaySaleVoidAmt/100, sett->ulAlipaySaleVoidAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支付宝退货", sett->iAlipaySaleRefundNum, sett->ulAlipaySaleRefundAmt/100, sett->ulAlipaySaleRefundAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权", sett->iAlipayScanPreauthNum, sett->ulAlipayScanPreauthAmt/100, sett->ulAlipayScanPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权", sett->iAlipayQrPreauthNum, sett->ulAlipayQrPreauthAmt/100, sett->ulAlipayQrPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权完成", sett->iAlipayPreauthCompNum, sett->ulAlipayPreauthCompAmt/100, sett->ulAlipayPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "支预授权撤销",sett->iAlipayPreauthVoidNum, sett->ulAlipayPreauthCompAmt/100, sett->ulAlipayPreauthCompAmt%100);
               _uiPrintEx(1, szBuf, 0);
			   
                 sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联扫码付",sett->iUnionpayScanSaleNum, sett->ulUnionpayScanSaleAmt/100, sett->ulUnionpayScanSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联付款码", sett->iUnionpayQrSaleNum, sett->ulUnionpayQrSaleAmt/100, sett->ulUnionpayQrSaleAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联撤销",sett->iUnionpaySaleVoidNum, sett->ulUnionpaySaleVoidAmt/100, sett->ulUnionpaySaleVoidAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银联退货", sett->iUnionpaySaleRefundNum, sett->ulUnionpaySaleRefundAmt/100, sett->ulUnionpaySaleRefundAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银扫预授权", sett->iUnionpayScanPreauthNum, sett->ulUnionpayScanPreauthAmt/100, sett->ulUnionpayScanPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银码预授权", sett->iUnionpayQrPreauthNum, sett->ulUnionpayQrPreauthAmt/100, sett->ulUnionpayQrPreauthAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银预授权完成", sett->iUnionpayPreauthCompNum, sett->ulUnionpayPreauthCompAmt/100, sett->ulUnionpayPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
		sprintf(szBuf, "%-14s %4d %8lu.%02lu", "银预授权撤销",sett->iUnionpayPreauthVoidNum, sett->ulUnionpayPreauthCompAmt/100, sett->ulUnionpayPreauthCompAmt%100);
		_uiPrintEx(1, szBuf, 0);
    	}
	
	memset(szBuf, '-', _uiGetPCols());
	szBuf[_uiGetPCols()]=0;
	_uiPrintEx(1, szBuf, 0);

	 //服务热线
	   sprintf(szBuf,"服务热线:%s",gl_SysInfo.ucServiceHotline);
	   _uiPrintEx(1,szBuf,0);

    if(ucPrintSettleFlag == 1)	
    {
    	_uiPrintEx(1, "备注:", 0);
       	 _uiPrintSetFont(1);
        _uiPrintEx(2, "重打印", 0);
        _uiPrintSetFont(2);
    }	
	
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 1);
    
    _uiPrintClose();       
 
    return 0;
}

//注意查询/打印汇总与结算不同(flage 0 查询/打印汇总1 结算汇总)
void vGenTransSum(int *piSaleNum, ulong *pulSaleAmt, int *piSaleVoidNum, ulong *pulSaleVoidAmt, 
        int *piAuthCmpNum, ulong *pulAuthCmpAmt, int *piAuthCmpVoidNum, ulong *pulAuthCmpVoidAmt,
        int *piRefundNum, ulong *pulRefundAmt,uchar flag)
{
    int i;
    uint uiRet;
    stTransRec rec;
    int iSaleNum=0, iSaleVoidNum=0, iAuthCmpNum=0, iAuthCmpVoidNum=0, iRefundNum=0;
    ulong ulSaleAmt=0L, ulSaleVoidAmt=0L, ulAuthCmpAmt=0L, ulAuthCmpVoidAmt=0L, ulRefundAmt=0L;

    for(i=0; i<gl_SysData.uiTransNum; i++)
    {
        memset(&rec, 0 , sizeof(stTransRec));
        uiRet=uiMemManaGetTransRec(i, &rec);
        if(uiRet)
            dbg("GetTransRec fail:%d\n", uiRet);

        dbg("GetTransRec, %d uiTransType=%04X\n", i, rec.uiTransType);
        if(rec.uiTransType>=0x8000)
            continue;
        switch(rec.uiTransType)
        {
            case TRANS_TYPE_SALE:
	    case TRANS_TYPE_DAIRY_SALE:
		 if(flag == 1 && rec.ucVoidFlag == 0x01)
		 	continue;
                iSaleNum++;
                ulSaleAmt+=rec.ulAmount;
                break;
            case TRANS_TYPE_SALEVOID:
		if(flag == 1) continue;		
                iSaleVoidNum++;
                ulSaleVoidAmt+=rec.ulAmount;
                break;
            case TRANS_TYPE_PREAUTH_COMP:
		 if(flag ==1 && rec.ucVoidFlag == 0x01)
		 	continue;
                iAuthCmpNum++;
                ulAuthCmpAmt+=rec.ulAmount;
                break;
            case TRANS_TYPE_PREAUTH_COMPVOID:
		if(flag == 1) continue;			
                iAuthCmpVoidNum++;
                ulAuthCmpVoidAmt+=rec.ulAmount;
                break;
            case TRANS_TYPE_REFUND:
                iRefundNum++;
                ulRefundAmt+=rec.ulAmount;
                break;
            default:
                break;
        }
    }

    if(piSaleNum)
    {
        *piSaleNum=iSaleNum;
        *pulSaleAmt=ulSaleAmt;
    }
    if(piSaleVoidNum)
    {
        *piSaleVoidNum=iSaleVoidNum;
        *pulSaleVoidAmt=ulSaleVoidAmt;
    }
    if(piAuthCmpNum)
    {
        *piAuthCmpNum=iAuthCmpNum;
        *pulAuthCmpAmt=ulAuthCmpAmt;
    }
    if(piAuthCmpVoidNum)
    {
        *piAuthCmpVoidNum=iAuthCmpVoidNum;
        *pulAuthCmpVoidAmt=ulAuthCmpVoidAmt;
    }
    if(piRefundNum)
    {
        *piRefundNum=iRefundNum;
        *pulRefundAmt=ulRefundAmt;
    }
    return;
}

//注意查询/打印汇总与结算不同(flage 0 查询/打印汇总1 结算汇总)
void vGenQrTransSum_hrt(int *piScanSaleNum, ulong *pulScanSaleAmt, int *piQrSaleNum, ulong *pulQrSaleAmt, uchar flag)
{
   int i;
    uint uiRet;
    stQrTransRec QRrec;
    int iScanSaleNum=0, iQrSaleNum=0;
    ulong ulScanSaleAmt=0L, ulQrSaleAmt=0L;
    
    for(i=0; i<gl_SysData.uiQrTransNum; i++)
    {
        memset(&QRrec, 0 , sizeof(stQrTransRec));
        uiRet=uiMemManaGetQrTransRec(i, &QRrec);
        if(uiRet)
            dbg("GetQrTransRec fail:%d\n", uiRet);

        dbg("GetQrTransRec, %d, uiTransType=%04X\n", i, QRrec.uiTransType);
        if(QRrec.uiTransType<0x8000)
            continue;
	if(QRrec.uiTransType==QRTRANS_TYPE_SALE)		
        {
                 if(QRrec.ucPayMode == B_posTophone)
                 {
		if(QRrec.ucProType == 0x02)
			continue;
	       else if(flag == 1 && QRrec.ucVoidFlag == 0x01)
 			continue;
		   
		iScanSaleNum++;
		ulScanSaleAmt+=QRrec.ulAmount;
                 }else  if(QRrec.ucPayMode == Z_phoneTopos)
                 {

                 	if(QRrec.ucProType == 0x02)
			continue;
                        else if(flag == 1 && QRrec.ucVoidFlag == 0x01)
 			continue;
						
		iQrSaleNum++;
		ulQrSaleAmt+=QRrec.ulAmount;
                 }				
        }
	continue;		
    }
	
	if(iScanSaleNum)
	{
		*piScanSaleNum=iScanSaleNum;
		*pulScanSaleAmt=ulScanSaleAmt;
	}
	if(iQrSaleNum)
	{
		*piQrSaleNum=iQrSaleNum;
		*pulQrSaleAmt=ulQrSaleAmt;
	}
    return;
}

//注意查询/打印汇总与结算不同(flage 0 查询/打印汇总1 结算汇总)
void vGenQrTransSum(int *piScanSaleNum, ulong *pulScanSaleAmt, int *piQrSaleNum, ulong *pulQrSaleAmt, int *piSaleVoidNum, ulong *pulSaleVoidAmt, 
        int *piSaleRefundNum, ulong *pulSaleRefundAmt, uchar PayWay,uchar flag)
{
   int i;
    uint uiRet;
    stQrTransRec QRrec;
    int iScanSaleNum=0, iQrSaleNum=0, iSaleVoidNum=0,iSaleRefundNum=0;
    ulong ulScanSaleAmt=0L, ulQrSaleAmt=0L, ulSaleVoidAmt=0L, ulSaleRefundAmt=0L;
    
    for(i=0; i<gl_SysData.uiQrTransNum; i++)
    {
        memset(&QRrec, 0 , sizeof(stQrTransRec));
        uiRet=uiMemManaGetQrTransRec(i, &QRrec);
        if(uiRet)
            dbg("GetQrTransRec fail:%d\n", uiRet);

        dbg("GetQrTransRec, %d, uiTransType=%04X\n", i, QRrec.uiTransType);
        if(QRrec.uiTransType<0x8000)
            continue;
	if(QRrec.uiTransType==QRTRANS_TYPE_SALE)		
        {
	            if(PayWay == MODE_WETCHAT && QRrec.ucPayWay == MODE_WETCHAT)
	            {
	                     if(QRrec.ucPayMode == B_posTophone)
	                     {
				if(QRrec.ucProType == 0x02)
					continue;
			       else if(flag == 1 && QRrec.ucVoidFlag == 0x01)
		 			continue;
				   
				iScanSaleNum++;
				ulScanSaleAmt+=QRrec.ulAmount;
	                     }else  if(QRrec.ucPayMode == Z_phoneTopos)
	                     {

	                     	if(QRrec.ucProType == 0x02)
					continue;
                                else if(flag == 1 && QRrec.ucVoidFlag == 0x01)
		 			continue;
								
				iQrSaleNum++;
				ulQrSaleAmt+=QRrec.ulAmount;
	                     }				
	            }else if(PayWay == MODE_ALIPAY && QRrec.ucPayWay == MODE_ALIPAY)
	            {
	            	      if(QRrec.ucPayMode == B_posTophone)
	                     {
				if(QRrec.ucProType == 0x02)
					continue;
                                else if(flag == 1 && QRrec.ucVoidFlag == 0x01)
		 			continue;
								
				iScanSaleNum++;
				ulScanSaleAmt+=QRrec.ulAmount;
	                     }else  if(QRrec.ucPayMode == Z_phoneTopos)
	                     {
	                     	if(QRrec.ucProType == 0x02)
					continue;
                                else if(flag == 1 && QRrec.ucVoidFlag == 0x01)
		 			continue;
								
				iQrSaleNum++;
				ulQrSaleAmt+=QRrec.ulAmount;
	                     }	
	            } else if(PayWay == MODE_UNIONPAY && QRrec.ucPayWay == MODE_UNIONPAY)
	            {
	            	     if(QRrec.ucPayMode == B_posTophone)
	                     {
				if(QRrec.ucProType == 0x02)
					continue;
                                else if(flag == 1 && QRrec.ucVoidFlag == 0x01)
		 			continue;
								
				iScanSaleNum++;
				ulScanSaleAmt+=QRrec.ulAmount;
	                     }else  if(QRrec.ucPayMode == Z_phoneTopos)
	                     {
	                     	if(QRrec.ucProType == 0x02)
					continue;
                               else if(flag == 1 && QRrec.ucVoidFlag == 0x01)
		 			continue;
							   
				iQrSaleNum++;
				ulQrSaleAmt+=QRrec.ulAmount;
	                     }	
	            }		
        }else if(QRrec.uiTransType==QRTRANS_TYPE_VOID)
        {
			if(PayWay == MODE_WETCHAT && QRrec.ucPayWay == MODE_WETCHAT)
	            {
				iSaleVoidNum++;
				ulSaleVoidAmt+=QRrec.ulAmount;
	              		
	            }else if(PayWay == MODE_ALIPAY && QRrec.ucPayWay == MODE_ALIPAY)
	            {
				iSaleVoidNum++;
				ulSaleVoidAmt+=QRrec.ulAmount;
	                  
	            } else if(PayWay == MODE_UNIONPAY && QRrec.ucPayWay == MODE_UNIONPAY)
	            {
				iSaleVoidNum++;
				ulSaleVoidAmt+=QRrec.ulAmount;             
	            }	
			
        }else if(QRrec.uiTransType==QRTRANS_TYPE_REFUND)
        {
        	    if(PayWay == MODE_WETCHAT && QRrec.ucPayWay == MODE_WETCHAT)
	            {
				iSaleRefundNum++;
				ulSaleRefundAmt+=QRrec.ulAmount;
	              		
	            }else if(PayWay == MODE_ALIPAY && QRrec.ucPayWay == MODE_ALIPAY)
	            {
				iSaleRefundNum++;
				ulSaleRefundAmt+=QRrec.ulAmount;
	                  
	            } else if(PayWay == MODE_UNIONPAY && QRrec.ucPayWay == MODE_UNIONPAY)
	            {
				iSaleRefundNum++;
				ulSaleRefundAmt+=QRrec.ulAmount;           
	            }				
        }
	continue;		
    }
	
	if(iScanSaleNum)
	{
		*piScanSaleNum=iScanSaleNum;
		*pulScanSaleAmt=ulScanSaleAmt;
	}
	if(iQrSaleNum)
	{
		*piQrSaleNum=iQrSaleNum;
		*pulQrSaleAmt=ulQrSaleAmt;
	}
	if(iSaleVoidNum)
	{
		*piSaleVoidNum=iSaleVoidNum;
		*pulSaleVoidAmt=ulSaleVoidAmt;
	}
       if(iSaleRefundNum)
	{
		*piSaleRefundNum=iSaleRefundNum;
		*pulSaleVoidAmt=ulSaleRefundAmt;
	}
    return;
}

//注意查询/打印汇总与结算不同(flage 0 查询/打印汇总1 结算汇总)
void vGenQrPreauthTransSum(int *piScanPreauthNum, ulong *pulScanPreauthAmt, int *piQrPreauthNum, ulong *pulQrPreauthAmt, int *piPreauthCompNum, ulong *pulPreauthCompAmt, 
        int *piPreauthVoidNum, ulong *pulPreauthVoidAmt, uchar PayWay,uchar flag)
{
   int i;
    uint uiRet;
    stQrTransRec QRrec;
    int iScanPreauthNum=0, iQrPreauthNum=0, iPreauthCompNum=0,iPreauthVoidNum=0;
    ulong ulScanPreauthAmt=0L, ulQrPreauthAmt=0L, ulPreauthCompAmt=0L, ulPreauthVoidAmt=0L;
    
    for(i=0; i<gl_SysData.uiQrTransNum; i++)
    {
        memset(&QRrec, 0 , sizeof(stQrTransRec));
        uiRet=uiMemManaGetQrTransRec(i, &QRrec);
        if(uiRet)
            dbg("GetQrTransRec fail:%d\n", uiRet);

        dbg("GetQrTransRec, %d, uiTransType=%04X\n", i, QRrec.uiTransType);
        if(QRrec.uiTransType<0x8000)
            continue;
	if(QRrec.uiTransType==SCAN_TRANS_TYPE_PREAUTH)		
        {
	            if(PayWay == MODE_WETCHAT && QRrec.ucPayWay == MODE_WETCHAT)
	            {
	                     if(QRrec.ucPayMode == B_posTophone)
	                     {
				if(QRrec.ucProType == 0x02)
					continue;
				else if(flag == 1 && (QRrec.ucVoidFlag == 0x01 || QRrec.ucVoidFlag == 0x02))
		 			continue;
				
				iScanPreauthNum++;
				ulScanPreauthAmt+=QRrec.ulAmount;
	                     }else  if(QRrec.ucPayMode == Z_phoneTopos)
	                     {

	                     	if(QRrec.ucProType == 0x02)
					continue;
                                else if(flag == 1 && (QRrec.ucVoidFlag == 0x01 || QRrec.ucVoidFlag == 0x02))
		 			continue;
								
				iQrPreauthNum++;
				ulQrPreauthAmt+=QRrec.ulAmount;
	                     }				
	            }else if(PayWay == MODE_ALIPAY && QRrec.ucPayWay == MODE_ALIPAY)
	            {
	            	      if(QRrec.ucPayMode == B_posTophone)
	                     {
				if(QRrec.ucProType == 0x02)
					continue;
				else if(flag == 1 && (QRrec.ucVoidFlag == 0x01 || QRrec.ucVoidFlag == 0x02))
		 			continue;
				
				iScanPreauthNum++;
				ulScanPreauthAmt+=QRrec.ulAmount;
	                     }else  if(QRrec.ucPayMode == Z_phoneTopos)
	                     {

	                     	if(QRrec.ucProType == 0x02)
					continue;
				else if(flag == 1 && (QRrec.ucVoidFlag == 0x01 || QRrec.ucVoidFlag == 0x02))
		 			continue;
				
				iQrPreauthNum++;
				ulQrPreauthAmt+=QRrec.ulAmount;
	                     }	
	            } else if(PayWay == MODE_UNIONPAY && QRrec.ucPayWay == MODE_UNIONPAY)
	            {
	            	     if(QRrec.ucPayMode == B_posTophone)
	                     {
				if(QRrec.ucProType == 0x02)
					continue;
                                else if(flag == 1 && (QRrec.ucVoidFlag == 0x01 || QRrec.ucVoidFlag == 0x02))
		 			continue;
								
				iScanPreauthNum++;
				ulScanPreauthAmt+=QRrec.ulAmount;
	                     }else  if(QRrec.ucPayMode == Z_phoneTopos)
	                     {
	                     	if(QRrec.ucProType == 0x02)
					continue;
                               else if(flag == 1 && (QRrec.ucVoidFlag == 0x01 || QRrec.ucVoidFlag == 0x02))
		 			continue;
							   
				iQrPreauthNum++;
				ulQrPreauthAmt+=QRrec.ulAmount;
	                     }	
	            }		
        }else if(QRrec.uiTransType==SCAN_TRANS_TYPE_PREAUTH_COMP)
        {
			if(PayWay == MODE_WETCHAT && QRrec.ucPayWay == MODE_WETCHAT)
	            {
				iPreauthCompNum++;
				ulPreauthCompAmt+=QRrec.ulAmount;
	              		
	            }else if(PayWay == MODE_ALIPAY && QRrec.ucPayWay == MODE_ALIPAY)
	            {
				iPreauthCompNum++;
				ulPreauthCompAmt+=QRrec.ulAmount;
	                  
	            } else if(PayWay == MODE_UNIONPAY && QRrec.ucPayWay == MODE_UNIONPAY)
	            {
				iPreauthCompNum++;
				ulPreauthCompAmt+=QRrec.ulAmount;   
	            }	
			
        }else if(QRrec.uiTransType==SCAN_TRANS_TYPE_PREAUTHVOID)
        {
        	    if(PayWay == MODE_WETCHAT && QRrec.ucPayWay == MODE_WETCHAT)
	            {
				iPreauthVoidNum++;
				ulPreauthVoidAmt+=QRrec.ulAmount;
	              		
	            }else if(PayWay == MODE_ALIPAY && QRrec.ucPayWay == MODE_ALIPAY)
	            {
				iPreauthVoidNum++;
				ulPreauthVoidAmt+=QRrec.ulAmount;
	                  
	            } else if(PayWay == MODE_UNIONPAY && QRrec.ucPayWay == MODE_UNIONPAY)
	            {
				iPreauthVoidNum++;
				ulPreauthVoidAmt+=QRrec.ulAmount;      
	            }				
        }
	continue;		
    }
	
	if(iScanPreauthNum)
	{
		*piScanPreauthNum=iScanPreauthNum;
		*pulScanPreauthAmt=ulScanPreauthAmt;
	}
	if(iQrPreauthNum)
	{
		*piQrPreauthNum=iQrPreauthNum;
		*pulQrPreauthAmt=ulQrPreauthAmt;
	}
	if(iPreauthCompNum)
	{
		*piPreauthCompNum=iPreauthCompNum;
		*pulPreauthCompAmt=ulPreauthCompAmt;
	}
       if(iPreauthVoidNum)
	{
		*piPreauthVoidNum=iPreauthVoidNum;
		*pulPreauthVoidAmt=ulPreauthVoidAmt;
	}
    return;
}

int iViewTransSum(void)
{
    int iSaleNum=0, iSaleVoidNum=0, iAuthCmpNum=0, iAuthCmpVoidNum=0, iRefundNum=0;
    ulong ulSaleAmt=0L, ulSaleVoidAmt=0L, ulAuthCmpAmt=0L, ulAuthCmpVoidAmt=0L, ulRefundAmt=0L;
  
    int page, line;
    uint uiKey;
    ulong ulTimer;
    int maxpage=1;
    uchar isexit = 0;
    char szTmp[20+1];
	
    _vCls();
    vDispCenter(1, "交易汇总", 1);

    vGenTransSum(&iSaleNum, &ulSaleAmt, &iSaleVoidNum, &ulSaleVoidAmt, 
                &iAuthCmpNum, &ulAuthCmpAmt, &iAuthCmpVoidNum, &ulAuthCmpVoidAmt,
                &iRefundNum, &ulRefundAmt,0);
	
//   vGenQrTransSum_hrt(&iScanSaleNum, &ulScanSaleAmt, &iQrSaleNum, &ulQrSaleAmt,0);
	
    page=1;
    while(isexit != 1)
    {
        if(_uiGetVLines()>6)
            line=2;
        else
            line=2;
        if(page==1)
        {
        //    vDispVarArg(line++, "消费总笔数:%d", iSaleNum);
        //    vDispVarArg(line++, "消费总金额:%lu.%02lu", ulSaleAmt/100, ulSaleAmt%100);
               vDispVarArg(line++, "消费总笔数:");	
		sprintf(szTmp,"%d",iSaleNum);
               vDispRight(line++,szTmp);
			   	
	       vDispVarArg(line++, "消费总金额:");	
	       sprintf(szTmp,"%lu.%02lu",ulSaleAmt/100, ulSaleAmt%100);
	       vDispRight(line++,szTmp);		   
        } 
/*		
	 if(page==2)
        {
      	    vDispCenterEx(line++, "扫码交易", 0, 1, page, maxpage);
            vDispVarArg(line++, "支付总笔数:%d", iScanSaleNum+iQrSaleNum);
            vDispVarArg(line++, "支付总金额:%lu.%02lu", (ulScanSaleAmt+ulQrSaleAmt)/100, (ulScanSaleAmt+ulQrSaleAmt)%100);	    
            vClearLines(line);
        }
 */    
        _vFlushKey();	
       _vSetTimer(&ulTimer, 6000L); //60秒
        while(1)
        {
            if(_uiTestTimer(ulTimer))
	   {
		isexit = 1;
		break;
            }
            else if(_uiKeyPressed())
            {
		uiKey=_uiGetKey();
                if(uiKey==_KEY_UP && page>1)
                {
                    page--;
                    break;
                }
                if((uiKey == _KEY_DOWN || uiKey == _KEY_ENTER ) && page<maxpage)
                {
                    page++;
                    break;
                }
                if(uiKey == _KEY_ESC)
                    return 1;
                if((uiKey == _KEY_DOWN || uiKey == _KEY_ENTER )  && page == maxpage)
                {
                	isexit = 1;
		   	break;
                }
            }	
        }
    }
    return 0;
}

int iViewTransByTTC(void)
{
    int iRet;
    char buf[50]={0};
    ulong ulTTC;
    stTransRec rec;
    stQrTransRec QrTrRec;
    int line;
    uchar ucSign;
    uint uiKey,uiRet;
    int page = 0;	
   uchar ucPinPos;
    char szTitle[50+1]= {0};  
    char tmp[50];
   ulong ulTimer;
    int idxcard, idxqr;
		
    _vCls();
    vDispCenter(1, "按凭证号查询交易", 1);

    if(gl_SysData.uiTransNum==0 && gl_SysData.uiQrTransNum==0)
    {
        vMessage("没有交易");
        return 0;
    }
	
    _vDisp(2, "请输入凭证号:"); 
    ucPinPos = _uiGetVCols();
    iRet=iInput(INPUT_NORMAL|INPUT_LEFT, 3, ucPinPos, buf, 6, 1, 6, 60);
    if(iRet<0)
        return -1;
    ulTTC=atol(buf);
    if(ulTTC==0)
    {
        vMessage("无效凭证号");
        return -1;
    }

    idxcard = 0;
    page = 1;
    while(idxcard < gl_SysData.uiTransNum)
    {
        uiRet = uiMemManaGetTransRec(idxcard, &rec);
        if(uiRet)
                    return 1;
		
        if(rec.ulTTC==ulTTC)
        {
            ucSign=0;
            switch (rec.uiTransType)
            {
            case TRANS_TYPE_SALE:
                strcpy(szTitle, "消费");
                if(rec.ucVoidFlag)
                    strcat(szTitle, "(撤)");
                break;
	  case  TRANS_TYPE_DAIRY_SALE:
		strcpy(szTitle, "日结消费");
		if(rec.ucVoidFlag)
			strcat(szTitle, "(撤)");
		break;			
            case TRANS_TYPE_SALEVOID:
                strcpy(szTitle, "消费撤销");
                ucSign=1;
                break;
            case TRANS_TYPE_PREAUTH:
                strcpy(szTitle, "预授权");
                break;
            case TRANS_TYPE_PREAUTHVOID:
                strcpy(szTitle, "预授权撤销");
                ucSign=1;
                break;
            case TRANS_TYPE_PREAUTH_COMP:
                strcpy(szTitle, "预授权完成");
                if(rec.ucVoidFlag)
                    strcat(szTitle, "(撤)");
                break;
            case TRANS_TYPE_PREAUTH_COMPVOID:
                strcpy(szTitle, "预授权完成撤销");
                ucSign=1;
                break;
            case TRANS_TYPE_REFUND:
                strcpy(szTitle, "退货");
                ucSign=1;
                break;
            default:
                continue;
            }
#ifdef  ST7571

#else
	 vDispCenter(1, szTitle, 1);

        if(page == 1)
        {
                line=2;
                sprintf(buf, "金额:%s%lu.%02lu", ucSign?"-":"", rec.ulAmount/100, rec.ulAmount%100);
                _vDisp(line++, buf);
                sprintf(buf, "凭证号:%06lu", rec.ulTTC);
                _vDisp(line++, buf);

                vUnpackPan(rec.sPan, buf);
                if(rec.uiTransType != TRANS_TYPE_PREAUTH || (rec.uiTransType == TRANS_TYPE_PREAUTH  && gl_SysInfo.ucAuthCardMask == 1))
                    memcpy(buf+6, "*********", strlen(buf)-6-4);		//显示卡号首6位末4位，中间屏蔽(预授权交易不屏蔽)
                if(strlen(buf)<=_uiGetVCols()-5)
                    vDispVarArg(line++, "卡号:%s", buf);
                else
                    vDispVarArg(line++, "卡%s", buf);
    	}

	if(page == 2)
	{
		line=2;
                vDispVarArg(line++, "参考号:%.12s", rec.sReferenceNo);
                vOneTwo0(rec.sDateTime, 6, buf);
                if(_uiGetVCols()>=5+19)
                    vDispVarArg(line++, "时间:20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", buf, buf+2, buf+4, buf+6, buf+8, buf+10);
                else
                    vDispVarArg(line++, "20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", buf, buf+2, buf+4, buf+6, buf+8, buf+10);
                buf[0]=0;
                
                if(TRANS_TYPE_SALEVOID==rec.uiTransType || TRANS_TYPE_PREAUTH_COMPVOID==rec.uiTransType)
                    vDispVarArg(line++, "原凭证号:%06lu", rec.ulVoidTTC);
                else if(TRANS_TYPE_PREAUTH==rec.uiTransType || TRANS_TYPE_PREAUTH_COMP==rec.uiTransType || TRANS_TYPE_PREAUTHVOID==rec.uiTransType)
                    vDispVarArg(line++, "授权码:%.6s", rec.sAuthCode);
                else if(TRANS_TYPE_REFUND==rec.uiTransType)
                {
                    vOneTwo0(rec.sVoidInfo+6, 2, tmp);
                    vDispVarArg(line++, "原交易日期:%.4s", tmp);
                }
	}
        
        if(line<_uiGetVLines())
            vClearLines(line);
		
	if(page == 1)
		vDispCenter(_uiGetVLines(), "[取消]退出 [确认]更多", 0);
	else if(page == 2)
		vDispCenter(_uiGetVLines(), "[取消]退出", 0);

	_vFlushKey();	
	_vSetTimer(&ulTimer, 60*100);
	while(1)
	{
		if(_uiKeyPressed())
		{
		        _vSetTimer(&ulTimer, 60*100);
			uiKey=_uiGetKey();
			if(uiKey==_KEY_ESC)
				return 0;
			if(uiKey==_KEY_DOWN || uiKey==_KEY_UP) 
			{
				if(page == 2)
					page = 1;
				break;
			}
			if(uiKey==_KEY_ENTER)
			{
				page = 2;
				break;
			}
		}
		
		if(_uiTestTimer(ulTimer))
			return 0;
	}  
#endif
        }
	else
		idxcard ++;	
    }

    //扫码交易	
    idxqr = 0;
    page = 1;
	while(idxqr<gl_SysData.uiQrTransNum)
	{
		memset(&QrTrRec, 0, sizeof(QrTrRec));	
		uiRet = uiMemManaGetQrTransRec(idxqr, &QrTrRec);
		if(uiRet)
                    return 1;
		
                if(QrTrRec.ulTTC==ulTTC  && (QrTrRec.uiTransType == QRTRANS_TYPE_VOID ||( QrTrRec.uiTransType == QRTRANS_TYPE_SALE && QrTrRec.ucProType == 0x01)))
        	{
		 	memset( tmp,0x00,sizeof(tmp));		
			switch (QrTrRec.uiTransType)
			{
				case QRTRANS_TYPE_SALE:
					if(QrTrRec.ucPayMode == Z_phoneTopos)
						strcpy(tmp, "主扫支付");
					else if(QrTrRec.ucPayMode == B_posTophone)
						strcpy(tmp, "被扫支付");
					break;
				case QRTRANS_TYPE_VOID:
					strcpy(tmp, "扫码撤销");
					break; 
				default:
					strcpy(tmp, "(未知交易)");
					break;
			}

			_vCls();
			 vDispCenter(1, tmp, 1);
		
			if(page == 1)
			{			
				vDispVarArg(2, "金额:%lu.%02lu", QrTrRec.ulAmount/100, QrTrRec.ulAmount%100);

				if(QRTRANS_TYPE_SALE == QrTrRec.uiTransType) {				
					vDispVarArg(3, "凭证号:%06lu", QrTrRec.ulTTC);  
					memset(tmp,0x00,sizeof(tmp));
					sprintf(tmp,"%.5s*****%.5s",QrTrRec.orderId,&QrTrRec.orderId[strlen(QrTrRec.orderId)-5]);
					vDispVarArg(4, "单号:%.30s", tmp);
				}

		 		 if(QRTRANS_TYPE_VOID ==QrTrRec.uiTransType )
			        {	        
			            vDispVarArg(3,"凭证%06lu", QrTrRec.ulTTC); 
					memset(tmp,0x00,sizeof(tmp));
					sprintf(tmp,"%.5s*****%.5s",QrTrRec.szVoidOrderId,&QrTrRec.szVoidOrderId[strlen(QrTrRec.szVoidOrderId)-5]);
					vDispVarArg(4, "单号:%.30s", tmp);
			        }
			}

			if(page == 2)
			{	
				   line=2;
				   memset( tmp,0x00,sizeof(tmp));
				    strcpy(tmp, "支付类型:");
				    if(QrTrRec.ucPayWay==MODE_WETCHAT)
				    {
				        strcat(tmp, "微信支付");
				    }else if(QrTrRec.ucPayWay==MODE_ALIPAY)
				    {
				        strcat(tmp, "支付宝支付");
				    }else if(QrTrRec.ucPayWay==MODE_UNIONPAY)
				    {
				        strcat(tmp, "银联支付");
				    }

				_vDisp(line++, tmp);	  
				
				vDispVarArg(line++, "参考号:%.12s", QrTrRec.sReferenceNo);
				vOneTwo0(QrTrRec.sDateTime, 6, buf);
				if(_uiGetVCols()>=5+19)
					vDispVarArg(line++, "时间:20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", buf, buf+2, buf+4, buf+6, buf+8, buf+10);
				else
					vDispVarArg(line++, "20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", buf, buf+2, buf+4, buf+6, buf+8, buf+10);
				buf[0]=0;

				if(QRTRANS_TYPE_VOID==QrTrRec.uiTransType )
					vDispVarArg(line++, "原凭证号:%06lu", QrTrRec.ulVoidTTC);			
		        }
	
		if(line<_uiGetVLines())
			vClearLines(line);
		
		if(page == 1)
			vDispCenter(_uiGetVLines(), "[取消]退出 [确认]更多", 0);
                else if(page == 2)
			vDispCenter(_uiGetVLines(), "[取消]退出", 0);
				
		_vFlushKey();
		_vSetTimer(&ulTimer, 60*100);
		while(1)
		{
			if(_uiKeyPressed())
			{
				uiKey =_uiGetKey();
				if(uiKey==_KEY_ESC)
					return 0;	
				if(uiKey==_KEY_DOWN || uiKey==_KEY_UP) 
				{
					if(page == 2)
						page = 1;
					break;
				}
				if(uiKey == _KEY_ENTER)
				{
					page = 2;
					break;
				}
			}
			
			if(_uiTestTimer(ulTimer))
				return 0;
		}
	  }
	  else 
	  	idxqr ++;
    }
	
    vClearLines(2);
    vMessage("无此交易");
    return 1;
}

int iViewTransByCardNo(void)
{
    int iRet;
    char buf[50];
    uchar sPan[20] = {0};
    stTransRec rec;
    int line;
    uchar ucSign;
     uint uiKey,uiRet;
    int page = 0;
    char szTitle[50+1]= {0};
    uchar ucPinPos;
    ulong ulTimer;
    int idx,idxcard;
    char tmp[50];
   uchar flag = 1;                                         // 1 down  0 up
   ushort uiPayNum = 0;				// 交易笔数
   
    _vCls();
    vDispCenter(1, "按卡号查询交易", 1);

    if(gl_SysData.uiTransNum==0)
    {
        vMessage("没有交易");
        return 0;
    }
	
    _vDisp(2, "请输入卡号:");
   ucPinPos = _uiGetVCols();	
    iRet=iInput(INPUT_NORMAL|INPUT_LEFT, 3, ucPinPos, sPan, 19, 16, 19, 60);
    if(iRet<0)
        return -1;  

/*
得到此卡号支付笔数
*/
     idx = 0;
    while(idx < gl_SysData.uiTransNum)
    {
        memset(&rec, 0, sizeof(rec));	
        if(uiMemManaGetTransRec(idx, &rec))
            break;

	vUnpackPan(rec.sPan, buf);	
        if( strcmp( buf, sPan) == 0 )
        {			
		uiPayNum ++;	
    	}

        idx ++;  
    }

    dbg("uiPayNum:%d\n",uiPayNum);
	
    idx = 0;
    idxcard = 0;
    page = 1;
    while(idx < gl_SysData.uiTransNum)
    {
        uiRet = uiMemManaGetTransRec(gl_SysData.uiTransNum-idx-1, &rec);
        if(uiRet)
                    return 1;
		
	vUnpackPan(rec.sPan, buf);	
        if( strcmp( buf, sPan) == 0 )
        {
            ucSign=0;
            switch (rec.uiTransType)
            {
            case TRANS_TYPE_SALE:
                strcpy(szTitle, "消费");
                if(rec.ucVoidFlag)
                    strcat(szTitle, "(撤)");
                break;
	  case  TRANS_TYPE_DAIRY_SALE:
		strcpy(szTitle, "日结消费");
		if(rec.ucVoidFlag)
			strcat(szTitle, "(撤)");
		break;			
            case TRANS_TYPE_SALEVOID:
                strcpy(szTitle, "消费撤销");
                ucSign=1;
                break;
            case TRANS_TYPE_PREAUTH:
                strcpy(szTitle, "预授权");
                break;
            case TRANS_TYPE_PREAUTHVOID:
                strcpy(szTitle, "预授权撤销");
                ucSign=1;
                break;
            case TRANS_TYPE_PREAUTH_COMP:
                strcpy(szTitle, "预授权完成");
                if(rec.ucVoidFlag)
                    strcat(szTitle, "(撤)");
                break;
            case TRANS_TYPE_PREAUTH_COMPVOID:
                strcpy(szTitle, "预授权完成撤销");
                ucSign=1;
                break;
            case TRANS_TYPE_REFUND:
                strcpy(szTitle, "退货");
                ucSign=1;
                break;
            default:
                continue;
            }
#ifdef  ST7571

#else
    //     if(uiPayNum > 1)
	 	vDispCenterEx(1, szTitle, 1, 0, idxcard, uiPayNum  -1);
   //      else
//		 vDispCenter(1, szTitle, 1);
		 
        if(page == 1)
        {
                line=2;
                sprintf(buf, "金额:%s%lu.%02lu", ucSign?"-":"", rec.ulAmount/100, rec.ulAmount%100);
                _vDisp(line++, buf);
                sprintf(buf, "凭证号:%06lu", rec.ulTTC);
                _vDisp(line++, buf);

                vUnpackPan(rec.sPan, buf);
                if(rec.uiTransType != TRANS_TYPE_PREAUTH || (rec.uiTransType == TRANS_TYPE_PREAUTH  && gl_SysInfo.ucAuthCardMask == 1))
                    memcpy(buf+6, "*********", strlen(buf)-6-4);		//显示卡号首6位末4位，中间屏蔽(预授权交易不屏蔽)
                if(strlen(buf)<=_uiGetVCols()-5)
                    vDispVarArg(line++, "卡号:%s", buf);
                else
                    vDispVarArg(line++, "卡%s", buf);
    	}

	if(page == 2)
	{
		line=2;
                vDispVarArg(line++, "参考号:%.12s", rec.sReferenceNo);
                vOneTwo0(rec.sDateTime, 6, buf);
                if(_uiGetVCols()>=5+19)
                    vDispVarArg(line++, "时间:20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", buf, buf+2, buf+4, buf+6, buf+8, buf+10);
                else
                    vDispVarArg(line++, "20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", buf, buf+2, buf+4, buf+6, buf+8, buf+10);
                buf[0]=0;
                
                if(TRANS_TYPE_SALEVOID==rec.uiTransType || TRANS_TYPE_PREAUTH_COMPVOID==rec.uiTransType)
                    vDispVarArg(line++, "原凭证号:%06lu", rec.ulVoidTTC);
                else if(TRANS_TYPE_PREAUTH==rec.uiTransType || TRANS_TYPE_PREAUTH_COMP==rec.uiTransType || TRANS_TYPE_PREAUTHVOID==rec.uiTransType)
                    vDispVarArg(line++, "授权码:%.6s", rec.sAuthCode);
                else if(TRANS_TYPE_REFUND==rec.uiTransType)
                {
                    vOneTwo0(rec.sVoidInfo+6, 2, tmp);
                    vDispVarArg(line++, "原交易日期:%.4s", tmp);
                }
	}
        
        if(line<_uiGetVLines())
            vClearLines(line);

#ifdef LCD_GC9106
	if(page == 1)
		vDispCenter(_uiGetVLines(), "[取消]退出[确认]更多", 0);
	else if(page == 2)
		vDispCenter(_uiGetVLines(), "[取消]退出[↑↓]翻页", 0);
#else
	if(page == 1)
		vDispCenter(_uiGetVLines(), "[取消]退出 [确认]更多", 0);
	else if(page == 2)
		vDispCenter(_uiGetVLines(), "[取消]退出 [↑↓]翻页", 0);
#endif

	_vFlushKey();	
	_vSetTimer(&ulTimer, 60*100);
	while(1)
	{
		if(_uiKeyPressed())
		{
		       _vSetTimer(&ulTimer, 60*100);
			uiKey=_uiGetKey();
			if(uiKey==_KEY_ESC)
				return 0;
			if(uiKey==_KEY_DOWN)
			{		
			        if( page == 1)
			       {
				       if(idxcard != uiPayNum -1)
				      {
						idx++;
						flag = 1;
						idxcard ++;
						dbg("idxcard1:%d\n",idx);
			        	}
					 else
					 	continue;
			        }
				if(page == 2)
					page = 1;
				break;
			}
			if(uiKey==_KEY_UP)
			{
				if(page == 1)
				{
				 	if(idxcard > 0 )   
					{
						idx --;
			        		flag = 0;
						idxcard--;
						dbg("idxcard2:%d\n",idx);
				 	}
					else 
						continue;
				}
				if(page == 2)
					 	page = 1;
				break;
			}
			if(uiKey==_KEY_ENTER)
			{
				page = 2;
				break;
			}
		}
		
		if(_uiTestTimer(ulTimer))
			return 0;
	}  
#endif
        }
	else
	{     
	        if(flag == 1)
			idx ++;
		else if(flag == 0)
			idx --;
		continue;
	}	
    }

    vClearLines(2);
    vMessage("没有交易");
    return 1;
}

uchar all_para_down_check(void)
{
      uchar proc_req = 0;
       int CommRetryCount = 0;
       int ret;

       dbg("gl_SysInfo.proc_req:%d\n",gl_SysInfo.proc_req);  
      dbg("gl_SysInfo.ucClssParamLoad:%d\n",gl_SysInfo.ucClssParamLoad);
	gl_ucProcReqFlag = 0;	  
	while(1)   
		{		
		       if(proc_req == 1)  break;	
			   
			if(gl_SysInfo.proc_req == '1')
			{
				;
			} else if(gl_SysInfo.proc_req == '2')
			{
				;
			} else if(gl_SysInfo.proc_req == '3')
			{
				while(CommRetryCount < gl_SysInfo.ucCommRetryCount+1)
				{
					ret = iPosLogin(1);
					if(ret != 0) 
					{
						CommRetryCount ++;
						gl_SysData.ucPosLoginFlag = 0;
						uiMemManaPutSysData();
						continue;	
					}
					else
						break;	
				}
				
			} else if(gl_SysInfo.proc_req == '4' || gl_SysInfo.uiCaKeyNum == 0)
			{
				 iDownloadCAPK();
			} else if(gl_SysInfo.proc_req == '5' || gl_SysInfo.uiAidNum == 0)
			{
				iDownloadEmvParam();
			} else if(gl_SysInfo.proc_req == '6')
			{
				;
			} else if(gl_SysInfo.proc_req == '7')
			{
				;
			} else if(gl_SysInfo.proc_req == '8')
			{
				;
			} else if(gl_SysInfo.proc_req == '9' ||gl_SysInfo.ucClssParamLoad==0)
			{
			//	iDownLoadBackBIN();
			} else if(gl_SysInfo.proc_req == 'b')
			{
				;
			} else if(gl_SysInfo.proc_req == 'c')
			{
				;
			} 

	                if(gl_SysInfo.uiCaKeyNum> 0 && gl_SysInfo.uiAidNum > 0)
	               	{
	               		proc_req = 1;
	                }				
		}
	iSetConnStatus(0, NULL);    //关长链接	
	return 0;
}


void GetBankName(char *cmpbuf,char * text)
{
	if(memcmp("0102",cmpbuf,4)==0)
		memcpy(text,"工商银行",8);
	else if(memcmp("0103",cmpbuf,4)==0)
		memcpy(text,"农业银行",8);
	else if(memcmp("0104",cmpbuf,4)==0)
		memcpy(text,"中国银行",8);
	else if(memcmp("0105",cmpbuf,4)==0)
		memcpy(text,"建设银行",8);
	else if(memcmp("0100",cmpbuf,4)==0)
		memcpy(text,"邮储银行",8);
	else if(memcmp("0301",cmpbuf,4)==0)
		memcpy(text,"交通银行",8);
	else if(memcmp("0302",cmpbuf,4)==0)
		memcpy(text,"中信银行",8);
	else if(memcmp("0303",cmpbuf,4)==0)
		memcpy(text,"光大银行",8);
	else if(memcmp("0304",cmpbuf,4)==0)
		memcpy(text,"华夏银行",8);
	else if(memcmp("0305",cmpbuf,4)==0)
		memcpy(text,"民生银行",8);
	else if(memcmp("0306",cmpbuf,4)==0)
		memcpy(text,"广发银行",8);
	else if(memcmp("0307",cmpbuf,4)==0)
		memcpy(text,"平安银行",8);
	else if(memcmp("0410",cmpbuf,4)==0)
		memcpy(text,"平安银行",8);
	else if(memcmp("0308",cmpbuf,4)==0)
		memcpy(text,"招商银行",8);
	else if(memcmp("0309",cmpbuf,4)==0)
		memcpy(text,"兴业银行",8);
	else if(memcmp("0310",cmpbuf,4)==0)
		memcpy(text,"浦发银行",8);
	else if(memcmp("0403",cmpbuf,4)==0)
		memcpy(text,"北京银行",8);
	else if(memcmp("0401",cmpbuf,4)==0)
		memcpy(text,"上海银行",8);
	else memcpy(text,cmpbuf,8);

	text[10]=0;
	return;
}

uchar *bank_list[]={
	"01020000","工商银行",
	"01030000","农业银行",
	"01040000","中国银行",
	"01050000","建设银行",
	"01000000","邮储银行",
	"03010000","交通银行",
	"03020000","中信银行",
	"03030000","光大银行",
	"03040000","华夏银行",
	"03050000","民生银行",
	"03060000","广发银行",
	"03070000","平安银行",
	"04100000","平安银行",
	"03080000","招商银行",
	"03090000","兴业银行",
	"03100000","浦发银行",
	"04030000","北京银行",
	"04010000","上海银行",
	"ZZZZZZZZ","                        "
};

void  get_bankno(uchar *str,uchar *OutData)
{
	byte i = 0;

	while(1){
		if(memcmp(bank_list[i],"ZZZZZZZZ",8) == 0) break;
		if(memcmp(str,bank_list[i],8) == 0) break;
		i+=2;
	}

	strcpy(OutData,bank_list[i+1]);
	return;
}

