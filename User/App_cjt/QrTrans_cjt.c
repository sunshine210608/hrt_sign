#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "VposFace.h"
#include "pub.h"
#include "Pack8583.h"
#include "cJSON.h"
#include "debug.h"
#include "tag.h"
#include "myHttp.h"
#include "myBase64.h"
#include "utf2gb.h"

#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "St8583_cjt.h"
#include "tcpcomm.h"
#include "func.h"
#include "QrTrans_cjt.h"


extern int iHttpSendRecv8583(uchar ucRevFlag);
extern int iCheckTranStat(uint uiTransType, uchar ucMode);
extern void flushStatusBar(int iFlushNow);
extern int iPrintQrTrans(stQrTransRec *QrRec, int dup);
extern void vShowWaitEx(char *pszMessage, long lWaitTime, int keyFlag);

extern void vSetCommNoDisp(uchar disp);
extern int iTcpSendRecv(char *pszSvrIp, char *pszSvrPort, char *pszBakSvrIp, char *pszBakSvrPort,
        uchar *psSnd, uint uiSndLen, uchar *psRsp, uint uiRspSize, uint *puiRcvLen, uint iTimeOutMs,uchar ucRevFlag);
extern int iSetConnStatus(char cLinkFlag, char *pszUrl);
extern void vGenFiled63CellInfo(uchar *pszFd63);
extern uint Select_pay_way(void);
extern void TermRemindInfo(void);
extern void TermRemindUseInfo(void);
extern void vShowHostErrMsg(uchar *pszHostErrCode, uchar *pszHostErrMsg);
extern int DispTransQrCode(char *pszQrCodeStr);
extern uint _uiCheckTimer(ulong ulTimer);
extern void GetPublicField059(char *pszEncPin, uchar *pszRandom, uchar asOut[]);
extern void GetPublicField058(uchar asOut[]);
extern void DivPublicField061(u8 *pInData,int iLen, stQrTransRec *QrTrRec);
extern void vSetCommCallBack(void (*func)(void));
extern void vSetQrPreView(int flag);
extern int iGetStringGBK(unsigned char *pszStr, int iStrLen);
extern int iGetQrPreView(void);
extern void vDispWaitQrImg(int iInitFlag);
extern void vMessageMulEx(char *pszMessage);

//static void vAppPackPinBlock(uchar *pszCardId, uchar *pszPin, uchar *psKey, int iKeyLen, uchar *psOut);
int iPackReq8583(st8583 *pSend8583, char *sCommBuf, uint *puiLen);
//static int iMagTrans(uint uiTransType, int iEntry, int iVoidRecIdx, ulong ulVoidTTC);
int iUnPackRsp8583(char *sCommBuf, uint uiRecvLen, st8583 *pRecv8583);

void vPrt8583Fields(char ucType, st8583 *pMsg8583);
void vPrtTransRecInfo(void);

uchar Keyflag;

//static int sg_iRevTry=0;
//static int sg_iVoidRecIndex;
u8 whitchAuthCode(u8 * authCode)
{
	u8 isweb = 0;
	
	if(strlen(authCode) == 18 && 
		(memcmp(authCode,"10",2) == 0||memcmp(authCode,"11",2) == 0||
	   memcmp(authCode,"12",2) == 0||memcmp(authCode,"13",2) == 0||
	   memcmp(authCode,"14",2) == 0||memcmp(authCode,"15",2) == 0))
	{
		isweb = MODE_WETCHAT;
	}
	else if((strlen(authCode) >= 16 && strlen(authCode) <= 24) &&
		(memcmp(authCode,"25",2) == 0 || memcmp(authCode,"26",2) == 0 ||
		memcmp(authCode,"27",2) == 0 || memcmp(authCode,"28",2) == 0 ||
		memcmp(authCode,"29",2) == 0 || memcmp(authCode,"30",2) == 0))
	{
		isweb = MODE_ALIPAY;
	}
	else if(strlen(authCode) == 19 && 
		(memcmp(authCode,"62",2) == 0))
	{
		isweb = MODE_UNIONPAY;
	}
	
	return isweb;

}

//减小存储空间
uchar ucGetQrPayMode(char *pszPayMode)
{
    if(pszPayMode==NULL || pszPayMode[0]==0)
        return 0;   //无效
    if( !strcmp("WX", pszPayMode) )
    {
        return 1;
    }
    if( !strcmp("ZFB", pszPayMode) )
    {
        return 2;
    }
    if( !strcmp("CUPQRC", pszPayMode) )
    {
        return 3;
    }
    return 0;
}


int iAddTLV2Str(char *psOut, char *pszTag, uint len, char *psVal, uint *outlen)
{
    char *psbak=psOut;

    strcpy(psOut, pszTag);
    psOut+=strlen(pszTag);
    if(len==0)
        len=strlen(psVal);
    if(len<128)
    {
        *psOut=len;
        psOut++;
    }else{
        if(len<256)
        {
            *psOut++=0x81;
            *psOut++=len;
        }else{
            *psOut++=0x82;
            *psOut++=(len/256);
            *psOut++=(len%256);
        }
    }
    memcpy(psOut, psVal, len);
    if(outlen)
        *outlen=psOut-psbak+len;
    return 0;
}

//获取51域tlv, v为字符串格式且v中不会包含tag字符串
int iGetTLVStr(char *pszStr, char *pszTag, char *pszVal, uint *outlen)
{
    uchar *p;
    int len;
    p=(uchar*)strstr(pszStr, pszTag);
    if(p)
    {
        p+=strlen(pszTag);
        if(p[0]>=0x80)
        {
            len=ulStrToLong(p+1, p[0]&0x0F);
            p+=(1+p[0]&0x0F);
        }else
        {
            len=p[0];
            p++;
        }
        if(p+len>(uchar*)pszStr+strlen(pszStr)) //长度超过原始字符串长度
            return 1;
        vMemcpy0(pszVal, p, len);
        if(outlen)
            *outlen=len;
        return 0;
    }
    return 1;
}
extern int iDispTransQrCode(uchar *pszPrompt, char *pszQrCodeStr);

int iGetQrCode(char *pszPrompt, char *pszQrCode, int size)
{ 
    int ret;
    ulong ulTimeOut;
    
    if(pszPrompt)
    {
        vDispCenter(3, "开始扫码...", 0);
        vDispCenter(_uiGetVLines()-1, pszPrompt, 0);

    #if 0
        vDispCenter(_uiGetVLines(), "[确认]扫码,[取消]退出)", 0);
        if(!bOK())
            return 1;
    #else
        vDispCenter(_uiGetVLines(), "(按取消键退出)", 0);
    #endif
        
    }
    pszQrCode[0]=0;
    ret=_uiQrStart();
    if(ret)
    {
        //_vQrEnd();
        vMessage("初始化扫码模块失败");
        return 1;
    }
    _vFlushKey();

 #ifndef SUPPORT_8W_CAMERA    
    _vSetTimer(&ulTimeOut, 90*100);     //设置90秒超时
    while(_uiTestTimer(ulTimeOut)==0)
    {
        if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
        {
            ret=-2;
            break;
        }
        ret=_uiGetQrCode(pszQrCode, size);
        if(ret)
            break;
        _vDelay(1);
    }
    #else
	ret=_iGetQrCodeWithTime(pszQrCode, size, 1, 90*1000, NULL);
    #endif	
    _vQrEnd();
    
    _vCls();
#ifndef USE_LTE_4G
    flushStatusBar(1);
#endif    
    if(ret>0 && pszQrCode[0])
        return 0;
    else  
        return 1;
}

//获取扫码数据
int scanCode(u8 *OutBuff,int size)
{
    int iRet;
    ulong ulTimeOut;
    int first,iPreView;

    vClearLines(2);
	
   if(iPosHardwareModule(CHK_HAVE_CAM, NULL, 0)<=0)
    {
    	vMessage( "不支持扫码");
        return -1;
    }
   
    vSetQrPreView(gl_SysInfo.ucQrPreViewFlag);
	
    iRet=_uiQrStart();
    if(iRet)
    {           
        vMessage("初始化扫码模块失败");
        return -1;
    }
    _vFlushKey();

    #ifndef SUPPORT_8W_CAMERA			   
    _vSetTimer(&ulTimeOut, 90*100);     //设置90秒超时
    first=1;
    iPreView=iGetQrPreView();
    while(_uiTestTimer(ulTimeOut)==0)
    {
        if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
        {
            break;
        }
        if(!iPreView)
        {
            vDispWaitQrImg(first);
            first=0;
        }
        iRet=_uiGetQrCode(OutBuff, size);
        if(iRet)
            break;
        _vDelay(1);
    }
    #else
        _iGetQrCodeWithTime(OutBuff, size, 1, 90*1000, NULL);
    #endif	
    _vQrEnd();

    _vCls();
    #ifndef USE_LTE_4G
    flushStatusBar(1); 
    #endif
    if(OutBuff[0]==0)      //用户取消或超时
    {
        vMessage("扫码失败");
        return -1;
    }

   return iRet;
}

void vDispWarnInfoCallBack(void)
{
	uint uiKey;

	if(!Keyflag &&  _uiKeyPressed())
	{
		uiKey =_uiGetKey();
		if(uiKey ==_KEY_CANCEL )
		{
			   _vCls();
		           vDispMid(3, "终止自动查询请稍候...");
			   Keyflag=1;
		}
	}
   
    return;
}

void vGetPrtQrMerchInfo(uchar *psFields, stQrTransRec *rec)
{
    int len;
    uchar *p;
    uchar *CurPos;
	
    len=strlen(psFields);
    if(len==0)
        return;
    CurPos=psFields; 
    
     //商户号
     p = (uchar *)strchr(CurPos,'|');
     memcpy(rec->sPrtMerchId, CurPos, p-CurPos);
    CurPos = p+1;
	 
     //终端号
      p = (uchar *)strchr(CurPos,'|');
     memcpy(rec->sPrtPosId, CurPos,p-CurPos);
     CurPos = p+1;
 
      //商户名	  
      rtrim(CurPos);
      strcpy(rec->szPrtMerchName,CurPos);  		
#ifdef REMOTE_GBK
    {
        char gbkstr[100];
        
        strcpy(gbkstr, (char*)rec->szPrtMerchName);        
        iGetStringGBK((uchar*)gbkstr, -1);
    }
#endif 
	  
    return;
}

unsigned char iQRCodePay(u8 nMode)
{
        uchar szQrCode[500] = {0};
        int iRet;
	char szTitle[20+1];
        ulong ulAmount,QrAmount,GprsAmount,ScanAmount;
	uchar szBuf[100];
	uchar PayWay;	
	ulong ulTTC;	
	ulong ulTimer,ulTimer1;
	uint uiKey;
	u16 len =0;
	uint time1 = 0;
	uchar szDisp[100];
	u8 timce=0;
	stQrTransRec QrTrRec;
	
	if(nMode == Z_phoneTopos)
		strcpy(szTitle,"二维码支付");
	else if(nMode == B_posTophone)
		strcpy(szTitle,"扫码支付");

	_vCls();	
	vDispCenter(1, szTitle, 1);
	
	if(iCheckTranStat( QRTRANS_TYPE_SALE,0))
        	return 1;

	_vCls();	
	vDispCenter(1, szTitle, 1);
	
	if(gl_SysInfo.ucQrSaleSwitch == 0) 
	{
		TermRemindInfo();
		return 1;
        }

	gl_ucQrTransFlag = 1;

/*
	if(gl_SysInfo.iCommType == VPOSCOMM_WIFI)
    {
    	vMessage("基站信息获取中请稍候");
    	iRet = iGetLocalPosition();
	if(iRet != 0)	
		return 1;
	vClearLines(2);
    }
*/
	
	if(iGetAmount(NULL, "请输入金额",NULL, szBuf)<=0)
		return 1;	
	ulAmount=atol(szBuf);

        if(nMode == Z_phoneTopos)
        {
		QrAmount = atol(gl_SysInfo.ucQrAmount);
		GprsAmount = atol(gl_SysInfo.ucGprsAmount);	
	        
	    if(gl_SysInfo.ucGprsUseFlag == 1 &&  gl_SysInfo.ucGprsPayFlag == 0 && ulAmount < (QrAmount + GprsAmount))
	   {
		sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", (QrAmount + GprsAmount)/100, (QrAmount + GprsAmount)%100);
		vMessageMul(szDisp);
		return 1;
	  
	    }else if(ulAmount < QrAmount)
	    {
		sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", QrAmount/100, QrAmount%100);
		vMessageMul(szDisp);
		return 1;
	    }	    	
        }else if(nMode == B_posTophone)		
	{
		ScanAmount = atol(gl_SysInfo.ucScanAmount);
		GprsAmount = atol(gl_SysInfo.ucGprsAmount);	
	  
	    if(gl_SysInfo.ucGprsUseFlag == 1 &&  gl_SysInfo.ucGprsPayFlag == 0  && ulAmount < (QrAmount + GprsAmount))
	   {
		sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", (ScanAmount + GprsAmount)/100, (ScanAmount + GprsAmount)%100);
		vMessageMul(szDisp);
		return 1;
	
	    }else if(ulAmount < ScanAmount)
	    {
		sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", ScanAmount/100, ScanAmount%100);
		vMessageMul(szDisp);
		return 1;
	    }
	}
		
	if(nMode == B_posTophone)
	{		
		sprintf(szBuf, "金额:%lu.%02lu", ulAmount/100, ulAmount%100);
		vDispCenter(2, szBuf, 0);
		vDispCenter(3, "支持微信/支付宝/银联扫码", 0);
                vDispCenter(_uiGetVLines(), "按[确认]键扫码", 0);

		_vSetTimer(&ulTimer, 6000L); //60秒		
		_vFlushKey();
		while(1)
		{
			if(_uiKeyPressed())
			{
				uiKey=_uiGetKey();
				if(uiKey==_KEY_ENTER)
					break;
				if(uiKey==_KEY_CANCEL)
					return 1;
			}	
			if(_uiTestTimer(ulTimer))
				return 1;
		}
		
		iRet = scanCode(szQrCode,sizeof(szQrCode));		
		if(iRet == -1)
               		 return 1;		
	
		dbg("qrcode=[%s]\n", szQrCode);

		PayWay= whitchAuthCode(szQrCode);
		if(PayWay != MODE_WETCHAT && PayWay != MODE_ALIPAY && PayWay != MODE_UNIONPAY)
		{
			TermRemindInfo();
			return 1;
		}		
	}

start:
	
	vDispCenter(1, szTitle, 1);
	
         memset(&gl_Send8583, 0, sizeof(gl_Send8583));
         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
		 
        strcpy((char *)gl_Send8583.Msg01, "0200");
	strcpy((char *)gl_Send8583.Field02,"0002220000000000");	
	strcpy((char *)gl_Send8583.Field03, "000000");
	sprintf((char *)gl_Send8583.Field04, "%012lu", ulAmount);
	
	vIncTTC();
	ulTTC=gl_SysData.ulTTC;
	sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);
	strcpy((char *)gl_Send8583.Field25, "69");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

        //DE59
        if(nMode == Z_phoneTopos)
		GetPublicField059(NULL,NULL, gl_Send8583.Field59);
	else if(nMode == B_posTophone)
		GetPublicField059(NULL,szQrCode+strlen(szQrCode)-6, gl_Send8583.Field59);
	
        //DE61
        if(nMode == Z_phoneTopos)
        {
         	memcpy(gl_Send8583.Field61,"\xDF\x26\x03",3);
		strcpy(gl_Send8583.Field61+3,"ZFB");	
        }
	else if(nMode == B_posTophone)
	{
	        memcpy(gl_Send8583.Field61,"\xDF\x26\x03",3);
		strcpy(gl_Send8583.Field61+3,"ZFB");		
		memcpy(gl_Send8583.Field61+6,"\xDF\x35",2);
		vLongToStr(strlen(szQrCode), 1, gl_Send8583.Field61+8);
		sprintf((char *)gl_Send8583.Field61+9, "%s", szQrCode);			
	}

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

   	 //打8583包-填充http-发送-接收-解http-解8583包
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
	        return 1;
   	 }

	if (strcmp((char *)gl_Recv8583.Field39, "00") && strcmp((char *)gl_Recv8583.Field39, "01"))
	{
		 vShowHostErrMsg(gl_Recv8583.Field39,gl_Recv8583.Field56+2);
		return 1;
	} 
		
	if(nMode == B_posTophone)
	{      
		memset(&QrTrRec, 0, sizeof(QrTrRec));			
		QrTrRec.uiTransType=QRTRANS_TYPE_SALE;

		QrTrRec.ucPayMode= B_posTophone;
		QrTrRec.ucProType = 0x02; //待确认
		QrTrRec.ulAmount=ulAmount;
		QrTrRec.ulTTC=ulTTC;
		dbg("QrTrRec.ulTTC12:%d\r\n",QrTrRec.ulTTC);
		strcpy(QrTrRec.szchnlQrCode, szQrCode);
		vTwoOne(gl_SysData.szCurOper, 2, QrTrRec.sOperNo);

		_vGetTime(szBuf);
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 12, QrTrRec.sDateTime);
//		QrTrRec.ucPayWay = PayWay;		
		if(gl_Recv8583.Field37[0])
			strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);	
		
		len = strlen(gl_Recv8583.Field61);
                dbg("len:%d\n",len);
				
	         DivPublicField061(gl_Recv8583.Field61,len,&QrTrRec);	
		dbg("orderId:%s\n",QrTrRec.orderId);
			 
#if 0			 
		 uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrRec);
		 
		 gl_SysData.uiQrTransNum ++;	
		 uiMemManaPutSysData();		
#endif

	 	_vCls();
		vDispCenter(1, szTitle, 1);			

		vDispCenter(2, "查询中,请稍候...", 0);
	//	vDispCenter(4, "[取消]退出",0);	

               	 memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

		strcpy((char *)gl_Send8583.Msg01, "0200");
		strcpy((char *)gl_Send8583.Field02,"0002220000000000");	
		strcpy((char *)gl_Send8583.Field03, "310000");
		sprintf((char *)gl_Send8583.Field04, "%012lu", QrTrRec.ulAmount);
	
//	vIncTTC();
//	ulTTC=gl_SysData.ulTTC;
//	sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);

		strcpy((char *)gl_Send8583.Field25, "69");

	        if(QrTrRec.sReferenceNo[0])
	        	sprintf((char *)gl_Send8583.Field37, "%.12s", QrTrRec.sReferenceNo);
		strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
		strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	        //DE59
		GetPublicField059(NULL,NULL, gl_Send8583.Field59);
	
	        //DE61
	        if(nMode == Z_phoneTopos)
	        {
	         	memcpy(gl_Send8583.Field61,"\xDF\x27",2);
			len = strlen(QrTrRec.orderId);
			gl_Send8583.Field61[2] = len;
			strcpy(gl_Send8583.Field61+3,QrTrRec.orderId);	
	        }
		else if(nMode == B_posTophone)
		{		
			memcpy(gl_Send8583.Field61,"\xDF\x27",2);
			len = strlen(QrTrRec.orderId);
			gl_Send8583.Field61[2] = len;
			strcpy(gl_Send8583.Field61+3,QrTrRec.orderId);	
		}

		 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);
	 
	          _vSetTimer(&ulTimer1, 1000L); //10秒	 
	         _vFlushKey();
		while (timce < 3)
		{	       
			time1 = _uiCheckTimer(ulTimer1); 	
			if(!time1 ) {
				vIncTTC();
				ulTTC=gl_SysData.ulTTC;
				sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);	
			iRet = iHttpSendRecv8583(0);
			if (iRet)
			{
				return 1;
			}

			if(memcmp(gl_Recv8583.Field39,"01",2)==0x00)
			{						
				 timce ++;
				continue;
			}	

			if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
			{      
			         _vCls();
				vDispCenter(1, szTitle, 1);
			       //成功
#ifdef ENABLE_PRINTER 
				{		
				       QrTrRec.ucProType = 0x01;
					#if 0   
					uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);			
					
					gl_SysData.ulLastTransTTC = QrTrRec.ulTTC;
					uiMemManaPutSysData();
					#endif
					
					vShowWaitEx("支付成功,打印票据...", -1, 0); 
					iPrintQrTrans(&QrTrRec, 0);
					vShowWaitEx(NULL, 0, 0x03); 
				}
#else    
				{
				        QrTrRec.ucProType = 0x01;
					#if 0	
					uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);	
					#endif
					vMessage("支付成功");
				}
#endif 
		             return 0;
			}	
			else
			{		
			//失败
			 	vShowHostErrMsg(gl_Recv8583.Field39,gl_Recv8583.Field56+2);
				
			        return 1;
			}			
	     	}

	        	if(_uiKeyPressed())
			{
				uiKey =_uiGetKey();
				if(uiKey ==_KEY_CANCEL)
				{
					return 1;
				}
			}		
		}

	        vShowHostErrMsg(gl_Recv8583.Field39,gl_Recv8583.Field56+2);
	        dbg("QrTrRec.ucPayWay:%d\n",QrTrRec.ucPayWay);	
	}
	else if(nMode == Z_phoneTopos) 
	{			
        	memset(&QrTrRec, 0, sizeof(QrTrRec));
		QrTrRec.uiTransType=QRTRANS_TYPE_SALE;
                QrTrRec.ucPayMode= Z_phoneTopos;
		QrTrRec.ucProType = 0x02; //待确认
		QrTrRec.ulAmount=ulAmount;
		QrTrRec.ulTTC=ulTTC;				
		vTwoOne(gl_SysData.szCurOper, 2, QrTrRec.sOperNo);

		_vGetTime(szBuf);
		
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 14, QrTrRec.sDateTime);
	//	 dbg("PayWay:%d\n",PayWay);
	//	QrTrRec.ucPayWay = PayWay;
	//	 dbg("QrTrRec.ucPayWay0:%d\n",QrTrRec.ucPayWay);				
		if(gl_Recv8583.Field37[0])
			strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);				

		len = strlen(gl_Recv8583.Field61);
                dbg("len:%d\n",len);
				
	         DivPublicField061(gl_Recv8583.Field61,len,&QrTrRec);	
		dbg("orderId:%s\n",QrTrRec.orderId);
		
#if 0		
	        uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrRec);
			
		gl_SysData.uiQrTransNum ++;
	        uiMemManaPutSysData();		
#endif

	       _vCls();
		vDispCenter(1, szTitle, 1);
	//	_vSetBackLight(1);				
           		DispTransQrCode(QrTrRec.szchnlUrl);

		sprintf(szDisp, "%lu.%02lu元", QrTrRec.ulAmount/100, QrTrRec.ulAmount%100);
#ifdef LCD_GC9106
		_vDispAt(2, 20-strlen(szDisp), szDisp);
		_vDispAt(3, 20-strlen("请扫码二"), "请扫码二");
		_vDispAt(4, 20-strlen("维码支付"), "维码支付");	
#else
		_vDispAt(2, 21-strlen(szDisp), szDisp);
		_vDispAt(3, 21-strlen("请扫码二"), "请扫码二");
		_vDispAt(4, 21-strlen("维码支付"), "维码支付");	
#endif		
	//	_vDispAt(3, 11, "[确认]查询");		
	//	_vDispAt(4, 11, "[取消]退出");		 
	        timce ++;
	
	          _vSetTimer(&ulTimer1, 1000L); //10秒	 
	         _vFlushKey();
		while (1)
		{	       
			time1 = _uiCheckTimer(ulTimer1); 	
			if(!time1 ) {					
				 if(timce <3)
				{
					_vCls();
					goto start;	
				 }else
				 	break;
		     	}

	        	if(_uiKeyPressed())
			{
				uiKey =_uiGetKey();
				if(uiKey ==_KEY_CANCEL)						    
				{
					return 1;
				}
			}		
		}
		
	        dbg("QrTrRec.ucPayWay:%d\n",QrTrRec.ucPayWay);
	}
	
	return 0;			
}

unsigned char iHbQRCodePay(u8 nMode,ulong Amount,   int fqFlag,int fqNum)
{
        uchar szQrCode[500] = {0};
        int iRet;
	char szTitle[20+1];
	uchar szBuf[100];
	uchar PayWay;	
	ulong ulTTC;	
	ulong ulTimer,ulTimer1;
	uint uiKey;
	u16 len =0;
	uint time1 = 0;
	u8 timce=0;
	stQrTransRec QrTrRec;
		
	 if(fqFlag == 0)
		strcpy(szTitle,"花呗支付");
	else if(fqFlag == 1)
		strcpy(szTitle,"花呗分期支付");

	_vCls();	
	vDispCenter(1, szTitle, 1);	

	if(gl_SysInfo.ucQrSaleSwitch == 0) 
	{
		TermRemindInfo();
		return 1;
        }

	gl_ucQrTransFlag = 1;
	
	if(nMode == B_posTophone)
	{		
	        if(fqFlag == 0)
	        {
			sprintf(szBuf, "金额:%lu.%02lu", Amount/100, Amount%100);
			vDispCenter(2, szBuf, 0);
	        }
		else if(fqFlag == 1)
	       {
			sprintf(szBuf, "金额:%lu.%02lu", Amount/100, Amount%100);
			vDispCenter(2, szBuf, 0);
		
			sprintf(szBuf, "分期期数:%d期", fqNum);
			vDispCenter(3, szBuf, 0);
		}
		vDispCenter(4, "请扫描支付宝付款码", 0);
                vDispCenter(_uiGetVLines(), "按[确认]键扫码", 0);

		_vSetTimer(&ulTimer, 6000L); //60秒		
		_vFlushKey();
		while(1)
		{
			if(_uiKeyPressed())
			{
				uiKey=_uiGetKey();
				if(uiKey==_KEY_ENTER)
					break;
				if(uiKey==_KEY_CANCEL)
					return 1;
			}	
			if(_uiTestTimer(ulTimer))
				return 1;
		}
		
		iRet = scanCode(szQrCode,sizeof(szQrCode));		
		if(iRet == -1)
               		 return 1;		
	
		dbg("qrcode=[%s]\n", szQrCode);

		PayWay = whitchAuthCode(szQrCode);
		 if(PayWay  != MODE_ALIPAY )
		{
			TermRemindInfo();
			return 1;
		}			 
	}

start:	
	
	 vDispCenter(1, szTitle, 1);
	 
         memset(&gl_Send8583, 0, sizeof(gl_Send8583));
         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
		 
        strcpy((char *)gl_Send8583.Msg01, "0200");
	strcpy((char *)gl_Send8583.Field02,"0002220000000000");	
	strcpy((char *)gl_Send8583.Field03, "000000");
	sprintf((char *)gl_Send8583.Field04, "%012lu", Amount);
	
	vIncTTC();
	ulTTC=gl_SysData.ulTTC;
	sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);
	strcpy((char *)gl_Send8583.Field25, "69");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	 //DE59
	if(nMode == Z_phoneTopos)
		GetPublicField059(NULL,NULL, gl_Send8583.Field59);
	else if(nMode == B_posTophone)
		GetPublicField059(NULL,szQrCode+strlen(szQrCode)-6, gl_Send8583.Field59);
			
        //DE61
        if(nMode == Z_phoneTopos)
        {
                if(fqFlag == 0)
         	{
         		memcpy(gl_Send8583.Field61,"\xDF\x26\x04",3);
			strcpy(gl_Send8583.Field61+3,"ZFB1");	
                }else 
                {
                	if(gl_SysInfo.ucHbfqFlag)
                	{
                		if(fqNum >0 && fqNum <10)
                		{
                			memcpy(gl_Send8583.Field61,"\xDF\x26\x04",3);
					sprintf(gl_Send8583.Field61+3,"ZFB%d",fqNum);	
                		}
				else if(fqNum <= 99)
				{
					memcpy(gl_Send8583.Field61,"\xDF\x26\x05",3);
					sprintf(gl_Send8583.Field61+3,"ZFB%02d",fqNum);	
				}
                	}
                	else
			{
				if(fqNum ==6)
	                	{
	                		memcpy(gl_Send8583.Field61,"\xDF\x26\x04",3);
					strcpy(gl_Send8583.Field61+3,"ZFB6");	
	                	}else if(fqNum ==12)
	                	{
	                		memcpy(gl_Send8583.Field61,"\xDF\x26\x05",3);
					strcpy(gl_Send8583.Field61+3,"ZFB12");	
	                	}
                	}			
                }
        }
	else if(nMode == B_posTophone)
	{
                if(fqFlag == 0)
         	{
         		memcpy(gl_Send8583.Field61,"\xDF\x26\x04",3);
			strcpy(gl_Send8583.Field61+3,"ZFB1");	

			memcpy(gl_Send8583.Field61+7,"\xDF\x35",2);
			vLongToStr(strlen(szQrCode), 1, gl_Send8583.Field61+9);
			sprintf((char *)gl_Send8583.Field61+10, "%s", szQrCode);
                }else 
                {
                	if(gl_SysInfo.ucHbfqFlag)
                	{
                		if(fqNum >0 && fqNum <10)
	                	{
	                		memcpy(gl_Send8583.Field61,"\xDF\x26\x04",3);
					sprintf(gl_Send8583.Field61+3,"ZFB%d",fqNum);	

					memcpy(gl_Send8583.Field61+7,"\xDF\x35",2);
					vLongToStr(strlen(szQrCode), 1, gl_Send8583.Field61+9);
					sprintf((char *)gl_Send8583.Field61+10, "%s", szQrCode);
					
	                	}else if(fqNum <= 99)
	                	{
	                		memcpy(gl_Send8583.Field61,"\xDF\x26\x05",3);
					sprintf(gl_Send8583.Field61+3,"ZFB%02d",fqNum);	

					memcpy(gl_Send8583.Field61+8,"\xDF\x35",2);
					vLongToStr(strlen(szQrCode), 1, gl_Send8583.Field61+10);
					sprintf((char *)gl_Send8583.Field61+11, "%s", szQrCode);	
	                	}
                	}
	                else
			{	
				if(fqNum ==6)
	                	{
	                		memcpy(gl_Send8583.Field61,"\xDF\x26\x04",3);
					strcpy(gl_Send8583.Field61+3,"ZFB6");	

					memcpy(gl_Send8583.Field61+7,"\xDF\x35",2);
					vLongToStr(strlen(szQrCode), 1, gl_Send8583.Field61+9);
					sprintf((char *)gl_Send8583.Field61+10, "%s", szQrCode);
					
	                	}else if(fqNum ==12)
	                	{
	                		memcpy(gl_Send8583.Field61,"\xDF\x26\x05",3);
					strcpy(gl_Send8583.Field61+3,"ZFB12");	

					memcpy(gl_Send8583.Field61+8,"\xDF\x35",2);
					vLongToStr(strlen(szQrCode), 1, gl_Send8583.Field61+10);
					sprintf((char *)gl_Send8583.Field61+11, "%s", szQrCode);	
	                	}
	                }			
                }		
	}

	 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

   	 //打8583包-填充http-发送-接收-解http-解8583包
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
	        return 1;
   	 }

	if (strcmp((char *)gl_Recv8583.Field39, "00") && strcmp((char *)gl_Recv8583.Field39, "01"))
	{
		vShowHostErrMsg(gl_Recv8583.Field39,gl_Recv8583.Field56+2);
		return 1;
	} 
		
	if(nMode == B_posTophone)
	{      
		memset(&QrTrRec, 0, sizeof(QrTrRec));			
		QrTrRec.uiTransType=QRTRANS_TYPE_SALE;

		QrTrRec.ucHubei = 1;  
		QrTrRec.ucHubeiFq = fqFlag;
		QrTrRec.iHubeiNum = fqNum; 
			   
		QrTrRec.ucPayMode= B_posTophone;
		QrTrRec.ucProType = 0x02; //待确认
		QrTrRec.ulAmount=Amount;
		QrTrRec.ulTTC=ulTTC;
		dbg("QrTrRec.ulTTC12:%d\r\n",QrTrRec.ulTTC);
		strcpy(QrTrRec.szchnlQrCode, szQrCode);
		vTwoOne(gl_SysData.szCurOper, 2, QrTrRec.sOperNo);

		_vGetTime(szBuf);
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 12, QrTrRec.sDateTime);
//		QrTrRec.ucPayWay = PayWay;		
		if(gl_Recv8583.Field37[0])
			strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);	
		
		len = strlen(gl_Recv8583.Field61);
                dbg("len:%d\n",len);
				
	         DivPublicField061(gl_Recv8583.Field61,len,&QrTrRec);	
		dbg("orderId:%s\n",QrTrRec.orderId);

#if 0			 
		 uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrRec);
		 
		 gl_SysData.uiQrTransNum ++;	
		 uiMemManaPutSysData();		
#endif

	 	_vCls();
		vDispCenter(1, szTitle, 1);			

		vDispCenter(2, "查询中,请稍候...", 0);
	//	vDispCenter(4, "[取消]退出",0);	

		 memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	         memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

		strcpy((char *)gl_Send8583.Msg01, "0200");
		strcpy((char *)gl_Send8583.Field02,"0002220000000000");	
		strcpy((char *)gl_Send8583.Field03, "310000");
		sprintf((char *)gl_Send8583.Field04, "%012lu", QrTrRec.ulAmount);
	
	//	vIncTTC();
	//	ulTTC=gl_SysData.ulTTC;
	//	sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);

		strcpy((char *)gl_Send8583.Field25, "69");
	        if(QrTrRec.sReferenceNo[0])
	        	sprintf((char *)gl_Send8583.Field37, "%.12s", QrTrRec.sReferenceNo);
		strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
		strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

		    //DE59
		GetPublicField059(NULL,NULL, gl_Send8583.Field59);
	
	        //DE61
	        if(nMode == Z_phoneTopos)
	        {
	         	memcpy(gl_Send8583.Field61,"\xDF\x27",2);
			len = strlen(QrTrRec.orderId);
			gl_Send8583.Field61[2] = len;
			strcpy(gl_Send8583.Field61+3,QrTrRec.orderId);	
	        }
		else if(nMode == B_posTophone)
		{			
			memcpy(gl_Send8583.Field61,"\xDF\x27",2);
			len = strlen(QrTrRec.orderId);
			gl_Send8583.Field61[2] = len;
			strcpy(gl_Send8583.Field61+3,QrTrRec.orderId);
		}
	
		 memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);
 
	          _vSetTimer(&ulTimer1, 1000L); //10秒	 
	         _vFlushKey();
		while (timce < 3)
		{			        
			time1 = _uiCheckTimer(ulTimer1); 	
			if(!time1 ) {
				vIncTTC();
				ulTTC=gl_SysData.ulTTC;
				sprintf((char *)gl_Send8583.Field11, "%06lu", ulTTC);	
			iRet = iHttpSendRecv8583(0);
			if (iRet)
			{		
				return 1;
			}
                       // strcpy(gl_Recv8583.Field39,"00");	//linshi
			if(memcmp(gl_Recv8583.Field39,"01",2)==0x00)
			{					
				timce ++;
				continue;
			}	
			
			if(memcmp(gl_Recv8583.Field39,"00",2)==0x00)
			{      
			         _vCls();
				vDispCenter(1, szTitle, 1);
			       //成功
#ifdef ENABLE_PRINTER 
				{		
				       QrTrRec.ucProType = 0x01;					   
					#if 0   
					uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);			
					
					gl_SysData.ulLastTransTTC = QrTrRec.ulTTC;
					uiMemManaPutSysData();
					#endif
					
					vShowWaitEx("支付成功,打印票据...", -1, 0); 
					iPrintQrTrans(&QrTrRec, 0);
					vShowWaitEx(NULL, 0, 0x03); 
				}
#else    
				{
				        QrTrRec.ucProType = 0x01;
					#if 0	
					uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum-1, &QrTrRec);	
					#endif
					vMessage("支付成功");
				}
#endif 
			             return 0;
			}	
			else
			{		
			//失败
			 	vShowHostErrMsg(gl_Recv8583.Field39,gl_Recv8583.Field56+2);
				
			        return 1;
			}			
		     }

	        	if(_uiKeyPressed())
			{
				uiKey =_uiGetKey();
				if(uiKey ==_KEY_CANCEL)
				{
					return 1;
				}
			}		
		}

		vShowHostErrMsg(gl_Recv8583.Field39,gl_Recv8583.Field56+2);
	        dbg("QrTrRec.ucPayWay:%d\n",QrTrRec.ucPayWay);	
	}
	else if(nMode == Z_phoneTopos) 
	{	  
        	memset(&QrTrRec, 0, sizeof(QrTrRec));
		QrTrRec.uiTransType=QRTRANS_TYPE_SALE;

		QrTrRec.ucHubei = 1;  
		QrTrRec.ucHubeiFq = fqFlag;
		QrTrRec.iHubeiNum = fqNum;
		
                QrTrRec.ucPayMode= Z_phoneTopos;
		QrTrRec.ucProType = 0x02; //待确认
		QrTrRec.ulAmount=Amount;
		QrTrRec.ulTTC=ulTTC;			
		vTwoOne(gl_SysData.szCurOper, 2, QrTrRec.sOperNo);

		_vGetTime(szBuf);
		
		if(gl_Recv8583.Field13[0])
			memcpy(szBuf+4, gl_Recv8583.Field13, 4);
		if(gl_Recv8583.Field12[0])
			memcpy(szBuf+8, gl_Recv8583.Field12, 6);
		vTwoOne(szBuf+2, 14, QrTrRec.sDateTime);
//		 dbg("PayWay:%d\n",PayWay);
//		QrTrRec.ucPayWay = PayWay;
//		 dbg("QrTrRec.ucPayWay0:%d\n",QrTrRec.ucPayWay);				
		if(gl_Recv8583.Field37[0])
			strcpy(QrTrRec.sReferenceNo, gl_Recv8583.Field37);				

		len = strlen(gl_Recv8583.Field61);
                dbg("len:%d\n",len);
				
	         DivPublicField061(gl_Recv8583.Field61,len,&QrTrRec);	
		dbg("orderId:%s\n",QrTrRec.orderId);	

#if 0		
	        uiMemManaPutQrTransRec(gl_SysData.uiQrTransNum, &QrTrRec);
			
		gl_SysData.uiQrTransNum ++;
	        uiMemManaPutSysData();		
#endif

	       _vCls();
		vDispCenter(1, szTitle, 1);
	//	_vSetBackLight(1);				
           		DispTransQrCode(QrTrRec.szchnlUrl);

		sprintf(szBuf, "%lu.%02lu元", Amount/100, Amount%100);	
#ifdef LCD_GC9106
		_vDispAt(2, 20-strlen(szBuf), szBuf);
		_vDispAt(3, 20-strlen("请扫码二"), "请扫码二");
		_vDispAt(4, 20-strlen("维码支付"), "维码支付");	
#else
		_vDispAt(2, 21-strlen(szBuf), szBuf);
		_vDispAt(3, 21-strlen("请扫码二"), "请扫码二");
		_vDispAt(4, 21-strlen("维码支付"), "维码支付");	
#endif		
       //		_vDispAt(3, 11, "[确认]查询");		
      //		_vDispAt(4, 11, "[取消]退出");	
		 timce ++;

	          _vSetTimer(&ulTimer1, 1000L); //10秒	 
	         _vFlushKey();
		while (1)
		{			        
			time1 = _uiCheckTimer(ulTimer1); 	
			if(!time1 ) {					
				 if(timce <3)
				{
					_vCls();
					goto start;	
				 }else
				 	break;
		    	 }

	        	if(_uiKeyPressed())
			{
				uiKey =_uiGetKey();
				if(uiKey ==_KEY_CANCEL)						    
				{
					return 1;
				}
			}		
		}
	
	        dbg("QrTrRec.ucPayWay:%d\n",QrTrRec.ucPayWay);
	}

	return 0;	
}

unsigned char iSaleHb(void)
{
	ulong ulTimer;
	uint uiKey;
	int payway = 0,payfq = 0;
	  ulong ulAmount,ulAmount6,ulAmount6Fee,ulAmount12,ulAmount12Fee,HuabeiAmount,GprsAmount;
	uchar szBuf[100];
	uchar szDisp[100];

	uint count, free,num1,num2;
	int page, curpage,line,i;
	ulong ulAmountFq,ulAmountFqFee;
	uchar tmp[10] = {0};
	uchar exit = 0;
	
	_vCls();	
	vMessageMulEx("本产品为花呗专区,非花呗交易请退出选择\"扫码收款\"");
#ifdef LCD_GC9106
       _vDisp(5,"退出            确认"); 
#else
       _vDisp(5,"退出             确认"); 
#endif
 	   
	  _vSetTimer(&ulTimer, 6000L); //60秒	                 
         _vFlushKey();
	while (1)
	{
		 if(_uiTestTimer(ulTimer))
		 	return 1;
		else if(_uiKeyPressed())
		{
			uiKey =_uiGetKey();
			if(uiKey == _KEY_ENTER)
				break;
			else if(uiKey ==_KEY_CANCEL)
				return 1;
		}	
	}

	 _vCls();	   

        vDispCenter(1, "花呗", 1);
	_vDisp(2,"1.花呗分期支付");
	_vDisp(3,"2.花呗支付");
	
	_vSetTimer(&ulTimer, 6000L); //60秒	   
	_vFlushKey();
	while(1)
	{
		 if(_uiTestTimer(ulTimer))
		 	return 1;
		else if(_uiKeyPressed())
		{
			uiKey=_uiGetKey();
			if(uiKey==_KEY_1)
			{	
				payway = 1;
				break;
			}	
			else if(uiKey==_KEY_2)
			{
				payway = 0;
				break;
			}
			else if(uiKey==_KEY_CANCEL)
				return 1;
		}	
	}

       	if(iCheckTranStat( QRTRANS_TYPE_SALE,0))
        	return 1;
 
	if(payway == 1)
		vDispCenter(1, "花呗分期支付", 1);
	else if(payway == 0)
		vDispCenter(1, "花呗支付", 1);
	
/*
    if(gl_SysInfo.iCommType == VPOSCOMM_WIFI)
    {
        vClearLines(2);
    	vMessage("基站信息获取中请稍候");
    	iRet = iGetLocalPosition();
	if(iRet != 0)	
		return 1;
	vClearLines(2);
    }
*/	

	//金额流程	
       	if(iGetAmount(NULL, "请输入金额",NULL, szBuf)<=0)
		return 1;	
	ulAmount=atol(szBuf);

	//分期界面(有分期就分期界面)
	if(payway == 1)
	{
		HuabeiAmount = atol(gl_SysInfo.ucHuabeiAmount);
		GprsAmount = atol(gl_SysInfo.ucGprsAmount);	
		
		if(gl_SysInfo.ucGprsUseFlag == 1 &&  gl_SysInfo.ucGprsPayFlag == 0  && ulAmount < (HuabeiAmount + GprsAmount))
		{
			sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", (HuabeiAmount + GprsAmount)/100, (HuabeiAmount + GprsAmount)%100);
			vMessageMul(szDisp);
			return 1;		  
		}else if(ulAmount < HuabeiAmount)	
		{
			sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", HuabeiAmount/100, HuabeiAmount%100);
			vMessageMul(szDisp);
			return 1;	
		}	
		else if(ulAmount >= 10000000)
		{
			vMessageMul("单笔交易金额需要小于100000元");
			return 1;
		}	

		_vCls();
		if(gl_SysInfo.ucHbfqFlag)
		{
			vOneTwo(&gl_SysInfo.ucHbfqCount, 1, tmp);
			count = (uint)ulA2L(tmp,2);
			dbg("count:%d\n",count);

			if(count > 9)
				count = 9;	//最多9个分期
			if(count%2)
			{
				page = count /2 +1;
			}	
			else
				page = count / 2 ;

			i = 0;
			curpage = 1;
			while(i < count && exit == 0)
			{										
				vDispCenterEx(1, "选择分期数", 1, 1, curpage, page);

				vClearLines(2);
				line = 2;	
				
				vOneTwo(&gl_SysInfo.ucHbfqInfo[i*4],1,tmp);
				num1 = (uint)ulA2L(tmp,2);
				dbg("num1:%d\n",num1);
				
				vOneTwo(&gl_SysInfo.ucHbfqInfo[i*4+1],3,tmp);
				free =  (uint)ulA2L(tmp,6);	
				dbg("free1:%d\n",free);
				
				ulAmountFqFee = (ulAmount* free/1000)/num1;
				dbg("ulAmountFqFee:%d\n",ulAmountFqFee);
				
				ulAmountFq = ulAmount/num1 + ulAmountFqFee;
				dbg("ulAmountFq:%d\n",ulAmountFq);
				
				vDispMidVarArg(line++,"%d.￥%lu.%02lu元*%d期",  i+1,ulAmountFq/100, ulAmountFq%100,num1);
				vDispMidVarArg(line++,"手续费:%lu.%02lu/期",   ulAmountFqFee/100, ulAmountFqFee%100);

				num2 = 0;
				 if(i  != count-1)
				{
					i++;
					
					vOneTwo(&gl_SysInfo.ucHbfqInfo[i*4],1,tmp);
					num2 = (uint)ulA2L(tmp,2); 
					dbg("num2:%d\n",num2);
					
					vOneTwo(&gl_SysInfo.ucHbfqInfo[i*4+1],3,tmp);
					free =  (uint)ulA2L(tmp,6);
					dbg("free2:%d\n",free);
					
					ulAmountFqFee = (ulAmount* free/1000)/num2;
					dbg("ulAmountFqFee:%d\n",ulAmountFqFee);
					
					ulAmountFq = ulAmount/num2 + ulAmountFqFee; 
					dbg("ulAmountFq:%d\n",ulAmountFq);
					
					vDispMidVarArg(line++,"%d.￥%lu.%02lu元*%d期",  i+1,ulAmountFq/100, ulAmountFq%100,num2);
					vDispMidVarArg(line++,"手续费:%lu.%02lu/期",   ulAmountFqFee/100, ulAmountFqFee%100);					
				 }	
				
				_vSetTimer(&ulTimer, 6000L); //60秒
				_vFlushKey();
				while(1)
				{
					if(_uiTestTimer(ulTimer))
				 		return 1;
					else if(_uiKeyPressed())
					{
						uiKey=_uiGetKey();
						if(uiKey==_KEY_1 && curpage ==1)
						{
							payfq = num1;
							exit = 1;
							break;
						}	
						else if(uiKey==_KEY_2 && curpage ==1 && num2 > 0)
						{
							payfq = num2;
							exit = 1;
							break;
						}	
						else if(uiKey==_KEY_3 && curpage ==2)
						{
							payfq = num1;
							exit = 1;
							break;
						}	
						else if(uiKey==_KEY_4 && curpage ==2 && num2 > 0)
						{
							payfq = num2;
							exit = 1;
							break;
						}
						else if(uiKey==_KEY_5 && curpage ==3)
						{
							payfq = num1;
							exit = 1;
							break;
						}	
						else if(uiKey==_KEY_6 && curpage ==3 && num2 > 0)
						{
							payfq = num2;
							exit = 1;
							break;
						}
						else if(uiKey==_KEY_7 && curpage ==4)
						{
							payfq = num1;
							exit = 1;
							break;
						}	
						else if(uiKey==_KEY_8 && curpage ==4 && num2 > 0)
						{
							payfq = num2;
							exit = 1;
							break;
						}
						else if(uiKey==_KEY_9 && curpage ==5)
						{
							payfq = num1;
							exit = 1;
							break;
						}												
						else if(uiKey == _KEY_DOWN)
						{
							if(curpage < page)
							{
								i++;	
								curpage ++;
								break;
							}
						}
						else if(uiKey == _KEY_UP)	
						{
							if(curpage != 1)
							{
								if(i%2)
									i -= 3;
								else	
									i -= 2;
								curpage --;
								break;
							}
						}
						else if(uiKey==_KEY_CANCEL)
							return 1;
					}	
				}
			}	
		}
		else
		{
			ulAmount6Fee = (ulAmount* 4.5/100)/6;
			ulAmount6 = 	ulAmount/6 + ulAmount6Fee;

			ulAmount12Fee = (ulAmount* 7.5/100)/12;
			ulAmount12 = 	ulAmount/12 + ulAmount12Fee;
			
			vDispCenter(1,"选择分期数",0);
			vDispMidVarArg(2,"1.￥%lu.%02lu元*12期",    ulAmount12/100, ulAmount12%100); 		
			vDispMidVarArg(3,"手续费:%lu.%02lu/期", ulAmount12Fee/100, ulAmount12Fee%100); 

			vDispMidVarArg(4,"2.￥%lu.%02lu元*6期",    ulAmount6/100, ulAmount6%100);
			vDispMidVarArg(5,"手续费:%lu.%02lu/期",   ulAmount6Fee/100, ulAmount6Fee%100);

			_vSetTimer(&ulTimer, 6000L); //60秒
			_vFlushKey();
			while(1)
			{
				if(_uiTestTimer(ulTimer))
			 		return 1;
				else if(_uiKeyPressed())
				{
					uiKey=_uiGetKey();
					if(uiKey==_KEY_1 || uiKey == _KEY_ENTER)
					{
						payfq = 12;
						break;
					}	
					else if(uiKey==_KEY_2)
					{
						payfq = 6;
						break;
					}	
					else if(uiKey==_KEY_CANCEL)
						return 1;
				}	
			}
		}	
	}else if(payway == 0)
	{
		HuabeiAmount = atol(gl_SysInfo.ucHuabeiAmount);
		GprsAmount = atol(gl_SysInfo.ucGprsAmount);	
	  
		if(gl_SysInfo.ucGprsUseFlag == 1 &&  gl_SysInfo.ucGprsPayFlag == 0  && ulAmount < (HuabeiAmount + GprsAmount))
		{
			sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", (HuabeiAmount + GprsAmount)/100, (HuabeiAmount + GprsAmount)%100);
			vMessageMul(szDisp);
			return 1;		  
		}else if(ulAmount < HuabeiAmount)	
		{
			sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", HuabeiAmount/100, HuabeiAmount%100);
			vMessageMul(szDisp);
			return 1;	
		}
	}
	dbg("payfq:%d\n",payfq);

          _vCls();	   
        if(payway == 1)
		vDispCenter(1, "花呗分期支付", 1);
	else if(payway == 0)
		vDispCenter(1, "花呗支付", 1);
	
	  _vDisp(2,"1.扫码支付");
	_vDisp(3,"2.二维码支付");

	_vSetTimer(&ulTimer, 6000L); //60秒
	_vFlushKey();
	while(1)
	{
		if(_uiTestTimer(ulTimer))
		 	return 1;
		else if(_uiKeyPressed())
		{
			uiKey=_uiGetKey();
			if(uiKey==_KEY_1||uiKey==_KEY_2)
				break;
			else if(uiKey==_KEY_CANCEL)
				return 1;
		}	
	} 

	if(uiKey == _KEY_1)   
		return iHbQRCodePay(B_posTophone,ulAmount,payway,payfq);
        else if(uiKey == _KEY_2)
		return iHbQRCodePay(Z_phoneTopos,ulAmount,payway,payfq);
	else 
		return 1;
}
