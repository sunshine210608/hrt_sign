//#include "vposface.h"
#include "Pack8583.h"
#include "St8583_cjt.h"

const FIELD_ATTR Msg0[] =
    {
        {Attr_n, Attr_fix, Msg0_01Len}, // message type
        {Attr_Over, Attr_fix, Attr_fix}
    };

const FIELD_ATTR CjtData0[] =
{
    {Attr_b,			Attr_fix,		Data0_BitMapLen}, // bit map;
    {Attr_n,			Attr_var1,      Data0_02Len},    // pan
    {Attr_n,			Attr_fix,		Data0_03Len},     // processing code
    {Attr_n,			Attr_fix, 		Data0_04Len},     // amount
    
#if 1        
    {Attr_b, Attr_var2, Data0_05Len},          // 05
    {Attr_a, 		    Attr_var1, 		Data0_06Len},          // 06
    {Attr_UnUsed, Attr_var1, 0},     	// datetime
    {Attr_UnUsed, Attr_fix, 0},          // 08
    {Attr_UnUsed, Attr_fix, 0},          // 09
    {Attr_UnUsed, Attr_fix, 0},          // 10
#endif
	{Attr_n, 		Attr_fix, 		Data0_11Len},     // TTC
    {Attr_n, 		Attr_fix, 		Data0_12Len},     // time of local transaction
    {Attr_n, 		Attr_fix, 		Data0_13Len},     // date of local transaction
    {Attr_n, 		Attr_fix, 		Data0_14Len},     // expire date
    {Attr_n, 		Attr_fix, 		Data0_15Len},     // 15
    {Attr_UnUsed, Attr_fix, 0},          // 16
    {Attr_UnUsed, Attr_fix, 0},          // 17
    {Attr_UnUsed, Attr_fix, 0},          // 18
    {Attr_UnUsed, Attr_fix, 0},          // 19
    {Attr_UnUsed, Attr_fix, 0},          // 20
    {Attr_UnUsed, Attr_fix, 0},          // 21
    {Attr_n, 		Attr_fix, 		Data0_22Len},     // entry mode, 银联左靠
    {Attr_n,			Attr_fix, 	Data0_23Len},     // pan serial no
    {Attr_n,                Attr_fix,            Data0_24Len},          // 24
    {Attr_n, 		Attr_fix,		Data0_25Len},     // 25
    {Attr_n, 		Attr_fix,		Data0_26Len},     // 26
    {Attr_n, 		Attr_fix, 		Data0_27Len},          // 27
    {Attr_a, 		Attr_fix, 		Data0_28Len},     	 // 28
    {Attr_a, 		Attr_fix, 		Data0_29Len},     	 // 29
    {Attr_a, 		Attr_fix, 		Data0_30Len},          // 30
    {Attr_UnUsed, Attr_fix, 0},          // 31
    {Attr_n,			Attr_var1,	 	Data0_32Len},     // 32
    {Attr_UnUsed, Attr_fix, 0},          // 33
    {Attr_UnUsed, Attr_fix, 0},          // 34
    {Attr_n,			Attr_var1,	 	Data0_35Len},    // track2
    {Attr_n,			Attr_var2, 		Data0_36Len},    // track3
    {Attr_a,			Attr_fix, 		Data0_37Len},     // system trace no
    {Attr_a,			Attr_fix, 		Data0_38Len},     // auth code
    {Attr_a,			Attr_fix, 		Data0_39Len},     // response code
    {Attr_b,			Attr_var2,		Data0_40Len},     
    {Attr_a, 			Attr_fix, 	    Data0_41Len},     // card acceptor terminal identification
    {Attr_a, 			Attr_fix,		Data0_42Len},     // card acceptor identification code
    {Attr_a, 			Attr_fix,		Data0_43Len}, 
    {Attr_a,			Attr_var1,		Data0_44Len},    // 44
    {Attr_b, 			Attr_var1, Data0_45Len},          // 45
    {Attr_UnUsed, Attr_fix, 0},          // 46
    {Attr_a,			Attr_var2,		Data0_47Len},    // 47
    {Attr_a,			Attr_var2,		Data0_48Len},    // 附加数据
    {Attr_n,			Attr_fix,			Data0_49Len},     // 交易货币代码
    {Attr_UnUsed, Attr_fix, 0},          // 50
    {Attr_UnUsed,			Attr_var2,			0},          // 51
    {Attr_b,			Attr_fix,			Data0_52Len},     // pin data
    {Attr_n,			Attr_fix,			Data0_53Len},     // 53
    {Attr_a,			Attr_var2,		Data0_54Len},    // 附加金额
    {Attr_b,			Attr_var2,		Data0_55Len},    // IC卡数据
    {Attr_b,			Attr_var2,		Data0_56Len},    // 56
    {Attr_b,			Attr_var2,		Data0_57Len},    // 57
    {Attr_b,			Attr_var2,		Data0_58Len},    // 58
    {Attr_b,			Attr_var2,		Data0_59Len},    // 59
    {Attr_a,			Attr_var2,		Data0_60Len},//    {Attr_n,			Attr_var2,		Data0_60Len},   // reserved private
    {Attr_a,			Attr_var2,		Data0_61Len},  //{Attr_n,			Attr_var2,		Data0_61Len},    // reserved private
    {Attr_b,			Attr_var2,		Data0_62Len},    //密钥/参数
    {Attr_a,			Attr_var2,		Data0_63Len},    // reserved private
    {Attr_b,			Attr_fix,		Data0_64Len},     // MAC
    {Attr_Over, Attr_fix, Attr_fix}
};
