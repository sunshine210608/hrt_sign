#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "VposFace.h"
#include "pub.h"
#include "cJSON.h"
#include "debug.h"
#include "sys_littlefs.h"
#include "tag.h"
#include "func.h"
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "Manage_cjt.h"

extern int SecReadTusnKey(byte *tusnKey);
extern int gprs_at_IMSI(char* pszIMSI);
extern int iGetICCID(char *pszICCID);
extern int gprs_at_GetCellInfo(char *pszCellInfo);
extern int SecCalTusnCiphertext(char *tusn, char *randomFactor, unsigned char *tusnKey, 
		char *szTusnCiphertextOut);
extern int gprs_at_localip(char *ip);
extern int wifi_at_get_localIP(char *pszIp, char *pszMac);
extern void dbgHexEx(uchar *head, uchar *in, uint len,uchar const *pasFun, int displine);
extern char* _pszGetCLIVer(char *pszVer);
extern int _iSm4(uint uiMode, uchar *psSource, uchar *psKey, uchar *psResult);
	
typedef struct __SCRT_TMNL_INFO
{
    u8 ucTmnlType[4];//设备类型(字符串) "01":ATM,"02":传统POS,"03":MPOS,"04":智能POS,"05":扫码支付POS
    u32 uiTmnlSrlNmbLen;//终端硬件序列号长度
    u8 ucTmnlSrlNmb[52];//终端硬件序列号:6位厂商编号+2位终端类型（同设备类型）+42位自定义序列号   
    u8 ucfactCipher[8];//硬件序列号密文数据
    u8 ucRevered[32];//保留位    
}SCRT_TMNL_INFO;//终端密文信息


u16 GetTLVFieldData(char sp,u16 sn,u8 *lpIn,u32 nLc,u8 *lpOut)
{
	u8 *p1,*p2,k;
	u16 len = 0;
	u32 j = 0;

	p1 = lpIn;

//	Trace("huangxin", "p1=%s\r\n",p1);
	
	//防止下面的算法溢出
	for(k = 0, j = 0; j < nLc; j++)
	{
		if(p1[j] == sp)
			k++;
	}
//	Trace("huangxin", "j=%d,k=%d\r\n",j,k);

	if(k < sn)
		return 0;
	//end 防止下面的算法溢出

	//460,07,18CC,8646E92
	for(k = 0;k < sn;k ++)
	{
		if(k == sn)	break;
		p1 = (uchar *)strchr(p1,sp);
		if(p1 == NULL)
		{
			return 0;
		}
		p1 ++;
	}
//	Trace("huangxin", "p1=%s\r\n",p1);
	p2 = (uchar *)strchr(p1,sp);
	if(p2 == NULL) p2 = lpIn + nLc;
//	Trace("huangxin", "p2=%s\r\n",p2);
	len = p2 - p1;
//	Trace("huangxin", "len=%d\r\n",len);
	memcpy(lpOut,p1,len);
	return len;
}


u16 SetTlvData(u8 *pOutData,u8 ucTag,u8 *pInData,u16 usInLen)
{
    u8 temp[128] = {0};
    u16 usLen = 0;
    if (pOutData == NULL || pInData == NULL)
    {
        return 0;
    }

    sprintf(&temp[usLen],"%02d",ucTag);
    usLen += 2;
    sprintf(&temp[usLen],"%03d",usInLen);
    usLen += 3;
    memcpy(&temp[usLen],pInData,usInLen);
    usLen += usInLen;
    TraceHex("value",temp, usLen);

    memcpy(pOutData,temp,usLen);
    return usLen;
}


//pszRandom: 加密随机因子:银行卡交易采用卡号后6位；扫码付交易采用C2B码后6位
void GetPublicField059(char *pszEncPin, uchar *pszRandom, uchar asOut[])
{
	char szBuf[100];
	unsigned char tusnKey[20];
	char szTusnCiphertextOut[10];
	uchar *p;
	int len;
	int ret;
        uchar ucMcc[30+1]={0}, ucMnc[30+1]={0}, ucLac[30+1]={0}, ucCi[30+1]={0}; 
        uchar possn[20+1]={0},csn[11+1] = {0};
	char szLocalIp[20]={0}, szMac[20]={0};
	
	ret = SecReadTusnKey(tusnKey);
	if (ret != 0)
	{
		dbg("No TusnKey, no Field59\n");		
		return;
	}
	TraceHex("tusnKey",tusnKey,16);  
	
	p=asOut+2;
       if(gl_SysInfo.ucSmFlag == 1 && pszEncPin && pszEncPin[0])
       {
       		p[0] = 0xA1;
		p[1] = strlen(pszEncPin);
		memcpy(p+2,pszEncPin,p[1]);  	  
		p += 2 + p[1];
       }
	   
	p[0] = 0x04;
	p[1] = 2;
	memcpy(p+2,"02",p[1]);  	  
	p += 2 + p[1];	

        if(gl_ucQrTransFlag == 0)
	{
		#ifdef TESTTUSN
			memcpy(szBuf, TESTTUSN, strlen(TESTTUSN));
		       szBuf[20] = 0;
		#else
			_uiGetSerialNo(szBuf);
		#endif
		
		p[0] = 0x05;
		p[1] = strlen(szBuf);
		memcpy(p+2,szBuf,p[1]);  	  
		p += 2 + p[1];		  
        }else
        {
		#ifdef TESTTUSN
			memcpy(possn, TESTTUSN, strlen(TESTTUSN));
			memcpy(csn,possn+9,11);
		#else
			_uiGetSerialNo(possn);
			memcpy(csn,possn+9,11);
		#endif

		p[0] = 0x05;
		p[1] = strlen(csn);
		memcpy(p+2,csn,p[1]);  	  
		p += 2 + p[1];
        }
		
	if(pszRandom && pszRandom[0])
	{
		p[0] = 0x06;
		p[1] = strlen(pszRandom);
		memcpy(p+2,pszRandom,p[1]);  	  //加密随机因子(磁道2后6位)
		p += 2 + p[1];

	        szTusnCiphertextOut[0]=0;
#ifdef TESTTUSN
               strcpy(szTusnCiphertextOut,"0BFAAADC");
		if(szTusnCiphertextOut[0])
		{
			p[0] = 0x07;					
			p[1]=strlen(szTusnCiphertextOut);
			memcpy(p+2,szTusnCiphertextOut,p[1]); //硬件序列号密文数据	
			p+=2+p[1];
		}  
#else
		ret=SecCalTusnCiphertext(szBuf, pszRandom, tusnKey, szTusnCiphertextOut);
		if(ret==0 && szTusnCiphertextOut[0])
		{
			p[0] = 0x07;					
			p[1]=strlen(szTusnCiphertextOut);
			memcpy(p+2,szTusnCiphertextOut,p[1]); //硬件序列号密文数据	
			p+=2+p[1];
		}   
#endif		
	}

	p[0] = 0x08;
	p[1] = 8;
	_pszGetCLIVer(szBuf);	
	strcpy(p+2, szBuf);
	p += 2 + p[1];     

       if(gl_SysInfo.iCommType == VPOSCOMM_GPRS) 
	{
		//基站信息
		memset(szBuf,0,sizeof(szBuf));
		gprs_at_GetCellInfo(szBuf);
		dbg("gprs_at_GetCellInfo:%s\r\n",szBuf);
		//460,07,18CC,8646E92  lac和cid都为16进制，不存在在填0
	        GetTLVFieldData(',', 0, szBuf, strlen(szBuf), ucMcc);
	        GetTLVFieldData(',', 1, szBuf, strlen(szBuf), ucMnc);
		GetTLVFieldData(',', 2, szBuf, strlen(szBuf), ucLac);
		GetTLVFieldData(',', 3, szBuf, strlen(szBuf), ucCi);

		p[0] = 0x10;
		p[1] = strlen(ucMcc);	
	        memcpy(p+2,ucMcc,p[1]);  
		p += 2 + p[1];	
		   
	        p[0] = 0x11;
		strcpy(ucMnc, "00");
		p[1] = strlen(ucMnc);	
		memcpy(p+2,ucMnc,p[1]);   
		p += 2 + p[1];	
		
	        p[0] = 0x12;
		p[1] = strlen(ucLac);
		memcpy(p+2,ucLac,p[1]);  
		p += 2 + p[1];	
			
		p[0] = 0x13;
		p[1] = strlen(ucCi);
		memcpy(p+2,ucCi,p[1]);  
		p += 2 + p[1];	

	        if(gl_ucQrTransFlag  == 0) 
		{
			//14 IP        
		        memset(szBuf,0x00,sizeof(szBuf));
		        gprs_at_localip(szBuf); 
		       p[0] = 0x14;
		       p[1] = strlen(szBuf);
		       memcpy(p+2,szBuf,p[1]);  	  
		       p += 2 + p[1];	 

			//15	
		        memset(szBuf,0x00,sizeof(szBuf));
		        iGetICCID(szBuf); 
		       p[0] = 0x15;
		       p[1] = strlen(szBuf);
		       memcpy(p+2,szBuf,p[1]);  	  
		       p += 2 + p[1];	
	        }else if(gl_ucQrTransFlag == 1)
	        {
	        	//14 IP        
		        memset(szBuf,0x00,sizeof(szBuf));
		        gprs_at_localip(szBuf); 
		       p[0] = 0x14;
		       p[1] = strlen(szBuf);
		       memcpy(p+2,szBuf,p[1]);  	  
		       p += 2 + p[1];	

			//15	
		        memset(szBuf,0x00,sizeof(szBuf));
		        iGetICCID(szBuf); 
		       p[0] = 0x15;
		       p[1] = strlen(szBuf);
		       memcpy(p+2,szBuf,p[1]);  	  
		       p += 2 + p[1];	

		       //16	   
#ifdef TESTTUSN
			memcpy(szBuf, TESTTUSN, strlen(TESTTUSN));
			szBuf[20] = 0;
#else
			_uiGetSerialNo(szBuf);
#endif

			p[0] = 0x16;
			p[1] = strlen(szBuf);
			memcpy(p+2,szBuf,p[1]);  	  
			p += 2 + p[1];	
	        }
       	}else if(gl_SysInfo.iCommType == VPOSCOMM_WIFI)
       	{
       		dbgHex("gl_baseStation059",gl_baseStation,strlen(gl_baseStation));
			
       		//460,07,18CC,8646E92  lac和cid都为16进制，不存在在填0
	        GetTLVFieldData(',', 0, gl_baseStation, strlen(gl_baseStation), ucMcc);
	        GetTLVFieldData(',', 1, gl_baseStation, strlen(gl_baseStation), ucMnc);
		GetTLVFieldData(',', 2, gl_baseStation, strlen(gl_baseStation), ucLac);
		GetTLVFieldData(',', 3, gl_baseStation, strlen(gl_baseStation), ucCi);
		
       		p[0] = 0x10;
		p[1] = strlen(ucMcc);	
	        memcpy(p+2,ucMcc,p[1]);  
		p += 2 + p[1];	
		   
	        p[0] = 0x11;
		strcpy(ucMnc, "00");
		p[1] = strlen(ucMnc);	
		memcpy(p+2,ucMnc,p[1]);   
		p += 2 + p[1];	
		
	        p[0] = 0x12;
		p[1] = strlen(ucLac);
		memcpy(p+2,ucLac,p[1]);  
		p += 2 + p[1];	
			
		p[0] = 0x13;
		p[1] = strlen(ucCi);
		memcpy(p+2,ucCi,p[1]);  
		p += 2 + p[1];	

		 if(gl_ucQrTransFlag  == 0) 
		{
			//14 IP        
		        memset(szBuf,0x00,sizeof(szBuf));
		        wifi_at_get_localIP(szLocalIp, szMac); 
		       p[0] = 0x14;
		       p[1] = strlen(szLocalIp);
		       memcpy(p+2,szLocalIp,p[1]);  	  
		       p += 2 + p[1];	 
	        }else if(gl_ucQrTransFlag == 1)
	        {
	        	//14 IP        
		        memset(szBuf,0x00,sizeof(szBuf));
		        wifi_at_get_localIP(szLocalIp, szMac);
		       p[0] = 0x14;
		       p[1] = strlen(szLocalIp);
		       memcpy(p+2,szLocalIp,p[1]);  	  
		       p += 2 + p[1];	

		       //16	   
#ifdef TESTTUSN
			memcpy(szBuf, TESTTUSN, strlen(TESTTUSN));
			szBuf[20] = 0;
#else
			_uiGetSerialNo(szBuf);
#endif

			p[0] = 0x16;
			p[1] = strlen(szBuf);
			memcpy(p+2,szBuf,p[1]);  	  
			p += 2 + p[1];	
	        }
       	}

//扫码交易用到
	//A3
//        if(strcmp(gl_Send8583.Field23,"03"))
//	{
//        }	

	len=p-asOut-2;
	asOut[0]=len/256;
	asOut[1]=len%256;
	TraceHex("gl_Send8583.Field59",asOut+2,len);  
}

uint32 EncryptMagData(u8 ucTrackData[],u16 iLen,u8 lpOut[])
{
    u8 tmp[256], buf[128];
    u8 *pBuf=0;
    bool bIsEncryp = false;
    u8 tmpend = 0;
    int len= 0;
	
    if(ucTrackData==NULL||lpOut==NULL)return 1;
 	dbgHex("gl_SysData.sMagKey", gl_SysData.sMagKey, 16);
    if (iLen > 16)
    {
        dbg("ucTrackData=[%s]\r\n", ucTrackData);
		memset(buf,0,sizeof(buf));

	if(gl_SysInfo.ucSmFlag == 0)	
	{
	        vTwoOne(ucTrackData, iLen,buf);
		pBuf = &buf[(iLen + 1) / 2 - 9];
        //加密
		 _vDes(TRI_ENCRYPT, pBuf, gl_SysData.sMagKey, pBuf);
//		 _vDes(TRI_ENCRYPT, pBuf+8, gl_SysData.sMagKey, pBuf+8);
	}
	else if(gl_SysInfo.ucSmFlag == 1)
	{
	        len = iLen - 1;
		dbg("len:%d\n",len);	
	        if(len >= 32)
		{
			vTwoOne(ucTrackData, iLen,buf);
			pBuf = &buf[(iLen + 1) / 2 - 17];
	        }else 
	        {
	              tmpend  = ucTrackData[len];
	               while(len < 32)
	        	{
	                        ucTrackData[len] = 'F';
				len++;	
	               	}
		        dbg("ucTrackData2=[%s]\r\n", ucTrackData);
				   
			ucTrackData[len] = tmpend;
                        len++;
			iLen = len;
						
			ucTrackData[iLen] = 0;	
			dbg("ucTrackData1=[%s]\r\n", ucTrackData);
			vTwoOne(ucTrackData, iLen,buf);
			pBuf = &buf[(iLen + 1) / 2 - 17];	
	        }
			
		_iSm4(ENCRYPT, pBuf, gl_SysData.sMagKey, pBuf);
	}
		memset(tmp, 0, sizeof(tmp));
        vOneTwo(buf, (iLen + 1) / 2,tmp);
        dbg("EncryptMagData=[%s]\r\n", tmp);
		memcpy(lpOut,tmp,iLen);
        bIsEncryp = true;
    }


    return 0;
}


void AjustTrack2Data(u8 asTrack2Data[]){
	u16 i = 0;
	if(asTrack2Data==NULL)return;
	
	for(i=0;i<strlen(asTrack2Data);i++){
		if(asTrack2Data[i]=='='){
			asTrack2Data[i] = 'D';
			break;
		}
	}		

}

void GetPublicField058(uchar asOut[])
{
       uchar szBuf[100];
	uchar ucMcc[30+1]={0}, ucMnc[30+1]={0}, ucLac[30+1]={0}, ucCi[30+1]={0}; 
	uchar *p;
	int len;
//基站信息
	memset(szBuf,0,sizeof(szBuf));
	gprs_at_GetCellInfo(szBuf);
	dbg("gprs_at_GetCellInfo:%s\r\n",szBuf);
//460,07,18CC,8646E92  lac和cid都为16进制，不存在在填0
        GetTLVFieldData(',', 0, szBuf, strlen(szBuf), ucMcc);
        GetTLVFieldData(',', 1, szBuf, strlen(szBuf), ucMnc);
	GetTLVFieldData(',', 2, szBuf, strlen(szBuf), ucLac);
	GetTLVFieldData(',', 3, szBuf, strlen(szBuf), ucCi);
 //       GetTLVFieldData(',', 4, szBuf, strlen(szBuf), ucImsi);

        p = szBuf;

        p[0] = 0xA1;
	p += 1;
	
	p[0] = 0xBD;
	p[1] = 2;
        memcpy(p+2,"01",p[1]);
	p += 2 + p[1];	
	   
        p[0] = 0xB1;
	p[1] = strlen(ucMcc);	
        memcpy(p+2,ucMcc,p[1]);  
	p += 2 + p[1];	
	   
        p[0] = 0xB2;
	strcpy(ucMnc, "00");
	p[1] = strlen(ucMnc);	
	memcpy(p+2,ucMnc,p[1]);   
	p += 2 + p[1];	
	
        p[0] = 0xB3;
	p[1] = strlen(ucLac);
	memcpy(p+2,ucLac,p[1]);  
	p += 2 + p[1];	
		
	p[0] = 0xB4;
	p[1] = strlen(ucCi);
	memcpy(p+2,ucCi,p[1]);  
	p += 2 + p[1];	

  if(gl_ucQrTransFlag == 1)	
	{
		p[0] = 0xD1;
		 p[1] = 0x00;
		 p[2] = 0xD2;
		 p[3] = 0x00;   

	        p += 4;
  	}
  
	len = p - szBuf;	
	dbg("len:%d\n",len);	

	memcpy(asOut,szBuf,len);
	dbgHex("asOut", asOut,  len);
}

int  DivPublicField047(u8 *pInData,int iLen)
{
       uchar uiSvrPort[4+1] = {0},uiBakSvrPort[4+1] = {0};
       uchar  szMerchName[40+1]  = {0};	
       int len,len1;
       uint i,length,count;
       uchar tmp[2+1] = {0};
       uchar *p;

/*	
       if(iLen != 194)
       	{
       	   vMessage("参数错误");
       	   return -1;
       	}
*/
	
       vMemcpy0(gl_SysInfo.szPosId,pInData,8);
       dbg("gl_SysInfo.szPosId:%s\n",gl_SysInfo.szPosId);	   
	pInData += 8;
	
	vMemcpy0(gl_SysInfo.szMerchId,pInData,15);
	dbg("gl_SysInfo.szMerchId:%s\n",gl_SysInfo.szMerchId);	
	pInData += 15;

	vMemcpy0(szMerchName,pInData,40);
	rtrim(szMerchName);
	strcpy(gl_SysInfo.szMerchName,szMerchName);
	dbgHex("gl_SysInfo.szMerchName",gl_SysInfo.szMerchName,strlen(gl_SysInfo.szMerchName));	
	pInData += 40;

	  //主
	  p = (uchar *)strchr(pInData,':');
	 if(p == NULL)
    	 {
    	 	return -1;
     	 }	 
	  len = p - pInData;
	  vMemcpy0(gl_SysInfo.szSvrIp, pInData, len);
          dbg("gl_SysInfo.szSvrIp:%s\n",gl_SysInfo.szSvrIp);
          pInData += len;

          pInData +=1; //:

	len1 = 21 - len -1;   
        vMemcpy0(uiSvrPort,pInData,len1);
        rtrim(uiSvrPort);		
        gl_SysInfo.uiSvrPort = atol(uiSvrPort);
	dbg("gl_SysInfo.uiSvrPort:%d\n",gl_SysInfo.uiSvrPort);	
	pInData += len1;

         //备
	  p = (uchar *)strchr(pInData,':');
	  len = p - pInData;
	  vMemcpy0(gl_SysInfo.szBakSvrIp, pInData, len);
          dbg("gl_SysInfo.szBakSvrIp:%s\n",gl_SysInfo.szBakSvrIp);
          pInData += len;

          pInData +=1; //:

	len1 = 21 - len -1;   
        vMemcpy0(uiBakSvrPort,pInData,len1);
        rtrim(uiBakSvrPort);		
        gl_SysInfo.uiBakSvrPort = atol(uiBakSvrPort);
	dbg("gl_SysInfo.uiBakSvrPort:%d\n",gl_SysInfo.uiBakSvrPort);	
	pInData += len1;

	gl_SysInfo.ucDepositFlag = *pInData - '0';
	dbg("gl_SysInfo.ucDepositFlag:%d\n",gl_SysInfo.ucDepositFlag);
	pInData += 1;

	vMemcpy0(gl_SysInfo.ucDepositAmount,pInData,12);
	dbg("gl_SysInfo.ucDepositAmount:%s\n",gl_SysInfo.ucDepositAmount);	
	pInData += 12;

	gl_SysInfo.ucActivityFlag = *pInData - '0';
	dbg("gl_SysInfo.ucActivityFlag:%d\n",gl_SysInfo.ucActivityFlag);
	pInData += 1;

	vMemcpy0(gl_SysInfo.ucFirstSwipeAmount,pInData,12);
	dbg("gl_SysInfo.ucFirstSwipeAmount:%s\n",gl_SysInfo.ucFirstSwipeAmount);	
	pInData += 12;

	vMemcpy0(gl_SysInfo.ucQrAmount,pInData,12);
	dbg("gl_SysInfo.ucQrAmount:%s\n",gl_SysInfo.ucQrAmount);	
	pInData += 12;

	vMemcpy0(gl_SysInfo.ucScanAmount,pInData,12);
	dbg("gl_SysInfo.ucScanAmount:%s\n",gl_SysInfo.ucScanAmount);	
	pInData += 12;

	vMemcpy0(gl_SysInfo.ucHuabeiAmount,pInData,12);
	dbg("gl_SysInfo.ucHuabeiAmount:%s\n",gl_SysInfo.ucHuabeiAmount);	
	pInData += 12;

	vMemcpy0(gl_SysInfo.ucSwipeAmount,pInData,12);
	dbg("gl_SysInfo.ucSwipeAmount:%s\n",gl_SysInfo.ucSwipeAmount);	
	pInData += 12;

	vMemcpy0(gl_SysInfo.ucGprsAmount,pInData,12);
	dbg("gl_SysInfo.ucGprsAmount:%s\n",gl_SysInfo.ucGprsAmount);	
	pInData += 12;

	gl_SysInfo.ucGprsAllowFlag = *pInData - '0';
	dbg("gl_SysInfo.ucGprsAllowFlag:%d\n",gl_SysInfo.ucGprsAllowFlag);
	pInData += 1;

	gl_SysInfo.ucGprsPayFlag = *pInData - '0';
	dbg("gl_SysInfo.ucGprsPayFlag:%d\n",gl_SysInfo.ucGprsPayFlag);
	pInData += 1;

        gl_SysInfo.ucGprsUseFlag = *pInData - '0';
	dbg("gl_SysInfo.ucGprsUseFlag:%d\n",gl_SysInfo.ucGprsUseFlag);
	pInData += 1;
//	strcpy(pInData,"FQ821001000011020000220300002304000044050000550600004507000077080000881200007511000076");
	if(memcmp("FQ",pInData,2) == 0)
	{
		length = 0;
		count = 0;
		
		pInData += 2;
		length = (uint)ulA2L(pInData,2);
		dbg("length:%d\n",length);

		pInData += 2;
		vTwoOne(pInData, 2, &gl_SysInfo.ucHbfqCount);

		vOneTwo(&gl_SysInfo.ucHbfqCount, 1, tmp);
		count = (uint)ulA2L(tmp,2);
		dbg("count:%d\n",count);
		pInData += 2;
		if(count > 9)
			count = 9;	//最多9个分期
		for(i = 0;i<count;i++)
		{		
			vTwoOne(pInData, 8, &gl_SysInfo.ucHbfqInfo[i*4]);
			pInData += 8;
		}

		dbgHex("gl_SysInfo.ucHbfqInfo",gl_SysInfo.ucHbfqInfo,count*4);
		
		if(count)
			gl_SysInfo.ucHbfqFlag = 1;
	}
	uiMemManaPutSysInfo();

	return 0;
}

void DivPublicField060(u8 *pInData,int iLen)
{
        int i = 0,len =0;
	
        while(i < iLen)
        {
		if(*(pInData + i) == 0x01)
		{
			i++;
			len = *(pInData + i);
			i++;
			vMemcpy0(gl_SysInfo.ucMerchantQr,pInData + i,len);
			i += len;
			continue;
		}else if(*(pInData + i) == 0x02)
		{
			i++;
			len = *(pInData + i);
			i++;
			vMemcpy0(gl_SysInfo.ucMerchantMsg,pInData + i,len);
			i += len;
			continue;
		}

		i++;
		continue;
	}

        dbgHex("gl_SysInfo.ucMerchantQr", gl_SysInfo.ucMerchantQr, strlen(gl_SysInfo.ucMerchantQr));
	dbgHex("gl_SysInfo.ucMerchantMsg", gl_SysInfo.ucMerchantMsg, strlen(gl_SysInfo.ucMerchantMsg));	
        uiMemManaPutSysInfo();
	return ;
}

void DivPublicField061(u8 *pInData,int iLen, stQrTransRec *QrTrRec)
{
	uchar tagLen;
        int len;

       dbg("iLen:%d\n",iLen);
	dbgHex("pInData:", pInData,iLen);
	while(iLen > 0)
	{
		if ((*pInData & 0x0F) == 0x0F)
		{
			if(*pInData == 0xDF && *(pInData+1) == 0x27)
			{
				tagLen = pInData[2];
				len = tagLen;
				if(len > 48)
					vMemcpy0(QrTrRec->orderId,pInData + 3,48);
				else
					vMemcpy0(QrTrRec->orderId,pInData + 3,len);
				pInData += 3 + len;
				iLen  -= 3 + len;
				dbgHex("QrTrRec->orderId:", QrTrRec->orderId,len);
				continue;
			}
			else if (*pInData == 0xDF && *(pInData+1) == 0x28)
			{
				tagLen = pInData[2];
				len = tagLen;
				if(len > 200)
					vMemcpy0(QrTrRec->szchnlUrl,pInData + 3,200);
				else
					vMemcpy0(QrTrRec->szchnlUrl,pInData + 3,len);
				pInData += 3 + len;
				iLen  -= 3 + len;
				dbgHex("QrTrRec->szchnlUrl:", QrTrRec->szchnlUrl,len);
				continue;
			}
			else if (*pInData == 0xDF && *(pInData+1) == 0x29)
			{
				tagLen = pInData[2];
				len = tagLen;
				if(len > 48)
					vMemcpy0(QrTrRec->ucPayAccount,pInData + 3,48);
				else
					vMemcpy0(QrTrRec->ucPayAccount,pInData + 3,len);
				pInData += 3 + len;
				iLen  -= 3 + len;
				dbgHex("QrTrRec->ucPayAccount:",QrTrRec->orderMsg,len);
				continue;
			}
			else if (*pInData == 0xDF && *(pInData+1) == 0x30)
			{
				tagLen = pInData[2];
				len = tagLen;
				if(len > 48)
					vMemcpy0(QrTrRec->ucPayMethod,pInData + 3,48);
				else
					vMemcpy0(QrTrRec->ucPayMethod,pInData + 3,len);
				pInData += 3 + len;
				iLen  -= 3 + len;
				dbgHex("QrTrRec->ucPayMethod:",QrTrRec->ucPayMethod,len);
				continue;
			}
			else if (*pInData == 0xDF && *(pInData+1) == 0x31)
			{
				tagLen = pInData[2];
				len = tagLen;
				if(len > 19)
					vMemcpy0(QrTrRec->ucPayCard,pInData + 3,19);
				else
					vMemcpy0(QrTrRec->ucPayCard,pInData + 3,len);
				pInData += 3 + len;
				iLen  -= 3 + len;
				dbgHex("QrTrRec->ucPayCard:",QrTrRec->ucPayCard,len);
				continue;
			} 
			else if (*pInData == 0xDF && *(pInData+1) == 0x32)
			{
				tagLen = pInData[2];
				len = tagLen;
				if(len > 10)
					vMemcpy0(QrTrRec->issuerBankId,pInData + 3,10);
				else
					vMemcpy0(QrTrRec->issuerBankId,pInData + 3,len);
				pInData += 3 + len;
				iLen  -= 3 + len;
				dbgHex("QrTrRec->issuerBankId:",QrTrRec->issuerBankId,len);
				continue;
			}
			else if (*pInData == 0xDF && *(pInData+1) == 0x33)
			{
				tagLen = pInData[2];
				len = tagLen;
				if(len > 10)
					vMemcpy0(QrTrRec->recvBankId,pInData + 3,10);
				else
					vMemcpy0(QrTrRec->recvBankId,pInData + 3,len);
				pInData += 3 + len;
				iLen  -= 3 + len;
				dbgHex("QrTrRec->recvBankId:",QrTrRec->recvBankId,len);
				continue;
			}
			else if (*pInData == 0xDF && *(pInData+1) == 0x34)
			{
				tagLen = pInData[2];
				len = tagLen;
				if(len > 10)
					vMemcpy0(QrTrRec->ucPayVoucher,pInData + 3,10);
				else
					vMemcpy0(QrTrRec->ucPayVoucher,pInData + 3,len);
				pInData += 3 + len;
				iLen  -= 3 + len;
				dbgHex("QrTrRec->ucPayVoucher:",QrTrRec->ucPayVoucher,len);
				continue;
			}else
			{
				tagLen = pInData[2];
				len = tagLen;
				pInData += 3 + len;
				iLen  -=3 + len;
				continue;
			}
		}else
               {
               			tagLen= pInData[1];
				len = tagLen;
				pInData += 2 + len;
				iLen  -= 2 + len;
				continue;
               	}
		/*
		 if (memcmp(pInData,"\xDF\x27",2) == 0)
		{
			tagLen = pInData[2];
			len = tagLen;
			memcpy(QrTrRec->orderId,pInData + 3,len);
			pInData += 3 + len;
			iLen  -= 3 + len;
			dbgHex("QrTrRec->orderId:", QrTrRec->orderId,len);
			continue;
		}
		else if (memcmp(pInData,"\xDF\x28",2) == 0)
		{
			tagLen = pInData[2];
			len = tagLen;
			memcpy(QrTrRec->szchnlUrl,pInData + 3,len);
			pInData += 3 + len;
			iLen  -= 3 + len;
			dbgHex("QrTrRec->szchnlUrl:", QrTrRec->szchnlUrl,len);
			continue;
		}
		else if (memcmp(pInData,"\xDF\x29",2) == 0)
		{
			tagLen = pInData[2];
			len = tagLen;
			memcpy(QrTrRec->ucPayAccount,pInData + 3,len);
			pInData += 3 + len;
			iLen  -= 3 + len;
			dbgHex("QrTrRec->ucPayAccount:",QrTrRec->orderMsg,len);
			continue;
		}
		else if (memcmp(pInData,"\xDF\x30",2) == 0)
		{
			tagLen = pInData[2];
			len = tagLen;
			memcpy(QrTrRec->ucPayMethod,pInData + 3,len);
			pInData += 3 + len;
			iLen  -= 3 + len;
			dbgHex("QrTrRec->ucPayMethod:",QrTrRec->ucPayMethod,len);
			continue;
		}
		else if (memcmp(pInData,"\xDF\x31",2) == 0)
		{
			tagLen = pInData[2];
			len = tagLen;
			memcpy(QrTrRec->ucPayCard,pInData + 3,len);
			pInData += 3 + len;
			iLen  -= 3 + len;
			dbgHex("QrTrRec->ucPayCard:",QrTrRec->ucPayCard,len);
			continue;
		} 
		else if (memcmp(pInData,"\xDF\x32",2) == 0)
		{
			tagLen = pInData[2];
			len = tagLen;
			memcpy(QrTrRec->issuerBankId,pInData + 3,len);
			pInData += 3 + len;
			iLen  -= 3 + len;
			dbgHex("QrTrRec->issuerBankId:",QrTrRec->issuerBankId,len);
			continue;
		}
		else if (memcmp(pInData,"\xDF\x33",2) == 0)
		{
			tagLen = pInData[2];
			len = tagLen;
			memcpy(QrTrRec->recvBankId,pInData + 3,len);
			pInData += 3 + len;
			iLen  -= 3 + len;
			dbgHex("QrTrRec->recvBankId:",QrTrRec->recvBankId,len);
			continue;
		}
		else if (memcmp(pInData,"\xDF\x34",2) == 0)
		{
			tagLen = pInData[2];
			len = tagLen;
			memcpy(QrTrRec->ucPayVoucher,pInData + 3,len);
			pInData += 3 + len;
			iLen  -= 3 + len;
			dbgHex("QrTrRec->ucPayVoucher:",QrTrRec->ucPayVoucher,len);
			continue;
		}

		pInData ++;
		iLen --;
		continue;
		*/
	}		
}
