#ifndef _TRANS_SDJ_H
#define _TRANS_SDJ_H

#define COMPANY_MARK		"000073"    //�ն˳��ұ�ʶ
#define SUBVERSION			"01"
#define SUBVERSION_G		"52"


//显示服务端错误信�?
void vShowHostErrMsg(uchar *pszHostErrCode, uchar *pszHostErrMsg);

//int iHttpSndRcv8583Msg(st8583 *pSnd8583, st8583 *pRcv8583,uchar ucRevFlag);
int iHttpSendRecv8583(uchar ucRevFlag);

//签到
int iPosLogin(uchar ucDispMsg);

int iPosLogout(void);

int iOnlineSettle(void);

void vClearIcParam(uchar flag);

//更新公钥
int iDownloadCAPK(void);

//下载IC交易参数(Aid)
int iDownloadEmvParam(void);

//pszPan为null�? pszEncPin输出为明文pin，否则输出密文pin
// -1：取�?-3：超�?>=0:密码长度(0为直接按确认�?
int iInputPinAndEnc(uchar ucPass, unsigned long ulAmount, char *pszPrompt, char *pszPan, char *pszEncPin);

//检查交易状�?
int iCheckTranStat(uint uiTransType, uchar ucMode);
int iCardTrans(uint uiTransType);
void vPrtTransRecInfo(void);
int iPackReq8583(st8583 *pSend8583, char *sCommBuf, uint *puiLen);
int iUnPackRsp8583(char *sCommBuf, uint uiRecvLen, st8583 *pRecv8583);

//预授权撤销、预授权完成、退�?需要输入原交易信息
int iTrans2(uint uiTransType);
int iTransVoid(uint uiTransType);

//int iMagTrans(uint uiTransType, int iEntry, int iVoidRecIdx, ulong ulVoidTTC);
int iUploadOfflineTrans(void);
void vPrt8583Fields(char ucType, st8583 *pMsg8583);
int iImportTmkfromMainPos(void);
//int iUploadSignFile(int envFlag, int iIdx);
void vGetPrtMerchInfo(uchar *psFields, stTransRec *rec);
extern int iUploadComfirmSaleTrade(void);
extern void GetExpiredData(u8 asTrack2Data[],u8 asExpiredDate[]);
#endif
