#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "user_projectconfig.h"

#include "VposFace.h"
#include "pub.h"
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "test_menu.h"
#include "myHttp.h"
//#include "TMS.h"
#include "tcpcomm.h"
#include "func.h"
#include "menu_cjt.h"
#include "debug.h"

#if defined(PROJECT_CY21) || defined(PROJECT_CY80)
#undef _KEY_FN
#define _KEY_FN _KEY_UP
#endif

#define B_posTophone					1		//被扫(机器扫手机)
#define Z_phoneTopos					2		//主扫(手机扫机器)

#ifdef USE_LTE_4G
#define NetName	"4G"
#else
#define NetName	"gprs"
#endif

extern int iCardTrans(uint uiTransType);
extern int iTermActive(void);
extern int iQrPayMaster(int type);
extern int iQrPay(int type);
extern int iQrPayVoid(int payType);
extern int iTransVoid(uint uiTransType);
extern void vSetExitToMainFlag(char flag);
extern int iTrans2(uint uiTransType);
extern int iDoOperPause(void *p);
extern int mFacTpAdjust(void *m);
extern unsigned char iSaleHb(void);
extern uchar all_para_down_check(void);
extern int iPrintQrTrans(stQrTransRec *QrRec, int dup);
extern void vMessageMulEx(char *pszMessage);
extern int iOpenGprsMessage(void);
extern int iDoPosLogin(void);
extern int iOfflineSettle(void);
extern uint iResetOperPwd(void);
extern int iPrintSettle(stSettleInfo *sett,uchar ucPrintSettleFlag);
extern void vClearWilrelessIcon(void);
extern int iSetCommParam(void);

int mViewTermInfo(int flag);
int mViewTermInfo1(void *p);
int mViewTermInfo2(void *p);
int iFactorySimpleTest(void);
int mInitAllParam(void *p);
extern int iCalUseSpace(void *p);

int iDoSystemPwd(void *p);
int iDoSafePwd(void * p);
int iDoResetMainOperPwd(void *p);

void TermRemindInfo(void);
void TermRemindUseInfo(void);
int mCommConfig(void *p);
int mTpduSet(void *p);
int mCommConfig(void *p);
int mOtherCommConfig(void *p);
int  mSetCommFunc(void);
int iTpduParamSet(void);
int mSetCommMana(void *p);
int mSetCommOther(void *p);
int mTransPasswordSwitch(void *p);
int mTransCtrolSwtich(void *p);
int mQrTransCtrolSwtich(void *p);
int mTransBrushSwitch(void *p);
int mTransSettleSwitch(void *p);
int mTransOtherSwitch(void *p);
int mRemoteUpdate(void *p);
int SetNoPinParam(void);
int SetNoSignParam(void);
int SetSmParam(void);

extern int iTermViewParam(void);
static int iDoOperAdd(void *p);
static int iDoOperDel(void *p);
static int iDoOperView(void *p);
static int iDoOperChangePwd(void *p);
extern int iDoChangeOperPwd(void);
extern int iUploadSignFile(int envFlag, int iIdx, stTransRec *recIn);
extern int iTermPrintParam(void);
extern int iPosLogin(uchar ucDispMsg);
extern int iDoOperLogin(void);
extern int iPosLogout(void);
extern int iOnlineSettle(void);
extern int iDownloadCAPK(void);
extern int iDownloadEmvParam(void);
extern int iDownLoadBIN(void);
extern int iDownLoadBackBIN(char * date,char * cardno);
extern int iViewTransList(void);
extern int iViewTransSum(void);
extern int iViewTransByTTC(void);
extern int iViewTransByCardNo(void);
int SetCommOther(void);
extern void vInputTMK(void);
extern void vPrtParam(void);
extern unsigned char iQRCodePay(u8 nMode);
extern int iMainOperChgPwd(void);
extern int iSetPosParam(void);
extern int iSetTransParam(void);
int iSetAllCommParam(void);

extern int gprs_at_testgprssleep(void);
extern int iTestCloseGprs(void);
extern int iTestGprsInit(void);
extern int iTestPowerOffGprs(void);
extern int iTestGprsAct(void);
extern int iDownMK(void);
SMenu menu_QrCode ;
extern int mConfig(void *p);
extern int mTusnKey(void *p);
extern int iCheckIpFormat(char *pszIP);
extern int iTmsProc(int cmd, int dispflag);

//手机
int mMobilePay(void *p)
{
	 if(gl_SysInfo.ucSaleSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }

	gl_ucProcReqFlag = 1;	
	gl_ucMobileTransFlag = 1;
	iCardTrans(TRANS_TYPE_MOBILESALE);
//	all_para_down_check();	
	return 0;
}

//扫码
int mSaleQr(void *p)
{      
       dbg("ucDepositFlag:%d  ucActivityFlag:%d\n",gl_SysInfo.ucDepositFlag,gl_SysInfo.ucActivityFlag);
        _vCls();
       if(gl_SysInfo.ucDepositFlag == 0 || gl_SysInfo.ucActivityFlag == 0)
       {
       		vMessageMul("请先使用刷卡交易激活设备");
		return -1;			
       }
			   
	return EnterTestMenu(&menu_QrCode);	
}

//花呗
int mSaleHb(void *p)
{	
        dbg("ucDepositFlag:%d  ucActivityFlag:%d\n",gl_SysInfo.ucDepositFlag,gl_SysInfo.ucActivityFlag);
	_vCls();
       if(gl_SysInfo.ucDepositFlag == 0 || gl_SysInfo.ucActivityFlag == 0)
       {
       		vMessageMul("请先使用刷卡交易激活设备");
		return -1;			
       }
	   
	return iSaleHb();
}

//消费
int mSale(void *p)
{		
        if(gl_SysInfo.ucSaleSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }

	gl_ucProcReqFlag = 1;	
	gl_ucMobileTransFlag = 0;
	iCardTrans(TRANS_TYPE_SALE);
//	all_para_down_check();	
	return 0;
}

//撤销
int mSaleVoid(void *p)
{		
	 if(gl_SysInfo.ucVoidSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }
	 
	 iTransVoid(TRANS_TYPE_SALEVOID);
	 vTakeOutCard();
	 return 0;
}

//退货
int mRefund(void *p)
{   	
	if(gl_SysInfo.ucRefundSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }
		 
	iTrans2(TRANS_TYPE_REFUND);
	vTakeOutCard();
	return 0;
}

//其他-插卡消费
int mIcSale(void *p)
{	
	if(gl_SysInfo.ucSaleSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }

	gl_ucProcReqFlag = 1;		
	iCardTrans(TRANS_TYPE_ICSALE);
	all_para_down_check();	
	return 0;
}

//其他-闪付凭密
int mQpbocPinSale(void *p)
{	
       	if(gl_SysInfo.ucSaleSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }

	gl_ucProcReqFlag = 1;		
	iCardTrans(TRANS_TYPE_NFCPINSALE);
	gl_ucNFCPinFlag=0;
	all_para_down_check();	
	return 0;
}

int mQpbocPreAuth(void *p)
{	
         if(gl_SysInfo.ucAuthSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }
				
	iCardTrans(TRANS_TYPE_NFCPINPREAUTH);
	gl_ucNFCPinFlag=0;
	return 0;
}


//其他-余额查询
int mQueryBal(void *p)
{	
         if(gl_SysInfo.ucBalanceSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }

	gl_ucMobileTransFlag = 0;	 
	return iCardTrans(TRANS_TYPE_BALANCE);
}

//失败电签-查看失败电签
int mViewFailTrans(void *p)
{
	int i, n, num;
	stTransRec rec;
	uchar szBuf[40];
	uint uiKey;
	int step;
	ulong ulTimer;
    //int iNeedSign=0;

	_vCls();
	vDispCenter(1, "查看失败电签", 1);
	num=0;
	for(i=0; i<gl_SysData.uiTransNum; i++)
	{
		uiMemManaGetTransRec(i, &rec);
		if(rec.uiTransType>=0x8000)
			continue;
		if(rec.ucSignupload==0xFF)
			continue;
        if(rec.ucSignupload>3)	//多次失败的不再显示
			continue;        
        //if(rec.ucTransAttr&0x02 && rec.ucTransAttr&0x04)
        //    iNeedSign++;
		num++;
	}
	if(num==0)
	{
		vMessage("无失败电签记录");
		return 0;
	}
	n=0;
	step=-1;
	for(i=gl_SysData.uiTransNum-1; i<gl_SysData.uiTransNum && i>=0; i+=step)
	{
		uiMemManaGetTransRec(i, &rec);
		if(rec.uiTransType>=0x8000)
			continue;
		if(rec.ucSignupload==0xFF)
			continue;
		if(rec.ucSignupload>3)	//多次失败的不再显示
			continue;
        
        vGetBankCardTransName(rec.uiTransType, rec.ucVoidFlag, szBuf);
    #ifdef ST7789
		vDispVarArg(2, "%d/%d %s", n+1, num, szBuf);
		vDispVarArg(3, "金额:%lu.%02lu", rec.ulAmount/100, rec.ulAmount%100);
		vDispVarArg(4, "凭证号:%06lu", rec.ulTTC);
		vUnpackPan(rec.sPan, szBuf);
		if(strlen(szBuf)>10)
			memset(szBuf+5, '*', strlen(szBuf)-10);
		vDispVarArg(5, "卡号:%s", szBuf);
		vOneTwo0(rec.sDateTime, 6, szBuf);
		vDispVarArg(6, "时间:20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", szBuf, szBuf+2, szBuf+4, szBuf+6, szBuf+8, szBuf+10);
     #else
		vDispVarArg(2, "%d/%d %s", n+1, num, szBuf);
		sprintf(szBuf, "%lu.%02lu              ", rec.ulAmount/100, rec.ulAmount%100);
        sprintf(szBuf+_uiGetVCols()-10, "凭证%06lu", rec.ulTTC);
		_vDisp(3, szBuf);
		vUnpackPan(rec.sPan, szBuf);
		if(strlen(szBuf)>10)
			memset(szBuf+5, '*', strlen(szBuf)-10);
		vDispVarArg(4, "%s", szBuf);
		vOneTwo0(rec.sDateTime, 6, szBuf);
		vDispVarArg(5, "20%.2s-%.2s-%.2s %.2s:%.2s:%.2s", szBuf, szBuf+2, szBuf+4, szBuf+6, szBuf+8, szBuf+10);        
     #endif
		if(num>1)
			vDispCenter(_uiGetVLines(), "[取消]退出 [↑↓]翻页", 0);
		else
        {
            memset(szBuf, 0, sizeof(szBuf));
            memset(szBuf, ' ', strlen("[取消]退出 [↑↓]翻页"));
            memcpy(szBuf, "[取消]退出", strlen("[取消]退出"));
			vDispCenter(_uiGetVLines(), (char*)szBuf, 0);
        }
		_vSetTimer(&ulTimer, 30*100);	//30sec
		while(1)
		{
			if(_uiKeyPressed())
			{
				uiKey=_uiGetKey();
				if(uiKey==_KEY_UP && n>0 && i<gl_SysData.uiTransNum-1)
				{
					n--;
					step=1;
					break;
				}
				if(uiKey==_KEY_DOWN && n<num-1 && i>0)
				{
					n++;
					step=-1;
					break;
				}
				if(uiKey==_KEY_ESC)
					return 0;
				_vSetTimer(&ulTimer, 30*100);	//30sec
				continue;
			}
			if(_uiTestTimer(ulTimer))
				return 1;
		}
		continue;
	}
	return 0;
}

//电签POS签名不允许取消,可以超时(3分钟)
//超时的电签在失败电签里可以补签
//签名上送不提示成功失败，失败电签上送时提示成功失败
//结算时上送失败电签，不处理未签名的交易，签名上送失败不提示不影响继续结算
//mode:1-需补签 0-不需补签(结算)
int iSendFailSignOper(int mode, int *piNum)
{
    int i, num,flag;
	stTransRec rec;
    int ret=0;

    if(gl_SysData.ucFailSignNum==0)
    {
        if(piNum)
            *piNum=0;
        return 0;
    }
		
    if(mode!=0)
        vDispCenter(1, "上送失败电签", 1);

    flag=0;
    //上送失败电签
    for(i=0, num=0; i<gl_SysData.uiTransNum && num<gl_SysData.ucFailSignNum; i++)
    {
        uiMemManaGetTransRec(i, &rec);
        if(rec.ucSignupload==0xFF)
            continue;
        if(rec.ucSignupload>3)	//失败多次的,不再上送
            continue;		
        
        if(rec.ucUploadFlag!=0xFF)      //脱机交易无签名
            continue;

	flag=1;
	
        rec.ucSettSignupload = 1;
	 uiMemManaPutTransRec(i,&rec);	
	 
#if 0
        if((rec.ucTransAttr&0x02) && (rec.ucTransAttr&0x04))
        {
            dbgLTxt("SendFailSign %d, rec->ucTransAttr=%02X\n", i, rec.ucTransAttr);
            if(mode)
            {
                vClearLines(2);
                iBillSign(0, i, &rec);
                vClearLines(2);
                if(rec.ucTransAttr&0x04)
                    continue;
            }else
                continue;
        }
 #endif
 
        if(mode==0 && num==0)
            vDispCenter(1, "上送失败电签", 1);        //mode=0只有需要上送的时候才显示标题
        
        vDispVarArg(2, "上送第%d笔签名...",  num+1);
        vClearLines(3);
     
        dbgLTxt("iUploadSignFile %d, rec->ucTransAttr=02X\n", i, rec.ucTransAttr);
        ret=iUploadSignFile(1, i, &rec);
	gl_ucSignuploadFlag = 0;
        if(ret==-99)
            break;
        num++;
    }
    if(piNum)
        *piNum=num;

	    if(flag==0)
    {
        gl_SysData.ucFailSignNum=0;     //重置有效失败电签笔数为0
        uiMemManaPutSysInfo();
    }
    
    return ret;
}

//失败电签-上送失败电签
int mSendFailTrans(void *p)
{
	int ret, num;

	_vCls();
	vDispCenter(1, "上送电签签名", 1);
	ret=iSendFailSignOper(1, &num);
	if(ret==0 && num==0)
	{
		vMessage("无失败电签记录");
		return 0;
	}
	return 0;
}

//终端参数打印
int mTermPrintParam(void *p)
{
	return iTermPrintParam();
}

//终端参数显示
int mTermViewParam(void *p)
{
	return iTermViewParam();
}

//设置打印张数
int mSetPrintNum(void *p)
{
	int ret;
        uchar ucPinPos;
	char buf[50];		

	_vCls();
	vDispCenter(1, "打印张数设置:",1);	
	_vDisp(2, "交易打印张数(1-3):");	
	memset(buf,0x00,sizeof(buf));
	sprintf(buf, "%d", gl_SysInfo.ucPrinterAttr);
	ucPinPos = _uiGetVCols() - 1;	
	while(1)
	{
		ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT,3,ucPinPos, buf, 1, 0, 1, NULL);
		if(ret<0)
			return -1;
		if(ret == 0 ) break;		
		if(buf[0] < '1' || buf[0] > '3') continue;		
		if( gl_SysInfo.ucPrinterAttr  != (buf[0] - '0'))
		{      
			gl_SysInfo.ucPrinterAttr = buf[0] - '0';
			uiMemManaPutSysInfo();
			break;
		}
		break;
	}

	return 0;	
}

//管理-签到
int mTermLogin(void *p)
{
	iDoPosLogin();	
	return 0;
}

//操作员签到
int mOperLogin(void *p)
{
	return iDoOperLogin();
}

int mPosLogout(void *p)
{
	return iPosLogout();
}

//管理-结算
int mSettle(void *p)
{
	return iOfflineSettle();
}

int mSetWifi(void *p)
{
	 return 0;
}

int mResetOperPwd(void *p)
{
	iResetOperPwd();
	return 0;
}

int mDownloadCAPK(void *p)
{
	return iDownloadCAPK();
}

int mDownloadEmvParam(void *p)
{
	return iDownloadEmvParam();
}

int mDownloadBIN(void *p)
{
	return iDownLoadBIN();
}

//int mDownloadBackBIN(void *p)
//{
//	return iDownLoadBackBIN();
//}

int mSetNFCTransPrior(void *p)
{
    int ret, curr;
    
    _vCls();
    vDispCenter(1, "优先挥卡交易", 1);
    curr= (gl_SysInfo.ucNFCTransPrior == 1)? 1:2;
    ret=iSelectOne(NULL, "1.打开挥卡优先", "2.关闭挥卡优先", "", curr, 30, 1);	
    if(ret<0)
		return 0;	
  if(ret  !=  curr)
    {
        ret = (ret == 1) ? 1 : 0;
        gl_SysInfo.ucNFCTransPrior=ret;
        uiMemManaPutSysInfo();
    }

    return 0;	
}

int mQueryTransList(void *p)
{ 
	iViewTransList();
	return 0;
}

int mQueryTransSum(void *p)
{
	return iViewTransSum();
}

int mQueryTransByInvoiceNo(void *p)
{
	return iViewTransByTTC();
}

int mQueryTransByCardNo(void *p)
{
	return iViewTransByCardNo();
}

//管理-交易查询
SMenu menu_QueryTrans =
{"交易查询", 0, 1, 0, 
	{
		{"查询交易明细", mQueryTransList, 0}, 		
		{"查询交易汇总", mQueryTransSum, 0}, 
		{"按卡号查询",       mQueryTransByCardNo, 0}, 
		NULL
	}
};

//管理-下载
SMenu menu_ICDownload =
{"非接专用菜单", 0, 1, 0, 
	{		
		{"IC卡参数下载", mDownloadEmvParam, 0}, 
		{"IC卡公钥同步", mDownloadCAPK}, 
	//	{"免密新增BIN表B下载", mDownloadBIN}, 
	//	{"免密BIN表黑名单下载",mDownloadBackBIN},
		{"优先挥卡交易开关", mSetNFCTransPrior,0},
		NULL
	}
};

SMenu menu_CommParamSet =
{"参数设置", 0, 1, 0, 
	{
		{"TPDU", mTpduSet, 0}, 
		{"通讯设置", mSetCommMana, 0},
		{"按键音设置", mSetCommOther,0},
		NULL
	}
};

SMenu menu_OperManage =
{"柜员管理", 0, 1, 0, 
	{
		{"增加操作员", iDoOperAdd}, 
		{"删除操作员", iDoOperDel}, 
		{"查看操作员", iDoOperView}, 
		{"操作员改密", iDoOperChangePwd}, 		
		NULL
	}
};

int mCommConfig(void *p)
{
    return iCommConfig();
}

int mSetCommOther(void *p)
{
       return SetCommOther();   	
}

int mTestGprSleep()
{
	gprs_at_testgprssleep();
	return 0;
}

int mRegGprs()
{
    
	gprs_at_RegApp();
	return 0;
}
int mActGprs()
{
        
	iTestGprsAct();
	return 0;
}
int mCloseGprs()
{
    
	iTestCloseGprs();
	return 0;
}
int mInitGprs()
{
    
	iTestGprsInit();
	return 0;
}
int mPowerOffGprs()
{
    
	iTestPowerOffGprs();
	return 0;
}

extern int gprs_at_MobileId(char* pszMobileId);
int mReadMobileId()
{
	char szMobileId[20]={0};

	gprs_at_MobileId(szMobileId);
	_vDisp(6, szMobileId);
	_uiGetKey();
	return 0;
}

int mGprsIMSI()
{
    char imsi[50];
    
    imsi[0]=0;
	gprs_at_IMSI(imsi);
    vDispVarArg(4, "imsi:%s.", imsi);
    _uiGetKey();
    return 0;
}

int mGprsVer()
{
	char ver[100];

    ver[0]=0;
    _vCls();
	gprs_at_gmr(ver, sizeof(ver)-1);
    vDispVarArg(4, "ver:%s", ver);
    _uiGetKey();
    return 0;
}

int mGprsAT()
{
    int ret;
    _vCls();
	ret=gprs_at_MoudleIsActive(1);
    vDispVarArg(4, "AT\\r ret:%d", ret);
    _uiGetKey();
    return 0;
}

int mGprspower()
{
    _vCls();
	gprs_poweron();
    _uiGetKey();
    return 0;
}
int mGprsreset()
{
    _vCls();
	gprs_modulepin_reset();
    _uiGetKey();
    return 0;
}
int mGprsATE0()
{
    int ret;
    _vCls();
	ret=gprs_at_DisableEcho();
    vDispVarArg(4, "ATE0\\r ret:%d", ret);
    _uiGetKey();
    return 0;
}
int mGprsimei()
{
    char imei[30];
    imei[0]=0;
    _vCls();
	gprs_at_imei(imei);
    vDispVarArg(4, "imei:%s", imei);
    _uiGetKey();
    return 0;
}

extern int iTestGprsLoc(void *p);
extern int iTestSim(void *p);

#if 0
//管理
SMenu menu_GPRS =
{"GPRS", 0, 1, 2, 
	{
		//{"gprs状态", mTestGprSleep, 0}, 
		//{"gprs RegAPP", mRegGprs, 0}, 
		//{"gprs ACT", mActGprs, 0}, 
        {"sim poweron", mGprspower, 0},
        {"sim reset", mGprsreset, 0},
        {"sim AT", mGprsAT, 0},
        {"sim ATE0", mGprsATE0, 0},
		//{"gprs关闭", mCloseGprs, 0}, 
		//{"gprs下电", mPowerOffGprs, 0},
		{"gprs初始化", mInitGprs, 0},
        {"gprs IMEI", mGprsimei, 0},
		{"gprs位置", iTestGprsLoc, 0},
		//{"sim卡号", mReadMobileId, 0},
		{"gprs ver", mGprsVer, 0},
		{"sim IMSI", mGprsIMSI, 0},
		
		//{"sim ", iTestSim, 0},
		NULL
	}
};
#endif

//管理
SMenu menu_Login =
{"签  到", 0, 1, 1, 
	{
		{"pos签到", mTermLogin, 0}, 
//		{"操作员签到", mOperLogin, 0}, 
		NULL
	}
};
extern int mGetBattery(void *p);

int mSetAutoPowerOff(void *p)
{
	int ret, curr;
	
	_vCls();
	vDispCenter(1, "自动关机设置", 1);	
	
	curr= (gl_SysInfo.ucPowerOffSwitch== 0)? 1:0;
	ret=iSelectOne("是否支持自动关机", "1.支持", "0.不支持", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if( ret!= curr)
	{
	    ret = (ret == 0) ? 1 : 0;
	    gl_SysInfo.ucPowerOffSwitch=ret;
	    uiMemManaPutSysInfo();
		
		if(gl_SysInfo.ucPowerOffSwitch == 0)
			_vSetAutoPowerOff(30*60);//秒为单位
		else if(gl_SysInfo.ucPowerOffSwitch == 1)
			_vSetAutoPowerOff(0);//秒为单位	
	}
}

//管理
SMenu menu_Manage =
{"管  理", 0, 1, 2, 
	{
		{"签  到", 0, &menu_Login}, 
		{"结  算", mSettle},
		{"交易查询", 0, &menu_QueryTrans}, 
		{"版  本", mViewTermInfo1}, 
		{"参数设置", mConfig}, 
#ifdef TMS_PROC		
		{"远程更新",mRemoteUpdate},
#endif
		{"关机设置",mSetAutoPowerOff},
		NULL
	}
};

int mInputTmk(void *p)
{
	vInputTMK();
	return 0;
}

/*
void vTermInfoPage1(void)
{
	char szBuf[50];
	int line;

	//vDispCenter(1, "终端信息", 0);
	vClearLines(6);
	
	line=1;
    _vDisp(line++, "TUSN:");
	_uiGetSerialNo((uchar*)szBuf);
	_vDisp(line++, szBuf);
    
	szBuf[0]=0;
	_vDisp(line++, "IMEI:");
	iGetIMEI(szBuf);
	_vDisp(line++, szBuf);
    
	szBuf[0]=0;
	_vDisp(line++, "ICCID:");
	iGetICCID(szBuf);
	_vDisp(line++, szBuf);
}

void vTermInfoPage2(void)
{
	char szBuf[50];
	int line;

	vClearLines(6);
	line=1;
	
	readMainBoardSN(szBuf);
	vDispVarArg(line++, "主板:%s", szBuf);
    _pszGetAppVer(szBuf);
    vDispVarArg(line++, "软件版本:%s", szBuf);
	vDispVarArg(line++, "参数版本:%s", gl_SysInfo.szParamVer);
	vDispVarArg(line++, "商户号%s", gl_SysInfo.szMerchId);
	vDispVarArg(line++, "终端号:%s", gl_SysInfo.szPosId);
    vDispVarArg(line++, "%s %s", __DATE__, __TIME__);
}


#define FACTORY_KEY		"98"
extern int endlessloop(void);
extern void vDispTermQrCode(int iFlag);
int mViewTermInfo(int flag)
{
	int line;
	char szBuf[50];
	ulong ultimer;
	uint uiKey;
    char bCheck;
    int page=0;

	_vCls();
#if 1
	page=1;
	vTermInfoPaye1();
#else
	line=1;
    _uiGetSerialNo((uchar*)szBuf);
    vDispVarArg(line++, "TUSN:%s", szBuf);
    
	szBuf[0]=0;
	iGetIMEI(szBuf);		
	vDispVarArg(line++, "IMEI:%s", szBuf);
    
    szBuf[0]=0;
    iGetICCID(szBuf);
    vDispVarArg(line++, "ICCID:%s", szBuf);

    _pszGetAppVer(szBuf);
    vDispVarArg(line++, "软件版本:%s", szBuf);

	vDispVarArg(line++, "参数版本:%s", gl_SysInfo.szParamVer);
	vDispVarArg(line++, "商户号:%s", gl_SysInfo.szMerchId);
	vDispVarArg(line++, "终端号:%s", gl_SysInfo.szPosId);
    vDispVarArg(line++, "  %s %s", __DATE__, __TIME__);
#endif

	//3秒内按功能键+"9898"进入装载终端序号界面
	_vFlushKey();
	uiKey=0;
    bCheck=0;
	memset(szBuf, 0, sizeof(szBuf));
	_vSetTimer(&ultimer, 30*100);
	while (!_uiTestTimer(ultimer))
	{
		if(_uiKeyPressed())
		{
            uiKey=_uiGetKey();
            if(flag==1 && uiKey==_KEY_FN && szBuf[0]==0)
            {
                //memset(szBuf, 0, sizeof(szBuf));
                bCheck=1;
                continue;
            }
            if(bCheck==0 && (uiKey==_KEY_DOWN || uiKey==_KEY_UP))
            {
                _vSetTimer(&ultimer, 30*100);
                if(page==3 && uiKey==_KEY_DOWN)
                    continue;
                if(page==1 && uiKey==_KEY_UP)
                    continue;
                if(uiKey==_KEY_DOWN)
                {
                    page++;
                    
                }else if(uiKey==_KEY_UP)
                {
                    page--;
                }
                
                if(page==1)
                    vTermInfoPage1();
                else if(page==2)
                    vTermInfoPage2();
                else
                    vDispTermQrCode(0);
                continue;
            }
            
            if(bCheck==0)
                break;
            
            szBuf[strlen(szBuf)]=uiKey;
            if(memcmp(FACTORY_KEY, szBuf, strlen(szBuf)))
            {
                break;
            }
            if(strcmp(FACTORY_KEY, szBuf)==0)
            {
                iFactorySimpleTest();
                break;
            }
		}
	}
	return 0;
}
*/
int mViewTermInfo1(void *p)
{
#ifdef ENABLE_PRINTER 
//    iTermPrintAppVer();	
#endif

    return mViewTermInfo(1);
}
/*
int mViewTermInfo2(void *p)
{
    //return mViewTermInfo(2);
    vDispTermQrCode(0);
    return 0;
}
*/

SMenu menu_QpbocPin =
{"闪付凭密", 0, 1, 0, 
	{

		{"消费凭密", mQpbocPinSale}, 
		{"预授权凭密", mQpbocPreAuth}, 
		NULL
	}
};

SMenu menu_Other =
{"其  他", 0, 1, 0, 
	{
		{"余额查询", mQueryBal}, 
//		{"电子现金", mEcSale}, 	
                
		{"插卡消费", mIcSale}, 
		{"闪付凭密", 0,&menu_QpbocPin}, 
		NULL
	}
};

int mTpduSet(void *p)
{
	return iTpduParamSet();
}

int mSetCommMana(void *p)
{
    return mSetCommFunc();
}

int mImportKeyFromIC(void *p)
{
	_vCls();
	vDispCenter(1, "IC卡导主密钥", 1);
	vMessage("暂不支持");
	return 0;
}

int mImportKeyFromPos(void *p)
{
    
//	iImportTmkfromMainPos();
//	return 0;
	_vCls();
	vDispCenter(1, "母POS导主密钥", 1);
	vMessage("暂不支持");
	return 0;
}

int mPrintParam(void)
{
	vPrtParam();
	return 0;
}

SMenu menu_ImportKey =
{"密钥下装", 0, 1, 0, 
	{
		{"IC卡导主密钥",  mImportKeyFromIC }, 
		{"母POS导主密钥", mImportKeyFromPos }, 
		{"手输主密钥",  mInputTmk },
		NULL
	}
};

//预授权
int mPreAuth(void *p)
{	
	if(gl_SysInfo.ucAuthSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }
	
	return iCardTrans(TRANS_TYPE_PREAUTH);
}

int mPreAuthVoid(void *p)
{		
	if(gl_SysInfo.ucAuthCancelSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }
	
	iTrans2(TRANS_TYPE_PREAUTHVOID);
	vTakeOutCard();
	return 0;
}

int mPreAuthComp(void *p)
{	
	if(gl_SysInfo.ucAuthCompleteSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }

	gl_ucProcReqFlag = 1;	
	iTrans2(TRANS_TYPE_PREAUTH_COMP);
	all_para_down_check();	
	vTakeOutCard();
	return 0;
}

int mPreAuthCompVoid(void *p)
{	
	if(gl_SysInfo.ucAuthCompleteVoidSwitch == 0) 
	{
		TermRemindInfo();
		return -1;
        }
	
	iTransVoid(TRANS_TYPE_PREAUTH_COMPVOID);
	vTakeOutCard();
	return 0;
}

SMenu menu_CardPreAuth =
{"银行卡预授权", 0, 1, 0, 
	{
		{"预授权",  mPreAuth }, 
		{"预授权撤销", mPreAuthVoid }, 
		{"预授权完成", mPreAuthComp },  
		{"预授权完成撤销", mPreAuthCompVoid },  
		NULL
	}
};

SMenu menu_PreAuth =
{"预授权", 0, 1, 0, 
	{
		{"银行卡预授权",  0,&menu_CardPreAuth }, 
		NULL
	}
};

int mQRCodeZPay(void *p)
{	
	return iQRCodePay(Z_phoneTopos);
}

int mQRCodeBPay(void *p)
{		
	return iQRCodePay(B_posTophone);
}

SMenu menu_QrCode =
{"扫码", 0, 1, 0, 
	{
		{"扫码支付",  mQRCodeBPay}, 		
		{"二维码支付",mQRCodeZPay}, 
		NULL
	}
};

//选择支付方式
uint Select_pay_way(void)
{
	uint uiKey;
       _vCls();	   

        vDispCenter(1, "请选择扫码类别:", 1);
	_vDisp(2,"1.银联支付");
	_vDisp(3,"2.微信支付");
	_vDisp(4,"3.支付宝支付");	
	_vFlushKey();
	while(1)
	{
		if(_uiKeyPressed())
		{
			uiKey=_uiGetKey();
			if(uiKey==_KEY_1||uiKey==_KEY_2||uiKey==_KEY_3)
				return uiKey;
			if(uiKey==_KEY_CANCEL)
				return 0;
		}	
	}
	
}

extern int iPrintTrans(stTransRec *rec, int dup, uchar *pszReference);
extern int iPrintTransByTTC(void);
extern int iPrintAll(void);
extern int iPrintSum(void);

int mPrintLast(void *p)
{
        int  i,j;
        stQrTransRec  TransQrRec;
			
	vDispCenter(1, "重打上笔交易", 1);
	vClearLines(2);

	    if(gl_SysData.uiTransNum==0 && gl_SysData.uiQrTransNum==0)
	    {
	        vMessage("没有交易");
	        return 0;
	    }
 
    if(gl_SysData.uiTransNum > 0)
     {
	    for(i = 0; i < gl_SysData.uiTransNum; i ++)
	    {
	        uiMemManaGetTransRec(i, &gl_TransRec);	        
	        if(gl_TransRec.ulTTC == gl_SysData.ulLastTransTTC)
	        {
	            iPrintTrans(&gl_TransRec, 1, NULL);
	           return 1;
	        }
	    }
     }  
	
     if(gl_SysData.uiQrTransNum > 0)
     {
	    for(j = 0; j < gl_SysData.uiQrTransNum; j++)
	    {
	        uiMemManaGetQrTransRec(j, &TransQrRec);	
		if(TransQrRec.ucProType == 0x02)	
			continue;
	        if(TransQrRec.ulTTC == gl_SysData.ulLastTransTTC)
	        {
	            iPrintQrTrans(&TransQrRec, 1);
	           return 1;
	        }
	    }	   
     }

     if( j >= gl_SysData.uiQrTransNum)
    {
        vMessage("没有交易");
        return 0;
    } 	 	

	 return 0;
}


void mPrintLastError(void)
{
        int  i,j;
        stQrTransRec  TransQrRec;

       dbg("gl_SysData.ucPrintErr:%d\n",gl_SysData.ucPrintErr);
	   
	if(gl_SysData.ucPrintErr == 1)
	{
		vDispCenter(1, "重打上笔异常交易", 1);
		vClearLines(2);

	     if(gl_SysData.uiTransNum > 0)
	     {
		    for(i = 0; i < gl_SysData.uiTransNum; i ++)
		    {
		        uiMemManaGetTransRec(i, &gl_TransRec);	        
		        if(gl_TransRec.ulTTC == gl_SysData.ulLastTransTTC)
		        {
		            iPrintTrans(&gl_TransRec, 1, NULL);
		           return ;
		        }
		    }
	     }  
	}else if(gl_SysData.ucPrintErr == 2)
	{
		vDispCenter(1, "重打上笔异常交易", 1);
		vClearLines(2);
		
		if(gl_SysData.uiQrTransNum > 0)
	     {
		    for(j = 0; j < gl_SysData.uiQrTransNum; j++)
		    {
		        uiMemManaGetQrTransRec(j, &TransQrRec);	
			if(TransQrRec.ucProType == 0x02)	
				continue;
		        if(TransQrRec.ulTTC == gl_SysData.ulLastTransTTC)
		        {
		            iPrintQrTrans(&TransQrRec, 1);
		           return ;
		        }
		    }	   
	     }
	}

	 return ;
}

int mPrintOne(void *p)
{
	iPrintTransByTTC();
	return 0;
}

int mPrintAll(void *p)
{
	iPrintAll();
	return 0;
}

int mPrintSum(void *p)
{
	iPrintSum();
	return 0;
}

int mPrintLastSettle(void *p)
{
	return iPrintSettle(NULL,1);
}

SMenu menu_Print =
{"打印功能", 0, 1, 1, 
	{
		{"重打最后一笔", mPrintLast}, 
		{"重打任意一笔", mPrintOne},  
		{"打印交易明细", mPrintAll}, 
		{"打印交易汇总", mPrintSum}, 
		{"重打印结算单", mPrintLastSettle}, 
		NULL
	}
};

extern SMenu menu_EmvMainMemu;

extern int iTestDispQrcode(void *m);

SMenu menu_CJTMainMemu =
{"主菜单", 0, 1, 2, 
	{
		{"刷卡", mSale},
		{"扫  码", mSaleQr},   
                {"花呗", mSaleHb},   
                {"手机pay", mMobilePay},   		
		{"余额查询", mQueryBal},         
		{"管  理", 0, &menu_Manage},       
		NULL
	}
};

// 主管员功能
// 增加操作员
static int iDoOperAdd(void *p)
{
    uiOperAdd(gl_SysInfo.uiUiTimeOut);
	return 0;
}
// 删除操作员
static int iDoOperDel(void *p)
{
    uiOperSub(gl_SysInfo.uiUiTimeOut);
	return 0;
}
// 查看操作员
static int iDoOperView(void *p)
{
    uiOperList(gl_SysInfo.uiUiTimeOut);
	return 0;
}

// 操作员改密
static int iDoOperChangePwd(void *p)
{
       uiOperChangePwd();
	return 0;
}

static int iDoChangeMainOperPwd(void *p)
{
    
	iMainOperChgPwd();
	return 0;
}

int mGetLocal()
{
	char szLAC[10], szCellId[10];

	gprs_at_GetLACandCI(szLAC, szCellId);

	return 0;
}

int mSetPrint()
{
	int iKey;

	_vCls();
	vDispCenter(1, "打印设置", 1);
	_vDisp(2, "交易打印联数:");
	_vDisp(3, "0-不打印   1-打印一联");
	_vDisp(4, "2-打印两联 3-打印三联");
	vDispVarArg(5, "    请选择:[%d]", gl_SysInfo.ucPrinterAttr);
	while(1)
	{
		iKey=iGetKeyWithTimeout(gl_SysInfo.uiUiTimeOut);
		if(iKey<0 || iKey==_KEY_CANCEL)
			return -1;
		if(iKey==_KEY_ENTER)
			return 0;
		if(iKey>=_KEY_0 && iKey<=_KEY_3)
			break;
	}
	gl_SysInfo.ucPrinterAttr=iKey-_KEY_0;
	uiMemManaPutSysInfo();
	return 0;
}

int mCleanTrans()
{
	_vCls();

	vDispCenter(1, "清除交易", 1);
	_vDisp(2, "确认清除POS交易信息?");
	if(iOK(8)==1)
	{
        if(gl_SysData.uiTransNum || gl_SysData.uiQrTransNum)
        {
            gl_SysData.uiTransNum=0;
            gl_SysData.uiQrTransNum=0;
            uiMemManaPutSysData();
        }
		//清冲正
		if(uiMemManaIfExist8583(0))
			uiMemManaErase8583(0);
		if(uiMemManaIfExist8583(1))
			uiMemManaErase8583(1);	
	}
	return 0;
}

int iCleanTrans(void *p)
{
	_vCls();
	vDispCenter(1, "清除交易记录", 1);

    if(gl_SysData.uiTransNum || gl_SysData.uiQrTransNum)
    {
        _vDisp(2, "确认清除POS交易记录?");
        if(iOK(8)==1)
        {
            gl_SysData.uiTransNum=0;
            gl_SysData.uiQrTransNum=0;
            uiMemManaPutSysData();
            //清除签名文件信息
            uiMemClearAllJbg();

            vMessage("清除成功");
        }
    }else
        vMessage("无需清除");
	return 0;
}

int iCleanRev(void *p)
{
	_vCls();
	vDispCenter(1, "清除冲正信息", 1);
    
	if(uiMemManaIfExist8583(MSG8583_TYPE_REV) || uiMemManaIfExist8583(MSG8583_TYPE_SCR))
	{
		uiMemManaErase8583(MSG8583_TYPE_REV);
        uiMemManaErase8583(MSG8583_TYPE_SCR);
		vMessage("清除成功");
	}else
		vMessage("无需清除");
	return 0;
}

int iCleanScript(void *p)
{
	_vCls();
	vDispCenter(1, "清除脚本结果", 1);

	if(uiMemManaIfExist8583(MSG8583_TYPE_SCR))
	{
		uiMemManaErase8583(MSG8583_TYPE_SCR);
		vMessage("清除成功");
	}else
		vMessage("无需清除");
	return 0;
}

int iCleanFailSign(void *p)
{
	int i;
	stTransRec rec;
	int flag=0;

	_vCls();
	vDispCenter(1, "清除失败电签", 1);
    
    if(gl_SysData.ucFailSignNum)
    {
        gl_SysData.ucFailSignNum=0;
        uiMemManaPutSysData();
    }

	for(i=0; i<gl_SysData.uiTransNum; i++)
	{
		uiMemManaGetTransRec(i, &rec);
		if(rec.ucSignupload!=0xFF)
		{
			flag=1;
			rec.ucSignupload=0xFF;
			uiMemManaPutTransRec(i, &rec);
		}
		continue;
	}
    uiMemClearAllJbg();
	if(flag)
		vMessage("清除成功");
	else
		vMessage("无需清除");
	return 0;
}

int iSetQrPreView(void *p)
{
    int ret;
	int curr;

	_vCls();
	curr=gl_SysInfo.ucQrPreViewFlag+1;
	vDispCenter(1, "设置扫码预览", 1);
	ret=iSelectOne("打开或关闭扫码预览:", "1.关预览", "2.开预览", "", curr, 30, 1);
	if(ret>0 && curr!=ret)
	{
		gl_SysInfo.ucQrPreViewFlag=ret-1;
        uiMemManaPutSysInfo();
	}
	return 0;
}

int mQueryICCID(void *p)
{
    char szBuf[50] = {0};
	
    _vCls();
    vDispCenter(1, "ICCID查询", 1);
	
    //ICCID
   _vDisp(2, "ICCID:");
    iGetICCID(szBuf);
    _vDisp(3, szBuf);
    iGetKeyWithTimeout(0);    	
		
    return 0;
}
	
int mConfig(void *p)
{
	uint uiKey;
	ulong ulTimer;
	char keyBuf[8+1];
        int i;
	int curpage = 1;
	
	while(1) 
	 {
		vDispCenterEx(1, "参数设置", 1, 1, curpage, 2);
		vClearLines(2);

                if(curpage == 1)
                {                	
			_vDisp(2,"1.通讯设置");
			_vDisp(3,"2.按键音设置");
			_vDisp(4,"3.免密设置");
			_vDisp(5,"4.免签设置");
			
			_vSetTimer(&ulTimer, 6000L); //60秒	   
			_vFlushKey();
			i=0;
			memset(keyBuf,0x00,sizeof(keyBuf));
			
			while(1)
			{
				 if(_uiTestTimer(ulTimer))
				 	return 1;
				else if(_uiKeyPressed())
				{
					_vSetTimer(&ulTimer, 6000L);
					uiKey=_uiGetKey();
					if(uiKey==_KEY_1)
					{	
						iSetCommParam();
						break;
					}	
					else if(uiKey==_KEY_2)
					{
						SetCommOther();		
						break;
					}
					else if(uiKey==_KEY_3)
					{
						SetNoPinParam();	
						break;
					}
					else if(uiKey==_KEY_4)
					{
						SetNoSignParam();
						break;
					}
					else if(uiKey==_KEY_DOWN)
					{
						curpage ++;
						break;
					}
					else if(uiKey==_KEY_CANCEL)
						return 1;
               			 }
			}	
                }	
		else if(curpage ==2)
		{			
			_vDisp(2,"1.国密设置");

		
			_vSetTimer(&ulTimer, 6000L); //60秒	   
			_vFlushKey();
			i=0;
			memset(keyBuf,0x00,sizeof(keyBuf));
			while(1)
			{
				 if(_uiTestTimer(ulTimer))
				 	return 1;
				 else if(_uiKeyPressed())
				{
					_vSetTimer(&ulTimer, 6000L);
					uiKey=_uiGetKey();
					if(uiKey==_KEY_1)
					{	
						SetSmParam();
						break;
					}
				
					 else if(uiKey==_KEY_CANCEL)
						return 1;
					 else if(uiKey==_KEY_UP)
					{
							curpage --;
							break;
					}
					 else if(uiKey==_KEY_DOWN)           	          			
	            				uiKey='#';
			
					keyBuf[i++]=uiKey;	
					dbg("keyBuf:%s\n",keyBuf);
					if(strcmp("#888888#", keyBuf)==0)
			               {
			                	EnterTestMenu(&menu_CommParamSet);	
						break;
			            	}
		           		if(i>=sizeof(keyBuf)-1)
		                		break;	
				}						
			}	
		}
	}		
}

int mRemoteUpdate(void *p)
{
char szDateTime[14+1];

#ifdef TMS_PROC
	_vGetTime(szDateTime);
	 if(memcmp(szDateTime, gl_SysInfo.ucTimeTms, 8))
	{
		memcpy(gl_SysInfo.ucTimeTms,szDateTime,8);
		uiMemManaPutSysInfo();
		
	}
	 iTmsProc(0, 1);
#endif

	return 0;	
}

int mTusnKey(void *p)
{
	unsigned char tusnKey[20];
	
	memset(tusnKey, 0x15, 16);
	SecWriteTusnKey(tusnKey);
	dbg("Write TusnKey Success\n");

         _vCls();
	vMessage("注入成功");
	return 0;
}

int mSysterOperUnlockPos(void *p)
{
    _vCls();
    vDispCenter(1, "终端解锁", 1);
    if(gl_SysData.ucOperPauseMode != 2)
    {
        vMessage("终端未锁定,无需解锁");
        return 0;
    }
    vSetExitToMainFlag(1);
    gl_SysData.ucPosLoginFlag=0;
    gl_SysData.ucOperPauseMode=1;
    uiMemManaPutSysData();
    vMessage("终端解锁成功");
    return 0;
}


//主管功能菜单
SMenu menu_MainOperMemu =
{"主管功能", 0, 1, 1, 

	{
#ifdef ENABLE_PRINTER 
		{"终端参数打印", mTermPrintParam}, 
#else
               {"终端参数显示", mTermViewParam},
#endif
		{"非接专用菜单",0, &menu_ICDownload}, 	
#ifdef ENABLE_PRINTER 		
	        {"设置打印张数", mSetPrintNum}, 
#endif
                {"通讯方式设置", mCommConfig}, 
//                {"WIFI设置", mSetWifi},              
		{"用户管理",0, &menu_OperManage},
		{"修改主管密码", iDoChangeMainOperPwd},		
		{"重置操作员密码", mResetOperPwd}, 
		{"ICCID查询",mQueryICCID},
#ifdef TMS_PROC		
		{"远程更新",mRemoteUpdate},
#endif		
		NULL
	}
};

// 主管员功能
int iDoMainOperFunc(void)
{
	//int iSub;
	int iRet;
	uchar ucPinPos, szPin[7];

	vClearLines(2);
	_vDisp(2, "请输入操作员密码:");
	ucPinPos = _uiGetVCols();
	iRet = iInput(INPUT_PIN | INPUT_LEFT, 4, ucPinPos, szPin, 6, 6, 6, gl_SysInfo.uiUiTimeOut);
	if (iRet <0)
		return -1;

       	if (strcmp((char *)szPin, (char *)gl_SysInfo.szMainOperPwd)  != 0)
	{
		vDispMid(4,"密码错误"); //
		iGetKeyWithTimeout(3);
		return (-1);
	}

	gl_ucTimeOutFlag = 0;	   
	EnterTestMenu(&menu_MainOperMemu);
	
	return 0;
}

int mInitAllParam(void *p)
{
    
	_vCls();
	_vDisp(2, "是否清空POS数据?");
	_vDisp(3, "参数和交易会全部丢失!");
	if(iOK(8)>0)
	{
		_vDisp(4, "再次确认要清空数据?");
		_vDisp(5, "清空后POS将重启");
		if(iOK(8)>0)
		{
			memset(&gl_SysInfo, 0, sizeof(gl_SysInfo));
			memset(&gl_SysData, 0, sizeof(gl_SysData));
            uiMemManaPutSysData();
            uiMemManaPutSysInfo();
			uiMemClearAllJbg();
			systemReboot();
		}
	}
	return 0;
}

int mSetPosParam(void *p)
{
	return iSetPosParam();
}

SMenu menu_TransCtrolSwtich =
{"交易开关控制", 0, 1, 0, 
	{
		{"传统类交易",  mTransCtrolSwtich}, 
		{"扫码类交易", mQrTransCtrolSwtich},
		NULL
	}
};

SMenu menu_TransCtrol =
{"交易管理设置", 0, 1, 0, 
	{
		{"交易开关控制",  0,&menu_TransCtrolSwtich}, 
		{"交易输密控制", mTransPasswordSwitch}, 
		{"交易刷卡控制", mTransBrushSwitch},
		{"结算交易控制", mTransSettleSwitch},
		{"其他交易控制", mTransOtherSwitch},
		NULL
	}
};

int mTransCtrolSwtich(void *p)
{
	int curr, ret;
	_vCls();
	vDispCenter(1, "传统类交易", 1);

	curr= (gl_SysInfo.ucSaleSwitch == 1)? 1:2;
	ret=iSelectOne("消费", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucSaleSwitch=ret;
	    uiMemManaPutSysInfo();
	}

        vClearLines(2);
	curr= (gl_SysInfo.ucVoidSwitch == 1)? 1:2;
	ret=iSelectOne("撤销", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucVoidSwitch=ret;
	    uiMemManaPutSysInfo();
	}	

	 vClearLines(2);
	curr= (gl_SysInfo.ucRefundSwitch == 1)? 1:2;
	ret=iSelectOne("退货", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucRefundSwitch=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucBalanceSwitch == 1)? 1:2;
	ret=iSelectOne("余额查询", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
		return 0;
	if(ret != curr)
	{
		ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucBalanceSwitch=ret;
		uiMemManaPutSysInfo();
	}

	 vClearLines(2);
	curr= (gl_SysInfo.ucAuthSwitch == 1)? 1:2;
	ret=iSelectOne("预授权", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucAuthSwitch=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucAuthCancelSwitch == 1)? 1:2;
	ret=iSelectOne("预授权撤销", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucAuthCancelSwitch=ret;
	    uiMemManaPutSysInfo();
	}

	 vClearLines(2);
	curr= (gl_SysInfo.ucAuthCompleteSwitch == 1)? 1:2;
	ret=iSelectOne("预授权完成请求", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucAuthCompleteSwitch=ret;
	    uiMemManaPutSysInfo();
	}

	 vClearLines(2);
	curr= (gl_SysInfo.ucAuthCompletenoticeSwitch == 1)? 1:2;
	ret=iSelectOne("预授权完成通知", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	   ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucAuthCompletenoticeSwitch=ret;
	    uiMemManaPutSysInfo();
	}
	
	  vClearLines(2);
	curr= (gl_SysInfo.ucAuthCompleteVoidSwitch == 1)? 1:2;
	ret=iSelectOne("预授权完成撤销", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	     ret = (ret == 1) ? 1 : 0;		
	    gl_SysInfo.ucAuthCompleteVoidSwitch=ret;
	    uiMemManaPutSysInfo();
	}
	
	return 0;
}

int mQrTransCtrolSwtich(void *p)
{

	int curr, ret;
	_vCls();
	vDispCenter(1, "扫码交易", 1);

	curr= (gl_SysInfo.ucQrSaleSwitch == 1)? 1:2;
	ret=iSelectOne("扫码消费", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucQrSaleSwitch=ret;
	    uiMemManaPutSysInfo();
	}

        vClearLines(2);
	curr= (gl_SysInfo.ucQrRefundSwitch == 1)? 1:2;
	ret=iSelectOne("扫码退款", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret!=curr)
	{
	     ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucQrRefundSwitch=ret;
	    uiMemManaPutSysInfo();
	}	

	 vClearLines(2);
	curr= (gl_SysInfo.ucQrVoidSwitch == 1)? 1:2;
	ret=iSelectOne("扫码撤销", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret!=curr)
	{
	   ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucQrVoidSwitch=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucQrPreAuthSwitch == 1)? 1:2;
	ret=iSelectOne("扫码预授权", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
		return 0;
	if(ret!=curr)
	{
	        ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucQrPreAuthSwitch=ret;
		uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucQrPreAuthCancelSwitch == 1)? 1:2;
	ret=iSelectOne("扫码预授权撤销", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
		return 0;
	if(ret!=curr)
	{
	        ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucQrPreAuthCancelSwitch=ret;
		uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucQrAuthCompleteSwitch == 1)? 1:2;
	ret=iSelectOne("扫码预授权完成", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
		return 0;
	if(ret!=curr)
	{
	        ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucQrAuthCompleteSwitch=ret;
		uiMemManaPutSysInfo();
	}
	return 0;
}

int mTransPasswordSwitch(void *p)
{
	int curr, ret;
	_vCls();
	vDispCenter(1, "交易输密控制", 1);

	curr= (gl_SysInfo.ucSaleVoidPinFlag == 1)? 1:2;
	ret=iSelectOne("消费撤销是否输密", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucSaleVoidPinFlag=ret;
	    uiMemManaPutSysInfo();
	}

        vClearLines(2);
	curr= (gl_SysInfo.ucAuthVoidPinFlag == 1)? 1:2;
	ret=iSelectOne("预授权撤销是否输密", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret!=curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucAuthVoidPinFlag=ret;
	    uiMemManaPutSysInfo();
	}	

	 vClearLines(2);
	curr= (gl_SysInfo.ucAuthCompVoidPinFlag == 1)? 1:2;
	ret=iSelectOne("预授权完成撤销是否输密", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    return 0;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucAuthCompVoidPinFlag=ret;
	    uiMemManaPutSysInfo();
	}


	vClearLines(2);
	curr= (gl_SysInfo.ucAuthCompPinFlag == 1)? 1:2;
	ret=iSelectOne("预授权完成请求是否输密", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
		return 0;
	if(ret != curr)
	{
	        ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucAuthCompPinFlag=ret;
		uiMemManaPutSysInfo();
	}

	return 0;
}

int mTransBrushSwitch(void *p)
{
	int curr, ret;
	_vCls();
	vDispCenter(1, "交易刷卡控制", 1);

	curr= (gl_SysInfo.ucSaleVoidCardFlag == 1)? 1:2;
	ret=iSelectOne("消费撤销是否刷卡", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if(ret!=curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucSaleVoidCardFlag=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucAuthCompleteVoidCardFlag == 1)? 1:2;
	ret=iSelectOne("预授权完成撤销是否刷卡", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if(ret!=curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucAuthCompleteVoidCardFlag=ret;
	    uiMemManaPutSysInfo();
	}	
	return 0;
}

int mTransSettleSwitch(void *p)
{
	int curr, ret;
	_vCls();
	vDispCenter(1, "结算交易控制", 1);

	curr= (gl_SysInfo.ucAutoLogoutFlag == 1)? 1:2;
	ret=iSelectOne("结算是否自动签退", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if(ret!=curr)
	{
	    ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucAutoLogoutFlag=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucSettlePrintDetail == 1)? 1:2;
	ret=iSelectOne("结算是否打印明细", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if(ret != curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucSettlePrintDetail=ret;
	    uiMemManaPutSysInfo();
	}	
	return 0;
}

int mTransOtherSwitch(void *p)
{
	int curr, ret;
	_vCls();
	vDispCenter(1, "其他交易控制", 1);

	curr= (gl_SysInfo.ucMainOperPwd == 1)? 1:2;
	ret=iSelectOne("是否输入主管密码", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if(ret!=curr)
	{
	    ret = (ret == 1) ? 1 : 0;	
	    gl_SysInfo.ucMainOperPwd=ret;
	    uiMemManaPutSysInfo();
	}

	vClearLines(2);
	curr= (gl_SysInfo.ucHandCard == 1)? 1:2;
	ret=iSelectOne("是否允许手输卡号", "1.是", "2.否", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if(ret!=curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucHandCard=ret;
	    uiMemManaPutSysInfo();
	}	
	return 0;
}

int mSetTransParam(void *p)
{  
	return iSetTransParam();
}

extern int iSetApnParam(int *p);

SMenu menu_PwdMana = 
{"密码管理", 0, 1, 0, 
    {
		{"修改管理员密码", iDoSystemPwd},
		{"修改安全密码", iDoSafePwd},
		{"重置主管密码", iDoResetMainOperPwd}, 
		NULL
	}
};

SMenu menu_OtherMana =
{"其它功能设置", 0, 1, 0, 
    {
		{"清除交易流水", iCleanTrans},
		{"清除冲正信息", iCleanRev},
		{"清除脚本结果", iCleanScript},
		{"清除失败电签", iCleanFailSign},
		{"设置扫码预览", iSetQrPreView, NULL},
		{"清除锁定标志",mSysterOperUnlockPos},
		{"终端参数打印", mTermPrintParam}, 
		{"非接专用菜单",0, &menu_ICDownload}, 	 
		NULL
	}
};

//系统管理员菜单�
SMenu menu_SystemMemu =
{"系统设置", 0, 1, 0, 
	{
		{"商户参数设置", mSetPosParam},
		{"交易管理设置", 0,&menu_TransCtrol},
		{"系统参数设置", mSetTransParam},	
#ifdef USE_LTE_4G         
		{"通讯参数设置", 0, &menu_CommParamSet}, 
#else
       		{"通讯参数设置", mCommConfig}, 
#endif        
		{"终端密钥管理", 0,&menu_ImportKey}, 
		{"密码管理", 0,&menu_PwdMana},  		        
                {"其它管理设置", 0, &menu_OtherMana},    
                {"版  本", mViewTermInfo1}, 	
		NULL
	}
};

int iDoSystemMenu(void)
{
	int ret;
	char szBuf[10];
	uchar ucPinPos;

	vClearLines(2);
	_vDisp(2, "请输入操作员密码:");
	ucPinPos = _uiGetVCols();
	ret=iInput(INPUT_PIN | INPUT_LEFT, 4, ucPinPos, szBuf, 8, 8, 8, 30);
	if(ret<8)
	{
		return 1;
	}

#if 1
	if(strcmp(szBuf, gl_SysInfo.szSysOperPwd) && strcmp(szBuf, "23609898"))
#else
	if(strcmp(szBuf, gl_SysInfo.szSysOperPwd))
#endif
	{
		vDispMid(4,"密码错误"); //
		 iGetKeyWithTimeout(3);
		return 1;
	}

        gl_ucTimeOutFlag = 0;   	   
	EnterTestMenu(&menu_SystemMemu);
	
	return 0;
}


int iDoCjtMainMenu(void)
{
	EnterTestMenu(&menu_CJTMainMemu);	
	return 0;
}

//-------------------------------------------------------------------
//提示信息
//-------------------------------------------------------------------
void TermRemindInfo(void)
{	
	_vCls();
	vMessage("暂不支持该交易");
}

//-------------------------------------------------------------------
//提示信息
//-------------------------------------------------------------------
void TermRemindUseInfo(void)
{	
	_vCls();
	vMessage("功能异常!");
}

int mSetCommFunc(void)
{
    iSetAllCommParam();
    return 0;
}

int iTpduParamSet(void)
{
    int ret;
    char buf[50] = {0},bufbak[20] = {0};
    uchar ucPinPos;
	
    _vCls();
    vDispCenter(1, "TPDU设置", 1); 
   _vDisp(2, "银行卡TPDU:");   
    if(gl_SysInfo.sTPDU[0]==0x60)
        vOneTwo0(gl_SysInfo.sTPDU, 5, buf);    
    strcpy(bufbak, buf);
    ucPinPos = _uiGetVCols() - 10;	
    ret=iInput(INPUT_NORMAL|INPUT_INITIAL,3, ucPinPos, buf, 10, 10, 10, 30);
    if(ret<0)
        return -1;
    if(ret==10 && strcmp(buf, bufbak))
    {
        vTwoOne(buf, 10, gl_SysInfo.sTPDU);
        uiMemManaPutSysInfo();
    } 

     vClearLines(2);
     	 
   _vDisp(2, "扫码TPDU:");   
   buf[0]=0;
    if(gl_SysInfo.sQrTPDU[0]==0x60)
        vOneTwo0(gl_SysInfo.sQrTPDU, 5, buf);    
    strcpy(bufbak, buf);
    ucPinPos = _uiGetVCols() - 10;	
    ret=iInput(INPUT_NORMAL|INPUT_INITIAL,3, ucPinPos, buf, 10, 10, 10, 30);
    if(ret<0)
        return -1;
    if(ret==10 && strcmp(buf, bufbak))
    {
        vTwoOne(buf, 10, gl_SysInfo.sQrTPDU);
        uiMemManaPutSysInfo();
    } 
	
    return 0;
}

int SetCommOther(void)
{
	int ret, curr;
	
	_vCls();
	vDispCenter(1, "按键音设置", 1);
	gl_SysInfo.ucKeyBeep=_cGetKeyVoice();
	dbg("gl_SysInfo.ucKeyBeep111:%d\n",gl_SysInfo.ucKeyBeep);
	curr= (gl_SysInfo.ucKeyBeep == 1)? 1:2;
	ret=iSelectOne("设置按键音:", "1.开", "2.关", "", curr, 30, 1);
	if(ret<0)
	    	return -1;
	if(curr != ret)
	{
		ret = (ret == 1) ? 1 : 0;	
		gl_SysInfo.ucKeyBeep=ret;
		uiMemManaPutSysInfo();
		dbg("gl_SysInfo.ucKeyBeep222:%d\n",gl_SysInfo.ucKeyBeep);
		_vSetKeyVoice(gl_SysInfo.ucKeyBeep);        
	}

	return 0;
}

//1-映射 0-恢复
void vSetSpecAlphaKeyMap(int flag)
{
    if(flag==1)
    {
        _uiMapKey(_KEY_FN, _KEY_DOT);
        _uiMapKey(_KEY_UP, _KEY_STAR);
        _uiMapKey(_KEY_DOWN, _KEY_WELL);
        vSetSupportSpecAlphaKey(1);
    }else
    {
        _uiMapKey(_KEY_FN, _KEY_FN);
        _uiMapKey(_KEY_UP, _KEY_UP);
        _uiMapKey(_KEY_DOWN, _KEY_DOWN);
        vSetSupportSpecAlphaKey(0);
    }
    //vCheckKeyMap();
}

//TmsCommFlag:1-Tms通讯参数设置
int iSetAllCommParam(void)
{
    int ret;
    char buf[100];
    int  iPort;
    int  iMinLen;
    uchar ucMinLen;
   uchar ucPinPos;
    ulong ulTimer;
    uint uiKey;
    uchar szBuf[100];	
    ulong GprsAmount;	
    uint iOldCommType = 0;	
   int iRet;
   
    char *apszAlphaTable[10] = {
		"0 ,;:!",
		"1qzQZ",
		"2abcABC",
		"3defDEF",
		"4ghiGHI",
		"5jklJKL",
		"6mnoMNO",
		"7pqrsPQRS",
		"8tuvTUV",
		"9wxyzWXYZ"};

	vSetAlphaTable((uchar **)apszAlphaTable);
    
    _vCls();
        vDispCenter(1, "通讯设置", 1);
		
    /*
    curr=gl_SysInfo.ucWireLongLink;
    ret=iSelectOne("无线是否为长连接","1.长连接", "0.短连接","", curr, 30, 1);
    if(ret<0)
        return -1;
    if(ret!=curr)
    {
        gl_SysInfo.ucWireLongLink=ret;
        uiMemManaPutSysInfo();
    }

    vClearLines(2);
    _vDisp(2, "接入点号码:");
    strcpy(buf, "*99***1#");
	ucPinPos = _uiGetVCols() - strlen(buf);
    vSetSpecAlphaKeyMap(1);
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 8, 0, 8, 60);
    vSetSpecAlphaKeyMap(0);
    if(ret<0)
        return -1;
    
    vClearLines(2);
    strcpy(buf, (char*)gl_SysInfo.szApn);
	ucPinPos = _uiGetVCols() - strlen(buf);
    _vDisp(2, "APN名称:");
    //_vDisp(_uiGetVCols(), "(APN名称可为空)");
    vSetSpecAlphaKeyMap(1);
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 16, 0, 16, 60);
    vSetSpecAlphaKeyMap(0);
    if(ret<0)
        return -1;
    
    if(strcmp(buf, (char*)gl_SysInfo.szApn))
    {
        strcpy((char*)gl_SysInfo.szApn, buf);
        uiMemManaPutSysInfo();
    }


  vClearLines(2);  
    curr= (gl_SysInfo.ucTmsDomainOrIp == 1)? 1:2;
    ret=iSelectOne("TMS服务器访问方式:", "1.域名", "2.IP", "", curr, 30, 1);
    if(ret<0)
        return -1;
    if(curr!=ret)
    {
        ret = (ret == 1) ? 1 : 0;
        gl_SysInfo.ucTmsDomainOrIp=ret;
        uiMemManaPutSysInfo();
    }
	
        if(gl_SysInfo.ucTmsDomainOrIp==1)
        {
            vClearLines(2);     
            _vDisp(2, "TMS服务器域名:");
	  memset(buf,0x00,sizeof(buf));		
            strcpy(buf, gl_SysInfo.szTmsHost);
	   ucPinPos = _uiGetVCols() - strlen(buf);
            if(buf[0]==0)
                iMinLen=0;
            else
                iMinLen=5;
            vSetSpecAlphaKeyMap(1);
            ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, _uiGetVCols(), iMinLen, _uiGetVCols(), 60);
            vSetSpecAlphaKeyMap(0);
            if(ret<0)
                return -1;     
            if(strcmp(buf, gl_SysInfo.szTmsHost))
            {
                if(buf[0])
                    strcpy(gl_SysInfo.szTmsHost,buf);
                else
                    gl_SysInfo.szTmsHost[0]=0;
                uiMemManaPutSysInfo();
                dbg("szTmsHost:[%s]\n", gl_SysInfo.szTmsHost);				
            }     

 	   vClearLines(2);
            _vDisp(2, "TMS主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiTmsHostPort); 	
		ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiTmsHostPort)
            {                
		gl_SysInfo.uiTmsHostPort = iPort;
                uiMemManaPutSysInfo();        
            }

            vClearLines(2);
	     _vDisp(2, "TMS备用服务器域名:");
	    memset(buf,0x00,sizeof(buf));	 
            strcpy(buf, gl_SysInfo.szTmsHostBak);
			 ucPinPos = _uiGetVCols() - strlen(buf);
            if(buf[0]==0)
                iMinLen=0;
            else
                iMinLen=5;
            vSetSpecAlphaKeyMap(1);
            ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, _uiGetVCols(), iMinLen, _uiGetVCols(), 60);
            vSetSpecAlphaKeyMap(0);
            if(ret<0)
                return -1;
            if(strcmp(buf, gl_SysInfo.szTmsHostBak))
            {
                if(buf[0])
                    strcpy(gl_SysInfo.szTmsHostBak,buf);
                else
                    gl_SysInfo.szTmsHostBak[0]=0;
                uiMemManaPutSysInfo();            
                dbg("szTmsHostBak:[%s]\n", gl_SysInfo.szTmsHostBak);
            }			

	    vClearLines(2);
            _vDisp(2, "TMS备用主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiTmsHostPortBak); 
		ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiTmsHostPortBak)
            {
                
		gl_SysInfo.uiTmsHostPortBak = iPort;
                uiMemManaPutSysInfo();     
            }			
        }
	else  {
             vClearLines(2);
	     _vDisp(2, "TMS服务器IP:");			 
	    ucMinLen=7;
	    if(gl_SysInfo.szTmsIP1[0]==0)
	        ucMinLen=0;
	   	memset(buf,0x00,sizeof(buf));
	        sprintf(buf, "%s", gl_SysInfo.szTmsIP1);  	
		ucPinPos = _uiGetVCols() - strlen(buf);		
		_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
	    while(1)
	    {
	        ret=iInput(INPUT_ALL_CHAR|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 15, ucMinLen, 15, 30);
	        if(ucMinLen==0 && ret==0)
	            break;
	        if(ret<0)
	          	return -1;
	        if(ret>0 && iCheckIpFormat(buf))
	        {
	            vMessageEx("IP地址非法", 100);
	            _vDisp(_uiGetVLines(), "");
	            continue;
	        }
	        break;
	    }
		_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	          if(strcmp(gl_SysInfo.szTmsIP1,buf))
		{
			strcpy(gl_SysInfo.szTmsIP1,buf);
			uiMemManaPutSysInfo();
		}

            vClearLines(2);
            _vDisp(2, "TMS主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiTmsHostPort); 	
	   ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiTmsHostPort)
            {                
		gl_SysInfo.uiTmsHostPort = iPort;
                uiMemManaPutSysInfo();        
            }

            vClearLines(2);
	     _vDisp(2, "TMS备用服务器IP:");			 
	    ucMinLen=7;
	    if(gl_SysInfo.szTmsIP2[0]==0)
	        ucMinLen=0;
	   memset(buf,0x00,sizeof(buf));
	    sprintf(buf, "%s", gl_SysInfo.szTmsIP2);  
		ucPinPos = _uiGetVCols() - strlen(buf);
		_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
	    while(1)
	    {
	        ret=iInput(INPUT_ALL_CHAR|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 15, ucMinLen, 15, 30);
	        if(ucMinLen==0 && ret==0)
	            break;
	        if(ret<0)
	            return -1;
	        if(ret>0 && iCheckIpFormat(buf))
	        {
	            vMessageEx("IP地址非法", 100);
	            _vDisp(_uiGetVLines(), "");
	            continue;
	        }
	        break;
	    }
		_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
               if(strcmp(gl_SysInfo.szTmsIP2,buf))
		{
			strcpy(gl_SysInfo.szTmsIP2,buf);
			uiMemManaPutSysInfo();
		}

            vClearLines(2);
            _vDisp(2, "TMS备用主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiTmsHostPortBak); 
		ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiTmsHostPortBak)
            {
                
		gl_SysInfo.uiTmsHostPortBak = iPort;
                uiMemManaPutSysInfo();     
            }		
        }
*/

/*	
    vClearLines(2);
    curr= (gl_SysInfo.ucDomainOrIp == 1)? 1:2;
    ret=iSelectOne("服务器访问方式:", "1.域名", "2.IP", "", curr, 30, 1);
    if(ret<0)
        return -1;
    if(curr!=ret)
    {
        ret = (ret == 1) ? 1 : 0;
        gl_SysInfo.ucDomainOrIp=ret;
        uiMemManaPutSysInfo();
    }
*/	
#ifndef NO_WIFI
    if(iPosHardwareModule(CHK_HAVE_WIFI, NULL, 0)>0)
    {
        _vDisp(2, (uchar *)"请选择通讯方式:");    
        vDispVarArg(3, (uchar *)"1-%s 2-WIFI", NetName);
        if (gl_SysInfo.iCommType == VPOSCOMM_GPRS || gl_SysInfo.iCommType == VPOSCOMM_WIFI)
        {
            if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
                ret = 1;
            else
                ret = 2;
            vDispVarArg(4, "通讯方式:[%d]", ret);
            iOldCommType = gl_SysInfo.iCommType;
        }
        else
            _vDisp(4, (uchar *)"通讯方式:[ ]");
        _vSetTimer(&ulTimer, 6000);
        while (1)
        {
            if(_uiTestTimer(ulTimer))
                return -1;
            if(!_uiKeyPressed())
                continue;
            _vSetTimer(&ulTimer, 6000);
            uiKey = _uiGetKey();
            if (uiKey == _KEY_1)
            {	
               	 if(!gl_SysInfo.szPosId[0] && !gl_SysInfo.szMerchId[0])
	 	{
			_vCls();
		 	vMessageMul("请先使用WIFI初始化设备");
			return 	-1;	
	         }
					   
		 if(gl_SysInfo.ucGprsAllowFlag == 1)
	        {
	                 //是否切换WIFI模块
	                //选择是
	                gl_SysInfo.ucGprsFlag = 1;
					 
	        	 if(gl_SysInfo.ucGprsPayFlag == 0)
		        {
		        #ifndef NO_WIFI
		                GprsAmount = ( atol(gl_SysInfo.ucGprsAmount))/12;
		                sprintf(szBuf, "使用GPRS模块,会在下笔交易中扣取全年GPRS费用:%lu.%02lu元/月,是否确认使用?", GprsAmount/100, GprsAmount%100);
				_vCls();
				vMessageMulEx(szBuf);		
				iRet = iOK(60);
		            	if(iRet ==1)
		            	{
		            		//GPRS模块初始化
		            	        vClearLines(2);
				        _vDisp(3, "通讯设置中,请稍候...");
						
					 gl_SysInfo.iCommType = VPOSCOMM_GPRS;	
				        if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
				        {
				            iCommLogout(iOldCommType, 1);
				            uiMemManaPutSysInfo();
				            vClearWilrelessIcon();	  
				            iCommLogin();
				        }	
						
		            	 	//开通GPRS交易
		            	 	iOpenGprsMessage();
		            	 	gl_SysInfo.ucGprsUseFlag = 1;
					uiMemManaPutSysInfo();		
		            	}
				else 
					return -1;
			#endif	
		        }	
		        else
		        {
		        #ifndef NO_WIFI
		        	//GPRS模块初始化
	            	        vClearLines(2);
			        _vDisp(3, "通讯设置中,请稍候...");
					
				 gl_SysInfo.iCommType = VPOSCOMM_GPRS;	
			        if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
			        {
			            iCommLogout(iOldCommType, 1);
			            uiMemManaPutSysInfo();
			            vClearWilrelessIcon();	  
			            iCommLogin();
			        }
			#endif		
		        }
	        }
		else 
		{
			//不允许GPRS
			vMessage("不允许使用GPRS通讯方式");	
			return -1;
		}
		
                break;
            }
            if (uiKey == _KEY_2)
            {
                gl_SysInfo.iCommType = VPOSCOMM_WIFI;
		vClearLines(2);
		_vDisp(3, "通讯设置中,请稍候...");
		if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
		{
			iCommLogout(iOldCommType, 1);
			uiMemManaPutSysInfo();
			vClearWilrelessIcon();
		}
		ret = iWifiConfig(NULL);		
                break;
            }
            if (uiKey == _KEY_ENTER && iOldCommType)
           {
           	vClearLines(2);
		_vDisp(3, "通讯设置中,请稍候...");
		if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
		{
			iCommLogout(iOldCommType, 1);
			uiMemManaPutSysInfo();
			vClearWilrelessIcon();
		}

		if (gl_SysInfo.iCommType == VPOSCOMM_WIFI)
			ret = iWifiConfig(NULL);
		else
		{
			uiMemManaPutSysInfo();
			iCommLogin();
		}
           	break;
            }		
            if(uiKey == _KEY_ESC)
                break;
        }
        if(uiKey == _KEY_ESC && iOldCommType==0)		//取消设置
            return 1;
        
        if(uiKey == _KEY_ESC && iOldCommType==gl_SysInfo.iCommType)
            return 0;
/*            
        vClearLines(2);
        _vDisp(3, "通讯设置中,请稍候...");
        if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
        {
            iCommLogout(iOldCommType, 1);
            uiMemManaPutSysInfo();
            vClearWilrelessIcon();
        }

        if (gl_SysInfo.iCommType == VPOSCOMM_WIFI)
            ret = iWifiConfig(NULL);
        else
        {
            uiMemManaPutSysInfo();
            iCommLogin();
        }
*/
    }else
#endif        
    {
        _vDisp(2, (uchar *)"通讯方式:"); 
        vDispVarArg(3, (uchar *)"1.%s", NetName);
        iOldCommType=gl_SysInfo.iCommType;
        if(gl_SysInfo.iCommType != VPOSCOMM_GPRS)
        {
            gl_SysInfo.iCommType = VPOSCOMM_GPRS;
            uiMemManaPutSysInfo();
        }
        _vSetTimer(&ulTimer, 800);
        while (1)
        {
            if(_uiTestTimer(ulTimer))
                return -1;
            if(!_uiKeyPressed())
                continue;
            //_vSetTimer(&ulTimer, 800);
            uiKey = _uiGetKey();
            if (uiKey == _KEY_1 || uiKey == _KEY_ENTER || uiKey == _KEY_ESC)
                break;
        }
        if(uiKey == _KEY_ESC && iOldCommType==gl_SysInfo.iCommType)
            return 0;

        vClearLines(2);
        _vDisp(3, "通讯设置中,请稍候...");
        ret=iCommLogin();
        dbg("iCommLogin ret:%d\n", ret);
    }


        if(gl_SysInfo.ucDomainOrIp==1)
        {
            vClearLines(2);     
            _vDisp(2, "服务器域名:");
	  memset(buf,0x00,sizeof(buf));		
            strcpy(buf, gl_SysInfo.szSvrDomain);
	   ucPinPos = _uiGetVCols() - strlen(buf);
	   if(buf[0]==0)
                iMinLen=0;
            else
                iMinLen=5;
            vSetSpecAlphaKeyMap(1);
            ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, _uiGetVCols(), iMinLen, _uiGetVCols(), 60);
            vSetSpecAlphaKeyMap(0);
            if(ret<0)
                return -1;     
            if(strcmp(buf, gl_SysInfo.szSvrDomain))
            {
                if(buf[0])
                    strcpy(gl_SysInfo.szSvrDomain,buf);
                else
                    gl_SysInfo.szSvrDomain[0]=0;
                uiMemManaPutSysInfo();
                dbg("szSvrDomain:[%s]\n", gl_SysInfo.szSvrDomain);				
            }     

 	   vClearLines(2);
            _vDisp(2, "主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiSvrPort); 	
		ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiSvrPort)
            {                
		gl_SysInfo.uiSvrPort = iPort;
                uiMemManaPutSysInfo();        
            }
			

            vClearLines(2);
	     _vDisp(2, "备用服务器域名:");
	    memset(buf,0x00,sizeof(buf));	 
            strcpy(buf, gl_SysInfo.szBakSvrDomain);
			 ucPinPos = _uiGetVCols() - strlen(buf);
	   if(buf[0]==0)
                iMinLen=0;
            else
                iMinLen=5;		 
            vSetSpecAlphaKeyMap(1);
            ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, _uiGetVCols(), iMinLen, _uiGetVCols(), 60);
            vSetSpecAlphaKeyMap(0);
            if(ret<0)
                return -1;
            if(strcmp(buf, gl_SysInfo.szBakSvrDomain))
            {
                if(buf[0])
                    strcpy(gl_SysInfo.szBakSvrDomain,buf);
                else
                    gl_SysInfo.szBakSvrDomain[0]=0;
                uiMemManaPutSysInfo();            
                dbg("szBakSvrDomain:[%s]\n", gl_SysInfo.szBakSvrDomain);
            }			

	    vClearLines(2);
            _vDisp(2, "备用主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiBakSvrPort); 
		ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiBakSvrPort)
            {
                
		gl_SysInfo.uiBakSvrPort = iPort;
                uiMemManaPutSysInfo();     
            }			
        }
	else  {
                 _vCls();
                vDispCenter(1, "通讯设置", 1);
	     _vDisp(2, "服务器IP:");			 
	    ucMinLen=7;
	    if(gl_SysInfo.szSvrIp[0]==0)
	        ucMinLen=0;
	   	memset(buf,0x00,sizeof(buf));
	        sprintf(buf, "%s", gl_SysInfo.szSvrIp);  	
		ucPinPos = _uiGetVCols() - strlen(buf);		
		_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
	    while(1)
	    {
	        ret=iInput(INPUT_ALL_CHAR|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 15, ucMinLen, 15, 30);
	        if(ucMinLen==0 && ret==0)
	            break;
	        if(ret<0)
	          	return -1;
	        if(ret>0 && iCheckIpFormat(buf))
	        {
	            vMessageEx("IP地址非法", 100);
	            _vDisp(_uiGetVLines(), "");
	            continue;
	        }
	        break;
	    }
		_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	          if(strcmp(gl_SysInfo.szSvrIp,buf))
		{
			strcpy(gl_SysInfo.szSvrIp,buf);
			uiMemManaPutSysInfo();
		}

            vClearLines(2);
            _vDisp(2, "主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiSvrPort); 	
	   ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiSvrPort)
            {                
		gl_SysInfo.uiSvrPort = iPort;
                uiMemManaPutSysInfo();        
            }

            vClearLines(2);
	     _vDisp(2, "备用服务器IP:");			 
	    ucMinLen=7;
	    if(gl_SysInfo.szBakSvrIp[0]==0)
	        ucMinLen=0;
	   memset(buf,0x00,sizeof(buf));
	    sprintf(buf, "%s", gl_SysInfo.szBakSvrIp);  
		ucPinPos = _uiGetVCols() - strlen(buf);
		_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
	    while(1)
	    {
	        ret=iInput(INPUT_ALL_CHAR|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 15, ucMinLen, 15, 30);
	        if(ucMinLen==0 && ret==0)
	            break;
	        if(ret<0)
	            return -1;
	        if(ret>0 && iCheckIpFormat(buf))
	        {
	            vMessageEx("IP地址非法", 100);
	            _vDisp(_uiGetVLines(), "");
	            continue;
	        }
	        break;
	    }
		_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
               if(strcmp(gl_SysInfo.szBakSvrIp,buf))
		{
			strcpy(gl_SysInfo.szBakSvrIp,buf);
			uiMemManaPutSysInfo();
		}

            vClearLines(2);
            _vDisp(2, "备用主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiBakSvrPort); 
		ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiBakSvrPort)
            {
                
		gl_SysInfo.uiBakSvrPort = iPort;
                uiMemManaPutSysInfo();     
            }		
        }

/*			 
    vClearLines(2);
    curr = gl_SysInfo.ucNeedApnUser;
    ret=iSelectOne("是否需要用户名", "1.不需要", "0.需要", "", curr, 30, 1);
    if(ret<0)
       return -1;
   if(curr!=ret)
    {
        gl_SysInfo.ucNeedApnUser=ret;
        uiMemManaPutSysInfo();
    }
    
    //curr=ret;
    //if(curr==2)
    {
        vClearLines(2);
        _vDisp(2, "用户名:");
        strcpy(buf, gl_SysInfo.szApnUser);
        vSetSpecAlphaKeyMap(1);
        ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, 0, buf, 16, 0, 16, 60);
        vSetSpecAlphaKeyMap(0);
        if(ret<0)
            return -1;      
        if(strcmp(buf, gl_SysInfo.szApnUser))
        {
            strcpy(gl_SysInfo.szApnUser, buf);
            uiMemManaPutSysInfo();
            dbg("szApnUser:[%s]\n", gl_SysInfo.szApnUser);
        }
        
        vClearLines(2);
        _vDisp(2, "用户密码:");
        strcpy(buf, gl_SysInfo.szApnPwd);
        vSetSpecAlphaKeyMap(1);
        ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, 0, buf, 16, 0, 16, 60);
        vSetSpecAlphaKeyMap(0);
        if(ret<0)
            return -1;      
        if(strcmp(buf, gl_SysInfo.szApnPwd))
        {
            strcpy(gl_SysInfo.szApnPwd, buf);
            uiMemManaPutSysInfo();
            dbg("szApnPwd:[%s]\n", gl_SysInfo.szApnPwd);
        }
    }
    
    vClearLines(2);
    _vDisp(2, "SIM卡密码:");
    strcpy(buf, gl_SysInfo.szSimPin);
    ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, 0, buf, 8, 0, 8, 30);
    if(ret<0)
        return -1;     
    if(strcmp(buf, gl_SysInfo.szSimPin))
    {
        strcpy(gl_SysInfo.szSimPin, buf);
        uiMemManaPutSysInfo();
        dbg("szSimPin:[%s]\n", gl_SysInfo.szSimPin);
    }
*/
	
    return 0;
}


//TmsCommFlag:1-Tms通讯参数设置
int iSetCommParam(void)
{
    int ret;
    ulong ulTimer;
    uint uiKey;
    uchar szBuf[100];	
    ulong GprsAmount;	
    uint iOldCommType = 0;	
   int iRet;
   
    char *apszAlphaTable[10] = {
		"0 ,;:!",
		"1qzQZ",
		"2abcABC",
		"3defDEF",
		"4ghiGHI",
		"5jklJKL",
		"6mnoMNO",
		"7pqrsPQRS",
		"8tuvTUV",
		"9wxyzWXYZ"};

	vSetAlphaTable((uchar **)apszAlphaTable);
    
    _vCls();
        vDispCenter(1, "通讯设置", 1);
		
    /*
    curr=gl_SysInfo.ucWireLongLink;
    ret=iSelectOne("无线是否为长连接","1.长连接", "0.短连接","", curr, 30, 1);
    if(ret<0)
        return -1;
    if(ret!=curr)
    {
        gl_SysInfo.ucWireLongLink=ret;
        uiMemManaPutSysInfo();
    }

    vClearLines(2);
    _vDisp(2, "接入点号码:");
    strcpy(buf, "*99***1#");
	ucPinPos = _uiGetVCols() - strlen(buf);
    vSetSpecAlphaKeyMap(1);
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 8, 0, 8, 60);
    vSetSpecAlphaKeyMap(0);
    if(ret<0)
        return -1;
    
    vClearLines(2);
    strcpy(buf, (char*)gl_SysInfo.szApn);
	ucPinPos = _uiGetVCols() - strlen(buf);
    _vDisp(2, "APN名称:");
    //_vDisp(_uiGetVCols(), "(APN名称可为空)");
    vSetSpecAlphaKeyMap(1);
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 16, 0, 16, 60);
    vSetSpecAlphaKeyMap(0);
    if(ret<0)
        return -1;
    
    if(strcmp(buf, (char*)gl_SysInfo.szApn))
    {
        strcpy((char*)gl_SysInfo.szApn, buf);
        uiMemManaPutSysInfo();
    }


  vClearLines(2);  
    curr= (gl_SysInfo.ucTmsDomainOrIp == 1)? 1:2;
    ret=iSelectOne("TMS服务器访问方式:", "1.域名", "2.IP", "", curr, 30, 1);
    if(ret<0)
        return -1;
    if(curr!=ret)
    {
        ret = (ret == 1) ? 1 : 0;
        gl_SysInfo.ucTmsDomainOrIp=ret;
        uiMemManaPutSysInfo();
    }
	
        if(gl_SysInfo.ucTmsDomainOrIp==1)
        {
            vClearLines(2);     
            _vDisp(2, "TMS服务器域名:");
	  memset(buf,0x00,sizeof(buf));		
            strcpy(buf, gl_SysInfo.szTmsHost);
	   ucPinPos = _uiGetVCols() - strlen(buf);
            if(buf[0]==0)
                iMinLen=0;
            else
                iMinLen=5;
            vSetSpecAlphaKeyMap(1);
            ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, _uiGetVCols(), iMinLen, _uiGetVCols(), 60);
            vSetSpecAlphaKeyMap(0);
            if(ret<0)
                return -1;     
            if(strcmp(buf, gl_SysInfo.szTmsHost))
            {
                if(buf[0])
                    strcpy(gl_SysInfo.szTmsHost,buf);
                else
                    gl_SysInfo.szTmsHost[0]=0;
                uiMemManaPutSysInfo();
                dbg("szTmsHost:[%s]\n", gl_SysInfo.szTmsHost);				
            }     

 	   vClearLines(2);
            _vDisp(2, "TMS主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiTmsHostPort); 	
		ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiTmsHostPort)
            {                
		gl_SysInfo.uiTmsHostPort = iPort;
                uiMemManaPutSysInfo();        
            }

            vClearLines(2);
	     _vDisp(2, "TMS备用服务器域名:");
	    memset(buf,0x00,sizeof(buf));	 
            strcpy(buf, gl_SysInfo.szTmsHostBak);
			 ucPinPos = _uiGetVCols() - strlen(buf);
            if(buf[0]==0)
                iMinLen=0;
            else
                iMinLen=5;
            vSetSpecAlphaKeyMap(1);
            ret=iInput(INPUT_ALPHA|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, _uiGetVCols(), iMinLen, _uiGetVCols(), 60);
            vSetSpecAlphaKeyMap(0);
            if(ret<0)
                return -1;
            if(strcmp(buf, gl_SysInfo.szTmsHostBak))
            {
                if(buf[0])
                    strcpy(gl_SysInfo.szTmsHostBak,buf);
                else
                    gl_SysInfo.szTmsHostBak[0]=0;
                uiMemManaPutSysInfo();            
                dbg("szTmsHostBak:[%s]\n", gl_SysInfo.szTmsHostBak);
            }			

	    vClearLines(2);
            _vDisp(2, "TMS备用主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiTmsHostPortBak); 
		ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiTmsHostPortBak)
            {
                
		gl_SysInfo.uiTmsHostPortBak = iPort;
                uiMemManaPutSysInfo();     
            }			
        }
	else  {
             vClearLines(2);
	     _vDisp(2, "TMS服务器IP:");			 
	    ucMinLen=7;
	    if(gl_SysInfo.szTmsIP1[0]==0)
	        ucMinLen=0;
	   	memset(buf,0x00,sizeof(buf));
	        sprintf(buf, "%s", gl_SysInfo.szTmsIP1);  	
		ucPinPos = _uiGetVCols() - strlen(buf);		
		_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
	    while(1)
	    {
	        ret=iInput(INPUT_ALL_CHAR|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 15, ucMinLen, 15, 30);
	        if(ucMinLen==0 && ret==0)
	            break;
	        if(ret<0)
	          	return -1;
	        if(ret>0 && iCheckIpFormat(buf))
	        {
	            vMessageEx("IP地址非法", 100);
	            _vDisp(_uiGetVLines(), "");
	            continue;
	        }
	        break;
	    }
		_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	          if(strcmp(gl_SysInfo.szTmsIP1,buf))
		{
			strcpy(gl_SysInfo.szTmsIP1,buf);
			uiMemManaPutSysInfo();
		}

            vClearLines(2);
            _vDisp(2, "TMS主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiTmsHostPort); 	
	   ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiTmsHostPort)
            {                
		gl_SysInfo.uiTmsHostPort = iPort;
                uiMemManaPutSysInfo();        
            }

            vClearLines(2);
	     _vDisp(2, "TMS备用服务器IP:");			 
	    ucMinLen=7;
	    if(gl_SysInfo.szTmsIP2[0]==0)
	        ucMinLen=0;
	   memset(buf,0x00,sizeof(buf));
	    sprintf(buf, "%s", gl_SysInfo.szTmsIP2);  
		ucPinPos = _uiGetVCols() - strlen(buf);
		_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
	    while(1)
	    {
	        ret=iInput(INPUT_ALL_CHAR|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 15, ucMinLen, 15, 30);
	        if(ucMinLen==0 && ret==0)
	            break;
	        if(ret<0)
	            return -1;
	        if(ret>0 && iCheckIpFormat(buf))
	        {
	            vMessageEx("IP地址非法", 100);
	            _vDisp(_uiGetVLines(), "");
	            continue;
	        }
	        break;
	    }
		_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
               if(strcmp(gl_SysInfo.szTmsIP2,buf))
		{
			strcpy(gl_SysInfo.szTmsIP2,buf);
			uiMemManaPutSysInfo();
		}

            vClearLines(2);
            _vDisp(2, "TMS备用主机端口号:");
	   memset(buf,0x00,sizeof(buf));		
	    sprintf(buf, "%d", gl_SysInfo.uiTmsHostPortBak); 
		ucPinPos = _uiGetVCols() - strlen(buf);
            ret=iInput(INPUT_NORMAL|INPUT_INITIAL|INPUT_LEFT, 3, ucPinPos, buf, 5, 1, 5, 30);
            if(ret<0)
                return -1;
	   iPort=atoi(buf);		
            if(iPort != gl_SysInfo.uiTmsHostPortBak)
            {
                
		gl_SysInfo.uiTmsHostPortBak = iPort;
                uiMemManaPutSysInfo();     
            }		
        }
*/

/*	
    vClearLines(2);
    curr= (gl_SysInfo.ucDomainOrIp == 1)? 1:2;
    ret=iSelectOne("服务器访问方式:", "1.域名", "2.IP", "", curr, 30, 1);
    if(ret<0)
        return -1;
    if(curr!=ret)
    {
        ret = (ret == 1) ? 1 : 0;
        gl_SysInfo.ucDomainOrIp=ret;
        uiMemManaPutSysInfo();
    }
*/	
#ifndef NO_WIFI
    if(iPosHardwareModule(CHK_HAVE_WIFI, NULL, 0)>0)
    {
        _vDisp(2, (uchar *)"请选择通讯方式:");    
        vDispVarArg(3, (uchar *)"1-%s 2-WIFI", NetName);
        if (gl_SysInfo.iCommType == VPOSCOMM_GPRS || gl_SysInfo.iCommType == VPOSCOMM_WIFI)
        {
            if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
                ret = 1;
            else
                ret = 2;
            vDispVarArg(4, "通讯方式:[%d]", ret);
            iOldCommType = gl_SysInfo.iCommType;
        }
        else
            _vDisp(4, (uchar *)"通讯方式:[ ]");
        _vSetTimer(&ulTimer, 6000);
        while (1)
        {
            if(_uiTestTimer(ulTimer))
                return -1;
            if(!_uiKeyPressed())
                continue;
            _vSetTimer(&ulTimer, 6000);
            uiKey = _uiGetKey();
            if (uiKey == _KEY_1)
            {	
               	 if(!gl_SysInfo.szPosId[0] && !gl_SysInfo.szMerchId[0])
	 	{
			_vCls();
		 	vMessageMul("请先使用WIFI初始化设备");
			return 	-1;	
	         }
					   
		 if(gl_SysInfo.ucGprsAllowFlag == 1)
	        {
	                 //是否切换WIFI模块
	                //选择是
	                gl_SysInfo.ucGprsFlag = 1;
					 
	        	 if(gl_SysInfo.ucGprsPayFlag == 0)
		        {
		        #ifndef NO_WIFI
		                GprsAmount = ( atol(gl_SysInfo.ucGprsAmount))/12;
		                sprintf(szBuf, "使用GPRS模块,会在下笔交易中扣取全年GPRS费用:%lu.%02lu元/月,是否确认使用?", GprsAmount/100, GprsAmount%100);
				_vCls();
				vMessageMulEx(szBuf);		
				iRet = iOK(60);
		            	if(iRet ==1)
		            	{
		            	        //GPRS模块初始化
		            	        vClearLines(2);
				        _vDisp(3, "通讯设置中,请稍候...");
					
					 gl_SysInfo.iCommType = VPOSCOMM_GPRS;	
				        if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
				        {
				            iCommLogout(iOldCommType, 1);
					    uiMemManaPutSysInfo();
				            vClearWilrelessIcon();	  
				            iCommLogin();	
				        }	
						
		            	 	//开通GPRS交易
	            	 		iOpenGprsMessage();
	            	 		gl_SysInfo.ucGprsUseFlag = 1;
					uiMemManaPutSysInfo();	
		            	}
				else 
					return -1;
			#endif	
		        }	else
			{
			#ifndef NO_WIFI
			         //GPRS模块初始化
	            	        vClearLines(2);
			        _vDisp(3, "通讯设置中,请稍候...");
				
				 gl_SysInfo.iCommType = VPOSCOMM_GPRS;	
			        if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
			        {
			            iCommLogout(iOldCommType, 1);
				    uiMemManaPutSysInfo();
			            vClearWilrelessIcon();	  
			            iCommLogin();	
			        }	
		       #endif			
			}
	        }
		else 
		{
			//不允许GPRS
			vMessage("不允许使用GPRS通讯方式");	
			return -1;
		}
		
                break;
            }
            if (uiKey == _KEY_2)
            {
                gl_SysInfo.iCommType = VPOSCOMM_WIFI;
		vClearLines(2);
		_vDisp(3, "通讯设置中,请稍候...");
		if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
		{
			iCommLogout(iOldCommType, 1);
			uiMemManaPutSysInfo();
			vClearWilrelessIcon();
		}
		ret = iWifiConfig(NULL);		
                break;
            }
            if (uiKey == _KEY_ENTER && iOldCommType)
           {
		vClearLines(2);
		_vDisp(3, "通讯设置中,请稍候...");
		if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
		{
			iCommLogout(iOldCommType, 1);
			uiMemManaPutSysInfo();
			vClearWilrelessIcon();
		}

		if (gl_SysInfo.iCommType == VPOSCOMM_WIFI)
			ret = iWifiConfig(NULL);
		else
		{
			uiMemManaPutSysInfo();
			iCommLogin();
		}
           	break;
            }		
            if(uiKey == _KEY_ESC)
                break;
        }
        if(uiKey == _KEY_ESC && iOldCommType==0)		//取消设置
            return 1;
        
        if(uiKey == _KEY_ESC && iOldCommType==gl_SysInfo.iCommType)
            return 0;
/*            
        vClearLines(2);
        _vDisp(3, "通讯设置中,请稍候...");
        if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
        {
            iCommLogout(iOldCommType, 1);
            uiMemManaPutSysInfo();
            vClearWilrelessIcon();
        }

        if (gl_SysInfo.iCommType == VPOSCOMM_WIFI)
            ret = iWifiConfig(NULL);
        else
        {
            uiMemManaPutSysInfo();
            iCommLogin();
        }
*/		
    }else
#endif        
    {
        _vDisp(2, (uchar *)"通讯方式:"); 
        vDispVarArg(3, (uchar *)"1.%s", NetName);
        iOldCommType=gl_SysInfo.iCommType;
        if(gl_SysInfo.iCommType != VPOSCOMM_GPRS)
        {
            gl_SysInfo.iCommType = VPOSCOMM_GPRS;
            uiMemManaPutSysInfo();
        }
        _vSetTimer(&ulTimer, 800);
        while (1)
        {
            if(_uiTestTimer(ulTimer))
                return -1;
            if(!_uiKeyPressed())
                continue;
            //_vSetTimer(&ulTimer, 800);
            uiKey = _uiGetKey();
            if (uiKey == _KEY_1 || uiKey == _KEY_ENTER || uiKey == _KEY_ESC)
                break;
        }
        if(uiKey == _KEY_ESC && iOldCommType==gl_SysInfo.iCommType)
            return 0;

        vClearLines(2);
        _vDisp(3, "通讯设置中,请稍候...");
        ret=iCommLogin();
        dbg("iCommLogin ret:%d\n", ret);
    }   
	
    return 0;
}

int SetNoPinParam(void)
{
	int ret, curr;
	uchar ucPinPos;
	uchar Nbuff[50];
	uchar ucKeyIn;                                    // 接受输入的字符
	uchar nMode;
	int InputCnt;
	ulong ulAmount;
	
	_vCls();
	vDispCenter(1, "免密设置", 1);	
   	_vDisp(2, "免密限额:");	
	memset(Nbuff,0x00,sizeof(Nbuff));
	sprintf(Nbuff, "%lu.%02lu", gl_SysInfo.ulNoPinLimit/100, gl_SysInfo.ulNoPinLimit%100);
         ucPinPos =_uiGetVCols()-strlen(Nbuff);
	_vDispAt(3,ucPinPos,Nbuff);
	
	_vFlushKey();
	nMode = 0x00;
	ulAmount = gl_SysInfo.ulNoPinLimit;
	InputCnt = 0;
        while(1)
    	{
    		if(nMode)
		{
			nMode=0x00;		
			memset(Nbuff,0x00,sizeof(Nbuff));
			sprintf(Nbuff,"%.2f", ulAmount/100.00); 					
			ucPinPos =_uiGetVCols()-strlen(Nbuff);
			_vDisp(3," ");
			_vDispAt(3,ucPinPos,Nbuff);
		}
    		if(_uiKeyPressed())
		{
			ucKeyIn=_uiGetKey();
			if(_KEY_ENTER== ucKeyIn)
			{			      
				break;	
			}
			else if(_KEY_CANCEL== ucKeyIn)
			{
				return 0;
			}
			else if((ucKeyIn >= _KEY_0)&&(ucKeyIn <= _KEY_9)&&(InputCnt<9))
			{
				ulAmount = ulAmount * 10 + (ucKeyIn - _KEY_0);
				nMode=0xaa;
				InputCnt++;
			}
			else if((_KEY_BKSP== ucKeyIn)&& (ulAmount > 0))
			{
				ulAmount =ulAmount / 10;
				nMode=0xaa;				
				if(InputCnt)
					InputCnt--;
			}
			else
			{
				nMode=0x00;
			}
		}
    	}
	if(ulAmount != gl_SysInfo.ulNoPinLimit && ulAmount >0)
	{
		gl_SysInfo.ulNoPinLimit = ulAmount;
		uiMemManaPutSysInfo();
	}

	
	vClearLines(2);
	curr= (gl_SysInfo.ucNoPinFlag == 1)? 1:2;
	ret=iSelectOne("免密标识", "1.启用", "2.关闭", "", curr, 30, 1);
	if(ret<0)
		return -1;
	if(ret!=curr)
	{
	        ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucNoPinFlag=ret;
		uiMemManaPutSysData();
	}

	return 0;
}

int SetNoSignParam(void)
{
	int ret, curr;
	uchar ucPinPos;
	uchar Nbuff[50];
	uchar ucKeyIn;                                    // 接受输入的字符
	uchar nMode;
	int InputCnt;
	ulong ulAmount;

	_vCls();
	vDispCenter(1, "免签设置", 1);
   	_vDisp(2, "免签限额:");	
	memset(Nbuff,0x00,sizeof(Nbuff));
	sprintf(Nbuff, "%lu.%02lu", gl_SysInfo.ulNoSignLimit/100, gl_SysInfo.ulNoSignLimit%100);
         ucPinPos =_uiGetVCols()-strlen(Nbuff);
	_vDispAt(3,ucPinPos,Nbuff);
	
	_vFlushKey();
	nMode = 0x00;
	ulAmount = gl_SysInfo.ulNoSignLimit;
	InputCnt = 0;
        while(1)
    	{
    		if(nMode)
		{
			nMode=0x00;		
			memset(Nbuff,0x00,sizeof(Nbuff));
			sprintf(Nbuff,"%.2f", ulAmount/100.00); 					
			ucPinPos =_uiGetVCols()-strlen(Nbuff);
			_vDisp(3," ");
			_vDispAt(3,ucPinPos,Nbuff);
		}
    		if(_uiKeyPressed())
		{
			ucKeyIn=_uiGetKey();
			if(_KEY_ENTER== ucKeyIn)
			{			      
				break;	
			}
			else if(_KEY_CANCEL== ucKeyIn)
			{
				return 0;
			}
			else if((ucKeyIn >= _KEY_0)&&(ucKeyIn <= _KEY_9)&&(InputCnt<9))
			{
				ulAmount = ulAmount * 10 + (ucKeyIn - _KEY_0);
				nMode=0xaa;
				InputCnt++;
			}
			else if((_KEY_BKSP== ucKeyIn)&& (ulAmount > 0))
			{
				ulAmount =ulAmount / 10;
				nMode=0xaa;				
				if(InputCnt)
					InputCnt--;
			}
			else
			{
				nMode=0x00;
			}
		}
    	}
	if(ulAmount != gl_SysInfo.ulNoSignLimit && ulAmount >0)
	{
		gl_SysInfo.ulNoSignLimit = ulAmount;
		uiMemManaPutSysInfo();
	}

   	vClearLines(2);
	curr= (gl_SysInfo.ucNoSignFlag == 1)? 1:2;
	ret=iSelectOne("免签标识", "1.启用", "2.关闭", "", curr, 30, 1);
	if(ret<0)
		return -1;
	if(ret!= curr)
	{
		ret = (ret == 1) ? 1 : 0;
		gl_SysInfo.ucNoSignFlag=ret;
		uiMemManaPutSysData();
	}

	return 0;
}

int SetSmParam(void)
{
	int ret, curr;
	
	_vCls();
	vDispCenter(1, "国密设置", 1);
	curr= (gl_SysInfo.ucSmFlag== 1)? 1:2;
	ret=iSelectOne("是否支持国密:", "1.支持", "2.不支持", "", curr, 30, 1);
	if(ret<0)
	    return -1;
	if( ret!= curr)
	{
	    ret = (ret == 1) ? 1 : 0;
	    gl_SysInfo.ucSmFlag=ret;
	   gl_SysData.ucPosLoginFlag = 0;
	    uiMemManaPutSysInfo();
	}

	return 0;
}
