#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sys_littlefs.h"
#include "VposFace.h"
#include "pub.h"
#include "debug.h"
#include "Transaction.h"
#include "tag.h"
#include "myHttp.h"
#include "myBase64.h"
#include "utf2gb.h"
#include "func.h"
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "appdebug.h"

void debugTrace(u8 const *pasFun, int line,  char const *pheFmt, ...)
{
    u8 *temp;
    int len =  strlen((char*)pasFun);
    if(len > 25)
		pasFun = &pasFun[len - 25];
	else 
		pasFun= 0;
    
    temp = (u8*)malloc(1024 + 128);
    if( NULL == temp )
    {
        return;
    }
    memset(temp, 0, 1024 + 128);

    
    {
        va_list varg;
        sprintf((char*)temp, "FUN:%s  LINE:%d  \r\n", pasFun, line);
        va_start(varg, pheFmt);//lint !e530  转换不定参数
        vsprintf(&temp[strlen(temp)], pheFmt, varg);
        dbg("%s\r\n",temp);
        va_end(varg);
    }
    if( NULL != temp )
    {
        free( temp);
    }    
    
}

