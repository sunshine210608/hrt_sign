#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "VposFace.h"
#include "Pub.h"

#include "lcdgui.h"
#include "debug.h"
#include "tcpcomm.h"

//#include "lakalaImg.h"
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "Manage_cjt.h"

#include "user_projectconfig.h"
#include "LogoPic.h"

extern int iDoCjtMainMenu(void);
//extern int iTermActive(void);
extern void PMUModeCheck(void);
extern void vSetExitToMainFlag(char flag);

extern void vSetWakeUpEventFlag(int flag);
extern int iGetWakeUpEventFlag(void);
extern int iPosSyncTime(void);
extern int iTcpConnPreProc(char cTipFlag, char *pcDispUpFlag);
void vPrtParam(void);
extern void TermRemindInfo(void);
extern void TermRemindUseInfo(void);
extern int iCardTrans(uint uiTransType);
extern int iMagCard(void);
extern int iInsertCard(void);
extern void vTakeOutCard(void);
extern void vDelSysUpdateFile(char force);
extern int iGetLocalPosition(void);
extern int iDoPosLogin(void);
extern int iDoHardWareTest(void);
extern int iOfflineSettle(void);

static uint sg_uiWirelessSignalTimeOut=30;

extern const struct font_desc Font_18x36;

unsigned char localgDzkCode[]={
	        /* 0xC8D5 [日]   [0]*/
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x00,0x60,0x00,0x01,
		0xFF,0xFF,0xF0,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,
		0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0xFF,0xFF,0xE0,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,
		0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,
		0x00,0x01,0xFF,0xFF,0xE0,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x60,0x00,0x01,0x80,0x00,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

		/* 0xD6D8 [重]   [1]*/
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xC0,0x00,0x00,0x00,0x3F,0xF0,0x00,0x00,0x1F,0xF8,0x00,0x00,0x07,
		0xF8,0xC0,0x00,0x00,0x00,0x00,0xC0,0x00,0x00,0x00,0x00,0xC0,0x0C,0x00,0x1F,0xFF,0xFF,0xFE,0x00,0x00,0x00,0xC0,0x00,0x00,0x00,0x00,0xC0,0x00,0x00,0x01,0x00,0xC0,0x60,0x00,0x01,0xFF,
		0xFF,0xF0,0x00,0x01,0x80,0xC0,0x60,0x00,0x01,0x80,0xC0,0x60,0x00,0x01,0x80,0xC0,0x60,0x00,0x01,0xFF,0xFF,0xE0,0x00,0x01,0x80,0xC0,0x60,0x00,0x01,0x80,0xC0,0x60,0x00,0x01,0x80,0xC0,
		0x60,0x00,0x01,0xFF,0xFF,0xE0,0x00,0x01,0x80,0xC0,0x60,0x00,0x00,0x00,0xC0,0x00,0x00,0x00,0x00,0xC0,0x30,0x00,0x07,0xFF,0xFF,0xF8,0x00,0x00,0x00,0xC0,0x00,0x00,0x00,0x00,0xC0,0x00,
		0x00,0x00,0x00,0xC0,0x08,0x00,0x00,0x00,0xC0,0x0C,0x00,0x1F,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

		/* 0xC6BE [凭]   [2]*/
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x00,0x38,0x00,0xF0,0x00,0x00,0x70,0x0F,0xF8,0x00,0x00,
		0xE1,0xFE,0x00,0x00,0x01,0xCE,0x18,0x00,0x00,0x01,0x80,0x18,0x00,0x00,0x03,0xC0,0x18,0x08,0x00,0x07,0x80,0x18,0x1C,0x00,0x0D,0x9F,0xFF,0xFE,0x00,0x19,0x80,0x18,0x00,0x00,0x21,0x80,
		0x18,0x00,0x00,0x01,0x80,0x18,0x00,0x00,0x01,0x80,0x18,0x10,0x00,0x01,0x80,0x18,0x38,0x00,0x01,0x8F,0xFF,0xFC,0x00,0x01,0x80,0x00,0x00,0x00,0x01,0x80,0x00,0x00,0x00,0x00,0x10,0x06,
		0x00,0x00,0x00,0x1F,0xFF,0x00,0x00,0x00,0x18,0x06,0x00,0x00,0x00,0x18,0x06,0x00,0x00,0x00,0x18,0x06,0x00,0x00,0x00,0x18,0x06,0x00,0x00,0x00,0x30,0x06,0x00,0x00,0x00,0x30,0x06,0x02,
		0x00,0x00,0x60,0x06,0x02,0x00,0x00,0xC0,0x06,0x02,0x00,0x03,0x80,0x06,0x06,0x00,0x0E,0x00,0x07,0xFF,0x00,0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		/* 0xD6A4 [证]   [3]*/
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x04,0x00,0x07,0x00,0x00,0x0C,0x00,0x03,
		0x8F,0xFF,0xFF,0x00,0x01,0x80,0x06,0x00,0x00,0x01,0x80,0x06,0x00,0x00,0x00,0x80,0x06,0x00,0x00,0x00,0x00,0x06,0x00,0x00,0x01,0x00,0x06,0x00,0x00,0x7F,0x80,0x06,0x00,0x00,0x03,0x00,
		0x06,0x00,0x00,0x03,0x03,0x86,0x00,0x00,0x03,0x03,0x06,0x00,0x00,0x03,0x03,0x06,0x0C,0x00,0x03,0x03,0x07,0xFE,0x00,0x03,0x03,0x06,0x00,0x00,0x03,0x03,0x06,0x00,0x00,0x03,0x03,0x06,
		0x00,0x00,0x03,0x03,0x06,0x00,0x00,0x03,0x03,0x06,0x00,0x00,0x03,0x0B,0x06,0x00,0x00,0x03,0x13,0x06,0x00,0x00,0x03,0x23,0x06,0x00,0x00,0x03,0x43,0x06,0x00,0x00,0x03,0x83,0x06,0x00,
		0x00,0x07,0x03,0x06,0x04,0x00,0x06,0x03,0x06,0x0E,0x00,0x02,0x3F,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

};

int localgDzkCodeIndex[] = {
	 /* 0xC8D5 [日]   [0]*/
	0xC8D5,
	/* 0xD6D8 [重]   [1]*/
	0xD6D8,
	/* 0xC6BE [凭]   [2]*/
	0xC6BE,
	/* 0xD6A4 [证]   [3]*/
	0xD6A4
};

void vTestActive(void)
{
    int ret;
    unsigned char sg_sActiveId[30+1], sActiveIdTmp[30+1];
    int iLenIn, iLenOut=0;
    
    vDispCenter(1, "解锁测试", 1);
    _uiGetKey();
    while(1)
    {
        memset(sg_sActiveId, 0, sizeof(sg_sActiveId));
        iLenIn=strlen((char*)sg_sActiveId);
        ret=TMS_Activate_Flow(0, sg_sActiveId, iLenIn, sActiveIdTmp, &iLenOut);
        if(ret==1)
        {
            if(iLenOut>0 && iLenOut<sizeof(sActiveIdTmp))
                sActiveIdTmp[iLenOut]=0;
            strcpy((char*)sg_sActiveId, (char*)sActiveIdTmp);
            
            _vDisp(2, "wait key for unlock");
            _uiGetKey();
            _vDisp(2, "");
            
            iLenIn=strlen((char*)sg_sActiveId);
            ret=TMS_Activate_Flow(1, sg_sActiveId, iLenIn, sActiveIdTmp, &iLenOut);
        }
        _vDisp(6, "继续测试?");
        if(_uiGetKey()==_KEY_ESC)
            break;
        _vCls();
        vDispCenter(1, "解锁测试", 1);
        continue;
    }
    return;
}

uchar g_ucGetTSNFlag=0;

// 应用主程序
void MyApplication(void)
{
	uint uiKey;
	uchar szTUSN[26 + 1], szAppVer[50], szDateTime[14 + 1];
	uchar szTmp[50];
	uchar ucDispUpdateFlag = 1;
	ulong ulWirelessSignalTimer;
	u8 possn[32 + 1] = {0},csn[11+1] = {0};
	char szBuf[40];
	int len;
	 
    	vDelSysUpdateFile(0);

	vTermInit(); // 终端初始化
	
	if(gl_SysInfo.ucPowerOffSwitch == 0)
		_vSetAutoPowerOff(30*60);//秒为单位
	else if(gl_SysInfo.ucPowerOffSwitch == 1)
		_vSetAutoPowerOff(0);//秒为单位		
	
#ifdef ENABLE_PRINTER 	
        PrintLoadFont(&Font_18x36);
	FontSetChnCodeType(FONT_CODE_TYPE_GB2312);
	FontSetChnGB2312BitMap(localgDzkCode, localgDzkCodeIndex,4);		
//	FontSetChnCodeType(FONT_CODE_TYPE_GB2312);
#endif
//	vSetUsbDebug(1);
   	 gl_ucNFCPinFlag=0;  
        gl_ucStandbyCardFlag = 0;
	gl_ucTimeOutFlag = 1;	
	gl_ucProcReqFlag = 0;
	gl_ucSignuploadFlag = 0;
	gl_ucQrTransFlag = 0;
    if (gl_SysInfo.iCommType != VPOSCOMM_GPRS && gl_SysInfo.iCommType != VPOSCOMM_WIFI)
	{
		gl_SysInfo.iCommType = VPOSCOMM_GPRS;
	}

   if( gl_SysInfo.iCommType != VPOSCOMM_GPRS && iPosHardwareModule(CHK_HAVE_WIFI, NULL, 0) <= 0 )
   {
	 	gl_SysInfo.iCommType = VPOSCOMM_GPRS;
		uiMemManaPutSysInfo();
   }

    if(gl_SysInfo.ucQrPreViewFlag  != 1)
    {
    		gl_SysInfo.ucQrPreViewFlag = 0x01;
		uiMemManaPutSysInfo();
    }

     if(gl_SysInfo.uiCommTimeout != 25)
    { 
    	gl_SysInfo.uiCommTimeout = 25;	
	uiMemManaPutSysInfo();
     }
	 
	gprs_at_poweron();  //要不要开机通讯模块就上电
   
	_vCls();	 
	_vFlushKey();	
	vDispMid(3, "初始化通讯,请稍候...");

       if(gl_SysInfo.iCommType == VPOSCOMM_WIFI)
       {
	    	vMessage("基站信息获取中请稍候");
	    	iGetLocalPosition();
		vClearLines(2);
    	}
		
	if(iCommLogin()!=0)
	{
		if(iPosHardwareModule(CHK_HAVE_WIFI, NULL, 0)>0)
			iCommConfig();
	}
	
//	_vCls(); 
	vPrtParam();

#if defined(ST7571) || defined(ST7567)
    _uiGetSerialNo(szTUSN);
#else
	strcpy(szTUSN, " TUSN:");
	_uiGetSerialNo(szTUSN + 6);
#endif    
	_pszGetAppVer(szAppVer);
	_vGetTime(szDateTime);
    
	iGetWirelessSignalQuality();
	_vSetTimer(&ulWirelessSignalTimer, sg_uiWirelessSignalTimeOut*100);

//        _uiMagReset();
 
	gl_SysData.ucPosLoginFlag=0;         
        gl_SysData.ucOperPauseMode=1;	 
       updateLastEventTimestamp();
	while (1)
	{		 		//操作员登录
		while ( gl_SysData.ucOperPauseMode)
		{		
		//        vTakeOutCardMain();
			if(gl_SysData.ucOperPauseMode == 1)
				iDoPosLogin();
			if(gl_SysData.ucOperPauseMode ==  2)
				iDoOperLeavePause();
				vSetExitToMainFlag(0);      //已回主界面,恢复0
				gl_ucTimeOutFlag = 1;
				ucDispUpdateFlag=1;
#if ENABLE_JTAG!=1
        PMUModeCheck();
#endif
		}  
		
		if(iGetWakeUpEventFlag())
		{
			vSetWakeUpEventFlag(0);
			iPosSyncTime();
		}
		                                                    
		if (_uiKeyPressed())
		{
			uiKey=_uiGetKey();
			_vFlushKey();
              
	                if(_KEY_ENTER == uiKey)
	                {
	                    gl_ucStandbyCardFlag = 0; 
	                    iDoCjtMainMenu();
			    if(gl_SysData.ucOperPauseMode == 0)			
			    	ucDispUpdateFlag = 1;
			    else 
				ucDispUpdateFlag = 0;
			//	vTakeOutCardMain();
	                }
			/*		
			else if(_KEY_9 == uiKey)
			{		
			        gl_ucStandbyCardFlag = 0;
				if(gl_SysInfo.ucDefaultTrans == 1)
				{						
					if(gl_SysInfo.ucSaleSwitch == 0)
					{
						TermRemindInfo();
					}
					else
						iCardTrans(TRANS_TYPE_SALE);	
				}
				else if(gl_SysInfo.ucDefaultTrans == 0)
				{		
												
					if(gl_SysInfo.ucAuthSwitch == 0)
					{
						TermRemindInfo();
					}
					else
						iCardTrans(TRANS_TYPE_PREAUTH);
				}	
				ucDispUpdateFlag = 1;
			}*/
			else if(_KEY_DOWN==uiKey)
			{
				 iDoHardWareTest();
				 ucDispUpdateFlag = 1;
			}
		
			if(ucDispUpdateFlag == 1)
			{
				vSetExitToMainFlag(0);
				_vFlushKey();            
			}				
		}
#if 0
               ret = iMagCard(); //刷卡流程
		if(ret == 1 || ret == 2 || ret == 3)  
		{//进入交易流程
		       if(ret == 1)
			   	gl_ucStandbyCardFlag = 1; //1.IC 2 NFC 3.MAG
		       else if(ret == 2)
		      	 	gl_ucStandbyCardFlag = 2;
		       else if(ret == 3)	
			   	gl_ucStandbyCardFlag = 3;
			if(gl_SysInfo.ucDefaultTrans == 1)
				iCardTrans(TRANS_TYPE_SALE);								//消费交易
			else
			{
				 iCardTrans(TRANS_TYPE_PREAUTH);					
			}
			ucDispUpdateFlag = 1;			
		}else if(ret == -1)
		      	ucDispUpdateFlag = 1;

		ret  = iInsertCard(); //插卡流程
		if(ret == 1)                           
		{     //进入交易流程
		        gl_ucStandbyCardFlag = 1;				
			if(gl_SysInfo.ucDefaultTrans == 1)
				iCardTrans(TRANS_TYPE_SALE);								//消费交易
			else
				iCardTrans(TRANS_TYPE_PREAUTH);
			vTakeOutCard();
			ucDispUpdateFlag = 1;			
		}else if(ret == -1)
		      	ucDispUpdateFlag = 1;
#endif	
                
		if((gl_SysData.uiTransNum + gl_SysData.uiQrTransNum) >= gl_SysInfo.ucMaxTradeCnt)
		{
			 iOfflineSettle();
		}
		
		//显示待机界面
		if (ucDispUpdateFlag && gl_SysData.ucOperPauseMode == 0)
		{
			_vCls();			
#ifdef ST7789
#endif
#if defined(ST7567) || defined(LCD_GC9106)
                        vDispCenter(1, "会员宝Pro", 0); 
			 if(gl_SysInfo.szMerchName[0])
			 {
				if(strlen((char *)gl_SysInfo.szMerchName)>_uiGetVCols())
				{
			               strcpy(szBuf, gl_SysInfo.szMerchName);
			                len=iSplitGBStr(szBuf, _uiGetVCols());
			                szBuf[len]=0;
			                vDispCenter(2, szBuf,0);
			                vDispCenter(3, gl_SysInfo.szMerchName+len,0);
				}				
				else
				{
			               vDispCenter(2,gl_SysInfo.szMerchName,0);
				}				
				vDispCenter(4, "按<确认>键,开始交易", 0);
			 }else
			 {
			#ifdef  TESTTUSN
				memcpy(possn, TESTTUSN, strlen(TESTTUSN));				
			#else
				_uiGetSerialNo(possn);	
			#endif
				if(possn[0])
				{
					memcpy(csn,possn+9,11);
					szBuf[0] = 0;
			        	sprintf(szBuf,"SN:%s",csn);
			 		vDispCenter(3, szBuf,0);
			 	}
			 }
			vDispCenter(5, "客服电话400-010-5670",0);
		//	vDispCenter(1, "瑞银信", 0);
		//	_vDispAt(2, 11, "欢迎使用");
		//	_vDispAt(2, 11, szAppVer);
#endif           
 
//            if(g_ucGetTSNFlag)
//            {
//            #if defined(ST7571) || defined(ST7567)
//                _uiGetSerialNo(szTUSN);
//            #else
//                _uiGetSerialNo(szTUSN + 6);
//            #endif
//                g_ucGetTSNFlag=0;
//            }
//			_vDisp(4, szTUSN);
		}

		_vGetTime(szTmp);
		if ((ucDispUpdateFlag || strcmp(szDateTime, szTmp)) && gl_SysData.ucOperPauseMode == 0)
		{
			strcpy(szDateTime, szTmp);
			sprintf(szTmp, "%.4s-%.2s-%.2s %.2s:%.2s:%.2s",szDateTime, szDateTime + 4, szDateTime + 6, szDateTime + 8, szDateTime + 10, szDateTime + 12);
		//	vDispCenter(5, szTmp, 0);
		}
		ucDispUpdateFlag = 0;
		
#if ENABLE_JTAG!=1
        PMUModeCheck();
#endif
        if(_uiTestTimer(ulWirelessSignalTimer))
        {
            iGetWirelessSignalQuality();
            _vSetTimer(&ulWirelessSignalTimer, sg_uiWirelessSignalTimeOut*100);
            
            //检查网络连接情况
            if(_uiCheckBattery()>=1)
                iTcpConnPreProc(1,NULL);
        }		
		_vDelay(1);
	}
}


void vPrtParam(void)
{
	dbg("\n********************************\n");
	dbg("InitFlag:0x%04X\n", gl_SysInfo.ulInitFlag);

	dbg("SysInfo size=%d, SysData size=%d, TransRec size=%d\n", sizeof(gl_SysInfo), sizeof(gl_SysData), sizeof(gl_TransRec));

	dbgHex("TMK", gl_SysInfo.sMasterKey, 16);
	dbgHex("pinK", gl_SysData.sPinKey, 16);
	dbgHex("macK", gl_SysData.sMacKey, 16);
	dbgHex("magK", gl_SysData.sMagKey, 16);
	dbg("\n");

	dbg("PosId:[%s]\n", gl_SysInfo.szPosId);
	dbg("MerchId:[%s]\n", gl_SysInfo.szMerchId);

	dbg("Tms Host:[%s][%d]\n", gl_SysInfo.szTmsHost, gl_SysInfo.uiTmsHostPort);
	dbg("wifi:[%s]\n", gl_SysInfo.szWifiName);

	dbg("Svr:[%s][%d]\n", gl_SysInfo.szSvrIp, gl_SysInfo.uiSvrPort);
	dbg("BakSvr:[%s][%d]\n", gl_SysInfo.szBakSvrIp, gl_SysInfo.uiBakSvrPort);
	dbg("ESignSvr:[%s]\n", gl_SysInfo.szESignSvrIp, gl_SysInfo.uiESignSvrPort);

	dbg("AidNum:[%d]\n", gl_SysInfo.uiAidNum);
	dbg("CaKeyNum:[%d]\n", gl_SysInfo.uiCaKeyNum);

	dbg("TTC:[%d]\n", gl_SysData.ulTTC);
	dbg("BatchNo:[%d]\n", gl_SysData.ulBatchNo);

	dbg("TransNum:[%d]\n", gl_SysData.uiTransNum);
	dbg("QrTransNum:[%d]\n", gl_SysData.uiQrTransNum);
        dbg("szParamVer:[%s]\n", gl_SysInfo.szParamVer);
	dbg("\n");
}

//签字板交易特征码生成: 交易月日[4]+检索参考号[12]共16字节，压缩成8字节，前后4字节异或得到4字节，再转为8字节可见字符
/*交易特征码共8个字节（ASCII表示），构成方式如下：
对于联机交易：数据块（BLOCK）为清算日期（N4，来自15域应答）+检索参考号（N12，来自37域应答），若终端没有获取到15域，用0000代替。
对于离线交易 ：数据块（BLOCK）为批次号（N6，来自60.2域请求）+流水号（N6，来自11域请求）＋0000
上述数据块（BLOCK）的高8个字节和低8个字节压缩BCD异或后得出的4字节结果转为8字节ASCII后为交易特征码。
*/

void vSetWirelessSignalTimer(unsigned int uiTimeOutSec)
{
    sg_uiWirelessSignalTimeOut=uiTimeOutSec;
}
