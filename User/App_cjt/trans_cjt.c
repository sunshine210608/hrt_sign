#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sys_littlefs.h"
#include "VposFace.h"
#include "pub.h"
#include "Pack8583.h"
#include "cJSON.h"
#include "debug.h"
#include "Transaction.h"

#include "tag.h"
#include "myHttp.h"
#include "myBase64.h"
#include "utf2gb.h"
#include "func.h"
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "St8583_cjt.h"
#include "tcpcomm.h"
#include "Manage_cjt.h"
#include "QrTrans_cjt.h"
#include "trans_cjt.h"
#include "formData.h"
#include "pinpad.h"
#include "global.h"

extern int qpbocTransaction(char *szPrompt);
extern void vSetCommNoDisp(uchar disp);
extern int iTcpSendRecv(char *pszSvrIp, char *pszSvrPort, char *pszBakSvrIp, char *pszBakSvrPort,
        uchar *psSnd, uint uiSndLen, uchar *psRsp, uint uiRspSize, uint *puiRcvLen, uint iTimeOutMs,uchar ucRevFlag);
extern void vSetExitToMainFlag(char flag);
extern int iSetConnStatus(char cLinkFlag, char *pszUrl);
extern int iGetSignMsgFd55FromEnv(uchar *psOutData);
extern uint _uiCheckBattery(void);
extern uchar all_para_down_check(void);
extern void updateLastEventTimestamp(void);
extern void vSetCommCallBack(void (*func)(void));
extern int _iSm4(uint uiMode, uchar *psSource, uchar *psKey, uchar *psResult);
extern int iRyxTmsUpdateQuery(void);
extern int iSendFailSignOper(int mode, int *piNum);
extern int iGetQPbocTryFlag(void);
extern uint uiMemManaPutSettleInfo(stSettleInfo *pSettle);
extern int iQuickTransaction(const char *TransType);
extern int iGetEnvCardId(unsigned char *pszCardId, unsigned char *pucCardSeqNo, unsigned char *pszTrack2);
extern int iGetStringGBK(unsigned char *pszStr, int iStrLen);
extern int  DivPublicField047(u8 *pInData,int iLen);
extern void DivPublicField060(u8 *pInData,int iLen);
extern void vMessageMulEx(char *pszMessage);
extern int DispUpdateParamQrCode(char *pszQrCodeStr);
extern void vMessageMulLast2(char *pszMessage);
extern int iDoPosLogin(void);
extern int iBillPreSign(int flag,void *pRec);

void vAppPackPinBlock(uchar *pszCardId, uchar *pszPin, uchar *psKey, int iKeyLen, uchar *psOut);
static int iMagTrans(uint uiTransType, int iEntry, int iVoidRecIdx, ulong ulVoidTTC);

int iPackReq8583(st8583 *pSend8583, char *sCommBuf, uint *puiLen);
int iUnPackRsp8583(char *sCommBuf, uint uiRecvLen, st8583 *pRecv8583);
void vPrt8583Fields(char ucType, st8583 *pMsg8583);
void vPrtTransRecInfo(void);
int iDownloadEmvParam(void);
int iDownloadCAPK(void);
void vGetPrtMerchInfo(uchar *psFields, stTransRec *rec);
int iPosLogin(uchar ucDispMsg);

static int sg_iRevTry=0;
static int sg_iVoidRecIndex;
extern uchar sg_cNoDisp;
extern void serialAsc(const char * sFormat, ...);
extern void serialHex(unsigned char *head, unsigned char *in, unsigned int len);
extern void dbgHex(uchar *head, uchar *in, uint len);
extern void dbgHexEx(uchar *head, uchar *in, uint len,uchar const *pasFun, int displine);
extern void vMessageMulLast3(char *pszMessage);
extern int iPrintAll(void);
extern void vDispRight(uint uiLine, char *pszMsg);
extern uchar ucGetUsbDebug(void);

void GetExpiredData(u8 asTrack2Data[],u8 asExpiredDate[])
{
    u8 i=0;
	if(asTrack2Data==NULL||asExpiredDate==NULL)return;
	dbg("asTrack2Data=[%s]\r\n", asTrack2Data);

//	if(strlen(asTrack2Data)<20)return;
    for(i = 0; i < 20;i++ )
    {
        if((asTrack2Data[i] == 'D') || (asTrack2Data[i] == 'F') ||  (asTrack2Data[i] == '='))
        {
            break;
        }
        
    }


    memcpy(asExpiredDate, &asTrack2Data[i + 1], 4);
	dbg("asExpiredDate=[%s]\r\n", asExpiredDate);

}

#if 1
static int sg_iTVRRevFlag;
//日结消费情况下IC卡校验失败或第二次GAC失败,设置即时冲正
void vSetTransRevFlag(int flag)
{
    dbg("********* vSetTransRevFlag:%d\n", flag);
    sg_iTVRRevFlag=flag;
}

int iGetTransRevFlag(void)
{
    return sg_iTVRRevFlag;
}
#endif

int iGetVoidRecIdx(void)
{
    return sg_iVoidRecIndex;
}
struct stHostErrInfo
{
    uchar code;
    char *pszErrMsg;
};
struct stHostErrInfo sg_HostErrInfo[] = {
    {0x00,  "交易成功"},
    {0x01,	"请持卡人与发卡银行联系"},
    {0x03,	"无效商户"},
    {0x04,	"此卡为无效卡（POS）"},
    {0x05,	"持卡人认证失败"},
    {0x10,	"部分批准金额"},
    {0x11,	"成功,VIP客户"},
    {0x12,	"无效交易"},
    {0x13,	"无效金额"},
    {0x14,	"无效卡号"},
    {0x15,	"此卡无对应发卡方"},
    {0x21,	"该卡未初始化或睡眠卡"},
    {0x22,	"操作有误,或超出交易允许天数"},
    {0x25,	"没有原始交易,请联系发卡方"},
    {0x30,	"请重试"},
    {0x34,	"作弊卡,呑卡"},
    {0x38,	"密码错误次数超限,请与发卡方联系"},
    {0x40,	"发卡方不支持的交易"},
    {0x41,	"挂失卡,请没收(POS)"},
    {0x43,	"被窃卡,请没收"},
    {0x45,	"请使用芯片"},
    {0x51,	"可用余额不足"},
    {0x54,	"该卡已过期"},
    {0x55,	"密码错"},
    {0x57,	"交易关闭,联系发卡行"},
    {0x58,	"发卡方不允许该卡在本终端进行此交易"},
    {0x59,	"卡片校验错"},
    {0x61,	"交易金额超限"},
    {0x62,	"受限制的卡"},
    {0x64,	"交易金额与原交易不匹配"},
    {0x65,	"超出取款次数限制"},
    {0x68,	"交易超时,请重试"},
    {0x75,	"密码错误次数超限"},
    {0x90,	"系统日切,请稍后重试"},
    {0x91,	"发卡方状态不正常,请稍后重试"},
    {0x92,	"发卡方线路异常,请稍后重试"},
    {0x94,	"拒绝,重复交易,请稍后重试"},
    {0x96,	"拒绝,交换中心异常,请稍后重试"},
    {0x97,	"终端号未登记"},
    {0x98,	"发卡方超时"},
    {0x99,	"PIN 格式错,请重新签到"},
    {0xA0,	"MAC 校验错,请重新签到"},
    {0xA1,	"转账货币不一致"},
    {0xA2,	"交易成功,请向资金转入行确认"},
    {0xA3,	"资金到账行账号不正确"},
    {0xA4,	"交易成功,请向资金到账行确认"},
    {0xA5,	"交易成功,请向资金到账行确认"},
    {0xA6,	"交易成功,请向资金到账行确认"},
    {0xA7,	"安全处理失败"},
    {0xAA,  "交易处理中"},
    {0xBB,  "交易失败"},
    {0xFF,  NULL}
};

//显示服务端错误信息
void vShowHostErrMsg(uchar *pszHostErrCode, uchar *pszHostErrMsg)
{
    char szBuf[40],disp[256+1]={0};
    uchar ucErrCode;
    int i;
	int col=0;
	uchar buf[40];

	vClearLines(2);	 
	do{
		if(pszHostErrCode!=NULL&& pszHostErrMsg && pszHostErrMsg[0]){
			memset(disp,0,sizeof(disp));			
			vDispMid(3,pszHostErrCode);
			dbgHex("pszHostErrMsg",pszHostErrMsg,strlen(pszHostErrMsg));		 
			if(strlen((char *)pszHostErrMsg)>_uiGetVCols())
			{
					col=iSplitGBStr(pszHostErrMsg, _uiGetVCols());
					vMemcpy0(buf, (uchar*)pszHostErrMsg, col);
					vDispMid(4,buf);	
					vDispMid(5,pszHostErrMsg +col);	
			}
			else
				vDispMid(4,pszHostErrMsg);	
			vMessage(NULL);
			break;
		}
	    if(strcmp((char*)pszHostErrCode, "96")==0 && pszHostErrMsg && pszHostErrMsg[0])
	    {
	        if(strlen(pszHostErrMsg)>_uiGetVCols())
	        {
	            i=iSplitGBStr(pszHostErrMsg, _uiGetVCols());
	            vMemcpy0(szBuf, pszHostErrMsg, i);
	            _vDisp(_uiGetVLines()-1, szBuf);
	            vMessage(pszHostErrMsg+i);
	        }else
	            vMessage(pszHostErrMsg);
	        break;;
	    }
	    
	    vTwoOne(pszHostErrCode, 2, &ucErrCode);

	    for (i = 0; sg_HostErrInfo[i].pszErrMsg != NULL && sg_HostErrInfo[i].code != ucErrCode; i++)
	        ;
	    if (sg_HostErrInfo[i].pszErrMsg == NULL)
	    {
	        sprintf((char *)szBuf, "卡中心处理失败%2.2s", pszHostErrCode);
	        vMessage(szBuf);
	    }
	    else
	    {
	//        vMessage(sg_HostErrInfo[i].pszErrMsg);
		if (strcmp((char *)pszHostErrCode, "AA") == 0)
			vDispMid(3,"IP");
		else if (strcmp((char *)pszHostErrCode, "BB") == 0)
			vDispMid(3,"IF");
	         else
			vDispMid(3,pszHostErrCode);
		if(strlen((char *)sg_HostErrInfo[i].pszErrMsg)>_uiGetVCols())
		{	  		
			col=iSplitGBStr((uchar*)sg_HostErrInfo[i].pszErrMsg, _uiGetVCols());
			vMemcpy0(buf, (uchar*)sg_HostErrInfo[i].pszErrMsg, col);
			 vDispMid(4,buf);	
			 vDispMid(5,sg_HostErrInfo[i].pszErrMsg +col);	
		}
		else
			vDispMid(4,sg_HostErrInfo[i].pszErrMsg);		
		
		vMessage(NULL);
	    }

	}while(0);
	

    return;
}

extern void vGetCurSvrIP(char *ip, char *port);
static int sg_iCurTransIpIdx=0;
static int sg_iCurESignIpIdx=0;

int iDecryptWk(void)
{
	u32 iLen= 0;
	uchar sTmp[40];

#if 0
    memset(gl_Recv8583.Field62,0x00,sizeof(gl_Recv8583.Field62));
    memcpy(gl_Recv8583.Field62,"\x00\x64\x01\x78\xAE\x77\xC1\x62\x1B\x55\x15\xDF\x0C\x8E\x3A\x59\x0A\x0D\x14\x75\x6D\x4C\x87\x1E\x81\xF8\xA7\x02\x3A\xD1\x80\x13\x71\x49\xCB\x63\xD6\x62\xAE\x9F\x88\x41\x8E\xC3\xF8\x63\x20\x44\xC4\xFA\x01\x81\x03\xCD\xC0\xF9\x98\x64\x6E\xA2\xBA\xE2\x88\x91\x7D\xB7\x7A\x8A\x7D\x9D\x9A\xD4\xAC\x12\xB5\xFA\x57\x00\x81\xE7\x59\x8A\x0D\xB2\xC9\xD5\x1D\x45\x1E\x7C\xDE\xA7\xDB\x7F\x43\xF8\xC5\x6B\xA0\xD1\xF3\x4A",102);
    TraceHex("DE621",&gl_Recv8583.Field62[2],100);
#endif

    //key
    if (gl_Recv8583.Field62[0] || gl_Recv8583.Field62[1])
    {
        iLen=ulStrToLong(gl_Recv8583.Field62,2);//gl_Recv8583.Field62[0]*256+gl_Recv8583.Field62[1];
		dbg("iLen = %d \n",iLen);

        if(iLen!=24 && iLen!=40 && iLen!=56 && iLen != 60 && iLen != 84)
        {
            vMessage("签到响应信息错");
            return -1;
        }

#ifndef NO_SEC_FORDEMO	
		//使用安全模块内的主密钥
		{
			uchar tmk[16];
			int ret;
			memset(tmk, 0, sizeof(tmk));
	           	ret=LoadTMK(tmk);        
			if(ret)
			{
				char szTmp[100];
	           		 //装载密钥失败
	            		sprintf(szTmp, "LoadTmk Fail:%d", ret);
	           		 dbg("%s\n", szTmp);
				vMessage(szTmp);
				return -1;

			}
	       		 memcpy(gl_SysInfo.sMasterKey, tmk, 16);     
		} 
#endif	

        if(gl_SysInfo.ucSmFlag==0)
        {
            if(iLen==24)
            {
		 _vDes(TRI_DECRYPT, gl_Recv8583.Field62 + 2, gl_SysInfo.sMasterKey, gl_SysData.sPinKey);
                memset(sTmp, 0, 8);
                _vDes(ENCRYPT, sTmp, gl_SysData.sPinKey, sTmp+8);
                if(memcmp(gl_Recv8583.Field62 + 2 +8, sTmp+8, 4))
                {
                    dbg("cal pinkey:", gl_SysData.sPinKey, 8);
                    dbg("cal pinkey ckv:", sTmp+8, 4);
                    vMessage("PinKey校验错");
                    return -1;
                }
                memcpy(gl_SysData.sPinKey+8, gl_SysData.sPinKey, 8);
                
                _vDes(TRI_DECRYPT, gl_Recv8583.Field62 + 2 + 12, gl_SysInfo.sMasterKey, gl_SysData.sMacKey);
                memset(sTmp, 0, 8);
                _vDes(ENCRYPT, sTmp, gl_SysData.sMacKey, sTmp+8);
                if(memcmp(gl_Recv8583.Field62+2+12+8, sTmp+8, 4))
                {
                    vMessage("MacKey校验错");
                    return -1;
                }
            }else if(iLen==40)
            {
                _vDes(TRI_DECRYPT, gl_Recv8583.Field62+2, gl_SysInfo.sMasterKey, gl_SysData.sPinKey);
                _vDes(TRI_DECRYPT, gl_Recv8583.Field62+2+8, gl_SysInfo.sMasterKey, gl_SysData.sPinKey+8);
                memset(sTmp, 0, 8);
                _vDes(TRI_ENCRYPT, sTmp, gl_SysData.sPinKey, sTmp+8);
                if(memcmp(gl_Recv8583.Field62+2+16, sTmp+8, 4))
                {
                    vMessage("PinKey校验错");
                    return -1;
                }
                
                _vDes(TRI_DECRYPT, gl_Recv8583.Field62+2+20, gl_SysInfo.sMasterKey, gl_SysData.sMacKey);
                memset(sTmp, 0, 8);
                _vDes(ENCRYPT, sTmp, gl_SysData.sMacKey, sTmp+8);
                if(memcmp(gl_Recv8583.Field62+2+20+16, sTmp+8, 4))
                {
                    vMessage("MacKey校验错");
                    return -1;
                }
            }else if(iLen == 60)
       	    {
                _vDes(TRI_DECRYPT, gl_Recv8583.Field62+2, gl_SysInfo.sMasterKey, gl_SysData.sPinKey);
                _vDes(TRI_DECRYPT, gl_Recv8583.Field62+2+8, gl_SysInfo.sMasterKey, gl_SysData.sPinKey+8);
                memset(sTmp, 0, 8);
                _vDes(TRI_ENCRYPT, sTmp, gl_SysData.sPinKey, sTmp+8);
                if(memcmp(gl_Recv8583.Field62+2+16, sTmp+8, 4))
                {
                    vMessage("PinKey校验错");
                    return -1;
                }
                
                _vDes(TRI_DECRYPT, gl_Recv8583.Field62+2+20, gl_SysInfo.sMasterKey, gl_SysData.sMacKey);
                memset(sTmp, 0, 8);
                _vDes(ENCRYPT, sTmp, gl_SysData.sMacKey, sTmp+8);
                if(memcmp(gl_Recv8583.Field62+2+20+16, sTmp+8, 4))
                {
                    vMessage("MacKey校验错");
                    return -1;
                }

	        _vDes(TRI_DECRYPT, gl_Recv8583.Field62+2 + 20 + 20, gl_SysInfo.sMasterKey, gl_SysData.sMagKey);
                _vDes(TRI_DECRYPT, gl_Recv8583.Field62+2+20 + 20+8, gl_SysInfo.sMasterKey, gl_SysData.sMagKey+8);
                memset(sTmp, 0, 8);
                _vDes(TRI_ENCRYPT, sTmp, gl_SysData.sMagKey, sTmp+8);
                if(memcmp(gl_Recv8583.Field62+2+ 20 + 20 + 16, sTmp+8, 4))
                {
                    vMessage("TdkKey校验错");
                    return -1;
                }
          }
	    else if(iLen==56 || iLen == 80)
            {
                vMessage("密钥长度不支持");
                return -1;
            }
            dbgHex("pin text", gl_SysData.sPinKey, 16);
            dbgHex("mac text", gl_SysData.sMacKey, 8);
	    dbgHex("tdk text", gl_SysData.sMagKey, 16);
        }		
	else    
        {
		if(iLen==40)
		{
			_iSm4(DECRYPT, gl_Recv8583.Field62+2, gl_SysInfo.sMasterKey, gl_SysData.sPinKey);
			memset(sTmp, 0, 16);
			_iSm4(ENCRYPT, sTmp, gl_SysData.sPinKey, sTmp+16);
			if(memcmp(gl_Recv8583.Field62+2+16, sTmp+16, 4))
			{
				vMessage("PinKey校验错");
				return -1;
			}

			_iSm4(DECRYPT, gl_Recv8583.Field62+2+20, gl_SysInfo.sMasterKey, gl_SysData.sMacKey);
			memset(sTmp, 0, 16);
			_iSm4(ENCRYPT, sTmp, gl_SysData.sMacKey, sTmp+16);
			if(memcmp(gl_Recv8583.Field62+2+20+16, sTmp+16, 4))
			{
				vMessage("MacKey校验错");
				return -1;
			}
		}
		else if(iLen == 60)
		{
			_iSm4(DECRYPT, gl_Recv8583.Field62+2, gl_SysInfo.sMasterKey, gl_SysData.sPinKey);
			memset(sTmp, 0, 16);
			_iSm4(ENCRYPT, sTmp, gl_SysData.sPinKey, sTmp+16);
			if(memcmp(gl_Recv8583.Field62+2+16, sTmp+16, 4))
			{
				vMessage("PinKey1校验错");
				return -1;
			}

			_iSm4(DECRYPT, gl_Recv8583.Field62+2+20, gl_SysInfo.sMasterKey, gl_SysData.sMacKey);
			memset(sTmp, 0, 16);
			_iSm4(ENCRYPT, sTmp, gl_SysData.sMacKey, sTmp+16);
			if(memcmp(gl_Recv8583.Field62+2+20+16, sTmp+16, 4))
			{
				vMessage("MacKey1校验错");
				return -1;
			}

			_iSm4(DECRYPT, gl_Recv8583.Field62+2 + 20 + 20, gl_SysInfo.sMasterKey, gl_SysData.sMagKey);
		    	 memset(sTmp, 0, 16);
			_iSm4(ENCRYPT, sTmp, gl_SysData.sMagKey, sTmp+16);
			if(memcmp(gl_Recv8583.Field62+2+ 20 + 20 + 16, sTmp+16, 4))
			{
				vMessage("TdkKey1校验错");
				return -1;
			}				
		}
	            dbgHex("pin text", gl_SysData.sPinKey, 16);
	            dbgHex("mac text", gl_SysData.sMacKey, 16);
		    dbgHex("tdk text", gl_SysData.sMagKey, 16);
        }

	TraceHex("master text", gl_SysInfo.sMasterKey, 16);
        TraceHex("pin text", gl_SysData.sPinKey, 16);
        TraceHex("mac text", gl_SysData.sMacKey, 16);
	TraceHex("mag text", gl_SysData.sMagKey, 16);
    }
	return 0x00;
}

static int iHttpSndRcv8583Msg(st8583 *pSnd8583, st8583 *pRcv8583,uchar ucRevFlag)
{
   uchar szHostName[256 + 1], szSubUrl[256 + 1];
    char szSvrIp[20], szSvrPort[10];
    char szBakSvrIP[20], szBakSvrPort[10];
    char ip[20], port[10];
    int iEsign=0;    
    uchar sCommBuf[2 * 1024]={0};
    uint uiSendLen, uiRecvLen;
    int ret;
   uint uiPort = 0;
   
    memset(sCommBuf,0,sizeof(sCommBuf));
	uiSendLen = 0;
    ret = iPackReq8583(pSnd8583, sCommBuf, &uiSendLen);
    if (ret)
    {
        dbg("pack 8583 err:%d  FUN:%s  LINE:%d \n", ret,(u8 const*)__FUNCTION__, __LINE__);
        return -1*COM_ERR_PACK;
    }
	
    
    if(uiSendLen>sizeof(sCommBuf))
    {
        dbg("err.8583 long:%d\n", uiSendLen);
        return -1*COM_ERR_PACK;
    }
#if 1
    if(gl_SysInfo.uiCommTimeout<10)
        gl_SysInfo.uiCommTimeout=60;

	    if(gl_SysInfo.ucDomainOrIp == 1)	
    	{    	       
    	         if(!sg_cNoDisp)
    	       {
    	                vDispMid(3, "域名解析...");
	    	}
		ret = iParseHttpUrl(gl_SysInfo.szSvrDomain, szHostName, szSubUrl, &uiPort);
		if (ret)
		{
			vMessage("解析域名失败");
			return -1*COM_PARAMETER;
		}
		ret = iGetHostByName((char*)szHostName, szSvrIp);		
		if(ret)
		{
			vMessage("解析域名IP失败");
			return -1*COM_PARAMETER;
		}				
		sprintf(szSvrPort, "%d", gl_SysInfo.uiSvrPort);         
		   
		ret = iParseHttpUrl(gl_SysInfo.szBakSvrDomain, szHostName, szSubUrl, &uiPort);
		if (ret)
		{
			vMessage("解析备用域名失败");
			return -1*COM_PARAMETER;
		}
		ret = iGetHostByName((char*)szHostName, szBakSvrIP);
		if(ret)
		{
			vMessage("解析备用域名IP失败");
			return -1*COM_PARAMETER;
		}
		sprintf(szBakSvrPort, "%d", gl_SysInfo.uiBakSvrPort);
		
    	}
    	else if(gl_SysInfo.ucDomainOrIp== 0)	
    	{
		strcpy(szSvrIp, (char*)gl_SysInfo.szSvrIp);
		sprintf(szSvrPort, "%d", gl_SysInfo.uiSvrPort);
		
		strcpy(szBakSvrIP, (char*)gl_SysInfo.szBakSvrIp);
		sprintf(szBakSvrPort, "%d", gl_SysInfo.uiBakSvrPort);
	}
			
	dbg("szSvrIp:%s szSvrPort:%s uiSendLen:%d FUN:%s LINE:%d\n", szSvrIp,szSvrPort,uiSendLen,(u8 const*)__FUNCTION__, __LINE__);
	
	dbg("start to call iTcpSendRecv\r\n");
	_vDelay(10);	
    ret = iTcpSendRecv(szSvrIp, szSvrPort, szBakSvrIP, szBakSvrPort, sCommBuf, uiSendLen, sCommBuf, sizeof(sCommBuf), &uiRecvLen, gl_SysInfo.uiCommTimeout*1000,ucRevFlag);
    updateLastEventTimestamp();         //更新休眠时间戳
    vGetCurSvrIP(ip, port);
    if(ip[0])
    {
        if(iEsign)
        {
            if(strcmp(ip, (char*)gl_SysInfo.szESignSvrIp)==0 && atoi(port)==gl_SysInfo.uiESignSvrPort)
                sg_iCurESignIpIdx=0;
            else
                sg_iCurESignIpIdx=1;
        }else
        {
            if(strcmp(ip, (char*)gl_SysInfo.szSvrIp)==0 && atoi(port)==gl_SysInfo.uiSvrPort)
                sg_iCurTransIpIdx=0;
            else
                sg_iCurTransIpIdx=1;
        }
    }	
    if(ret)
    {
        return ret;
    }
#endif
/*
if(gl_TransRec.uiTransType == TRANS_TYPE_SALE)
{    memset(sCommBuf,0x00,sizeof(sCommBuf));
      uiRecvLen = 227;
      memcpy(sCommBuf,"\x00\xE1\x60\x00\x06\x00\x04\x13\x04\x02\x10\x70\x3C\x01\x80\x0A\xE2\x82\x09\x19\x06\x21\x21\x42\x10\x00\x00\x00\x00\x36\x00\x00\x00\x00\x00\x00\x00\x22\x22\x00\x00\x30\x14\x51\x01\x12\x06\x29\x11\x00\x04\x00\x31\x33\x34\x30\x31\x34\x35\x31\x30\x30\x30\x36\x30\x30\x35\x36\x31\x37\x35\x37\x37\x38\x44\x38\x36\x34\x30\x30\x30\x30\x30\x30\x30\x30\x36\x30\x30\xBE\xFD\xB8\xF3\xB4\xF3\xBE\xC6\xB5\xEA\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x00\x23\x38\x36\x34\x31\x30\x30\x30\x37\x30\x31\x31\x36\x35\x30\x36\x37\x39\x30\x38\x32\x32\x39\x38\x01\x56\x00\x36\x9F\x36\x02\x07\x7E\x91\x0A\x24\xB8\x15\xA5\x45\xB5\x52\x7F\x30\x30\x72\x11\x86\x0F\x04\xDA\x9F\x79\x0A\x00\x00\x00\x05\x00\x00\x5F\xF6\xC9\xBE\x00\x23\x30\x30\x30\x30\x30\x31\x30\x31\x20\x30\x30\x30\x30\x33\x30\x30\x31\xBD\xE8\xBC\xC7\xBF\xA8\x2A\x52\xEA\x86\x5C\xBB\xDA\x6A",uiRecvLen);   
}
     dbgHex("sCommBuf", sCommBuf, uiRecvLen);
*/	 
    ret = iUnPackRsp8583(sCommBuf, uiRecvLen, pRcv8583);
    if (ret)
    {
        dbg("unpack 8583 err:%d\n", ret);
        return COM_ERR_UNPACK;
    }

    #ifdef DUP_TEST
		{
		uint uiKey;
		
		if (gl_TransRec.uiTransType == TRANS_TYPE_SALE ||  gl_TransRec.uiTransType == TRANS_TYPE_DAIRY_SALE ||  gl_TransRec.uiTransType  == TRANS_TYPE_SALEVOID
			|| gl_TransRec.uiTransType == TRANS_TYPE_PREAUTH || gl_TransRec.uiTransType  == TRANS_TYPE_PREAUTHVOID ||  gl_TransRec.uiTransType == TRANS_TYPE_PREAUTH_COMP
			||  gl_TransRec.uiTransType == TRANS_TYPE_PREAUTH_COMPVOID )
		{
	        	vClearLines(2);
	//		vDispMid(3,gl_Recv8583.Field63+3);		
			_vDisp(5,"模拟收不到数据");                        

			_vFlushKey();
			while(1)
			{				
				if(_uiKeyPressed())
				{
					uiKey=_uiGetKey();
					if(uiKey==_KEY_ENTER)
					{
						ret = COM_ERR_RECV;

					        break;
					}
					else if(uiKey==_KEY_CANCEL)								    
					{
						ret = 0;

					        break;
					}
				}		
			}
			return ret;
		}
	}
    #endif	

    return 0;
}

void vCleanReverse(void)
{
    uiMemManaErase8583(MSG8583_TYPE_REV);
    sg_iRevTry=0;
}

void vCleanScript(void)
{
	uiMemManaErase8583(MSG8583_TYPE_SCR);
	sg_iRevTry=0;
}

//立即冲正处理
int iLiJiProcReverse(st8583 *pSnd8583, st8583 *pRcv8583)
{
    //银联不支持隔日冲正
    int ret;
    st8583 *pSend8583;
    st8583 *pRecv8583;
    int iRet=0;
    
    //vSetTransRevFlag(0);
    pSend8583=NULL;
    pRecv8583=NULL;
    if(uiMemManaIfExist8583(MSG8583_TYPE_REV)==0)
    {
        return 0;
    }
	
    if(pSnd8583==NUL)
        pSend8583=malloc(sizeof(st8583));
    else
        pSend8583=pSnd8583;
    
    if(pRcv8583==NULL)
        pRecv8583=malloc(sizeof(st8583));
    else
        pRecv8583=pRcv8583;
    
    if(pSend8583==NULL || pRecv8583==NULL)
    {
        //vPrtLklDebugLog(2, 1, "错误", "内存不足,无法生成冲正报文,交易退出.\n", 0);
        vMessage("内存不足");
        iRet=9;
    }
    
    if(pSend8583 && pRecv8583 && uiMemManaGet8583(MSG8583_TYPE_REV, pSend8583)==0)
    {
        dbg("start Reverse 8583 proc...\n");

        vClearLines(2);
        //vDispVarArg(2, "上送冲正信息%d,请稍候", i+1);    
        vDispMid(2, "冲正处理");                
        ret = iHttpSndRcv8583Msg(pSend8583, pRecv8583,0);
        vClearLines(2);
        if(ret==0 && (strcmp((char*)pRecv8583->Field39, "00")==0 || strcmp((char*)pRecv8583->Field39, "12")==0
                || strcmp((char*)pRecv8583->Field39, "25")==0))
        {
            vCleanReverse();
            //_vDisp(2, "冲正成功");
            //_vDisp(3, "上送当前交易请求...");
            //_vDelay(100);
            iRet=0;
        }else
        {
                iRet=1;        
        }

    vClearLines(2);
    }
    if(pSnd8583==NULL && pSend8583)
        free(pSend8583);
    if(pRcv8583==NULL && pRecv8583)
        free(pRecv8583);
    return iRet;
}

//冲正处理
int iProcReverse(st8583 *pSnd8583, st8583 *pRcv8583)
{
    //银联不支持隔日冲正
    uchar szDateTime[14+1];
    uchar szDate[5] = {0};	
    int time = 0;		
    int ret;
    st8583 *pSend8583;
    st8583 *pRecv8583;
    int iRet=0;
    int i;
    
    //vSetTransRevFlag(0);
    pSend8583=NULL;
    pRecv8583=NULL;
    if(uiMemManaIfExist8583(MSG8583_TYPE_REV)==0)
    {
        sg_iRevTry=0;
        return 0;
    }

    sg_iRevTry=0;
	
    if(pSnd8583==NUL)
        pSend8583=malloc(sizeof(st8583));
    else
        pSend8583=pSnd8583;
    
    if(pRcv8583==NULL)
        pRecv8583=malloc(sizeof(st8583));
    else
        pRecv8583=pRcv8583;
    
    if(pSend8583==NULL || pRecv8583==NULL)
    {
        //vPrtLklDebugLog(2, 1, "错误", "内存不足,无法生成冲正报文,交易退出.\n", 0);
        vMessage("内存不足");
        iRet=9;
    }
    
    if(pSend8583 && pRecv8583 && uiMemManaGet8583(MSG8583_TYPE_REV, pSend8583)==0)
    {
        dbg("start Reverse 8583 proc...\n");
        _vGetTime(szDateTime);
        memcpy(szDate,szDateTime+8,4);
	time = atoi(szDate);
        if( pSend8583->Field13[0] && (( memcmp(szDateTime+4, pSend8583->Field13, 4)) ||( gl_SysInfo.ucCleanReverseFlag == 1 && memcpy(szDateTime+4,pSend8583->Field13,4) ==0 && time >=  atoi(gl_SysInfo.ucTimeCleanReverse))))
        {
            //日期不同,删冲正
            dbg("Reverse 8583 date expire, delete!!! msgdate=[%.4s], currdate=[%4s]\n", pRecv8583->Field13, szDateTime+4);
            vPrt8583Fields(1, pSend8583);
            vCleanReverse();
            iRet=0;
        }else
        {
            for(i=sg_iRevTry; i<gl_SysInfo.ucMaxResendCount; i++)     //改为连续冲正模式
            {
                vClearLines(2);
                //vDispVarArg(2, "上送冲正信息%d,请稍候", i+1);    
                vDispMidVarArg(2, "冲正处理%d次", i+1);                
                ret = iHttpSndRcv8583Msg(pSend8583, pRecv8583,0);
                vClearLines(2);
                if(ret==0 && (strcmp((char*)pRecv8583->Field39, "00")==0 || strcmp((char*)pRecv8583->Field39, "12")==0
                        || strcmp((char*)pRecv8583->Field39, "25")==0))
                {
                    vCleanReverse();
                    //_vDisp(2, "冲正成功");
                    //_vDisp(3, "上送当前交易请求...");
                    //_vDelay(100);
                    iRet=0;
                    break;
                }else
                {
                    if(++sg_iRevTry>=gl_SysInfo.ucMaxResendCount)
                    {
                        //冲正重复发送次数超限,删冲正
                        vCleanReverse();
                        //vClearLines(2);
                        //_vDisp(3, "冲正不成功");
                        //_vDisp(4, "冲正次数超限,清除");
                        //_vDelay(100);
                        iRet=0;
                        break;
                    }else
                    {
                        //_vDisp(3, "冲正不成功");
                        //_vDelay(100);
                        iRet=1;
                        continue;
                    }
                }
            }
            vClearLines(2);
        }
    }
    if(pSnd8583==NULL && pSend8583)
        free(pSend8583);
    if(pRcv8583==NULL && pRecv8583)
        free(pRecv8583);
    return iRet;
}

int iHttpSendRecv8583(uchar ucRevFlag)
{
//    uint uiRet;
    int ret;
//    uchar ucCode;
    
    #if 1
    //签到交易或激活绑定交易或参数下载不需要先发送脚本通知和冲正报文
    if(memcmp(gl_Send8583.Msg01, "08",2))
    {
        //联机时先发送脚本通知8583, 再发送冲正8583, 之后才发送当前联机交易8583
        //脚本8583
        /*
        if(uiMemManaGet8583(MSG8583_TYPE_SCR, &upSnd8583)==0)
        {
            ret = iHttpSndRcv8583Msg(&upSnd8583, &upRcv8583,0);
            if(ret==0)
            {
                if(strcmp((char*)upRcv8583.Field39, "00")==0)
                {
                    uiMemManaErase8583(MSG8583_TYPE_SCR);
                }
            }
        }
        
        */
        
        //检查处理待冲正报文
        iProcReverse(NULL, NULL);

#if 0        
         if(uiMemManaIfExist8583(MSG8583_TYPE_SCR))
        {
            //已有脚本通知结果未成功发送
            dbg("have script result need send.\n");
            vClearLines(2);
            _vDisp(_uiGetVLines()-1, "有待上送的脚本通知结果");
            vMessage("当前交易失败");
            return 1;
        }
		   
        if(uiMemManaIfExist8583(MSG8583_TYPE_REV))
        {
            //已有冲正未成功发送
            dbg("have rev trans need send.\n");
            vClearLines(2);
            _vDisp(_uiGetVLines()-1, "有待上送的冲正交易");
            vMessage("当前交易失败");
            return 1;
        }
#endif	
    }
    #endif

    dbg("start to call iHttpSndRcv8583Msg...\n");
    ret = iHttpSndRcv8583Msg(&gl_Send8583, &gl_Recv8583,ucRevFlag);   
	dbg("iHttpSndRcv8583Msg ret=%d\n",ret);
    if(ucRevFlag && ret<=0)
    {
        if(ret<0 || (ret==0 && strcmp((char*)gl_Recv8583.Field39, "00")))
        {
            //未成功发送,删冲正; 服务端明确拒绝的交易,删冲正
            //注意，服务端处理成功（39域返回00）暂不删冲正, 接触式IC卡写卡失败还需发起冲正
            //(39域返回00时:IC卡和磁条卡保存成功交易记录时清冲正)
            uiMemManaErase8583(MSG8583_TYPE_REV);
        }
    }
    if(ret)
    {
        char szBuf[50];
        vClearLines(2);
        switch(ret)
        {
            case -1*COM_ERR_PACK:
                strcpy(szBuf, "打包失败");
                break;
            case -1*COM_ERR_NO_CONNECT:
                strcpy(szBuf, "连接失败");
                break;
            case -1*COM_ERR_SEND:
                strcpy(szBuf, "发送失败");
                break;
            case COM_ERR_RECV:
                strcpy(szBuf, "接收失败");
                break;
            case COM_ERR_UNPACK:
                strcpy(szBuf, "解包失败");
                break;
            default:
                 strcpy(szBuf, "通讯失败");
        }
        strcat(szBuf,",交易未成功");
        vMessage(szBuf);
    }
    return ret;
}

/* 
//59域不需要送，基站信息改63.2
void vGenFiled59(uchar *pszFd59)
{
    char lac[10], cellid[10], iccid[60];
    char *p;

    p=(char*)pszFd59;
    if(iGetLACandCell(lac, cellid)==0 && strlen(lac)==5)
    {
        sprintf(p+strlen(p), "11%02X%.5s%.5s", 5+5, lac, cellid);
    }
    iGetICCID(iccid);
    if(iccid[0])
        sprintf(p+strlen(p), "12%02X%.50s", strlen(iccid), iccid);
}
*/

void vGenFiled63CellInfo(uchar *pszFd63)
{
    char cell[30], iccid[50];
    int lentmp;
    
    sprintf((char *)pszFd63, "%-3.3s", gl_SysData.szCurOper);   //63.1
 
    iGetGprsCellInfo(cell, NULL);
    iGetICCID(iccid);

    if(cell[0]) //63.2
    {
        lentmp=5+strlen(cell);
        pszFd63[strlen((char*)pszFd63)]=((lentmp/10)<<4) + lentmp%10;
        strcat((char *)pszFd63, "GUTE:");
        strcat((char *)pszFd63, cell);
        if(iccid[0])    //63.3
        {
            lentmp=strlen(iccid);
            pszFd63[strlen((char*)pszFd63)]=((lentmp/10)<<4) + lentmp%10;
            strcat((char *)pszFd63, iccid);
        }
    }
}

//参数下载
int iPosUpdateParam(void)
{
	uchar szDateTime[14 + 1],temp[128+1]={0},possn[20+1]={0},csn[11+1] = {0};
	int iRet;
	int i=0;
        u16 len=0;
        uint uiKey;
	ulong ulTimer;
		
	_vCls();
	gl_ucQrTransFlag = 1;
	vDispCenter(1, "参数下载", 1);

	if(_uiCheckBattery()<=1)
	{
	    vMessage("电池电量低,请先充电");
	    return 1;
	}
        	
	_vGetTime(szDateTime);
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
	strcpy((char *)gl_Send8583.Msg01, "0800");
        strcpy((char *)gl_Send8583.Field03, "550000");
		
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	vIncTTC();

	strcpy((char *)gl_Send8583.Field24, "008");
	
	//47域
	memcpy(temp,"01",2);
	i+=2;

#ifdef TESTTUSN
	memcpy(possn, TESTTUSN, strlen(TESTTUSN));
//        strcpy(csn,"111111111111111");
        memcpy(csn,possn+9,11);
#else
       _uiGetSerialNo(possn);
	memcpy(csn,possn+9,11);
#endif
	memcpy(&temp[i],csn,strlen(csn));
	i+=strlen(csn);
	
	memcpy(gl_Send8583.Field47, temp,i);
 
	//打8583包-填充http-发送-接收-解http-解8583包
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
	    //显示错误信息
	    return iRet;
	}

	if (strcmp((char *)gl_Recv8583.Field39, "00") != 0 && strcmp((char *)gl_Recv8583.Field39, "25") !=0 )
	{
		//显示服务端错误信息
		vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);
		return -1;
	}
		   
	//47域
	if(gl_Recv8583.Field47[0])
	{
		DivPublicField047(gl_Recv8583.Field47,strlen(gl_Recv8583.Field47));
	}

        //60域
        if(gl_Recv8583.Field60[0] && strcmp((char *)gl_Recv8583.Field39, "25") == 0)
	{
		len = strlen(gl_Recv8583.Field60);
	        DivPublicField060(gl_Recv8583.Field60,len);       
                
	    //显示二维码	
	    	 _vCls();
	//	_vSetBackLight(1);				
                DispUpdateParamQrCode(gl_SysInfo.ucMerchantQr);
#if defined(LCD_GC9106) 
		vMessageMulLast3(gl_SysInfo.ucMerchantMsg);
#else
                vMessageMulLast2(gl_SysInfo.ucMerchantMsg);
#endif

                	_vSetTimer(&ulTimer, 6000L); //60秒		
			_vFlushKey();
			while(1)
			{
				if(_uiKeyPressed())
				{
					uiKey=_uiGetKey();
					if(uiKey==_KEY_ENTER ||uiKey== _KEY_ESC)
						break;
				}	
				if(_uiTestTimer(ulTimer))
					break;	
			}

		_vCls();
		vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);

	        gl_SysData.ucPosLoginFlag=0;
    		uiMemManaPutSysData();
			
		return -1;		
	} 
        
	strcpy(temp,"终端参数更新成功");
	vClearLines(2);
	vDispMid(3, temp);
	
    return 0;
}

bool ParseBasicParam(u8 temp[],u16 totalLen)
{
	u16 curlen = 0;
	u8 i;
	u8 tempdata[120 +1];
	u8 tag;
        u8 PaySwitch;
	uchar *ptr = NULL;
	
	while( curlen < totalLen )
	{
		vTwoOne( &temp[curlen], 2 ,&tag);
               
		if(tag != 0x11 && tag != 0x12 && tag != 0x13)	
		{
		        vClearLines(2);
			vMessage("终端菜单参数错误");
			return FALSE;
		}
			
		switch(tag)
		{
			case 0x11:  //交易位图
				curlen += 2;
				
				memset(tempdata,0x00,sizeof(tempdata));
				memcpy( tempdata, &temp[curlen], 32 );				

                                ptr = tempdata;
                               for(i = 0;i< 32;i++) 
                               	{                
                               		if(i!=0)
						ptr+=1;
									
						PaySwitch=*ptr; 
						PaySwitch&=0x0f;
					if(i==0)
						gl_SysInfo.ucBalanceSwitch=PaySwitch; // 查询
					else if(i==1)
						gl_SysInfo.ucSaleSwitch=PaySwitch;//消费开关
					else if(i==2)
					 	gl_SysInfo.ucVoidSwitch=PaySwitch;//消费撤销
					else if(i==3)
						gl_SysInfo.ucRefundSwitch=PaySwitch;//退款
					else if(i==4)
					 	gl_SysInfo.ucAuthSwitch=PaySwitch;//预授权
					else if(i==5)
					 	gl_SysInfo.ucAuthCancelSwitch=PaySwitch;//预授权撤销
					else if(i==6)
						gl_SysInfo.ucAuthCompleteSwitch=PaySwitch; //预授权完成
					else if(i==7)
						gl_SysInfo.ucAuthCompleteVoidSwitch=PaySwitch; //预授权完成撤销
					else if(i==8) //签到
					  ;
					else if(i==9)//POS初始化
						;
					else if(i==10)//POS参数下载
						;
					else if(i==11)  //电子签名上传
					       gl_SysInfo.ucSignNameSwitch=PaySwitch;
					else if(i==12) //电子现金消费（快速支付）
						;
					else if(i==13)//.电子现金消费（普通支付）
						;
					else if(i==14) //电子现金明细查询
						;
					else if(i==15) //电子现金余额查询
						;
					else if(i==16) //脱机退货
						;
					else if(i==17) //制定账户圈存
						;
					else if(i==18) //非制定账户圈存
						;
					else if(i==19) //现金充值
					  ;
					else if(i==20) //现金充值撤销
						;
					else if(i==21) //插卡消费
						;
					else if(i==22) //闪付凭密消费
						;
					else if(i==23) //闪付凭密预授权
					;
                                        else if(i==24) //日结消费
						;							
                               	}
							   
				curlen += 32;
				uiMemManaPutSysInfo();
				break;

			case 0x12: //商户名称 中文
				curlen += 2;
				
				memset(tempdata, 0, sizeof(tempdata));				
				memcpy(tempdata, &temp[curlen], 40);
				
				for(i=0;i<40;i++)
				{
					if(tempdata[i] != 0x20)
						gl_SysInfo.szMerchName[i] = tempdata[i];
				}

				curlen += 40;
				uiMemManaPutSysInfo();
				break;

			case 0x13: //广告
				curlen += 2;

                                memcpy(gl_SysInfo.szAdvInfo, &temp[curlen], 120);
				curlen += 120;
				uiMemManaPutSysInfo();
				break;
					
			default:
				break;	

    		}
	}

	return TRUE;
}

//开通GPRS报文
int iOpenGprsMessage(void)
{
	uchar szDateTime[14 + 1],temp[128+1]={0};
	int iRet;

	_vCls();
	gl_ucQrTransFlag = 2;
	vDispCenter(1, "GPRS报文开通", 1);

	if(_uiCheckBattery()<=1)
	{
	    vMessage("电池电量低,请先充电");
	    return 1;
	}
/*
      if(gl_SysInfo.iCommType == VPOSCOMM_WIFI)
    {
    	vMessage("基站信息获取中请稍候");
    	iRet = iGetLocalPosition();
	if(iRet != 0)	
		return 1;
	vClearLines(2);
    }
*/	  
	_vGetTime(szDateTime);
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
	strcpy((char *)gl_Send8583.Msg01, "0500");
        strcpy((char *)gl_Send8583.Field03, "990000");
		
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	vIncTTC();

	strcpy((char *)gl_Send8583.Field24, "008");

        strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
	
	//59域
        GetPublicField059(NULL,NULL, gl_Send8583.Field59);
 
	//打8583包-填充http-发送-接收-解http-解8583包
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
	    //显示错误信息
	    return iRet;
	}

	if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
	{
		//显示服务端错误信息
		vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);
		return -1;
	}
		           
	strcpy(temp,"GPRS报文开通成功");
	vClearLines(2);
	vMessage(temp);
	
    return 0;
}

//主密钥联机下载
int iDownMK(void)
{
	uchar szDateTime[14 + 1],temp[128+1]={0},possn[20+1]={0},csn[11+1] = {0}, check[8] = {0},macbuf[8] = {0},check_sm4[16] = {0},macbuf_sm4[16] = {0};
	uchar  sMasterKey[16+1] = {0},tmkKey[16+1] = {0};				// 终端主密钥密文
	int iRet;
		
	_vCls();
	gl_ucQrTransFlag = 0;
	vDispCenter(1, "主密钥下载", 1);

	if(_uiCheckBattery()<=1)
	{
	    vMessage("电池电量低,请先充电");
	    return 1;
	}
   
	_vGetTime(szDateTime);
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
	strcpy((char *)gl_Send8583.Msg01, "0800");
        strcpy((char *)gl_Send8583.Field03, "888888");
		
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	vIncTTC();

	strcpy((char *)gl_Send8583.Field24, "008");

        strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
	
	//60域SN号
#ifdef TESTTUSN
	memcpy(possn, TESTTUSN, strlen(TESTTUSN));
        memcpy(csn,possn+9,11);
#else
       _uiGetSerialNo(possn);
	memcpy(csn,possn+9,11);
#endif
       sprintf((char *)gl_Send8583.Field60,"%.11s",csn);
 //      strcpy(gl_Send8583.Field60,csn);

	//61域
	if(gl_SysInfo.ucSmFlag == 1)
		sprintf((char *)gl_Send8583.Field61, "%06lu%-3.3s", gl_SysData.ulBatchNo, "SM4");
        else
		sprintf((char *)gl_Send8583.Field61, "%06lu%-3.3s", gl_SysData.ulBatchNo, "999");	
		
	//打8583包-填充http-发送-接收-解http-解8583包
	iRet = iHttpSendRecv8583(0);
	if (iRet)
	{
	    //显示错误信息
	    return iRet;
	}

	if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
	{
		//显示服务端错误信息
		vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);
		return -1;
	}
		   
	//48域
	vTwoOne(gl_Recv8583.Field48, 32, sMasterKey);
	memcpy(tmkKey, gl_SysInfo.szPosId, 8);
        dbgHex("tmkkey", tmkKey, 16);

	dbgHex("sMasterKey", sMasterKey, 16);	
	 if(gl_SysInfo.ucSmFlag==0)
	{
		_vDesPlus(TRI_DECRYPT, sMasterKey, 16, tmkKey, gl_SysInfo.sMasterKey);		
		 dbgHex("gl_SysInfo.sMasterKey", gl_SysInfo.sMasterKey, 16);
		 //校验		
		memset(macbuf, 0x00, 8);
		_vDes(TRI_ENCRYPT, macbuf, gl_SysInfo.sMasterKey,check);
		vTwoOne(gl_Recv8583.Field48+32, 16, macbuf);	
		if( memcmp( check, macbuf, 8))
		{
			TraceHex("TMK_check", check, 8);
			vClearLines(2);
			vMessage("TMK校验错");
			 
			 return -1;
		}
	 }else
	 {
	        _iSm4(DECRYPT, sMasterKey, tmkKey, gl_SysInfo.sMasterKey);
		dbgHex("gl_SysInfo.sMasterKey", gl_SysInfo.sMasterKey, 16);
		 //校验		
		memset(macbuf_sm4, 0x00, 16);
		_iSm4(ENCRYPT, macbuf_sm4, gl_SysInfo.sMasterKey,check_sm4);
		dbgHex("check_sm4", check_sm4, 16);	
		vTwoOne(gl_Recv8583.Field48+32, 16, macbuf_sm4);	
		dbgHex("macbuf_sm4", macbuf_sm4, 8);
		if( memcmp( check_sm4, macbuf_sm4, 8))
		{
			TraceHex("TMK_check_sm4", check, 16);
			vClearLines(2);
			vMessage("TMK校验错");
			 
			 return -1;
		}
	 }
	SaveTMK(gl_SysInfo.sMasterKey);
	
	gl_SysInfo.ucMKFlag = 1;
	uiMemManaPutSysData();
	
	strcpy(temp,"主密钥成功");
	vClearLines(2);
	vDispMid(3, temp);
//	vMessage(temp);
	
    return 0;
}

//签到
int iPosLogin(uchar ucDispMsg)
{
    uchar szDateTime[14 + 1];
    int iRet;
    u8 possn[32 + 1] = {0},csn[11+1] = {0};
	u8   MacKey[16+1] = {0};
	u8   PinKey[16+1] = {0};
	u8   check[8] = {0},macbuf[8] = {0},checkvelue[8] = {0},check_sm4[16] = {0},macbuf_sm4[16] = {0};
	
    gl_ucQrTransFlag = 0;	

   if(ucDispMsg)	  
  {
	vDispCenter(1, "POS签到", 1);
	vClearLines(2);

	if(_uiCheckBattery()<=1)
	{
		vMessage("电池电量低,请先充电");
		return 1;
	}
   }
   
    _vGetTime(szDateTime);
    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
    strcpy((char *)gl_Send8583.Msg01, "0800");
    strcpy((char *)gl_Send8583.Field03, "990000");
	
    vIncTTC();
    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

    strcpy((char *)gl_Send8583.Field24, "04");
	
    if(gl_SysInfo.szPosId[0]==0)
        strcpy((char *)gl_Send8583.Field41, "00000000");
    else
        strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    if(gl_SysInfo.szMerchId[0]==0)
        strcpy((char *)gl_Send8583.Field42, "000000000000000");
    else
        strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	//60域SN号
#ifdef TESTTUSN
	memcpy(possn, TESTTUSN, strlen(TESTTUSN));
        memcpy(csn,possn+9,11);
#else
       _uiGetSerialNo(possn);
	memcpy(csn,possn+9,11);
#endif
       strcpy(gl_Send8583.Field60,csn);

	//61域
	if(gl_SysInfo.ucSmFlag == 1)
		sprintf((char *)gl_Send8583.Field61, "%06lu%-3.3s", gl_SysData.ulBatchNo, "SM4");
        else
		sprintf((char *)gl_Send8583.Field61, "%06lu%-3.3s", gl_SysData.ulBatchNo, "001");	
    gl_ucProcReqFlag = 1;	
	
    //打8583包-填充http-发送-接收-解http-解8583包
    iRet = iHttpSendRecv8583(0);
    if (iRet)
    {
	//显示错误信息
	return iRet;
    }
    if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
    {
        //显示服务端错误信息
        vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);
        return -1;
    }

#ifndef NO_SEC_FORDEMO	
		//使用安全模块内的主密钥
		{
			uchar tmk[16];
			int ret;
			memset(tmk, 0, sizeof(tmk));
	           	ret=LoadTMK(tmk);        
			if(ret)
			{
				char szTmp[100];
	           		 //装载密钥失败
	            		sprintf(szTmp, "LoadTmk Fail:%d", ret);
	           		 dbg("%s\n", szTmp);
				vMessage(szTmp);
				return -1;

			}
	       		 memcpy(gl_SysInfo.sMasterKey, tmk, 16);     
		} 
#endif	

	if(gl_SysInfo.ucSmFlag==0)
	{
	    //工作密钥
	    if(memcmp(gl_Recv8583.Field48,"98511",5)==0)
	    {
	        vTwoOne(gl_Recv8583.Field48+5, 32, PinKey);
				
		_vDes(TRI_DECRYPT, PinKey, gl_SysInfo.sMasterKey, gl_SysData.sPinKey);
		_vDes(TRI_DECRYPT, PinKey+8, gl_SysInfo.sMasterKey, gl_SysData.sPinKey+8);

		//校验	
		memset(macbuf, 0x00, 8);
		_vDes(TRI_ENCRYPT, macbuf, gl_SysData.sPinKey,check);
	        vTwoOne(gl_Recv8583.Field48+39, 16, checkvelue);			
		if( memcmp( check, checkvelue, 8) )
		{
			TraceHex("Pinkey_check", check, 8);
			vClearLines(2);
			vMessage("PIN校验错");
			return -1;
		}
	    }
		
	     if(memcmp(gl_Recv8583.Field48+55,"99511",5)==0)
	    {
		 vTwoOne(gl_Recv8583.Field48+60, 32, MacKey);
		 	
		_vDes(TRI_DECRYPT, MacKey, gl_SysInfo.sMasterKey, gl_SysData.sMacKey);
		_vDes(TRI_DECRYPT, MacKey+8, gl_SysInfo.sMasterKey, gl_SysData.sMacKey+8);

		dbgHex("gl_SysData.sMacKey:login", gl_SysData.sMacKey, 16);
		
		//校验	
		memset(macbuf, 0x00, 8);
		_vDes(TRI_ENCRYPT, macbuf, gl_SysData.sMacKey,check);
	       vTwoOne(gl_Recv8583.Field48+94, 16, checkvelue);
		if( memcmp( check, checkvelue, 8) )
		{
		        TraceHex("MacKey_check", check, 8);
			vClearLines(2);
			vMessage("MAC校验错");
			return -1;
		}
	    }
   }else
   {
   	    if(memcmp(gl_Recv8583.Field48,"98511",5)==0)
	    {
	        vTwoOne(gl_Recv8583.Field48+5, 32, PinKey);
	
	 	_iSm4(DECRYPT, PinKey, gl_SysInfo.sMasterKey,gl_SysData.sPinKey);
		dbgHex("gl_SysData.sPinKey", gl_SysData.sPinKey, 16);
		 //校验		
		memset(macbuf_sm4, 0x00, 16);
		_iSm4(ENCRYPT, macbuf_sm4, gl_SysData.sPinKey,check_sm4);
		dbgHex("check_sm4", check_sm4, 16);	
		vTwoOne(gl_Recv8583.Field48+39, 8, macbuf_sm4);	
		dbgHex("macbuf_sm4", macbuf_sm4, 4);
		if( memcmp( check_sm4, macbuf_sm4, 4))
		{
			TraceHex("Pinkey_check_sm4", check, 4);
			vClearLines(2);
			vMessage("PIN校验错");			 
			 return -1;
		}
	    }
	
	     if(memcmp(gl_Recv8583.Field48+47,"99511",5)==0)
	    {
		 vTwoOne(gl_Recv8583.Field48+52, 32, MacKey);

		_iSm4(DECRYPT, MacKey, gl_SysInfo.sMasterKey,gl_SysData.sMacKey);
		dbgHex("gl_SysData.sMacKey", gl_SysData.sMacKey, 16);
		 //校验		
		memset(macbuf_sm4, 0x00, 16);
		_iSm4(ENCRYPT, macbuf_sm4, gl_SysData.sMacKey,check_sm4);
		dbgHex("check_sm4", check_sm4, 16);	
		vTwoOne(gl_Recv8583.Field48+86, 8, macbuf_sm4);	
		dbgHex("macbuf_sm4", macbuf_sm4, 4);
		if( memcmp( check_sm4, macbuf_sm4, 4))
		{
			TraceHex("MacKey_check_sm4", check, 4);
			vClearLines(2);
			vMessage("MAC校验错");			 
			 return -1;
		}
	    }
   }
            //跟服务器对时
    if(gl_Recv8583.Field12[0] && gl_Recv8583.Field13[0])
    {
        _vGetTime(szDateTime);
        memcpy(szDateTime+4, gl_Recv8583.Field13, 4);
        memcpy(szDateTime+4+4, gl_Recv8583.Field12, 6);
        _uiSetTime(szDateTime);
    }
		
    _vGetTime(szDateTime);
    vTwoOne(szDateTime, 14, gl_SysData.sLoginTime);
    gl_SysData.ucPosLoginFlag=1;
    uiMemManaPutSysInfo();
    uiMemManaPutSysData();   

    if(ucDispMsg)	
    {
    	vClearLines(2);
	 vDispMid(3, "签到成功");
    }	 

    all_para_down_check();	
    return 0;
}

int iPosLogout(void)
{
     int iRet;
	 
    vDispCenter(1, "POS签退", 1);
    vClearLines(2);
    gl_ucQrTransFlag = 0;

    if(_uiCheckBattery()<=1)
    {
        vMessage("电池电量低,请先充电");
        return 1;
    }
    if(gl_SysData.ucPosLoginFlag==0 && memcmp(gl_SysData.szCurOper, "00", OPER_NO_LEN) == 0)
    {
        vDispMid(3,"主管操作员不能交易");	
        vMessageLine(4,"请先签到");
        return 1;
    }
		   
    if(gl_SysData.uiTransNum + gl_SysData.uiQrTransNum > 0)
    {
	        vMessage("请先结算");   
	        iRet = iOnlineSettle();
		if(iRet)
			return 1;
    }				

    if(gl_SysData.ucPosLoginFlag == 1)
    {
            vDispCenter(1, "POS签退", 1);
            vClearLines(2);
			
	    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
	    strcpy((char *)gl_Send8583.Msg01, "0820");
	    vIncTTC();
	    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	    if(gl_SysInfo.szPosId[0]==0)
	        strcpy((char *)gl_Send8583.Field41, "00000000");
	    else
	        strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	    if(gl_SysInfo.szMerchId[0]==0)
	        strcpy((char *)gl_Send8583.Field42, "000000000000000");
	    else
	        strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	//DE60
	    sprintf((char *)gl_Send8583.Field60, "%.2s%06lu%.3s", "00", gl_SysData.ulBatchNo, "002");

	//	vLongToStr(iLen, 2, gl_Send8583.Field60);

	    gl_ucProcReqFlag = 1;	
	    //打8583包-填充http-发送-接收-解http-解8583包
	    iRet = iHttpSendRecv8583(0);
	    if (iRet)
	    {
		//显示错误信息
		return iRet;
	    }
	    if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
	    {
	        //显示服务端错误信息
	        vShowHostErrMsg(gl_Recv8583.Field39, NULL);
	        return -1;
	    }
  
		vShowWaitEx(NULL, 0, 0x03);

		gl_SysData.ucPosLoginFlag=0;
		gl_SysData.ucOperPauseMode=1;	   
		uiMemManaPutSysData();

		vClearLines(2);
	    	vMessage("签退成功");
		
		vSetExitToMainFlag(1);	   
     }
	
	return 0;
}

int iOnlineSettle(void)
{ 
    int iSaleNum=0, iSaleVoidNum=0, iAuthCmpNum=0, iAuthCmpVoidNum=0, iRefundNum=0;
    ulong ulSaleAmt=0L, ulSaleVoidAmt=0L, ulAuthCmpAmt=0L, ulAuthCmpVoidAmt=0L, ulRefundAmt=0L;
    
      int iWetchatScanSaleNum=0, iWetchatQrSaleNum=0,iWetchatSaleVoidNum=0,iWetchatSaleRefundNum=0,iWetchatScanPreauthNum=0, iWetchatQrPreauthNum=0,iWetchatPreauthCompNum=0,iWetchatPreauthVoidNum=0;
      int iAlipayScanSaleNum=0, iAlipayQrSaleNum=0,iAlipaySaleVoidNum=0,iAlipaySaleRefundNum=0,iAlipayScanPreauthNum=0, iAlipayQrPreauthNum=0,iAlipayPreauthCompNum=0,iAlipayPreauthVoidNum=0;	  
      int iUnionpayScanSaleNum=0, iUnionpayQrSaleNum=0,iUnionpaySaleVoidNum=0,iUnionpaySaleRefundNum=0,iUnionpayScanPreauthNum=0, iUnionpayQrPreauthNum=0,iUnionpayPreauthCompNum=0,iUnionpayPreauthVoidNum=0;	  

     ulong ulWetchatScanSaleAmt=0L, ulWetchatQrSaleAmt=0L, ulWetchatSaleVoidAmt=0L, ulWetchatSaleRefundAmt=0L, ulWetchatScanPreauthAmt=0L, ulWetchatQrPreauthAmt=0L, ulWetchatPreauthCompAmt=0L, ulWetchatPreauthVoidAmt=0L;
     ulong ulAlipayScanSaleAmt=0L, ulAlipayQrSaleAmt=0L, ulAlipaySaleVoidAmt=0L, ulAlipaySaleRefundAmt=0L, ulAlipayScanPreauthAmt=0L, ulAlipayQrPreauthAmt=0L, ulAlipayPreauthCompAmt=0L, ulAlipayPreauthVoidAmt=0L;
     ulong ulUnionpayScanSaleAmt=0L, ulUnionpayQrSaleAmt=0L, ulUnionpaySaleVoidAmt=0L, ulUnionpaySaleRefundAmt=0L, ulUnionpayScanPreauthAmt=0L, ulUnionpayQrPreauthAmt=0L, ulUnionpayPreauthCompAmt=0L, ulUnionpayPreauthVoidAmt=0L;


    stSettleInfo sett;	
    char szTitle[30];
    uint uiKey;
    int page, line;
    ulong ulTimer;
    int maxpage=13;
   uchar isexit = 0;
   uchar szDateTime[14 + 1];
    int iRet;
	
   _vCls();
   gl_ucQrTransFlag = 0;
    strcpy(szTitle, "联机结算");
    vDispCenter(1, szTitle, 1);
	
    if(_uiCheckBattery()<=1)
    {
        vMessage("电池电量低,请先充电");
        return 1;
    }
    if(gl_SysData.ucPosLoginFlag==0 && memcmp(gl_SysData.szCurOper, "00", OPER_NO_LEN) == 0)
    {
        vDispMid(3,"主管操作员不能交易");	
        vMessageLine(4,"请先签到");
        return 1;
    }
	
    if(gl_SysData.uiTransNum==0 && gl_SysData.uiQrTransNum==0 && uiMemManaIfExist8583(MSG8583_TYPE_REV)==0)
    {
        vMessage("没有交易, 无需结算");
        return -1;
    }

      vGenTransSum(&iSaleNum, &ulSaleAmt, &iSaleVoidNum, &ulSaleVoidAmt, 
                &iAuthCmpNum, &ulAuthCmpAmt, &iAuthCmpVoidNum, &ulAuthCmpVoidAmt,
                &iRefundNum, &ulRefundAmt,1);
	  
   vGenQrTransSum(&iWetchatScanSaleNum, &ulWetchatScanSaleAmt, &iWetchatQrSaleNum, &ulWetchatQrSaleAmt, 
   	        &iWetchatSaleVoidNum,&ulWetchatSaleVoidAmt,&iWetchatSaleRefundNum,&ulWetchatSaleRefundAmt,MODE_WETCHAT,1);
   
    vGenQrPreauthTransSum(&iWetchatScanPreauthNum, &ulWetchatScanPreauthAmt, &iWetchatQrPreauthNum, &ulWetchatQrPreauthAmt, 
   	        &iWetchatPreauthCompNum,&ulWetchatPreauthCompAmt,&iWetchatPreauthVoidNum,&ulWetchatPreauthVoidAmt,MODE_WETCHAT,1);
	
  vGenQrTransSum(&iAlipayScanSaleNum, &ulAlipayScanSaleAmt, &iAlipayQrSaleNum, &ulAlipayQrSaleAmt, 
   	        &iAlipaySaleVoidNum,&ulAlipaySaleVoidAmt,&iAlipaySaleRefundNum,&ulAlipaySaleRefundAmt,MODE_ALIPAY,1);
   
    vGenQrPreauthTransSum(&iAlipayScanPreauthNum, &ulAlipayScanPreauthAmt, &iAlipayQrPreauthNum, &ulAlipayQrPreauthAmt, 
   	        &iAlipayPreauthCompNum,&ulAlipayPreauthCompAmt,&iAlipayPreauthVoidNum,&ulAlipayPreauthVoidAmt,MODE_ALIPAY,1);

   vGenQrTransSum(&iUnionpayScanSaleNum, &ulUnionpayScanSaleAmt, &iUnionpayQrSaleNum, &ulUnionpayQrSaleAmt, 
   	        &iUnionpaySaleVoidNum,&ulUnionpaySaleVoidAmt,&iUnionpaySaleRefundNum,&ulUnionpaySaleRefundAmt,MODE_UNIONPAY,1);
   
    vGenQrPreauthTransSum(&iUnionpayScanPreauthNum, &ulUnionpayScanPreauthAmt, &iUnionpayQrPreauthNum, &ulUnionpayQrPreauthAmt, 
   	        &iUnionpayPreauthCompNum,&ulUnionpayPreauthCompAmt,&iUnionpayPreauthVoidNum,&ulUnionpayPreauthVoidAmt,MODE_UNIONPAY,1);
   
    page=1;
    while(isexit != 1)
    {
        if(_uiGetVLines()>6)
            line=2;
        else
            line=1;
        if(page==1)
        {
            vDispCenterEx(line++, "银行卡消费交易", 0, 1, page, maxpage);
            vDispVarArg(line++, "消费总笔数:%d", iSaleNum);
            vDispVarArg(line++, "消费总金额:%lu.%02lu", ulSaleAmt/100, ulSaleAmt%100);
            vDispVarArg(line++, "撤销总笔数:%d", iSaleVoidNum); 
            vDispVarArg(line++, "撤销总金额:%lu.%02lu", ulSaleVoidAmt/100, ulSaleVoidAmt%100);
        }
        if(page==2)
        {
            vDispCenterEx(line++, "预授权完成交易", 0, 1, page, maxpage);
            vDispVarArg(line++, "完成总笔数:%d", iAuthCmpNum);
            vDispVarArg(line++, "完成总金额:%lu.%02lu", ulAuthCmpAmt/100, ulAuthCmpAmt%100);
            vDispVarArg(line++, "撤销总笔数:%d", iAuthCmpVoidNum); 
            vDispVarArg(line++, "撤销总金额:%lu.%02lu", ulAuthCmpVoidAmt/100, ulAuthCmpVoidAmt%100);
        }
        if(page==3)
        {
            vDispCenterEx(line++, "银行卡退货交易", 0, 1, page, maxpage);
            vDispVarArg(line++, "退货总笔数:%d", iRefundNum);
            vDispVarArg(line++, "退货总金额:%lu.%02lu", ulRefundAmt/100, ulRefundAmt%100);
            vClearLines(line);
        }    
        if(page==4)
        {
      	    vDispCenterEx(line++, "扫码消费", 0, 1, page, maxpage);
            vDispVarArg(line++, "微信总笔数:%d", iWetchatScanSaleNum+iWetchatQrSaleNum);
            vDispVarArg(line++, "微信总金额:%lu.%02lu", (ulWetchatScanSaleAmt+ulWetchatQrSaleAmt)/100, (ulWetchatScanSaleAmt+ulWetchatQrSaleAmt)%100);	    
  	    vDispVarArg(line++, "支付宝总笔数:%d", iAlipayScanSaleNum+iAlipayQrSaleNum);
  	    vDispVarArg(line++, "支付宝总金额:%lu.%02lu", (ulAlipayScanSaleAmt+ulAlipayQrSaleAmt)/100, (ulAlipayScanSaleAmt+ulAlipayQrSaleAmt)%100);
            vClearLines(line);
        }
        if(page==5)
        {
      	    vDispCenterEx(line++, "扫码消费", 0, 1, page, maxpage);	    
  	    vDispVarArg(line++, "银联总笔数:%d", iUnionpayScanSaleNum+iUnionpayQrSaleNum);
  	    vDispVarArg(line++, "银联总金额:%lu.%02lu", (ulUnionpayScanSaleAmt+ulUnionpayQrSaleAmt)/100, (ulUnionpayScanSaleAmt+ulUnionpayQrSaleAmt)%100);
            vClearLines(line);
        }
	if(page==6)
        {
      	    vDispCenterEx(line++, "扫码退款", 0, 1, page, maxpage);
            vDispVarArg(line++, "微信总笔数:%d", iWetchatSaleRefundNum);
            vDispVarArg(line++, "微信总金额:%lu.%02lu", ulWetchatSaleRefundAmt/100, ulWetchatSaleRefundAmt%100);	    
  	    vDispVarArg(line++, "支付宝总笔数:%d", iAlipaySaleRefundNum);
  	    vDispVarArg(line++, "支付宝总金额:%lu.%02lu", ulAlipaySaleRefundAmt/100, ulAlipaySaleRefundAmt%100);
            vClearLines(line);
        }	
        if(page==7)
        {
      	    vDispCenterEx(line++, "扫码退款", 0, 1, page, maxpage);    
  	    vDispVarArg(line++, "银联总笔数:%d", iUnionpaySaleRefundNum);
  	    vDispVarArg(line++, "银联总金额:%lu.%02lu", ulUnionpaySaleRefundAmt/100, ulUnionpaySaleRefundAmt%100);
            vClearLines(line);
        }  	
        if(page==8)
        {
      	    vDispCenterEx(line++, "扫码预授权", 0, 1, page, maxpage);
            vDispVarArg(line++, "微信总笔数:%d", iWetchatScanPreauthNum+iWetchatQrPreauthNum);
            vDispVarArg(line++, "微信总金额:%lu.%02lu",(ulWetchatScanPreauthAmt+ulWetchatQrPreauthAmt)/100, (ulWetchatScanPreauthAmt+ulWetchatQrPreauthAmt)%100);	    
  	    vDispVarArg(line++, "支付宝总笔数:%d", iAlipayScanPreauthNum+iAlipayQrPreauthNum);
  	    vDispVarArg(line++, "支付宝总金额:%lu.%02lu",  (ulAlipayScanPreauthAmt+ulAlipayQrPreauthAmt)/100,  (ulAlipayScanPreauthAmt+ulAlipayQrPreauthAmt)%100);
            vClearLines(line);
        }
        if(page==9)
        {
      	    vDispCenterEx(line++, "扫码预授权", 0, 1, page, maxpage);
    	    vDispVarArg(line++, "银联总笔数:%d", iUnionpayScanPreauthNum+iUnionpayQrPreauthNum);
  	    vDispVarArg(line++, "银联总金额:%lu.%02lu",  (ulUnionpayScanPreauthAmt+ulUnionpayQrPreauthAmt)/100,  (ulUnionpayScanPreauthAmt+ulUnionpayQrPreauthAmt)%100);   
            vClearLines(line);
        }		
        if(page==10)
        {
      	    vDispCenterEx(line++, "扫码预授权撤销", 0, 1, page, maxpage);
            vDispVarArg(line++, "微信总笔数:%d", iWetchatPreauthVoidNum);
            vDispVarArg(line++, "微信总金额:%lu.%02lu", ulWetchatPreauthVoidAmt/100, ulWetchatPreauthVoidAmt%100);	    
  	    vDispVarArg(line++, "支付宝总笔数:%d", iAlipayPreauthVoidNum);
  	    vDispVarArg(line++, "支付宝总金额:%lu.%02lu", ulAlipayPreauthVoidAmt/100, ulAlipayPreauthVoidAmt%100);
            vClearLines(line);
        }
        if(page==11)
        {
      	    vDispCenterEx(line++, "扫码预授权撤销", 0, 1, page, maxpage);
  	    vDispVarArg(line++, "银联总笔数:%d", iUnionpayPreauthVoidNum);
  	    vDispVarArg(line++, "银联总金额:%lu.%02lu", ulUnionpayPreauthVoidAmt/100, ulUnionpayPreauthVoidAmt%100);	    
            vClearLines(line);
        }
		
        if(page==12)
        {
      	    vDispCenterEx(line++, "扫码预授权完成", 0, 1, page, maxpage);
            vDispVarArg(line++, "微信总笔数:%d", iWetchatPreauthCompNum);
            vDispVarArg(line++, "微信总金额:%lu.%02lu", ulWetchatPreauthCompAmt/100, ulWetchatPreauthCompAmt%100);	    
  	    vDispVarArg(line++, "支付宝总笔数:%d", iAlipayPreauthCompNum);
  	    vDispVarArg(line++, "支付宝总金额:%lu.%02lu", ulAlipayPreauthCompAmt/100, ulAlipayPreauthCompAmt%100);
            vClearLines(line);
        }
        if(page==13)
        {
      	    vDispCenterEx(line++, "扫码预授权完成", 0, 1, page, maxpage);
  	    vDispVarArg(line++, "银联总笔数:%d", iUnionpayPreauthCompNum);
  	    vDispVarArg(line++, "银联总金额:%lu.%02lu", ulUnionpayPreauthCompAmt/100, ulUnionpayPreauthCompAmt%100);	   	    
            vClearLines(line);
        }

	_vFlushKey();	
        _vSetTimer(&ulTimer, 1500);
        while(!_uiTestTimer(ulTimer))
        {
            if(_uiKeyPressed())
            {
                _vSetTimer(&ulTimer, 1500);
                uiKey=_uiGetKey();
                if(uiKey==_KEY_UP && page>1)
                {
                    page--;
                    break;
                }
                if((uiKey == _KEY_DOWN || uiKey == _KEY_ENTER ) && page<maxpage)
                {
                    page++;
                    break;
                }
                if(uiKey == _KEY_ESC)
                    return 1;
                if((uiKey == _KEY_DOWN || uiKey == _KEY_ENTER )  && page == maxpage)
                {
                	isexit = 1;
		   	break;
                }
            }
        }
    }
	
	 //失败电签上送
//    	iSendFailSignOper(0, NULL);

         iSendFailSignOper(0, NULL);
	
	//检查处理待冲正报文
	iProcReverse(NULL, NULL);

	_vCls();
	strcpy(szTitle, "联机结算");
	vDispCenter(1, szTitle, 1);

	   vGenQrTransSum(&iWetchatScanSaleNum, &ulWetchatScanSaleAmt, &iWetchatQrSaleNum, &ulWetchatQrSaleAmt, 
   	        &iWetchatSaleVoidNum,&ulWetchatSaleVoidAmt,&iWetchatSaleRefundNum,&ulWetchatSaleRefundAmt,MODE_WETCHAT,0);
   
    vGenQrPreauthTransSum(&iWetchatScanPreauthNum, &ulWetchatScanPreauthAmt, &iWetchatQrPreauthNum, &ulWetchatQrPreauthAmt, 
   	        &iWetchatPreauthCompNum,&ulWetchatPreauthCompAmt,&iWetchatPreauthVoidNum,&ulWetchatPreauthVoidAmt,MODE_WETCHAT,0);
	
  vGenQrTransSum(&iAlipayScanSaleNum, &ulAlipayScanSaleAmt, &iAlipayQrSaleNum, &ulAlipayQrSaleAmt, 
   	        &iAlipaySaleVoidNum,&ulAlipaySaleVoidAmt,&iAlipaySaleRefundNum,&ulAlipaySaleRefundAmt,MODE_ALIPAY,0);
   
    vGenQrPreauthTransSum(&iAlipayScanPreauthNum, &ulAlipayScanPreauthAmt, &iAlipayQrPreauthNum, &ulAlipayQrPreauthAmt, 
   	        &iAlipayPreauthCompNum,&ulAlipayPreauthCompAmt,&iAlipayPreauthVoidNum,&ulAlipayPreauthVoidAmt,MODE_ALIPAY,0);

   vGenQrTransSum(&iUnionpayScanSaleNum, &ulUnionpayScanSaleAmt, &iUnionpayQrSaleNum, &ulUnionpayQrSaleAmt, 
   	        &iUnionpaySaleVoidNum,&ulUnionpaySaleVoidAmt,&iUnionpaySaleRefundNum,&ulUnionpaySaleRefundAmt,MODE_UNIONPAY,0);
   
    vGenQrPreauthTransSum(&iUnionpayScanPreauthNum, &ulUnionpayScanPreauthAmt, &iUnionpayQrPreauthNum, &ulUnionpayQrPreauthAmt, 
   	        &iUnionpayPreauthCompNum,&ulUnionpayPreauthCompAmt,&iUnionpayPreauthVoidNum,&ulUnionpayPreauthVoidAmt,MODE_UNIONPAY,0);
	
	 memset(&sett, 0, sizeof(sett)); 
	 
	 sett.iSaleNum = iSaleNum;
	 sett.ulSaleAmt = ulSaleAmt;

	 sett.iSaleVoidNum = iSaleVoidNum;
	 sett.ulSaleVoidAmt = ulSaleVoidAmt;
	 
	 sett.iAuthCmpNum = iAuthCmpNum;
	 sett.ulAuthCmpAmt = ulAuthCmpAmt;

	 sett.iAuthCmpVoidNum = iAuthCmpVoidNum;
	 sett.ulAuthCmpVoidAmt = ulAuthCmpVoidAmt;
	 
	 sett.iRefundNum = iRefundNum;
	 sett.ulRefundAmt = ulRefundAmt;
	 
	 sett.iWetchatScanSaleNum = iWetchatScanSaleNum;
	 sett.ulWetchatScanSaleAmt = ulWetchatScanSaleAmt;
	 sett.iWetchatQrSaleNum = iWetchatQrSaleNum;
	 sett.ulWetchatQrSaleAmt = ulWetchatQrSaleAmt;
         sett.iWetchatSaleVoidNum = iWetchatSaleVoidNum;
	 sett.ulWetchatSaleVoidAmt = ulWetchatSaleVoidAmt;
	 sett.iWetchatSaleRefundNum = iWetchatSaleRefundNum;
	 sett.ulWetchatSaleRefundAmt = ulWetchatSaleRefundAmt;
	 
	 sett.iWetchatScanPreauthNum = iWetchatScanPreauthNum;
	 sett.ulWetchatScanPreauthAmt = ulWetchatScanPreauthAmt;
	 sett.iWetchatQrPreauthNum = iWetchatQrPreauthNum;
	 sett.ulWetchatQrPreauthAmt = ulWetchatQrPreauthAmt;
         sett.iWetchatPreauthCompNum = iWetchatPreauthCompNum;
	 sett.ulWetchatPreauthCompAmt = ulWetchatPreauthCompAmt;
	 sett.iWetchatPreauthVoidNum = iWetchatPreauthVoidNum;
	 sett.ulWetchatPreauthVoidAmt = ulWetchatPreauthVoidAmt;

	 sett.iAlipayScanSaleNum = iAlipayScanSaleNum;
	 sett.ulAlipayScanSaleAmt = ulAlipayScanSaleAmt;
	 sett.iAlipayQrSaleNum = iAlipayQrSaleNum;
	 sett.ulAlipayQrSaleAmt = ulAlipayQrSaleAmt;
         sett.iAlipaySaleVoidNum = iAlipaySaleVoidNum;
	 sett.ulAlipaySaleVoidAmt = ulAlipaySaleVoidAmt;
	 sett.iAlipaySaleRefundNum = iAlipaySaleRefundNum;
	 sett.ulAlipaySaleRefundAmt = ulAlipaySaleRefundAmt;
	 
	 sett.iAlipayScanPreauthNum = iAlipayScanPreauthNum;
	 sett.ulAlipayScanPreauthAmt = ulAlipayScanPreauthAmt;
	 sett.iAlipayQrPreauthNum = iAlipayQrPreauthNum;
	 sett.ulAlipayQrPreauthAmt = ulAlipayQrPreauthAmt;
         sett.iAlipayPreauthCompNum = iAlipayPreauthCompNum;
	 sett.ulAlipayPreauthCompAmt = ulAlipayPreauthCompAmt;
	 sett.iAlipayPreauthVoidNum = iAlipayPreauthVoidNum;
	 sett.ulAlipayPreauthVoidAmt = ulAlipayPreauthVoidAmt;

	 sett.iUnionpayScanSaleNum = iUnionpayScanSaleNum;
	 sett.ulUnionpayScanSaleAmt = ulUnionpayScanSaleAmt;
	 sett.iUnionpayQrSaleNum = iUnionpayQrSaleNum;
	 sett.ulUnionpayQrSaleAmt = ulUnionpayQrSaleAmt;
         sett.iUnionpaySaleVoidNum = iUnionpaySaleVoidNum;
	 sett.ulUnionpaySaleVoidAmt = ulUnionpaySaleVoidAmt;
	 sett.iUnionpaySaleRefundNum = iUnionpaySaleRefundNum;
	 sett.ulUnionpaySaleRefundAmt = ulUnionpaySaleRefundAmt;
	 
	 sett.iUnionpayScanPreauthNum = iUnionpayScanPreauthNum;
	 sett.ulUnionpayScanPreauthAmt = ulUnionpayScanPreauthAmt;
	 sett.iUnionpayQrPreauthNum = iUnionpayQrPreauthNum;
	 sett.ulUnionpayQrPreauthAmt = ulUnionpayQrPreauthAmt;
         sett.iUnionpayPreauthCompNum = iUnionpayPreauthCompNum;
	 sett.ulUnionpayPreauthCompAmt = ulUnionpayPreauthCompAmt;
	 sett.iUnionpayPreauthVoidNum = iUnionpayPreauthVoidNum;
	 sett.ulUnionpayPreauthVoidAmt = ulUnionpayPreauthVoidAmt;
	 
       sett.iDebitNum = sett.iSaleNum + sett.iAuthCmpNum ;
       sett.ulDebitAmt = sett.ulSaleAmt  + sett.ulAuthCmpAmt ;

	sett.iCreditNum = sett.iSaleVoidNum + sett.iAuthCmpVoidNum  + sett.iRefundNum ;
	sett.ulCreditAmt = sett.ulSaleVoidAmt  + sett.ulAuthCmpVoidAmt  + sett.ulRefundAmt;
	   	
	sett.ulBatchNo=gl_SysData.ulBatchNo;
	_vGetTime(szDateTime);
	vTwoOne(szDateTime+2, 12, sett.sDateTime);
	vTwoOne(gl_SysData.szCurOper, 2, sett.sOper);

	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
    
    strcpy((char *)gl_Send8583.Msg01, "0500");
    vIncTTC();
    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
    sprintf((char *)gl_Send8583.Field48, "%012lu%03d%012lu%03d0%012lu%03d%012lu%03d0", sett.iDebitNum,sett.ulDebitAmt,sett.iCreditNum,sett.ulCreditAmt,
		0,0,0,0);
   	
    strcpy((char *)gl_Send8583.Field49, "156");
    sprintf((char *)gl_Send8583.Field60, "%.2s%06lu%.3s", "00", gl_SysData.ulBatchNo, "201");
    sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);  
    
    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

    //打8583包-填充http-发送-接收-解http-解8583包
    iRet = iHttpSendRecv8583(0);
    if (iRet)
    {
        //显示错误信息
        vShowCommErrMsg(iRet);
        
        return iRet;
    }

     if(gl_Recv8583.Field48[0] )
     {
         if(gl_Recv8583.Field48[30] == '1')
     	  	vMessage("对账平");
	else if(gl_Recv8583.Field48[30] == '2')
		vMessage("对账不平");
     }
	

	
#ifdef ENABLE_PRINTER      
    //保存结算单数据(用于补打上笔结算单)
    uiMemManaPutSettleInfo(&sett);
#endif

    vClearLines(2);
#ifdef ENABLE_PRINTER 
    vShowWaitEx("结算成功,打印结算单", -1, 0);      //用vShowWaitEx,有打印时显示并等待打印完成,无打印时保证信息显示时间
    iPrintSettle(&sett,0);
#else
    vMessage("结算成功");
#endif      				

    if(gl_SysInfo.ucSettlePrintDetail)
   {
   	vClearLines(2);
   	 vDispMid(3,"是否打印结算明细");
  	_vDisp(5,"[取消]        [确认]");   

	_vFlushKey();
	while(1)
	{				
		if(_uiKeyPressed())
		{
			uiKey=_uiGetKey();
			if(uiKey==_KEY_ENTER)
			{
				iPrintAll();							//打印交易明细
	                	break;
			}
			else if(uiKey==_KEY_CANCEL)								    
				break;					
	        }		
        }	
    }
  		
    //保存sysdata  
    gl_SysData.uiTransNum=0;
    gl_SysData.uiQrTransNum=0;
   gl_SysData.ucFailSignNum=0;
    if(++gl_SysData.ulBatchNo>999999L)
        gl_SysData.ulBatchNo=1;

    uiMemManaPutSysData();

    //清除签名文件信息
    uiMemClearAllJbg();
    
    vShowWaitEx(NULL, 0, 0x03);
   
    if(gl_SysInfo.ucAutoLogoutFlag)
    {	
	iPosLogout();
    }
	
    return 0;
}

int iOfflineSettle(void)
{ 
    int iSaleNum=0, iSaleVoidNum=0, iAuthCmpNum=0, iAuthCmpVoidNum=0, iRefundNum=0;
    ulong ulSaleAmt=0L, ulSaleVoidAmt=0L, ulAuthCmpAmt=0L, ulAuthCmpVoidAmt=0L, ulRefundAmt=0L;
    
    int iScanSaleNum=0, iQrSaleNum=0; 
    ulong ulScanSaleAmt=0L, ulQrSaleAmt=0L; 

    stSettleInfo sett;	
    char szTitle[30];
    uint uiKey;
    int page, line;
    ulong ulTimer;
    int maxpage=1;
   uchar isexit = 0;
   uchar szDateTime[14 + 1];
   char szTmp[20+1];
   
 /* 
   uchar CommBuf[100] = {0};
   int iMacLen = 40;
   uchar sMac[8] = {0};

  memcpy(CommBuf,"\x62\x58\x08\x16\x44\x39\x67\x03\x00\x00\x00\x00\x00\x00\x00\x05\x00\x00\x01\x23\x13\x44\x36\x11\x11\x01\x56\x35\x35\x35\x36\x31\x37\x35\x38\x31\x38\x00\x00\x00",40);

  strcpy(gl_SysData.sMacKey,"\x94\x29\x9A\x55\xEC\x5C\xC2\xF3\xE7\x6C\xD2\xEA\x07\x6E\x82\x3F");
   iCalMac(CommBuf, iMacLen, NULL, sMac);
*/  
   _vCls();
    strcpy(szTitle, "离线结算");
    vDispCenter(1, szTitle, 1);

    if(gl_SysData.uiTransNum==0 && gl_SysData.uiQrTransNum==0 && uiMemManaIfExist8583(MSG8583_TYPE_REV)==0)
    {
        vMessage("没有交易, 无需结算");
        return -1;
    }

      vGenTransSum(&iSaleNum, &ulSaleAmt, &iSaleVoidNum, &ulSaleVoidAmt, 
                &iAuthCmpNum, &ulAuthCmpAmt, &iAuthCmpVoidNum, &ulAuthCmpVoidAmt,
                &iRefundNum, &ulRefundAmt,1);
//      vGenQrTransSum_hrt(&iScanSaleNum, &ulScanSaleAmt, &iQrSaleNum, &ulQrSaleAmt,1);
   
    page=1;
    while(isexit != 1)
    {
        if(_uiGetVLines()>6)
            line=2;
        else
            line=2;
        if(page==1)
        {
        //    vDispVarArg(line++, "消费总笔数:%d", iSaleNum);
       //     vDispVarArg(line++, "消费总金额:%lu.%02lu", ulSaleAmt/100, ulSaleAmt%100);
                vDispVarArg(line++, "消费总笔数:");	
		sprintf(szTmp,"%d",iSaleNum);
                vDispRight(line++,szTmp);
			   	
	       vDispVarArg(line++, "消费总金额:");	
	       sprintf(szTmp,"%lu.%02lu",ulSaleAmt/100, ulSaleAmt%100);
	       vDispRight(line++,szTmp);
        }
/*		
	if(page==2)
        {
            vDispCenterEx(line++, "扫码交易", 0, 1, page, maxpage);
            vDispVarArg(line++, "支付总笔数:%d", iScanSaleNum + iQrSaleNum);
            vDispVarArg(line++, "支付总金额:%lu.%02lu", (ulScanSaleAmt+ulQrSaleAmt)/100, (ulScanSaleAmt+ulQrSaleAmt)%100);	    
            vClearLines(line);
        }   	
*/
	_vFlushKey();	
       _vSetTimer(&ulTimer, 6000L); //60秒
        while(1)
        {
            if(_uiTestTimer(ulTimer))
	   {
		isexit = 1;
		break;
            }
           else if(_uiKeyPressed())
            {
                uiKey=_uiGetKey();
                if(uiKey==_KEY_UP && page>1)
                {
                    page--;
                    break;
                }
                if((uiKey == _KEY_DOWN || uiKey == _KEY_ENTER ) && page<maxpage)
                {
                    page++;
                    break;
                }
                if(uiKey == _KEY_ESC)
                    return 1;
                if((uiKey == _KEY_DOWN || uiKey == _KEY_ENTER )  && page == maxpage)
                {
                	isexit = 1;
		   	break;
                }
            }
        }
    }
	
	_vCls();
	strcpy(szTitle, "离线结算");
	vDispCenter(1, szTitle, 1);

	 memset(&sett, 0, sizeof(sett)); 
	 
	 sett.iSaleNum = iSaleNum;
	 sett.ulSaleAmt = ulSaleAmt;
	 
	 sett.iAuthCmpNum = iAuthCmpNum;
	 sett.ulAuthCmpAmt = ulAuthCmpAmt;
	 
	 sett.iRefundNum = iRefundNum;
	 sett.ulRefundAmt = ulRefundAmt;
	 
	 sett.iScanSaleNum = iScanSaleNum;
	 sett.ulScanSaleAmt = ulScanSaleAmt;
	
	sett.iQrSaleNum = iQrSaleNum;
	sett.ulQrSaleAmt = ulQrSaleAmt;

	sett.ulBatchNo=gl_SysData.ulBatchNo;
	_vGetTime(szDateTime);
	vTwoOne(szDateTime+2, 12, sett.sDateTime);
	vTwoOne(gl_SysData.szCurOper, 2, sett.sOper);
	
#ifdef ENABLE_PRINTER      
    //保存结算单数据(用于补打上笔结算单)
    uiMemManaPutSettleInfo(&sett);
#endif

    vClearLines(2);
#ifdef ENABLE_PRINTER 
    vShowWaitEx("结算成功,打印结算单", -1, 0);      //用vShowWaitEx,有打印时显示并等待打印完成,无打印时保证信息显示时间
    iPrintSettle(&sett,0);

    if(gl_SysInfo.ucSettlePrintDetail)
   {
   	vClearLines(2);
   	 vDispMid(3,"是否打印结算明细");
  	_vDisp(5,"[取消]        [确认]");   

	_vFlushKey();
	while(1)
	{				
		if(_uiKeyPressed())
		{
			uiKey=_uiGetKey();
			if(uiKey==_KEY_ENTER)
			{
				iPrintAll();							//打印交易明细
	                	break;
			}
			else if(uiKey==_KEY_CANCEL)								    
				break;					
	        }		
        }	
    }	
#else
    vMessage("结算成功");
#endif      				
  		
    //保存sysdata  
    gl_SysData.uiTransNum=0;
    gl_SysData.uiQrTransNum=0;
    if(++gl_SysData.ulBatchNo>999999L)
        gl_SysData.ulBatchNo=1;

    uiMemManaPutSysData();

    //清除签名文件信息
    uiMemClearAllJbg();

    //清文件
    _vClearTransFile();
		
    vShowWaitEx(NULL, 0, 0x03);
   
    if(gl_SysInfo.ucAutoLogoutFlag)
    {
	gl_SysData.ucPosLoginFlag=0;
	gl_SysData.ucOperPauseMode=1;
	uiMemManaPutSysData();
        vSetExitToMainFlag(1);      //设置退出到主界面	
    }
	
    return 0;
}

void vClearIcParam(uchar flag)
{
    if(gl_SysInfo.uiCaKeyNum && flag==1)
    {
        gl_SysInfo.uiCaKeyNum=0;
        uiMemManaPutSysInfo();
    }

    if(gl_SysInfo.uiAidNum && flag==2)
    {
        gl_SysInfo.uiAidNum=0;
        uiMemManaPutSysInfo();
    }
}

//更新公钥
int iDownloadCAPK(void)
{	
	uchar tmpbuf[2056],buf[50];
	int iRet;
	int iKeyNum,iLen,tmplen,i,k,j;
	int iRidNum,iNum;
	ushort uiCaKeyNum = 0;
	typedef struct{
		uchar Rid[5];
		uchar expiry[8];
	}Tlv_data;
	Tlv_data tdata[MAX_CA_KEY_NUM];

	_vCls();
	gl_ucQrTransFlag = 0;	
	vDispCenter(1, "公钥下载", 1);	

	iRidNum = 0;
	iLen = 0;
	tmplen = 0;
	
	gl_SysInfo.uiCaKeyNum=0;
	uiMemManaPutSysInfo();
    	
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
	strcpy((char *)gl_Send8583.Msg01, "0800");
	strcpy((char *)gl_Send8583.Field03, "990000");
	
	vIncTTC();
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

        strcpy((char *)gl_Send8583.Field24, "009");
		
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
	sprintf((char *)gl_Send8583.Field61, "%06lu%-3.3s", gl_SysData.ulBatchNo, "092");
    
        //打8583包-填充http-发送-接收-解http-解8583包
        iRet = iHttpSendRecv8583(0);
        if (iRet)
        {
            vClearIcParam(1);
            //显示错误信息
            return iRet;
        }
		
        if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
        {
            vClearIcParam(1);
            //显示服务端错误信息
            vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);
            return -1;
        }
        
         tmplen = ulStrToLong(gl_Recv8583.Field56, 2) - 3;
	 memcpy(tmpbuf,gl_Recv8583.Field56+2+3,tmplen);		
	 dbgHex("tmpbuf111",tmpbuf,tmplen);	  

        iRidNum = tmplen /19;
   	for(i = 0;i < iRidNum;i++){
	memcpy(buf,tmpbuf+i*19,19);
	k=0;
		while(k<19){			
			if(memcmp(buf+k,"\x9f\x06\x05",3)==0){
			k+=3;
			memcpy(tdata[i].Rid,buf+k,5);
			k+=5;
			dbgHex("tdata[i].Rid",tdata[i].Rid,5);
			continue;
			}
		
			if(memcmp(buf+k,"\xdf\x25\x08",3)==0){
				k+=3;
				memcpy(tdata[i].expiry,buf+k,8);
				k+=8;
				dbgHex("tdata[i].expiry",tdata[i].expiry,8);
				continue;
			}
			k++;
		}
   	}

        for(i = 0;i < iRidNum;i++) {
		memset(&gl_Send8583, 0, sizeof(gl_Send8583));
		strcpy((char *)gl_Send8583.Msg01, "0800");
               	strcpy((char *)gl_Send8583.Field03, "990000");
	
		vIncTTC();
		sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

       	        strcpy((char *)gl_Send8583.Field24, "009");
		strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
		strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

                if(memcmp(tdata[i].Rid,"\xA0\x00\x00\x03\x33",5) == 0)
			iNum = 6; //刚开始说是12
		else
			iNum = 6;
		for(iKeyNum = 1;iKeyNum <= iNum;iKeyNum ++)		
               	{
               	        vClearLines(2);
			vDispVarArg(2, "No.%d",uiCaKeyNum+1);
		
	               	iLen = 2; 
	                memcpy(&gl_Send8583.Field56[iLen],"\x9f\x06\x05",3);
			iLen += 3;		
			memcpy(&gl_Send8583.Field56[iLen],tdata[i].Rid,5);		
			iLen += 5;
	                memcpy(&gl_Send8583.Field56[iLen],"\xdf\x25\x08",3);
			iLen += 3;		
	                memcpy(&gl_Send8583.Field56[iLen],tdata[i].expiry,8);	
			iLen += 8;		
	                memcpy(&gl_Send8583.Field56[iLen],"\xdf\x28\x01",3);
			iLen += 3;		
	                gl_Send8583.Field56[iLen] = iKeyNum;	 
			iLen += 1;
			vLongToStr(iLen-2, 2, gl_Send8583.Field56);
			
	                sprintf((char *)gl_Send8583.Field61, "%06lu%-3.3s", gl_SysData.ulBatchNo, "093");

				//打8583包-填充http-发送-接收-解http-解8583包
			iRet = iHttpSendRecv8583(0);
			if (iRet)
			{
				//显示错误信息
				return iRet;
			}
			if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
			{
				//显示服务端错误信息
				vShowHostErrMsg(gl_Recv8583.Field39, NULL);
				return -1;
			}

							
			iLen = gl_Recv8583.Field56[0]*256+gl_Recv8583.Field56[1];
                        j  = 0;
			k = 0;
			memset(tmpbuf,0x00,sizeof(tmpbuf));
                        while(j < iLen)
                        {
                        	if(memcmp(gl_Recv8583.Field56+2+j,"\xdf\x28\x01",3)==0)
                        	{
                        	   	 j  += 3;
				   	 j += 1;				
                        	}else if(memcmp(gl_Recv8583.Field56+2+j,"\xdf\x25\x08",3)==0)
                        	{
                        	  	j +=3;
				        j += 8;							   
                        	}else 
                        	{
                        		tmpbuf[k] = gl_Recv8583.Field56[2+j];
					k ++;
					j ++;
                        	}	
                        		
                        }	
						
			dbgHex("tmpbuf", tmpbuf,k);			
			uiMemManaPutCaPubKey(uiCaKeyNum, tmpbuf, k);
			uiCaKeyNum ++;
        	}
        }	

       dbg("uiCaKeyNum:%d\n",uiCaKeyNum);        
		
	gl_SysInfo.uiCaKeyNum=uiCaKeyNum;
	uiMemManaPutSysInfo();		
	vDispMid(3,"公钥下载成功");

	return(0);
}

//下载IC交易参数(Aid)
int iDownloadEmvParam(void)
{
	int   iRet, i,j,k,iAidNum,len,iLen,len1;
        int   NextFlag = 0;
	uchar tmpbuf[2056];
	
	typedef struct {
		byte AID[16+1];
		int AIDLen;
		uchar expiry[8];
	}_EMVAPP_;

	_EMVAPP_ applist[MAX_AID_NUM];

	_vCls();
	vDispCenter(1, "PBOC参数下载", 1);

	iAidNum = 0;
	gl_SysInfo.uiAidNum = 0;
	uiMemManaPutSysInfo();

	for(;;) {
	// 查询AID
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
	strcpy((char *)gl_Send8583.Msg01, "0800");
	strcpy((char *)gl_Send8583.Field03, "990000");
	
	vIncTTC();
	sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

        strcpy((char *)gl_Send8583.Field24, "009");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	iLen = 2; 
	memcpy(&gl_Send8583.Field56[iLen],"\xdf\x27\x01",3);
	iLen += 3;		
	gl_Send8583.Field56[iLen] = NextFlag;	
        iLen ++;
	vLongToStr(iLen-2, 2, gl_Send8583.Field56);	
					
	sprintf((char *)gl_Send8583.Field61, "%06lu%-3.3s", gl_SysData.ulBatchNo, "091");

        //打8583包-填充http-发送-接收-解http-解8583包
        iRet = iHttpSendRecv8583(0);
        if (iRet)
        {
            vClearIcParam(2);
            //显示错误信息
            return iRet;
        }
        if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
        {
            vClearIcParam(2);

            //显示服务端错误信息
            vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);
            return -1;
        } 

         iLen = ulStrToLong(gl_Recv8583.Field56, 2);
	 memcpy(tmpbuf,gl_Recv8583.Field56+2,iLen);		
	 dbgHex("tmpbuf111",tmpbuf,iLen);	  

        i = 0;
   	while(i < iLen)
	{
	        if(memcmp(gl_Recv8583.Field56+2+i,"\xdf\x26",2)==0) 
	        {
	                i += 2;
			i ++;
                        len = gl_Recv8583.Field56[2 + i];
                        i++;
			memcpy(tmpbuf,gl_Recv8583.Field56 + 2 + i,len);			
			i += len;	
			
			j = 0;
			while(j<len){			
				if(memcmp(tmpbuf+j,"\x9f\x06",2)==0){
				j+=2;
				len1 = tmpbuf[j];
				j++;
				memcpy(applist[iAidNum].AID,tmpbuf+j,len1);
			        applist[iAidNum].AIDLen = len1;
				j += len1;	
				
				dbgHex("applist[iAidNum].AID",applist[iAidNum].AID,len1);	
				}
				
				 if(memcmp(tmpbuf+j,"\xdf\x25\x08",3)==0){
					j +=3;
					memcpy(applist[iAidNum].expiry,tmpbuf+j,8);
					j +=8;
					dbgHex("applist[iAidNum].expiry",applist[iAidNum].expiry,8);	
				}
				
			        iAidNum++;
			}
	        }else if(memcmp(gl_Recv8583.Field56+2+i,"\xdf\x27\x01",3) == 0)
        	{
        	        i += 3;
			NextFlag = gl_Recv8583.Field56[2 + i];
			i++;
        	}else 
        	       i++;
   	}      

         if(NextFlag == 0) 	break;
    }

	for(i = 0; i< iAidNum; i++){
		vClearLines(2);
		vDispVarArg(2, "No.%d", i+1);		

		memset(&gl_Send8583, 0, sizeof(st8583));
		strcpy((char *)gl_Send8583.Msg01, "0800");
		strcpy((char *)gl_Send8583.Field03, "990000");
		
		vIncTTC();
		sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

       	        strcpy((char *)gl_Send8583.Field24, "009");
		strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
		strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
		
		iLen = 2; 
		memcpy(&gl_Send8583.Field56[iLen],"\x9f\x06",2);
		iLen += 2;		
		gl_Send8583.Field56[iLen] = applist[i].AIDLen;
		iLen ++;		
		memcpy(&gl_Send8583.Field56[iLen],applist[i].AID,applist[i].AIDLen);		
		iLen += applist[i].AIDLen;
                dbgHex("applist[i].AID",applist[i].AID,applist[i].AIDLen);
				
		dbgHex("applist[i].expiry",applist[i].expiry,8);	
		memcpy(&gl_Send8583.Field56[iLen],"\xdf\x25\x08",3);
		iLen += 3;		
		memcpy(&gl_Send8583.Field56[iLen],applist[i].expiry,8);	
		iLen += 8;		

		vLongToStr(iLen-2, 2, gl_Send8583.Field56);
			
	        sprintf((char *)gl_Send8583.Field61, "%06lu%-3.3s", gl_SysData.ulBatchNo, "093");

		//打8583包-填充http-发送-接收-解http-解8583包
		iRet = iHttpSendRecv8583(0);
		if (iRet)
		{
			//显示错误信息
			return iRet;
		}
		if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
		{
			//显示服务端错误信息
			vShowHostErrMsg(gl_Recv8583.Field39, NULL);
			return -1;
		}

		if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
		{
			//显示服务端错误信息
			vShowHostErrMsg(gl_Recv8583.Field39, NULL);
			return -1;
		}

               iLen = gl_Recv8583.Field56[0]*256 + gl_Recv8583.Field56[1];
                j  = 0;
		k = 0;
		memset(tmpbuf,0x00,sizeof(tmpbuf));
                    while(j < iLen)
                    {
                    	if(memcmp(gl_Recv8583.Field56+2+j,"\xdf\x28\x01",3)==0)
                    	{
                    	   	 j  += 3;
			   	 j += 1;				
                    	}else if(memcmp(gl_Recv8583.Field56+2+j,"\xdf\x25\x08",3)==0)
                    	{
                    	  	j +=3;
			        j += 8;							   
                    	}else 
                    	{
                    		tmpbuf[k] = gl_Recv8583.Field56[2+j];
				k ++;
				j ++;
                    	}	
                    		
                    }	
						
			dbgHex("tmpbuf", tmpbuf,k);	
    #if 1        
    {
	//sdj aid参数没有9F09,但有9F08,需将9F08(TAG_ICC_APP_VERSION_NUMBER)替换为9F09(TAG_APP_VERSION_NUMBER)
	byte tmp[50];
	int tagBinLen;
	TagList tmpTagList;
	Tag3ByteList tmpTag3ByteList;
	uint16 tmpTLVBufLen;

	InitTagList(&tmpTagList);
	InitTag3ByteList(&tmpTag3ByteList);

	BuildTagListOneLevelBctc(tmpbuf, iLen, &tmpTagList, &tmpTag3ByteList);

	if(GetTagValueSize(&tmpTagList, TAG_APP_VERSION_NUMBER)==0 
	&& GetTagValueSize(&tmpTagList, TAG_ICC_APP_VERSION_NUMBER)>0)
	{
		tagBinLen=GetTagValueSize(&tmpTagList, TAG_ICC_APP_VERSION_NUMBER);			
		memcpy(tmp, GetTagValue(&tmpTagList, TAG_ICC_APP_VERSION_NUMBER), tagBinLen);

		SetTagValue(TAG_APP_VERSION_NUMBER, tmp, tagBinLen, &tmpTagList);
		removeTag(&tmpTagList, TAG_ICC_APP_VERSION_NUMBER);

		TagListBuildTLV(&tmpTagList, tmpbuf, &tmpTLVBufLen);

		dbgHex("9F08->9F09", tmpbuf, tmpTLVBufLen);
		iLen=tmpTLVBufLen;
	}else
		dbg("no 9F08 or have 9F09.");
		FreeTagList(&tmpTagList);
    	}
    #endif
		
	uiMemManaPutAid(i, tmpbuf, k);	
	}

	gl_SysInfo.uiAidNum=iAidNum;
	uiMemManaPutSysInfo();

	vDispMid(3,"参数下载成功");
	return(0);
}

int iDownLoadBIN(void)
{
       int iRet;
       u8 temp[4] = {0};
	int	iCnt,currentBinCnt = 0;
	uchar *tmpPtr;
	uchar downFlag = 0x32;
	u16 num;
	u32 iLen;
	
	_vCls();
	vDispCenter(1, "BIN表B下载", 1);

	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
	strcpy((char *)gl_Send8583.Msg01, "0800");
	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
	sprintf((char *)gl_Send8583.Field60, "00" "%06lu" "396", gl_SysData.ulBatchNo);

        currentBinCnt = 0x00;
	downFlag= 0x32;
	num=0;
        while(downFlag== 0x32)
        {
		if(currentBinCnt)
			sprintf((char *)temp, "%03ld", currentBinCnt+1);
		else
			memcpy(temp,"000",3);
		iLen = 3;		

		vLongToStr(iLen, 2, gl_Send8583.Field62);
		memcpy(gl_Send8583.Field62+2, temp,iLen);
		TraceHex("de62",gl_Send8583.Field62,iLen+2);	

	       
	            //打8583包-填充http-发送-接收-解http-解8583包
	        iRet = iHttpSendRecv8583(0);  
	       	if (iRet)
		{
			//显示错误信息
			return iRet;
		}
	        	
		if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
		{
			//显示服务端错误信息
			vShowHostErrMsg(gl_Recv8583.Field39, NULL);
			return -1;
		}

		//数据解析
		tmpPtr = &gl_Recv8583.Field62[2];
		iLen=ulStrToLong(gl_Recv8583.Field62,2);

	        //标志位0表示无卡BIN可更新，1表示无后续卡BIN下载，2表示有后续卡BIN下载
		downFlag = *tmpPtr;
		if(downFlag == 0x30)
		{
		        vMessage("无卡BIN可更新");
		}
		
		iLen --;
		tmpPtr ++;	

		//最后一条卡bin编号
		memset(temp,0x00,sizeof(temp));
		memcpy(temp,tmpPtr,3);
		currentBinCnt = atoi(temp);
		tmpPtr += 3;
		iLen -= 3;

		for(iCnt=0;iCnt< iLen;)
		{
			//保存卡bin	
			memcpy(&gl_CardBinb.ucCardList[num * 12],tmpPtr,12);
			num ++;
			tmpPtr += 12;
			iCnt += 12;
		}
	}

	//保存条数长度
	if(num > 0)
	{
           gl_CardBinb.ulCardNum[0] = (uchar)(num/256);
	   gl_CardBinb.ulCardNum[1] = (uchar)(num%256);
	}	

	//下载结束报文
	_vCls();
	vDispCenter(1, "BIN表B下载结束", 1);
	memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
	strcpy((char *)gl_Send8583.Msg01, "0800");

	strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
	sprintf((char *)gl_Send8583.Field60, "00" "%06lu" "397", gl_SysData.ulBatchNo);

	iRet = iHttpSendRecv8583(0);

	if (iRet)
	{
		//显示错误信息
		return iRet;
	}

	vMessage("BINB表下载成功");	
    return 0;
}


void  iDownLoadBackBIN(char * date,char * cardno)
{
	u16 count;
	char tmp_card[19+1];
	int i;
	
	count = gl_Cardclblack.ulCardNum[0] * 256 + gl_Cardclblack.ulCardNum[1];      

	if(strlen(gl_Cardclblack.szDate) ==0)
	{		
		strcpy(gl_Cardclblack.szDate, date);
		_uiXMemWrite(MEM_CARDBLACK, 0, date, 4);	
        }

        memset(tmp_card,' ',sizeof(tmp_card));
	memcpy(tmp_card, cardno, strlen(cardno));
	tmp_card[19]=0;
	
	//保存卡bin		
	if(count > 0)
	{
	       for(i = 0;i < count;i++)
	       	{
	       		if(memcmp( &gl_Cardclblack.ucCardList[i * 19], tmp_card, 19) == 0)
					return;
	       	}		   	
	  }
		
        strcpy(&gl_Cardclblack.ucCardList[count * 19], tmp_card);
	_uiXMemWrite(MEM_CARDBLACK, 4+2+ count * 19, tmp_card, 19);

	count = count +1;		
	gl_Cardclblack.ulCardNum[0] = (uchar)(count/256);
	gl_Cardclblack.ulCardNum[1] = (uchar)(count%256);
	_uiXMemWrite(MEM_CARDBLACK, 4, gl_Cardclblack.ulCardNum, 2);
	
	 return ;
}
	
//检查交易状态
int iCheckTranStat(uint uiTransType, uchar ucMode)
{   
    char szDateTime[14+1], szBuf[30];
    int ret;
	
    if(_uiCheckBattery()<=1)
    {
        vMessage("电池电量低,请先充电");
        return 3;
    }
    if(gl_SysData.ucPosLoginFlag==0)
    {
	ret = iDoPosLogin();
        if(ret != FUNCRET_OK)
	{
		return ret;
	}
	vClearLines(2);
    }

	#ifdef TMS_PROC
	_vGetTime(szDateTime);
	 if(memcmp(szDateTime, gl_SysInfo.ucTimeTms, 8))
	{
		memcpy(gl_SysInfo.ucTimeTms,szDateTime,8);
		uiMemManaPutSysInfo();
		iTmsProc(0, 1);
	}	
	#endif
	
    //跨日,自动重新签到
    _vGetTime(szDateTime);
//   strcpy(gl_SysData.sLoginTime,"\x20\x21\x10\x19\x13\x43\x55");	
    vOneTwo0(gl_SysData.sLoginTime, 7, szBuf);
    if(memcmp(szDateTime, szBuf, 8))
    {
	ret = iDoPosLogin();
        if(ret != FUNCRET_OK)
	{
		return ret;
	}
	vClearLines(2);
    }

    //跨日删除黑名单
    _vGetTime(szDateTime);
    if(memcmp(szDateTime+4, gl_Cardclblack.szDate, 4))
    {
	memset(&gl_Cardclblack, 0x00, sizeof(gl_Cardclblack));
	if(_uiXMemWrite(MEM_CARDBLACK, 0, &gl_Cardclblack, sizeof(stCardclblack)))
		return 1;
    }
	
     if(uiTransType<0x8000 && (gl_SysInfo.uiAidNum==0 || gl_SysInfo.uiCaKeyNum==0))
    {
        vMessage("IC卡参数错,请重新签到");
        return 1;
    }
    
    if( uiTransType!=TRANS_TYPE_BALANCE && uiTransType!= QRTRANS_TYPE_QUERY)
    {
        /*
        //检查打印机
        if(gl_SysInfo.ucPrinterAttr && _uiCheckBattery()<=2)
        {
            _vDisp(_uiGetVLines(), "电量低无法打印,是否继续?");
            if(iOK(3)!=1)
                return 3;
            _vDisp(_uiGetVLines(), "");
        }
        */
        
	if((gl_SysData.uiTransNum + gl_SysData.uiQrTransNum) >= gl_SysInfo.ucMaxTradeCnt)
	{
		vMessage("交易满,请先结算");
		return 2;
	}

	if(gl_SysData.ucFailSignNum>=gl_SysInfo.ucFailSignMax)
        {
            vDispCenter(_uiGetVLines()-1, "失败电签达限值", 0);
            //#if (PROJECT_CY20  == 1) && defined(APP_LKL)
            #ifdef APP_LKL
            //vDispCenter(_uiGetVLines(), "请结算上送或清理", 0);
            //#else
            vDispCenter(_uiGetVLines(), "请手工上送或清理", 0);
            #endif
            vMessage(NULL);
            return 2;
        }
    }      
    return 0;
}

int iCardTrans(uint uiTransType)
{
    int iRet;
    uchar szTmp[100];
    char  szTitle[10+1];
    ulong ulAmount,DepositAmount,GprsAmount,FirstSwipeAmount,SwipeAmount;
    int iEntry, mode;
    uchar ucEmvTransType;
    uchar szDisp[100];
    uchar ucMobileFlag=0;
	
    gl_ucNFCPinFlag=0;
    gl_ucQrTransFlag = 0;	
   _vCls();  
    switch(uiTransType)
    {
        case TRANS_TYPE_DAIRY_SALE:
            strcpy(szTitle, "日结消费");	
            ucEmvTransType=0x00;
            break;	
        case TRANS_TYPE_SALE:
            strcpy(szTitle, "消费");
            ucEmvTransType=0x00;
            break;
	case TRANS_TYPE_MOBILESALE:
            strcpy(szTitle, "手机pay");
            ucEmvTransType=0x00;
            break;		
        case TRANS_TYPE_PREAUTH:
            strcpy(szTitle, "预授权");
            ucEmvTransType=0x03;
            break;
        case TRANS_TYPE_BALANCE:
            strcpy(szTitle, "查余额");
            ucEmvTransType=0x31;
            break;
        case TRANS_TYPE_ICSALE:
            strcpy(szTitle, "插卡消费");
            ucEmvTransType=0x00;
            break;  
	case TRANS_TYPE_NFCPINSALE:
	    strcpy(szTitle, "消费凭密");
	    ucEmvTransType=0x00;
            break;
	case TRANS_TYPE_NFCPINPREAUTH:
	    strcpy(szTitle, "预授权凭密");
	    ucEmvTransType=0x00;
            break;		
    }
    vDispCenter(1, szTitle, 1); 
    
    if(iCheckTranStat(uiTransType, 0))
        return 1;

/*	
    if(gl_SysInfo.iCommType == VPOSCOMM_WIFI && uiTransType != TRANS_TYPE_BALANCE)
    {
    	vMessage("基站信息获取中请稍候");
    	iRet = iGetLocalPosition();
	if(iRet != 0)	
		return 1;
	vClearLines(2);
    }
*/
	
   dbg("gl_SysInfo.ucDepositFlag:%d\n",gl_SysInfo.ucDepositFlag);	
   if(uiTransType ==TRANS_TYPE_SALE || uiTransType == TRANS_TYPE_MOBILESALE)
   {
        FirstSwipeAmount =  atol(gl_SysInfo.ucFirstSwipeAmount);	
	GprsAmount = ( atol(gl_SysInfo.ucGprsAmount))/12;				
        if(gl_SysInfo.ucDepositFlag == 0)
       {       
                _vCls();
                if(gl_SysInfo.ucGprsUseFlag == 1 && gl_SysInfo.ucGprsPayFlag == 0)
		{              
			sprintf(szDisp, "该机具需缴纳服务费%lu.%02lu元+GPRS流量费%lu.%02lu元/月", FirstSwipeAmount/100, FirstSwipeAmount%100,GprsAmount/100, GprsAmount%100);
			vMessageMulEx(szDisp);		
			vMessage(NULL);  	
	         }	
		else 
		{
			sprintf(szDisp, "该机具需缴纳服务费%lu.%02lu元", FirstSwipeAmount/100, FirstSwipeAmount%100);
			vMessageMul(szDisp);		
		}			   
        }		
   }
   
    _vCls();
     vDispCenter(1, szTitle, 1); 	
    if(uiTransType!=TRANS_TYPE_BALANCE && gl_ucStandbyCardFlag != 1 && gl_ucStandbyCardFlag != 2)
    {
       if(iGetAmount(NULL, "请输入金额",NULL, szTmp)<=0)
      {        	
      		return 1;
       }
        ulAmount = atol(szTmp);
    }else
        ulAmount=0L;

     if(uiTransType ==TRANS_TYPE_SALE || uiTransType == TRANS_TYPE_MOBILESALE) 
     {
	DepositAmount = atol(gl_SysInfo.ucDepositAmount);
	GprsAmount = atol(gl_SysInfo.ucGprsAmount);	

	if(gl_SysInfo.ucDepositFlag == 0)
	{
		if(gl_SysInfo.ucGprsUseFlag == 1 && gl_SysInfo.ucGprsPayFlag == 0 && ulAmount < (DepositAmount + GprsAmount))
		{              
			sprintf(szDisp, "该机具需缴纳服务费%lu.%02lu元+GPRS流量费%lu.%02lu元/月", FirstSwipeAmount/100, FirstSwipeAmount%100,(GprsAmount/12)/100, (GprsAmount/12)%100);
			vMessageMulEx(szDisp);	
			vMessage(NULL);  
			return 1;
		}	
		else if(ulAmount < DepositAmount)
		{
			sprintf(szDisp, "该机具需缴纳服务费%lu.%02lu元", FirstSwipeAmount/100, FirstSwipeAmount%100);
			vMessageMul(szDisp);	
			return 1;
		}	
	}	

	SwipeAmount = atol(gl_SysInfo.ucSwipeAmount);
	GprsAmount = atol(gl_SysInfo.ucGprsAmount);	
		    
	if(gl_SysInfo.ucGprsUseFlag == 1 &&  gl_SysInfo.ucGprsPayFlag == 0 && ulAmount < (SwipeAmount + GprsAmount))
	{
		sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", (SwipeAmount + GprsAmount)/100, (SwipeAmount + GprsAmount)%100);
		vMessageMul(szDisp);
		return 1;

	}else if(ulAmount < SwipeAmount)
	{
		sprintf(szDisp, "单笔交易金额不能小于%lu.%02lu元", SwipeAmount/100, SwipeAmount%100);
		vMessageMul(szDisp);
		return 1;
	}	 
    }
  
    sprintf(szTmp, "金额:%lu.%02lu", ulAmount / 100, ulAmount % 100);
    if(uiTransType==TRANS_TYPE_BALANCE)
    {
        szTmp[0]=0;
    }
	if(gl_ucStandbyCardFlag == 0)	
	{
		mode=WAIT_IC | WAIT_NFC | WAIT_MAG;
		if(gl_SysInfo.ucNFCTransPrior && (uiTransType==TRANS_TYPE_SALE ||uiTransType==TRANS_TYPE_DAIRY_SALE))
		mode= WAIT_NFC | WAIT_MAG;
		if(uiTransType==TRANS_TYPE_ICSALE)
		mode=WAIT_IC;
		else if(uiTransType==TRANS_TYPE_NFCPINSALE || uiTransType==TRANS_TYPE_NFCPINPREAUTH ||uiTransType == TRANS_TYPE_MOBILESALE)
		mode=WAIT_NFC;

      		iEntry=0;
CARDTRANS_QPBOC_RETRY:  
		iEntry = iWaitCard(mode, szTitle, szTmp, gl_SysInfo.uiUiTimeOut);
		dbg("get card, entry:%d\n", iEntry);
		if (iEntry <= 0)
		{
			return iEntry;
		}
	}else if(gl_ucStandbyCardFlag == 1) 	
	{
		iEntry = 1;
	}else if(gl_ucStandbyCardFlag == 2)
	{
		iEntry = 2;
	}else if(gl_ucStandbyCardFlag == 3)
	{
		iEntry = 3;
	}
	
    _vCls();        //因显示过图片，此处需调用_vCls清屏
    vDispCenter(1, szTitle, DISP_REVERSE);    
    
    if(uiTransType==TRANS_TYPE_ICSALE)
        uiTransType=TRANS_TYPE_SALE;
    if(uiTransType==TRANS_TYPE_NFCPINSALE)
    {
        gl_ucNFCPinFlag=1;
        uiTransType=TRANS_TYPE_SALE;
    }
    if(uiTransType==TRANS_TYPE_NFCPINPREAUTH)
    {
        gl_ucNFCPinFlag=1;
        uiTransType=TRANS_TYPE_PREAUTH;
    }
    if(uiTransType==TRANS_TYPE_MOBILESALE)
    {
        uiTransType=TRANS_TYPE_SALE;
	ucMobileFlag = 1;
    }	
    memset(&gl_TransRec, 0, sizeof(gl_TransRec));
    gl_TransRec.uiTransType=uiTransType;
    gl_TransRec.ulAmount=ulAmount;

    if(ucMobileFlag == 1)
		gl_TransRec.ucMobileFlag = 1;
	
    if(iEntry ==3 || iEntry == 4)
    {
        //刷卡显示确认卡号
        uchar szMag2[45];
        char *p;

        szMag2[0]=0;
        _uiMagGet(2, szMag2);
        szMag2[20]=0;
        p=strchr(szMag2, '=');
        if(p==NULL)
        {
            vMessage("刷卡错");
            return -1;
        }
        *p=0;
		
        vClearLines(2);
        _vDisp(2, "请确认卡号:");
        _vDisp(3, szMag2);
        vDispCancelAndConfirm(1, 1);
        if(iOK(30)!=1)
            return -1;

        return iMagTrans(uiTransType, iEntry, -1, 0);
    }

    //IC卡
    if(iEntry==1)
        gl_TransRec.uiEntryMode=50;     //接触式IC卡
    else
        gl_TransRec.uiEntryMode=70;     //QPboc
    
    vPrtTransRecInfo();
    
    EMVL2_Transaction_SetParaFromUI(ucEmvTransType, 0, ulAmount, 0);
    if(iEntry!=1)
    {
        iRet=qpbocTransaction(NULL);
        _uiCloseCard(8);
	#ifdef QPBOC_RETRY
        if(iRet && iGetQPbocTryFlag())
        {
            _vCls();
            vDispCenter(1, szTitle, 1);
            vMessageEx("请重新挥卡", 80);
            _vDisp(3, "");
            
            iEntry=0;
            //非接失败,有插卡,直接转为插卡处理
            if((mode&WAIT_IC) && _uiTestCard(0)==1 && _uiResetCard(0, NULL) > 0)
            {
                iEntry=1;
            }
            goto CARDTRANS_QPBOC_RETRY;
        }
        #endif  
    }
	else
    {
        Transaction(" ");
        vTakeOutCard();
        _uiCloseCard(0);
    }
   gl_ucNFCPinFlag=0;
	
    return 0;
}

void vPrtTransRecInfo(void)
{
#ifdef JTAG_DEBUG    
    char szTmp[30];
    
    dbg("***** gl_TransRec *****\n");
    dbg("uiTransType=0x%04X\n", gl_TransRec.uiTransType);
    dbg("ucVoidFlag=%u\n", gl_TransRec.ucVoidFlag);
    dbg("ucUploadFlag=0x%02X\n", gl_TransRec.ucUploadFlag);
    dbgHex("sPan", gl_TransRec.sPan, 10);
    dbg("ucPanSerNo=%u\n", gl_TransRec.ucPanSerNo);
    dbg("ulAmount=%lu\n", gl_TransRec.ulAmount);
    dbg("ulBalance=%u\n", gl_TransRec.ulBalance);
    dbg("ulTTC=%u\n", gl_TransRec.ulTTC);
    dbg("ulVoidTTC=%u\n", gl_TransRec.ulVoidTTC);
    dbgHex("sDateTime", gl_TransRec.sDateTime, 6);
    
    sprintf(szTmp, "%.12s", gl_TransRec.sReferenceNo);
    dbg("sReferenceNo=%s\n", szTmp);
    
    dbg("uiEntryMode=%u\n", gl_TransRec.uiEntryMode);
    
    vOneTwo0(gl_TransRec.sOperNo, 1, szTmp);
    dbg("sOperNo=%s\n", szTmp);
    
    dbg("ucTransAttr=0x%02X\n", gl_TransRec.ucTransAttr);
    dbgHex("sIcData", gl_TransRec.sIcData, gl_TransRec.uiIcDataLen);
    dbg("***** gl_TransRec End*****\n\n");
#endif
}


//sdj pin密文
void vAppPackPinBlock(uchar *pszCardId, uchar *pszPin, uchar *psKey, int iKeyLen, uchar *psOut)
{
    uchar szPan[20+1], sTmp[32+1];
    uchar sPinBlk[16];
    int i;
    int iAlgLen;
    
    if(gl_SysInfo.ucSmFlag==0)
        iAlgLen=8;
    else
        iAlgLen=16;
    
    //卡号（2域）右边数第二位开始，向左取12位
    memcpy(szPan, pszCardId+strlen((char*)pszCardId)-13, 12);

    sPinBlk[0]=strlen((char*)pszPin);           //长度[1]+pin(压缩BCD)+0xFF[...]
    memset(sTmp, 'F', 30);
    memcpy(sTmp, pszPin, sPinBlk[0]);
    vTwoOne(sTmp, 30, sPinBlk+1);
   
    memset(sTmp, 0x00, 16);
    vTwoOne(szPan, 12, sTmp+iAlgLen-6);         //12字节卡号压缩后放在右侧,前补0x00

    for(i=0; i<iAlgLen; i++)
        sPinBlk[i]^=sTmp[i];
    
dbg("pan:%s, pin:%s, AlgLen=%d\n", pszCardId, pszPin, iAlgLen);
dbgHex("packpin xor", sPinBlk, 16);
dbgHex("pk", psKey, 16);
    
   if(gl_SysInfo.ucSmFlag==0)
    {
        if(iKeyLen==8)
            _vDes(ENCRYPT, sPinBlk, psKey, psOut);
        else if(iKeyLen==16)
            _vDes(TRI_ENCRYPT, sPinBlk, psKey, psOut);
        else if(iKeyLen==24)
            _vDes(TRI3_ENCRYPT, sPinBlk, psKey, psOut);
    }else if(gl_SysInfo.ucSmFlag==1)
    {
        _iSm4(ENCRYPT, sPinBlk, psKey, psOut);
    }
    
    dbgHex("pin blk", psOut, iAlgLen);
    return;
}

// 计算报文mac
// in  : psIn : 输入数据
//       iLen : 输入数据长度
// out : Mac  : 8字节Mac
// ret : 0    : OK
//       1    : 安全模块故障
//       2    : 无主密钥
int iCalMac(uchar *psIn, int iLen, uchar *psKey, uchar *psMac)
{
    //int   iRet;
   int i, l, k;
    uchar sBuf[8+1], sTmp[8+1],sBuf_sm4[16+1], sTmp_sm4[16+1];
    uchar key[16+1]={0};     

     memcpy(key, gl_SysData.sMacKey, 16);
	
    dbgHex("gl_SysData.sMacKey", gl_SysData.sMacKey, 16);
    dbg("macdata: %02X%02X ..., len:%d\n", psIn[0], psIn[1], iLen);
    dbgHex("mackey:", key, 16);
    dbgHex("psIn",psIn,iLen);

	if(gl_SysInfo.ucSmFlag == 0)
	{
		memset(sBuf, 0x00, 8);	

	       if(iLen%8)
		{
			l = iLen /8 +1;
			memset(psIn + iLen, 0x00, 8-iLen%8);
	       	}	
	       else
			l = iLen / 8 ;

		for (i = 0; i < l; i++) {
			for (k = 0; k < 8; k++) {
				sBuf[k] = sBuf[k] ^ psIn[i * 8 + k];
			}
			dbgHex("sBuf1", sBuf, 8);
			memset(sTmp,0x00,sizeof(sTmp));
			memcpy(sTmp,sBuf,8);
			memset(sBuf,0x00,sizeof(sBuf));
			_vDes(ENCRYPT, sTmp, key, sBuf);
			dbgHex("sBuf2", sBuf, 8);
		}
	
	    //右半部分解密
		memset(sTmp, 0, 8);
	        _vDes(DECRYPT, sBuf, key+8, sTmp);
	       dbgHex("sTmp", sTmp, 8);
			
	     //左半部分加密
	   	 _vDes(ENCRYPT, sTmp, key, psMac);
	    	dbgHex("psMac", psMac, 8);
	}	else
	{
		memset(sBuf_sm4, 0x00, 16);	

	       if(iLen%16)
		{
			l = iLen /16 +1;
			memset(psIn + iLen, 0x00, 16-iLen%16);
	       	}	
	       else
			l = iLen / 16 ;

		for (i = 0; i < l; i++) {
			for (k = 0; k < 16; k++) {
				sBuf_sm4[k] = sBuf_sm4[k] ^ psIn[i * 16 + k];
			}
			dbgHex("sBuf1_sm4", sBuf_sm4, 16);
			memset(sTmp_sm4,0x00,sizeof(sTmp_sm4));
			memcpy(sTmp_sm4,sBuf_sm4,16);
			memset(sBuf_sm4,0x00,sizeof(sBuf_sm4));
			_iSm4(ENCRYPT, sTmp_sm4, gl_SysData.sMacKey,sBuf_sm4);
			dbgHex("sBuf2_sm4", sBuf_sm4, 16);
		}

		//取前半部分
		memcpy(psMac,sBuf_sm4,8);	
		dbgHex("psMac", psMac, 8);
	}
	return(0);
}

int iPackReq8583(st8583 *pSend8583, char *sCommBuf, uint *puiLen)
{
	uint  uiSendLen, uiHeadLen;
	int   iRet;
    int   iMacLen;
    uchar CommBuf[2*1024];
    uchar tmpBuf[100+1];
    uchar MacKey[8+1];
		
    uiHeadLen=2;	
    //tpdu
    if(gl_SysInfo.sTPDU[0]==0x60)  
	     if(gl_ucQrTransFlag == 1)	
		 	memcpy(sCommBuf+uiHeadLen,gl_SysInfo.sQrTPDU,5); //扫码
	     else	 	
	     		memcpy(sCommBuf+uiHeadLen,gl_SysInfo.sTPDU,5); //银行卡
    else
        memcpy(sCommBuf+uiHeadLen,"\x60\x00\x00\x00\x00",5);
    uiHeadLen+=5;

    if(gl_SysInfo.ucSmFlag == 1)
		memcpy(sCommBuf+uiHeadLen,"\x15\x00",2);
    else 
		memcpy(sCommBuf+uiHeadLen,"\x14\x00",2);	
    uiHeadLen += 2;	
	
//    memcpy(sCommBuf+uiHeadLen, "\x61\x31\x00\x31\x00\x46", 6);
//    uiHeadLen+=6;

	uiSendLen = 0;
//	dbgHex("pSend8583", (uchar *)pSend8583, sizeof(st8583));
    iRet= iPack8583(Msg0, CjtData0, (uchar *)pSend8583, sCommBuf+uiHeadLen, &uiSendLen);
    if(iRet) {
        vPrt8583Fields(1, pSend8583);
        dbg("iPack8583 err:%d\n", iRet);
		return(2001);
	}
	if(memcmp(pSend8583->Field64, "\x00\x00\x00\x00", 4))
    {

   //     iMacLen = uiSendLen-8;
   // 
//		iRet = iCalMac(sCommBuf+uiHeadLen, iMacLen, NULL, sCommBuf+uiHeadLen+uiSendLen-8);
//		if(iRet) {
//			return(2001);
//		}


        
	 if(gl_ucQrTransFlag == 1)	
	{
	//扫码
	    memset(CommBuf,0x00,sizeof(CommBuf));
	    memset(tmpBuf,0x00,sizeof(tmpBuf));
	    memset(MacKey,0x00,sizeof(tmpBuf));	
			 
	   //41域
	        memcpy( CommBuf,pSend8583->Field41,8);

		strcpy(tmpBuf,pSend8583->Field04+1);
		strcpy(tmpBuf+11,pSend8583->Field11);
		strcpy(tmpBuf+11+6,pSend8583->Field42);
		vTwoOne(tmpBuf, 32, MacKey);

		dbgHex("CommBuf", CommBuf, strlen(CommBuf));
		dbgHex("MacKey", MacKey, 16);
		
		_vDes(TRI_ENCRYPT, CommBuf, MacKey,sCommBuf+uiHeadLen+uiSendLen-8);
	 }	
	 else	 	
	 { 	   
	   //银行卡
	   memset(CommBuf,0x00,sizeof(CommBuf));
	   iMacLen = 0;
	    //2域
	    if(strlen(pSend8583->Field02)%2)
	    {
	         memset(tmpBuf,'0',sizeof(tmpBuf));
		memcpy(tmpBuf+1,pSend8583->Field02,strlen(pSend8583->Field02));	 
                 vTwoOne(tmpBuf, strlen(pSend8583->Field02)+1, CommBuf  + iMacLen);
		 iMacLen += (strlen(pSend8583->Field02)+1)/2;	
			
	    }else 
	    {
	    	vTwoOne(pSend8583->Field02, strlen(pSend8583->Field02),  CommBuf  + iMacLen);
		iMacLen += strlen(pSend8583->Field02)/2;	
	    }

	   //3域
	   	vTwoOne(pSend8583->Field03, strlen(pSend8583->Field03),  CommBuf  + iMacLen);
	        iMacLen += strlen(pSend8583->Field03)/2;

	  //4域
		vTwoOne(pSend8583->Field04, strlen(pSend8583->Field04),  CommBuf  + iMacLen);
	        iMacLen += strlen(pSend8583->Field04)/2;
			
	   //11域
                vTwoOne(pSend8583->Field11, strlen(pSend8583->Field11),  CommBuf  + iMacLen);
	        iMacLen += strlen(pSend8583->Field11)/2;
			
	   //12域
               vTwoOne(pSend8583->Field12, strlen(pSend8583->Field12),  CommBuf  + iMacLen);
               iMacLen += strlen(pSend8583->Field12)/2;
		   
	   //13域
                vTwoOne(pSend8583->Field13, strlen(pSend8583->Field13),  CommBuf  + iMacLen);
                iMacLen += strlen(pSend8583->Field13)/2; 
	   
	   //49域
	  	memset(tmpBuf,'0',sizeof(tmpBuf));
		memcpy(tmpBuf+1,pSend8583->Field49,strlen(pSend8583->Field49));	 
                 vTwoOne(tmpBuf, strlen(pSend8583->Field49)+1, CommBuf  + iMacLen);
		 iMacLen += (strlen(pSend8583->Field49)+1)/2;	
	    
	   //41域
	        memcpy( CommBuf  + iMacLen,pSend8583->Field41,8);
	     	iMacLen += 8;
	 
		iRet = iCalMac(CommBuf, iMacLen, NULL, sCommBuf+uiHeadLen+uiSendLen-8);
		if(iRet) {
			return(2001);
		}
	 }

				
        //for vPrt8583Fields
        memcpy(pSend8583->Field64+2, sCommBuf+uiHeadLen+uiSendLen-8, 8);
	}
    vPrt8583Fields(1, pSend8583);	


    sCommBuf[0]=(uiHeadLen+uiSendLen-2)/256;
    sCommBuf[1]=(uiHeadLen+uiSendLen-2)%256;
  
    *puiLen=uiSendLen+uiHeadLen;
	dbg("\r\n==================start=============\r\n");
	dbgHex("req 8583", sCommBuf, uiSendLen+uiHeadLen);
	serialHex("send req 8583", sCommBuf, uiSendLen+uiHeadLen);
	_vDelay(10);
	dbg("\r\n===================end===============\r\n");

    return 0;
}

int iUnPackRsp8583(char *sCommBuf, uint uiRecvLen, st8583 *pRecv8583)
{
	uint  uiHeadLen;
	int   iRet;
    uchar sMac[8];
   uchar	posComHead[6+1]={0};		//EMV参数和公钥更新标志
   int   iMacLen;
       uchar CommBuf[2*1024];
    uchar tmpBuf[100+1];
    uchar MacKey[8+1];
	
//    uiHeadLen=2+5+6;
   uiHeadLen=2+5;
//   vOneTwo0(sCommBuf+uiHeadLen, 3, posComHead);
//   uiHeadLen +=6;
//   dbg("posComHead:%s\n",posComHead);
   uiHeadLen += 2;
   
   if( gl_ucProcReqFlag == 1 && gl_ucSignuploadFlag == 0)
  {
	if((posComHead[5]>='0' && posComHead[5]<='9')
		 ||(posComHead[5]=='b')||(posComHead[5]=='c'))
		 {
		    gl_SysInfo.proc_req = posComHead[5];
		}
  }
   
    memset((char *)pRecv8583, 0, sizeof(st8583));
    iRet = iUnPack8583(Msg0, CjtData0, sCommBuf+uiHeadLen, uiRecvLen-uiHeadLen, (uchar *)pRecv8583);
	if(iRet) {
        dbg("unpack 8583 err:%d\n", iRet);
		return(2002);
	}
    vPrt8583Fields(2, pRecv8583);

    /*
	if(strcmp((char *)(pSend8583->Field11), (char *)(pRecv8583->Field11)) != 0) {
		gl_RTD.uiHostTryTime ++;
		return(COMM_RECV); // 接收报文TTC不符, 认为接收失败
	}
    */

	//if(uiMacFlag && strcmp((char *)(pRecv8583->Field39), "00")==0) 
    //// 39域为00才验MAC 且 bitmap有64域

	dbg("\r\n================start================\r\n");
	dbgHex("res 8583", sCommBuf, uiRecvLen);
	_vDelay(5);
	dbg("\r\n==================end==================\r\n");
//	if(strcmp((char *)(pRecv8583->Field39), "14")==0)
//		strcpy(pRecv8583->Field39,"00");
       if(strcmp((char *)(pRecv8583->Field39), "00")==0 && (pRecv8583->BitMap[7]&0x01) && strcmp(pRecv8583->Msg01,"0830") && strcmp(pRecv8583->Field03,"440000")) //电签不校验MAC
      {
#if 0    
        //若为签到交易且44域有新mackey
        if(strcmp(pRecv8583->Msg01, "0830")==0 && memcmp(pRecv8583->Field03, "91", 2)==0 && pRecv8583->Field44[1]>16)
        {
            uchar sKeyTmp[8];
            _vDes(TRI_DECRYPT, pRecv8583->Field44 + 2 + 16, gl_SysInfo.sMasterKey, sKeyTmp);
            dbgHex("mack cipher", pRecv8583->Field44 + 2 + 16, 8);
            dbgHex("tmk", gl_SysInfo.sMasterKey, 16);
            dbgHex("new mackey", sKeyTmp, 8);
            
            iCalMac(sCommBuf+uiHeadLen, uiRecvLen-uiHeadLen-8, sKeyTmp, sMac);
        }else
#endif
		
//        iCalMac(sCommBuf+uiHeadLen, uiRecvLen-uiHeadLen-8, NULL, sMac);
          dbg("gl_ucQrTransFlag:%d\n",gl_ucQrTransFlag);
          if(gl_ucQrTransFlag == 1)	
	{
	//扫码
	    memset(CommBuf,0x00,sizeof(CommBuf));
	    memset(tmpBuf,0x00,sizeof(tmpBuf));
	    memset(MacKey,0x00,sizeof(tmpBuf));	
			 
	   //41域
	        memcpy( CommBuf,pRecv8583->Field41,8);

		strcpy(tmpBuf,pRecv8583->Field04+1);
		strcpy(tmpBuf+11,pRecv8583->Field11);
		strcpy(tmpBuf+11+6,pRecv8583->Field42);
		vTwoOne(tmpBuf, 32, MacKey);

		dbgHex("CommBuf", CommBuf, strlen(CommBuf));
		dbgHex("MacKey", MacKey, 16);
		
		_vDes(TRI_ENCRYPT, CommBuf, MacKey,sMac);
	 }	
	 else		
	 { 		 
	   //银行卡
	   memset(CommBuf,0x00,sizeof(CommBuf));
	   iMacLen = 0;
	    //2域
	    if(strlen(pRecv8583->Field02)%2)
	    {
	         memset(tmpBuf,'0',sizeof(tmpBuf));
		memcpy(tmpBuf+1,pRecv8583->Field02,strlen(pRecv8583->Field02));	 
                 vTwoOne(tmpBuf, strlen(pRecv8583->Field02)+1, CommBuf  + iMacLen);
		 iMacLen += (strlen(pRecv8583->Field02)+1)/2;	
			
	    }else 
	    {
	    	vTwoOne(pRecv8583->Field02, strlen(pRecv8583->Field02),  CommBuf  + iMacLen);
		iMacLen += strlen(pRecv8583->Field02)/2;	
	    }

	   //3域
	   	vTwoOne(pRecv8583->Field03, strlen(pRecv8583->Field03),  CommBuf  + iMacLen);
	        iMacLen += strlen(pRecv8583->Field03)/2;

	  //4域
		vTwoOne(pRecv8583->Field04, strlen(pRecv8583->Field04),  CommBuf  + iMacLen);
	        iMacLen += strlen(pRecv8583->Field04)/2;
			
	   //11域
                vTwoOne(pRecv8583->Field11, strlen(pRecv8583->Field11),  CommBuf  + iMacLen);
	        iMacLen += strlen(pRecv8583->Field11)/2;
			
	   //12域
               vTwoOne(pRecv8583->Field12, strlen(pRecv8583->Field12),  CommBuf  + iMacLen);
               iMacLen += strlen(pRecv8583->Field12)/2;
		   
	   //13域
                vTwoOne(pRecv8583->Field13, strlen(pRecv8583->Field13),  CommBuf  + iMacLen);
                iMacLen += strlen(pRecv8583->Field13)/2; 
	   
	   //49域		 
		memcpy(tmpBuf,pRecv8583->Field49,strlen(pRecv8583->Field49));	 
                 vTwoOne(tmpBuf, strlen(pRecv8583->Field49), CommBuf  + iMacLen);
		 iMacLen += strlen(pRecv8583->Field49)/2;	
		 
	   //38域
	         if(pRecv8583->Field38[0])
	        {
	        	memcpy( CommBuf  + iMacLen,pRecv8583->Field38,6);
	     		iMacLen += 6;
	         }		
			
	   //39域
	   	 memcpy( CommBuf  + iMacLen,pRecv8583->Field39,2);
	     	iMacLen += 2;
			
	   //41域
	        memcpy( CommBuf  + iMacLen,pRecv8583->Field41,8);
	     	iMacLen += 8;
	 
		iRet = iCalMac(CommBuf, iMacLen, NULL, sMac);
		if(iRet) {
			return(2001);
		}
		
	 }

	     if(memcmp(sMac, sCommBuf+uiRecvLen-8, 8) != 0) {
            dbg("unpack 8583, mac err!!!\n");
            dbg("msg mac", sCommBuf+uiRecvLen-8, 8);
            dbg("cal mac", sMac, 8);
            vMessage("交易失败,报文mac错");
			return(4);
		}

        dbg("recv 8583 mac ok.\n");
	}
	
    return 0;
}

//预授权撤销、预授权完成、退货,需要输入原交易信息
int iTrans2(uint uiTransType)
{
    int iRet;
    char szBuf[100], szTitle[20+1];
    int i;
    uchar ucEmvTransType;
    int  iEntry, mode;
    uchar szCardId[20+1], ucCardSeqNo;
    uchar szTrack2[50];

	uchar szDateTime[14+1];
	char *p;
	ulong ulAmount;
	char szRid[2+1];
	uchar ucFd60MsgCode;
	uchar ucPreAuthType=0;  //预授权类交易
	uchar ucNeedPin=1;

	uchar szEncPin[32+1];
	uchar ucByPass=gl_SysInfo.ucByPass;
	uchar asBuf[128+1]={0};
	
    _vCls();
   gl_ucQrTransFlag = 0;	
    switch(uiTransType)
    {
        case TRANS_TYPE_PREAUTHVOID:
            strcpy(szTitle, "预授权撤销");
            ucEmvTransType=0x20;
            break;
        case TRANS_TYPE_PREAUTH_COMP:
            strcpy(szTitle, "预授权完成");
            ucEmvTransType=0x00;
            break;
        case TRANS_TYPE_REFUND:
            strcpy(szTitle, "退货");
            ucEmvTransType=0x20;
            break;
        default:
            return 1;
    }
    vDispCenter(1, szTitle, 1); 
    if(iCheckTranStat(uiTransType, 0))
        return 1;
    
    if(uiTransType!=TRANS_TYPE_PREAUTH_COMP)
    {
        if(iMainOperCheck((char*)gl_SysInfo.szMainOperPwd))
            return -1;
    }
	
/*
    szBuf[0]=0;
    iEntry = iWaitCard(WAIT_IC | WAIT_NFC | WAIT_MAG, szTitle, szBuf, gl_SysInfo.uiUiTimeOut);
    if (iEntry <= 0)
        return iEntry;
*/

    memset(&gl_TransRec, 0, sizeof(gl_TransRec));
    gl_TransRec.uiTransType=uiTransType;

   if(uiTransType==TRANS_TYPE_REFUND)
    {
        _vDisp(2, "请输原交易参考号:");
        if(iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_CENTER_SPACE, 3, 4, szBuf, 12, 12, 12, 30)!=12)
            return -1;
        memcpy(gl_TransRec.sReferenceNo, szBuf, 12);
        vTwoOne(szBuf, 12, gl_TransRec.sVoidInfo);
    }

    vClearLines(2);
    _vDisp(2, "请输入原交易日期:");
    _vDisp(3, "      (月月日日)");
    while(1)	
   {	 
   	   iRet=iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_CENTER_SPACE, 4, 9, szBuf, 4, 4, 4, 30);
	    if(iRet<0)
	        return -1;
	    i=ulA2L(szBuf, 2);
	    if(i==0 || i>12)
	    {
	        vMessageLine(4,"无效日期");
	       	_vDisp(4, " ");
	       continue;
	    }
	    i=ulA2L(szBuf+2, 2);
	    if(i==0 || i>31)
	    {
	        vMessageLine(4,"无效日期");
		_vDisp(4, " ");
	        continue;
	    }
	   if (iRet == 4)
            break;	
    }	
    vTwoOne(szBuf, 4, gl_TransRec.sDateTime+1);
    vTwoOne(szBuf, 4, gl_TransRec.sVoidInfo+6);

    if(uiTransType!=TRANS_TYPE_REFUND)
    {
         vClearLines(2);
        _vDisp(2, "请输入原授权码:");
        iRet=iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_CENTER_SPACE, 3, 9, szBuf, 6, 6, 6, 30);
        if(iRet<0)
            return -1;
        memcpy(gl_TransRec.sAuthCode, szBuf, 6);
        memcpy(gl_TransRec.sVoidInfo, szBuf, 6);
    }

    vClearLines(2);
    if(iGetAmount(NULL, "请输入金额", NULL,szBuf)<=0)
        return 1;
    gl_TransRec.ulAmount = atol(szBuf);

   if(uiTransType == TRANS_TYPE_REFUND && gl_TransRec.ulAmount > gl_SysInfo.ulRefund_limit)	
   {
   	vClearLines(2);
	vMessage("退货金额超限");
	return -1;
   }
   
    sprintf(szBuf, "金额:%lu.%02lu", gl_TransRec.ulAmount / 100, gl_TransRec.ulAmount % 100);
     dbg("gl_SysInfo.ucHandCard:%d\n",gl_SysInfo.ucHandCard);
   if(gl_SysInfo.ucHandCard == 1)	
   	mode = WAIT_IC | WAIT_NFC | WAIT_MAG | WAIT_MAN;
   else
   	mode = WAIT_IC | WAIT_NFC | WAIT_MAG;
   dbg("mode:%02x\n",mode);
   iEntry=0;
TRANS2_QPBOC_RETRY: 
    iEntry = iWaitCard(mode, szTitle, szBuf, gl_SysInfo.uiUiTimeOut);
    if (iEntry <= 0)
        return iEntry;
	
    //根据刷卡插卡跳转
    dbg("get card, entry:%d\n", iEntry);
    //刷卡显示确认卡号
    if(iEntry ==3 || iEntry == 4)
    {		
	//刷卡显示确认卡号
        uchar szMag2[45];
        char *p;

        szMag2[0]=0;
        _uiMagGet(2, szMag2);
        szMag2[20]=0;
        p=strchr(szMag2, '=');
        if(p==NULL)
        {
            vMessage("刷卡错");
            return -1;
        }
        *p=0;
		
        vClearLines(2);
        _vDisp(2, "请确认卡号:");
        _vDisp(3, szMag2);
        vDispCancelAndConfirm(1, 1);
        if(iOK(30)!=1)
            return -1;

        return iMagTrans(uiTransType, iEntry, -1, 0);	
    }
	
     if(iEntry == 5)
     {
	
    int iRet;
    char szTitle[20+1];

    uchar szBuf[100];
    uchar szDateTime[14+1];
	ulong ulAmount;
	char szRid[2+1];
    uchar ucFd60MsgCode;
    uchar ucPreAuthType=0;  //预授权类交易
    uchar ucNeedPin=1;
  
    uchar szEncPin[32+1];
    uchar ucByPass=gl_SysInfo.ucByPass;
	uchar asBuf[128+1]={0};
	
	//手输显示确认卡号		
        vClearLines(2);
        _vDisp(2, "请确认卡号:");
        _vDisp(3, gl_ucSPan);
        vDispCancelAndConfirm(1, 1);
        if(iOK(30)!=1)
            return -1;

      vClearLines(2);
    _vDisp(2, "请输入卡有效期:");
    _vDisp(3, "      (年年月月)");
   memset(szBuf,0x00,sizeof(szBuf));	
    while(1)
    {
	    iRet=iInput(INPUT_NORMAL|INPUT_DOT_SPACE|INPUT_CENTER_SPACE|INPUT_INITIAL, 4, 9, szBuf, 4, 0, 4, 30);
	    if(iRet<0)
	        return -1;
	   if(iRet == 4)	
	   {
		i=ulA2L(szBuf+2, 2);
		if(i==0 || i>31)
		{
			vMessageLine(4,"无效日期");
			_vDisp(4, " ");
			 memset(szBuf,0x00,sizeof(szBuf));
			continue;
		}
		break;
	    }
	   
	   if (iRet == 0)
            break;	
	   
    }	   
    _vGetTime(szDateTime);
	vPrtTransRecInfo();

    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

   strcpy(gl_Send8583.Field14,szBuf);
		
    switch(uiTransType)
    {
        case TRANS_TYPE_REFUND:     //退货
            //流程:主管-卡-参考号-日期-金额-(不需密码)
            //无冲正
            strcpy(szTitle, "退货");
            strcpy((char *)gl_Send8583.Msg01, "0220");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=25;
            ucNeedPin=0;       
            break;
        case TRANS_TYPE_PREAUTHVOID:    //预授权撤销
            //流程:主管-卡-日期-授权码-金额-密码(参数)
            strcpy(szTitle, "预授权撤销");
            strcpy((char *)gl_Send8583.Msg01, "0100");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=11;
            ucPreAuthType=1; 
            ucNeedPin=gl_SysInfo.ucAuthVoidPinFlag;   
            break;
        case TRANS_TYPE_PREAUTH_COMP:    //预授权完成(请求)
            //流程:卡-日期-授权码-金额-密码(参数)
            strcpy(szTitle, "预授权完成");
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "000000");
            ucFd60MsgCode=20;
            ucPreAuthType=1;
            ucNeedPin=gl_SysInfo.ucAuthCompPinFlag;     
            break;
    }
    _vCls();
    vDispCenter(1, szTitle, 1);

    if(uiTransType==TRANS_TYPE_BALANCE)
        ulAmount=0;
    else
        ulAmount=gl_TransRec.ulAmount;
    if(ucNeedPin)
    {        
        iRet=iInputPinAndEnc(ucByPass, ulAmount, "请持卡人输入密码:", (char*)gl_ucSPan, szEncPin);
        if(iRet<0)
            return iRet;
    }else
        szEncPin[0]=0;

    strcpy((char*)gl_Send8583.Field02, gl_ucSPan);

    if(uiTransType!=TRANS_TYPE_BALANCE)
        sprintf((char *)gl_Send8583.Field04, "%012lu", gl_TransRec.ulAmount);

    vIncTTC();	
    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

    sprintf((char *)gl_Send8583.Field22, "%03d", 10);

    if(ucPreAuthType)
        strcpy((char *)gl_Send8583.Field25, "06");
    else
        strcpy((char *)gl_Send8583.Field25, "00");


    //Field35,Field36,Field38

	
//磁道加密	

  
    //消费撤销/预授权完成撤销/退货,37域上送原交易参考号
    if(uiTransType==TRANS_TYPE_SALEVOID || uiTransType==TRANS_TYPE_PREAUTH_COMPVOID
		|| uiTransType==TRANS_TYPE_REFUND)
    {
        memcpy(gl_Send8583.Field37, gl_TransRec.sReferenceNo, 12);
    }
    
    //预授权完成或撤销类交易,需上送原交易授权码
    if((gl_Send8583.Field03[0]=='2' || uiTransType==TRANS_TYPE_PREAUTH_COMP) 
        && gl_TransRec.sAuthCode[0])
    {
        memcpy(gl_Send8583.Field38, gl_TransRec.sAuthCode, 6);
    }
    gl_TransRec.uiTransType=uiTransType;
    gl_TransRec.sReferenceNo[0]=0;
    gl_TransRec.sAuthCode[0]=0;

    
    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
    strcpy((char *)gl_Send8583.Field49, "156");

    if(szEncPin[0])
    {
	strcpy((char*)gl_Send8583.Field26, "12");
	
        gl_Send8583.Field22[2]='1';
	if(gl_SysInfo.ucSmFlag == 0)	
        {
        	memcpy(gl_Send8583.Field52, "\x00\x08", 2);
        	vTwoOne(szEncPin, 16, gl_Send8583.Field52+2);
	}else if(gl_SysInfo.ucSmFlag == 1)
	{
		memcpy(gl_Send8583.Field52, "\x00\x08", 2);
		memset(gl_Send8583.Field52+2, 0x00, 8);         //国密算法,pin密文放在59域,52域填全0
	}
        memset(gl_Send8583.Field53, '0', 16);
        if(gl_SysInfo.ucSmFlag == 0)		
        	memcpy(gl_Send8583.Field53, "26", 2);			//双倍长密钥
        else if(gl_SysInfo.ucSmFlag == 1)
	{
		memcpy(gl_Send8583.Field53, "23", 2);			//双倍长密钥	

		//DE59 21号文(消费撤销)    
	    if( gl_TransRec.uiTransType == TRANS_TYPE_PREAUTHVOID 
			|| gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP 
			|| gl_TransRec.uiTransType == TRANS_TYPE_REFUND)
	    { 
	        GetPublicField059((char*)szEncPin,gl_ucSPan+strlen(gl_ucSPan)-6, gl_Send8583.Field59);
	    }
        }	

	memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密
    }
	else
    {
               gl_Send8583.Field22[2]='2';
		
	memset(gl_Send8583.Field53, '0', 16);
	if(gl_SysInfo.ucSmFlag == 0)	
        	memcpy(gl_Send8583.Field53, "06", 2);			//双倍长密钥
        else if(gl_SysInfo.ucSmFlag == 1)
		memcpy(gl_Send8583.Field53, "03", 2);			//双倍长密钥	
	
	memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密
    }

    if(iEntry==4)
        strcpy(szRid, "52");
    else
        strcpy(szRid, "50");
	
//DE58  基站信息
    if( gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID ||gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP
		|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
    {
    	 memset(asBuf,0,sizeof(asBuf));
    	GetPublicField058(asBuf);

	vLongToStr(strlen(asBuf), 2, gl_Send8583.Field58);
	memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf));
	dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf));
    }

//	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s%.2s", ucFd60MsgCode, gl_SysData.ulBatchNo, "000", szRid);

    sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s%.3s", ucFd60MsgCode, gl_SysData.ulBatchNo, "000","601");

   dbg("gl_Send8583.Field60:%s\n",gl_Send8583.Field60);
   
    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP
		|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
    {
        //sprintf((char *)gl_Send8583.Field61, "%06lu%06lu0000", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
        vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", 0L, 0L, szBuf);
    }

//DE63	


    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

    //打8583包-填充http-发送-接收-解http-解8583包
    if(uiTransType==TRANS_TYPE_BALANCE ||uiTransType==TRANS_TYPE_REFUND)
        iRet = iHttpSendRecv8583(0);
    else
    {
        iRet = iHttpSendRecv8583(1);
    }
    if (iRet)
    {
        	//显示错误信息
        	return iRet;	  
    }   
    if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
    {
        //显示服务端错误信息		
        vShowHostErrMsg(gl_Recv8583.Field39, NULL);
        return -1;
    }

    //磁条卡交易删冲正
    uiMemManaErase8583(MSG8583_TYPE_REV);
    
    gl_TransRec.uiTransType=uiTransType;
    gl_TransRec.uiEntryMode=atoi(gl_Send8583.Field22);
    if(gl_Recv8583.Field13[0]&&gl_Recv8583.Field12[0])  //用后台返回时间做交易记录时间
    {
        memcpy(szDateTime+4, gl_Recv8583.Field13, 4);
        memcpy(szDateTime+8, gl_Recv8583.Field12, 6);
    }
    vTwoOne(szDateTime+2, 12, gl_TransRec.sDateTime);
    
    if(gl_Recv8583.Field15[0])
        vTwoOne(gl_Recv8583.Field15, 4, gl_TransRec.sSettleDate);
    else
        memcpy(gl_TransRec.sSettleDate, "\x00\x00", 2);

    if(gl_Recv8583.Field14[0] && memcmp(gl_Recv8583.Field14, "0000", 4)!=0)		
   		 vTwoOne(gl_Recv8583.Field14, 4, gl_TransRec.sCardExpire);
	else
	{
      	  	vTwoOne(gl_Send8583.Field14, 4, gl_TransRec.sCardExpire);
	}
	
    vPackPan(gl_ucSPan, gl_TransRec.sPan);
    gl_TransRec.ucPanSerNo=0xFF;
    gl_TransRec.ulAmount=ulAmount;
    gl_TransRec.ucUploadFlag=0xFF;
    gl_TransRec.ucVoidFlag=0;
    gl_TransRec.ulTTC=atol(gl_Recv8583.Field11);
    if(gl_Recv8583.Field27[0]) {	
   	 if(gl_Recv8583.Field27[0] ==  '0')  gl_TransRec.ucCardClass = 0;
    	if(gl_Recv8583.Field27[0] ==  '1')  gl_TransRec.ucCardClass = 1;	
   	 dbg("gl_TransRec.ucCardClass:%d\n",gl_TransRec.ucCardClass );
    }
    memcpy(gl_TransRec.sReferenceNo, gl_Recv8583.Field37, 12);	// 交易参考号, 联机交易存在
    memcpy(gl_TransRec.sAuthCode, gl_Recv8583.Field38, 6);      // 授权码

    vTwoOne(gl_SysData.szCurOper, 2, gl_TransRec.sOperNo);

    if(gl_Recv8583.Field43[0])
    {
    	vGetPrtMerchInfo(gl_Recv8583.Field43, &gl_TransRec);
    }	

    //磁条卡交易时,将磁道信息保存至sIcData中
    if(gl_Send8583.Field35[0])
    {
        strcpy(gl_TransRec.sIcData, gl_Send8583.Field35);
    }
    if(gl_Send8583.Field36[0])
    {
        strcpy(gl_TransRec.sIcData+50, gl_Send8583.Field36);
    }

     //发卡行收单行
     if(gl_Recv8583.BitMap[5] & 0x10) {
		memcpy(gl_TransRec.issuerBankId,gl_Recv8583.Field44,8);
		dbgHex("gl_TransRec.issuerBankId", gl_TransRec.issuerBankId, 8);
		gl_TransRec.issuerBankId[8]=0;
		if (strlen(gl_Recv8583.Field44) > 11)
		{
			memcpy(gl_TransRec.recvBankId,gl_Recv8583.Field44+11,8);
			gl_TransRec.recvBankId[8]=0;
			dbgHex("gl_TransRec.recvBankId", gl_TransRec.recvBankId, 8);
		}
	}	
		
     //预设需签字+需纸签,电签成功后修改为无需纸签
    gl_TransRec.ucTransAttr=(0x02|0x04);
	 
    uiMemManaPutTransRec(gl_SysData.uiTransNum++, &gl_TransRec);
    gl_SysData.ulLastTransTTC = gl_TransRec.ulTTC;
    uiMemManaPutSysData();

    //vMessage("交易成功");
#if 1
    //签字
    if(gl_SysInfo.ucSupportESign == 1)
    	iBillSign(1, -1, NULL);
#endif

    _vCls();
    vDispCenter(1, szTitle, 1);    

#ifdef ENABLE_PRINTER 
    {		
        //打印票据
        vShowWaitEx("交易成功,打印票据...", -1, 0);
	dbg("gl_TransRec.ulAmount:%ld\n",gl_TransRec.ulAmount);	
        iPrintTrans(&gl_TransRec, 0, gl_Recv8583.Field63+3);
        vShowWaitEx(NULL, 0, 0x03);
    }
#else    
    {
        vMessage("交易成功");
    }
#endif    
    return 0;
     }
	 
    EMVL2_Transaction_SetParaFromUI(ucEmvTransType, 0x00, gl_TransRec.ulAmount, 0);
    if(iEntry==1)
    {
        gl_TransRec.uiEntryMode=50;     //接触式IC卡
  	iRet=iQuickTransaction(NULL);
  //      _uiCloseCard(0);
    }else{
        gl_TransRec.uiEntryMode=70;     //QPboc        
        iRet = qpbocTransaction(NULL);
        _uiCloseCard(8);
    #ifdef QPBOC_RETRY
        if(iRet && iGetQPbocTryFlag())
        {
            _vCls();
            vDispCenter(1, szTitle, 1);
            vMessageEx("请重新挥卡", 80);
            _vDisp(3, "");
            
            iEntry=0;
            //非接失败,有插卡,直接转为插卡处理
            if(/*(mode&WAIT_IC) && */_uiTestCard(0)==1 && _uiResetCard(0, NULL) > 0)
            {
                iEntry=1;
            }            
            goto TRANS2_QPBOC_RETRY;
        }
    #endif 		
    }
	dbg("iRet:%d\n",iRet);
	if(iRet != 0)
		return 1;
	
	szTrack2[0]=0;
	szCardId[0]=0;
	if(iGetEnvCardId(szCardId, &ucCardSeqNo, szTrack2))
	{
	vClearLines(2);
	vMessage("读卡失败");
	if(iEntry==1)
	{
	    //vTakeOutCard();
	    //_uiCloseCard(0);
	}
	return 1;
	}

	_vGetTime(szDateTime);
		vPrtTransRecInfo();

	    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
			
	    switch(uiTransType)
	    {
	        case TRANS_TYPE_REFUND:     //退货
	            //流程:主管-卡-参考号-日期-金额-(不需密码)
	            //无冲正
	            strcpy(szTitle, "退货");
	            strcpy((char *)gl_Send8583.Msg01, "0220");
	            strcpy((char *)gl_Send8583.Field03, "200000");
	            ucFd60MsgCode=25;
	            ucNeedPin=0;       
	            break;
	        case TRANS_TYPE_PREAUTHVOID:    //预授权撤销
	            //流程:主管-卡-日期-授权码-金额-密码(参数)
	            strcpy(szTitle, "预授权撤销");
	            strcpy((char *)gl_Send8583.Msg01, "0100");
	            strcpy((char *)gl_Send8583.Field03, "200000");
	            ucFd60MsgCode=11;
	            ucPreAuthType=1; 
	            ucNeedPin=gl_SysInfo.ucAuthVoidPinFlag;   
	            break;
	        case TRANS_TYPE_PREAUTH_COMP:    //预授权完成(请求)
	            //流程:卡-日期-授权码-金额-密码(参数)
	            strcpy(szTitle, "预授权完成");
	            strcpy((char *)gl_Send8583.Msg01, "0200");
	            strcpy((char *)gl_Send8583.Field03, "000000");
	            ucFd60MsgCode=20;
	            ucPreAuthType=1;
	            ucNeedPin=gl_SysInfo.ucAuthCompPinFlag;     
	            break;
	    }
	    _vCls();
	    vDispCenter(1, szTitle, 1);

	    if(uiTransType==TRANS_TYPE_BALANCE)
	        ulAmount=0;
	    else
	        ulAmount=gl_TransRec.ulAmount;
	    if(ucNeedPin)
	    {        
	        iRet=iInputPinAndEnc(ucByPass, ulAmount, "请持卡人输入密码:", (char*)szCardId, szEncPin);
	        if(iRet<0)
	            return iRet;
	    }else
	        szEncPin[0]=0;

	  strcpy((char*)gl_Send8583.Field02, szCardId);

	    if(uiTransType!=TRANS_TYPE_BALANCE)
	        sprintf((char *)gl_Send8583.Field04, "%012lu", gl_TransRec.ulAmount);

	    vIncTTC();	
	    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

        p=strchr((char*)szTrack2, 'D');
        if(p)
            memcpy((char *)gl_Send8583.Field14, p+1, 4);
		
	    sprintf((char *)gl_Send8583.Field22, "%03d", gl_TransRec.uiEntryMode);

           if(ucCardSeqNo!=0xFF)
		     	sprintf((char *)gl_Send8583.Field23, "%03d", ucCardSeqNo);
								
	    if(ucPreAuthType)
	        strcpy((char *)gl_Send8583.Field25, "06");
	    else
	        strcpy((char *)gl_Send8583.Field25, "00");


	    //Field35,Field36,Field38		
	//磁道加密	
   	 if(szTrack2[0]){
		AjustTrack2Data(szTrack2);	
		EncryptMagData(szTrack2, strlen(szTrack2), gl_Send8583.Field35);
	}
	  
	    //消费撤销/预授权完成撤销/退货,37域上送原交易参考号
	    if(uiTransType==TRANS_TYPE_SALEVOID || uiTransType==TRANS_TYPE_PREAUTH_COMPVOID
			|| uiTransType==TRANS_TYPE_REFUND)
	    {
	        memcpy(gl_Send8583.Field37, gl_TransRec.sReferenceNo, 12);
	    }
	    
	    //预授权完成或撤销类交易,需上送原交易授权码
	    if((gl_Send8583.Field03[0]=='2' || uiTransType==TRANS_TYPE_PREAUTH_COMP) 
	        && gl_TransRec.sAuthCode[0])
	    {
	        memcpy(gl_Send8583.Field38, gl_TransRec.sAuthCode, 6);
	    }
	    gl_TransRec.uiTransType=uiTransType;
	    gl_TransRec.sReferenceNo[0]=0;
	    gl_TransRec.sAuthCode[0]=0;

	    
	    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
	    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
	    strcpy((char *)gl_Send8583.Field49, "156");

	    if(szEncPin[0])
	    {
			strcpy((char*)gl_Send8583.Field26, "12");
		
	        gl_Send8583.Field22[2]='1';
		if(gl_SysInfo.ucSmFlag == 0)		
	       { 	
	       		memcpy(gl_Send8583.Field52, "\x00\x08", 2);
	        	vTwoOne(szEncPin, 16, gl_Send8583.Field52+2);
		}
		else if(gl_SysInfo.ucSmFlag == 1)
		{
			memcpy(gl_Send8583.Field52, "\x00\x08", 2);
			memset(gl_Send8583.Field52+2, 0x00, 8);         //国密算法,pin密文放在59域,52域填全0

		}
	        memset(gl_Send8583.Field53, '0', 16);
		if(gl_SysInfo.ucSmFlag == 0)		
	        	memcpy(gl_Send8583.Field53, "26", 2);			//双倍长密钥
	         else if(gl_SysInfo.ucSmFlag == 1)
	         {
	         	memcpy(gl_Send8583.Field53, "23", 2);			//双倍长密钥
	         			//DE59 21号文(消费撤销)    
			    if( gl_TransRec.uiTransType == TRANS_TYPE_PREAUTHVOID 
					|| gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP 
					|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND )
			    { 
			        GetPublicField059((char*)szEncPin,szCardId+strlen(szCardId)-6, gl_Send8583.Field59);
			    }
	         }
	        if(gl_SysInfo.magEncrypt == 1)	
        		memcpy(&gl_Send8583.Field53[2], "1", 1);		//磁道加密
        	else if(gl_SysInfo.magEncrypt == 0)	
			memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密
	    }
		else
	    {
	        gl_Send8583.Field22[2]='2';

		memset(gl_Send8583.Field53, '0', 16);
		if(gl_SysInfo.ucSmFlag == 0)	
			memcpy(gl_Send8583.Field53, "06", 2);			//双倍长密钥
		else if(gl_SysInfo.ucSmFlag == 1)
			memcpy(gl_Send8583.Field53, "03", 2);			//双倍长
		if(gl_SysInfo.magEncrypt == 1)	
        		memcpy(&gl_Send8583.Field53[2], "1", 1);		//磁道加密
        	else if(gl_SysInfo.magEncrypt == 0)	
			memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密
	    }

	    if(iEntry==4)
	        strcpy(szRid, "52");
	    else
	        strcpy(szRid, "50");
		
	//DE58  基站信息
	    if( gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID ||gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP
			|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
	    {
	    	 memset(asBuf,0,sizeof(asBuf));
	    	GetPublicField058(asBuf);

		vLongToStr(strlen(asBuf)+3, 2, gl_Send8583.Field58);
		memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf)+3);
		dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf)+3);
	    }

	//	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s%.2s", ucFd60MsgCode, gl_SysData.ulBatchNo, "000", szRid);

	    sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s%.3s", ucFd60MsgCode, gl_SysData.ulBatchNo, "000","601");

	    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP
			|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
	    {
	        //sprintf((char *)gl_Send8583.Field61, "%06lu%06lu0000", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
	        vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
	        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", 0L, 0L, szBuf);
	    }

	//DE63	
         if(gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
		sprintf((char *)gl_Send8583.Field63, "%-3.3s", "CUP");	

	    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

	    //打8583包-填充http-发送-接收-解http-解8583包
	    if(uiTransType==TRANS_TYPE_BALANCE ||uiTransType==TRANS_TYPE_REFUND)
	        iRet = iHttpSendRecv8583(0);
	    else
	    {
	        iRet = iHttpSendRecv8583(1);
	    }
	    if (iRet)
	    {
        	//显示错误信息
        	return iRet;	 
	
	    }   
	    if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
	    {
	        //显示服务端错误信息		
	        vShowHostErrMsg(gl_Recv8583.Field39, NULL);
	        return -1;
	    }

	    //磁条卡交易删冲正
	    uiMemManaErase8583(MSG8583_TYPE_REV);
	    
	    gl_TransRec.uiTransType=uiTransType;
	    gl_TransRec.uiEntryMode=atoi(gl_Send8583.Field22);
	    if(gl_Recv8583.Field13[0]&&gl_Recv8583.Field12[0])  //用后台返回时间做交易记录时间
	    {
	        memcpy(szDateTime+4, gl_Recv8583.Field13, 4);
	        memcpy(szDateTime+8, gl_Recv8583.Field12, 6);
	    }
	    vTwoOne(szDateTime+2, 12, gl_TransRec.sDateTime);
	    
	    if(gl_Recv8583.Field15[0])
	        vTwoOne(gl_Recv8583.Field15, 4, gl_TransRec.sSettleDate);
	    else
	        memcpy(gl_TransRec.sSettleDate, "\x00\x00", 2);

	    if(gl_Recv8583.Field14[0] && memcmp(gl_Recv8583.Field14, "0000", 4)!=0)		
	   		 vTwoOne(gl_Recv8583.Field14, 4, gl_TransRec.sCardExpire);
		else
		{
	      	  	vTwoOne(gl_Send8583.Field14, 4, gl_TransRec.sCardExpire);
		}
		
	    vPackPan(gl_ucSPan, gl_TransRec.sPan);
	    gl_TransRec.ucPanSerNo=0xFF;
	    gl_TransRec.ulAmount=ulAmount;
	    gl_TransRec.ucUploadFlag=0xFF;
	    gl_TransRec.ucVoidFlag=0;
	    gl_TransRec.ulTTC=atol(gl_Recv8583.Field11);
	    if(gl_Recv8583.Field27[0]) {	
	   	 if(gl_Recv8583.Field27[0] ==  '0')  gl_TransRec.ucCardClass = 0;
	    	if(gl_Recv8583.Field27[0] ==  '1')  gl_TransRec.ucCardClass = 1;	
	   	 dbg("gl_TransRec.ucCardClass:%d\n",gl_TransRec.ucCardClass );
	    }
	    memcpy(gl_TransRec.sReferenceNo, gl_Recv8583.Field37, 12);	// 交易参考号, 联机交易存在
	    memcpy(gl_TransRec.sAuthCode, gl_Recv8583.Field38, 6);      // 授权码
	    vPackPan(szCardId, gl_TransRec.sPan);
	    vTwoOne(gl_SysData.szCurOper, 2, gl_TransRec.sOperNo);

	    if(gl_Recv8583.Field43[0])
	    {
	    	vGetPrtMerchInfo(gl_Recv8583.Field43, &gl_TransRec);
	    }	

	    //磁条卡交易时,将磁道信息保存至sIcData中
	    if(gl_Send8583.Field35[0])
	    {
	        strcpy(gl_TransRec.sIcData, gl_Send8583.Field35);
	    }
	    if(gl_Send8583.Field36[0])
	    {
	        strcpy(gl_TransRec.sIcData+50, gl_Send8583.Field36);
	    }

	     //发卡行收单行
	     if(gl_Recv8583.BitMap[5] & 0x10) {
			memcpy(gl_TransRec.issuerBankId,gl_Recv8583.Field44,8);
			dbgHex("gl_TransRec.issuerBankId", gl_TransRec.issuerBankId, 8);
			gl_TransRec.issuerBankId[8]=0;
			if (strlen(gl_Recv8583.Field44) > 11)
			{
				memcpy(gl_TransRec.recvBankId,gl_Recv8583.Field44+11,8);
				gl_TransRec.recvBankId[8]=0;
				dbgHex("gl_TransRec.recvBankId", gl_TransRec.recvBankId, 8);
			}
		}	
			
	     //预设需签字+需纸签,电签成功后修改为无需纸签
	    gl_TransRec.ucTransAttr=(0x02|0x04);
		 
	    uiMemManaPutTransRec(gl_SysData.uiTransNum++, &gl_TransRec);
	    gl_SysData.ulLastTransTTC = gl_TransRec.ulTTC;
	    uiMemManaPutSysData();

	    //vMessage("交易成功");
#if 1
	    //签字
	    if(gl_SysInfo.ucSupportESign == 1)
	    	iBillSign(1, -1, NULL);
#endif

	    _vCls();
	    vDispCenter(1, szTitle, 1);    

#ifdef ENABLE_PRINTER 
	    {			
	        //打印票据
	        vShowWaitEx("交易成功,打印票据...", -1, 0);
	        iPrintTrans(&gl_TransRec, 0, gl_Recv8583.Field63+3);
	        vShowWaitEx(NULL, 0, 0x03);
	    }
#else    
	    {
	        vMessage("交易成功");
	    }
#endif 
	
    return 0;
}

int iTransVoid(uint uiTransType)
{
    int iRet;
    char szBuf[100], szTitle[20+1];
    ulong ulVoidTTC;
    char szVoidPan[20+1];
    int iIndex;
    int i;
    uchar ucEmvTransType;
    uint uiOldTransType;
    int col;
    uchar ucFd60MsgCode;
    uchar ucPreAuthType=0;  //预授权类交易
     uchar ucNeedPin=1;
   uchar ucNeedCard=1;     //撤销类交易是否用卡	
     uchar szEncPin[32+1];
    uchar szDateTime[14+1];
   ulong ulAmount;
   uchar ucByPass=gl_SysInfo.ucByPass;
    int  iEntry;
	uchar szCardId[20+1], ucCardSeqNo;
    uchar szTrack2[50];
	uchar asBuf[128+1]={0};
   stQrTransRec QrTrRec;
   
    _vCls();
    gl_ucQrTransFlag = 0;
    switch(uiTransType)
    {
        case TRANS_TYPE_SALEVOID:
            strcpy(szTitle, "消费撤销");
            uiOldTransType=TRANS_TYPE_SALE;
            ucEmvTransType=0x20;
	   ucNeedCard=gl_SysInfo.ucSaleVoidCardFlag;		
            break;          
        case TRANS_TYPE_PREAUTH_COMPVOID:
            strcpy(szTitle, "预授权完成撤销");
            uiOldTransType=TRANS_TYPE_PREAUTH_COMP;
            ucEmvTransType=0x20;
	    ucNeedCard=gl_SysInfo.ucAuthCompleteVoidCardFlag;
            break;
        case TRANS_TYPE_PREAUTHVOID:
            //预授权撤销不查交易记录,需输入交易信息,转为使用iTrans2接口
            return iTrans2(uiTransType);
        default:
            return 1;
    }
    vDispCenter(1, szTitle, 1); 
    if(iCheckTranStat(uiTransType, 0))
        return 1;
        
        if(iMainOperCheck((char*)gl_SysInfo.szMainOperPwd))
            return -1;
        dbg("dd uiTransType=%04X, uiOldTransType=%04X\n", gl_TransRec.uiTransType, uiOldTransType);
 
    ulVoidTTC=0L;
    iIndex=0;
    if(uiTransType!=TRANS_TYPE_PREAUTH_COMP && uiTransType!=TRANS_TYPE_PREAUTHVOID)
    {
        vClearLines(2);
        _vDisp(2, "请输入原凭证号:");
	col=(_uiGetVCols()-6)/2;	
        iRet=iInput(INPUT_NORMAL|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, col+1, szBuf, 6, 1, 6, 30);
        if(iRet<0)
            return -1;
        ulVoidTTC=atol(szBuf);
        if(ulVoidTTC==0)
        {
            vMessage("无效凭证号");
            return -1;
        }
        
        vClearLines(2);

        for(iIndex=0; iIndex<gl_SysData.uiQrTransNum; iIndex++)
        {
		memset(&QrTrRec,0,sizeof(stQrTransRec));
		if(uiMemManaGetQrTransRec(iIndex, &QrTrRec))
		{
			vMessage("读取交易记录失败");
			return -1;
		}
		dbg("ulTTC=%d\r\n",QrTrRec.ulTTC);
		if(QrTrRec.ulTTC == ulVoidTTC)
		{
	       		vDispMid(3,"该交易为二维码交易");	
		 	vDispMid(4,"请到二维码菜单撤销");	
			vMessage(NULL);
			return -1;
		}	
        }
				
        for(iIndex=0; iIndex<gl_SysData.uiTransNum; iIndex++)
        {
            if(uiMemManaGetTransRec(iIndex, &gl_TransRec))
            {
                vMessage("读取交易记录失败");
                return -1;
            }
            if(gl_TransRec.ulTTC==ulVoidTTC)
                break;
        }
        
        if(iIndex>=gl_SysData.uiTransNum)
        {
            //return iQrPayVoid(ulVoidTTC);   //搜索扫码支付记录
            vMessage("无此交易记录");
            return -1;
        }
        
        if(gl_TransRec.uiTransType!=uiOldTransType)
        {
            vMessage("交易类型不匹配");
            return -1;
        }
        if(TRANS_TYPE_SALE==uiOldTransType && gl_TransRec.ucUploadFlag!=0xFF)
        {
            vMessage("脱机交易不可撤销");
            return -1;
        }

        if(gl_TransRec.ucVoidFlag)
        {
            vMessageEx("该交易已撤销", 800);
            return -1;
        }
				
        vClearLines(2);
        i=1;
        vMemcpy0((uchar*)szBuf, (uchar*)szTitle, strlen(szTitle)-4);
        vDispVarArg(i++, "原交易:%s", szBuf);
        vUnpackPan(gl_TransRec.sPan, (uchar*)szVoidPan);
        if(strlen(szVoidPan)+5<=_uiGetVCols())
            vDispVarArg(i++, "卡号:%s", szVoidPan);
        else
            vDispVarArg(i++, "卡号:%.6s***%.7s", szVoidPan, szVoidPan+strlen(szVoidPan)-7);
        vDispVarArg(i++, "凭证号:%06lu", gl_TransRec.ulTTC);
        vDispVarArg(i++, "金额:%lu.%02lu", gl_TransRec.ulAmount/100, gl_TransRec.ulAmount%100);
        if(gl_TransRec.sReferenceNo[0])
            vDispVarArg(i++, "参考号:%.12s", gl_TransRec.sReferenceNo);


        vDispCenter(++i, "请确认以上原交易信息", 0);
        vDispCenter(++i, "按[确认]后继续", 0);
        if(!bOK())
            return -1;

	_vCls();
        vDispCenter(1, szTitle, 1); 
        sg_iVoidRecIndex=iIndex;
    
        sprintf(szBuf, "金额:%lu.%02lu元", gl_TransRec.ulAmount / 100, gl_TransRec.ulAmount % 100);
    }else
        szBuf[0]=0;
	 dbg("ucNeedCard:%d\n",ucNeedCard);
	  gl_TransRec.ucSignupload=0x00; 
         if(ucNeedCard)	
     	{          dbg("need card\n");
	    	    iEntry=0;
TRANSVOID_QPBOC_RETRY:   	 
		    iEntry = iWaitCard(WAIT_IC | WAIT_NFC | WAIT_MAG, szTitle, szBuf, gl_SysInfo.uiUiTimeOut);
		    if (iEntry <= 0)
		        return iEntry;
		   
		    dbg("get card, entry:%d\n", iEntry);
		    //刷卡显示确认卡号
		    if(iEntry>=3)
		    {
			        char *p;

			        szBuf[0]=0;
			        _uiMagGet(2, szBuf);
			        p=strchr(szBuf, '=');
			        if(p==NULL)
			        {
			            vMessage("刷卡错");
			            return -1;
			        }
			        *p=0;
			        if(szVoidPan[0] && strcmp(szBuf, szVoidPan))
			        {
			            vMessage("卡号与原交易不一致");
			            return -1;
			        }		 
				return iMagTrans(uiTransType, iEntry, iIndex, ulVoidTTC);
			}

		    gl_TransRec.uiTransType=uiTransType;
		    gl_TransRec.ulVoidTTC=ulVoidTTC;
		    EMVL2_Transaction_SetParaFromUI(ucEmvTransType, 0x00, gl_TransRec.ulAmount, 0);
		    if(iEntry==1)
		    {
		        gl_TransRec.uiEntryMode=50;     //接触式IC卡
		        iRet=iQuickTransaction(NULL);
		    //    _uiCloseCard(0);
		    }else{
		        gl_TransRec.uiEntryMode=70;     //QPboc        
		        iRet = qpbocTransaction(NULL);
		        _uiCloseCard(8);
			#ifdef QPBOC_RETRY
		        if(iRet && iGetQPbocTryFlag())
		        {
		            _vCls();
		            vDispCenter(1, szTitle, 1);
		            vMessageEx("请重新挥卡", 80);
		            _vDisp(3, "");
		            
		            iEntry=0;
		            if(/*(mode&WAIT_IC) && */_uiTestCard(0)==1 && _uiResetCard(0, NULL) > 0)
		            {
		                iEntry=1;
		            }              
		            goto TRANSVOID_QPBOC_RETRY;
		        }
		        #endif 	
		    }
                    dbg("iRet:%d\n",iRet);					 	
		    if(iRet != 0)
				return 1;
				
		    szTrack2[0]=0;
		    szCardId[0]=0;
		    if(iGetEnvCardId(szCardId, &ucCardSeqNo, szTrack2))
		    {
		        vClearLines(2);
		        vMessage("读卡失败");
		        if(iEntry==1)
		        {
		            //vTakeOutCard();
		            //_uiCloseCard(0);
		        }
		        return 1;
		    }

		    _vGetTime(szDateTime);
			vPrtTransRecInfo();

		    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
		    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
		    
		    switch(uiTransType)
		    {			
		        case TRANS_TYPE_SALEVOID:   //消费撤销
		            //流程:主管-凭证号-原交易-卡(参数)-密码(参数)
		            strcpy((char *)gl_Send8583.Msg01, "0200");
		            strcpy((char *)gl_Send8583.Field03, "200000");
		            ucFd60MsgCode=23;
		            ucNeedPin=gl_SysInfo.ucSaleVoidPinFlag;
		            break;
		        case TRANS_TYPE_PREAUTH_COMPVOID:    //预授权完成撤销
		            //流程:主管-凭证号-原交易-卡(参数)-(不需密码)
		            strcpy((char *)gl_Send8583.Msg01, "0200");
		            strcpy((char *)gl_Send8583.Field03, "200000");
		            ucFd60MsgCode=21;
		            ucPreAuthType=1;
		            ucNeedPin=gl_SysInfo.ucAuthCompVoidPinFlag;            
		            break;
		    }

		     vUnpackPan(gl_TransRec.sPan, szCardId);   
		     ulAmount=gl_TransRec.ulAmount;
		    if(ucNeedPin)
		    {
		        
		        iRet=iInputPinAndEnc(ucByPass, ulAmount, "请持卡人输入密码:", (char*)szCardId, szEncPin);
		        if(iRet<0)
		            return iRet;
		    }else
		        szEncPin[0]=0;

		    strcpy((char*)gl_Send8583.Field02, szCardId);

		    if(uiTransType!=TRANS_TYPE_BALANCE)
		        sprintf((char *)gl_Send8583.Field04, "%012lu", gl_TransRec.ulAmount);

		    vIncTTC();	
		    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

		    sprintf((char *)gl_Send8583.Field22, "%03d", gl_TransRec.uiEntryMode);

                     if(ucCardSeqNo!=0xFF)
		     	sprintf((char *)gl_Send8583.Field23, "%03d", ucCardSeqNo);
						
	//	     vOneTwo(gl_TransRec.sCardExpire, 2, (char *)gl_Send8583.Field14);
		    if(ucPreAuthType)
		        strcpy((char *)gl_Send8583.Field25, "06");
		    else
		        strcpy((char *)gl_Send8583.Field25, "00");


		    //Field35,Field36,Field38
		    
		//磁道加密	
		      if(szTrack2[0]){
			  if(gl_SysInfo.magEncrypt == 1) 	
			  {
				AjustTrack2Data(szTrack2);	
				EncryptMagData(szTrack2, strlen(szTrack2), gl_Send8583.Field35);
		      	 }	
			 else if(gl_SysInfo.magEncrypt ==0) 
			 	strcpy(gl_Send8583.Field35,szTrack2);
		    }
         	  
	
		    //消费撤销/预授权完成撤销/退货,37域上送原交易参考号
		    if(uiTransType==TRANS_TYPE_SALEVOID || uiTransType==TRANS_TYPE_PREAUTH_COMPVOID
				|| uiTransType==TRANS_TYPE_REFUND)
		    {
		        memcpy(gl_Send8583.Field37, gl_TransRec.sReferenceNo, 12);
		    }
		    
		    //预授权完成或撤销类交易,需上送原交易授权码
		    if((gl_Send8583.Field03[0]=='2' || uiTransType==TRANS_TYPE_PREAUTH_COMP) 
		        && gl_TransRec.sAuthCode[0])
		    {
		        memcpy(gl_Send8583.Field38, gl_TransRec.sAuthCode, 6);
		    }
		    gl_TransRec.uiTransType=uiTransType;
		    gl_TransRec.sReferenceNo[0]=0;
		    gl_TransRec.sAuthCode[0]=0;
		    gl_TransRec.ulVoidTTC=ulVoidTTC;

		    
		    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
		    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
		    strcpy((char *)gl_Send8583.Field49, "156");

		    if(szEncPin[0])
		    {
				strcpy((char*)gl_Send8583.Field26, "12");
			
		        gl_Send8583.Field22[2]='1';
			if(gl_SysInfo.ucSmFlag == 0)	
		        {
		        	memcpy(gl_Send8583.Field52, "\x00\x08", 2);
		        	vTwoOne(szEncPin, 16, gl_Send8583.Field52+2);
			}else if(gl_SysInfo.ucSmFlag == 1)
			{
				memcpy(gl_Send8583.Field52, "\x00\x08", 2);
				memset(gl_Send8583.Field52+2, 0x00, 8);         //国密算法,pin密文放在59域,52域填全0
			}   

		        memset(gl_Send8583.Field53, '0', 16);
     			if(gl_SysInfo.ucSmFlag == 0)		
        			memcpy(gl_Send8583.Field53, "26", 2);			//双倍长密钥
        		else if(gl_SysInfo.ucSmFlag == 1)
			{			        
				memcpy(gl_Send8583.Field53, "23", 2);			//双倍长密钥
				//DE59 21号文(消费撤销)    
			    if( gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID 
					|| gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
			    { 
			        GetPublicField059((char*)szEncPin,szCardId+strlen(szCardId)-6, gl_Send8583.Field59);
			    }                      
        		}	
		        if(gl_SysInfo.magEncrypt == 1)	
        			memcpy(&gl_Send8583.Field53[2], "1", 1);		//磁道加密
       			else if(gl_SysInfo.magEncrypt == 0)	
				memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密
		    }
			else
		    {
		        gl_Send8583.Field22[2]='2';

			memset(gl_Send8583.Field53, '0', 16);
			if(gl_SysInfo.ucSmFlag == 0)	
				memcpy(gl_Send8583.Field53, "06", 2);			//双倍长密钥
			else if(gl_SysInfo.ucSmFlag == 1)
				memcpy(gl_Send8583.Field53, "03", 2);			//双倍长密钥	
			if(gl_SysInfo.magEncrypt == 1)	
				memcpy(&gl_Send8583.Field53[2], "1", 1);		//磁道加密
			else if(gl_SysInfo.magEncrypt == 0)	
				memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密
		    }

		//DE58  基站信息
		if( gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
		{
			memset(asBuf,0,sizeof(asBuf));
			GetPublicField058(asBuf);

			vLongToStr(strlen(asBuf), 2, gl_Send8583.Field58);
			memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf));
			dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf));
		}
	
		//	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s%.2s", ucFd60MsgCode, gl_SysData.ulBatchNo, "000", szRid);

		   sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s%.3s", ucFd60MsgCode, gl_SysData.ulBatchNo, "000","601");
		    if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID)
		    {
		        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
		    }
		    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP
				|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
		    {
		        //sprintf((char *)gl_Send8583.Field61, "%06lu%06lu0000", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
		        vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
		        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", 0L, 0L, szBuf);
		    }
		    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
		    {
				vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
		        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC, szBuf);
		    }

		    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

		    //打8583包-填充http-发送-接收-解http-解8583包
		    if(uiTransType==TRANS_TYPE_BALANCE ||uiTransType==TRANS_TYPE_REFUND)
		        iRet = iHttpSendRecv8583(0);
		    else
		    {
		        iRet = iHttpSendRecv8583(1);
		    }
		    if (iRet)
		    {
	        	//显示错误信息
	        	return iRet;	 
		    }   
		    if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
		    {
		        //显示服务端错误信息		
		        vShowHostErrMsg(gl_Recv8583.Field39, NULL);
		        return -1;
		    }

		    //删冲正
		    uiMemManaErase8583(MSG8583_TYPE_REV);
		    
		    gl_TransRec.uiTransType=uiTransType;
		    gl_TransRec.uiEntryMode=atoi(gl_Send8583.Field22);
		    if(gl_Recv8583.Field13[0]&&gl_Recv8583.Field12[0])  //用后台返回时间做交易记录时间
		    {
		        memcpy(szDateTime+4, gl_Recv8583.Field13, 4);
		        memcpy(szDateTime+8, gl_Recv8583.Field12, 6);
		    }
		    vTwoOne(szDateTime+2, 12, gl_TransRec.sDateTime);
		    
		    if(gl_Recv8583.Field15[0])
		        vTwoOne(gl_Recv8583.Field15, 4, gl_TransRec.sSettleDate);
		    else
		        memcpy(gl_TransRec.sSettleDate, "\x00\x00", 2);
		    
		    vPackPan(szCardId, gl_TransRec.sPan);
		    gl_TransRec.ucPanSerNo=0xFF;
		    gl_TransRec.ulAmount=ulAmount;
		    gl_TransRec.ucUploadFlag=0xFF;
		    gl_TransRec.ucVoidFlag=0;
		    gl_TransRec.ulTTC=atol(gl_Recv8583.Field11);
		    gl_TransRec.ulVoidTTC=ulVoidTTC;
		    if(gl_Recv8583.Field27[0]) {	
		   	 if(gl_Recv8583.Field27[0] ==  '0')  gl_TransRec.ucCardClass = 0;
		    	if(gl_Recv8583.Field27[0] ==  '1')  gl_TransRec.ucCardClass = 1;	
		   	 dbg("gl_TransRec.ucCardClass:%d\n",gl_TransRec.ucCardClass );
		    }
		    memcpy(gl_TransRec.sReferenceNo, gl_Recv8583.Field37, 12);	// 交易参考号, 联机交易存在
		    memcpy(gl_TransRec.sAuthCode, gl_Recv8583.Field38, 6);      // 授权码

		    vTwoOne(gl_SysData.szCurOper, 2, gl_TransRec.sOperNo);

		    if(gl_Recv8583.Field43[0])
		    {
		    	vGetPrtMerchInfo(gl_Recv8583.Field43, &gl_TransRec);
		    }	

		    //磁条卡交易时,将磁道信息保存至sIcData中
		    if(gl_Send8583.Field35[0])
		    {
		        strcpy(gl_TransRec.sIcData, gl_Send8583.Field35);
		    }
		    if(gl_Send8583.Field36[0])
		    {
		        strcpy(gl_TransRec.sIcData+50, gl_Send8583.Field36);
		    }

		     //发卡行收单行
		     if(gl_Recv8583.BitMap[5] & 0x10) {
				memcpy(gl_TransRec.issuerBankId,gl_Recv8583.Field44,8);
				dbgHex("gl_TransRec.issuerBankId", gl_TransRec.issuerBankId, 8);
				gl_TransRec.issuerBankId[8]=0;
				if (strlen(gl_Recv8583.Field44) > 11)
				{
					memcpy(gl_TransRec.recvBankId,gl_Recv8583.Field44+11,8);
					gl_TransRec.recvBankId[8]=0;
					dbgHex("gl_TransRec.recvBankId", gl_TransRec.recvBankId, 8);
				}
			}	
				
		     //预设需签字+需纸签,电签成功后修改为无需纸签
		    gl_TransRec.ucTransAttr=(0x02|0x04);
			 
		    uiMemManaPutTransRec(gl_SysData.uiTransNum++, &gl_TransRec);
		    gl_SysData.ulLastTransTTC = gl_TransRec.ulTTC;
		    uiMemManaPutSysData();

		    if(ulVoidTTC>0 && iIndex>=0)
		    {
		        //原交易做取消标志
		        stTransRec voidTransRec;
		        uiMemManaGetTransRec(iIndex, &voidTransRec);
		        voidTransRec.ucVoidFlag=1;
		        uiMemManaPutTransRec(iIndex, &voidTransRec);
		    }

		    //vMessage("交易成功");

#if 1
	    //签字
	    if(gl_SysInfo.ucSupportESign == 1)
	    	iBillSign(1, -1, NULL);
#endif

		    _vCls();
		    vDispCenter(1, szTitle, 1);    

#ifdef ENABLE_PRINTER 
		    {			
		        //打印票据
		        vShowWaitEx("交易成功,打印票据...", -1, 0);
		        iPrintTrans(&gl_TransRec, 0, gl_Recv8583.Field63+3);
		        vShowWaitEx(NULL, 0, 0x03);
				 vTakeOutCard();
		    }
#else    
		    {
		        vMessage("交易成功");
		    }
#endif  
			
    }else
    {	
    dbg("not need card\n");
    _vGetTime(szDateTime);
	vPrtTransRecInfo();

    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
    
    switch(uiTransType)
    {			
        case TRANS_TYPE_SALEVOID:   //消费撤销
            //流程:主管-凭证号-原交易-卡(参数)-密码(参数)
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=23;
            ucNeedPin=gl_SysInfo.ucSaleVoidPinFlag;
            break;
        case TRANS_TYPE_PREAUTH_COMPVOID:    //预授权完成撤销
            //流程:主管-凭证号-原交易-卡(参数)-(不需密码)
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=21;
            ucPreAuthType=1;
            ucNeedPin=gl_SysInfo.ucAuthCompVoidPinFlag;            
            break;
    }

     vUnpackPan(gl_TransRec.sPan, szCardId);   
     ulAmount=gl_TransRec.ulAmount;
    if(ucNeedPin)
    {
        
        iRet=iInputPinAndEnc(ucByPass, ulAmount, "请持卡人输入密码:", (char*)szCardId, szEncPin);
        if(iRet<0)
            return iRet;
    }else
        szEncPin[0]=0;

    strcpy((char*)gl_Send8583.Field02, szCardId);

    if(uiTransType!=TRANS_TYPE_BALANCE)
        sprintf((char *)gl_Send8583.Field04, "%012lu", gl_TransRec.ulAmount);

    vIncTTC();	
    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

    sprintf((char *)gl_Send8583.Field22, "%03d", 10);

     vOneTwo(gl_TransRec.sCardExpire, 2, (char *)gl_Send8583.Field14);
	
    if(ucPreAuthType)
        strcpy((char *)gl_Send8583.Field25, "06");
    else
        strcpy((char *)gl_Send8583.Field25, "00");


    //Field35,Field36,Field38
	
//磁道加密	
  
    //消费撤销/预授权完成撤销/退货,37域上送原交易参考号
    if(uiTransType==TRANS_TYPE_SALEVOID || uiTransType==TRANS_TYPE_PREAUTH_COMPVOID
		|| uiTransType==TRANS_TYPE_REFUND)
    {
        memcpy(gl_Send8583.Field37, gl_TransRec.sReferenceNo, 12);
    }
    
    //预授权完成或撤销类交易,需上送原交易授权码
    if((gl_Send8583.Field03[0]=='2' || uiTransType==TRANS_TYPE_PREAUTH_COMP) 
        && gl_TransRec.sAuthCode[0])
    {
        memcpy(gl_Send8583.Field38, gl_TransRec.sAuthCode, 6);
    }
    gl_TransRec.uiTransType=uiTransType;
    gl_TransRec.sReferenceNo[0]=0;
    gl_TransRec.sAuthCode[0]=0;
    gl_TransRec.ulVoidTTC=ulVoidTTC;

    
    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
    strcpy((char *)gl_Send8583.Field49, "156");

    if(szEncPin[0])
    {
		strcpy((char*)gl_Send8583.Field26, "12");
	
        gl_Send8583.Field22[2]='1';
        memcpy(gl_Send8583.Field52, "\x00\x08", 2);
        vTwoOne(szEncPin, 16, gl_Send8583.Field52+2);

        memset(gl_Send8583.Field53, '0', 16);
	if(gl_SysInfo.ucSmFlag == 0)		
		memcpy(gl_Send8583.Field53, "26", 2);			//双倍长密钥
	else if(gl_SysInfo.ucSmFlag == 1)
	{			        
		memcpy(gl_Send8583.Field53, "23", 2);			//双倍长密钥
		//DE59 21号文(消费撤销)    
	    if( gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID 
			|| gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
	    { 
	        GetPublicField059((char*)szEncPin,szCardId+strlen(szCardId)-6, gl_Send8583.Field59);
	    }                      
	}
        memcpy(&gl_Send8583.Field53[2], "0", 1);		//无磁道
    }
	else
    {
        gl_Send8583.Field22[2]='2';

	
	memset(gl_Send8583.Field53, '0', 16);
	if(gl_SysInfo.ucSmFlag == 0)	
        	memcpy(gl_Send8583.Field53, "06", 2);			//双倍长密钥
       else if(gl_SysInfo.ucSmFlag == 1)
		memcpy(gl_Send8583.Field53, "03", 2);			//双倍长密钥
	memcpy(&gl_Send8583.Field53[2], "0", 1);		//无磁道		
    }

	//DE58  基站信息
	if( gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
	{
		memset(asBuf,0,sizeof(asBuf));
		GetPublicField058(asBuf);

		vLongToStr(strlen(asBuf)+3, 2, gl_Send8583.Field58);
		memcpy(gl_Send8583.Field58+2, asBuf,strlen(asBuf)+3);
		dbgHex("gl_Send8583.Field58",gl_Send8583.Field58,strlen(asBuf)+3);
	}
	
//	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s%.2s", ucFd60MsgCode, gl_SysData.ulBatchNo, "000", szRid);

//    sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s", ucFd60MsgCode, gl_SysData.ulBatchNo, "000");
    sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s%.3s", ucFd60MsgCode, gl_SysData.ulBatchNo, "000","601");
    if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID)
    {
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
    }
    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP
		|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
    {
        //sprintf((char *)gl_Send8583.Field61, "%06lu%06lu0000", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
        vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", 0L, 0L, szBuf);
    }
    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
    {
		vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC, szBuf);
    }

    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

    //打8583包-填充http-发送-接收-解http-解8583包
    if(uiTransType==TRANS_TYPE_BALANCE ||uiTransType==TRANS_TYPE_REFUND)
        iRet = iHttpSendRecv8583(0);
    else
    {
        iRet = iHttpSendRecv8583(1);
    }
    if (iRet)
    {
    	//显示错误信息
    	return iRet;	 
    }   
    if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
    {
        //显示服务端错误信息		
        vShowHostErrMsg(gl_Recv8583.Field39, NULL);
        return -1;
    }

    //删冲正
    uiMemManaErase8583(MSG8583_TYPE_REV);
    
    gl_TransRec.uiTransType=uiTransType;
    gl_TransRec.uiEntryMode=atoi(gl_Send8583.Field22);
    if(gl_Recv8583.Field13[0]&&gl_Recv8583.Field12[0])  //用后台返回时间做交易记录时间
    {
        memcpy(szDateTime+4, gl_Recv8583.Field13, 4);
        memcpy(szDateTime+8, gl_Recv8583.Field12, 6);
    }
    vTwoOne(szDateTime+2, 12, gl_TransRec.sDateTime);
    
    if(gl_Recv8583.Field15[0])
        vTwoOne(gl_Recv8583.Field15, 4, gl_TransRec.sSettleDate);
    else
        memcpy(gl_TransRec.sSettleDate, "\x00\x00", 2);
    
    vPackPan(szCardId, gl_TransRec.sPan);
    gl_TransRec.ucPanSerNo=0xFF;
    gl_TransRec.ulAmount=ulAmount;
    gl_TransRec.ucUploadFlag=0xFF;
    gl_TransRec.ucVoidFlag=0;
    gl_TransRec.ulTTC=atol(gl_Recv8583.Field11);
    gl_TransRec.ulVoidTTC=ulVoidTTC;
    if(gl_Recv8583.Field27[0]) {	
   	 if(gl_Recv8583.Field27[0] ==  '0')  gl_TransRec.ucCardClass = 0;
    	if(gl_Recv8583.Field27[0] ==  '1')  gl_TransRec.ucCardClass = 1;	
   	 dbg("gl_TransRec.ucCardClass:%d\n",gl_TransRec.ucCardClass );
    }
    memcpy(gl_TransRec.sReferenceNo, gl_Recv8583.Field37, 12);	// 交易参考号, 联机交易存在
    memcpy(gl_TransRec.sAuthCode, gl_Recv8583.Field38, 6);      // 授权码

    vTwoOne(gl_SysData.szCurOper, 2, gl_TransRec.sOperNo);

    if(gl_Recv8583.Field43[0])
    {
    	vGetPrtMerchInfo(gl_Recv8583.Field43, &gl_TransRec);
    }	

    //磁条卡交易时,将磁道信息保存至sIcData中
    if(gl_Send8583.Field35[0])
    {
        strcpy(gl_TransRec.sIcData, gl_Send8583.Field35);
    }
    if(gl_Send8583.Field36[0])
    {
        strcpy(gl_TransRec.sIcData+50, gl_Send8583.Field36);
    }

     //发卡行收单行
     if(gl_Recv8583.BitMap[5] & 0x10) {
		memcpy(gl_TransRec.issuerBankId,gl_Recv8583.Field44,8);
		dbgHex("gl_TransRec.issuerBankId", gl_TransRec.issuerBankId, 8);
		gl_TransRec.issuerBankId[8]=0;
		if (strlen(gl_Recv8583.Field44) > 11)
		{
			memcpy(gl_TransRec.recvBankId,gl_Recv8583.Field44+11,8);
			gl_TransRec.recvBankId[8]=0;
			dbgHex("gl_TransRec.recvBankId", gl_TransRec.recvBankId, 8);
		}
	}	
		
     //预设需签字+需纸签,电签成功后修改为无需纸签
    gl_TransRec.ucTransAttr=(0x02|0x04);
	 
    uiMemManaPutTransRec(gl_SysData.uiTransNum++, &gl_TransRec);
    gl_SysData.ulLastTransTTC = gl_TransRec.ulTTC;
    uiMemManaPutSysData();

    if(ulVoidTTC>0 && iIndex>=0)
    {
        //原交易做取消标志
        stTransRec voidTransRec;
        uiMemManaGetTransRec(iIndex, &voidTransRec);
        voidTransRec.ucVoidFlag=1;
        uiMemManaPutTransRec(iIndex, &voidTransRec);
    }

    //vMessage("交易成功");
    _vCls();
    vDispCenter(1, szTitle, 1);    

#ifdef ENABLE_PRINTER 
    {	
        //打印票据
        vShowWaitEx("交易成功,打印票据...", -1, 0);
        iPrintTrans(&gl_TransRec, 0, gl_Recv8583.Field63+3);
        vShowWaitEx(NULL, 0, 0x03);
    }
#else    
    {
        vMessage("交易成功");
    }
#endif    
    }
    return 0;
}
/*
int iBillSign(void)
{
    extern int iSignDraw(uchar *pszFileName, uchar *pszSpecCode);
    extern int iGetSignMsgFd55(uchar *psOutData);

    uchar szFileName[30];
    uchar szCode[20], buf[10];
    int i, ret;
    
#if 0//PROJECT_CY21  == 1     
    {
        if(gl_TransRec.ucTransAttr&0x02)
        {
            gl_TransRec.ucTransAttr&=(0xFF-0x02);
            uiMemManaPutTransRec(gl_SysData.uiTransNum-1, &gl_TransRec);
            dbg("gl_TransRec.ucTransAttr:%02X", gl_TransRec.ucTransAttr);
        }
        return 0;
    }
#endif    
    
    ret=(gl_TransRec.uiEntryMode%100)/10;
    if(ret!=2 && ret!=5)
        _uiCloseCard(8);
    
    //if((gl_TransRec.ucTransAttr&0x20)==0)   //不需要签字
    //    return 0;
    //允许免签+额度+交易类型
    if(gl_SysInfo.ucNoSignFlag && gl_TransRec.ulAmount<=gl_SysInfo.ulNoSignLimit && gl_TransRec.ucUploadFlag==0xFF && gl_TransRec.uiEntryMode/10==7)
    {
        if(gl_TransRec.ucTransAttr&0x02)
        {
            gl_TransRec.ucTransAttr&=(0xFF-0x02);
            gl_TransRec.ucSignupload=0xFF;      //无需上送签名
            uiMemManaPutTransRec(gl_SysData.uiTransNum-1, &gl_TransRec);
        }
        return 0;
    }

    vOneTwo0(gl_TransRec.sDateTime+1, 2, szFileName);
    sprintf((char*)szFileName+4, "_%06lu.jbg", gl_TransRec.ulTTC);

    if(gl_TransRec.ucUploadFlag==0xFF)
    {
        //联机交易: 15域清算日期[4]+37域参考号[12]
        memset(szCode, '0', 16);
        szCode[16]=0;
        if(gl_Recv8583.Field15[0])
            memcpy(szCode, gl_Recv8583.Field15, 4);
        if(gl_TransRec.sReferenceNo[0])
            memcpy(szCode+4, gl_TransRec.sReferenceNo, 12);
    }else{
        //脱机交易：批次号[6]+ttc[6]+0000
        sprintf(szCode, "%06lu%06lu0000", gl_SysData.ulBatchNo, gl_TransRec.ulTTC);
    }
    //前后8字节,压缩BCD,异或,再转16进制字符串
    vTwoOne(szCode, 16, buf);
    for(i=0; i<4; i++)
        buf[i]^=buf[4+i];
    vOneTwo0(buf, 4, szCode);
        
    ret=iSignDraw(szFileName, szCode);
    if(ret)
    {
        gl_TransRec.ucTransAttr|=0x02;
        gl_TransRec.ucTransAttr|=0x04;  //b3置位,需要纸签
        uiMemManaPutTransRec(gl_SysData.uiTransNum-1, &gl_TransRec);
    }else
    {
        gl_TransRec.ucTransAttr|=0x02;
        uiMemManaPutTransRec(gl_SysData.uiTransNum-1, &gl_TransRec);
        
        //上送
        iUploadSignFile(1, -1);
    }

    return ret;
}
*/
//根据交易不同,其它可能需要的参数在gl_TransRec中(金额,参考号,授权号,日期)
int iMagTrans(uint uiTransType, int iEntry, int iVoidRecIdx, ulong ulVoidTTC)
{
    int iRet;
    char szTitle[20+1];
    
	uchar szCardId[20+1];
    uchar szBuf[100];
    uchar szDateTime[14+1];
    char *p;
	ulong ulAmount;
	char szRid[2+1];
    uchar ucFd60MsgCode;
    uchar ucPreAuthType=0;  //预授权类交易
    uchar ucNeedPin=1;
    uchar ucNeedCard=1;     
    uchar szEncPin[32+1];
    uchar ucByPass=gl_SysInfo.ucByPass;
	uchar asTrack2Data[Data0_35Len+1]={0},asTrack3Data[Data0_36Len+1]={0};
    st8583			Send8583;							// 发送8583结构
	
    _vGetTime(szDateTime);
	vPrtTransRecInfo();

    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
    memset(&Send8583,0,sizeof(gl_Send8583));
		
    switch(uiTransType)
    {
        case TRANS_TYPE_SALE:       //消费
            //流程:卡-金额-密码
            strcpy(szTitle, "消费");
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "000000");
            ucFd60MsgCode=22;
            ucNeedPin=1;
            break;
        case TRANS_TYPE_DAIRY_SALE:       //日结消费
            //流程:卡-金额-密码
            strcpy(szTitle, "日结消费");
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "280000");
	    gl_TransRec.ucSaleType = 1;
            ucFd60MsgCode=22;
            ucNeedPin=1;
            break;
			
	case TRANS_TYPE_NFCPINSALE:
            strcpy(szTitle, "闪付凭密");
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "000000");
            ucFd60MsgCode=22;
            ucNeedPin=1;			
	    break;
			
        case TRANS_TYPE_SALEVOID:   //消费撤销
            //流程:主管-凭证号-原交易-卡(参数)-密码(参数)
            strcpy(szTitle, "消费撤销");
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=23;
            ucNeedPin=gl_SysInfo.ucSaleVoidPinFlag;
            break;
        case TRANS_TYPE_REFUND:     //退货
            //流程:主管-卡-参考号-日期-金额-(不需密码)
            //无冲正
            strcpy(szTitle, "退货");
            strcpy((char *)gl_Send8583.Msg01, "0220");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=25;
            ucNeedPin=0;       
            break;
        case TRANS_TYPE_BALANCE:    //查余额
            //流程:卡-密码
            //无冲正
            strcpy(szTitle, "查余额");
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "310000");
            ucFd60MsgCode=0x01;
            ucNeedPin=1;
            break;
        case TRANS_TYPE_PREAUTH:    //预授权
            //流程:卡-金额-密码
            strcpy(szTitle, "预授权");
            strcpy((char *)gl_Send8583.Msg01, "0100");
            strcpy((char *)gl_Send8583.Field03, "030000");
            ucFd60MsgCode=10;
            ucPreAuthType=1;
            ucNeedPin=1;
            break;
        case TRANS_TYPE_PREAUTHVOID:    //预授权撤销
            //流程:主管-卡-日期-授权码-金额-密码(参数)
            strcpy(szTitle, "预授权撤销");
            strcpy((char *)gl_Send8583.Msg01, "0100");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=11;
            ucPreAuthType=1; 
            ucNeedPin=gl_SysInfo.ucAuthVoidPinFlag;   
            break;
        case TRANS_TYPE_PREAUTH_COMP:    //预授权完成(请求)
            //流程:卡-日期-授权码-金额-密码(参数)
            strcpy(szTitle, "预授权完成");
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "000000");
            ucFd60MsgCode=20;
            ucPreAuthType=1;
            ucNeedPin=gl_SysInfo.ucAuthCompPinFlag;     
            break;
        case TRANS_TYPE_PREAUTH_COMPVOID:    //预授权完成撤销
            //流程:主管-凭证号-原交易-卡(参数)-(不需密码)
            strcpy(szTitle, "预授权完成撤销");
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=21;
            ucPreAuthType=1;
            ucNeedPin=gl_SysInfo.ucAuthCompVoidPinFlag;            
            break;
    }
    _vCls();
    vDispCenter(1, szTitle, 1);

    if(iEntry==3 || iEntry==4)
        ucNeedCard=1;
    
    if(ucNeedCard)
    {
        szBuf[0]=0;
        _uiMagGet(2, szBuf);
        p=strchr(szBuf, '=');
        if(p==NULL)
            p=strchr(szBuf, 'D');
        if(p==NULL)
        {
            vMessage("刷卡错");
            return -1;
        }
        *p=0;
        strcpy(szCardId, szBuf);
    }else
    {
        vUnpackPan(gl_TransRec.sPan, szCardId);
    }
    if(uiTransType==TRANS_TYPE_BALANCE)
        ulAmount=0;
    else
        ulAmount=gl_TransRec.ulAmount;
    if(ucNeedPin)
    {
        
        iRet=iInputPinAndEnc(ucByPass, ulAmount, "请持卡人输入密码:", (char*)szCardId, szEncPin);
        if(iRet<0)
            return iRet;
    }else
        szEncPin[0]=0;

    strcpy((char*)gl_Send8583.Field02, szCardId);

    if(uiTransType!=TRANS_TYPE_BALANCE)
        sprintf((char *)gl_Send8583.Field04, "%012lu", gl_TransRec.ulAmount);

    vIncTTC();	
    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);

    sprintf((char *)gl_Send8583.Field22, "%03d", 20);

    strcpy((char *)gl_Send8583.Field24, "004");	 

    strcpy((char *)gl_Send8583.Field25, "14");

    //Field35,Field36,Field38
    if(ucNeedCard)
    {
        _uiMagGet(2, asTrack2Data);
        _uiMagGet(3, asTrack3Data);
    }
	else
    {
        if(gl_TransRec.sIcData[0])
            strcpy(asTrack2Data, gl_TransRec.sIcData);
        if(gl_TransRec.sIcData[50])
            strcpy(asTrack3Data, gl_TransRec.sIcData+50);
    }
	AjustTrack2Data(asTrack2Data);
	GetExpiredData(asTrack2Data,(char *)gl_Send8583.Field14);


         
	if(gl_SysInfo.magEncrypt == 1)
	{
//磁道加密	
	EncryptMagData(asTrack2Data, strlen(asTrack2Data), gl_Send8583.Field35);
	EncryptMagData(asTrack3Data, strlen(asTrack3Data), gl_Send8583.Field36);
	}
	else if(gl_SysInfo.magEncrypt ==0)
	{
       		 _uiMagGet(2, gl_Send8583.Field35);
        	_uiMagGet(3, gl_Send8583.Field36);
	}
  
    //消费撤销/预授权完成撤销/退货,37域上送原交易参考号
    if(uiTransType==TRANS_TYPE_SALEVOID || uiTransType==TRANS_TYPE_PREAUTH_COMPVOID
		|| uiTransType==TRANS_TYPE_REFUND)
    {
        memcpy(gl_Send8583.Field37, gl_TransRec.sReferenceNo, 12);
    }
    
    //预授权完成或撤销类交易,需上送原交易授权码
    if((gl_Send8583.Field03[0]=='2' || uiTransType==TRANS_TYPE_PREAUTH_COMP) 
        && gl_TransRec.sAuthCode[0])
    {
        memcpy(gl_Send8583.Field38, gl_TransRec.sAuthCode, 6);
    }
    gl_TransRec.uiTransType=uiTransType;
    gl_TransRec.sReferenceNo[0]=0;
    gl_TransRec.sAuthCode[0]=0;
    gl_TransRec.ulVoidTTC=ulVoidTTC;

    
    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
    strcpy((char *)gl_Send8583.Field49, "156");

    if(szEncPin[0])
    {
//		strcpy((char*)gl_Send8583.Field26, "12");
	
        gl_Send8583.Field22[2]='1';
	if(gl_SysInfo.ucSmFlag == 0)	
        {
        	memcpy(gl_Send8583.Field52, "\x00\x08", 2);
        	vTwoOne(szEncPin, 16, gl_Send8583.Field52+2);
	}else if(gl_SysInfo.ucSmFlag == 1)
	{
		memcpy(gl_Send8583.Field52, "\x00\x08", 2);
		memset(gl_Send8583.Field52+2, 0x00, 8);         //国密算法,pin密文放在59域,52域填全0
	}
	
	memset(gl_Send8583.Field53, '0', 16);
        if(gl_SysInfo.ucSmFlag == 0)		
        	memcpy(gl_Send8583.Field53, "26", 2);			//双倍长密钥
        else if(gl_SysInfo.ucSmFlag == 1)
	{
		memcpy(gl_Send8583.Field53, "24", 2);			//双倍长密钥	
        }	
	if(gl_SysInfo.magEncrypt == 1)	
        	memcpy(&gl_Send8583.Field53[2], "1", 1);		//磁道加密
        else if(gl_SysInfo.magEncrypt == 0)	
		memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密	
		
    }
	else
    {
        gl_Send8583.Field22[2]='2';	

	memset(gl_Send8583.Field53, '0', 16);
	if(gl_SysInfo.ucSmFlag == 0)	
        	memcpy(gl_Send8583.Field53, "06", 2);			//双倍长密钥
        else if(gl_SysInfo.ucSmFlag == 1)
		memcpy(gl_Send8583.Field53, "04", 2);			//双倍长密钥	
       	if(gl_SysInfo.magEncrypt == 1)	
        	memcpy(&gl_Send8583.Field53[2], "1", 1);		//磁道加密
        else if(gl_SysInfo.magEncrypt == 0)	
		memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密
    }

    if(iEntry==4)
        strcpy(szRid, "52");
    else
        strcpy(szRid, "50");

//DE59 21号文(消费，预授权)    
    if( gl_TransRec.uiTransType==TRANS_TYPE_SALE || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH || gl_TransRec.uiTransType==TRANS_TYPE_BALANCE)
    { 
        GetPublicField059((char*)szEncPin,szCardId+strlen(szCardId)-6, gl_Send8583.Field59);
    }

    if(gl_SysInfo.ucSmFlag == 0)
   	 	sprintf((char *)gl_Send8583.Field61, "%06lu%03s%06lu", gl_SysData.ulBatchNo, gl_SysData.szCurOper , gl_SysData.ulTTC);
     else 
	 	sprintf((char *)gl_Send8583.Field61, "%06lu%03s%06lu", gl_SysData.ulBatchNo, "SM4" , gl_SysData.ulTTC);
	 
    if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID)
    {
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
    }
    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP
		|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
    {
        //sprintf((char *)gl_Send8583.Field61, "%06lu%06lu0000", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
        vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", 0L, 0L, szBuf);
    }
    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
    {
		vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC, szBuf);
    }

    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

if(gl_TransRec.uiTransType !=TRANS_TYPE_BALANCE)	
 {
 	Send8583 = gl_Send8583;

	    gl_TransRec.ucUploadFlag=0xFF;
	    gl_TransRec.ucSignupload=0x00;	
#if 1
	   if(gl_SysInfo.ucSupportESign == 1)
	   {
	   	vTwoOne(szDateTime+2, 12, gl_TransRec.sDateTime);
	        gl_TransRec.ulTTC=gl_SysData.ulTTC;
		vPackPan(szCardId, gl_TransRec.sPan);
	   	iBillPreSign(1, &gl_TransRec);
	   }	
#endif

	    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
	    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
	   gl_Send8583 = Send8583;
}

    _vCls();
    vDispCenter(1, szTitle, 1);
	
    //打8583包-填充http-发送-接收-解http-解8583包
    if(uiTransType==TRANS_TYPE_BALANCE ||uiTransType==TRANS_TYPE_REFUND)
        iRet = iHttpSendRecv8583(0);
    else
    {
        iRet = iHttpSendRecv8583(0);
    }
    if (iRet)
    {
    	//显示错误信息
    	return iRet;	 
    }   
    if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
    {
        //显示服务端错误信息		
        vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);
        return -1;
    }

    if(TRANS_TYPE_BALANCE==gl_TransRec.uiTransType)
	{
		if(strlen(gl_Recv8583.Field54)>=20)
		{
			ulong ulBal;

			ulBal=ulA2L(gl_Recv8583.Field54+8, 12);

			//_vCls();
			//vDispCenter(1, "余额查询", 0);
            		vClearLines(2);			
			vDispVarArg(3, "余额:%s%lu.%02lu", (gl_Recv8583.Field54[7]=='C')?"":"-", ulBal/100, ulBal%100);
			iGetKeyWithTimeout(3);
		}
        return 0;
	}

    //磁条卡交易删冲正
    uiMemManaErase8583(MSG8583_TYPE_REV);
    
    gl_TransRec.uiTransType=uiTransType;
    gl_TransRec.uiEntryMode=atoi(gl_Send8583.Field22);
    if(gl_Recv8583.Field13[0]&&gl_Recv8583.Field12[0])  //用后台返回时间做交易记录时间
    {
        memcpy(szDateTime+4, gl_Recv8583.Field13, 4);
        memcpy(szDateTime+8, gl_Recv8583.Field12, 6);
    }
    vTwoOne(szDateTime+2, 12, gl_TransRec.sDateTime);
    
    if(gl_Recv8583.Field15[0])
        vTwoOne(gl_Recv8583.Field15, 4, gl_TransRec.sSettleDate);
    else
        memcpy(gl_TransRec.sSettleDate, "\x00\x00", 2);

    if(gl_Recv8583.Field14[0] && memcmp(gl_Recv8583.Field14, "0000", 4)!=0)		
   		 vTwoOne(gl_Recv8583.Field14, 4, gl_TransRec.sCardExpire);
	else
	{
      	  	vTwoOne(gl_Send8583.Field14, 4, gl_TransRec.sCardExpire);
	}
	
    vPackPan(szCardId, gl_TransRec.sPan);
    gl_TransRec.ucPanSerNo=0xFF;
    gl_TransRec.ulAmount=ulAmount;
    gl_TransRec.ucUploadFlag=0xFF;
    gl_TransRec.ucVoidFlag=0;
    gl_TransRec.ulTTC=atol(gl_Recv8583.Field11);
    gl_TransRec.ulVoidTTC=ulVoidTTC;
    if(gl_Recv8583.Field27[0]) {	
   	 if(gl_Recv8583.Field27[0] ==  '0')  gl_TransRec.ucCardClass = 0;
    	if(gl_Recv8583.Field27[0] ==  '1')  gl_TransRec.ucCardClass = 1;	
   	 dbg("gl_TransRec.ucCardClass:%d\n",gl_TransRec.ucCardClass );
    }
    memcpy(gl_TransRec.sReferenceNo, gl_Recv8583.Field37, 12);	// 交易参考号, 联机交易存在
    memcpy(gl_TransRec.sAuthCode, gl_Recv8583.Field38, 6);      // 授权码

    vTwoOne(gl_SysData.szCurOper, 2, gl_TransRec.sOperNo);

//    if(gl_Recv8583.Field43[0])
//    {
//    	vGetPrtMerchInfo(gl_Recv8583.Field43, &gl_TransRec);
//    }		

    //磁条卡交易时,将磁道信息保存至sIcData中
    if(gl_Send8583.Field35[0])
    {
        strcpy(gl_TransRec.sIcData, gl_Send8583.Field35);
    }
    if(gl_Send8583.Field36[0])
    {
        strcpy(gl_TransRec.sIcData+50, gl_Send8583.Field36);
    }

     //发卡行收单行
     if(gl_Recv8583.BitMap[5] & 0x10) {
		memcpy(gl_TransRec.issuerBankId,gl_Recv8583.Field44,8);
		dbgHex("gl_TransRec.issuerBankId", gl_TransRec.issuerBankId, 8);
		gl_TransRec.issuerBankId[8]=0;
		if (strlen(gl_Recv8583.Field44) > 11)
		{
			memcpy(gl_TransRec.recvBankId,gl_Recv8583.Field44+11,8);
			gl_TransRec.recvBankId[8]=0;
			dbgHex("gl_TransRec.recvBankId", gl_TransRec.recvBankId, 8);
		}
	}	
		
     //预设需签字+需纸签,电签成功后修改为无需纸签
    gl_TransRec.ucTransAttr=(0x02|0x04);

    uiMemManaPutTransRec(gl_SysData.uiTransNum++, &gl_TransRec);
    gl_SysData.ulLastTransTTC = gl_TransRec.ulTTC;
    uiMemManaPutSysData();

    if(ulVoidTTC>0 && iVoidRecIdx>=0)
    {
        //原交易做取消标志
        stTransRec voidTransRec;
        uiMemManaGetTransRec(iVoidRecIdx, &voidTransRec);
        voidTransRec.ucVoidFlag=1;
        uiMemManaPutTransRec(iVoidRecIdx, &voidTransRec);
    }

    //vMessage("交易成功");
#if 0
    //签字
     if(gl_SysInfo.ucSupportESign == 1)
    	iBillSign(1, -1, NULL);
#endif

    _vCls();
    vDispCenter(1, szTitle, 1);    

#ifdef ENABLE_PRINTER 
    {		
        //打印票据
        vShowWaitEx("交易成功,打印票据...", -1, 0);
        iPrintTrans(&gl_TransRec, 0, gl_Recv8583.Field63+3);
        vShowWaitEx(NULL, 0, 0x03);
    }
#else    
    {
        gl_SysInfo.ucDepositFlag = 1;
	gl_SysInfo.ucActivityFlag = 1;		
	if(gl_SysInfo.ucGprsUseFlag  == 1)
		gl_SysInfo.ucGprsPayFlag = 1;
	
        vMessage("交易成功");
    }
#endif    
//日结消费
	do{
		if(gl_TransRec.ucSaleType!=1)break;

		iUploadComfirmSaleTrade();
		
	}while(0);

    return 0;
}

int iUploadOfflineTrans(void)
{
    stTransRec transRec;
    int i;
    int iRet;
    char szBuf[100];
    uchar szDateTime[14+1];
    
    for(i=0; i<gl_SysData.uiTransNum; i++)
    {
        uiMemManaGetTransRec(i, &transRec);

        if(transRec.uiTransType!=TRANS_TYPE_SALE || transRec.ucUploadFlag!=0x01)
            continue;
        
        _vGetTime(szDateTime);
        memset(&gl_Send8583, 0, sizeof(gl_Send8583));
        memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
        strcpy((char *)gl_Send8583.Msg01, "0320");
        vUnpackPan(transRec.sPan, gl_Send8583.Field02);
        strcpy((char *)gl_Send8583.Field03, "000000");
        sprintf((char *)gl_Send8583.Field04, "%012lu", transRec.ulAmount);
//        strcpy((char *)gl_Send8583.Field07, (char *)szDateTime + 4);
        vIncTTC();
        sprintf((char *)gl_Send8583.Field11, "%06lu", transRec.ulTTC);
        vOneTwo0(transRec.sDateTime, 6, (uchar*)szBuf);
        sprintf((char *)gl_Send8583.Field12, "%.6s", szBuf + 6);
        sprintf((char *)gl_Send8583.Field13, "%.4s", szBuf + 2);

        sprintf((char *)gl_Send8583.Field22, "%03d", transRec.uiEntryMode);
        if(transRec.ucPanSerNo!=0xFF)
            sprintf((char *)gl_Send8583.Field23, "%03d", transRec.ucPanSerNo);
        strcpy((char *)gl_Send8583.Field25, "00");

        //strcpy((char *)gl_Send8583.Field35, "6227614850220023=19052010000051000000");
        
        strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
        strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
        strcpy((char *)gl_Send8583.Field49, "156");

        //55域
        gl_Send8583.Field55[0]=transRec.uiIcDataLen/256;
        gl_Send8583.Field55[1]=transRec.uiIcDataLen%256;
        memcpy(gl_Send8583.Field55+2, transRec.sIcData, transRec.uiIcDataLen);
        
        //vGenField57(gl_Send8583.Field57);
        sprintf((char *)gl_Send8583.Field60, "%.2s%.4s", "01", "0000");
        //vFillFieldLen(gl_Send8583.Field60);
        sprintf((char *)gl_Send8583.Field62+2, "%06lu%06lu", gl_SysData.ulBatchNo, transRec.ulTTC);
        vFillFieldLen(gl_Send8583.Field62);
        memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

        //打8583包-填充http-发送-接收-解http-解8583包
        iRet = iHttpSendRecv8583(0);
        if (iRet)
        {
            //显示错误信息
            return iRet;
        }
        if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
        {
            //显示服务端错误信息
            vShowHostErrMsg(gl_Recv8583.Field39, NULL);
            //return -1;
            continue;
        }
        transRec.ucUploadFlag=0x02;
        uiMemManaPutTransRec(i, &transRec);
    }

    return 0;
}

void vPrt8583Fields(char ucType, st8583 *pMsg8583)
{
	char szTip[20];
	u16 len=0;
	int mode = 0;

#ifdef JTAG_DEBUG
    mode=1;
#endif
     if(mode || ucGetUsbDebug())
     {
	dbg("\n\n");
	serialAsc("\n\n");
	if(ucType==1)
	{
		dbg("*** Send 8583 Fields ***\n");
		serialAsc("*** Send 8583 Fields ***\n");
		strcpy(szTip, "Snd->Field");
	}else
	{
		dbg("*** Recv 8583 Fields ***\n");
		serialAsc("*** Recv 8583 Fields ***\n");
		strcpy(szTip, "Rcv->Field");
	}
	
	dbg("%s00:[%s]\n", szTip, pMsg8583->Msg01);
	serialAsc("%s00:[%s]\n", szTip, pMsg8583->Msg01);
	dbgHex("xxx->Field01(BitMap)", pMsg8583->BitMap, 8);
	serialHex("xxx->Field01(BitMap)", pMsg8583->BitMap, 8);
	if(pMsg8583->Field02[0])
	{
		dbg("%s02:[%s]\n", szTip, pMsg8583->Field02);
		serialAsc("%s02:[%s]\n", szTip, pMsg8583->Field02);
	}	
	if( pMsg8583->Field03[0])
	{
		dbg("%s03:[%s]\n", szTip, pMsg8583->Field03);
		serialAsc("%s03:[%s]\n", szTip, pMsg8583->Field03);
	}	
	if(pMsg8583->Field04[0])
	{
		dbg("%s04:[%s]\n", szTip, pMsg8583->Field04);
		serialAsc("%s04:[%s]\n", szTip, pMsg8583->Field04);
	}	
	len = ulStrToLong(pMsg8583->Field05,2);
   	 if(len>0)
   	 {
     	  	 dbgHex("xxx->Field05", pMsg8583->Field05+2, len);
		serialHex("xxx->Field05", pMsg8583->Field05+2, len);
        	_vDelay(5);
    	}	
	
//    len = (pMsg8583->Field06[0]|0xF0)>>4*10 + (pMsg8583->Field06[0]|0x0F);
//    if(len>0)
//    {
//		dbgHex("xxx->Field06", pMsg8583->Field06, len+1);
//    }
	 if(pMsg8583->Field06[0])
	{
		dbg("%s06:[%s]\n", szTip, pMsg8583->Field06);
                serialAsc("%s06:[%s]\n", szTip, pMsg8583->Field06);
	 }			
	if(pMsg8583->Field11[0])
	{
		dbg("%s11:[%s]\n", szTip, pMsg8583->Field11);
	        serialAsc("%s11:[%s]\n", szTip, pMsg8583->Field11);
	}
	if(pMsg8583->Field12[0])
	{
		dbg("%s12:[%s]\n", szTip, pMsg8583->Field12);
		serialAsc("%s12:[%s]\n", szTip, pMsg8583->Field12);
	}
	if(pMsg8583->Field13[0])
	{
		dbg("%s13:[%s]\n", szTip, pMsg8583->Field13);
		serialAsc("%s13:[%s]\n", szTip, pMsg8583->Field13);
	}	
	if(pMsg8583->Field14[0])
	{
		dbg("%s14:[%s]\n", szTip, pMsg8583->Field14);
		serialAsc("%s14:[%s]\n", szTip, pMsg8583->Field14);
	}	
	if(pMsg8583->Field15[0])
	{
		dbg("%s15:[%s]\n", szTip, pMsg8583->Field15);
		serialAsc("%s15:[%s]\n", szTip, pMsg8583->Field15);
	}		
	if(pMsg8583->Field22[0])
	{
		dbg("%s22:[%s]\n", szTip, pMsg8583->Field22);
		serialAsc("%s22:[%s]\n", szTip, pMsg8583->Field22);
	}	
	if(pMsg8583->Field23[0])
	{
		dbg("%s23:[%s]\n", szTip, pMsg8583->Field23);
		serialAsc("%s23:[%s]\n", szTip, pMsg8583->Field23);
	}
	if(pMsg8583->Field24[0])
	{
		dbg("%s24:[%s]\n", szTip, pMsg8583->Field24);
		serialAsc("%s24:[%s]\n", szTip, pMsg8583->Field24);
	}
	if(pMsg8583->Field25[0])
	{
		dbg("%s25:[%s]\n", szTip, pMsg8583->Field25);
		serialAsc("%s25:[%s]\n", szTip, pMsg8583->Field25);
	}	
	if(pMsg8583->Field26[0])
	{
		dbg("%s26:[%s]\n", szTip, pMsg8583->Field26);
		serialAsc("%s26:[%s]\n", szTip, pMsg8583->Field26);
	}	
	if(pMsg8583->Field27[0])
	{
		dbg("%s27:[%s]\n", szTip, pMsg8583->Field27);
		serialAsc("%s27:[%s]\n", szTip, pMsg8583->Field27);
	}	
	if(pMsg8583->Field28[0])
	{
		dbg("%s28:[%s]\n", szTip, pMsg8583->Field28);
		serialAsc("%s28:[%s]\n", szTip, pMsg8583->Field28);
	}	
	if(pMsg8583->Field29[0])
	{
		dbg("%s29:[%s]\n", szTip, pMsg8583->Field29);
		serialAsc("%s29:[%s]\n", szTip, pMsg8583->Field29);
	}	
	if(pMsg8583->Field30[0])
	{
		dbg("%s30:[%s]\n", szTip, pMsg8583->Field30);
		serialAsc("%s30:[%s]\n", szTip, pMsg8583->Field30);
	}	
    	if(pMsg8583->Field32[0])
	{
		dbg("%s32:[%s]\n", szTip, pMsg8583->Field32);
		serialAsc("%s32:[%s]\n", szTip, pMsg8583->Field32);
    	}
	if(pMsg8583->Field35[0])
	{
		dbg("%s35:[%s]\n", szTip, pMsg8583->Field35);
		serialAsc("%s35:[%s]\n", szTip, pMsg8583->Field35);
	}
	if(pMsg8583->Field36[0])
	{
		dbg("%s36:[%s]\n", szTip, pMsg8583->Field36);
		serialAsc("%s36:[%s]\n", szTip, pMsg8583->Field36);
	}	
	if(pMsg8583->Field37[0])
	{
		dbg("%s37:[%s]\n", szTip, pMsg8583->Field37);
		serialAsc("%s37:[%s]\n", szTip, pMsg8583->Field37);
	}	
	if(pMsg8583->Field38[0])
	{
		dbg("%s38:[%s]\n", szTip, pMsg8583->Field38);
		serialAsc("%s38:[%s]\n", szTip, pMsg8583->Field38);
	}	
	if(pMsg8583->Field39[0])
	{
		dbg("%s39:[%s]\n", szTip, pMsg8583->Field39);
		serialAsc("%s39:[%s]\n", szTip, pMsg8583->Field39);
	}

	if( memcmp(pMsg8583->Field40, "\x00\x00", 2) )
	{
		dbgHex("xxx->Field40", pMsg8583->Field40+2, pMsg8583->Field40[0]*256+pMsg8583->Field40[1]);
		serialHex("xxx->Field40", pMsg8583->Field40+2, pMsg8583->Field40[0]*256+pMsg8583->Field40[1]);
	}
	if(pMsg8583->Field41[0])
	{
		dbg("%s41:[%s]\n", szTip, pMsg8583->Field41);
		serialAsc("%s41:[%s]\n", szTip, pMsg8583->Field41);
	}
	if(pMsg8583->Field42[0])
	{
		dbg("%s42:[%s]\n", szTip, pMsg8583->Field42);
		serialAsc("%s42:[%s]\n", szTip, pMsg8583->Field42);
	}

	if(pMsg8583->Field44[0])  
	{
		dbgHex("xxx->Field44", pMsg8583->Field44, strlen(pMsg8583->Field44));	
		serialHex("xxx->Field44", pMsg8583->Field44, strlen(pMsg8583->Field44));	
	}
	
	len = ulStrToLong(pMsg8583->Field45,2);
    	if(len>0)
    	{
		dbgHex("xxx->Field45", pMsg8583->Field45+2, len);
		serialHex("xxx->Field45", pMsg8583->Field45+2, len);
		_vDelay(5);
    	}
	
    	if(pMsg8583->Field47[0])
	{
		dbg("%s47:[%s]\n", szTip, pMsg8583->Field47);
		serialAsc("%s47:[%s]\n", szTip, pMsg8583->Field47);
	}
	if(pMsg8583->Field48[0])
	{
		dbg("%s48:[%s]\n", szTip, pMsg8583->Field48);
		serialAsc("%s48:[%s]\n", szTip, pMsg8583->Field48);
	}
	if(pMsg8583->Field49[0])
	{
		dbg("%s49:[%s]\n", szTip, pMsg8583->Field49);
		serialAsc("%s49:[%s]\n", szTip, pMsg8583->Field49);
	}
#if 0	
    if(pMsg8583->Field51[0])
		dbg("%s51:[%s]\n", szTip, pMsg8583->Field51);
#endif
	if( memcmp(pMsg8583->Field52, "\x00\x00", 2) )
	{
		dbgHex("xxx->Field52:", pMsg8583->Field52+2, 8);
		serialHex("xxx->Field52:", pMsg8583->Field52+2, 8);
	}
	if(pMsg8583->Field53[0])
	{
		dbg("%s53:[%s]\n", szTip, pMsg8583->Field53);
		serialAsc("%s53:[%s]\n", szTip, pMsg8583->Field53);
	}
	if(pMsg8583->Field54[0])
	{
		dbg("%s54:[%s]\n", szTip, pMsg8583->Field54);	
		serialAsc("%s54:[%s]\n", szTip, pMsg8583->Field54);	
	}
	if( memcmp(pMsg8583->Field55, "\x00\x00", 2) )
    	{
    		len = ulStrToLong(pMsg8583->Field55,2);
		dbgHex("xxx->Field55", pMsg8583->Field55+2, len);
		serialHex("xxx->Field55", pMsg8583->Field55+2, len);
       		 _vDelay(20);
    	}
	len = ulStrToLong(pMsg8583->Field56,2);
    	if(len>0)
    	{
		dbgHex("xxx->Field56", pMsg8583->Field56, len+2);
		serialHex("xxx->Field56", pMsg8583->Field56, len+2);
		_vDelay(5);

    	}
	len = ulStrToLong(pMsg8583->Field57,2);
  	if(len>0)
    	{
		dbg("%s57:[%s]\n", szTip, &pMsg8583->Field57[2]);
		serialAsc("%s57:[%s]\n", szTip, &pMsg8583->Field57[2]);
    	}
	len = ulStrToLong(pMsg8583->Field58,2);
    	if(len>0)
    	{
		dbgHex("xxx->Field58", pMsg8583->Field58, len+2);
		serialHex("xxx->Field58", pMsg8583->Field58, len+2);
		_vDelay(5);

    	}
	len = ulStrToLong(pMsg8583->Field59,2);
    	if(len>0)
    	{
		dbgHex("xxx->Field59", pMsg8583->Field59, len+2);
		serialHex("xxx->Field59", pMsg8583->Field59, len+2);
		_vDelay(5);
    	}
    	if(pMsg8583->Field60[0])
	{
		dbgHex("xxx->Field60", pMsg8583->Field60, strlen(pMsg8583->Field60));
		serialHex("xxx->Field60", pMsg8583->Field60, strlen(pMsg8583->Field60));
		_vDelay(5);
	}
	if(pMsg8583->Field61[0])
	{
		dbg("%s61:[%s]\n", szTip, pMsg8583->Field61);	
		serialAsc("%s61:[%s]\n", szTip, pMsg8583->Field61);	
	}
	if( memcmp(pMsg8583->Field62, "\x00\x00", 2) )
	{  
		len = ulStrToLong(pMsg8583->Field62,2);
		dbgHex("xxx->Field62", pMsg8583->Field62+2, len);
		serialHex("xxx->Field62", pMsg8583->Field62+2, len);
		_vDelay(5);
	}
	if(pMsg8583->Field63[0])
	{
	    	dbgHex("xxx->Field63", pMsg8583->Field63, strlen(pMsg8583->Field63) );	
	 	serialHex("xxx->Field63", pMsg8583->Field63, strlen(pMsg8583->Field63) );	
	}
	if( memcmp(pMsg8583->Field64, "\x00\x00\x00\x00", 4) )
	{
		dbgHex("xxx->Field64", pMsg8583->Field64+2, 8);
		serialHex("xxx->Field64", pMsg8583->Field64+2, 8);
	}
	dbg("*** Print 8583 Fields End! ***\n");
	serialAsc("*** Print 8583 Fields End! ***\n");
	dbg("\n\n");
     }  
}

/*
子POS同母POS协议：
STX(0x02)+len[2]+cmd[1]+data[n]+ETX(0x03)+CRC
其中：len为cmd+data长度,高位在前；
      CRC为从STX开始至ETX(包括STX和ETX)所有字符异或的结果

cmd: 0x01 取子POS的TMK
请求data 子pos序列号[n] 
响应data 响应码[1]+TMK密文[16]+校验码[3]。
			 响应码0为成功，成功时才有TMK密文和校验码。
			 TMK加密密钥为双方约定的固定des密钥,校验码为明文TMK对0做des加密后的前三字节。

响应码:
	0-成功
	1-母POS无此子POS序列号对应TMK
	2-母POS未注入密钥
	3-不支持的指令
	4-请求报文crc错
	5-请求报文格式错
*/
#define TMK_KEK         "JSDTKKEK"
int iImportTmkfromMainPos(void)
{
    uchar sBuf[100], szTmp[30], cmd, crc;
    uchar sTmk[16];
    int i, len;
    ulong ulTimer;

    _vCls();
    vDispCenter(1, "下载主密钥", 1);
    
    cmd=0x01;   //0x01 取TMK

    sBuf[0]=STX;
    //sBuf[1]=0;      //数据长度高字节,暂赋值0
    //sBuf[2]=0;      //数据长度低字节,暂赋值0

    //包含指令的数据域
    len=3;
    sBuf[len++]=cmd;   

    _uiGetSerialNo(szTmp);
    memcpy(sBuf+len, szTmp, strlen(szTmp));
    len+=strlen(szTmp);

    //数据域结束,长度赋值
    sBuf[1]=(len-3)/256;
    sBuf[2]=(len-3)%256;

    sBuf[len++]=ETX;
    crc=sBuf[0];
    for(i=1; i<len; i++)
        crc^=sBuf[i];
    sBuf[len++]=crc;
    
    _ucAsyncReset();

    if(_ucAsyncSendBuf(sBuf, len))
    {
        vMessage("发送失败,请检查usb连线正常");
        return 1;
    }
    
    len=0;
    _vSetTimer(&ulTimer, 10*100);
    while(!_uiTestTimer(ulTimer))
    {
        if(_ucAsyncTest() && _ucAsyncGet()==STX)
        {
            sBuf[0]=STX;
            if(_ucAsyncGetBuf(sBuf+1, 2, 1))
            {
                vMessage("接收响应长度失败");
                return 1;
            }
            len=sBuf[1]*256+sBuf[2];
            if(len<=0 || len>200)
            {
                vMessage("数据格式错");
                return 1;
            }
            if(_ucAsyncGetBuf(sBuf+3, len+2, 2))
            {
                vMessage("接收数据失败");
                return 1;
            }
            len+=5;
            break;
        }
        _vDelay(10);
    }
    if(len==0)
    {
        vMessage("接收超时");
        return 1;
    }
    if(sBuf[3]!=cmd && sBuf[len-2]!=ETX)
    {
        vMessage("数据格式错2");
        return 1;
    }
    crc=sBuf[0];
    for(i=1; i<len-1; i++)
        crc^=sBuf[i];
    if(crc!=sBuf[len-1])
    {
        vMessage("校验位错");
        return 1;
    }

    if(sBuf[4]!=0x00)
    {
        switch (sBuf[4])
        {
        case 0x01:
            strcpy(szTmp, "无此序列号密钥");
            break;
        case 0x02:
            strcpy(szTmp, "母POS还未注入密钥");
            break;
        case 0x03:
            strcpy(szTmp, "不支持的指令");
            break;
        case 0x04:
            strcpy(szTmp, "请求报文CRC错");
            break;
        case 0x05:
            strcpy(szTmp, "请求报文CRC错");
            break;
        default:
            strcpy(szTmp, "处理失败");
            break;
        }
        vMessage(szTmp);
        return 1;
    }
    if(len-5-2<16+3)
    {
        vMessage("数据格式错3");
        return 1;
    }

    _vDesPlus(DECRYPT, sBuf+5, 16, TMK_KEK, sTmk);
    memset(sBuf, 0, 8);
    _vDes(TRI_ENCRYPT, sBuf, sTmk, szTmp);
    if(memcmp(sBuf+5+16, szTmp, 3))
    {
        vMessage("密钥校验错");
        return 1;
    }
    memcpy(gl_SysInfo.sMasterKey, sTmk, 16);
    uiMemManaPutSysInfo();

    vMessage("密钥注入成功");
    return 0;
}

int GetPublicFieldSign(stTransRec *recIn,uchar *psOutData)
{
        uchar szBuf[700 +1]={0};
	uchar *p;
	uchar buf[60 + 1];
	int len = 0;	
	p = szBuf;
#if 1
	len = strlen(gl_SysInfo.szMerchName);
        p[0] = 0xFF;
	p[1] = 0x00;
	p[2] = len;
        memcpy(p+3,gl_SysInfo.szMerchName,p[2]);
	p += 3 + p[2];	

	  dbgHex("szBuf", szBuf,  3 + p[2]);
#endif

 #if 1
	   
        if(recIn->uiTransType == TRANS_TYPE_SALE)
	{
		len = strlen("消费");
		p[0] = 0xFF;
		p[1] = 0x01;
		p[2] = len;
		memcpy(p+3,"消费",p[2]);
		p += 3 + p[2];	
        }

        memset(buf,0x00,sizeof(buf));
	vTwoOne(gl_SysData.szCurOper, 2, buf);
	len = strlen(buf);
	p[0] = 0xFF;
	p[1] = 0x02;
	p[2] = len;
        memcpy(p+3,buf,p[2]);
	p += 3 + p[2];	

	len = strlen(recIn->recvBankId);
	p[0] = 0xFF;
	p[1] = 0x03;
	p[2] = len;
        memcpy(p+3,recIn->recvBankId,p[2]);
	p += 3 + p[2];	

	len = strlen(recIn->issuerBankId);
	p[0] = 0xFF;
	p[1] = 0x04;
	p[2] = len;
        memcpy(p+3,recIn->issuerBankId,p[2]);
	p += 3 + p[2];	

	len = strlen(recIn->sCardExpire);
	p[0] = 0xFF;
	p[1] = 0x05;
	p[2] = len;
        memcpy(p+3,recIn->sCardExpire,p[2]);
	p += 3 + p[2];	

        memset(buf,0x00,sizeof(buf));	
	buf[0] = 0x20;
        strcpy(buf+1,recIn->sDateTime);
	len = strlen(buf);
	p[0] = 0xFF;
	p[1] = 0x06;
	p[2] = len;
        memcpy(p+3,buf,p[2]);
	p += 3 + p[2];	

        //预授权类交易才有
        if(recIn->uiTransType == TRANS_TYPE_PREAUTH)
	{
		len = strlen(recIn->sAuthCode);
		p[0] = 0xFF;
		p[1] = 0x07;
		p[2] = len;
	        memcpy(p+3,recIn->sAuthCode,p[2]);
		p += 3 + p[2];	
        }
#endif

#if 1
	//IC卡交易
	dbg("recIn->uiEntryMode:%d\n",recIn->uiEntryMode);
	dbg("recIn->uiTransType :%04x\n",recIn->uiTransType );
	if((recIn->uiEntryMode/10==5 || recIn->uiEntryMode/10==7) && (recIn->uiTransType == TRANS_TYPE_SALE ||recIn->uiTransType == TRANS_TYPE_PREAUTH))
	{

        int tagBinLen,tagBinLen1,tagBinLen2,tagBinLen3;
        TagList tmpTagList;
        Tag3ByteList tmpTag3ByteList;
        ushort tag;
	char AppLabel[16+1] = {0};
	char AppName[16+1] = {0};
        char Aid[16+1] = {0};
	char ARQC[8+1] = {0};	
	
        InitTagList(&tmpTagList);
        InitTag3ByteList(&tmpTag3ByteList);
        
        BuildTagListOneLevelBctc(recIn->sIcData, recIn->uiIcDataLen, &tmpTagList, &tmpTag3ByteList);

        // 应用标签50
        tag=0x50;
	memcpy(AppLabel,GetTagValue(&runningTimeEnv.tagList, tag),
	GetTagValueSize(&runningTimeEnv.tagList, tag));
	tagBinLen = GetTagValueSize(&runningTimeEnv.tagList, tag);
        dbgHex("AppLabel", AppLabel,tagBinLen);	
          if(tagBinLen > 0)
          {
		len = tagBinLen;
		p[0] = 0xFF;
		p[1] = 0x30;
		p[2] = len;
		memcpy(p+3,AppLabel,p[2]);
		p += 3 + p[2];	
        }

	 // 应用名称9F12
        tag=0x9F12;
	memcpy(AppName,GetTagValue(&runningTimeEnv.tagList, tag),
	GetTagValueSize(&runningTimeEnv.tagList, tag));
	tagBinLen1 = GetTagValueSize(&runningTimeEnv.tagList, tag);
        dbgHex("AppName", AppName,tagBinLen1);
           if(tagBinLen1 > 0)
	 {
	   	len = tagBinLen1;
		p[0] = 0xFF;
		p[1] = 0x31;
		p[2] = len;
		memcpy(p+3,AppName,p[2]);
		p += 3 + p[2];	
        }
		
	//应用标识4F
        tag=0x4F;
	memcpy(Aid,GetTagValue(&runningTimeEnv.tagList, tag),
	GetTagValueSize(&runningTimeEnv.tagList, tag));
	tagBinLen2 = GetTagValueSize(&runningTimeEnv.tagList, tag);
        dbgHex("Aid", Aid,tagBinLen2);
       if(tagBinLen2 > 0)
	{
		len = tagBinLen2;
		p[0] = 0xFF;
		p[1] = 0x22;
		p[2] = len;
		memcpy(p+3,Aid,p[2]);
		p += 3 + p[2];	
        }  

	//应用密文9F26
	 tag=0x9F26;
    	memcpy(ARQC,GetTagValue(&runningTimeEnv.tagList, tag),
	GetTagValueSize(&runningTimeEnv.tagList, tag));
	tagBinLen3 = GetTagValueSize(&runningTimeEnv.tagList, tag);
         dbgHex("ARQC", ARQC,tagBinLen3);
	if(tagBinLen3 > 0)	
	{
		len = tagBinLen3;
		p[0] = 0xFF;
		p[1] = 0x23;
		p[2] = len;
		memcpy(p+3,ARQC,p[2]);
		p += 3 + p[2];	
	}
		
        FreeTagList(&tmpTagList);  
	}
	
//		p[0] = 0xFF;
//		p[1] = 0x40;
//		p[2] = len;
//		memcpy(p+3,"PBOC DEBIT",p[2]);
//		p += 3 + p[2];	
 #endif      

	len = p - szBuf;	
	dbg("len:%d\n",len);	

	memcpy(psOutData,szBuf,len);
	dbgHex("sign_asOut", psOutData,  len);
	
	return len;
}

//int iUploadSignFile(int envFlag, int iIdx)
int iUploadSignFile(int envFlag, int iIdx, stTransRec *recIn)
{
    char szFileName[30];
    uchar buf[100];
    uchar sJbg[999];    //银联规定签字最大长度999（0.9版)
    int  iJbgLen=0;
    int iRet;
    stTransRec *rec;
    int num, iMaxNum;
	
/*	
    if(iIdx<0 || iIdx>=gl_SysData.uiTransNum)
    {
        iIdx=gl_SysData.uiTransNum-1;
    }
    if(rec==NULL)
    {
        uiMemManaGetTransRec(iIdx, &recTmp);
        rec=&recTmp;
    }else
    */
        rec=recIn;
    //if(rec->uiTransType>=0x8000)        //扫码交易无需上送
    //    return 0;
    if(rec->ucSignupload==0xFF || rec->ucSignupload>3)
    {
        dbg("not need upload signfile!!!\n");
        //vMessageEx("当前签名无需上传", 200);
        return 0;
    }

    vOneTwo0(rec->sDateTime, 6, buf);
    sprintf(szFileName, "%.4s_%06lu.jbg", buf+2, rec->ulTTC);
    if(uiMemGetJbg(szFileName, sJbg, sizeof(sJbg), &iJbgLen)!=0 || iJbgLen<100)
    {
        dbg("iUploadSignFile: rec->ucTransAttr=%02X\n", rec->ucTransAttr);
        dbg("uiMemGetJbg err:[%s]\n", szFileName);
        vMessageEx("读取签名文件失败", 200);
	if(rec->ucSettSignupload == 1)  
        	iPrintTrans(rec, 1, NULL);
        return 1;
    }

    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));

    strcpy((char *)gl_Send8583.Msg01, "0820");
    vUnpackPan(rec->sPan, gl_Send8583.Field02);
    strcpy((char *)gl_Send8583.Field03, "440000");
    sprintf((char *)gl_Send8583.Field04, "%012lu", rec->ulAmount);
    sprintf((char *)gl_Send8583.Field11, "%06lu", rec->ulTTC);
//	vOneTwo(rec->sSettleDate, 2, (char *)gl_Send8583.Field15);
    if(rec->sReferenceNo[0])
        sprintf((char *)gl_Send8583.Field37, "%.12s", rec->sReferenceNo);
    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);   
    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId); 

    //55域
    iRet = GetPublicFieldSign(recIn, gl_Send8583.Field55+2);
    if(iRet>0)
        {
            gl_Send8583.Field55[0]=iRet/256;
            gl_Send8583.Field55[1]=iRet%256;
        }else
        {
            dbg("iGetSignMsgFd55FromEnv err.\n");
            return 1;
        }
		
    /*
    //55域
    //盛迪嘉：9F26,95,4F,5F34,9B,9F36,82,9F37,50
    if(envFlag==1)
    {
        iRet=iGetSignMsgFd55FromEnv(gl_Send8583.Field55+2);
        if(iRet>0)
        {
            gl_Send8583.Field55[0]=iRet/256;
            gl_Send8583.Field55[1]=iRet%256;
        }else
        {
            dbg("iGetSignMsgFd55FromEnv err.\n");
            return 1;
        }
    }*/
#if 0    //不送55域
    if(rec->uiIcDataLen)
    {
        TagList tmpTagList;
        Tag3ByteList tmpTag3ByteList;
        tti_uint16 tagTLVLen=0;

        InitTagList(&tmpTagList);
        InitTag3ByteList(&tmpTag3ByteList);
        
        BuildTagListOneLevelBctc(rec->sIcData, rec->uiIcDataLen, &tmpTagList, &tmpTag3ByteList);
        
        {
            //盛迪嘉签名上送报文55域: 9F26,95,4F,5F34,9B,9F36,82,9F37,50
            tti_uint16 tagList[] = { 
                                    0x9F26,			// ARQC
                                    0x95,			// TVR
                                    0x4F,			// AID
                                    0x5F34,			// CSN
                                    0x9B,			// TSI
                                    0x9F36,			// ATC
                                    0x82,			// AIP
                                    0x9F37,			// UNPR NUM
                                    0x50			// APPLAB									
                                    };
            

            TagArrsBuildTLV(&tmpTagList, tagList, sizeof(tagList)/sizeof(tagList[0]), gl_Send8583.Field55+2, &tagTLVLen);
            hexdumpEx("signFd55:", gl_Send8583.Field55+2, tagTLVLen);
        }
        gl_Send8583.Field55[0]=tagTLVLen/256;
        gl_Send8583.Field55[1]=tagTLVLen%256;
        
        FreeTagList(&tmpTagList);
    }else
    {
        //磁条卡没有55
    }
#endif    
    sprintf((char *)gl_Send8583.Field60, "%02u%06lu", 7, gl_SysData.ulBatchNo);
    gl_Send8583.Field62[0]=iJbgLen/256;
    gl_Send8583.Field62[1]=iJbgLen%256;
    memcpy(gl_Send8583.Field62+2, sJbg, iJbgLen);
    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

    //打8583包-填充http-发送-接收-解http-解8583包

	//iRet = iHttpSendRecv8583(0);
	_vCls();
	vDispCenter(1, "电子签名上送", 1);
	//sprintf(buf, "交易成功,上送电子票据%d...", iJbgLen);
	//_vDisp(2, buf);
    	num=0;
    //if(envFlag!=1)
        iMaxNum=gl_SysInfo.uiESignSendCount;
    //else
    //    iMaxNum=1;
    while(++num<=iMaxNum)
    {
        vDispMidVarArg(2, "上送电子签名%d次...", num);
	gl_ucSignuploadFlag = 1;	
    	iRet=iHttpSndRcv8583Msg(&gl_Send8583, &gl_Recv8583,0);
	vClearLines(2);
	 if(iRet==0 && strcmp((char *)gl_Recv8583.Field39, "96") != 0)
            break;
    }
	
    if (iRet || strcmp((char *)gl_Recv8583.Field39, "00") != 0)
    {
        vClearLines(2);
//        vMessageEx("电子签名上送失败", 200);
	vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);
/*
       if(rec->ucSignupload==0x00)
        {
            //if(envFlag==2)
            //    rec->ucSignupload++;
            //else
                rec->ucSignupload=1;
		rec->ucTransAttr|=0x04;     //b3电签不成功/需纸签
            uiMemManaPutTransRec(iIdx,rec);			
        } 
	 */  
	if(rec->ucSettSignupload == 1)  
        	iPrintTrans(rec, 1, NULL);
        if(iRet<0)
            return -99;
        return -1;
    }
/*	
    rec->ucSignupload=0xFF;
    uiMemManaPutTransRec(iIdx, rec);
*/
    if(gl_SysData.ucFailSignNum)         //预先增加了失败电签笔数,成功上送后减去
    {
        gl_SysData.ucFailSignNum--;
        uiMemManaPutSysData();
    }
		
    vClearLines(2);
    if(envFlag)
        vMessageEx("上送成功", 200);
    return 0;
}

void vGetPrtMerchInfo(uchar *psFields, stTransRec *rec)
{
    int len;
    uchar *p;
    uchar *CurPos;
	
    len=strlen(psFields);
    if(len==0)
        return;
    CurPos=psFields; 
    
     //商户号
     p = (uchar *)strchr(CurPos,'|');
     memcpy(rec->sPrtMerchId, CurPos, p-CurPos);
    CurPos = p+1;
	 
     //终端号
      p = (uchar *)strchr(CurPos,'|');
     memcpy(rec->sPrtPosId, CurPos,p-CurPos);
     CurPos = p+1;
 
      //商户名	  
      rtrim(CurPos);
      strcpy(rec->szPrtMerchName,CurPos);  		
#ifdef REMOTE_GBK
    {
        char gbkstr[100];
        
        strcpy(gbkstr, (char*)rec->szPrtMerchName);        
        iGetStringGBK((uchar*)gbkstr, -1);
    }
#endif 
	  
    return;
}

int iUploadComfirmSaleTrade(void)
{
	uchar szRid[3+1]={0};
    int iRet;
 
    stTransRec rec;
	int iIdx=gl_SysData.uiTransNum-1;
	dbg("uiTransNum=%d\r\n",iIdx);

	memset(&rec,0,sizeof(stTransRec));
    iRet = uiMemManaGetTransRec(iIdx, &rec);

	
	if(iRet)return 1;
    
	

    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
//MSG TYPE
    strcpy((char *)gl_Send8583.Msg01, "0620");
//DE02
    vUnpackPan(rec.sPan, gl_Send8583.Field02);
//DE03
	strcpy((char *)gl_Send8583.Field03, "280000");
//DE04
    sprintf((char *)gl_Send8583.Field04, "%012lu", rec.ulAmount);
//DE11    
    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
	vIncTTC();
//DE37
    if(strlen(rec.sReferenceNo))
        sprintf((char *)gl_Send8583.Field37, "%.12s", rec.sReferenceNo);
//DE38
    if(strlen(rec.sAuthCode))
        sprintf((char *)gl_Send8583.Field37, "%.6s", rec.sAuthCode);	
//DE41,42	
    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);

//DE60
	memset(szRid,0,sizeof(szRid));
	if(rec.uiEntryMode/10==2)		//磁条卡
	{
		szRid[0]=0;
	}else {
		strcpy(szRid, "50");	
	}
	sprintf((char *)gl_Send8583.Field60, "%02u%06lu%.3s%.2s", 22, gl_SysData.ulBatchNo, "000", szRid);
//DE61
	sprintf((char *)gl_Send8583.Field61, "%06lu%06lu", gl_SysData.ulBatchNo, rec.ulTTC);
//DE63
	sprintf((char *)gl_Send8583.Field63, "%-3.3s", gl_SysData.szCurOper);  
//DE64
    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

    //打8583包-填充http-发送-接收-解http-解8583包
    _vCls();
    vDispCenter(1, "日结确认", 1);
    iRet=iHttpSndRcv8583Msg(&gl_Send8583, &gl_Recv8583,0);
       
    if (iRet || strcmp((char *)gl_Recv8583.Field39, "00") != 0)
    {
        vClearLines(2);
        vMessageEx("日结确认上送失败", 200);
        return -1;
    }
//    uiMemManaPutTransRec(iIdx, rec);
    
    vClearLines(2);

    vMessageEx("日结确认上送成功", 200);
    return 0;

}

