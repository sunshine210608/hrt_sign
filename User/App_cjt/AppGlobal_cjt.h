#ifndef _APP_GLOBAL_H
#define _APP_GLOBAL_H
#include "VposFace.h"
#include "EmvProc.h"
#include "Oper.h"
#include "St8583_cjt.h"

#include "Define.h"
#include "appdebug.h"

#define Trace(ptag, arg...)  debugTrace((u8 const*)__FUNCTION__, __LINE__, ##arg)
#define TraceHex(head, in, len) dbgHexEx(head, in, len,(u8 const*)__FUNCTION__, __LINE__);

/*  
折扣版本需求:
一、菜单变更
1、刷卡消费 2、扫码支付
3、折扣消费 4、管理
5、优惠消费 6、其他

a、新增三个菜单，分别为 1、刷卡消费 3、折扣消费 5、优惠消费
b、删除原有菜单 撤销、预授权、退货，后面菜单依次上移重新命名序号即可

二、报文变更
a、新增的三个菜单，都是基于原有消费交易，处理流程不做任何变更。只根据135菜单入口不同，分别在F40-TagC1中标记出来
b、详细菜单标识见文档F40，C1用法
F40TagC1	11:刷卡消费 13:折扣消费 15:优惠消费
*/

#define POS_APP_VERSION					"1.20" // pos程序版本号, version
#define POS_APP_RELEASE_DATE			"2016/4/28" // 发行时间

#ifndef OPER_NO_LEN
#define OPER_NO_LEN						2	    // 操作员号码长度
#endif

#define CARD_BIN_LEN					14		// 卡bin长度, 卡BIN[14] // 尾部补空
//#define CARD_SLOT_NO					8       // 卡座号
#define MAX_AID_NUM						20      // 最多支持的AID个数
#define MAX_CA_KEY_NUM					40      // 最多支持的公钥个数
#define MAX_OPER_NUM					20      // 最多操作员个数
#define MAX_CARD_BIN_WHITE				1		// 最多卡BIN白名单个数100
#define MAX_CARD_BIN_BLACK				1		// 最多卡BIN黑名单个数100
#define MAX_SETTLE_LIST_NUM				5		// 最多结算数据个数

#ifdef MAX_TRANS_RECORD_NUM
#define MAX_TRANS_NUM   MAX_TRANS_RECORD_NUM
#else
#define MAX_TRANS_NUM					400    // 交易记录容量,最多1000
#endif
#define MAX_QRTRANS_NUM					200

#define BLACK_CARD_ID_LEN				11		// Pan[10]/*后补'F'*/+PanSeq[1]/*无PanSeq填'FF'*/
#define BLACK_HASH_INDEX_NUM			0  		// 黑名单Hash表个数
#define BLACK_MAX_NUM					0   	// 最多黑名单个数

// 终端应用类型
#define TERM_TYPE_GENERAL				0x0000	// 通用类型

// 交易类型
#define TRANS_TYPE_SALE					0x0000 // 消费
#define TRANS_TYPE_DAIRY_SALE			0x0010 // 日结消费
#define TRANS_TYPE_SALEVOID				0x0020 // 消费撤销
#define TRANS_TYPE_REFUND				0x2220 // 退货
#define TRANS_TYPE_BALANCE				0x2031 // 余额查询
#define TRANS_TYPE_PREAUTH              0x1003 // 预授权
#define TRANS_TYPE_PREAUTHVOID          0x1020 // 预授权撤销
#define TRANS_TYPE_PREAUTH_COMP         0x2000 // 预授权完成
#define TRANS_TYPE_PREAUTH_COMPVOID     0x2020 // 预授权完成撤销

#define TRANS_TYPE_ICSALE               0x0100 // 插卡消费
#define TRANS_TYPE_NFCPINSALE			0x0101 // 闪付凭密消费
#define TRANS_TYPE_NFCPINPREAUTH			0x0102 // 闪付凭密预授权
#define TRANS_TYPE_MOBILESALE			0x0104 // 手机PAY

#define QRTRANS_TYPE_SALE				0x8001	// 扫码消费
#define QRTRANS_TYPE_VOID				0x8002	// 扫码消费撤销
#define QRTRANS_TYPE_REFUND				0x8003	// 扫码退款
#define QRTRANS_TYPE_QUERY				0x8004    // 支付查询
//#define QRTRANS_TYPE_PARTREFUND			0x8004	// 扫码部分退货

#define SCAN_TRANS_TYPE_PREAUTH 					0x8005	        // 扫码预授权
#define SCAN_TRANS_TYPE_PREAUTHVOID				0x8006	       // 扫码预授权撤销
#define SCAN_TRANS_TYPE_PREAUTH_COMP				0x8007	      // 扫码预授权完成

#pragma pack(1)
// 系统参数
typedef struct {

	//TMS参数
   	 uchar ucTmsAccMode;         //TMS访问方式
	uchar szTmsHost[40+1];
	ushort uiTmsHostPort;
   	 uchar szTmsHostBak[40+1];
	ushort uiTmsHostPortBak;
	uchar szTmsIP1[15+1];  
	uchar szTmsIP2[15+1];
	uchar sTmsTPDU[5];
	uchar  ucTmsDomainOrIp;                // 域名或IP: 1-域名 0-IP
	
	uchar szParamVer[20+1];				// 参数版本, TMS下载的参数版本

	//后台通讯参数
	uchar szSvrIp[15+1];
	ushort uiSvrPort;
	uchar szBakSvrIp[15+1];
	ushort uiBakSvrPort;
	uchar sTPDU[5];
        uchar sQrTPDU[5];
			
	// 必须输入参数
	uchar  szPosId[8+1];				// 终端号
	uchar  szMerchId[15+1];				// 商户号
	uchar  szMerchName[40+1];			// 商户名
	uchar  szAdvInfo[1];                       //广告语

	int    iCommType;			        // 网络类型, VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_WIFI

	uchar  szWifiName[40+1];
	uchar  szWifiNameUtf8[40+1];
	uchar  szWifiPwd[20+1];

	uchar  ucMasterKeyIndex;			// IC卡本机终端主密钥索引
	uchar  sMasterKey[16];				// 终端主密钥
        uchar  sMasterKeyCiphertext[16];				// 终端主密钥密文
        
	uchar szQrTranUrl[150];

	uchar   ucKeyBeep ;                  // 0-无按键音 1有按键音
	uchar szSysVer[20+1];          		//账户名改为系统版本号,用于定义当前sysinfo和sysdata结构体或初始值版本信息,初始化时填写

	uchar  ucPosAppType;				// N2, POS终端应用类型
	ushort uiCommTimeout;               // 通讯响应超时时间(秒)
	ushort uiConnectTimeout;                       // 连接超时
	ushort  ucCommRetryCount;			// 交易重试次数,
	ushort uiUiTimeOut;                               //操作超时时间
	uchar  ucAutoLogoutFlag;			// 是否自动签退, 1-自动 0-不自动
	ushort ucMaxResendCount;			// 冲正重发次数
	uchar  sTransSupport[4];			// B32, 支持的交易类型

	//特定交易是否需要持卡人刷卡、输入密码
	uchar  ucSaleVoidCardFlag;			// 消费撤销是否需要刷卡
	uchar  ucAuthCompleteVoidCardFlag;			// 预授权完成撤销是否需要刷卡

	uchar  ucSaleVoidPinFlag;			// 消费撤销是否需要密码
	uchar  ucAuthVoidPinFlag;			// 预授权撤销是否需要密码
	uchar  ucAuthCompVoidPinFlag;		// 预授权完成撤销是否需要输入密码
	uchar  ucAuthCompPinFlag;			// 预授权完成是否需要输入密码

	// 其它参数
	ushort uiAidNum;					// 终端支持的AID个数
	ushort uiCaKeyNum;					// 终端支持的CA公钥个数�
	uchar  ucClssParamLoad;                            //非接参数是否已经下载
	uchar  ucTermTestMode;				// 测试状态标志, 0:正常状态 1:测试状态

	uchar  szSysOperPwd[8+1];           // 系统管理员密码(99操作员)
	uchar  szMainOperPwd[6+1];          // 主管员密码
	uchar  szSafeSetPwd[6+1];			// 安全设置密码,手工设置参数使用

	uchar  ucPrinterAttr;				// 打印属性, 0:无打印机或不打印 1:打印1张单据 2:打印2张单据 3:打印3张单据 4:套打单据

	short  uiAutoUploadNum;				// 自动上送笔数
	ushort uiPositTime;					// 定位时间间隔(分钟)

	//电子签名参数
	uchar  ucSupportESign;				// 是否支持电子签名
	ushort uiESignSendCount;			// 签名重试次数
	ushort uiESignTimeout;				// 电子签名等待时间
	uchar  szESignSvrIp[15+1];			// 电子签名上传服务器地址
	ushort uiESignSvrPort;
	uchar  szESignBakSvrIp[15+1];			// 电子签名上传服务器地址
	ushort uiESignBakSvrPort;
	
	//非接
	uchar ucNoPinFlag;					// 1-支持 0-不支持免密
	ulong  ulNoPinLimit;		                       // 免密限额
	ulong  ulNoSignLimit;	                      // 免签限额
	uchar ucNoSignFlag;					// 0-不支持 1-支持免签
	ulong ulFlashMaxNum;	
	ulong ulFlashProcPerTransTime;  //处理单笔闪卡时间T1
	ulong ulFlashReadCardInfoTime;  //读卡数据时间T3
        ulong ulFlashExpire; //有效时间T2
	uchar ucOnlinefirst;     //是否打开消费借贷记联机优先
	uchar ucBINAFlag;
	uchar ucBINBFlag;
	uchar ucCDCVMFlag;
	
	uchar  ucLoadAid;					// 下载AID标志, 0:未下载 1:已下载
	uchar  ucLoadCaPubKey;				// 下载CA公钥标志, 0:未下载 1:已下载

	uchar	ucCanRefund;				// 是否允许退货

	uchar   szApn[16+1];
	uchar   szApnUser[16+1];
	uchar   szApnPwd[16+1];
	uchar  ucNeedApnUser;

	uchar  ucWireLongLink;              // 1-无线长连接
	uchar  ucDomainOrIp;                // 域名或IP: 1-域名 0-IP
	uchar szSvrDomain[27+1];            //服务器域名
	uchar szBakSvrDomain[27+1];			//备用服务器域名

	uchar  szSimPin[8+1];               // sim卡密码

	uchar  ucQrPreViewFlag;				// 扫码预览: 0-关闭 1-开启

	uchar ucSaleSwitch;
	uchar ucVoidSwitch;
	uchar ucRefundSwitch;
	uchar ucBalanceSwitch;
	uchar ucAuthSwitch;
	uchar ucAuthCancelSwitch;
	uchar ucAuthCompleteSwitch;
	uchar ucAuthCompletenoticeSwitch;
	uchar ucAuthCompleteVoidSwitch;
	uchar ucSignNameSwitch;	

	uchar ucQrSaleSwitch;
	uchar ucQrRefundSwitch;
	uchar ucQrVoidSwitch;
	uchar ucQrPreAuthSwitch;
        uchar ucQrPreAuthCancelSwitch;
	uchar ucQrAuthCompleteSwitch;  

	uchar ucSettlePrintDetail; 

	uchar ucMainOperPwd;
	uchar ucHandCard;

	uchar ucPrintAcquirer;
	uchar ucPrintIssuer;
	uchar ucPrintTitle[1];

	uchar ucPrintEN;
	ushort ucMaxTradeCnt;
        uchar ucFailSignMax;				// 失败电签存储笔数,默认50
        
	ulong ulRefund_limit;
	uchar ucDefaultTrans;
	uchar ucPrtNegative;
	uchar ucAuthCardMask;

	uchar  ucNFCTransPrior;	

	uchar proc_req;	
	uchar ucNeedPin;

	uchar ucSmFlag;
	uchar magEncrypt;
 	uchar  ucAmtInPutMode;              // 金额输入方式: 0-分为单位 1-元为单位
 	
	uchar ucByPass;//是否允许bypass
	uchar ucQrView;//是否扫码预览
	uchar ucDoReverseFlag;//是否立即冲正
	uchar ucCleanReverseFlag;//是否开启隔日删除冲正
	uchar ucTimeCleanReverse[1];//隔日删除冲正时间
	uchar ucPrintMerchantQrFlag;                                            //是否打印商户联广告二维码
	uchar ucPrintMerchantMsgFlag;                                          //是否打印商户联广告信息
	uchar ucPrintMerchantQr[1];                                            //商户联打印广告二维码=https://a.ruiyinxin.com/adms/ext/qrcode/?p=1
	uchar ucPrintMerchantMsg[1];                                            //商户联打印广告文字=【扫一扫】下载瑞易生活、交易查询、结算查询、秒办信用卡...
 	uchar ucPrintCardholderQrFlag;  					//是否打印持卡人联广告二维码=0
 	uchar ucPrintCardholderMsgFlag;   					//是否打印持卡人联广告信息=0
 	uchar ucPrintCardholderQr[1];					//持卡人联打印广告二维码=https://a.ruiyinxin.com/adms/ext/qrcode/?p=1
 	uchar ucPrintCardholderMsg[1]; 					//持卡人联打印广告文字=【扫一扫】下载瑞易生活、交易查询、结算查询、秒办信用卡...
 	uchar ucServiceHotline[1];					       //服务热线=4001177777

        //和融通
        uchar ucMKFlag;                                                                        //主密钥更新标志
        uchar ucDepositFlag;                                                                 //设备是否已收押金
        uchar ucDepositAmount[12+1];                                                          //首笔押金金额
        uchar ucActivityFlag;                                                              //设备是否已激活
        uchar ucFirstSwipeAmount[12+1];                                                           //刷卡首笔最低金额
        uchar ucQrAmount[12+1];                                                                //扫码主扫最低金额
        uchar ucScanAmount[12+1];                                                           //扫码被扫最低金额
        uchar ucHuabeiAmount[12+1];                                                       //花呗分期交易最低金额
        uchar ucSwipeAmount[12+1];                                                       //刷卡最低金额
        uchar ucGprsAmount[12+1];                                                         //使用GPRS模块，会在下月交易中心扣取xx元流量费
        uchar ucGprsAllowFlag;                                                               //是否允许GPRS通讯方式
        uchar ucGprsPayFlag;                                                                 //是否已经缴过GPRS费用
        uchar ucGprsUseFlag;                                                                 //是否已经使用过GPRS通讯方式交易

	uchar ucMerchantQr[100];    
	uchar ucMerchantMsg[100]; 

	uchar ucGprsFlag;                                                                       // 1.GPRS 0.WIFI

	uchar ucHbfqFlag;										  //后台下发分期的标志
        uchar ucHbfqCount;									 //花呗分期个数
	uchar ucHbfqInfo[36+1];								//花呗分期信息4*9
	uchar  ucPowerOffSwitch;           				               // 自动关机: 1-永不 0-30分钟, 默认0
	uchar ucTimeTms[8+1];								//TMS时间
	uchar  sMem[15];

	ulong  ulInitFlag;                  // 0x55AA5AA5 表示终端已经初始化过
} stSysInfo;

// 结算结构
typedef struct {
	ulong ulDebitCount;					// IC卡借记笔数
	ulong ulDebitTotal;					// IC卡借记总金额
	ulong ulCreditCount;				// IC卡贷记笔数
	ulong ulCreditTotal;				// IC卡贷记总金额
	ulong ulBatchNo;					// 计算批次号
	uchar sDateTime[7];					// 结算时间(YYYYMMDDhhmmss)
	uchar sAccountDate[2];				// 清算日期(MMDD)
} stSettle;

typedef struct {

	ulong  ulBatchNo;					// 批次号
	uchar  sOper[1];					// 操作员
	uchar  sDateTime[6];

	uchar  ucSettleUnEven;				// 对账平 0-平 1-不平

	uint   iSaleNum;
	ulong  ulSaleAmt;
	
        uint   iSaleVoidNum;
	ulong  ulSaleVoidAmt;
	
	uint   iRefundNum;
	ulong  ulRefundAmt;
	
	uint   iAuthCmpNum;
	ulong  ulAuthCmpAmt;

       	uint   iAuthCmpVoidNum;
	ulong  ulAuthCmpVoidAmt;
	
        uint iWetchatScanSaleNum;
	 uint iWetchatQrSaleNum;
	 uint iWetchatSaleVoidNum;
	 uint iWetchatSaleRefundNum;
	 uint  iWetchatScanPreauthNum;
	 uint  iWetchatQrPreauthNum;
	 uint iWetchatPreauthCompNum;
	 uint iWetchatPreauthVoidNum;
         uint iAlipayScanSaleNum;
	 uint	iAlipayQrSaleNum;
	uint	iAlipaySaleVoidNum;
	uint	iAlipaySaleRefundNum;
	uint  iAlipayScanPreauthNum;
	uint iAlipayQrPreauthNum;
	uint iAlipayPreauthCompNum;
	 uint iAlipayPreauthVoidNum;	  
         uint iUnionpayScanSaleNum;
         uint iUnionpayQrSaleNum;
	  uint iUnionpaySaleVoidNum;
	  uint iUnionpaySaleRefundNum;
	  uint iUnionpayScanPreauthNum;
	  uint iUnionpayQrPreauthNum;
	  uint iUnionpayPreauthCompNum;
	  uint iUnionpayPreauthVoidNum;  

         ulong ulWetchatScanSaleAmt;
	ulong ulWetchatQrSaleAmt;
	ulong  ulWetchatSaleVoidAmt;
	ulong  ulWetchatSaleRefundAmt;
	ulong  ulWetchatScanPreauthAmt;
	ulong  ulWetchatQrPreauthAmt;
	ulong  ulWetchatPreauthCompAmt;
	ulong  ulWetchatPreauthVoidAmt;
         ulong ulAlipayScanSaleAmt;
         ulong ulAlipayQrSaleAmt;
         ulong ulAlipaySaleVoidAmt;
        ulong  ulAlipaySaleRefundAmt;
         ulong ulAlipayScanPreauthAmt;
        ulong  ulAlipayQrPreauthAmt;
         ulong ulAlipayPreauthCompAmt;
         ulong ulAlipayPreauthVoidAmt;
        ulong ulUnionpayScanSaleAmt;
       ulong  ulUnionpayQrSaleAmt;
        ulong ulUnionpaySaleVoidAmt;
        ulong ulUnionpaySaleRefundAmt;
        ulong ulUnionpayScanPreauthAmt;
        ulong ulUnionpayQrPreauthAmt;
        ulong ulUnionpayPreauthCompAmt;
        ulong ulUnionpayPreauthVoidAmt;

         uint iScanSaleNum;
	 uint iQrSaleNum;

	ulong ulScanSaleAmt;
	ulong ulQrSaleAmt;
	 
	uint   iDebitNum;  //借记
	ulong  ulDebitAmt;

	uint   iCreditNum; //贷记
	ulong  ulCreditAmt;
} stSettleInfo;

// 系统数据
typedef struct {
	ulong  ulTTC;						// 交易流水号
	ulong  ulBatchNo;                   // 交易批次号

	uchar  ucTmsDebugMode;				// TMS诊断模式 1开 0关

	ushort	uiTimeOut;
	ushort uiTransNum;					// 交易笔数
	ushort uiTransUpload;				// 已上送的交易笔数

	ushort uiQrTransNum;				// 扫码交易笔数

	uchar  szCurOper[2+1];				// 当前签到操作员号, "\x00\x.."表示无操作员签到
	uchar  ucPosLoginFlag;					// 终端签到标志, 0:未签到 1:已经签到
	uchar  sLoginTime[7];				// 终端签到时间
	uchar  sSettleTime[7];				// 上次结算时间
	uchar  ucNeedReLogin;				// 需重新签到标志, 0:不需要 1:需要
	uchar  ucOperPauseMode;				// 操作员状态, 0:正常状态 1:暂退状态2:锁定状态
	uchar  sPinKey[16];					// Pin密钥
	uchar  sMacKey[16];             		// 工作密钥
	uchar  sMagKey[16];             		// 磁道加密密钥

	uchar  ucFailSignNum;               // 失败电签笔数
	uchar  ucSettleFlag;                // 终端结算标志, 0:未在结算中 1:正在结算中(结算报文未发送) 2:正在结算中(结算单据未打印) 3:清除交易记录中
	uchar  ucSettleListPos;				// 最后结算信息存储位置, 0-(最多次数-1)
	// 0xFF 表示没有存储过任何历史结算数据
	
	//tms下载app,断点续传信息
	uchar  ucTmsDownloadFlag;			// 下载标志,是否需要更新下载
	ulong  ulTmsTaskId;
	uchar  sTmsTaskId[8];				// 瑞银信使用sTmsTaskId，不使用ulTmsTaskId
	ulong  ulTmsDlStart;				// app下载起始位(断点续传)
	ulong  ulTmsNewLen;					// 待下载app长度
	uchar  szTmsNewVer[20+1];			// 待下载的app新版本号

	uchar  sNewAppMd5[16];              //待下载的app md5值
   	short  iTaskResult;                 //任务结果
    
    	uchar sTmsDate[4];                  //最近一次TMS时间,跨日时需先TMS
    	
	uchar  sMem[64];									

	ushort uiCrc;						// 检查和

	ulong num;
	ulong  ulLastTransTTC;						// 末笔交易流水号
	uchar ucPrintErr;                                     //0 末笔打印正常 1 银行卡末笔打印异常 2扫码交易末笔打印异常
	
} stSysData;

// 交易记录
typedef struct {
    ushort uiTransType;                 // 交易类型
	ulong  ulTTC;                       // Pos终端流水号
	uchar  ucVoidFlag;					// 0x00表示没有被取消, 0x01表示已经被取消
	uchar  ucSignupload;				// 0x00需上送 0x01-0x03上送失败次数 0xFF-已上送或无需上送 这是上送电签的标识
	uchar  ucUploadFlag;				// 0x01表示需上送, 0x02表示已上送, 0xFF为联机交易不需上送
	uchar  ucSettSignupload;                      //结算上送标志
    uchar  sPan[10];                    // 主账号, CN, 后补F
    uchar  ucPanSerNo;                  // 主账号序列号, 0xFF表示无此项
	ulong  ulAmount;					// 交易金额
	ulong  ulBalance;					// 交易后余额
    
	ulong  ulVoidTTC;					// 被取消交易的票据号(也就是原TTC)
    uchar  sDateTime[6];                // 交易日期时间, YYMMDDhhmmss
	uchar  sReferenceNo[12];			// 交易参考号, 联机交易存在
	uchar  sAuthCode[6];
	ushort uiEntryMode;					// 域 22 服务点输入方式码			
	uchar  sOperNo[1];                  // CN格式的2位操作员号, 0xFF表示无操作员签到
	uchar  ucTransAttr;					// 交易属性, 该笔交易额外属性
										//		b8-b3 : 保留
										//      b3    : 0:电签        1:纸签(电签POS表示电签不成功)
										//      b2    : 0:不需要签字   1:需要签字
	                                    //		b1    : 0:消费成功交易 1:闪卡嫌疑消费交易 (其它交易无效)
	ushort uiIcDataLen;                 // IC卡数据长度
	uchar  sIcData[260];                // IC卡数据, TLV格式
	uchar  sVoidInfo[6+2];				// 退货时存放输入的原交易参考号[6](BCD)和日期[2](MMDD), 预授权类撤销和完成交易存放输入的原授权码

	uchar sPrtPosId[8+1];                                 //打印替换的终端号
	uchar  sPrtMerchId[15+1];				// 打印替换的商户号
    uchar  szPrtMerchName[1];	    // 打印替换的商户名
    
    uchar  ucSaleType;                  // 1-日结消费 0-普通消费
    uchar  sSettleDate[2];              // 清算日期
    	byte	issuerBankId[8+1];	//发卡行号
	byte	recvBankId[8+1];		//收单行号
	uchar  ucCardClass;                 //卡类型: 0-国内借记卡 1-国内贷记卡
	uchar  sCardExpire[2];              // 卡有效期yymm	
	uchar ucScriptFlag;
	uchar  gl_uc5f20[26+1];
	uchar  ucMobileFlag;             //手机pay标志
} stTransRec;

typedef struct {
    ushort  uiTransType;                 // 交易类型 1-消费 2-撤销 3-退货 4-部分退货
		ulong  ulTTC;                       // Pos终端流水号TTC
		uchar  ucVoidFlag;					// 0x00表示没有被取消, 0x01表示已经被取消,0x02表示已经被完成(预授权)

		ulong  ulAmount;					// 当前交易金额
		ulong  ulVoidTTC;					// 被取消的原TTC
		uchar  szVoidOrderId[60+1];      	      // 被取消的原支付渠道订单号
		uchar  ucPayWay;					// 支付类型(支付渠道)//选择交易模式 微信、支付宝、银联二维码    

		uchar  sDateTime[6];                // 交易时间YYMMDDhhmmss
		uchar  szAccount[31+1];				// 付款账号
		uchar  sOperNo[1]; 					// CN格式的2位操作员号, 0xFF表示无操作员签到
		uchar  sReferenceNo[12+1];			// 交易参考号, 联机交易存在
		uchar szchnlUrl[200+1];                          //URL DF28  
		uchar  szchnlQrCode[30+1];      	      // 二维码              
 		
		uchar ucProType;					    //标志状态  (0x01表示支付成功,0x02.未支付(对二维码支付有效))
		uchar ucOldProType;
		uchar ucPayMode;					    //	主扫/被扫

		uchar sPrtPosId[8+1];                                 //打印替换的终端号
		uchar  sPrtMerchId[15+1];				// 打印替换的商户号
		uchar  szPrtMerchName[40+1];	    // 打印替换的商户名

	        uchar orderType[2+1];                  	 //支付通路
                uchar orderId[48+1];      	      	       // 订单编号    DF27          
                uchar ucPayAccount[48+1];              //付款账号     DF29
                uchar ucPayMethod[48+1];              //付款方式   DF30
                uchar ucPayCard[19+1];                  //付款卡号   DF31
                uchar issuerBankId[10+1];       	//发卡行号    DF32
	        uchar recvBankId[10+1];	       //收单行号     DF33
	        uchar ucPayVoucher[10+1];          //付款凭证号 DF34


				
		uchar orderDesc[1];  		//订单描述
		uchar orderMsg[1];           //订单返回码描述
		uchar orderRemark[1];    //订单备注
		uchar organName[1];      //收单机构
		uchar couponAmt[1];      //优惠金额
		uchar issName[1];           //发卡行

		uchar ucHubei;
		uchar ucHubeiFq;
		uint    iHubeiNum;
		
		
} stQrTransRec;

// 运行数据
typedef struct {
	ulong  ulFreeTimer;					// 空闲计时器, 超时表示已经达到空闲时间
	ulong  ulEnvRefreshTimer;			// 重置显示环境计时器
	ushort uiHostTryTime;				// 连接后台服务连续失败次数(超过3次报警提示)
} stRunTimeData;

// BIN表B
typedef struct {
	uchar  ulCardNum[2];					
        uchar  ucCardList[1];
} stCardBinb;

// BIN表黑名单
typedef struct {
	uchar  szDate[4];	
	uchar  ulCardNum[2];
        uchar  ucCardList[1];
} stCardclblack;
#pragma pack()

extern stSysInfo       gl_SysInfo;			// 系统信息，设定的参数
extern stSysData       gl_SysData;			// 系统数据
extern stTransRec      gl_TransRec;			// 交易记录
//extern stRunTimeData   gl_RTD;				// 运行时数据

extern st8583          gl_Send8583;			// 发送8583结构
extern st8583          gl_Recv8583;			// 接收8583结构
extern uchar           gl_ucNFCPinFlag;     //闪付凭密标志
extern uchar           gl_uc9f6c[2+1];     
extern uchar           gl_ucOutsideCardFlag;
extern uchar           gl_ucStandbyCardFlag; //在待机界面读卡了就不需要再读卡
extern uchar          gl_ucSPan[19+1]; //手输卡号
extern uchar          gl_ucTimeOutFlag; //99 00 是否超时退出
extern uchar          gl_ucProcReqFlag;//判断报文头是否有处理要求
extern uchar          gl_ucSignuploadFlag; //电子签名上送交易标识
extern uchar          gl_ucQrTransFlag; //扫码交易标识
extern uchar          gl_ucMobileTransFlag; //手机pay交易标识
extern uchar          gl_ucAppUpdate;        //TMS应用下载更新标志
extern char          gl_baseStation[100];  //wifi情况下获取基站定位
extern stCardBinb        gl_CardBinb;  
extern stCardclblack        gl_Cardclblack;  
#endif
