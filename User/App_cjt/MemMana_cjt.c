// 扩展内存存储管理
// 目标: 将扩展内存操作与应用隔离, 以适应各种存储方式
// 本模块适用于支持标准VposFace.h扩展内存的终端
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "VposFace.h"
#include "Pub.h"
#include "debug.h"

#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"

static uchar  sg_ucRes8583Exist;		// Res8583存在标志
static uchar  sg_ucScript8583Exist;		// Script8583存在标志
#define RevMSG_Judge_LEN    40

// 建立扩展内存管理环境
// ret : 0 : OK
//       1 : 未初始化或扩展内存错误
uint uiMemManaSetEnv(void)
{
	uchar tmp[RevMSG_Judge_LEN];
	uint uiRet;
    int i;

	uiRet = _uiCheckXMem(1);
	if(uiRet)
		return uiRet;
	//dbg("_ulSetXMemSize tk=%lu\n", _ulGetTickTime()-tk1);

	_uiXMemRead(MEM_SYSINFO, 0, &gl_SysInfo, sizeof(gl_SysInfo));
	_uiXMemRead(MEM_SYSDATA, 0, &gl_SysData, sizeof(gl_SysData));
	_uiXMemRead(MEM_CARDBLACK, 0, &gl_Cardclblack, sizeof(gl_Cardclblack));
//	dbg("gl_Cardclblack.ulCardNum:%02x   %02x",  gl_Cardclblack.ulCardNum[0], gl_Cardclblack.ulCardNum[1]);
       dbgHex("gl_Cardclblack.ulCardNum:",  gl_Cardclblack.ulCardNum,2);
        dbgHex("gl_Cardclblack.ucCardList",  gl_Cardclblack.ucCardList,  (gl_Cardclblack.ulCardNum[0] * 256 + gl_Cardclblack.ulCardNum[1] )*12);
	// Res8583
	_uiXMemRead(MEM_REV8583, 0, tmp, sizeof(tmp));
	for(i=0; i<sizeof(tmp) && tmp[i]==0x00; i++)
		;
	if(i>=sizeof(tmp))
		sg_ucRes8583Exist = 0;
	else
		sg_ucRes8583Exist = 1;

	// Script8583
	_uiXMemRead(MEM_SCR8583, 0, tmp, sizeof(tmp));
	for(i=0; i<sizeof(tmp) && tmp[i]==0x00; i++)
		;
	if(i>=sizeof(tmp))
		sg_ucScript8583Exist = 0;
	else
		sg_ucScript8583Exist = 1;

	//dbg("uiMemManaSetEnv total tk=%lu\n", _ulGetTickTime()-tk2);

	return(0);
}

// 首次扩展内存管理初始化
// ret : 0 : OK
//       1 : error
// Note: 只能在终端完全初始化时执行一次
//       gl_SysInfo, gl_SysData必须先填好再调用, 初始化后要建立初始环境
uint uiMemManaInit(void)
{
	st8583 Tmp8583;
	stSettleInfo sett;    
	stCardclblack TmpCardclblack;
	//stTransRec TmpTrans;
    int i;

	_vCls();
	vDispMid(2, "初始化数据");
	vDispMid(3, "请稍候...");
	//_uiGetKey();

    if(MAX_TRANS_NUM>TRANS_FILE_SIZE*TRANS_FILE_NUM)
		return(1);
    
	//建立目录和文件
	if(_uiCheckXMem(0))
		return 1;

	//按最大容量初始化各文件
	dbg("uiMemManaPutSysInfo:%d\n", sizeof(gl_SysInfo));
	if(uiMemManaPutSysInfo())
		return 1;
	dbg("uiMemManaPutSysData:%d\n", sizeof(gl_SysData));
	if(uiMemManaPutSysData())
		return 1;
	dbg("uiMemManaEraseOperList:%d\n", MAX_OPER_NUM*OPER_REC_SIZE);
	if(uiMemManaEraseOperList())
		return 1;
	
	dbg("Aids size:%d\n", AID_REC_SIZE*MAX_AID_NUM);
	for(i=0; i<MAX_AID_NUM; i++)
	{
		dbg("uiMemManaPutAid %d/%d\n", i, MAX_AID_NUM);
		if(uiMemManaPutAid(i, "", 0))
			return 1;
	}

	dbg("pubkeys size:%d\n", PUBK_REC_SIZE*MAX_CA_KEY_NUM);
	for(i=0; i<MAX_CA_KEY_NUM; i++)
	{
		dbg("uiMemManaPutCaPubKey %d/%d\n", i, MAX_CA_KEY_NUM);
		if(uiMemManaPutCaPubKey(i, "", 0))
		{
			return 1;
		}
	}

	//清文件
	_vClearTransFile();
	/*
	dbg("transrec size:%d\n", sizeof(stTransRec)*MAX_TRANS_NUM);
	memset(&gl_TransRec, 0x00, sizeof(gl_TransRec));
	for(i=0; i<MAX_TRANS_NUM && i<10; i++)			//为加快速度,只初始化前10笔
	{
		dbg("_uiXMemWrite stTransRec %d/%d\n", i, MAX_TRANS_NUM);
		if(uiMemManaPutTransRec(i, &gl_TransRec))
		{
			return 1;
		}
	}
	*/
		
	dbg("8583 size:%d\n", sizeof(st8583));
	memset(&Tmp8583, 0x00, sizeof(Tmp8583));
	dbg("_uiXMemWrite MEM_REV8583\n");
	if(_uiXMemWrite(MEM_REV8583, 0, &Tmp8583, sizeof(st8583)))
		return 1;
	dbg("_uiXMemWrite MEM_SCR8583\n");
	if(_uiXMemWrite(MEM_SCR8583, 0, &Tmp8583, sizeof(st8583)))
		return 1;
	memset(&sett, 0x00, sizeof(sett));
	if(_uiXMemWrite(MEM_SETTLEINFO, 0, &sett, sizeof(sett)))
		return 1;
	dbg("_uiXMemWrite MEM_CARDBLACK\n");
	dbg("stCardclblack size:%d\n", sizeof(stCardclblack));
	memset(&TmpCardclblack, 0x00, sizeof(TmpCardclblack));
	if(_uiXMemWrite(MEM_CARDBLACK, 0, &TmpCardclblack, sizeof(stCardclblack)))
		return 1;
    uiMemClearAllJbg();
    
    //uiMemManaEraseSettleList();
	dbg("init end.\n");

	uiMemManaSetEnv();
	return(0);
}

// 获取存储类型
// ret : 0 : 正常存储类型
//       1 : Flash存储类型
uint uiMemGetMemType(void)
{
    return(0);
}

////////// 系统参数 //////////
// 保存系统参数
// ret : 0 : OK
//       1 : 出错
uint uiMemManaPutSysInfo(void)
{
	return _uiXMemWrite(MEM_SYSINFO, 0, &gl_SysInfo, sizeof(gl_SysInfo));
}

// 读取系统参数
// ret : 0 : OK
//       1 : 出错
#if 0
必须调用uiMemManaSetEnv()函数获取初始值
uint uiMemManaGetSysInfo(void)
{
}
#endif

////////// 系统数据 //////////
// 保存系统数据
// ret : 0 : OK
//       1 : 出错
uint uiMemManaPutSysData(void)
{
	return _uiXMemWrite(MEM_SYSDATA, 0, &gl_SysData, sizeof(stSysData));
}
// 读取系统数据
// ret : 0 : OK
//       1 : 出错
#if 0
必须调用uiMemManaSetEnv()函数获取初始值
uint uiMemManaGetSysData(void)
{
}
#endif

////////// 保留8583 //////////
// 保存保留8583数据
// in  : iFlag : 0 : 冲正/重发8583
//               1 : 脚本上送8583
// ret : 0 : OK
//       1 : 出错
uint uiMemManaPut8583(int iFlag, st8583 *p8583)
{
	if(iFlag == MSG8583_TYPE_REV) {
		// Res8583
		if(sg_ucRes8583Exist)
			return(1);
		sg_ucRes8583Exist = 1;
		_uiXMemWrite(MEM_REV8583, 0, p8583, sizeof(st8583));
	} else {
		// Script8583
		if(sg_ucScript8583Exist)
			return(1);
		sg_ucScript8583Exist = 1;
		_uiXMemWrite(MEM_SCR8583, 0, p8583, sizeof(st8583));
	}
	return(0);
}
// 读取保留8583数据
// in  : iFlag : 0 : 冲正/重发8583
//               1 : 脚本上送8583
// ret : 0 : OK
//       1 : 无冲正
uint uiMemManaGet8583(int iFlag, st8583 *p8583)
{
	uint uiRet;
	uiRet = uiMemManaIfExist8583(iFlag);
	if(uiRet == 0)
		return(1);
    memset(p8583, 0, sizeof(st8583));
	if(iFlag == MSG8583_TYPE_REV) {
		// Res8583
		_uiXMemRead(MEM_REV8583, 0, p8583, sizeof(st8583));
	} else {
		// Script8583
		_uiXMemRead(MEM_SCR8583, 0, p8583, sizeof(st8583));
	}
	return(0);
}
// 判断是否存在报文
// in  : iFlag : 0 : 冲正/重发8583
//               1 : 脚本上送8583
// ret : 0 : 无
//       1 : 有
uint uiMemManaIfExist8583(int iFlag)
{        
	if(iFlag==MSG8583_TYPE_REV && sg_ucRes8583Exist)
		return(1);
	else if(iFlag==MSG8583_TYPE_SCR && sg_ucScript8583Exist)
		return(1);
	return(0);
}
// 清除8583报文
// in  : iFlag : 0 : 冲正/重发8583
//               1 : 脚本上送8583
// ret : 0 : OK
uint uiMemManaErase8583(int iFlag)
{
	uchar tmp[RevMSG_Judge_LEN];
	uint uiRet;
	uiRet = uiMemManaIfExist8583(iFlag);
	if(uiRet == 0)
		return(0);
	memset(tmp, 0x00, sizeof(tmp));
	if(iFlag == MSG8583_TYPE_REV) {
		sg_ucRes8583Exist = 0;
		_uiXMemWrite(MEM_REV8583, 0, tmp, sizeof(tmp));
	} else {	        
		sg_ucScript8583Exist = 0;
		_uiXMemWrite(MEM_SCR8583, 0, tmp, sizeof(tmp));
	}
	return(0);
}

////////// 卡BIN //////////
// 擦除卡BIN
// 同时擦除卡BIN黑/白名单
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseCardBin(void)
{
	return(0); // 不需要擦除
}
// 保存卡BIN 黑/白 名单
// in  : iFlag      : 0:保存白名单 1:保存黑名单
//       iIndex     : 索引, 0开始
// out : pszCardBin : 卡BIN
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutCardBin(int iFlag, int iIndex, char *pszCardBin)
{
	/*
	uint uiRet;
	char sCardBin[CARD_BIN_LEN];
	if(strlen(pszCardBin) > CARD_BIN_LEN)
		return(2);
    memset(sCardBin, ' ', CARD_BIN_LEN);
    memcpy(sCardBin, pszCardBin, strlen(pszCardBin));
	if(iFlag == 0) {
		// 保存白名单
		if(iIndex<0 || iIndex>=MAX_CARD_BIN_WHITE)
			return(2);
		uiRet = _uiXMemWrite(sCardBin, XMEM_OFFSET_CARD_BIN+(ulong)iIndex*CARD_BIN_LEN, CARD_BIN_LEN);
		if(uiRet)
			return(1);
	} else {
		// 保存黑名单
		if(iIndex<0 || iIndex>=MAX_CARD_BIN_BLACK)
			return(2);
		uiRet = _uiXMemWrite(sCardBin, XMEM_OFFSET_CARD_BIN+(ulong)(iIndex+MAX_CARD_BIN_WHITE)*CARD_BIN_LEN, CARD_BIN_LEN);
		if(uiRet)
			return(1);
	}
	*/
	return(0);
}
// 读取卡BIN 黑/白 名单
// in  : iFlag      : 0:读白名单 1:读黑名单
//       iIndex     : 索引, 0开始
// out : pszCardBin : 卡BIN
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetCardBin(int iFlag, int iIndex, char *pszCardBin)
{
	/*
	uint uiRet;
	if(iFlag == 0) {
		// 读取白名单
		if(iIndex<0 || iIndex>=MAX_CARD_BIN_WHITE)
			return(2);
		uiRet = _uiXMemRead(pszCardBin, XMEM_OFFSET_CARD_BIN+(ulong)iIndex*CARD_BIN_LEN, CARD_BIN_LEN);
		if(uiRet)
			return(1);
	} else {
		// 读取黑名单
		if(iIndex<0 || iIndex>=MAX_CARD_BIN_BLACK)
			return(2);
		uiRet = _uiXMemRead(pszCardBin, XMEM_OFFSET_CARD_BIN+(ulong)(iIndex+MAX_CARD_BIN_WHITE)*CARD_BIN_LEN, CARD_BIN_LEN);
		if(uiRet)
			return(1);
	}
    pszCardBin[CARD_BIN_LEN] = 0;
    for(uiRet=0; uiRet<CARD_BIN_LEN; uiRet++)
        if(pszCardBin[uiRet] == ' ')
            pszCardBin[uiRet] = 0;
	*/
	return(0);
}

////////// 结算列表 //////////
// 擦除结算列表
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseSettleList(void)
{
	/*
    stSettle Settle;
    uint uiRet;
    int i;
    memset(&Settle, 0xFF, sizeof(Settle));
    for(i=0; i<MAX_SETTLE_LIST_NUM; i++) {
        uiRet = uiMemManaPutSettleRec(i, &Settle);
        if(uiRet)
            return(1);
    }
	*/
	return(0);
}
// 保存结算记录
// in  : iIndex : 位置, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutSettleRec(int iIndex, stSettle *pSettle)
{
	/*
	uint uiRet;
	if(iIndex<0 || iIndex>=MAX_SETTLE_LIST_NUM)
		return(2);
	uiRet = _uiXMemWrite(pSettle, XMEM_OFFSET_SETTLE_LIST+iIndex*sizeof(stSettle), sizeof(stSettle));
	if(uiRet)
		return(1);
		*/
	return(0);
}
// 读取结算记录
// in  : iIndex : 位置, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetSettleRec(int iIndex, stSettle *pSettle)
{
/*
	uint uiRet;
	if(iIndex<0 || iIndex>=MAX_SETTLE_LIST_NUM)
		return(2);
	uiRet = _uiXMemRead(pSettle, XMEM_OFFSET_SETTLE_LIST+(ulong)iIndex*sizeof(stSettle), sizeof(stSettle));
	if(uiRet)
		return(1);
*/		
	return(0);
}


// 保存结算信息
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutSettleInfo(stSettleInfo *pSettle)
{
	return _uiXMemWrite(MEM_SETTLEINFO, 0, pSettle, sizeof(stSettleInfo));
}
// 读取结算信息
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetSettleInfo(stSettleInfo *pSettle)
{
	return _uiXMemRead(MEM_SETTLEINFO, 0, pSettle, sizeof(stSettleInfo));
}

////////// 交易记录 //////////
// 清除交易记录
// ret : 0 : OK
//       1 : 出错
uint uiMemManaTransRecErase(void)
{
	return(0); // 不需要清除
}
// 保存交易记录
// in  : iIndex : 记录索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutTransRec(int iIndex, stTransRec *rec)
{
	uint uiRet;
	int fileid;
	
	if(iIndex<0 || iIndex>=MAX_TRANS_NUM || MAX_TRANS_NUM>TRANS_FILE_NUM*TRANS_FILE_SIZE)
		return(2);

    //为加快速度,减少文件大小,改为多个文件
    fileid = MEM_TRANSREC_0+iIndex/TRANS_FILE_SIZE;
    
	uiRet = _uiXMemWrite(fileid, (ulong)(iIndex%TRANS_FILE_SIZE)*sizeof(stTransRec), rec, sizeof(stTransRec));
	if(uiRet)
		return(1);
	return(0);	
}
// 读取交易记录
// in  : iIndex : 记录索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetTransRec(int iIndex, stTransRec *rec)
{
	uint uiRet;
	int fileid;
	    
	if(iIndex<0 || iIndex>=MAX_TRANS_NUM || MAX_TRANS_NUM>TRANS_FILE_NUM*TRANS_FILE_SIZE)
		return(2);

    memset(rec, 0, sizeof(stTransRec));
    
    //为加快速度,减少文件大小,改为多个文件
    fileid = MEM_TRANSREC_0+iIndex/TRANS_FILE_SIZE;
    
	uiRet = _uiXMemRead(fileid, (ulong)(iIndex%TRANS_FILE_SIZE)*sizeof(stTransRec), rec, sizeof(stTransRec));
	if(uiRet)
		return(1);
	return(0);
}

// 保存交易记录
// in  : iIndex : 记录索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutQrTransRec(int iIndex, stQrTransRec *rec)
{
	uint uiRet;
	
	if(iIndex<0 || iIndex>=MAX_QRTRANS_NUM)
		return(2);
    
	uiRet = _uiXMemWrite(MEM_QRTRANSREC, (ulong)iIndex*sizeof(stQrTransRec), rec, sizeof(stQrTransRec));
	if(uiRet)
		return(1);
	return(0);	
}
// 读取交易记录
// in  : iIndex : 记录索引
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetQrTransRec(int iIndex, stQrTransRec *rec)
{
	uint uiRet;
	//int fileid;
	    
	if(iIndex<0 || iIndex>=MAX_QRTRANS_NUM)
		return(2);
    memset(rec, 0, sizeof(stQrTransRec));
	uiRet = _uiXMemRead(MEM_QRTRANSREC, (ulong)iIndex*sizeof(stQrTransRec), rec, sizeof(stQrTransRec));
	if(uiRet)
		return(1);
	return(0);
}

////////// 操作员 //////////
// 擦除操作员列表
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseOperList(void)
{
	uchar aOperList[MAX_OPER_NUM][OPER_REC_SIZE];

	memset(&aOperList[0][0], 0xFF, sizeof(aOperList));
	return _uiXMemWrite(MEM_OPER, 0, &aOperList[0][0], sizeof(aOperList));
}
// 保存操作员数据
// in  : iIndex : 操作员索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutOper(int iIndex, void *pOperRec)
{
	int uiRet;
    uchar aOperList[MAX_OPER_NUM][OPER_REC_SIZE];
    
    if(iIndex<0 || iIndex>=MAX_OPER_NUM)
		return(2);
    //return _uiXMemWrite(MEM_OPER, iIndex*OPER_REC_SIZE, pOperRec, OPER_REC_SIZE);
    
    uiRet=_uiXMemRead(MEM_OPER, 0, &aOperList[0], sizeof(aOperList));
    if(uiRet==0)
    {
        memcpy(&aOperList[0][0]+iIndex*OPER_REC_SIZE, pOperRec, OPER_REC_SIZE);
        uiRet=_uiXMemWrite(MEM_OPER, 0, &aOperList[0][0], sizeof(aOperList));
    }
    return uiRet;
}
// 读取操作员数据
// in  : iIndex : 操作员索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetOper(int iIndex, void *pOperRec)
{
	if(iIndex<0 || iIndex>=MAX_OPER_NUM)
		return(2);
    memset(pOperRec, 0, sizeof(OPER_REC_SIZE));
	return _uiXMemRead(MEM_OPER, (ulong)iIndex*OPER_REC_SIZE, pOperRec, OPER_REC_SIZE);
}

////////// EMV终端参数 //////////
// 保存EMV终端参数
// ret : 0 : OK
//       1 : 出错
uint uiMemManaPutEmvPara(stHxTermParam *pHxTermParam)
{
	//return _uiXMemWrite(pHxTermParam, XMEM_OFFSET_EMV_PARAM, sizeof(stHxTermParam));
    return 1;
}
// 读取EMV终端参数
// ret : 0 : OK
//       1 : 出错
uint uiMemManaGetEmvPara(stHxTermParam *pHxTermParam)
{
	//return _uiXMemRead(pHxTermParam, XMEM_OFFSET_EMV_PARAM, sizeof(stHxTermParam));
    return 1;
}

////////// AID //////////
// 擦除AID存储区
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseAid(void)
{
	//for(int i=0; i<MAX_AID_NUM; i++)
	//	uiMemManaPutAid(i, "", 0);
	return 0;
}
// 保存AID
// in  : iIndex : AID索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutAid(int iIndex, uchar *psTermAid, uint len)
{
	uchar tmp[AID_REC_SIZE]={0};

	dbg("put aid:idx=%d, len=%d\n", iIndex, len);

	if(iIndex<0 || iIndex>=MAX_AID_NUM || len+2>AID_REC_SIZE)
		return(2);

	tmp[0]=len/256;
	tmp[1]=len&0xFF;
	memcpy(tmp+2, psTermAid, len);
	//dbg("tmp2=[%02X%02X]\n", tmp[0], tmp[1]);
	return _uiXMemWrite(MEM_AID, (ulong)iIndex*AID_REC_SIZE, tmp, AID_REC_SIZE);
}

uint uiMemManaPutAidStr(int iIndex, uchar *pszTermAid)
{
	uchar tmp[AID_REC_SIZE]={0};
	uint len;

	if(iIndex<0 || iIndex>=MAX_AID_NUM)
		return(2);	
	len=strlen((char*)pszTermAid)/2;
	if(2+len>AID_REC_SIZE)
		return 2;

	tmp[0]=len/256;
	tmp[1]=len&0xFF;
	vTwoOne(pszTermAid, len*2, tmp+2);
	return _uiXMemWrite(MEM_AID, (ulong)iIndex*AID_REC_SIZE, tmp, AID_REC_SIZE);
}

// 读取AID
// in  : iIndex : AID索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetAid(int iIndex, uchar *psTermAid, uint *puiAidLen)
{
	uchar tmp[AID_REC_SIZE]={0};
	uint ret, len;

	if(iIndex<0 || iIndex>=MAX_AID_NUM)
		return(2);
	//dbg("get aid:idx=%d\n", iIndex);
	ret=_uiXMemRead(MEM_AID, (ulong)iIndex*AID_REC_SIZE, tmp, AID_REC_SIZE);
	if(ret)
		return ret;
	//dbg("tmp2=[%02X%02X]\n", tmp[0], tmp[1]);
	if(tmp[0]==0xFF && tmp[1]==0xFF)
	{
		psTermAid[0]=0;
		*puiAidLen=0;
		//dbg("get aid1:idx=%d, len=%d\n", iIndex, *puiAidLen);
		return 0;
	}
	len=tmp[0]*256+tmp[1];
	if(len>AID_REC_SIZE-2)
		return 1;
	memcpy(psTermAid, tmp+2, len);
	*puiAidLen=len;
	//dbg("get aid end:idx=%d, len=%d\n", iIndex, *puiAidLen);
	//dbgHex("get aid", psTermAid, len);
	return 0;
}
uint uiMemManaGetAidStr(int iIndex, uchar *pszTermAid)
{
	uchar tmp[AID_REC_SIZE]={0};
	uint ret, len;

	ret = uiMemManaGetAid(iIndex, tmp, &len);
	if(ret)
		return ret;
	vOneTwo0(tmp, len, pszTermAid);
	return 0;
}

////////// CA公钥 //////////
// 擦除CA公钥存储区
// ret : 0 : OK
//       1 : 出错
uint uiMemManaEraseCaPubKey(void)
{
	//for(int i=0; i<MAX_CA_KEY_NUM; i++)
	//	uiMemManaPutCaPubKey(i, "", 0);
	return 0;
}
// 保存CA公钥
// in  : iIndex : CA公钥索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaPutCaPubKey(int iIndex, uchar *psCaPublicKey, int len)
{
	uchar tmp[PUBK_REC_SIZE]={0};
	
	if(iIndex<0 || iIndex>=MAX_CA_KEY_NUM || 2+len>PUBK_REC_SIZE)
		return(2);

	tmp[0]=len/256;
	tmp[1]=len&0xFF;
	memcpy(tmp+2, psCaPublicKey, len);

	return _uiXMemWrite(MEM_CAPUBKEY, (ulong)iIndex*PUBK_REC_SIZE, tmp, PUBK_REC_SIZE);
}
uint uiMemManaPutCaPubKeyStr(int iIndex, uchar *pszCaPublicKey)
{
	uchar tmp[PUBK_REC_SIZE]={0};
	uint len;
	
	if(iIndex<0 || iIndex>=MAX_CA_KEY_NUM)
		return(2);
	len=strlen((char*)pszCaPublicKey)/2;
	if(2+len>PUBK_REC_SIZE)
		return 2;

	tmp[0]=len/256;
	tmp[1]=len&0xFF;
	vTwoOne(pszCaPublicKey, len*2, tmp+2);
	return _uiXMemWrite(MEM_CAPUBKEY, (ulong)iIndex*PUBK_REC_SIZE, tmp, PUBK_REC_SIZE);
}
// 读取CA公钥
// in  : iIndex : CA公钥索引, 0开始
// ret : 0 : OK
//       1 : 出错
//       2 : 超出范围
uint uiMemManaGetCaPubKey(int iIndex, uchar *psCaPublicKey, uint *puiLen)
{
	uchar tmp[PUBK_REC_SIZE]={0};
	uint ret, len;

	if(iIndex<0 || iIndex>=MAX_CA_KEY_NUM)
		return(2);
	ret=_uiXMemRead(MEM_CAPUBKEY, (ulong)iIndex*PUBK_REC_SIZE, tmp, PUBK_REC_SIZE);
	if(ret)
		return ret;
	if(tmp[0]==0xFF && tmp[1]==0xFF)
	{
		psCaPublicKey[0]=0;
		*puiLen=0;
		return 0;
	}
	len=tmp[0]*256+tmp[1];
	if(len>PUBK_REC_SIZE-2)
		return 1;	
	memcpy(psCaPublicKey, tmp+2, len);
	*puiLen=len;
	return 0;
}
uint uiMemManaGetCaPubKeyStr(int iIndex, uchar *pszCaPublicKey)
{
	uchar tmp[PUBK_REC_SIZE]={0};
	uint ret, len;

	if(iIndex<0 || iIndex>=MAX_CA_KEY_NUM)
		return(2);
	ret=uiMemManaGetCaPubKey(iIndex, tmp, &len);
	if(ret)
		return ret;
	vOneTwo0(tmp, len, pszCaPublicKey);
	return 0;
}

uint uiMemPutJbg(char *pszName, uchar *psData, int iLen)
{
    extern uint _uiXMemWriteJbgFile(char *pszFile, uchar *psData, int iLen);

    return _uiXMemWriteJbgFile(pszName, psData, iLen);
}

uint uiMemGetJbg(char *pszName, uchar *psData, int size, int *piLen)
{
    extern uint _uiXMemReadJbgFile(char *pszFile, uchar *psData, int size, int *piLen);

	return _uiXMemReadJbgFile(pszName, psData, size, piLen);
}

uint uiMemDelJbg(char *pszName)
{
    extern uint _uiXMemDelJbgFile(char *pszFile);
    return _uiXMemDelJbgFile(pszName);
}

uint uiMemClearAllJbg(void)
{
    extern uint _uiXMemClearJbgDir(void);
    return _uiXMemClearJbgDir();
}

/*
////////// 黑名单 //////////
// 黑名单初始化
// in  : iFlag : 0 : 完全初始化, 装机后运行1次
//               1 : 环境初始化, 开机后运行1次
// ret : 0 : OK
//       1 : 出错
// 完全初始化, 终端初始化后只能调用一次
uint uiMemManaBlackInit(int iFlag)
{
	uint uiRet;
	uiRet = uiSetBlackEnv(iFlag, BLACK_MAX_NUM, BLACK_CARD_ID_LEN, XMEM_OFFSET_BLACK, BLACK_HASH_INDEX_NUM);
	if(uiRet)
		return(1);
	return(0);
}
// 检查是否为黑名单
// In  : psBlackCardId : 卡号(二进制格式)
// ret : 0             : 不是黑名单
//       1             : 是黑名单
uint uiMemManaIsBlack(uchar *psBlackCardId)
{
	return(uiIsBlack(psBlackCardId));
}
// 增加黑名单
// In  : psBlackCardId : 卡号(二进制格式)
// ret : 0             : 增加成功
//       1             : 该卡号已经在黑名单之列
//       2             : 黑名单记录已满
//       3             : 出错
uint uiMemManaAddBlack(uchar *psBlackCardId)
{
	return(uiAddToBlackList(psBlackCardId));
}
// 删除黑名单
// In  : psBlackCardId : 卡号(二进制格式)
// Ret : 0             : 删除成功
//       1             : 该卡号不在黑名单之列
//       3             : 出错
uint uiMemManaDelBlack(uchar *psBlackCardId)
{
	return(uiDelFromBlackList(psBlackCardId));
}
// 取当前黑名单个数/作废黑名单个数
// ret : 黑名单个数
ulong ulMemManaGetBlackNum(void)
{
	return(ulGetBlackNum());
}
// ret : 作废黑名单个数
ulong ulMemManaGetVoidBlackNum(void)
{
	return(ulGetVoidBlackNum());
}
*/
