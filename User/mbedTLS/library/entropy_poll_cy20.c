/**
 * Entropy hardware poll function
 *
 *  Copyright (C) 2006-2018, Arm Limited, All Rights Reserved
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may
 *  not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  This file is part of mbed TLS (https://tls.mbed.org)
 */

#if !defined(MBEDTLS_CONFIG_FILE)
 #include "mbedtls/config.h"
#else
 #include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_ENTROPY_HARDWARE_ALT)

#include <string.h>
#include "mbedtls/entropy_poll.h"

#include <stdio.h>
#include <time.h>
#include "mhscpu.h"
#include "rng.h"

extern int rand_hardware_byte_ex(unsigned char *rand, int randLen);
/**
 * Entropy poll callback for a hardware source
 */
int mbedtls_hardware_poll (void *data, unsigned char *output,
                                  size_t len, size_t *olen) {
#if 0
	uint32_t buf[4];
	int i,j;


    TRNG_Start(TRNG0);
	
	for(i=0; i<len; i++)
	{
		while(0 != TRNG_Get(buf, TRNG0))
			;
		for(j=0; j<4 && i<len; j++,i++)
		{
			output[i]=buf[i];
		}
	}

  *olen = len;
  return (0);
	#else
 rand_hardware_byte_ex(output, len);
 *olen = len;
 return (0);
#endif 
}

#endif /* MBEDTLS_ENTROPY_HARDWARE_ALT */

extern int rtc_gettime(struct tm* pstrtm);
time_t time(time_t* timer)
{
	struct tm pgettm;
	time_t tmsec;
	int ret;
	ret=rtc_gettime(&pgettm);
	if(ret){
		//dbg("get rtc error\n");
		return 0;
	}
	tmsec = mktime(&pgettm);
		
	if(timer!=NULL)
		*timer=tmsec;
	return tmsec;
}


