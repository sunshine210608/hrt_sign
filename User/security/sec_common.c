/********************************************************************************

 *************************Copyright (C), 2017, ScienTech Inc.**************************

 ********************************************************************************
 * File Name     : common.c
 * Author        : Jay
 * Date          : 2017-02-08
 * Description   : .C file function description
 * Version       : 1.0
 * Function List :
 * 
 * Record        :
 * 1.Date        : 2017-02-08
 *   Author      : Jay
 *   Modification: Created file

********************************************************************************/
#include <string.h>
#include "debug.h"
#include "misce.h"

void delayms(void)
{
	int i;
	for(i=0;i<0x900;i++)
		;//for(j=0;j<1000;j++);
}

void delayms_ex(int ms)
{
	int i = 0;

	while(i < ms)
	{
		i++;
		delayms();
	}
}

void hexdumps(unsigned char *buf,int len)
{
	int i;
	//dbg("HexDump %d Bytes:\n\t",len);
	if(len>5120) {
		dbg("Data too large %d\n",len);
		return;
	}
	dbg("\t");
	for(i=0;i<len;i++){
		dbg("%02X ",buf[i]);
		if((i+1)%16==0){
			if((i+1)==len)
				dbg("\n");
			else
				dbg("\n\t");
		}
	}
	if(len%16) dbg("\n");
}

/*
int AscByteToHex(unsigned char *Dest, char Src)
{
    if((Dest == 0) || (Src < '0'))
        return -1;

    *Dest = Src - '0';
    if((*Dest > 9) && (*Dest < 17))
        return -1;

    if(*Dest >= 17)
    {
        *Dest -= 7;
        if(*Dest > 0x0f)
            return -1;
    }

    return 0;
}
*/

// "FFFF" -> "\xff\xff"
/*
int AtoHex(unsigned char *Dest, char *Src, int SrcLen)
{
    int i;
    unsigned char Tmp1, Tmp2;

    if((Dest == 0) || (Src == 0))
        return -1;

    for(i=0; i<SrcLen; i+=2)
    {
        if(AscByteToHex(&Tmp1, Src[i]) != 0)
            return -1;

        if(AscByteToHex(&Tmp2, Src[i+1]) != 0)
            return -1;

        Dest[i/2] = (Tmp1 << 4) | Tmp2;
    }

    return 0;
}
*/
// "\xff\xff" -> "FFFF" 
/*
int HextoA(unsigned char *Dest, const unsigned char *Src, int SrcLen)
{
    int i;
    unsigned char Tmp;

    if((Dest == 0) || (Src == 0))
        return -1;

    for(i=0; i<SrcLen; i++)
    {
        Tmp = (Src[i] >> 4) & 0x0f;
        Dest[i * 2] = Tmp + '0';
        if(Tmp > 9)
            Dest[i * 2] += 7;

        Tmp = Src[i] & 0x0f;
        Dest[i * 2 + 1] = Tmp + '0';
        if (Tmp > 9)
            Dest[i * 2 + 1] += 7;
    }

    return 0;
}
*/

unsigned char* Xor(unsigned char *Dest, unsigned char *Src)
{
    int i;
    
    if((Dest == 0) || (Src == 0))
        return 0;
    
    for(i=0; i<8; i++)
        Dest[i] ^= Src[i];

    return Dest;
}

unsigned char* Xor_ex(unsigned char *Dest, const unsigned char *Src, int len)
{
    int i;
    
    if((Dest == 0) || (Src == 0))
        return 0;
    
    for(i=0; i<len; i++)
        Dest[i] ^= Src[i];

    return Dest;
}

/*
int memcmp_ex(const void *buffer1, const void *buffer2, unsigned int count)
{
    unsigned int iLoop;
    int iFlag = 0;
    int iTmpFlag = 0;
 
    for (iLoop = 0; iLoop < count; iLoop ++)
    {
        //if (iFlag == 0)
        //{
            iTmpFlag = *((unsigned char *)buffer1) - *((unsigned char *)buffer2);
            if (iTmpFlag > 0 &&iFlag == 0)
            {
                iFlag = 1;
            }
            else if (iTmpFlag < 0 && iFlag == 0)
            {
                iFlag = -1;
            }else
            {
               
            }
        
        //printf("iLoop = %d\n", iLoop);
        //printf("buffer1 = [%02x], buffer2 = [%02x]\n", *((unsigned char *)buffer1), *((unsigned char *)buffer2));
       
        buffer1 = (unsigned char *)buffer1 + 1;
        buffer2 = (unsigned char *)buffer2 + 1;
    }
    
    return iFlag;
}
*/

unsigned char CalculateLRC(unsigned char *buf, int len)
{
    int i;
    unsigned char ch = 0;

    if(buf == 0)
        return 0;

    for(i=0; i<len; i++)
        ch ^= buf[i];

    return ch;
}


int formatOnlinePin(unsigned char *InputPin, int PinLen, unsigned char *OutFormatPIN)
{
    char PinTmp[16];

    if (PinLen< 4 || PinLen > 12)
    {
        return -1;
    }

    if (InputPin == 0 || OutFormatPIN == 0)
    {

        return -1;
    }

    PinTmp[0] = '0';
    if( PinLen < 10)
    {
        PinTmp[1] = PinLen+0x30;
    }else 
    {
        PinTmp[1] = PinLen+0x37;
    }
    memset(PinTmp + 2, 'F', 14);
    memcpy(PinTmp + 2, InputPin, PinLen);

    if (AtoHex(OutFormatPIN, PinTmp, 16))
    {    
        memset (OutFormatPIN, 0, sizeof(PinTmp));
        memset (PinTmp, 0, sizeof(PinTmp));
        return -1;
    }

    memset (PinTmp, 0, sizeof(PinTmp));
    return 0;
}


