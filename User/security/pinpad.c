#include <string.h>
#include <stdio.h>
#include "SysTick.h"
#include "pinpad.h"
#include "fixvar.h"
#include "des_sec.h"
#include "VposFace.h"
#include "pub.h"
#include "misce.h"
#include "sec_common.h"
#include "lcdgui.h"
#include "util.h"
#include "user_projectconfig.h"

extern tti_bool IsAutoTrans(void);
extern int Des3ECB(unsigned char* key, unsigned char* in, int len, unsigned char* out, int enc);

/*
	This function tries to check if exist the stored SK in the pinpad. If
	exsit, check the out SK's type and len does match stored SK type and len.	

	*StoredSKFlag value:
			SK_STORED_NO:	SK not store in Pinpad.
			SK_STORED_YES:	SK stored in Pinpad.

	Return value:
			  0:		OK.
			-1:		invalid paramater
			-2:		Fail to get stored SK.
			-3:		Type or len not match.
*/
int CheckStoredSK (byte Indicator, byte KeyLen, byte KeyType, int *StoredSKFlag)
{
	KEY_ATTR  StoredSKAttr;
	int iRet;

	if (StoredSKFlag == NULL)
		return -1;

	if (Indicator >= MAX_NUM_KEY || KeyType > KEY_TYPE_MAX)
		return -1;

	if (KeyLen != KEY_LEN_DOUBLE)
		return -1;

	DPRINTF("Indicator = %d\n", Indicator);
	iRet = SecGetKeyAttr(KEY_LEVEL_SK, Indicator, &StoredSKAttr);
	if (iRet != 0)
	{
		if (iRet == -4)
		{
			//SK not store in Pinpad.
			*StoredSKFlag = SK_STORED_NO;
			return 0;
		}		

		return -2;
	}else
	{
		//SK stored in Pinpad. 
		if (StoredSKAttr.type != KeyType || StoredSKAttr.len != KeyLen)
		{
			return -3;
		}

		*StoredSKFlag = SK_STORED_YES;
		return 0;
	}
}

/*Check the Master Key and session key.
    Return value
		  0:	OK
		-1:	invalid paramater
		-2: incorrect Key type
*/
int CheckKey(byte KeyLevel, byte Indicator, byte KeyLen, byte KeyType, byte KeyMode)
{
	KEY_ATTR KEKAttr;
	int iRet;

	if (KeyLevel != KEY_LEVEL_KEK && KeyLevel != KEY_LEVEL_SK)
		return -1;

	if (Indicator >= MAX_NUM_KEY || KeyType > KEY_TYPE_MAX)
		return -1;

	if (KeyLen != KEY_LEN_DOUBLE)
		return -1;

	if (KeyMode != KEY_MODE_3_LAYER_D_D)
		return -1;

	if (KeyLevel == KEY_LEVEL_KEK)
	{
	
	}
	else if (KeyLevel == KEY_LEVEL_SK)
	{
		//DPRINTF("Indicator is %c", Indicator);
		iRet = SecGetKeyAttr(KEY_LEVEL_KEK, Indicator, &KEKAttr);
		if (iRet != 0)
		{
			return -2;
		}

		DPRINTF("KEKAttr.type = %02x, KeyType = %02x\n", KEKAttr.type, KeyType);
		if (KEKAttr.type != KeyType)
		{
			return -2;
		}
		
		//check len
		if (KEKAttr.len != KEY_LEN_DOUBLE)
		{
			DPRINTF("Error KEKAttr.len = %d\n", KEKAttr.len);
			return -2;
		}
	}

	return 0;
}

// Command 30
void LoadOtpKey(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{
	byte type;
	byte tmp[100];
	byte KeyValue[16] = {0};
	byte KeySerial[8] = {0};
	int offset;

	memcpy(OutBuff, "30.", 3);

	OutBuff[3]  = ERR_RANGE;				// assume error
	*OutLen = 4;

	if (InLen != 49)
	{
		DPRINTF("LoadOtpKey len err\n");
		return;
	}

	type = InBuff[0];
	
	if (AtoHex(KeyValue, (char *)(&InBuff[1]), 32))
		return;
	
	if (AtoHex(KeySerial, (char *)(&InBuff[1 + 32]), 16))
		return;

	if (type == '0')
	{
		offset = 0;
	}else
	{
		offset = 50;
	}
	tmp[0] = type;
	memcpy(tmp + 1, KeyValue, 16);
	memcpy(tmp + 1 + 16, KeySerial, 8);
	
	SecWriteOtp(tmp, 25, offset);
	OutBuff[3]  = ERR_OK;

	return;
}

//Command 31
void RequstEncViaOtpKey()
{
	
}

//Command 32
void RequstMacViaOtpKey()
{
	
}



// Command 40
void LoadMasterSessionKey(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{
	byte Indicator, Type;
	byte EncKey[16 ] = {0};
	byte KeySerial[8] = {0};
	byte Len;
	int iRet;
	KEY_ATTR KEK;
	KEY_ATTR LoadKey;
	PINPAD_PARA 	PINPad_para;
	
	DPRINTF("Call LoadMasterSessionKey()\n");

	memcpy(OutBuff, "40.", 3);

	OutBuff[3]  = ERR_RANGE;				// assume error
	*OutLen = 4;
	iRet = LoadSetting(&PINPad_para);
	if (iRet!= 0)
	{
		DPRINTF("LoadSetting error\n");
		dwatch(iRet);
		return;
	}

	if (PINPad_para.KeyMode != KEY_MODE_3_LAYER_D_D)
	{
		DPRINTF("PINPad_para.KeyMode = %c error, not KEY_MODE_3_LAYER_D_D\n", PINPad_para.KeyMode);
		OutBuff[3]	= '4';
		return;
	}
	
	if (PINPad_para.MSKeyLoadMode != MASTER_SESSION_KEY_LOAD_MODE_NORMAL
			&& PINPad_para.MSKeyLoadMode != MASTER_SESSION_KEY_LOAD_MODE_UN_INIT)
	{
		OutBuff[3]	= '4';				// assume error
		DPRINTF("MSKeyLoadMode error\n");
		return;
	}	
	PINPad_para.MSKeyLoadMode = MASTER_SESSION_KEY_LOAD_MODE_NORMAL;

	if (InLen != 50)
	{
		DPRINTF("InLen error\n");
		return;
	}
	//DPRINTF("InBuff = %s\n", InBuff);
	
	Indicator = InBuff[0] - '0';	// convert to decimal, key indicator
	Type = InBuff[1] - '0' ;			// convert to decimal, key type

	if (Indicator >= MAX_NUM_KEY || Type > KEY_TYPE_MAX)
		return;
	
	iRet = SecCheckKeyExist(KEY_LEVEL_KTK, 0);
	if (iRet < 0)
	{
		OutBuff[3]  = ERR_INVALID;
		return;
	}
	
	if (AtoHex(EncKey, (char *)(&InBuff[2]), 32))
		return;

	if (AtoHex(KeySerial, (char *)(&InBuff[2 + 32]), 16))
		return;

	Len = KEY_LEN_DOUBLE;

	if (Type != KEY_TYPE_SESSION)
	{
		LoadKey.type = Type;
		LoadKey.idx = Indicator;
		LoadKey.level = KEY_LEVEL_KEK;
		LoadKey.len = Len;
		memcpy(LoadKey.sn, KeySerial, 8);
		LoadKey.dec_level = KEY_LEVEL_KTK;
		LoadKey.dec_idx = 0;
	}
	else
	{
		//DPRINTF("Type is KEY_TYPE_SESSION\n");
		iRet = SecGetKeyAttr(KEY_LEVEL_KEK, Indicator, &KEK);
		if (iRet < 0)
		{
			DPRINTF("SecGetKeyAttr error\n");
			//DPRINTF("iRet is : %d", iRet);
			OutBuff[3]  = ERR_INVALID;
			return;
		}
		
		LoadKey.type = KEK.type;
		LoadKey.idx = Indicator;
		LoadKey.level = KEY_LEVEL_SK;
		LoadKey.len = Len;
		memcpy(LoadKey.sn, KeySerial, 8);
		LoadKey.dec_level = KEY_LEVEL_KEK;
		LoadKey.dec_idx = Indicator;
	}

	//Check key match the mode
	iRet = CheckKey(LoadKey.level, LoadKey.idx, LoadKey.len, LoadKey.type, PINPad_para.KeyMode);
	if (iRet != 0)
	{
		OutBuff[3]  = ERR_INVALID;
		return;
	}

	iRet = SecWriteKey(&LoadKey, EncKey);
	if (iRet < 0)
	{
		OutBuff[3]  = ERR_INVALID;
		return;
	}
	
	OutBuff[3] = ERR_OK;
	*OutLen = 4;
	SaveSetting(&PINPad_para);
	return;
}


int BuildPinBlock(char *InputPin, int PinLen, byte *PinBlock)
{
	byte PinTmp[16];

	if ((PinLen < 4) && (PinLen > 12))
	{
		return -2;
	}

	if ((InputPin == NULL) && (PinBlock == NULL))
	{
		return -2;
	}

	PinTmp[0] = '0';

	if (PinLen < 10)
	{
		PinTmp[1] = PinLen+0x30;
	}else 
	{
		PinTmp[1] = PinLen+0x37;
	}

	memset(PinTmp + 2, 'F',14);
	memcpy(PinTmp + 2, InputPin, PinLen);
	if (AtoHex(PinBlock, (char *)PinTmp, 16))
	{	
		memset(PinTmp, 0, sizeof(PinTmp));
		return -1;
	}

	memset(PinTmp, 0, sizeof(PinTmp));
	return 0;
}

int BuildPanBlock(char *InputPanStr, int PanStrLen, byte *PanBlock)
{
	byte PanTmp[16];
	
	if (PanStrLen > 19 || PanStrLen < 2)
	{
		return -1;
	}

	if (InputPanStr == NULL || PanBlock == NULL)
	{
		return -1;
	}

	memset(PanTmp, '0', 16);

	if (PanStrLen - 1 >= 12)
	{
		//No need letf pad 0x00
		memcpy(PanTmp + 4, InputPanStr + PanStrLen - 13, 12);
	}else
	{
		//Need left pad (13 - PanStrLen) 0x00
		memcpy(PanTmp + 4 + (13 - PanStrLen), InputPanStr, PanStrLen - 1);
	}
	
	if (AtoHex(PanBlock, (char *)PanTmp, 16))
	{	
		memset(PanTmp, 0, sizeof(PanTmp));
		return -2;
	}

	memset(PanTmp, 0, sizeof(PanTmp));
	return 0;
}


int BuildPinBlockFormat4(char *InputPin, int PinLen, byte *PinBlock)
{
	byte PinTmp[32];
	
	if ((PinLen < 4) && (PinLen > 12))
	{
		return -2;
	}

	if ((InputPin == NULL) && (PinBlock == NULL))
	{
		return -2;
	}

	PinTmp[0] = '4';

	if (PinLen < 10)
	{
		PinTmp[1] = PinLen+0x30;
	}else 
	{
		PinTmp[1] = PinLen+0x37;
	}

	memset(PinTmp + 2, 'A',14);
	memcpy(PinTmp + 2, InputPin, PinLen);
	if (AtoHex(PinBlock, (char *)PinTmp, 16))
	{	
		memset(PinTmp, 0, sizeof(PinTmp));
		return -1;
	}

//	Jason remove it on 20191225 for HM chip
//	ucl_rng_read(PinBlock + 8, 8, UCL_RAND_DEFAULT);
	rand_hardware_byte_ex(PinBlock + 8, 8);
//  Jason remove end
	memset(PinTmp, 0, sizeof(PinTmp));
	return 0;
}

int BuildPanBlockFormat4(char *InputPanStr, int PanStrLen, byte *PanBlock)
{
	byte PanTmp[32];
	
	if (PanStrLen > 19 || PanStrLen < 2)
	{
		return -1;
	}

	if (InputPanStr == NULL || PanBlock == NULL)
	{
		return -1;
	}

	if (PanStrLen < 12)
	{
		PanTmp[0] = '0';
	}else
	{
		PanTmp[0] = PanStrLen - 12 + '0';
	}
	
	memset(PanTmp + 1, '0', 31);
	DPRINTF("PanStrLen = %d\n", PanStrLen);
	if (PanStrLen < 12)
	{
		//DPRINTF("PanStrLen < 12");
		memcpy(PanTmp + 1 + (12 - PanStrLen), InputPanStr, PanStrLen);
	}else
	{
		memcpy(PanTmp + 1, InputPanStr, PanStrLen);
	}
	if (AtoHex(PanBlock, (char *)PanTmp, 32))
	{	
		memset(PanTmp, 0, sizeof(PanTmp));
		return -2;
	}

	memset(PanTmp, 0, sizeof(PanTmp));
	return 0;
}

int BuildTrackBlock(char *InputTrackStr, int InputTrackStrLen, byte *OutputPanBlock)
{
	char padTrackStr[200];
	
	if (InputTrackStrLen % 2 != 0)
	{
		InputTrackStrLen+=1;
		sprintf(padTrackStr, "%s%s", InputTrackStr, "0");
	}else
	{
		sprintf(padTrackStr, "%s", InputTrackStr);
	}

	AtoHex(OutputPanBlock , padTrackStr + InputTrackStrLen - 18, 16);

	return 0;
}


void MSRequestPinEncEx(int keyIndicator, byte *formatPinXorformatPan, byte *OutBuff, int *OutLen)
{
	byte Indicator;	
	byte EncPINBlock[16];
	KEY_ATTR  PinKeyAttr;
	int iStoredSKFlag;
	int iRet;
	PINPAD_PARA PINPad_para;
	
	memcpy(OutBuff, "91.", 3);
	OutBuff[3]  = ERR_RANGE;				// assume error
	//*OutLen = 22;
	*OutLen = 4;

	iRet = LoadSetting(&PINPad_para);
	if (iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		dwatch(iRet);
		return;
	}

	if (PINPad_para.KeyMode != KEY_MODE_3_LAYER_D_D)
	{
		DPRINTF("KeyMode error\n");
		return;
	}
	
	Indicator = keyIndicator;

	PinKeyAttr.level = KEY_LEVEL_SK;
	PinKeyAttr.len = KEY_LEN_DOUBLE;
	PinKeyAttr.type = KEY_TYPE_ENC;
	PinKeyAttr.idx = Indicator;
	PinKeyAttr.dec_level = KEY_LEVEL_KEK;
	PinKeyAttr.dec_idx = Indicator;
	//check if exist stored SK in the pinpad
	iRet = CheckStoredSK(PinKeyAttr.idx, PinKeyAttr.len, PinKeyAttr.type, &iStoredSKFlag);
	if (iRet != 0)
	{
		if (iRet == -3)
		{
			DPRINTF("CheckStoredSK(): type or length unmatched");
			OutBuff[3] = ERR_INVALID;
		}

		return;
	}
	if (iStoredSKFlag != SK_STORED_YES)
	{		
		OutBuff[3] = ERR_INVALID;
		return;
	}

	//check key attr 
	iRet = CheckKey(PinKeyAttr.level, PinKeyAttr.idx, PinKeyAttr.len, PinKeyAttr.type, PINPad_para.KeyMode);
	if (iRet < 0)
	{
		if (iRet == -2)
		{
			DPRINTF("CheckKey(): incorrect key type");
			OutBuff[3] = ERR_INVALID;
		}

		return;
	}

	iRet = SecDesEnc(PinKeyAttr.level, PinKeyAttr.idx, formatPinXorformatPan, EncPINBlock);
	if (iRet != 0)
	{	
		return;
	}

	*OutLen = 20;
	OutBuff[3] = ERR_OK;
	HextoA((char *)(OutBuff + 4), EncPINBlock, 8);	
}



void MSRequestPanEncEx(int keyIndicator, byte *formatPan, byte *OutBuff, int *OutLen)
{
	byte Indicator;	
	byte EncPINBlock[16];
	KEY_ATTR  PinKeyAttr;
	int iStoredSKFlag;
	int iRet;
	PINPAD_PARA PINPad_para;
	
	memcpy(OutBuff, "93.", 3);
	OutBuff[3]  = ERR_RANGE;				// assume error
	//*OutLen = 22;
	*OutLen = 4;

	iRet = LoadSetting(&PINPad_para);
	if (iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		dwatch(iRet);
		return;
	}

	if (PINPad_para.KeyMode != KEY_MODE_3_LAYER_D_D)
	{
		DPRINTF("KeyMode error\n");
		return;
	}
	
	Indicator = keyIndicator;

	PinKeyAttr.level = KEY_LEVEL_SK;
	PinKeyAttr.len = KEY_LEN_DOUBLE;
	PinKeyAttr.type = KEY_TYPE_TRACK2;
	PinKeyAttr.idx = Indicator;
	PinKeyAttr.dec_level = KEY_LEVEL_KEK;
	PinKeyAttr.dec_idx = Indicator;
	//check if exist stored SK in the pinpad
	iRet = CheckStoredSK(PinKeyAttr.idx, PinKeyAttr.len, PinKeyAttr.type, &iStoredSKFlag);
	if (iRet != 0)
	{
		if (iRet == -3)
		{
			DPRINTF("CheckStoredSK(): type or length unmatched");
			OutBuff[3] = ERR_INVALID;
		}

		return;
	}
	if (iStoredSKFlag != SK_STORED_YES)
	{		
		OutBuff[3] = ERR_INVALID;
		return;
	}

	//check key attr 
	iRet = CheckKey(PinKeyAttr.level, PinKeyAttr.idx, PinKeyAttr.len, PinKeyAttr.type, PINPad_para.KeyMode);
	if (iRet < 0)
	{
		if (iRet == -2)
		{
			DPRINTF("CheckKey(): incorrect key type");
			OutBuff[3] = ERR_INVALID;
		}

		return;
	}

	iRet = SecDesEnc(PinKeyAttr.level, PinKeyAttr.idx, formatPan, EncPINBlock);
	if (iRet != 0)
	{	
		return;
	}

	*OutLen = 20;
	OutBuff[3] = ERR_OK;
	HextoA((char *)(OutBuff + 4), EncPINBlock, 8);	
}

void MSRequestTrackEncEx(int keyIndicator, byte *InputTrack, int InputTrackLen, byte *OutBuff, int *OutLen)
{
	byte Indicator;	
	byte EncTrackBlock[200];
	int EncTrackLen;
	KEY_ATTR  PinKeyAttr;
	int iStoredSKFlag;
	int iRet;
	PINPAD_PARA PINPad_para;
	
	memcpy(OutBuff, "94.", 3);
	OutBuff[3]  = ERR_RANGE;				// assume error
	*OutLen = 4;

	iRet = LoadSetting(&PINPad_para);
	if (iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		dwatch(iRet);
		return;
	}

	if (PINPad_para.KeyMode != KEY_MODE_3_LAYER_D_D)
	{
		DPRINTF("KeyMode error\n");
		return;
	}
	
	Indicator = keyIndicator;

	PinKeyAttr.level = KEY_LEVEL_SK;
	PinKeyAttr.len = KEY_LEN_DOUBLE;
	PinKeyAttr.type = KEY_TYPE_TRACK2;
	PinKeyAttr.idx = Indicator;
	PinKeyAttr.dec_level = KEY_LEVEL_KEK;
	PinKeyAttr.dec_idx = Indicator;
	//check if exist stored SK in the pinpad
	iRet = CheckStoredSK(PinKeyAttr.idx, PinKeyAttr.len, PinKeyAttr.type, &iStoredSKFlag);
	if (iRet != 0)
	{
		if (iRet == -3)
		{
			DPRINTF("CheckStoredSK(): type or length unmatched");
			OutBuff[3] = ERR_INVALID;
		}

		////Line;
		return;
	}
	if (iStoredSKFlag != SK_STORED_YES)
	{	
		////Line;
		OutBuff[3] = ERR_INVALID;
		return;
	}

	//check key attr 
	iRet = CheckKey(PinKeyAttr.level, PinKeyAttr.idx, PinKeyAttr.len, PinKeyAttr.type, PINPad_para.KeyMode);
	if (iRet < 0)
	{
		if (iRet == -2)
		{
			DPRINTF("CheckKey(): incorrect key type");
			OutBuff[3] = ERR_INVALID;
		}

		////Line;
		return;
	}

	iRet = SecDesEncTrack_UnionPay(PinKeyAttr.level, PinKeyAttr.idx, InputTrack, InputTrackLen, 
		EncTrackBlock, &EncTrackLen);
	if (iRet != 0)
	{	
		////Line;
		return;
	}

	*OutLen = EncTrackLen + 4;
	OutBuff[3] = ERR_OK;
	//HextoA(OutBuff + 4, EncTrackBlock, 8);	
	memcpy(OutBuff + 4, EncTrackBlock, EncTrackLen);
}



// Command 43
void AsciiHexMacCalculation(byte *InBuff, int InLen, byte *OutBuff, int *OutBuffLen)
{
	byte Indicator;
	byte MACData[512] = {0};
	byte ResultMAC[8];
	int iRet;
	int iStoredSKFlag;
	KEY_ATTR SKAttr;
	PINPAD_PARA PINPad_para;
	
	memcpy(OutBuff, "43.", 3);
	OutBuff[3] = ERR_RANGE;
	*OutBuffLen = 12;

	iRet = LoadSetting(&PINPad_para);
	if (iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		dwatch(iRet);
		return;
	}

	if (PINPad_para.KeyMode != KEY_MODE_3_LAYER_D_D)
	{
		DPRINTF("KeyMode error\n");
		return;
	}

	//DPRINTF("InBuff is:%s\n", InBuff);
	Indicator = InBuff[0] - '0';
	if (InLen > 1024 + 1)
	{
		DPRINTF("InLen error\n");
		return;
	}

	if ((InLen - 1)%16 != 0)
	{
		err("");
		return;
	}

	if (AtoHex(MACData, (char *)(InBuff + 1), InLen - 1))
		return;

	SKAttr.level = KEY_LEVEL_SK;
	SKAttr.len = KEY_LEN_DOUBLE;
	SKAttr.idx = Indicator;
	SKAttr.type = KEY_TYPE_MAC;
	SKAttr.dec_level = KEY_LEVEL_KEK;
	SKAttr.dec_idx = Indicator;

	//check if exist stored SK in the pinpad
	iRet = CheckStoredSK(SKAttr.idx, SKAttr.len, SKAttr.type, &iStoredSKFlag);
	if (iRet != 0)
	{
		if (iRet == -3)
		{
			OutBuff[3] = ERR_INVALID;
		}
		err("");
		return;
	}
	
	if (iStoredSKFlag != SK_STORED_YES)
	{		
		err("");
		OutBuff[3] = ERR_INVALID;
		return;
	}

	iRet = CheckKey(SKAttr.level, SKAttr.idx, SKAttr.len, SKAttr.type, PINPad_para.KeyMode);
	if (iRet < 0)
	{
		err("");
		return;
	}

	//iRet = SecMac(KEY_LEVEL_SK, Indicator, MACData, (InLen - 1)/2, ResultMAC);
	iRet = SecMac_UnionPay(KEY_LEVEL_SK, Indicator, MACData, (InLen - 1)/2, ResultMAC);
	if (iRet != 0)
	{
		err("");
		return;
	}
	//translate the Left 4 byte hex MAC to 8 ascii  hex byte 
	OutBuff[3] = ERR_OK;
	HextoA((char *)(OutBuff + 4), ResultMAC, 4);

	return;
}

// Command 52
void LoadSpecialKey(byte *InBuff, int InLen, byte *OutBuff, int *OutBuffLen)
{
	byte KeyTransform;
	byte TransKeyL[8] = {0};
	byte TransKeyR[8] = {0};
	byte SerialNumber[8] = {0};
	byte KeyValue[16] = {0};
	int iRet;
	KEY_ATTR LoadKey;
	PINPAD_PARA PINPad_para; 

	memcpy( OutBuff, "52.", 3);
	OutBuff[3] = ERR_RANGE;
	*OutBuffLen = 4;

	iRet = LoadSetting(&PINPad_para);
	if (iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		dwatch(iRet);
		return;
	}

	if (InLen != 49)
	{
		DPRINTF("InLen error\n");
		return;
	}

#ifndef FACTORY_APP
	dbg("PINPad_para.KeyMode = %c\n", PINPad_para.KeyMode);
	if (PINPad_para.KeyMode == KEY_MODE_3_LAYER_D_D)
	{
		OutBuff[3] = ERR_KTK_EXIST;
		DPRINTF("ERR_KTK_EXIST\n");
		*OutBuffLen = 4;
		return;
	}else if (PINPad_para.KeyMode != KEY_MODE_UN_INIT)
	{
		DPRINTF("PINPad_para.KeyMode != KEY_MODE_UN_INIT && != KEY_MODE_3_LAYER_D_D\n");
		return;
	}
#endif

	KeyTransform = InBuff[0];		// get and check key transform type parameter
	if (KeyTransform != KEY_TRANSFORM_CLR_KTK)
		return;

	if (AtoHex(TransKeyL, (char *)(InBuff + 1), 16))
		return;

	if (AtoHex(TransKeyR, (char *)(InBuff+1+16), 16))
		return;

	if (AtoHex(SerialNumber, (char *)(InBuff+1+16+16), 16))
		return;

	SecEraseAllKey();

	memcpy(KeyValue, TransKeyL, 8);
	memcpy(KeyValue + 8, TransKeyR, 8);	
	
	LoadKey.level = KEY_LEVEL_KTK;
	LoadKey.idx = 0;
	LoadKey.len = KEY_LEN_DOUBLE;
	LoadKey.type = KEY_TYPE_OTHERS;
	memcpy(LoadKey.sn, SerialNumber, 8);
	LoadKey.dec_level = KEY_LEVEL_NONE;
	LoadKey.dec_idx = 0;

#if 0
{
    uchar tmp[60];
    vOneTwo0(KeyValue, 8, tmp);
    _vDisp(3, tmp);
    vOneTwo0(KeyValue+8, 8, tmp);
    _vDisp(4, tmp);
    _uiGetKey();
    _vDisp(3, "");
    _vDisp(4, "");
}
#endif
	iRet = SecWriteKey(&LoadKey, KeyValue);
	ReportLine();
	if (iRet < 0)
	{
		PINPad_para.KeyMode = KEY_MODE_UN_INIT;
		SaveSetting(&PINPad_para);
		
		OutBuff[3]  = ERR_INVALID;

		memset(TransKeyL, 0, sizeof(TransKeyL));
		memset(TransKeyR, 0, sizeof(TransKeyR));
		memset(KeyValue, 0, sizeof(KeyValue));
		
		return;
	}

	PINPad_para.KeyMode = KEY_MODE_3_LAYER_D_D;
	PINPad_para.MSKeyLoadMode = MASTER_SESSION_KEY_LOAD_MODE_UN_INIT;
	SaveSetting(&PINPad_para);
	
	OutBuff[3] = ERR_OK;
	memset(TransKeyL, 0, sizeof(TransKeyL));
	memset(TransKeyR, 0, sizeof(TransKeyR));
	memset(KeyValue, 0, sizeof(KeyValue));

	return;
}

// Command 60
void LoadMasterSessionKeyByKBPK(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{
	byte Indicator, Type;
	byte EncKey[32] = {0};
	byte KeySerial[8] = {0};
	byte byWholeRequest[100] = {0};
	byte byRequestMAC[8] = {0};
	byte Len;
	int iRet;
	int iIndex;
	int iWholeRequestLen;
	KEY_ATTR KEK;
	KEY_ATTR LoadKey;
	PINPAD_PARA PINPad_para; 
	byte byRequestMACHex[4];
	
	DPRINTF("Call LoadMasterSessionKeyByKBPK()\n");

	memcpy( OutBuff, "60.", 3);
	OutBuff[3]  = ERR_RANGE;				// assume error
	*OutLen = 4;

	if (InLen != 77)
	{
		DPRINTF("Len error\n");
		return;
	}
	iRet = LoadSetting(&PINPad_para);
	if ( iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		dwatch(iRet);
		return;
	}

	if (PINPad_para.KeyMode != KEY_MODE_3_LAYER_D_D)
	{
		DPRINTF("KeyMode error\n");
		return;
	}

	if (PINPad_para.MSKeyLoadMode != MASTER_SESSION_KEY_LOAD_MODE_KBPK
		&& PINPad_para.MSKeyLoadMode != MASTER_SESSION_KEY_LOAD_MODE_UN_INIT)
	{
		OutBuff[3]  = '4';				// assume error
		DPRINTF("MSKeyLoadMode error\n");
		return;
	}	
	PINPad_para.MSKeyLoadMode = MASTER_SESSION_KEY_LOAD_MODE_KBPK;

	Indicator = InBuff[0] - '0';	// convert to decimal, key indicator
	Type = InBuff[1] - '0' ;			// convert to decimal, key type

	if (Indicator >= MAX_NUM_KEY || Type > KEY_TYPE_MAX)
	{
		DPRINTF("Indicator error\n");
		return;
	}
	iRet = SecCheckKeyExist(KEY_LEVEL_KTK, 0);
	if (iRet < 0)
	{
		DPRINTF("Get KTK error\n");
		OutBuff[3]  = ERR_INVALID;
		return;
	}

	iIndex = 1 + 1 + 3 + 4; //Encrypted KEY index
	if (AtoHex(EncKey, (char *)(&InBuff[iIndex]), 32))
	{
		DPRINTF("AtoHex EncKey error\n");
		return;
	}
	iIndex += 32;
	
	if (AtoHex(KeySerial, (char *)(&InBuff[iIndex]), 16))
	{
		DPRINTF("AtoHex KeySerial error\n");
		return;
	}
	iIndex += 16;

	iWholeRequestLen = InLen + 3 - 8;
	Len = KEY_LEN_DOUBLE;

	memcpy(byRequestMAC, InBuff + iWholeRequestLen - 3, 8);
	if (AtoHex(byRequestMACHex, (char *)byRequestMAC, 8))
	{
		DPRINTF("AtoHex byRequestMACHex error\n");
		return;
	}		
	memcpy(byWholeRequest, "60.", 3);
	memcpy(byWholeRequest + 3, InBuff, iWholeRequestLen - 3);
		
	if (Type != KEY_TYPE_SESSION)
	{
		LoadKey.type = Type;
		LoadKey.idx = Indicator;
		LoadKey.level = KEY_LEVEL_KEK;
		LoadKey.len = Len;
		memcpy( LoadKey.sn, KeySerial, 8);
		LoadKey.dec_level = KEY_LEVEL_KTK;
		LoadKey.dec_idx = 0;
	}
	else
	{
		DPRINTF("Type is KEY_TYPE_SESSION\n");
		iRet = SecGetKeyAttr(KEY_LEVEL_KEK, Indicator, &KEK);
		if (iRet < 0)
		{
			DPRINTF("SecGetKeyAttr KEY_LEVEL_KEK error\n");
			//DPRINTF("iRet is : %d", iRet);
			OutBuff[3]  = ERR_INVALID;
			return;
		}

		LoadKey.type = KEK.type;
		LoadKey.idx = Indicator;
		LoadKey.level = KEY_LEVEL_SK;
		LoadKey.len = Len;
		memcpy (LoadKey.sn, KeySerial, 8);
		LoadKey.dec_level = KEY_LEVEL_KEK;
		LoadKey.dec_idx = Indicator;
	}

	//Check key match the mode
	iRet = CheckKey(LoadKey.level, LoadKey.idx, LoadKey.len, LoadKey.type, PINPad_para.KeyMode);
	if (iRet != 0)
	{
		if ( iRet == -2)
		{
			DPRINTF("Not session Key: CheckKey error\n");
			OutBuff[3] = ERR_INVALID;
			return;
		}

		return;
	}

	iRet = SecCmpMACByKBPK(LoadKey, byWholeRequest, iWholeRequestLen, byRequestMACHex);		
	if (iRet != 0)
	{
		DPRINTF("Not session Key: SecCmpMACByKBPK error\n");				
		OutBuff[3]  = '3';
		return;
	}

	iRet = SecWriteKeyByKBPK(&LoadKey, EncKey);
	if (iRet != 0)
	{
		DPRINTF("Not session Key: SecWriteKeyByKBPK error\n");				
		OutBuff[3]  = ERR_INVALID;
		return;
	}

	OutBuff[3] = ERR_OK;
	*OutLen = 4;
	SaveSetting(&PINPad_para);
	return;
}


/*
"Load Initial Key" (External Command) 
    1) Store the initial PIN encryption key, as received in the externally initiated command,
        into Future Key Register #21. 
    2) Generate and store the LRC on this Future Key Register. 
    3) Write the address of Future Key Register #21 into the Current Key Pointer. 
    4) Store the Key Serial Number, as received in the externally initiated command,
        into the Key Serial Number     Register. (This register is the concatenation of
        the Initial Key Serial Number Register and the Encryption Counter.) 
    5) Clear the Encryption Counter (the 21 right-most bits of
        the Key Serial Number Register). 
    6) Set bit #1 (the left-most bit) of the Shift Register to "one", 
        setting all of the other bits to "zero". 
    7) Go to "New Key-3". 
*/
void LoadInitialKey(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{   
    int iRet = -1;	
    byte byArrTmp[40];    
	PINPAD_PARA PINPad_para;
 
	DPRINTF("Enter LoadInitialKey()\n");
    
    memcpy(OutBuff, "70.", 3);
    OutBuff[3]  = ERR_RANGE;                // assume error
    *OutLen = 4;
    
    iRet = LoadSetting(&PINPad_para);
    if (iRet != 0)
    {
        DPRINTF("LoadSetting error\n");
        dwatch(iRet);
        return;
    }

    if (AtoHex(byArrTmp, (char *)InBuff, 32)!= 0)
    {
        memset(byArrTmp, 0, sizeof(byArrTmp));
        return;
    }

	SecEraseAllKey();
	
    iRet = SecLoadInitDupkt(InBuff, InLen, OutBuff, OutLen);    
    if (iRet == 0)
    {     
        PINPad_para.KeyMode = KEY_MODE_DUKPT;
        SaveSetting(&PINPad_para);
    }else
    {
		PINPad_para.KeyMode = KEY_MODE_UN_INIT;
        SaveSetting(&PINPad_para);
	}

    memset(byArrTmp, 0, sizeof(byArrTmp));
    
    return;
}
  
/*
Request PIN Entry" (External Command) 
    1) Transfer the primary account number as received in the externally
        initiated command into the Account Number Register. 
    2) Activate the PIN Entry Device keyboard and the Enter key. 
    3) If the PIN is not entered, send the encrypted PIN block 
        response message without the PIN-related data elements and go to "Exit". 
    4) If the PIN is entered, use the card holder-entered PIN and the primary account number
        to generate the clear text PIN block and store it in Crypto Register-1. 
    5) Go to "Request PIN Entry 1". 
*/
void DUKPTRequestPINEncEx(byte *formatPinXorFormatPan, byte *OutBuff, int *OutLen)
{
	int iRet;
	PINPAD_PARA PINPad_para;

	iRet = LoadSetting(&PINPad_para);
	if (iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		dwatch(iRet);
		return;
	}
	
	if (PINPad_para.KeyMode != KEY_MODE_DUKPT)
	{
		return;
	}

  	SecDupktGetPINBlockEx(formatPinXorFormatPan, OutBuff, OutLen);
}

void DUKPTRequestPANEncEx(byte *FormatPan, byte *OutBuff, int *OutLen)
{
	int iRet;
	PINPAD_PARA PINPad_para;

	iRet = LoadSetting(&PINPad_para);
	if (iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		dwatch(iRet);
		return;
	}
	
	if (PINPad_para.KeyMode != KEY_MODE_DUKPT)
	{
		return;
	}

  	SecDupktGetPANBlockEx(FormatPan, OutBuff, OutLen);
}



/*
1) Convert the input string (4012345678909D987) to hex:
34 30 31 32 33 34 35 36 37 38 39 30 39 44 39 38 37
2) XOR the Initial Vector (all zeros) with the first 8 bytes of the hex value from step 1:
3430313233343536
3) Encrypt the result from step 2 with the left half of the MAC Encryption Key.
4) XOR the result from step 3 with the next 8 bytes of the hex value from step 1. If fewer than 8 bytes
remain, left justify the value and fill the remaining places with binary zeros.
5) Encrypt the result from step 4 with the left half of the MAC Encryption Key.
6) Go to step 4 until all input data has been used. After the last block has been encrypted, go to step 7.
7) Decrypt the result from step 5 with the right half of the MAC Encryption Key.
8) Encrypt the result from step 7 with the left half of the MAC Encryption Key.
9) Use the first 4 bytes of the result from step 8 as the MAC value. Show this result as hexadecimal digits.
*/
void RequestMacCode(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{
	int iRet;
	PINPAD_PARA PINPad_para;
    
    memcpy( OutBuff, "72.", 3);
    OutBuff[3] = ERR_RANGE;
    *OutLen = 4;
    
    iRet = LoadSetting(&PINPad_para);
    if (iRet != 0)
    {
        DPRINTF("LoadSetting error\n");
        return;
    }
    if (PINPad_para.KeyMode != KEY_MODE_DUKPT)
    {
        return;
    }
    
    SecRequestMacCode(InBuff, InLen, OutBuff, OutLen);
    
    return;
}


//command "80."
void LoadAesFixedKey(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	byte aesKey[16];
	byte keySerial[8];
	PINPAD_PARA PINPad_para;

	DPRINTF("LoadAesFixedKey()....\n");

	memcpy (OutBuff, "80.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	if (InLen != 48)
	{
		return;
	}

	if (AtoHex(aesKey, (char *)InBuff, 32) != 0)
	{
		return;
	}

	if (AtoHex(keySerial, (char *)InBuff + 32, 16) != 0)
	{
		return;
	}

	LoadSetting(&PINPad_para);

	DPRINTF("SecEraseAllKey ();\n");
	SecEraseAllKey();

	DPRINTF("SecWriteAesFixedKey ();\n");
	if (SecWriteAesFixedKey(aesKey, keySerial) == 0)
	{
	    PINPad_para.KeyMode = KEY_MODE_AES_FIXED_KEY;
	    SaveSetting(&PINPad_para);
	}else
	{
		PINPad_para.KeyMode = KEY_MODE_UN_INIT;
	   	SaveSetting(&PINPad_para);
	}
	
	memset(aesKey, 0, sizeof(aesKey));
	
	OutBuff[3]	= ERR_OK;
	return;
}

void AesFixedKeyRequestPINEncEx(byte *formatPINBlock, byte *formatPANBlock, byte *OutBuff, int *pOutLen)
{
	int iRet;
	byte aesKey[16];
	byte keySerial[8];
	byte internalBlockA[16];
	byte encryptedBlock[16];
	PINPAD_PARA PINPad_para;

	memcpy (OutBuff, "91.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	iRet = LoadSetting(&PINPad_para);
	if (iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		return;
	}
	if (PINPad_para.KeyMode != KEY_MODE_AES_FIXED_KEY)
	{
		DPRINTF("KeyMode != KEY_MODE_AES_FIXED_KEY\n");
		return;
	}
	
	iRet = SecReadAesFixedKey(aesKey, keySerial);
	if (iRet != 0)
	{
		return;
	}

	DPRINTF("formatPINBlock list\n");
	hexdump(formatPINBlock, 16);
	DPRINTF("formatPANBlock list\n");
	hexdump(formatPANBlock, 16);
	DPRINTF("aesKey list\n");
	hexdump(aesKey, 16);
	
	SecAesEnc(aesKey, formatPINBlock, internalBlockA);
	DPRINTF("internalBlockA list\n");
	hexdump(internalBlockA, 16);
	Xor_ex(internalBlockA, formatPANBlock, 16);
	SecAesEnc(aesKey, internalBlockA, encryptedBlock);

	memset(aesKey, 0, sizeof(aesKey));

	OutBuff[3]	= ERR_OK;
	HextoA((char *)(OutBuff + 4), encryptedBlock, 16);
	*pOutLen = 4 + 32;
}


void AesFixedKeyRequestPANEncEx(byte *formatPANBlock, byte *OutBuff, int *pOutLen)
{
	int iRet;
	byte aesKey[16];
	byte keySerial[8];
	byte encryptedBlock[16];
	PINPAD_PARA PINPad_para;

	memcpy (OutBuff, "93.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	iRet = LoadSetting(&PINPad_para);
	if (iRet != 0)
	{
		DPRINTF("LoadSetting error\n");
		return;
	}
	if (PINPad_para.KeyMode != KEY_MODE_AES_FIXED_KEY)
	{
		DPRINTF("KeyMode != KEY_MODE_AES_FIXED_KEY\n");
		return;
	}
	
	iRet = SecReadAesFixedKey(aesKey, keySerial);
	if (iRet != 0)
	{
		return;
	}

	//DPRINTF("formatPINBlock list\n");
	//hexdumps(formatPINBlock, 16);
	//DPRINTF("formatPANBlock list\n");
	//hexdumps(formatPANBlock, 16);
	
	SecAesEnc(aesKey, formatPANBlock, encryptedBlock);

	memset(aesKey, 0, sizeof(aesKey));

	OutBuff[3]	= ERR_OK;
	HextoA((char *)(OutBuff + 4), encryptedBlock, 16);
	*pOutLen = 4 + 32;
}


//0: enter ok
//-1: Cancel
//-2:Timeout
//-99: other error
int GetPIN(char *PINValueout, int *PINLenOut)
{
	//int rlen=0;
	int ret;
	//int ret = SoftPINPAD(PINValueout,&rlen,NULL);

	LcdClear();

	LcdPutsc("����������", 0);
	
	ret=iInput(INPUT_PIN, 3, 12, (byte *)PINValueout, 12, 4, 12, 30);
	DPRINTF("iInput return = %d\n", ret);
	//memset(PINValueout,1,12);
	if(ret < 0){
		if(ret== -3)
			ret=-2;
		else if(ret==-1)
			ret=-1;
		else
			ret = -99;
	}else{
		*PINLenOut = ret;
		PINValueout[ret] = '\0';
		DPRINTF("PINValueout = %s\n", PINValueout);
		ret = 0;
	}
	return ret;	
}

//0: enter ok
//-1: Cancel
//-2:Timeout
//-99: other error
int GetPINViaPrompt(char *szPrompt, char *PINValueout, int *PINLenOut)
{
	//int rlen=0;
	int ret;
	//int ret = SoftPINPAD(PINValueout,&rlen,NULL);

	LcdClear();

	if (szPrompt == NULL)
	{
		LcdPutsc("����������", 0);
	}else
	{
		LcdPutsc(szPrompt, 0);
	}

	if(IsAutoTrans() == FALSE)
	{
		ret=iInput(INPUT_PIN, 3, 12, (byte *)PINValueout, 12, 4, 12, 30);
	}else
	{
		//Auto Test PIN
		strcpy(PINValueout, "1234");
		ret=iInput(INPUT_PIN|INPUT_INITIAL, 3, 12, (byte *)PINValueout, 12, 4, 12, 1);
		*PINLenOut = 4;
		return 0;
	}
	DPRINTF("iInput return = %d\n", ret);
	//memset(PINValueout,1,12);
	if(ret < 0){
		if(ret== -3)
			ret=-2;
		else if(ret==-1)
			ret=-1;
		else
			ret = -99;
	}else{
		*PINLenOut = ret;
		PINValueout[ret] = '\0';
		DPRINTF("PINValueout = %s\n", PINValueout);
		ret = 0;
	}
	return ret;	
}


void RequestOfflinePIN(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	int pinLen;
  	char InputPINStr[13];

	memcpy (OutBuff, "92.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	iRet = GetPIN(InputPINStr, &pinLen);
	switch(iRet)
	{
		case 0:
			//OK
			break;

		case -1:
			OutBuff[3]	= ERR_CANCEL;
			*pOutLen = 4;
			return;

		case -2:
			OutBuff[3]	= ERR_TIMEOUT;
			*pOutLen = 4;
			return;

		default:
			return;
	}

	memcpy(OutBuff + 4, InputPINStr, pinLen);
	OutBuff[3]	= ERR_OK;				
	*pOutLen = 4 + pinLen;
	
	return;
}

tick gEnterPinLastTick = 0;

tick GetEnterPinLastTick(void)
{
	return gEnterPinLastTick;
}

void SetEnterPinLastTick(tick enterPinLastTick)
{
	gEnterPinLastTick = enterPinLastTick;
}

tick gEncTrackLastTick = 0;

tick GetEncTrackLastTick(void)
{
	return gEncTrackLastTick;
}

void SetEncTrackLastTick(tick encTrackLastTick)
{
	gEncTrackLastTick = encTrackLastTick;
}


void RequestPINEnc(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	int pinLen;
	byte panLen;
  	char InputPINStr[13];
	char InputPANStr[20];
	byte formatPINBlock[16];
	byte formatPANBlock[16];
	PINPAD_PARA PINPad_para;

	memcpy (OutBuff, "91.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	//DPRINTF("InBuff =%s\n", InBuff);

	//30 seconds can call this function one time
	if (get_diff_tick(get_tick(), GetEnterPinLastTick()) < 30*1000)
	{
		DPRINTF("30 seconds can call this function one time, please wait\n");
		return;
	}else
	{
		DPRINTF("30 seconds timeout\n");
	}

	iRet = LoadSetting(&PINPad_para);
    if (iRet != 0)
    {
        DPRINTF("LoadSetting error\n");
        return;
    }
	
    if (PINPad_para.KeyMode == KEY_MODE_UN_INIT)
    {
    	DPRINTF("KeyMode KEY_MODE_UN_INIT\n");
        return;
    }

	panLen = (InBuff[1] - '0')*10 + (InBuff[2] - '0');
	if (panLen < 2 || panLen > 19)
	{
		DPRINTF("panLen error\n");
		return;
	}

	if (panLen + 3 != InLen)
	{
		DPRINTF("panLen + 3 != Inlen error\n");
		return;
	}

    memcpy(InputPANStr, InBuff + 3, panLen);
	DPRINTF("List InputPANStr");
	hexdump((byte *)InputPANStr, panLen);
	

	iRet = GetPIN(InputPINStr, &pinLen);
	DPRINTF("pinlen=%d\n",pinLen);
	switch(iRet)
	{
		case 0:
			//OK
			break;

		case -1:
			OutBuff[3]	= ERR_CANCEL;
			*pOutLen = 4;
			return;

		case -2:
			OutBuff[3]	= ERR_TIMEOUT;
			*pOutLen = 4;
			return;

		default:
			return;
	}

	
	if (PINPad_para.KeyMode == KEY_MODE_3_LAYER_D_D || PINPad_para.KeyMode == KEY_MODE_DUKPT)
	{
		DPRINTF("PINPad_para.KeyMode %c\n",PINPad_para.KeyMode);
	
		if (BuildPinBlock(InputPINStr, pinLen, formatPINBlock)!= 0)
		{
			DPRINTF("Building PIN block failed\n");
			memset(InputPANStr,0,sizeof(InputPANStr));
			memset(InputPINStr,0,sizeof(InputPINStr));
			return;
		}

		if (BuildPanBlock(InputPANStr, panLen, formatPANBlock) != 0)
		{
			DPRINTF("BuildPanBlock failed");
			memset(InputPANStr,0,sizeof(InputPANStr));
			memset(InputPINStr,0,sizeof(InputPINStr));
			memset(formatPINBlock,0,sizeof(formatPINBlock));
			memset(formatPANBlock,0,sizeof(formatPANBlock));
			
			return;
		}
		DPRINTF("formatPINBlock list\n");
		hexdump(formatPINBlock, 8);
		DPRINTF("formatPANBlock list\n");
		hexdump(formatPANBlock, 8);
	
		Xor(formatPINBlock, formatPANBlock);
		DPRINTF("After Xor formatPINBlock list\n");
		hexdump(formatPINBlock, 8);

		if (PINPad_para.KeyMode == KEY_MODE_3_LAYER_D_D)
		{
			MSRequestPinEncEx(InBuff[0] - '0', formatPINBlock, OutBuff, pOutLen);
		}else
		{
			DUKPTRequestPINEncEx(formatPINBlock, OutBuff, pOutLen);
			DPRINTF("DUKPTRequestPINEncEx outbuff list");
			hexdump(OutBuff, *pOutLen);
			OutBuff[*pOutLen] = 0;
			DPRINTF("DUKPTRequestPINEncEx string = %s", OutBuff);
		}
		
	}
	else if (PINPad_para.KeyMode == KEY_MODE_AES_FIXED_KEY)
	{
		DPRINTF("KEY_MODE_AES_FIXED_KEY\n");
	
		iRet = BuildPinBlockFormat4(InputPINStr, pinLen, formatPINBlock);
		if (iRet != 0)
		{
			DPRINTF("BuildPinBlockFormat4 error\n");
			memset(InputPANStr,0,sizeof(InputPANStr));
			memset(InputPINStr,0,sizeof(InputPINStr));
			return;
		}

		iRet = BuildPanBlockFormat4(InputPANStr, panLen, formatPANBlock);
		if (iRet != 0)
		{
			DPRINTF("BuildPanBlockFormat4 error\n");
			memset(InputPANStr,0,sizeof(InputPANStr));
			memset(InputPINStr,0,sizeof(InputPINStr));
			memset(formatPINBlock,0,sizeof(formatPINBlock));
			memset(formatPANBlock,0,sizeof(formatPANBlock));
			return;
		}
	
		AesFixedKeyRequestPINEncEx(formatPINBlock, formatPANBlock, OutBuff, pOutLen);
	}else
	{
		DPRINTF("PINPad_para.KeyMode error\n");
		return;
	}

	memset(InputPANStr,0,sizeof(InputPANStr));
	memset(InputPINStr,0,sizeof(InputPINStr));
	memset(formatPINBlock,0,sizeof(formatPINBlock));
	memset(formatPANBlock,0,sizeof(formatPANBlock));

	SetEnterPinLastTick(get_tick());

	memcpy (OutBuff, "91.", 3);
    DPRINTF("online pin success\n");
    hexdump(OutBuff,*pOutLen);
	return;
}

void RequestPANEnc(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	//int pinLen;
	byte panLen;
  	char InputPINStr[13];
	char InputPANStr[20];
	byte formatPINBlock[16];
	byte formatPANBlock[16];
	PINPAD_PARA PINPad_para;

	memcpy (OutBuff, "93.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	//DPRINTF("InBuff =%s\n", InBuff);

	iRet = LoadSetting(&PINPad_para);
    if (iRet != 0)
    {
        DPRINTF("LoadSetting error\n");
        return;
    }
	
    if (PINPad_para.KeyMode == KEY_MODE_UN_INIT)
    {
    	DPRINTF("KeyMode KEY_MODE_UN_INIT\n");
        return;
    }

	panLen = (InBuff[1] - '0')*10 + (InBuff[2] - '0');
	if (panLen < 2 || panLen > 19)
	{
		DPRINTF("panLen error\n");
		return;
	}

	if (panLen + 3 != InLen)
	{
		DPRINTF("Len error\n");
		return;
	}

    memcpy(InputPANStr, InBuff + 3, panLen);
	DPRINTF("List InputPANStr");
	hexdump((byte *)InputPANStr, panLen);
	
	if (PINPad_para.KeyMode == KEY_MODE_3_LAYER_D_D || PINPad_para.KeyMode == KEY_MODE_DUKPT)
	{
		DPRINTF("PINPad_para.KeyMode %c\n",PINPad_para.KeyMode);
	
		if (BuildPanBlock(InputPANStr, panLen, formatPANBlock) != 0)
		{
			DPRINTF("BuildPanBlock failed");
			memset(InputPANStr,0,sizeof(InputPANStr));
			memset(InputPINStr,0,sizeof(InputPINStr));
			memset(formatPINBlock,0,sizeof(formatPINBlock));
			memset(formatPANBlock,0,sizeof(formatPANBlock));
			
			return;
		}
		
		DPRINTF("formatPANBlock list\n");
		hexdump(formatPANBlock, 8);

		if (PINPad_para.KeyMode == KEY_MODE_3_LAYER_D_D)
		{
			MSRequestPanEncEx(InBuff[0] - '0', formatPANBlock, OutBuff, pOutLen);
		}else
		{
			DUKPTRequestPANEncEx(formatPANBlock, OutBuff, pOutLen);
			//DPRINTF("DUKPTRequestPINEncEx outbuff list");
			//hexdump(OutBuff, *pOutLen);
			OutBuff[*pOutLen] = 0;
			//DPRINTF("DUKPTRequestPINEncEx string = %s", OutBuff);
		}
		
	}
	else if (PINPad_para.KeyMode == KEY_MODE_AES_FIXED_KEY)
	{
		DPRINTF("KEY_MODE_AES_FIXED_KEY\n");

		iRet = BuildPanBlockFormat4(InputPANStr, panLen, formatPANBlock);
		if (iRet != 0)
		{
			DPRINTF("BuildPanBlockFormat4 error\n");
			memset(InputPANStr,0,sizeof(InputPANStr));
			memset(InputPINStr,0,sizeof(InputPINStr));
			memset(formatPINBlock,0,sizeof(formatPINBlock));
			memset(formatPANBlock,0,sizeof(formatPANBlock));
			return;
		}
		//hexdump(formatPANBlock, 16);
	
		AesFixedKeyRequestPANEncEx(formatPANBlock, OutBuff, pOutLen);
	}else
	{
		DPRINTF("PINPad_para.KeyMode error\n");
		return;
	}

	memset(InputPANStr,0,sizeof(InputPANStr));
	memset(InputPINStr,0,sizeof(InputPINStr));
	memset(formatPINBlock,0,sizeof(formatPINBlock));
	memset(formatPANBlock,0,sizeof(formatPANBlock));

	memcpy (OutBuff, "93.", 3);
    //DPRINTF("online pin success\n");
    hexdump(OutBuff,*pOutLen);
	return;
}


void RequestTrackEnc(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	byte inputTrackStr[200];
	byte formatTrack[8];
	int trackLen;
	PINPAD_PARA PINPad_para;

	memcpy (OutBuff, "94.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	//DPRINTF("InBuff =%s\n", InBuff);
		//30 seconds can call this function one time
	if (get_diff_tick(get_tick(), GetEncTrackLastTick()) < 30*1000)
	{
		DPRINTF("30 seconds can call this function one time, please wait\n");
		return;
	}else
	{
		DPRINTF("30 seconds timeout\n");
	}

	iRet = LoadSetting(&PINPad_para);
    if (iRet != 0)
    {
        DPRINTF("LoadSetting error\n");
        return;
    }
	
    if (PINPad_para.KeyMode == KEY_MODE_UN_INIT)
    {
    	DPRINTF("KeyMode KEY_MODE_UN_INIT\n");
        return;
    }

	trackLen = (InBuff[1] - '0')*10 + (InBuff[2] - '0');
	if (trackLen < 19)
	{
		DPRINTF("trackLen error\n");
		return;
	}

	if (trackLen + 3 != InLen)
	{
		DPRINTF("Len error, trackLen = %d, InLen = %d\n", trackLen, InLen);
		return;
	}

    memcpy(inputTrackStr, InBuff + 3, trackLen);
	inputTrackStr[trackLen] = 0x00;
	DPRINTF("List inputTrackStr\n");
	hexdump(inputTrackStr, trackLen);
	
	if (PINPad_para.KeyMode == KEY_MODE_3_LAYER_D_D)
	{
		MSRequestTrackEncEx(InBuff[0] - '0', inputTrackStr, trackLen, OutBuff, pOutLen);
		
	}else
	{
		DPRINTF("PINPad_para.KeyMode error\n");
		return;
	}

	memset(inputTrackStr,0,sizeof(inputTrackStr));
	memset(formatTrack,0,sizeof(formatTrack));
	
	SetEncTrackLastTick(get_tick());
	memcpy (OutBuff, "94.", 3);
  	hexdump(OutBuff,*pOutLen);
	
	return;
}

void StartInjectKey(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	PINPAD_PARA PINPad_para;

	DPRINTF("StartInjectKey\n");

	memcpy (OutBuff, "A0.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	//InBuff[InLen] = 0x00;

	//DPRINTF("InBuff =%s\n", InBuff);

	iRet = LoadSetting(&PINPad_para);
    if (iRet != 0)
    {
        DPRINTF("LoadSetting error\n");
        return;
    }

	DPRINTF("SecEraseAllKey 1 times\n");
	//SecEraseAllKey();

	//PINPad_para.KeyMode = KEY_MODE_UN_INIT;
	//SaveSetting(&PINPad_para);

	OutBuff[3] = ERR_OK;				// assume error
	////Line;
	return;
}



void InjectTerminalSN(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	byte terminalSN[OTP_TERMINAL_SN_LEN];

	DPRINTF("InjectTerminalSN\n");

	memcpy (OutBuff, "B0.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	//DPRINTF("InBuff =%s\n", InBuff);
#ifndef FACTORY_APP
	if (InLen != (OTP_TERMINAL_SN_LEN - 3))
	{
		////Line;
		dbg("InLen error\n");
		return;
	}
#else
	if (InLen != (OTP_TERMINAL_SN_LEN - 4))
	{
		////Line;
		dbg("InLen error\n");
		return;
	}
#endif
/*
	if (AtoHex(terminalSN,InBuff, InLen) != 0)
	{
		dbg("AtoHex error\n");
		return;
	}
*/
	memset(terminalSN, 0x00, sizeof(terminalSN));
#ifndef FACTORY_APP
	memcpy(terminalSN, InBuff, OTP_TERMINAL_SN_LEN - 3);
#else
	memcpy(terminalSN, InBuff, OTP_TERMINAL_SN_LEN - 4);
#endif

	iRet = SecWriteTerminalSN(terminalSN);
	if (iRet != 0)
	{
		////Line;
		dbg("SecWriteTerminalSN error\n");
		return;
	}

	hexdumpEx("List terminalSN", terminalSN, OTP_TERMINAL_SN_LEN);

	OutBuff[3]	= ERR_OK;				// assume error
	return;
}

void InjectServerAuthKey(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	byte serverAuthKey[OTP_TERMINAL_SERVER_AUTH_KEY_LEN];

	DPRINTF("InjectServerAuthKey\n");

	memcpy (OutBuff, "B1.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	//DPRINTF("InBuff =%s\n", InBuff);

	if (InLen != (OTP_TERMINAL_SERVER_AUTH_KEY_LEN*2))
	{
		dbg("InLen error\n");
		return;
	}

	if (AtoHex(serverAuthKey, (char *)InBuff, InLen) != 0)
	{
		dbg("AtoHex error\n");
		return;
	}

	iRet = SecWriteServerAuthKey(serverAuthKey);
	if (iRet != 0)
	{
		dbg("SecWriteTerminalSN error\n");
		return;
	}

	hexdumpEx("List serverAuthKey", serverAuthKey, OTP_TERMINAL_SERVER_AUTH_KEY_LEN);
	
	OutBuff[3]	= ERR_OK;				// assume error
	return;
}


void InjectLocationMacKey(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	byte locationMacKey[OTP_TERMINAL_LOCATION_MAC_KEY_LEN];

	DPRINTF("InjectLocationMacKey\n");

	memcpy (OutBuff, "B2.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	//DPRINTF("InBuff =%s\n", InBuff);

	if (InLen != (OTP_TERMINAL_LOCATION_MAC_KEY_LEN*2))
	{
		dbg("InLen error\n");
		return;
	}

	if (AtoHex(locationMacKey, (char *)InBuff, InLen) != 0)
	{
		dbg("AtoHex error\n");
		return;
	}

	iRet = SecWriteLocationMacKey(locationMacKey);
	if (iRet != 0)
	{
		dbg("SecWriteLocationMacKey error\n");
		return;
	}

	hexdumpEx("List locationMacKey", locationMacKey, OTP_TERMINAL_LOCATION_MAC_KEY_LEN);


	OutBuff[3]	= ERR_OK;				// assume error
	return;
}

void RequestTerminalSN(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	byte terminalSN[OTP_TERMINAL_SN_LEN+1]={0};

	DPRINTF("RequestTerminalSN\n");

	memcpy (OutBuff, "B3.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	iRet = SecReadTerminalSN(terminalSN);
    if( iRet==0 && ((terminalSN[0]>='0' && terminalSN[0]<='9') || (terminalSN[0]>='a' && terminalSN[0]<='z')
        || (terminalSN[0]>='A' && terminalSN[0]<='Z') ) )
    {
        hexdumpEx("List terminalSN", terminalSN, strlen((char*)terminalSN));
        OutBuff[3]	= ERR_OK;
        strcpy((char*)OutBuff+4, (char*)terminalSN);
        *pOutLen = 4+strlen((char*)terminalSN);
        
        return;
    }
    
    dbg("SecReadTerminalSN error:%d\n", iRet);
    return;
}

extern int readMainBoardSN(char *sn);
void RequestMainboardSN(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	char mainboardSN[20+1]={0};    

	DPRINTF("RequestMainBoardSN\n");

	memcpy (OutBuff, "B5.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	iRet = readMainBoardSN(mainboardSN);
    if( iRet==0 )
    {
        hexdumpEx("List mainBoardSN", (byte *)mainboardSN, strlen(mainboardSN));
        OutBuff[3]	= ERR_OK;
        strcpy((char*)OutBuff+4, mainboardSN);
        *pOutLen = 4+strlen(mainboardSN);
        
        return;
    }
    
    dbg("readMainBoardSN error:%d\n", iRet);
    return;
}

extern int iGprsFacTest(char *pszIMEI, char *pszICCID);
void RequestICCID(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	int iRet;
	char iccid[30+1]={0};

	DPRINTF("RequestICCID\n");

	memcpy (OutBuff, "B4.", 3);
	OutBuff[3]	= ERR_RANGE;				// assume error
	*pOutLen = 4;

	iRet = iGprsFacTest(NULL, iccid);
    if( iRet==0 )
    {
        //hexdumpEx("List terminalSN", terminalSN, strlen((char*)terminalSN));
        OutBuff[3]	= ERR_OK;
        strcpy((char*)OutBuff+4, iccid);
        *pOutLen = 4+strlen(iccid);
        
        return;
    }
    
    dbg("iGprsFacTest error:%d\n", iRet);
    return;
}

extern void vDispTermQrCode(int iFlag);
void DispTermInfoQrCode(byte *InBuff, int InLen, byte *OutBuff, int *pOutLen)
{
	DPRINTF("DispTermInfoQrCode\n");

	memcpy (OutBuff, "B6.", 3);
    vDispTermQrCode(2);

    OutBuff[3]	= ERR_OK;
    *pOutLen = 4;
    
    return;
}


// Command 62
void InjectTusnKey(byte *InBuff, int InLen, byte *OutBuff, int *OutBuffLen)
{
	byte KeyTransform;
	byte TransKeyL[8] = {0};
	byte TransKeyR[8] = {0};
	byte SerialNumber[8] = {0};
	byte KeyValue[16] = {0};
	
	memcpy( OutBuff, "62.", 3);
	OutBuff[3] = ERR_RANGE;
	*OutBuffLen = 4;

	if (InLen != 49)
	{
		DPRINTF("InLen error\n");
		return;
	}


	KeyTransform = InBuff[0];		// get and check key transform type parameter
	if (KeyTransform != KEY_TRANSFORM_CLR_KTK)
		return;

	if (AtoHex(TransKeyL, (char *)(InBuff + 1), 16))
		return;

	if (AtoHex(TransKeyR, (char *)(InBuff+1+16), 16))
		return;

	if (AtoHex(SerialNumber, (char *)(InBuff+1+16+16), 16))
		return;

	//SecEraseAllKey();

	memcpy(KeyValue, TransKeyL, 8);
	memcpy(KeyValue + 8, TransKeyR, 8);	

	SecWriteTusnKey(KeyValue);
	OutBuff[3] = ERR_OK;
	*OutBuffLen = 4;

	return;
}

#define DEFAULT_DES3_ZMK_STRING	"E326F7024A0107857A85B37F736ED9F1"	

// Command 63
void InjectTusnKeyViaZMK(byte *InBuff, int InLen, byte *OutBuff, int *OutBuffLen)
{
	byte KeyTransform;
	byte TransKeyL[8] = {0};
	byte TransKeyR[8] = {0};
	byte SerialNumber[8] = {0};
	byte encKeyValue[16] = {0};
	byte byTusnKey[16] = {0};
	byte byDefaultZMK[16] = {0};
	byte byZMKCheckValue[3];
	byte byCalZMKCheckValue[8];
	char szZMKCheckValue[10];
	
	memcpy(OutBuff, "63.", 3);
	OutBuff[3] = ERR_RANGE;
	*OutBuffLen = 4;

	if (InLen != 49+6)
	{
		DPRINTF("InLen error\n");
		return;
	}

	KeyTransform = InBuff[0];		// get and check key transform type parameter
	if (KeyTransform != KEY_TRANSFORM_CLR_KTK)
	{
		REPORT_ERR_LINE();
		return;
	}
	if (AtoHex(TransKeyL, (char *)(InBuff + 1), 16))
	{
		REPORT_ERR_LINE();
		return;
	}
	if (AtoHex(TransKeyR, (char *)(InBuff+1+16), 16))
	{	
		REPORT_ERR_LINE();
		return;
	}
	if (AtoHex(SerialNumber, (char *)(InBuff+1+16+16), 16))
	{	
		REPORT_ERR_LINE();
		return;
	}
	memcpy(encKeyValue, TransKeyL, 8);
	memcpy(encKeyValue + 8, TransKeyR, 8);	

	memcpy(szZMKCheckValue, InBuff + 49, 6);
	
	if (AtoHex(byDefaultZMK, DEFAULT_DES3_ZMK_STRING, 32))
	{	
		REPORT_ERR_LINE();
		return;
	}
	if (AtoHex(byZMKCheckValue, szZMKCheckValue, 6))
	{	
		REPORT_ERR_LINE();
		return;
	}
	
	Des3ECB(byDefaultZMK, encKeyValue, 16 , byTusnKey, SEC_DEC);

	Des3ECB(byTusnKey, "\x00\x00\x00\x00\x00\x00\x00\x00", 8, byCalZMKCheckValue, SEC_ENC);

	//hexdumpEx("List encKeyValue", encKeyValue, 16);
	//hexdumpEx("List byTusnKey", byTusnKey, 16);
	//hexdumpEx("List byCalZMKCheckValue", byCalZMKCheckValue, 8);
	//hexdumpEx("List byZMKCheckValue", byZMKCheckValue, 3);

	if (memcmp(byCalZMKCheckValue, byZMKCheckValue, 3) != 0)
	{
		DPRINTF("ZMK Check Value err\n");
		return;
	}

	SecWriteTusnKey(byTusnKey);
	OutBuff[3] = ERR_OK;
	*OutBuffLen = 4;

	return;
}

typedef void (*SECCMDFUNC)(byte *, int, byte *, int *);

typedef struct {
	char *name;
	SECCMDFUNC func;
} SECSCommand;

SECSCommand outside_sec_cmdlist[] = {
	//{"40.", LoadMasterSessionKey},
	//{"43.", AsciiHexMacCalculation},
	{"52.", LoadSpecialKey},
	{"62.", InjectTusnKey},
    {"63.", InjectTusnKeyViaZMK},
	//{"60.", LoadMasterSessionKeyByKBPK},
	//{"70.", LoadInitialKey},
	//{"72.", RequestMacCode},
	//{"80.", LoadAesFixedKey},
	//{"91.",	RequestPINEnc},
	//{"92.", RequestOfflinePIN},
	{"A0.", StartInjectKey},
	{"B0.", InjectTerminalSN},
	//{"B1.", InjectServerAuthKey},
	//{"B2.", InjectLocationMacKey},
    {"B3.", RequestTerminalSN},
    {"B4.", RequestICCID},
    {"B5.", RequestMainboardSN},
    {"B6.", DispTermInfoQrCode},
	//{"TS.", TestDES},
};

SECSCommand internal_sec_cmdlist[] = {
	//{"40.", LoadMasterSessionKey},
	//{"43.", AsciiHexMacCalculation},
	//{"60.", LoadMasterSessionKeyByKBPK},
	//{"72.", RequestMacCode},
	//{"93.", RequestPANEnc},
	//{"91.",	RequestPINEnc},
	//{"92.", RequestOfflinePIN},
	//{"94.", RequestTrackEnc},
	{"A0.", StartInjectKey},
};


void OutsidePinpadCmd(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{
	int cnt;
	int commandnumber = sizeof(outside_sec_cmdlist) / sizeof(SECSCommand);

	for(cnt=0; cnt<commandnumber; cnt++)
	{
		if(memcmp_ex(InBuff, outside_sec_cmdlist[cnt].name, CMD_CODE_SIZE) == 0)
		{
			//printf("cmdlist[cnt].name = [%s]\n", cmdlist[cnt].name);
			InLen -= CMD_CODE_SIZE;
			outside_sec_cmdlist[cnt].func(InBuff+CMD_CODE_SIZE, InLen, OutBuff, OutLen);
			break;
		}
	}

	if(cnt >= commandnumber)
	{
		memcpy(OutBuff, InBuff, 3);
		OutBuff[3] = 'F';
		*OutLen = 4;
	}
	
	return;
}

void InternalPinpadCmd(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{
	int cnt;
	int commandnumber = sizeof(internal_sec_cmdlist) / sizeof(SECSCommand);

	DPRINTF("InternalPinpadCmd\n");
	hexdumpEx("InternalPinpadCmd list InBuff: ", InBuff, InLen);
	
	for(cnt=0; cnt<commandnumber; cnt++)
	{
		//DPRINTF("cmdlist[cnt].name = [%s]\n", internal_sec_cmdlist[cnt].name);
		if(memcmp_ex(InBuff, internal_sec_cmdlist[cnt].name, CMD_CODE_SIZE) == 0)
		{
			InLen -= CMD_CODE_SIZE;
			internal_sec_cmdlist[cnt].func(InBuff+CMD_CODE_SIZE, InLen, OutBuff, OutLen);
			//DPRINTF("OutLen = %d\n", *OutLen);
			break;
		}
	}

	if(cnt >= commandnumber)
	{
		memcpy(OutBuff, InBuff, 3);
		OutBuff[3] = 'F';
		*OutLen = 4;
	}
	
	return;
}

int SaveTMK(byte *TMK)
{
	int iRet;
	KEY_ATTR LoadKey;
	PINPAD_PARA PINPad_para; 
	byte SerialNumber[10];

	LoadSetting(&PINPad_para);

	SecEraseAllKey();
	memset(SerialNumber, 0x00, sizeof(SerialNumber));
	
	LoadKey.level = KEY_LEVEL_KTK;
	LoadKey.idx = 0;
	LoadKey.len = KEY_LEN_DOUBLE;
	LoadKey.type = KEY_TYPE_OTHERS;
	memcpy(LoadKey.sn, SerialNumber, 8);
	LoadKey.dec_level = KEY_LEVEL_NONE;
	LoadKey.dec_idx = 0;

	iRet = SecWriteKey(&LoadKey, TMK);
	ReportLine();
	if (iRet < 0)
	{
		PINPad_para.KeyMode = KEY_MODE_UN_INIT;
		SaveSetting(&PINPad_para);
		
		return -1;
	}

	PINPad_para.KeyMode = KEY_MODE_3_LAYER_D_D;
	PINPad_para.MSKeyLoadMode = MASTER_SESSION_KEY_LOAD_MODE_UN_INIT;
	SaveSetting(&PINPad_para);
	
	return 0;
}

int LoadTMK(byte *TMK)
{
	int iRet;
	KEY_ATTR LoadKey;
	PINPAD_PARA PINPad_para; 

	LoadSetting(&PINPad_para);

	if (PINPad_para.KeyMode != KEY_MODE_3_LAYER_D_D)
	{
		ReportLine();
		return -1;
	}

	LoadKey.level = KEY_LEVEL_KTK;
	iRet = SecReadKey(&LoadKey, TMK);
	if (iRet != 0)
	{
		ReportLine();
		return -2;
	}

	return 0;
}

int EraseTMK(void)
{
    PINPAD_PARA PINPad_para;

    LoadSetting(&PINPad_para);

    SecEraseAllKey();

    PINPad_para.KeyMode = KEY_MODE_UN_INIT;
    SaveSetting(&PINPad_para);

    return 0;
}

extern int SecReadTusnKey(byte *tusnKey);
int LoadTusnKey(byte *tusnKey)
{
	int ret;

	ret = SecReadTusnKey(tusnKey);
	if (ret != 0)
	{
		return ret;
	}

	return 0;
}
