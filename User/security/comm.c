/********************************************************************************

 *************************Copyright (C), 2017, ScienTech Inc.**************************

 ********************************************************************************
 * File Name     : comm.c
 * Author        : Jay
 * Date          : 2017-02-08
 * Description   : .C file function description
 * Version       : 1.0
 * Function List :
 * 
 * Record        :
 * 1.Date        : 2017-02-08
 *   Author      : Jay
 *   Modification: Created file
 * This file is used to receive Command from Outside device like KIS and then execute it.

********************************************************************************/
//#include <MAX325xx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
//#include <cdc_acm.h>

/** Local includes */
#include "comm.h"
#include "common.h"
#include "command.h"
#include "serial.h"
//#include "watchdog.h"
#include "debug.h"
#include "sysTick.h"
#include "keyboard.h"
#include "Sec_common.h"
#include "Lcdgui.h"
#include "user_projectconfig.h"


#define STX 0x02
#define ETX 0x03
#define ACK 0x06
#define ETB 0x17

/*use for data transfer more than 1024 bytes*/
static unsigned char *pGETBData = NULL; 

#define USB_UART           UARTUSB

//#define DPA_TEST_MODE	

int Execute_Cmd(unsigned char byCmdID, unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen);

unsigned char *getETBBuffer(int length){
    freeETBBuffer();
    pGETBData = malloc(length);
    if(pGETBData==NULL){
        dbg("malloc %d bytes memory failed\n",length);
    }
    return pGETBData;
}

void freeETBBuffer(void){
    if(pGETBData!=NULL){
        free(pGETBData);
        pGETBData = NULL;
    }
}

unsigned char *getETBBufferPoint(void){
    return pGETBData;
}

static unsigned char ACK_PACKAGE[]={0x02,0x06,0x03};
#define ACKLEN  3
void sendack(void)
{
	dbg("sendack()...\n");
	serialwrite(USB_UART, ACK_PACKAGE, ACKLEN);
	
}

int getack(void)
{
	int len;
	unsigned char buf[ACKLEN];
	len=serialread(USB_UART, buf, ACKLEN, 500);
	if(len != ACKLEN || memcmp(buf,ACK_PACKAGE,ACKLEN))
	{
		dbg("get ack err %d\n",len);
		return -1;
	}
	return 0;
}


void sendresponse(char cmd,char sta,unsigned char data)
{
	unsigned char buf[5]={0x02,0x00,0x00,0x00,0x03};
	buf[1]=cmd;
	buf[2]=sta;
	buf[3]=data;
	serialwrite(USB_UART, buf,sizeof(buf));
	getack();
}

/*STX CTL LEN1 LEN0 DATA... ETX LRC
length = length of DATA
*/
int getHostcommand(unsigned char *cmdid, unsigned char *data, int *datalen)
{
	int len;
	int cmddatalen;
	unsigned char buf[MAXPACKETLEN];
	unsigned char lrc,etbLRC=0;
    int etbttlen = 0;
    unsigned char *petb;

    /*prepare for command*/
	freeETBBuffer();

	do{
		len = serialread(USB_UART, buf, 1, 500);
		if(len == 1 && buf[0] == STX)
		{
			len = serialread(USB_UART, buf + 1, 3, 500);
			if (len == 3)
			{
				cmddatalen = (256* buf[2]) + buf[3];
				len = serialread(USB_UART, buf + TLHEADLEN, cmddatalen + 2, 1000);
                dbg("read len = %d ; cmddatalen = %d\n",len,cmddatalen);
				//hexdumps(buf,32);
				if (len == cmddatalen + 2)
				{
                    if((buf[TLHEADLEN + cmddatalen] == ETX) || (buf[TLHEADLEN + cmddatalen] == ETB)){
    					lrc = CalculateLRC(buf, cmddatalen + TLHEADLEN + 1);
					#ifndef DPA_TEST_MODE
    					if (buf[cmddatalen + TLHEADLEN + 1] == lrc)
					#endif
    					{			
                            *cmdid = buf[1];
                            if(buf[TLHEADLEN + cmddatalen] == ETX){
                                if(etbttlen && pGETBData != NULL){
                                    /*last etb package*/
                                    memcpy(pGETBData+etbttlen, buf + TLHEADLEN, cmddatalen);
                                    etbttlen += cmddatalen;
                                    if(etbttlen != *datalen){
                                        dbg("etbdatalen error %d %d\n",etbttlen ,*datalen);
                                        freeETBBuffer();
                                        return -4;
                                    }
                                    //check lrc
                                    lrc = CalculateLRC(pGETBData, etbttlen);
                                    dbg("last etb,lrc=%x\n",lrc);
                                    if(lrc!=etbLRC){
                                        dbg("etb lrc error,lrc=%x %x \n",lrc,etbLRC);
                                        freeETBBuffer();
                                        return -5;
                                    }
                                }else{
            						memcpy(data, buf + TLHEADLEN, cmddatalen);
            						*datalen = cmddatalen;
            						memset(buf, 0, sizeof(buf));
                                }
							#ifndef DPA_TEST_MODE	
                                sendack();
							#endif
        						return 0;
                            }else if(buf[TLHEADLEN + cmddatalen] == ETB){
                                /*first ETB package,first 4 bytes is head.
                                RES LRC TTLEN0 TTLEN1
                                */
                                if(etbttlen == 0){
                                    etbLRC = buf[4];
                                    *datalen = ((buf[5]<<16)+(buf[6]<<8)+buf[7]);
                                    dbg("etb ttlen = %dbytes %x\n",*datalen,etbLRC);
                                    petb = getETBBuffer(*datalen);
                                    if(petb==NULL){
                                        dbg("malloc etb buffer error\n");
                                        return -3;
                                    }
                                    
                                    memcpy(pGETBData+etbttlen, buf + TLHEADLEN + ETBHEADLEN, cmddatalen- ETBHEADLEN);
                                    etbttlen += cmddatalen - ETBHEADLEN;
                                    sendack();
                                    memset(buf,0,sizeof(buf));
                                    dbg("etb first %d bytes\n",etbttlen);
                                    continue;
                                }                                
                                memcpy(pGETBData+etbttlen, buf + TLHEADLEN, cmddatalen);
                                etbttlen += cmddatalen;
                                sendack();
                                memset(buf,0,sizeof(buf));
                                //dbg("etb %d bytes\n",etbttlen);
                                continue;
                            }
    					}
					#ifndef DPA_TEST_MODE
						else
						{
    						dbg("LRC error clrc=%x %x\n",lrc,buf[cmddatalen + TLHEADLEN + 1]);
                            break;
            }
					#endif
                    }else{
                        dbg("unknown etx/etb\n");
                        break;
                    }
				}else{
				    dbg("ttlen error\n");
					dbg("len = %d\n", len);
                    break;
                }
			}else{
                    dbg("len error\n");
                    break;
            }
		}else{
            //dbg("stx error\n");
            break;
        }
	}while(1);
	return -1;
}

void sendHostresponse(unsigned char cmdid, unsigned char *rsp, int rsplen)
{
	unsigned char buf[MAXPACKETLEN];
    int ret;
    if(rsplen<=MAXPACKETDATALEN){
        //STX 
    	buf[0] = 0x02;

    	//CMD ID
    	buf[1] = cmdid;
    	//Rsp len, 2 byte
    	buf[2] = (rsplen>>8)&0xff;
    	buf[3] = rsplen&0xff;

    	//Rsp data
    	memcpy(buf + 4, rsp, rsplen);

    	//ETX
    	buf[rsplen + 4] = 0x03;

    	//LRC
    	buf[rsplen + 5] = CalculateLRC(buf, rsplen + 5);

        //2.send res
    	ret=serialwrite(USB_UART, buf, rsplen + 6);
        if(ret!=(rsplen + 6)) {
            dbg("send data err\n");
            return ;
        }

        //3.get ack
    #ifndef DPA_TEST_MODE
        if (getack() != 0){
		    dbg("getack err cmdid = 0x%02x\n", cmdid);
	    }
	#endif
    }else{
        unsigned short plen=0,dlen,oft=0,ttdatalen=rsplen-1;
        unsigned char lastone = 0,ttlrc=0;
        if(pGETBData == NULL){
            dbg("pGETBData is null\n");
            return;
        }
        /*respen include 1 state byte length,rsp include state byte*/
        ttlrc =CalculateLRC(pGETBData, ttdatalen);
        dbg("ETB Packet,ttdatalen=%d,ttlrc=0x%x\n",ttdatalen,ttlrc);
        do{
            plen = 0;
            /*check if last packet*/
            if((ttdatalen - oft) < MAXPACKETDATALEN)
                lastone = 1;
            
            buf[plen++] = STX;
            buf[plen++] = cmdid;
            dlen = ((ttdatalen-oft)>MAXPACKETDATALEN)?MAXPACKETDATALEN:(ttdatalen-oft);
            buf[plen++] = (dlen>>8)&0xff;
    	    buf[plen++] = dlen&0xff;
            //1.first packet include 4 bytes header(stats+res+ttlen1+ttlen0)
            if(!oft){
                dbg("first ETB packet\n");
                /*status byte*/
                buf[plen++] = rsp[0];
                /*lrc for total data*/
                buf[plen++] = ttlrc ;  
                //total data length
                buf[plen++] = (ttdatalen>>8)&0xff;
    	        buf[plen++] = ttdatalen&0xff;
                dlen -= 4;
            }
            //data area
            memcpy(buf+plen,pGETBData+oft,dlen);
            oft += dlen ;
            plen += dlen;

            //etb or etx
            buf[plen++] = lastone?ETX:ETB;
            //lrc
            buf[plen] = CalculateLRC(buf, plen);
            plen++;
            dbg("plen=%d,lrc=0x%x\n",plen,buf[plen-1]);

            //2.send out packet
            ret=serialwrite(USB_UART,buf,plen);
            if(ret!=plen) {
                dbg("send data err\n");
                return ;
            }

            //3.get ack
            if (getack() != 0){
        		dbg("getack error cmdid = 0x%02x\n", cmdid);
                return;
        	}
            dbg("oft %d %d\n",oft,lastone);
        }while(lastone==0);
    }
}

//清串口
void COMM_clear(void)
{
	uint8_t ch;
	while(serialread(USB_UART, &ch, 1, 100)>0)
	{
	}
}

extern int iGetKeyWithTimeout(unsigned int uiTimeout);
extern void _vFlushKey(void);
extern int  iOK(unsigned short timeout);
extern void vDispTermQrCode(int iFlag);
int endlessloop(void)
{
	int ret;
	int iCmdDatalen;
	int iRspDataLen;
	//FIXME:heap already full?
	unsigned char byArrCmdData[MAXPACKETLEN], byArrRspData[MAXPACKETLEN];
	unsigned char cmdid;
	#ifdef LEDDEBUG
	char onoff=0;
	#endif
	//int chars=1;
	tick ticks=get_tick();
    uint16_t keyram;
    unsigned char lastcmd=0;
    
    unsigned char tusnbak[32+1]={0};
    unsigned char tusn[32+1]={0};
	
    /*
	if ((chars = acm_canread()) <= 0)
    {
		return 0;
	}
    */
    
    COMM_clear();
    
	LcdClear();
	LcdPutsc("等待指令中", 2);
	//LcdPutsc("wait for command", 0);
    KeyBoardClean();
    
    SecReadTerminalSN(tusnbak);
	
	while(!is_timeout(ticks, 30*60*1000))   //30分钟超时
	//while(1)
	{		
		memset(byArrCmdData, 0, sizeof(byArrCmdData));
		memset(byArrRspData, 0, sizeof(byArrRspData));

#if 1
        mdelay(1);
        if (KeyBoardRead(&keyram) == 0 && keyram == KEY_CANCEL)
        {
            return 0;
        }  
#endif        
		ret = getHostcommand(&cmdid, byArrCmdData, &iCmdDatalen);
		if (ret < 0)
		{	
			//return 0;
			//dbg("getHostcommand ret = %d\n", ret);
			continue;
		}
        
        if(lastcmd==0xB6)
            LcdClear();

		LcdPutsc("接受到指令，处理中", 0);
		//LcdPutsc("Received Command", 0);
		
	    Execute_Cmd(cmdid, byArrCmdData, iCmdDatalen, byArrRspData, &iRspDataLen);
        
        if(cmdid==OUTSIDE_PINPAD && memcmp(byArrCmdData, "B6.", 3)==0)
        {
            lastcmd=0xB6;
            sendHostresponse(cmdid, byArrRspData, iRspDataLen);
            #if 0
            iGetKeyWithTimeout(30);
            LcdClear();
            return 0;
            #else
            continue;       //不显示等待指令中,避免破坏二维码
            #endif
        }else
        {
            lastcmd=0;
            
            LcdPutsc("处理完毕， 发送回应", 0);
            //LcdPutsc("Executed Command and send response", 0);
            sendHostresponse(cmdid, byArrRspData, iRspDataLen);
            
            if(cmdid==OUTSIDE_PINPAD && memcmp(byArrCmdData, "B0.", 3)==0 && memcmp(byArrRspData, "B0.0", 4)==0 && tusnbak[0])
            {
                memset(tusn, 0, sizeof(tusn));
                SecReadTerminalSN(tusn);
                //注入了新的序列号,删TMK和tusnkey
                if(memcmp(tusn, tusnbak, sizeof(tusnbak)))
                {
                    EraseTMK();
                    SecEraseTusnKey();
#ifdef APP_LKL  					
                    vLoadTMKForSysInfo(1);
#endif                    
                    memcpy(tusnbak, tusn, sizeof(tusnbak));
                }
            }
            
            if(cmdid==OUTSIDE_PINPAD && memcmp(byArrCmdData, "52.", 3)==0)
            {
                LcdClear();
                if(memcmp(byArrRspData, "52.0", 4)==0)
                {         
#ifdef APP_LKL                    
                    vLoadTMKForSysInfo(0);
#endif                   
                    _vFlushKey();
                    LcdPutsc("密钥注入成功", 3);                    
                    //iGetKeyWithTimeout(5);
                    iOK(5);
                    vDispTermQrCode(0);
                    LcdClear();
                    return 0;
                }
                LcdPutsc("密钥注入失败", 3);
                iGetKeyWithTimeout(5);
            }
            LcdClear();
        }
		//LcdPutsc("wait for command", 0);
		LcdPutsc("等待指令中", 0);		
	}
	return 0;
}

SCommand cmdlist[] = {
	//{DPA_TEST, Cmd_DPA_Test},
	{GETVERSION,Cmd_GetVersion},
	{OUTSIDE_PINPAD, Cmd_Outside_PINPad},
	{INTERNAL_PINPAD, Cmd_Internal_PINPad},
#ifdef FACTORY_APP
	{GET_TUSN, Cmd_Get_TUSN},	
	{GET_KEY_STATUS, Cmd_Get_KeyStatus},
	//{GET_HARDWARE_TEST_RESULT, Cmd_Get_HardwareTestResult},
#endif
};


int Execute_Cmd(unsigned char byCmdID, unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen)
{
	int cnt;
	int commandnumber = sizeof(cmdlist) / sizeof(SCommand);

	for(cnt=0; cnt<commandnumber; cnt++)
	{
		if(byCmdID == cmdlist[cnt].id)
		{
			cmdlist[cnt].func(byArrCmdData, iCmdlen, byArrRspData, piRspLen);
			break;
		}
	}
	if(cnt >= commandnumber)
	{
		memcpy(byArrRspData, "F", 1);
		*piRspLen = 1;
	}
	
	return 0;
}

void serialHex(unsigned char *head, unsigned char *in, unsigned int len)
{
#if  0    
	int i;
	unsigned char col[50] = {0};
	unsigned char temp[50] = {0};

  	sprintf(temp,"%s(%d):\n", head, len);	
	serialwrite(USB_UART, temp, strlen(temp));

	for (i = 0; i < len; i++)
	{
	       sprintf(col, "%02X ", in[i]);
	       serialwrite(USB_UART,col,3);	
		   
	       if((i+1)%16==0)
		   	 serialwrite(USB_UART,"\r\n",2);
	}
	
	serialwrite(USB_UART,"\r\n\r\n",4);	
#endif    
}

void serialAsc(const char * sFormat, ...)
{
#if 0    
          int len;
          char szMsg[2048];
         

	  va_list args;
	  va_start(args, sFormat);
	  vsnprintf(szMsg, sizeof(szMsg), sFormat, args);  
	  va_end(args);	

         len=strlen(szMsg);  
         serialwrite(USB_UART,szMsg,len);	  
#endif    
}

