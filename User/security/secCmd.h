#ifndef __SECCMD_H__
#define __SECCMD_H__

int SecInit(void);

void SecGetVersion(char *szVer);

void OutsidePinpadCmd(unsigned char *InBuff, int InLen, unsigned char *OutBuff, int *OutLen);

void InternalPinpadCmd(unsigned char *InBuff, int InLen, unsigned char *OutBuff, int *OutLen);


#endif

