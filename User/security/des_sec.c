#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include "mhscpu.h"
#include "mh_ecc.h"
#include "mh_rsa.h"
#include "mh_rand.h"
#include "mh_bignum.h"
#include "mh_misc.h"
#include "mh_crypt_version.h"
#include "mh_des.h"
#include "mh_aes.h"
#include "mhscpu_sensor.h"
#include "mhscpu_bpk.h"
#include "des_sec.h"
#include "fixvar.h"
#include "sys_littlefs.h"
#include "misce.h"
#include "sysTick.h"
#include "ProjectConfig.h"
#include "utils.h"
#include "util.h"
#include "bpk.h"
#include "sec_common.h"
#include "mh_sm4.h"
#include "sensor.h"

#define CRYPT_INT						0

#define MH_MAX_RSA_MODULUS_WORDS		((MH_MAX_RSA_MODULUS_BYTES + 3) / 4)
#define MH_MAX_RSA_PRIME_WORDS			((MH_MAX_RSA_PRIME_BYTES + 3) / 4)



#define MML_MEM_SNVSRAM_BASE 0x20080000
volatile byte * pSecmem_base = (volatile byte *)0x20080000;
//volatile int * pIntSecmem_base = (volatile int *)MML_MEM_SNVSRAM_BASE;

DUKPT_PARAMETER staDUKPTPara;

DUKPT_PARAMETER* pStaDUKPTPara = NULL;//staDUKPTPara;


unsigned char DUKPT_MacKeyTmp[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x00,
						  0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x00};

unsigned char DUKPT_macCode[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x00, 
						0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x00};

unsigned char DUKPT_varCode[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF, 
						0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF};

unsigned char DUKPT_tmpCode[16] = {0xC0,0xC0,0xC0,0xC0,0x00,0x00,0x00,0x00, 
						0xC0,0xC0,0xC0,0xC0,0x00,0x00,0x00,0x00};

unsigned char DUKPT_tmpData[16] = {0xC0,0xC0,0xC0,0xC0,0x00,0x00,0x00,0x00,
						0xC0,0xC0,0xC0,0xC0,0x00,0x00,0x00,0x00};

#define DES_BLOCK_SIZE      8

#define FILE_NAME_SEC_BACKUP   "Backup"
                        
int Des3ECB(unsigned char* key, unsigned char* in, int len, unsigned char* out, int enc);
int SecEraseTransmissionKey(void);
                        
static int mh_des1(uint8_t *key, uint8_t *text_in, uint8_t *text_out, mh_crypt_mode_def crypt_mode)
{
    mh_des_key_def mh_key;
    mh_rng_callback f_rng = mh_rand_p;
    int ret;

    memset(mh_key, 0, sizeof(mh_key));
    memcpy(mh_key, key, sizeof(mh_key));
    
    if (crypt_mode == ENC) {
        ret = mh_des_enc(ECB, text_out, DES_BLOCK_SIZE, text_in, DES_BLOCK_SIZE, mh_key, NULL, f_rng, NULL);
    } else {
        ret = mh_des_dec(ECB, text_out, DES_BLOCK_SIZE, text_in, DES_BLOCK_SIZE, mh_key, NULL, f_rng, NULL);
    }
    if (ret != MH_RET_DES_SUCCESS) {
        return -1;
    }

    return 0;

}

static int mh_des3(uint8_t *key, uint8_t *text_in, uint8_t *text_out, mh_crypt_mode_def crypt_mode)
{
    mh_tdes_key_def mh_key;
    mh_des_iv_def mh_iv;
    mh_rng_callback f_rng = mh_rand_p;
    int ret;

    memset(&mh_key, 0, sizeof(mh_key));
    memset(mh_iv, 0, sizeof(mh_iv));
    memcpy(&mh_key, key, sizeof(mh_key));
    
    if (crypt_mode == ENC) {
        ret = mh_tdes_enc(ECB, text_out, DES_BLOCK_SIZE, text_in, DES_BLOCK_SIZE, &mh_key, mh_iv, f_rng, NULL);
    } else {
        ret = mh_tdes_dec(ECB, text_out, DES_BLOCK_SIZE, text_in, DES_BLOCK_SIZE, &mh_key, mh_iv, f_rng, NULL);
    }
    if (ret != MH_RET_DES_SUCCESS) {
        return -1;
    }

    return 0;

}

static int mh_des3_ecb(uint8_t *key, uint8_t *text_in, uint32_t text_in_len, uint8_t *text_out, mh_crypt_mode_def crypt_mode)
{
    mh_tdes_key_def mh_key;
    mh_des_iv_def mh_iv;
    mh_rng_callback f_rng = mh_rand_p;
    int ret;

    memset(&mh_key, 0, sizeof(mh_key));
    memset(mh_iv, 0, sizeof(mh_iv));
    memcpy(&mh_key, key, sizeof(mh_key));
    
    if (crypt_mode == ENC) {
        ret = mh_tdes_enc(ECB, text_out, text_in_len, text_in, text_in_len, &mh_key, mh_iv, f_rng, NULL);
    } else {
        ret = mh_tdes_dec(ECB, text_out, text_in_len, text_in, text_in_len, &mh_key, mh_iv, f_rng, NULL);
    }
    if (ret != MH_RET_DES_SUCCESS) {
		err("mh_tdes_enc err, return %d\n ", ret);
        return -1;
    }

    return 0;

}


#define AES_128_BLOCK_SIZE 16
#define AES_128_KEY_BLOCK_SIZE	16
static int mh_aes(uint8_t *key, uint8_t *text_in, uint8_t *text_out, mh_crypt_mode_def crypt_mode)
{
    mh_rng_callback f_rng = mh_rand_p;
    int ret;

	dbg("call mh_aes()\n");

    if (crypt_mode == ENC) {
        ret = mh_aes_enc(ECB, text_out, AES_128_BLOCK_SIZE, text_in, AES_128_BLOCK_SIZE, key, MH_AES_128, NULL, f_rng, NULL);
    } else {
        ret = mh_aes_dec(ECB, text_out, AES_128_BLOCK_SIZE, text_in, AES_128_BLOCK_SIZE, key, MH_AES_128, NULL, f_rng, NULL);
    }
    if (ret != MH_RET_AES_SUCCESS) {
        return -1;
    }

    return 0;
}

static int mh_aes_ecb(uint8_t *key, uint8_t *text_in, uint32_t text_in_len, uint8_t *text_out, mh_crypt_mode_def crypt_mode)
{
    mh_rng_callback f_rng = mh_rand_p;
    int ret;

    if (crypt_mode == ENC) {
        ret = mh_aes_enc(ECB, text_out, text_in_len, text_in, text_in_len, key, MH_AES_128, NULL, f_rng, NULL);
    } else {
        ret = mh_aes_dec(ECB, text_out, text_in_len, text_in, text_in_len, key, MH_AES_128, NULL, f_rng, NULL);
    }
    if (ret != MH_RET_AES_SUCCESS) {
        return -1;
    }

    return 0;
}




/* -------------  D E S ---------------------------------------- */
// Des(): DES encryption/decryption for one 64-bit block. The paramater key
//		  must be 8-byte length (64 bits). And the paramater in & out must
//		  also be 8-byte length.
// in: plaintext, out: ciphertext, bkey: key
// If enc == 0, decryption, otherwise encryption.

static void Des(unsigned char *bkey, unsigned char *in, unsigned char *out, int enc)
{
	mh_crypt_mode_def ucl_enc;

	if (enc == SEC_DEC)
	{
		ucl_enc = DEC;
	}else
	{
		ucl_enc = ENC;
	}

	mh_des1(bkey, in, out, ucl_enc);
	
	//Jason removed on 2019/12/15, need use HM encrypt api
	//ucl_des(out, in, bkey, ucl_enc);
	//Jason removed end
}


// Des3(): Triple DES encryption/decryption for one 64-bit block. The
//		paramater key must be 16-byte length. Paramater in & out must
//		be 8-byte length.
// in: plaintext, out: ciphertext, bkey: key
// If enc == 0, decryption, otherwise encryption.

static void Des3(unsigned char *key, unsigned char *in, unsigned char *out, int enc)
{
	unsigned char des3Key[24];
	mh_crypt_mode_def ucl_enc;

	memcpy(des3Key, key, 8);
	memcpy(des3Key + 8, key + 8, 8);
	memcpy(des3Key + 16, key, 8);

	if (enc == SEC_DEC)
	{
		ucl_enc = DEC;
	}else
	{
		ucl_enc = ENC;
	}
	
	//Jason removed on 2019/12/15, need use HM encrypt api
	//ucl_3des(out, in, des3Key, ucl_enc);
	mh_des3(des3Key, in, out, ucl_enc);
	//Jason removed end 
	
	memset(des3Key, 0, sizeof(des3Key));
}

// DesMac(): Caculate the MAC value of input string by des algorithm.
//		key & mac are 8-byte length.
static void DesMac(unsigned char *key,unsigned char *in, int len, unsigned char *mac)
{
	int uiOffset, i;
	unsigned char sOutMAC[8];

	DPRINTF("DesMac Key : \n");
	//PrintHexStr(pInterParam->DesMac_Para.key, 8);

	memset(sOutMAC, 0, 8);
	uiOffset = 0;

	while (len > uiOffset)
	{
		if ((len - uiOffset) <= 8)
		{
			for( i=0; i < len - uiOffset; i++ )	
				sOutMAC[i] ^= in[uiOffset + i];
			break;
		}

		for(i = 0; i < 8 ; i++ )
			sOutMAC[i] ^= in[uiOffset + i];
				
		uiOffset += 8;

		Des(key, in, sOutMAC, SEC_ENC);
		DPRINTF("loop mac :\n");
	}

	Des(key, sOutMAC, mac, SEC_ENC);
}

// Des3Mac(): Caculate the MAC value of input string by triple des algorithm.
//		key is 16-byte length. mac is 8-byte length.
static void Des3Mac(unsigned char *key, unsigned char *in, int len, unsigned char *mac )
{
	int uiOffset, i;
	unsigned char sOutMAC[8];

	memset(sOutMAC, 0, 8);
	uiOffset = 0;

	while (len > uiOffset)
	{
		if ((len - uiOffset) <= 8)
		{
			for( i=0; i < len - uiOffset; i++ )	
				sOutMAC[i] ^= in[uiOffset + i];
			break;
		}

		for(i = 0; i < 8 ; i++ )
			sOutMAC[i] ^= in[uiOffset + i];
				
		uiOffset += 8;

		Des3(key, sOutMAC, sOutMAC, SEC_ENC);

		//DPRINTF("loop mac :\n");
		//PrintHexStr(pInterParam->DesMac_Para.sOutMAC, 8);
	}

	Des3(key, sOutMAC, mac, SEC_ENC);
	
	//DPRINTF("last mac :\n");
	//PrintHexStr(pInterParam->DesMac_Para.mac, 8);
}


void Des3Mac_UnionPay(unsigned char *key, unsigned char *in, int len, unsigned char *mac )
{
	int uiOffset, i;
	unsigned char sOutMAC[8];
	//unsigned char tmpMAC[8];
	char tmpMACStr[16];
	unsigned char tmpMACStrLeft[8];
	unsigned char tmpMACStrRight[8];
	unsigned char tmpResult[8];
	
	memset(sOutMAC, 0, 8);
	uiOffset = 0;

	while (len > uiOffset)
	{
		if ((len - uiOffset) <= 8)
		{
			for( i=0; i < len - uiOffset; i++ )	
				sOutMAC[i] ^= in[uiOffset + i];
			break;
		}

		for(i = 0; i < 8 ; i++ )
			sOutMAC[i] ^= in[uiOffset + i];
				
		uiOffset += 8;

		//Des3(key, sOutMAC, sOutMAC, SEC_ENC);

		//DPRINTF("loop mac :\n");
		//PrintHexStr(pInterParam->DesMac_Para.sOutMAC, 8);
	}

	HextoA(tmpMACStr, sOutMAC, 8);
	memcpy(tmpMACStrLeft, tmpMACStr, 8);
	memcpy(tmpMACStrRight, tmpMACStr + 8, 8);
	Des3(key, tmpMACStrLeft, tmpResult, SEC_ENC);
	for(i = 0; i < 8 ; i++ )
		tmpMACStrRight[i] ^= tmpResult[i];
	Des3(key, tmpMACStrRight, mac, SEC_ENC);
	
}


// DesCBC(): CBC mode DES encryption (enc != 0), or decryption (enc == 0).
//		The length of in & out need be 8 bytes aligned, so zeros may be padded.
/*
static void DesCBC(unsigned char* key, unsigned char* in, int len, unsigned char* out, int enc)
{
	unsigned char initvector[8];
	unsigned char tmpIn[1024];
	int tmpLen;
	int ucl_enc;

	if (len > 1024)
	{
		return;
	}

	memset(tmpIn, 0x00, sizeof(tmpIn));

	if (len % 8 != 0)
	{
		//Pad 0x00
		memcpy(tmpIn, in, len);
		memset(tmpIn + len, 0x00, 8 - (len % 8));
		tmpLen = len + 8 - (len % 8);
	}else
	{
		memcpy(tmpIn, in, len);
		tmpLen = len;
	}

	if (enc == 0)
	{
		ucl_enc = UCL_CIPHER_DECRYPT;
	}else
	{
		ucl_enc = UCL_CIPHER_ENCRYPT;
	}
	
	memset(initvector, 0x00, sizeof(initvector));
	ucl_des_cbc(out, tmpIn, key, initvector, tmpLen, ucl_enc);

	memset(tmpIn, 0, sizeof(tmpIn));
}
*/

// Des3CBC(): CBC mode Triple DES encryption(enc != 0), or decryption(enc == 0).
//		The length of in & out need be 8 bytes aligned, so zeros may be padded.
/*
static void Des3CBC(unsigned char* key, unsigned char* in, int len, unsigned char* out, int enc)
{
	unsigned char initvector[8];
	unsigned char tmpIn[1024];
	unsigned char tmpKey[24];
	int ucl_enc;
	int tmpLen;

	if (len > 1024)
	{
		return;
	}

	memset(tmpIn, 0x00, sizeof(tmpIn));

	if (len % 8 != 0)
	{
		//Pad 0x00
		memcpy(tmpIn, in, len);
		memset(tmpIn + len, 0x00, 8 - (len % 8));
		tmpLen = len + 8 - (len % 8);
	}else
	{
		memcpy(tmpIn, in, len);
		tmpLen = len;
	}
	
	memset(initvector, 0x00, sizeof(initvector));
	if (enc == 0)
	{
		ucl_enc = UCL_CIPHER_DECRYPT;
	}else
	{
		ucl_enc = UCL_CIPHER_ENCRYPT;
	}
	memcpy(tmpKey, key, 16);
	memcpy(tmpKey + 16, key, 8);
	ucl_3des_cbc(out, tmpIn, key, initvector, tmpLen, ucl_enc);

	memset(tmpKey, 0, sizeof(tmpKey));
	memset(tmpIn, 0, sizeof(tmpIn));
}
*/

// Des3ECB(): ECB mode Triple DES encryption(enc != 0), or decryption(enc == 0).
//		The length of in & out need be 8 bytes aligned, so zeros may be padded.
int Des3ECB(unsigned char* key, unsigned char* in, int len, unsigned char* out, int enc)
{
	unsigned char tmpIn[1024];
	unsigned char tmpKey[24];
	//int uiOffset;
	mh_crypt_mode_def ucl_enc;
	int tmpLen;

	if (len > 1024)
	{
		return -1;
	}

	memset(tmpIn, 0x00, sizeof(tmpIn));

	if (len % 8 != 0)
	{
		//Pad 0x00
		memcpy(tmpIn, in, len);
		memset(tmpIn + len, 0x00, 8 - (len % 8));
		tmpLen = len + 8 - (len % 8);
	}else
	{
		memcpy(tmpIn, in, len);
		tmpLen = len;
	}

	if (enc == SEC_DEC)
	{
		ucl_enc = DEC;
	}else
	{
		ucl_enc = ENC;
	}

	memcpy(tmpKey, key, 16);
	memcpy(tmpKey + 16, key, 8);
	//Jason removed on 2019/12/15, need use HM encrypt api
	dbg("tmpLen = %d\n", tmpLen);
	hexdumpEx("mh_des3_ecb list buffer in", tmpIn, tmpLen);
	hexdumpEx("mh_des3_ecb list key in", tmpKey, 24);
	mh_des3_ecb(tmpKey, tmpIn, tmpLen, out, ucl_enc);
	//Jason removed end
	
	memset(tmpKey, 0, sizeof(tmpKey));
	memset(tmpIn, 0, sizeof(tmpIn));

	return tmpLen;
}



// AesECB(): ECB mode Triple DES encryption(enc != 0), or decryption(enc == 0).
//		The length of in & out need be 8 bytes aligned, so zeros may be padded.
static void AesECB(unsigned char* key, unsigned char* in, int len, unsigned char* out, int enc)
{
	unsigned char tmpIn[1024 + 10];
	//int uiOffset;
	mh_crypt_mode_def ucl_enc;
	int tmpLen;

	if (len > 1024)
	{
		return;
	}

	memset(tmpIn, 0x00, sizeof(tmpIn));

	if (len % AES_128_BLOCK_SIZE != 0)
	{
		//Pad 0x00
		memcpy(tmpIn, in, len);
		memset(tmpIn + len, 0x00, AES_128_BLOCK_SIZE - (len % AES_128_BLOCK_SIZE));
		tmpLen = len + AES_128_BLOCK_SIZE - (len % AES_128_BLOCK_SIZE);
	}else
	{
		memcpy(tmpIn, in, len);
		tmpLen = len;
	}

	if (enc == SEC_DEC)
	{
		ucl_enc = DEC;
	}else
	{
		ucl_enc = ENC;
	}

	//Jason removed on 2019/12/15, need use HM encrypt api
	mh_aes_ecb(key, tmpIn, tmpLen, out, ucl_enc);
	//Jason removed end
	
	memset(tmpIn, 0, sizeof(tmpIn));
}


#define SEC_MEMORY_SIZE	(1*1024)


static int SmemGetKey(KEY_ATTR *p, unsigned char *key);

static int SmemWriteKey(KEY_ATTR *p, unsigned char *key);

static int SmemEraseKey(KEY_ATTR *p);

static int SmemEraseAllKeyAndMac(void);

static int SmemEraseParameter(void);

static int SmemLoadParameter(PINPAD_PARA * pSetting);

static int SmemSetParameter(PINPAD_PARA*  pSetting);

static int CheckKeyValueDuplicate(KEY_ATTR *attr, unsigned char *key);

void writeKeyViaBPK(byte *key, int keyLen, int offset)
{
	uint32_t buf[BPK_KEY_NUM];
	uint32_t index = 0;
	//dbg("%s\n", __func__);
	while (BPK_IsReady() == RESET);

	if (keyLen != AES_128_KEY_BLOCK_SIZE)
	{
		err("keyLen err\n");
		return;
	}
	//set buf 0~X
	for (index = 0; index < (keyLen/4); index++)
	{
		Comm_HexToInt_LittleEndian(key + index*4, &(buf[index]));
		//dbg("buf[%d] = %d\n", index, buf[index]);
		//buf[index] = 0x00;
	}
	BPK_WriteKey(buf, keyLen/4, offset);
	
	//printBPK();
}

void readKeyViaBPK(byte *key, int keyLen, int offset)
{
	uint32_t buf[BPK_KEY_NUM];
	uint32_t index = 0;
	//dbg("%s\n", __func__);
	while (BPK_IsReady() == RESET)
	{
		mdelay(100);
		dbg("BPK_IsReady() == RESET\n");
	}

	//LINE("BPK debug");
	if (keyLen != AES_128_KEY_BLOCK_SIZE)
	{
		err("keyLen err\n");
		return;
	}

	//LINE("BPK debug");
	BPK_ReadKey(buf, keyLen/4, offset);
	
	for (index = 0; index < keyLen/4; index++)
	{
		Comm_IntToHex_LittleEndian(buf[index], key + index*4);
		//buf[index] = 0x00;
	}
	memset(buf, 0, sizeof(buf)/sizeof(uint32_t));
	//LINE("BPK debug");

	//dbg("readKeyViaBPK: List Key Store Key\n");
	//hexdump(key, AES_128_KEY_BLOCK_SIZE);
	//printBPK();
	
}

void vDelBackupKeyFile(void)
{
    char szPath[128];
	sprintf(szPath, "%s/%s", DIR_SECURITY, FILE_NAME_SEC_BACKUP);
    sys_lfs_remove(szPath);
}

static int SmemBackupKeyStoreKey(byte *aesKeyStoreKey)
{
	int ret;
	char szPath[128];
	lfs_file_t fp;

	sprintf(szPath, "%s/%s", DIR_SECURITY, FILE_NAME_SEC_BACKUP);
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}

	sys_lfs_file_write(&fp, aesKeyStoreKey, AES_128_KEY_BLOCK_SIZE);

	sys_lfs_file_close(&fp);
    return 0;
}

static int SmemLoadBackupKeyStoreKey(byte *aesKeyStoreKey)
{
	int ret;
	char szPath[128];
	lfs_file_t fp;

	sprintf(szPath, "%s/%s", DIR_SECURITY, FILE_NAME_SEC_BACKUP);
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_RDONLY);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}

	sys_lfs_file_read(&fp, aesKeyStoreKey, AES_128_KEY_BLOCK_SIZE);

	sys_lfs_file_close(&fp);

	return 0;
}

static int SmemWriteKeyStoreKeyViaBPK(unsigned char *aesKeyStoreKey);

static bool SmemIsKeyStoreKekExistViaBPK()
{
	byte aesKeyStoreKey[AES_128_KEY_BLOCK_SIZE];
	byte tmpZeroBuf[AES_128_KEY_BLOCK_SIZE];
    
	readKeyViaBPK(aesKeyStoreKey, AES_128_KEY_BLOCK_SIZE, 0);

	memset(tmpZeroBuf, 0x00, sizeof(tmpZeroBuf));
	if (memcmp_ex(aesKeyStoreKey, tmpZeroBuf, AES_128_KEY_BLOCK_SIZE) != 0)
	{
        dbg("SmemLoadBackupKeyStoreKey...\n");
		if (SmemLoadBackupKeyStoreKey(aesKeyStoreKey) != 0)
		{
			//aesKeyStoreKey is not zero and no backup key , backup it
			SmemBackupKeyStoreKey(aesKeyStoreKey);
		}
		//aesKeyStoreKey is not zero, it means key exist
		memset(aesKeyStoreKey, 0x00, sizeof(aesKeyStoreKey));
		return TRUE;
	}else
	{
        dbg("SmemLoadBackupKeyStoreKey...\n");
		if (SmemLoadBackupKeyStoreKey(aesKeyStoreKey) == 0)
		{
			//backup key store key is ok, re-write it to BPK
            dbg("SmemWriteKeyStoreKeyViaBPK...\n");
			SmemWriteKeyStoreKeyViaBPK(aesKeyStoreKey);
            dbg("SmemWriteKeyStoreKeyViaBPK ok.\n");
			return TRUE;
		}else
		{
            dbg("SmemLoadBackupKeyStoreKey fail.\n");
			//no backup key store key and aesKeyStoreKey is zero, it means key not exist
			return FALSE;
		}
	}
}

static int SmemReadKeyStoreKeyViaBPK(unsigned char *aesKeyStoreKey)
{
	readKeyViaBPK(aesKeyStoreKey, AES_128_KEY_BLOCK_SIZE, 0);

	return 0;
}

static int SmemWriteKeyStoreKeyViaBPK(unsigned char *aesKeyStoreKey)
{
	writeKeyViaBPK(aesKeyStoreKey, AES_128_KEY_BLOCK_SIZE, 0);

	return 0;
}

static int SmemInitSecFile()
{
	unsigned char tmp[SEC_MEMORY_SIZE];
	unsigned char tmpEnc[SEC_MEMORY_SIZE];
	unsigned char keyStoreKey[AES_128_KEY_BLOCK_SIZE];
	int ret;
	lfs_file_t fp;
	char szPath[128];

	sprintf(szPath, "%s/%s", DIR_SECURITY, FILE_NAME_TRANS_KEY);
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}

	memset(tmp, 0, sizeof(tmp));
	SmemReadKeyStoreKeyViaBPK(keyStoreKey);
	ucl_aes_ecb(tmp, SEC_MEMORY_SIZE, keyStoreKey, tmpEnc, SEC_ENC);
	//hexdumpEx("SmemInitSecFile (), List tmpEnc", tmpEnc, SEC_MEMORY_SIZE);
	
	sys_lfs_file_write(&fp, tmpEnc, SEC_MEMORY_SIZE);

	sys_lfs_file_close(&fp);

	SmemBackupKeyStoreKey(keyStoreKey);

	memset(tmp, 0x00, sizeof(tmp));
	memset(keyStoreKey, 0x00, sizeof(keyStoreKey));
	memset(tmpEnc, 0x00, sizeof(tmpEnc));
	
	return 0;
}

//#define SECURITY_USED_MALLOC

#ifndef SECURITY_USED_MALLOC
static int SmemRead(int offset, unsigned char *buf, int len)
{
	//memcpy(buf, (void *)(pSecmem_base + offset), len);
	lfs_file_t fp;
	int ret;
	char szPath[128];
	unsigned char tmpRead[SEC_MEMORY_SIZE];
	unsigned char tmpResult[SEC_MEMORY_SIZE];
	unsigned char aesKeyStoreKey[AES_128_KEY_BLOCK_SIZE];

	//dbg("SmemRead: Offset = %d\n", offset);

	sprintf(szPath, "%s/%s", DIR_SECURITY, FILE_NAME_TRANS_KEY);
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_RDONLY);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}
	//sys_lfs_file_seek(&fp, offset, LFS_SEEK_SET);
	sys_lfs_file_read(&fp, tmpRead, SEC_MEMORY_SIZE);
	sys_lfs_file_close(&fp);

	SmemReadKeyStoreKeyViaBPK(aesKeyStoreKey);
	//hexdumpEx("SmemRead: list read buffer", tmpRead + offset, len);
	ucl_aes_ecb(tmpRead, SEC_MEMORY_SIZE, aesKeyStoreKey, tmpResult, SEC_DEC);
	//hexdumpEx("SmemRead: list dec buffer", tmpResult + offset, len);
	memcpy(buf, tmpResult + offset, len);

	memset(aesKeyStoreKey, 0x00, sizeof(aesKeyStoreKey));
	memset(tmpResult, 0x00, sizeof(tmpResult));
	memset(tmpRead, 0x00, sizeof(tmpRead));
	
	return len;
}

static int SmemWrite(int offset, unsigned char *buf, int len)
{	
	//byte tmp[100];

	//memcpy((void *)(pSecmem_base + offset), buf, len);

	//memcpy(tmp, pSecmem_base + offset, len);

	//DPRINTF("List write buffer\n");
	//hexdump(tmp, len);
	lfs_file_t fp;
	int ret;
	lfs_ssize_t lfsRet;
	//int writeLen;
	char szPath[128];
	unsigned char tmpRead[SEC_MEMORY_SIZE + 10];
	unsigned char tmpDec[SEC_MEMORY_SIZE + 10];
	unsigned char tmpWrite[SEC_MEMORY_SIZE + 10];
	unsigned char aesKeyStoreKey[AES_128_KEY_BLOCK_SIZE];

	sprintf(szPath, "%s/%s", DIR_SECURITY, FILE_NAME_TRANS_KEY);
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_RDWR);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}
	dbg("Offset = %d, len = %d\n", offset, len);
	//hexdumpEx("SmemWrite: list write buffer", buf, len);
	
	lfsRet = sys_lfs_file_read(&fp, tmpRead, SEC_MEMORY_SIZE);
	dbg("sys_lfs_file_read return = %d \n", lfsRet);
	if (lfsRet != SEC_MEMORY_SIZE)
	{
		sys_lfs_file_close(&fp);
		err("sys_lfs_file_read return %d\n", lfsRet);
		return -2;
	}
	SmemReadKeyStoreKeyViaBPK(aesKeyStoreKey);
	////Line;
	ucl_aes_ecb(tmpRead, SEC_MEMORY_SIZE, aesKeyStoreKey, tmpDec, SEC_DEC);
	////Line;
	//hexdumpEx("SmemWrite: list dec buffer", tmpDec + offset, len);

	memcpy(tmpDec + offset, buf, len);
	//hexdumpEx("SmemWrite: list dec buffer after memcpy", tmpDec + offset, len);
	ucl_aes_ecb(tmpDec, SEC_MEMORY_SIZE, aesKeyStoreKey, tmpWrite, SEC_ENC);
	//hexdumpEx("SmemWrite: list enc buffer after memcpy", tmpWrite + offset, len);
	////Line;
	ret = sys_lfs_file_rewind(&fp);
	if (ret != 0)
	{
		sys_lfs_file_close(&fp);
		err("sys_lfs_file_rewind return %d\n", ret);
		return -3;
	}
	////Line;
	
	lfsRet = sys_lfs_file_write(&fp, tmpWrite, SEC_MEMORY_SIZE);
	dbg("sys_lfs_file_write return = %d \n", lfsRet);
	if (lfsRet != SEC_MEMORY_SIZE)
	{
		sys_lfs_file_close(&fp);
		err("sys_lfs_file_write return %d\n", lfsRet);
		return -4;
	}
	////Line;
	dbg("Before sys_lfs_file_close\n");
	sys_lfs_file_close(&fp);
	////Line;
	mdelay(50);
/*
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_RDWR);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}

	sys_lfs_file_read(&fp, tmpRead, SEC_MEMORY_SIZE);
	sys_lfs_file_close(&fp);
	hexdumpEx("SmemWrite: reopen file and list read buffer", tmpRead + offset, len);
*/
	memset(aesKeyStoreKey, 0x00, sizeof(aesKeyStoreKey));
	memset(tmpWrite, 0x00, sizeof(tmpWrite));
	memset(tmpRead, 0x00, sizeof(tmpRead));

	return len;
}

#else

static int SmemRead(int offset, unsigned char *buf, int len)
{
	lfs_file_t fp;
	int ret;
	char szPath[128];
	byte *tmpBufferMalloc;
	unsigned char * tmpRead;
	unsigned char * tmpResult;
	unsigned char aesKeyStoreKey[AES_128_KEY_BLOCK_SIZE];

	tmpBufferMalloc = malloc(SEC_MEMORY_SIZE * 2);
	if (tmpBufferMalloc == NULL)
	{
		err("tmpBufferMalloc malloc error\n");
		return -2;
	}
	tmpRead = tmpBufferMalloc + 0;
	tmpResult = tmpBufferMalloc + SEC_MEMORY_SIZE;
	
	//dbg("SmemRead: Offset = %d\n", offset);
	
	sprintf(szPath, "%s/%s", DIR_SECURITY, FILE_NAME_TRANS_KEY);
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_RDONLY);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		free(tmpBufferMalloc);
		return -1;
	}
	//sys_lfs_file_seek(&fp, offset, LFS_SEEK_SET);
	sys_lfs_file_read(&fp, tmpRead, SEC_MEMORY_SIZE);
	sys_lfs_file_close(&fp);

	SmemReadKeyStoreKeyViaBPK(aesKeyStoreKey);
	//hexdumpEx("SmemRead: list read buffer", tmpRead + offset, len);
	ucl_aes_ecb(tmpRead, SEC_MEMORY_SIZE, aesKeyStoreKey, tmpResult, SEC_DEC);
	//hexdumpEx("SmemRead: list dec buffer", tmpResult + offset, len);
	memcpy(buf, tmpResult + offset, len);

	memset(aesKeyStoreKey, 0x00, sizeof(aesKeyStoreKey));
	memset(tmpResult, 0x00, sizeof(tmpResult));
	memset(tmpRead, 0x00, sizeof(tmpRead));

	free(tmpBufferMalloc);
	sys_lfs_file_close(&fp);
	return len;
}

static int SmemWrite(int offset, unsigned char *buf, int len)
{	
	lfs_file_t fp;
	int ret;
	//int writeLen;
	char szPath[128];
	byte *tmpBufferMalloc;
	unsigned char * tmpRead;
	unsigned char * tmpDec;
	unsigned char * tmpWrite;
	unsigned char aesKeyStoreKey[AES_128_KEY_BLOCK_SIZE];

	sprintf(szPath, "%s/%s", DIR_SECURITY, FILE_NAME_TRANS_KEY);
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_RDWR);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}

	tmpBufferMalloc = malloc(SEC_MEMORY_SIZE * 3);
	if (tmpBufferMalloc == NULL)
	{
		err("tmpBufferMalloc malloc error\n");
		return -2;
	}
	tmpRead = tmpBufferMalloc + 0;
	tmpDec = tmpBufferMalloc + SEC_MEMORY_SIZE;
	tmpWrite = tmpBufferMalloc + (2 * SEC_MEMORY_SIZE);
	
	dbg("SmemWrite: Offset = %d, len = %d\n", offset, len);
	//hexdumpEx("SmemWrite: list write buffer", buf, len);
	
	sys_lfs_file_read(&fp, tmpRead, SEC_MEMORY_SIZE);
	SmemReadKeyStoreKeyViaBPK(aesKeyStoreKey);
	ucl_aes_ecb(tmpRead, SEC_MEMORY_SIZE, aesKeyStoreKey, tmpDec, SEC_DEC);

	//hexdumpEx("SmemWrite: list dec buffer", tmpDec + offset, len);

	memcpy(tmpDec + offset, buf, len);
	//hexdumpEx("SmemWrite: list dec buffer after memcpy", tmpDec + offset, len);
	ucl_aes_ecb(tmpDec, SEC_MEMORY_SIZE, aesKeyStoreKey, tmpWrite, SEC_ENC);
	//hexdumpEx("SmemWrite: list enc buffer after memcpy", tmpWrite + offset, len);

	sys_lfs_file_rewind(&fp);
	
	sys_lfs_file_write(&fp, tmpWrite, SEC_MEMORY_SIZE);
	
	sys_lfs_file_close(&fp);

/*
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_RDWR);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}

	sys_lfs_file_read(&fp, tmpRead, SEC_MEMORY_SIZE);
	sys_lfs_file_close(&fp);
	hexdumpEx("SmemWrite: reopen file and list read buffer", tmpRead + offset, len);
*/
	memset(aesKeyStoreKey, 0x00, sizeof(aesKeyStoreKey));
	memset(tmpWrite, 0x00, sizeof(tmpWrite));
	memset(tmpRead, 0x00, sizeof(tmpRead));

	free(tmpBufferMalloc);
	return len;
}

#endif

// Get the storage position (offset in the file) of the specified key.
static int GetPosition(KEY_ATTR *p)
{
	int pos, idx;

	//DPRINTF("Enter GetPosition!!!!\n");
	
	if (p == NULL)
	{
		// invalid paramater
		return -1;		
	}

	if (p->level == KEY_LEVEL_KTK)   
	{
		pos = KTK_BASE;
		idx = 0;
	}
	else if (p->level == KEY_LEVEL_KEK)   
	{
		pos = KEK_BASE;
		idx = p->idx;
	}
	else if (p->level == KEY_LEVEL_SK)    
	{
		pos = SK_BASE;
		idx = p->idx;
	}
	else
	{
		DPRINTF("Invalid key level: level(%0x2x)\n", p->level);
		// invalid level
		return -2;		
	}

	pos += idx * KEY_STORAGE_LEN;
	if (pos < TRANSACTION_KEY_MEM_SIZE - KEY_STORAGE_LEN)
	{
		//DPRINTF("Key postion ok  2222\n");
		//dwatch(pos);
		return pos;
	}

	DPRINTF("Key postion out of range: pos(%04x) level(%02x) idx(%02x)\n", pos, p->level, p->idx);
	// out of range of security memory
	return -3;		
}

/* SmemGetKey(): Get key info from security memory
	Return value
		 0: operation succeed.
		-1: invalid paramater.
		-2: fail to find key position.
		-3: Read security memory failed.
		-4: Key does not exist.
*/
static int SmemGetKey(KEY_ATTR *p, unsigned char *key)
{
	int pos;
	int len;
	int ret;
	unsigned char tmpKeyStorage[KEY_STORAGE_LEN];

	if (p == NULL || key == NUL)
	{
		return -1;		// invalid paramater.
	}

	pos = GetPosition(p);
	if (pos < 0)
	{
		return -2;		// fail to find key position.
	}

	len = KEY_STORAGE_LEN;
	ret = SmemRead(pos, tmpKeyStorage, len);
	if (ret < len)
	{
		return -3;		// Read security memory failed.
	}

	//DPRINTF("List read buffer\n");
	//hexdump(tmpKeyStorage, 26);
	p->len = tmpKeyStorage[24];
	p->type = tmpKeyStorage[25];
	if (p->len == KEY_LEN_ZERO)
	{
		DPRINTF("Key not exist at position: level(%02x), idx(%02x)\n", p->level, p->idx);
		memset(tmpKeyStorage, 0, sizeof(tmpKeyStorage));
		return -4;	// Key not exist.
	}

	memcpy(p->sn, tmpKeyStorage + 16, 8);
	memcpy(key, tmpKeyStorage, 16);

	memset(tmpKeyStorage, 0, sizeof(tmpKeyStorage));
	return 0;
}



/* SmemWriteKey(): Write a new key into security memory
	Return value
		 0: operation succeed.
		-1: invalid paramater.
		-2: fail to find key position.
		-3: Write security memory failed.
		-4:	Failed to get decryption key.
*/
static int SmemWriteKey(KEY_ATTR *p, unsigned char *key)
{
	int pos = 0;
	int len = 0;
 	int ret;
	unsigned char tmpKeyStr[KEY_STORAGE_LEN];
	unsigned char key_read[KEY_STORAGE_LEN];
	unsigned char key_write[KEY_STORAGE_LEN];
	KEY_ATTR deckeyattr;

	if (p == NULL || key == NULL)
	{
		return -1;		// invalid paramater.
	}

	//DPRINTF("Enter SmemWriteKey!!!\n");

	pos = GetPosition(p);
	//dwatch(pos);
	//DPRINTF("3333!!!\n");	
	if (pos < 0)
	{
		return -2;		// fail to find key position.
	}

	//DPRINTF("4444!!!\n");
	// If the key in the parameter is encrypted, it need to be decrypted.
	if (p->dec_level != KEY_LEVEL_NONE)
	{
		deckeyattr.level = p->dec_level;
		deckeyattr.idx = p->dec_idx;
	
		ret = SmemGetKey(&deckeyattr, key_read);
		if (ret != 0)
		{
			return -4;	// Failed to get decryption key.
		}
		
		// Decrypt the key.
		if (deckeyattr.len == KEY_LEN_SINGLE)
		{
			return -5; //No single key
		}

		if (deckeyattr.len == KEY_LEN_DOUBLE)
		{	
			Des3ECB(key_read, key, deckeyattr.len, key_write, 0);
			//hexdump(key_read, deckeyattr.len);
			//hexdump(key, deckeyattr.len);
			//DPRINTF("list write key\n");
			//hexdump(key_write, deckeyattr.len);
		}
	}

	len = KEY_STORAGE_LEN;
	tmpKeyStr[24] = p->len;
	tmpKeyStr[25] = p->type;
	memcpy(tmpKeyStr + 16, p->sn, 8);
	
	if (p->dec_level != KEY_LEVEL_NONE)
	{
		memcpy(tmpKeyStr, key_write, p->len);
	}
	else
	{
		memcpy(tmpKeyStr, key, p->len);	
	}

	ret = CheckKeyValueDuplicate(p, tmpKeyStr);
	if(ret == 1)
	{
		DPRINTF("CheckKeyValueDuplicate return 1\n");
		memset (tmpKeyStr, 0, sizeof(tmpKeyStr));
		memset (key_read, 0, sizeof(key_read));
		memset (key_write, 0, sizeof(key_write));
		return -4;
	}

	//DPRINTF("SmemWrite buffer list\n");
	//hexdump(tmpKeyStr, 26);
	ret = SmemWrite(pos, tmpKeyStr, len);
	if (ret < len)
	{
		memset (tmpKeyStr, 0, sizeof(tmpKeyStr));
		memset (key_read, 0, sizeof(key_read));
		memset (key_write, 0, sizeof(key_write));
		return -3;		// Write security memory failed.
	}

	memset (tmpKeyStr, 0, sizeof(tmpKeyStr));
	memset (key_read, 0, sizeof(key_read));
	memset (key_write, 0, sizeof(key_write));

	return 0;
}


void leftshift_onebit(unsigned char *input,unsigned char *output)
{
    int i;
    unsigned char overflow = 0;
	
    for (i=7; i>=0; i-- ) 
	{
        output[i] = input[i] << 1;
        output[i] |= overflow;
        overflow = (input[i] & 0x80)?1:0;
    }
    return;
}

/* For CMAC Calculation */
unsigned char const_Rb[8] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1b,
};
unsigned char const_Zero[8] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

unsigned char const_KBEK_CMAC1[8] = {
    0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
};

unsigned char const_KBEK_CMAC2[8] = {
    0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
};

unsigned char const_KBMK_CMAC1[8] = {
    0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x80,
};

unsigned char const_KBMK_CMAC2[8] = {
    0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x80,
};


void xor_64(unsigned char *a, unsigned char *b, unsigned char *out)
{
    int i;
    for (i=0;i<8; i++)
    {
        out[i] = a[i] ^ b[i];
    }
}

static int SmemGenerateKBEKandKBMK(unsigned char *KBPKIn, int iKeyLen, unsigned char *KBEKOut, unsigned char *KBMKOut) 
{
	unsigned char zero[100];
	unsigned char tmp[100];
	unsigned char tmp2[100];
	unsigned char K1[100];

	memset(zero, 0, sizeof(zero));

	if (iKeyLen == KEY_LEN_SINGLE)
	{
		Des(KBPKIn, zero, tmp, 1);
	}else
	{
		Des3(KBPKIn, zero, tmp, 1);
	}

	if ((tmp[0]&0x80) == 0)
	{
		leftshift_onebit(tmp, K1);
	}else
	{
		leftshift_onebit(tmp, tmp2);
		xor_64(tmp2, const_Rb, K1);				
	}

	if (iKeyLen == KEY_LEN_SINGLE)
	{
		memset(tmp, 0, sizeof(tmp));
		xor_64(K1, const_KBEK_CMAC1, tmp);
		Des(KBPKIn, tmp, KBEKOut, 1);
		
		memset(tmp, 0, sizeof(tmp));
		xor_64(K1, const_KBMK_CMAC1, tmp);
		Des(KBPKIn, tmp, KBMKOut, 1);
	}else
	{
		memset(tmp, 0, sizeof(tmp));
		xor_64(K1, const_KBEK_CMAC1, tmp);
		Des3(KBPKIn, tmp, KBEKOut, 1);

		memset(tmp, 0, sizeof(tmp));
		xor_64(K1, const_KBEK_CMAC2, tmp);
		Des3(KBPKIn, tmp, KBEKOut + 8, 1);
	
		memset(tmp, 0, sizeof(tmp));
		xor_64(K1, const_KBMK_CMAC1, tmp);
		Des3(KBPKIn, tmp, KBMKOut, 1);

		memset(tmp, 0, sizeof(tmp));
		xor_64(K1, const_KBMK_CMAC2, tmp);
		Des3(KBPKIn, tmp, KBMKOut + 8, 1);
	}

	return 0;
}

//Jason added 2012.03.30
/* SmemWriteKeyByKBPK(): Write a new key into security memory by KBPK
	Return value
		 0: operation succeed.
		-1: invalid paramater.
		-2: fail to find key position.
		-3: Write security memory failed.
		-4:	Failed to get decryption key.
*/
static int SmemWriteKeyByKBPK(KEY_ATTR *p, unsigned char *key)
{
	int pos = 0;
	int len = 0;
	int ret;
	unsigned char tmpKeyStr[KEY_STORAGE_LEN];
	unsigned char key_read[KEY_STORAGE_LEN];
	unsigned char key_write[KEY_STORAGE_LEN];
	unsigned char kbek_out[KEY_STORAGE_LEN];
	unsigned char kbmk_out[KEY_STORAGE_LEN];
	KEY_ATTR deckeyattr;

	if( p == NULL || key == NULL)
	{
		return -1;		// invalid paramater.
	}

	pos = GetPosition(p);
	if (pos < 0)
	{
		return -2;		// fail to find key position.
	}


	// If the key in the parameter is encrypted, it need to be decrypted.
	if (p->dec_level != KEY_LEVEL_NONE)
	{
		deckeyattr.level = p->dec_level;
		deckeyattr.idx = p->dec_idx;
	
		ret = SmemGetKey(&deckeyattr, key_read);
		if (ret != 0)
		{
			memset (key_read, 0, sizeof(key_read));
			return -4;	// Failed to get decryption key.
		}

		ret = SmemGenerateKBEKandKBMK(key_read, deckeyattr.len, kbek_out, kbmk_out);
		if (ret != 0)
		{
			memset (key_read, 0, sizeof(key_read));
			memset (kbek_out, 0, sizeof(kbek_out));
			memset (kbmk_out, 0, sizeof(kbmk_out));
			return -4; // Failed to get decryption key.
		}

		/*
		DPRINTF("list kepk\n");
		hexdump(key_read, 16);
		DPRINTF("list kbek_out\n");
		hexdump(kbek_out, 16);
		DPRINTF("list kbmk_out\n");
		hexdump(kbmk_out, 16);
		DPRINTF("list encrypted key\n");
		hexdump(key, 16);
		*/
		// Decrypt the key.
		if (deckeyattr.len == KEY_LEN_SINGLE)
		{
			Des(kbek_out, key, key_write, 0);
		}
		else if (deckeyattr.len == KEY_LEN_DOUBLE)
		{				
			Des3ECB(kbek_out, key, KEY_LEN_DOUBLE, key_write, 0);
			/*
			DPRINTF("list write key\n");
			hexdump(key_write, KEY_LEN_DOUBLE);
			*/
		}else
		{
			memset (key_read, 0, sizeof(key_read));
			memset (kbek_out, 0, sizeof(kbek_out));
			memset (kbmk_out, 0, sizeof(kbmk_out));
			return -4; // Failed to get decryption key.
		}
		memset (kbek_out, 0, sizeof(kbek_out));
		memset (kbmk_out, 0, sizeof(kbmk_out));
	}

	len = KEY_STORAGE_LEN;
	tmpKeyStr[24] = p->len;
	tmpKeyStr[25] = p->type;
	memcpy(tmpKeyStr + 16, p->sn, 8);
	
	if (p->dec_level != KEY_LEVEL_NONE)
	{
		memcpy(tmpKeyStr, key_write, p->len);
	}
	else
	{
		memcpy(tmpKeyStr, key, p->len);	
	}

	ret = CheckKeyValueDuplicate(p, tmpKeyStr);
	if(ret == 1)
	{
		DPRINTF("CheckKeyValueDuplicate return 1\n");
		memset (tmpKeyStr, 0, sizeof(tmpKeyStr));
		memset (key_read, 0, sizeof(key_read));
		memset (key_write, 0, sizeof(key_write));
		return -4;
	}

	ret = SmemWrite(pos, tmpKeyStr, len);
	if (ret < len)
	{
		memset (tmpKeyStr, 0, sizeof(tmpKeyStr));
		memset (key_read, 0, sizeof(key_read));
		memset (key_write, 0, sizeof(key_write));
		return -3;		// Write security memory failed.
	}

	memset (tmpKeyStr, 0, sizeof(tmpKeyStr));
	memset (key_read, 0, sizeof(key_read));
	memset (key_write, 0, sizeof(key_write));

	return 0;
}

/* SmemEraseKey(): Erase a key from security memory
	Return value
		 0: operation succeed.
		-1: invalid paramater.
		-2: fail to find key position.
		-3: Write security memory failed.
*/
static int SmemEraseKey(KEY_ATTR *p)
{
	int pos;
	int ret;
	unsigned char str[KEY_STORAGE_LEN];

	if (p == NULL)
	{
		return -1;
	}

	pos = GetPosition(p);
	if (pos < 0)
	{
		return -2;
	}

	memset(str, 0, sizeof(str));
	ret = SmemWrite(pos, str, KEY_STORAGE_LEN);
	if (ret < KEY_STORAGE_LEN)
	{
		return -3;		// Write security memory failed.
	}

	return 0;
}



/* Erase all keys and mac  from the security memory.
	Return value
		 0: operation succeed.
		-1: Failed.
*/
static int SmemEraseAllKeyAndMac()
{
	byte str[KEY_MEM_SIZE + 1];
	int ret;

	memset(str, 0, KEY_MEM_SIZE);	
	ret = SmemWrite(KEY_BASE, str, KEY_MEM_SIZE);
	if (ret != KEY_MEM_SIZE)
	{
		return -1;
	}

	return 0;
}


/* Erase  parameter area from the security memory.
	Return value
		 0: operation succeed.
		-1: Failed.
*/
static int SmemEraseParameter()
{
	unsigned char str[PARAMETER_MEM_SIZE];
	int ret;

	memset(str, 0, sizeof(str));
	
	ret = SmemWrite(PARAMETER_BASE, str, PARAMETER_MEM_SIZE);
	if (ret != PARAMETER_MEM_SIZE)
	{
		return -1;
	}

	return 0;
}

static int SmemLoadParameter(PINPAD_PARA * pSetting)
{
	if (pSetting == NULL)
	{
		return -4;
	}

	//memcpy(pSetting, pSecmem_base + PARAMETER_BASE, sizeof(PINPAD_PARA));
	SmemRead(PARAMETER_BASE, (byte *)pSetting, sizeof(PINPAD_PARA));

	/*
	if (pSetting->KeyMode == 0)
	{		
		//First time load the settings, set default value of key mode
		pSetting->KeyMode = KEY_MODE_UN_INIT;
	}
	*/

	return 0;	
}

static int SmemSetParameter(PINPAD_PARA * pSetting)
{
/*
	byte tmp[100];

	memcpy(tmp, (byte *)pSetting, sizeof(PINPAD_PARA));
	
	SmemWrite(PARAMETER_BASE, tmp, sizeof(PINPAD_PARA));
*/
	SmemWrite(PARAMETER_BASE, (byte *)pSetting, sizeof(PINPAD_PARA));

	return 0;
}

static int SmemCmpKeyPart(byte *InKey1, byte *InKey2, int InCmpLen)
{
	int iLoop = 0;
	int iRet;
	byte tmpKey1[24];
	byte tmpKey2[24];

	for (iLoop = 0; iLoop < InCmpLen; iLoop++)
	{
		tmpKey1[iLoop] = (InKey1[iLoop])&0xFE;
		tmpKey2[iLoop] = (InKey2[iLoop])&0xFE;
	}

	iRet = memcmp_ex(tmpKey1, tmpKey2, InCmpLen);

	return iRet;
}

static int CheckKeyValueDuplicate(KEY_ATTR *attr, byte *key)
{
	byte tmpBuf[KEY_STORAGE_LEN];
	byte tmpCheckKey[KEY_STORAGE_LEN];
	byte tmpStoreKey[KEY_STORAGE_LEN];	
	byte tmpStoreKeyLen;
	int pos = 0;
	int idx = 0;
	int ret;
	int position = 0;

	if (key == NULL || attr->len != KEY_LEN_DOUBLE)
	{
		return 0;
	}
	
	position = GetPosition(attr);

	for (idx = 0; idx < KEY_TOTALNUM; idx++)
	{
		pos = idx*KEY_STORAGE_LEN + KTK_BASE;
		if (pos != position)
		{
			ret = SmemRead(pos, tmpBuf, KEY_STORAGE_LEN);
			if (ret == KEY_STORAGE_LEN)
			{
				tmpStoreKeyLen = tmpBuf[24];
				//tmpStoreKeyType = tmpBuf[25];
	
				if (attr->len == KEY_LEN_DOUBLE)
				{
					if (tmpStoreKeyLen == KEY_LEN_ZERO)
					{
						continue;
					}
					else if (tmpStoreKeyLen == KEY_LEN_DOUBLE)
					{
						memcpy(tmpStoreKey, tmpBuf, KEY_LEN_DOUBLE);
						
						memcpy(tmpCheckKey, key, KEY_LEN_DOUBLE);
					
						ret = SmemCmpKeyPart(tmpStoreKey, tmpCheckKey, KEY_LEN_DOUBLE);
						if (ret == 0)
						{
							memset(tmpBuf, 0, sizeof(tmpBuf));
							memset(tmpCheckKey, 0, sizeof(tmpCheckKey));
							memset(tmpStoreKey, 0, sizeof(tmpStoreKey));
							return 1;
						}
					}else 
					{
						continue;
					}
				}
			}
			else
			{
				DPRINTF("SmemRead failed idx = %d\n", idx);
			}
		}
	}

	memset(tmpBuf, 0, sizeof(tmpBuf));
	memset(tmpCheckKey, 0, sizeof(tmpCheckKey));
	memset(tmpStoreKey, 0, sizeof(tmpStoreKey));

	return 0;
}


/* SecWriteKey(): Write a new key into security memory
	Return value
		 0: operation succeed.
		-1: invalid paramater.
		-2: fail to find key position.
		-3: Write security memory failed.
		-4:	Failed to get decryption key.
*/
int SecWriteKey(KEY_ATTR *attr, byte *key)
{
	return SmemWriteKey(attr, key);
}

/* SecReadKey(): Read a key from security memory to buffer
	Return value
		 0: operation succeed.
		-1: invalid paramater.
		-2: fail to find key position.
		-3: Write security memory failed.
		-4:	Failed to get decryption key.
*/
int SecReadKey(KEY_ATTR *attr, byte *key)
{
	return SmemGetKey(attr, key);
}



/* SmemWriteKeyByKBPK(): Write a new key into security memory by KBPK
	Return value
		 0: operation succeed.
		-1: invalid paramater.
		-2: fail to find key position.
		-3: Write security memory failed.
		-4:	Failed to get decryption key.
*/
int SecWriteKeyByKBPK(KEY_ATTR *attr, byte * key)
{	
	return SmemWriteKeyByKBPK(attr, key);
}


/* SecEraseKey(): Erase a key from security memory
	Return value
		 0: operation succeed.
		-2: fail to find key position.
		-3: Write security memory failed.
*/
int SecEraseKey(unsigned char level, unsigned char idx)
{
	KEY_ATTR attr;

	attr.level = level;
	attr.idx = idx;
	
	return SmemEraseKey(&attr);
}

/* SecEraseAllKey(): Erase all keys and Mac from security memory
	Return value
		 0: succeed.
		-1: failed
*/
int SecEraseAllKey()
{	
	return SmemEraseAllKeyAndMac();
}

/* SecEraseParameter(): Erase paramter area from security memory
	Return value
		 0: succeed.
		-1: failed
*/
int SecEraseParameter()
{
	return SmemEraseParameter();
}

/* SecGetKeyAttr(): Get attribute of a key from security memory
	Return value
		 0: operation succeed.
		-1: invalid paramater.
		-2: fail to find key position.
		-3: Read security memory failed.
		-4: Key does not exist.
*/
int SecGetKeyAttr(int keyLevelIn, int keyIndexIn, KEY_ATTR *pGetKeyAttrOut)
{
	KEY_ATTR tmpKeyAttr;
	byte tmpKeyBuf[24];
	int ret;

	tmpKeyAttr.level = keyLevelIn;
	tmpKeyAttr.idx = keyIndexIn;

	ret = SmemGetKey(&tmpKeyAttr, tmpKeyBuf);
	memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
	if (ret == 0)
	{
		memcpy(pGetKeyAttrOut, &tmpKeyAttr, sizeof(KEY_ATTR));
	}
	
	return ret;
}

/* SecCheckKeyExist(): Check the key is exist or not 
	Return value
		 0: Key exist.
		-1: invalid paramater.
		-2: fail to find key position.
		-3: Read security memory failed.
		-4: Key does not exist.
*/
int SecCheckKeyExist(int keyLevel, int keyIndex)
{
	KEY_ATTR tmpKeyAttr;
	byte tmpKeyBuf[24];
	int ret;

	tmpKeyAttr.level = keyLevel;
	tmpKeyAttr.idx = keyIndex;

	ret = SmemGetKey(&tmpKeyAttr, tmpKeyBuf);
	memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));

	return ret;
}


/* SecDesEnc(): Encrypt input with specified key.
	Return value
		 0: Operation succeed.
		-1: Fail to get key.
		-2: Wrong key length.
*/
int SecDesEnc(unsigned char level, unsigned char idx, unsigned char *input, unsigned char *output)
{
	KEY_ATTR attr;
	byte tmpKeyBuf[24];
	int ret;

	attr.level = level;
	attr.idx = idx;

	ret = SmemGetKey(&attr, tmpKeyBuf);
	if (ret != 0)
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -1;		// Fail to get key
	}

	if (attr.len != KEY_LEN_DOUBLE)
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -2;		// wrong key length.
	}

	DPRINTF("List SecDesEnc key\n");
	hexdump(tmpKeyBuf, KEY_LEN_DOUBLE);
	DPRINTF("List input\n");
	hexdump(input, 8);
	Des3(tmpKeyBuf, input, output, 1);
	DPRINTF("output output\n");
	hexdump(output, 8);

	memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
	
	return 0;
}


/* SecDesDec(): Decrypt input with specified key.
	Return value
		 0: Operation succeed.
		-1: Fail to get key.
		-2: Wrong key length.
*/
int SecDesDec(unsigned char level, unsigned char idx, unsigned char *input, unsigned char *output)
{
	KEY_ATTR attr;
	byte tmpKeyBuf[24];
	int ret;

	attr.level = level;
	attr.idx = idx;

	ret = SmemGetKey(&attr, tmpKeyBuf);
	if (ret != 0)
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -1;		// Fail to get key
	}

	if ((attr.len != KEY_LEN_SINGLE) && (attr.len != KEY_LEN_DOUBLE))
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -2;		// wrong key length.
	}

	if (attr.len == KEY_LEN_SINGLE)
	{
		Des(tmpKeyBuf, input, output, 0);
	}
	else
	{
		Des3(tmpKeyBuf, input, output, 0);
	}

	memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));

	return 0;
}


/* SecMac(): Caculate MAC of input data with specified key.
	Return value
		 0: Operation succeed.
		-1: Fail to get key.
		-2: Wrong key length.
*/
int SecMac(unsigned char level, unsigned char idx, unsigned char *input, int inputLen, 
	unsigned char *outputMac)
{
	KEY_ATTR attr;
	byte tmpKeyBuf[24];
	int ret;

	attr.level = level;
	attr.idx = idx;

	ret = SmemGetKey(&attr, tmpKeyBuf);
	if (ret != 0)
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -1;		// Fail to get key
	}

	if ((attr.len != KEY_LEN_SINGLE) && (attr.len != KEY_LEN_DOUBLE))
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -2;		// wrong key length.
	}

	if (attr.len == KEY_LEN_SINGLE)
	{
		DesMac(tmpKeyBuf, input, inputLen, outputMac);
	}
	else
	{
		Des3Mac(tmpKeyBuf, input, inputLen, outputMac);
	}

	memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
	
	return 0;
}

/* SecMac_UnionPay(): Caculate MAC of input data with specified key.
	Return value
		 0: Operation succeed.
		-1: Fail to get key.
		-2: Wrong key length.
*/
int SecMac_UnionPay(unsigned char level, unsigned char idx, unsigned char *input, int inputLen, 
	unsigned char *outputMac)
{
	KEY_ATTR attr;
	byte tmpKeyBuf[24];
	int ret;

	attr.level = level;
	attr.idx = idx;

	ret = SmemGetKey(&attr, tmpKeyBuf);
	if (ret != 0)
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -1;		// Fail to get key
	}

	if (attr.len != KEY_LEN_DOUBLE)
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -2;		// wrong key length.
	}

	Des3Mac_UnionPay(tmpKeyBuf, input, inputLen, outputMac);
	
	memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
	
	return 0;
}


/***********************************************************
	函数名称:	SecTrackEncryptAsciiUnionPay()
	函数功能:	银联加密磁道数据方法,附录E @银联卡受理终端应
				用规范-第1部分：销售点终端（POS）应用规范
				(QCUP 009.1-2010).pdf" 从结束标志“？”向左第2
				个字节开始，再向左取8个字节作为参与加密的数据
				输出的ASCII数据把=转为d
	入口参数:	pucKey - 密钥
				pucInputBuf - 待加密数据
				uiInputLen - 数据长度
	出口参数:	pucOutputBuf - 加密数据
	返 回 值:	数据长度
	备    注:	如果返回值小于0，表示加密失败
***********************************************************/
static int SecTrackEncryptAsciiUnionPay(byte *pucKey, byte * pucOutputBuf, byte * pucInputBuf, 
	unsigned int uiInputLen)
{
	int i = 0;
	byte ucCuttedBCD[8];
	byte ucEncryptedBCD[8];
	unsigned int uiOutputAsciiLen;
	unsigned char tmpBuf[1024] = {0};

	
	uiOutputAsciiLen = uiInputLen;
	memcpy(tmpBuf, pucInputBuf, uiInputLen);

	// 不为偶数时候补0
	if ((uiInputLen%2) != 0)
	{
		uiInputLen += 1;
		tmpBuf[uiInputLen - 1] = 0x30;
		//pucInputBuf[uiInputLen-1] = 0x30;
	}

//	PrintDEBUG("InputBuf:", pucInputBuf, uiInputLen);
//	CONV_Asc2Hex(ucCuttedBCD, 8, pucInputBuf+uiInputLen-18, 16);	// 向左第2个字节开始，因此是倒数第9字节开始，即ucInputBuf+uiInputLen-18
	CONV_Asc2Hex(ucCuttedBCD, 8, tmpBuf+uiInputLen-18, 16);	// 向左第2个字节开始，因此是倒数第9字节开始，即ucInputBuf+uiInputLen-18

//	PrintDEBUG("TDB:", ucCuttedBCD, 8);

	Des3ECB(pucKey, ucCuttedBCD, 8, ucEncryptedBCD, SEC_ENC);
	DPRINTF("ucEncryptedBCD\n");
	hexdump(ucEncryptedBCD, 8);
	
	//memcpy(pucOutputBuf, pucInputBuf, uiInputLen);
	memcpy(pucOutputBuf, tmpBuf, uiInputLen);
	CONV_Hex2Asc(pucOutputBuf+uiInputLen-18, 16, ucEncryptedBCD, 8);

	for (i=0; i<uiInputLen-18; i++)
	{
		if (pucInputBuf[i] == '=')
		{
			pucOutputBuf[i] = 'd';
		}
	}

//	PrintDEBUG("OutputBuf:", pucOutputBuf, uiOutputAsciiLen);

	return uiOutputAsciiLen;
}


int SecDesEncTrack_UnionPay(unsigned char level, unsigned char idx, 
	unsigned char *input, int inputlen, unsigned char *output, int *outputlen)
{
	KEY_ATTR attr;
	byte tmpKeyBuf[24];
	int ret;

	attr.level = level;
	attr.idx = idx;

	ret = SmemGetKey(&attr, tmpKeyBuf);
	if (ret != 0)
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -1;		// Fail to get key
	}

	if (attr.len != KEY_LEN_DOUBLE)
	{
		memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
		return -2;		// wrong key length.
	}

	DPRINTF("List SecDesEncTrack_UnionPay key\n");
	hexdump(tmpKeyBuf, KEY_LEN_DOUBLE);
	ret = SecTrackEncryptAsciiUnionPay(tmpKeyBuf, output, input, inputlen);

	memset(tmpKeyBuf, 0, sizeof(tmpKeyBuf));
	if (ret > 0)
	{
		*outputlen = ret;
		return 0;
	}else
	{
		return -3;
	}
}


int SecDesMac(unsigned char * key, unsigned char * in, int len, unsigned char * mac)
{
	DesMac(key, in, len, mac);

	return 0;
}

//Jason added start, 2012.03.09
int SecMacByKBPK(KEY_ATTR tAttr, byte *input, int inputlen, byte *macoutput)
{	
	KEY_ATTR tTmpKeyAttr; 
	byte byTmpKey[20];
	byte byTmpKBEK[20];
	byte byTmpKBMK[20];
	int ret;

	memcpy(&tTmpKeyAttr, &tAttr, sizeof(KEY_ATTR));
	if (tAttr.dec_level!= KEY_LEVEL_NONE)
	{
		tTmpKeyAttr.level = tAttr.dec_level;
		tTmpKeyAttr.idx = tAttr.dec_idx;
		ret = SmemGetKey(&tTmpKeyAttr, byTmpKey);
	
		if (ret != 0)
		{	
			DPRINTF("SmemGetKey error\n");
			return -1;
		}

		SmemGenerateKBEKandKBMK(byTmpKey, tTmpKeyAttr.len, byTmpKBEK, byTmpKBMK);
/*
		DPRINTF("KBEK:");
		hexdump(byTmpKey, 16);
		DPRINTF("KBMK:");
		hexdump(byTmpKBMK, 16);
		DPRINTF("KBEK:");
		hexdump(byTmpKBEK, 16);
*/		
		if (tTmpKeyAttr.len == KEY_LEN_SINGLE)
		{
			DPRINTF("Before DesMAC\n");
			DPRINTF("inputlen = [%d], input = [%s]\n", inputlen, input);
			DesMac(byTmpKBMK, input, inputlen, macoutput);				
		}else
		{
			DPRINTF("Double length\n");
			
			DPRINTF("Before Des3MAC\n");
			DPRINTF("inputlen = [%d], input = [%s]\n", inputlen, input);
			Des3Mac(byTmpKBMK, input, inputlen, macoutput);	
		}
	}
	else
	{
		return -1;
	}

	memset(byTmpKey, 0, sizeof(byTmpKey));
	memset(byTmpKBEK, 0, sizeof(byTmpKBEK));
	memset(byTmpKBMK, 0, sizeof(byTmpKBMK));
	
	return 0;
}

//End

bool SecIsTrack2KeyExist()
{
	int iLoop;
	int iRet;
	KEY_ATTR attr;
	unsigned char key[16];

	for (iLoop = 0; iLoop < SK_NUM; iLoop ++)
	{
		attr.level = KEY_LEVEL_SK;
		attr.idx = iLoop;
		iRet = SmemGetKey(&attr, key);
		if (iRet != 0)
		{
			continue;
			// Fail to get key
		}else
		{
			if (attr.type == KEY_TYPE_TRACK2)
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

int SecEncryptTrack2Data(unsigned char *Track2In, int iTrack2Len, 
	unsigned char * encTrack2Out, int *piEncTrack2Len)
{
	int iLoop;
	int iOutLen;
	int iRet;
	KEY_ATTR attr;
	unsigned char key[16];
	//unsigned char tmp[1024] = {0};

	for (iLoop = 0; iLoop < SK_NUM; iLoop ++)
	{
		attr.level = KEY_LEVEL_SK;
		attr.idx = iLoop;
		iRet = SmemGetKey(&attr, key);
		if(iRet != 0)
		{
			continue;
			// Fail to get key
		}else
		{
			if (attr.type == KEY_TYPE_TRACK2)
			{
				break;
			}
		}
	}

	if (iLoop < SK_NUM)
	{
		//Track 2 data key exist
		//printf("szTrack2In[%d] = [%s]\n", strlen(szTrack2In), szTrack2In);
		DPRINTF("Track2In is :\n");
		hexdump(Track2In , iTrack2Len);
		
		DPRINTF("track Key is :\n");
		hexdump(key , 16);

		iOutLen = Des3ECB(key, Track2In, iTrack2Len, encTrack2Out, 1);
		
		DPRINTF("encTrack2Out is :\n");
		hexdump(encTrack2Out , iOutLen);
		*piEncTrack2Len = iOutLen;

		return 0;
		
	}else
	{
		return -1;
	}

}



//Return value:0 - equal
//			  1 - Not equal
//			  -1 - error
int SecCmpMACByKBPK(KEY_ATTR tAttr, byte input[], int inputlen, byte InMAC[])
{
	int iRet;
	byte OutMAC[8];

	iRet = SecMacByKBPK(tAttr, input, inputlen, OutMAC);
	//DPRINTF("OutMAC:\n");
	//hexdump(OutMAC, 8);
	//DPRINTF("InMAC:");
	//hexdump(InMAC, 4);
	if (iRet != 0)
	{
		return -1;
	}

	if (memcmp_ex(OutMAC, InMAC, 4) != 0)
	{
		DPRINTF("memcmp_ex != 0 \n");
		return 1;
	}

	DPRINTF("return 0 \n");
	return 0;
}

//128 bit AES key encrypt 128 bit data
int SecAesEnc(byte *aesKey, byte *inputBlock, byte *outputBlock)
{
//	Jason remove it on 20191225 for HM chip	
#if 0
	int iRet;

	iRet = ucl_aes(outputBlock, inputBlock, aesKey, UCL_AES_KEYLEN_128, UCL_CIPHER_ENCRYPT);
	if (iRet != UCL_OK)
	{
		DPRINTF("ucl_aes return = %d, error\n", iRet);
		return -1;
	}

	return 0;
#else
	mh_aes(aesKey, inputBlock, outputBlock, ENC);
	return 0;
#endif
// Jason remove end
}

/*Load pinpad Settings
	Return value:
*/
int LoadSetting(PINPAD_PARA * pSetting)
{
	return SmemLoadParameter(pSetting);
}

/*Set pinpad Settings
	Return value:
	0:OK.
	-1: Open file error
	-2:Seek file error
	-3:Write file error
*/
int SaveSetting(PINPAD_PARA * pSetting)
{
	return SmemSetParameter(pSetting);
}

#define SNVSRAM_TEST_DATA       0x12345678
#define SNVSRAM_TEST_DATA_ADD   (MML_MEM_SNVSRAM_BASE + 0x100)

int checkSecNVSRAM(void){
	int iRet;
   	PINPAD_PARA tmpPINPad_para;
   	volatile unsigned int * pIntAddr;

   pIntAddr = (volatile unsigned int *)MML_MEM_SNVSRAM_BASE;
   *pIntAddr=0xFF;

   iRet = LoadSetting(&tmpPINPad_para);
   if (iRet != 0)
   {
		err("LoadSetting error");
		return -1;
   }
	if(memcmp_ex(tmpPINPad_para.SecurityVersion, SECURITY_VERSION_STRING, strlen(SECURITY_VERSION_STRING)) != 0){
		return 1;
	}
	return 0;
}

static void NVIC_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
    
	//NVIC_SetPriorityGrouping(NVIC_PriorityGroup_3);
		
	NVIC_ClearPendingIRQ(TRNG_IRQn);
	NVIC_InitStructure.NVIC_IRQChannel = TRNG_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 6;       //0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;      //2
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

#if CRYPT_INT
	NVIC_ClearPendingIRQ(CRYPT0_IRQn);
	NVIC_InitStructure.NVIC_IRQChannel = CRYPT0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
 	NVIC_Init(&NVIC_InitStructure);
#endif
}

void HMSecInit(void)
{
	uint32_t ver;
	
	SYSCTRL_AHBPeriphClockCmd(SYSCTRL_AHBPeriph_CRYPT, ENABLE);
	SYSCTRL_AHBPeriphResetCmd(SYSCTRL_AHBPeriph_CRYPT, ENABLE);
	SYSCTRL_APBPeriphClockCmd(SYSCTRL_APBPeriph_TRNG, ENABLE);
	//SYSCTRL_AHBPeriphResetCmd(SYSCTRL_APBPeriph_TRNG, ENABLE);
	
	mh_crypt_it_clear();          // 加解密完成都会产生中断
	
	mh_rand_init();               //硬件产生随机数
    NVIC_Configuration();
	ver = mh_crypt_version();
	DPRINTF("MegaHunt SCPU secure lib version is V%02x.%02x.%02x.%02x\n", ver >> 24, (ver >> 16)&0xFF, (ver>>8)&0xFF, ver & 0xFF);
	//DPRINTF("MegaHunt SCPU Crypt Test V1.0 start......\r\n");
}

void r_printf(uint32_t b, char *s)
{
    if (0 != b)
    {
        DPRINTF("pass: ");DPRINTF("%s", s);
    }
    else
    {
        DPRINTF("fail: ");DPRINTF("%s", s);
        while(1);
    }
}

void test_aes(void)
{
	uint32_t t;
	mh_pack_mode_def modes[2] = { ECB, CBC};
    mh_rng_callback f_rng = mh_rand_p;

	uint8_t plain[] = {0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9F, 0x96, 0xE9, 0x3D, 0x7E, 0x11, 0x73, 0x93, 0x17, 0x2A, 
										 0xAE, 0x2D, 0x8A, 0x57, 0x1E, 0x03, 0xAC, 0x9C, 0x9E, 0xB7, 0x6F, 0xAC, 0x45, 0xAF, 0x8E, 0x51};
		 
	uint8_t key128[] = {0x2B, 0x7E, 0x15, 0x16, 0x28, 0xAE, 0xD2, 0xA6, 0xAB, 0xF7, 0x15, 0x88, 0x09, 0xCF, 0x4F, 0x3C};
	uint8_t key192[] = {0x8E, 0x73, 0xB0, 0xF7, 0xDA, 0x0E, 0x64, 0x52, 0xC8, 0x10, 0xF3, 0x2B, 0x80, 0x90, 0x79, 0xE5, 
											0X62, 0xF8, 0xEA, 0xD2, 0x52, 0x2C, 0x6B, 0x7B};
	uint8_t key256[] = {0x60, 0x3D, 0xEB, 0x10, 0x15, 0xCA, 0x71, 0xBE, 0x2B, 0x73, 0xAE, 0xF0, 0x85, 0x7D, 0x77, 0x81, 
											0x1F, 0x35, 0x2C, 0x07, 0x3B, 0x61, 0x08, 0xD7, 0x2D, 0x98, 0x10, 0xA3, 0x09, 0x14, 0xDF, 0xF4};
		
	uint8_t iv[] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};

	uint8_t eCipher[3][32] = {{0x3A, 0xD7, 0x7B, 0xB4, 0x0D, 0x7A, 0x36, 0x60, 0xA8, 0x9E, 0xCA, 0xF3, 0x24, 0x66, 0xEF, 0x97, 
												0xF5, 0xD3, 0xD5, 0x85, 0x03, 0xB9, 0x69, 0x9D, 0xE7, 0x85, 0x89, 0x5A, 0x96, 0xFD, 0xBA, 0xAF},
											 {0xBD, 0x33, 0x4F, 0x1D, 0x6E, 0x45, 0xF2, 0x5F, 0xF7, 0x12, 0xA2, 0x14, 0x57, 0x1F, 0xA5, 0xCC,
												0x97, 0x41, 0x04, 0x84, 0x6D, 0x0A, 0xD3, 0xAD, 0x77, 0x34, 0xEC, 0xB3, 0xEC, 0xEE, 0x4E, 0xEF},
											 {0xF3, 0xEE, 0xD1, 0xBD, 0xB5, 0xD2, 0xA0, 0x3C, 0x06, 0x4B, 0x5A, 0x7E, 0x3D, 0xB1, 0x81, 0xF8,												 
												0x59, 0x1C, 0xCB, 0x10, 0xD4, 0x10, 0xED, 0x26, 0xDC, 0x5B, 0xA7, 0x4A, 0x31, 0x36, 0x28, 0x70}};	

	uint8_t cCipher[3][32] = {{0x76, 0x49, 0xAB, 0xAC, 0x81, 0x19, 0xB2, 0x46, 0xCE, 0xE9, 0x8E, 0x9B, 0x12, 0xE9, 0x19, 0x7D, 
														 0x50, 0x86, 0xCB, 0x9B, 0x50, 0x72, 0x19, 0xEE, 0x95, 0xDB, 0x11, 0x3A, 0x91, 0x76, 0x78, 0xB2},
														{0x4F, 0x02, 0x1D, 0xB2, 0x43, 0xBC, 0x63, 0x3D, 0x71, 0x78, 0x18, 0x3A, 0x9F, 0xA0, 0x71, 0xE8, 
														 0xB4, 0xD9, 0xAD, 0xA9, 0xAD, 0x7D, 0xED, 0xF4, 0xE5, 0xE7, 0x38, 0x76, 0x3F, 0x69, 0x14, 0x5A},
														{0xF5, 0x8C, 0x4C, 0x04, 0xD6, 0xE5, 0xF1, 0xBA, 0x77, 0x9E, 0xAB, 0xFB, 0x5F, 0x7B, 0xFB, 0xD6, 
														 0x9C, 0xFC, 0x4E, 0x96, 0x7E, 0xDB, 0x80, 0x8D, 0x67, 0x9F, 0x77, 0x7B, 0xC6, 0x70, 0x2C, 0x7D}}; 

	uint8_t cipher[32];
	uint8_t mplain[32];

	dbg("\nAES Test In\n");
														 
	memset(cipher, 0, sizeof(cipher));
	memset(mplain, 0, sizeof(mplain));
 	mh_aes_enc(modes[0], cipher, sizeof(cipher), plain, sizeof(plain), key128, MH_AES_128, iv, f_rng, NULL);
	mh_aes_dec(modes[0], mplain, sizeof(mplain), cipher, sizeof(cipher), key128, MH_AES_128, iv, f_rng, NULL);
	t = (!memcmp(&eCipher[0][0], cipher, sizeof(cipher)) && !memcmp(mplain, plain, sizeof(plain)));
	r_printf(t, "AES ECB key128 test\n");

	
	memset(cipher, 0, sizeof(cipher));
	memset(mplain, 0, sizeof(mplain));
 	mh_aes_enc(modes[0], cipher, sizeof(cipher), plain, sizeof(plain), key192, MH_AES_192, iv, f_rng, NULL);
	mh_aes_dec(modes[0], mplain, sizeof(mplain), cipher, sizeof(cipher), key192, MH_AES_192, iv, f_rng, NULL);
	t = (!memcmp(&eCipher[1][0], cipher, sizeof(cipher)) && !memcmp(mplain, plain, sizeof(plain)));
	r_printf(t, "AES ECB key192 test\n");
												 

	memset(cipher, 0, sizeof(cipher));
	memset(mplain, 0, sizeof(mplain));
 	mh_aes_enc(modes[0], cipher, sizeof(cipher), plain, sizeof(plain), key256, MH_AES_256, iv, f_rng, NULL);
	mh_aes_dec(modes[0], mplain, sizeof(mplain), cipher, sizeof(cipher), key256, MH_AES_256, iv, f_rng, NULL);
	t = (!memcmp(&eCipher[2][0], cipher, sizeof(cipher)) && !memcmp(mplain, plain, sizeof(plain)));
	r_printf(t, "AES ECB key256 test\n");

	
 	memset(cipher, 0, sizeof(cipher));
 	memset(mplain, 0, sizeof(mplain));
  	mh_aes_enc(modes[1], cipher, sizeof(cipher), plain, sizeof(plain), key128, MH_AES_128, iv, f_rng, NULL);
 	mh_aes_dec(modes[1], mplain, sizeof(mplain), cipher, sizeof(cipher), key128, MH_AES_128, iv, f_rng, NULL);
 	t = (!memcmp(&cCipher[0][0], cipher, sizeof(cipher)) && !memcmp(mplain, plain, sizeof(plain)));
 	r_printf(t, "AES CBC key128 test\n");

 	memset(cipher, 0, sizeof(cipher));
 	memset(mplain, 0, sizeof(mplain));
  	mh_aes_enc(modes[1], cipher, sizeof(cipher), plain, sizeof(plain), key192, MH_AES_192, iv, f_rng, NULL);
 	mh_aes_dec(modes[1], mplain, sizeof(mplain), cipher, sizeof(cipher), key192, MH_AES_192, iv, f_rng, NULL);
 	
 	t = (!memcmp(&cCipher[1][0], cipher, sizeof(cipher)) && !memcmp(mplain, plain, sizeof(plain)));
 	r_printf(t, "AES CBC key192 test\n");

 	memset(cipher, 0, sizeof(cipher));
 	memset(mplain, 0, sizeof(mplain));
  	mh_aes_enc(modes[1], cipher, sizeof(cipher), plain, sizeof(plain), key256, MH_AES_256, iv, f_rng, NULL);
 	mh_aes_dec(modes[1], mplain, sizeof(mplain), cipher, sizeof(cipher), key256, MH_AES_256, iv, f_rng, NULL);
 	t = (!memcmp(&cCipher[2][0], cipher, sizeof(cipher)) && !memcmp(mplain, plain, sizeof(plain)));
 	r_printf(t, "AES CBC key256 test\n");
}


void test_tdes(void)
{
    
    mh_rng_callback f_rng = mh_rand_p;
    uint32_t t;
    mh_pack_mode_def modes[2] = { ECB, CBC};
    char *cmodes[2] = {"ECB", "CBC"};

    uint8_t PLAIN[8] = {0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9F, 0x96};

    mh_tdes_key_def KEY = {{0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF},
                        {0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01},
                        {0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23}};
                                        
    uint8_t IV[8] = {0xF6, 0x9F, 0x24, 0x45, 0xDF, 0x4F, 0x9B, 0x17};
    

    uint8_t eCrypt[2][8] = {{0x06, 0xED, 0xE3, 0xD8, 0x28, 0x84, 0x09, 0x0A},
                            {0x71, 0x47, 0x72, 0xF3, 0x39, 0x84, 0x1D, 0x34}};

    uint8_t cCrypt[2][8] = {{0x74, 0x01, 0xCE, 0x1E, 0xAB, 0x6D, 0x00, 0x3C},
                             {0x20, 0x79, 0xC3, 0xD5, 0x3A, 0xA7, 0x63, 0xE1}};
                                                 
    uint8_t plain[8];
    uint8_t cipher[8];


    uint8_t xPlain[2][1024];
    uint8_t xCrypt[1024];
    
    uint32_t m;

                                                 
    DPRINTF("\nTDES Test In\n");

    memset(plain, 0, sizeof(plain));
    memset(cipher, 0, sizeof(cipher));
    //===================ECB======================
    mh_tdes_enc(modes[0], cipher, sizeof(cipher), PLAIN, sizeof(PLAIN), &KEY, NULL, f_rng, NULL);
    t = (!memcmp(&eCrypt[1][0], cipher, sizeof(cipher)));
    r_printf(t, "TDES_Encrypt ECB test\n");
    
    mh_tdes_dec(modes[0], plain, sizeof(plain), cipher, sizeof(cipher), &KEY, NULL, f_rng, NULL);
    t = (!memcmp(plain, PLAIN, sizeof(PLAIN)));
    r_printf(t, "TDES_Decrypt ECB test\n");
    
    //===================CBC======================
    memset(plain, 0, sizeof(plain));
    memset(cipher, 0, sizeof(cipher));
    mh_tdes_enc(modes[1], cipher, sizeof(cipher), PLAIN, sizeof(PLAIN), &KEY, IV, f_rng, NULL);
    //cmp cipher
    t = (!memcmp(&cCrypt[1][0], cipher, sizeof(cipher)));
    r_printf(t, "TDES_Encrypt CBC test\n");
    
    mh_tdes_dec(modes[1], plain, sizeof(plain), cipher, sizeof(cipher), &KEY, IV, f_rng, NULL);

    //cmp cipher
    t = (!memcmp(plain, PLAIN, sizeof(PLAIN)));
    r_printf(t, "TDES_Decrypt CBC test\n");

    
    //===================xLen=================================================================
    for (m = 0; m < 2; m++)
    {
        char s[30] = {0};
        memset(xPlain, 0, sizeof(xPlain));
        mh_rand(xPlain[0], sizeof(xPlain[0]));
        mh_tdes_enc(modes[m],  xCrypt, sizeof(xCrypt), xPlain[0], sizeof(xPlain[0]), &KEY, NULL, f_rng, NULL);
        mh_tdes_dec(modes[m],  xPlain[1], sizeof(xPlain[1]), xCrypt, sizeof(xCrypt), &KEY, NULL, f_rng, NULL);
        
        t = (!memcmp(xPlain[1], xPlain[0], sizeof(cipher)));
        sprintf(s, "TDES %d %s Test\n",sizeof(xPlain[0]), cmodes[m]);
        r_printf(t, s);
    }
}



/*
 * Export into an HEX ASCII string
 */
int bn_write_string(char *s, uint32_t *slen, const uint32_t *content, uint32_t len)
{
    int32_t ret = 0;
    uint32_t n;
    char *p;

	int32_t c;
	uint32_t i, j, k;

    n = len	* 8;
    n += 2;		//"\0"

    if( *slen < n )
    {
        *slen = n;
        return( -1 );
    }

    p = s;
 
	for( i = len, k = 0; i > 0; i-- )
	{
		for( j = 4; j > 0; j-- )
		{
			c = ( content[i - 1] >> ( ( j - 1 ) << 3) ) & 0xFF;

			if( c == 0 && k == 0 && ( i + j + 3 ) != 0 )
				continue;

			*(p++) = "0123456789ABCDEF" [c / 16];
			*(p++) = "0123456789ABCDEF" [c % 16];
			k = 1;
		}
	}
   
    *p++ = '\0';
    *slen = p - s;
	
    return( ret );
}


int bn_printf(const uint32_t *r, uint32_t len)
{
    char *s = NULL;
    uint32_t s_len;
    if(r == NULL || len == 0)
        return -1;
    //the X most significant bits number
    s_len = len;
    //radix is 16
    s_len = (s_len * 32) >> 2;
    s_len += 4;
    s = (char *)malloc(s_len);
    bn_write_string(s, &s_len, r, len);
    if(s_len <= 1)
        dbg("0");
    else
        dbg("%s", s);
    
    free(s);
    return 0;
}


uint32_t mh_trand(void *rand, uint32_t bytes);

void rand_self_test(void)
{
	uint32_t i;
	uint32_t rand[256]; 
	
	for(i = 1; i < 64; i++)
	{
		memset(rand, 0, sizeof(rand));
		mh_trand(rand, i);
		bn_printf(rand, sizeof(rand)/4);
		dbg("\n");
	}
}

void jsd_rand_self_test(void)
{
	uint32_t i;
	uint8_t rand[512]; 
	
	for(i = 1; i <= 400; i++)
	{
		memset(rand, 0, sizeof(rand));
		mh_trand(rand, i);
		hexdumpraw(rand, i);
		dbg("\n");
	}
}

//Get 4 byte rand data
tti_int32 rand_hardware_byte(tti_byte *rand)
{
	tti_int32 ret;
	tti_int32 *ptmp;

	DPRINTF("rand_hardware_byte\n");
	//LOGE("RSA :rand_hardware_byte");
//Jason Note: Need to implment HM security function on 2019.12.26
	ret = mh_trand(rand, 4);
	//LOGD("mh_trand return = %d\n", ret);
	//Jason note end
	if (ret != 0)
	{
		DPRINTF("mh_trand ERR\n");
	//	return ret;
	}

	hexdump(rand, 4);
	ptmp = (tti_int32 *)rand;
	//DPRINTF("RSA:*ptmp = 0x%04x\n", *ptmp);
	return *ptmp;
}

//Get N byte rand data
int rand_hardware_byte_ex(tti_byte *rand, int randLen)
{
	//int ret;

	DPRINTF("rand_hardware_byte_ex\n");
	//LOGE("RSA :rand_hardware_byte");
//Jason Note: Need to implment HM security function on 2019.12.26
	//ret = mh_trand(rand, randLen);
	//dbg("mh_trand ret = %d\n", ret);
	mh_trand(rand, randLen);
//Jason note end
	hexdumpraw(rand, randLen);
	return 0;
}

void test_jsd_tdes(void)
{
	byte tmp1[100];
	byte tmp2[100];
	byte tmp3[100];
	byte out[100];
	byte key[100];

	memcpy(tmp1, "12345678", strlen("12345678"));
	memcpy(tmp2, "12341234", strlen("12341234"));
	memcpy(tmp3, "11112222", strlen("11112222"));

	memcpy(key, "1234567887654321", strlen("1234567887654321"));

	hexdumpEx("Key", key, 16);
	hexdumpEx("in1", tmp1, 8);
	Des(key, tmp1, out, 1);	
	hexdumpEx("Enc out1", out, 8);

	hexdumpEx("Key", key, 16);
	hexdumpEx("in2", tmp2, 8);
	Des3(key, tmp2, out, 1);
	hexdumpEx("Enc out2", out, 8);

	hexdumpEx("Key", key, 16);
	hexdumpEx("in3", tmp3, 8);
	Des3(key, tmp3, out, 1);
	hexdumpEx("Enc out3", out, 8);

	hexdumpEx("Key", key, 16);
	hexdumpEx("in1", tmp1, 8);
	Des3(key, tmp1, out, 0);
	hexdumpEx("Dec out1", out, 8);

	hexdumpEx("Key", key, 16);
	hexdumpEx("in2", tmp2, 8);
	Des3(key, tmp2, out, 0);
	hexdumpEx("Dec out2", out, 8);

	hexdumpEx("Key", key, 16);
	hexdumpEx("in3", tmp3, 8);
	Des3(key, tmp3, out, 0);
	hexdumpEx("Dec out3", out, 8);
}

void test_jsd_des(void)
{
	byte tmp1[100];
	byte tmp2[100];
	byte tmp3[100];
	byte out[100];
	byte key[100];

	memcpy(tmp1, "12345678", strlen("12345678"));
	memcpy(tmp2, "12341234", strlen("12341234"));
	memcpy(tmp3, "11112222", strlen("11112222"));

	memcpy(key, "1234567812345678", strlen("1234567812345678"));

	hexdumpEx("Key", key, 8);
	hexdumpEx("in1", tmp1, 8);
	Des(key, tmp1, out, 1);	
	hexdumpEx("Enc out1", out, 8);

	hexdumpEx("Key", key, 8);
	hexdumpEx("in2", tmp2, 8);
	Des(key, tmp2, out, 1);
	hexdumpEx("Enc out2", out, 8);

	hexdumpEx("Key", key, 8);
	hexdumpEx("in3", tmp3, 8);
	Des(key, tmp3, out, 1);
	hexdumpEx("Enc out3", out, 8);

	hexdumpEx("Key", key, 8);
	hexdumpEx("in1", tmp1, 8);
	Des(key, tmp1, out, 0);
	hexdumpEx("Dec out1", out, 8);

	hexdumpEx("Key", key, 8);
	hexdumpEx("in2", tmp2, 8);
	Des(key, tmp2, out, 0);
	hexdumpEx("Dec out2", out, 8);

	hexdumpEx("Key", key, 8);
	hexdumpEx("in3", tmp3, 8);
	Des(key, tmp3, out, 0);
	hexdumpEx("Dec out3", out, 8);
}

void test_jsd_aes(void)
{
	byte inBuffer[3][100];
	byte outBuffer[3][100];
	byte key[100];
	int loop = 0;

	memcpy(inBuffer[0], "1234567812345678", strlen("1234567812345678"));
	memcpy(inBuffer[1], "1234567887654321", strlen("1234567887654321"));
	memcpy(inBuffer[2], "1234567812341234", strlen("1234567812341234"));

	memcpy(key, "1234567812345678", strlen("1234567812345678"));
	while(loop<sizeof(inBuffer)/sizeof(inBuffer[0]))
	{
		hexdumpEx("Key", key, 16);
		hexdumpEx("InBuffer", inBuffer[loop], 16);
		mh_aes(key, inBuffer[loop], outBuffer[loop], ENC);
		hexdumpEx("Enc out", outBuffer[loop], 16);
		
		hexdumpEx("Key", key, 16);
		hexdumpEx("InBuffer", inBuffer[loop], 16);
		mh_aes(key, inBuffer[loop], outBuffer[loop], DEC);
		hexdumpEx("DEC out", outBuffer[loop], 16);
		loop++;
	}
	
}

static int SmemInitOtpFile(void);

int SecDirCreat(void)
{
	int ret;
	dbg("SecDirCreat()\n");
	
	ret = sys_lfs_mkdir(DIR_SECURITY);
	if (ret != 0)
	{
		if (ret == LFS_ERR_EXIST)
		{
			LOGD("%s already exist\n", DIR_SECURITY);
		}else
		{
			LOGD("%s sys_lfs_mkdir ERR return = %d\n", DIR_SECURITY, ret);
			return -1;
		}	//return;
	}

#ifdef OTP_FLASH
	ret = sys_lfs_mkdir(DIR_OTP_FLASH);
	if (ret != 0)
	{
		if (ret == LFS_ERR_EXIST)
		{
			LOGD("%s already exist\n", DIR_OTP_FLASH);
		}else
		{
			LOGD("%s sys_lfs_mkdir ERR return = %d\n", DIR_OTP_FLASH, ret);
			return -1;
		}	//return;
	}else
	{
		SmemInitOtpFile();
	}
#endif
	
	return 0;
}

int SecEraseTusnKey(void);

int SecInit(void)
{
	int iRet;
	PINPAD_PARA tmpPINPad_para;
	byte randAES[AES_128_KEY_BLOCK_SIZE];

#ifndef DISABLE_ALLSENSOR
	if(checkIfSensorEnabled() == 0)
	{
		DPRINTF("checkIfSensorEnabled return 0, Tamper Enable is not open\n");
		//LcdClear();
		//LcdPutsc("Sensor not Enable", 2);
		//mdelay(2*1000);
		return -2;
	}
#else
    SecDirCreat();
	return 0;
#endif
  
	//HMSecInit();
	//bpk_init();
	SecDirCreat();

	if (SmemIsKeyStoreKekExistViaBPK() != TRUE)
	{
		DPRINTF("SmemIsKeyStoreKekExistViaBPK, Need clear security\n");
		DPRINTF("stored data is invalid, re-gen AES key and re-write data to NVSRAM\n");

		DPRINTF("List SecurityVersion\n");
		rand_hardware_byte_ex(randAES, AES_128_KEY_BLOCK_SIZE);
		SmemWriteKeyStoreKeyViaBPK(randAES);
		memset(randAES, 0, sizeof(randAES));
		
		SmemInitSecFile();	
		
		SecEraseAllKey();
		SecEraseParameter();
        
		SecEraseTusnKey();
		SecEraseTransmissionKey();
		memset(&tmpPINPad_para, 0x00, sizeof(PINPAD_PARA));
		memcpy(tmpPINPad_para.SecurityVersion, SECURITY_VERSION_STRING, strlen(SECURITY_VERSION_STRING));
		tmpPINPad_para.KeyMode = KEY_MODE_UN_INIT;
		
		SaveSetting(&tmpPINPad_para);
   }else
   {
	   iRet = LoadSetting(&tmpPINPad_para);
	   if (iRet != 0)
	   {
			err("LoadSetting error");
			return -1;
	   }

	   dbg("list SecurityVersion \n");
	   hexdump((byte*)(tmpPINPad_para.SecurityVersion), 10);
	   if (memcmp_ex(tmpPINPad_para.SecurityVersion, SECURITY_VERSION_STRING, 
	   		strlen(SECURITY_VERSION_STRING)) != 0)
	   {
			DPRINTF("SECURITY_VERSION_STRING Not match, Need clear security\n");
			DPRINTF("stored data is invalid, re-gen AES key and re-write data to NVSRAM\n");
			DPRINTF("List SecurityVersion\n");
			rand_hardware_byte_ex(randAES, AES_128_KEY_BLOCK_SIZE);
			SmemWriteKeyStoreKeyViaBPK(randAES);
			memset(randAES, 0, sizeof(randAES));
			
			SmemInitSecFile();	
			
			SecEraseAllKey();
			SecEraseParameter();
           
			SecEraseTusnKey();
			SecEraseTransmissionKey();
			memset(&tmpPINPad_para, 0x00, sizeof(PINPAD_PARA));
			memcpy(tmpPINPad_para.SecurityVersion, SECURITY_VERSION_STRING, strlen(SECURITY_VERSION_STRING));
			tmpPINPad_para.KeyMode = KEY_MODE_UN_INIT;
			
			SaveSetting(&tmpPINPad_para);
	   }
   }

	pStaDUKPTPara = &staDUKPTPara;

   return 0;
}

void SecGetVersion(char *szVer)
{
	strcpy(szVer, SEC_LIB_VER);
}


int CheckSecurityVersion()
{
	/*
	Security_Func_Result tParaResult;
	tParaResult = LoadSetting();

	if (memcmp_ex(tParaResult.PINPad_para.SecurityVersion, SECURITY_VERSION, strlen(SECURITY_VERSION)) != 0)
	{
		DPRINTF("Need clear security\n");
		SecEraseAllKey();
		SecEraseParameter();
		
		tParaResult = LoadSetting();
		memcpy(tParaResult.PINPad_para.SecurityVersion, SECURITY_VERSION, strlen(SECURITY_VERSION));
		SaveSetting(tParaResult.PINPad_para);
	}else
	{
		DPRINTF("No need clear security\n");
	}

	*/
	return 0;
}


//////////////////////////////////////////////////////////////

int DUKPTNewKey(void);
int DUKPTNewKey1(void);
int DUKPTNewKey2(void);
int DUKPTNewKey3(void);
int DUKPTNewKey4(void);

int NonReverKeyGenProc(void);
void Exit(void);
int DUKPTRequesPINEntry1(void);
int RequesPINEntry2(void);
void DUKPTSetBit(void);
void TripleDEAEncrypt(void);
//void DerivateInitialKey(void);

unsigned int DUKPTGetEncryptCounter(void);
void DUKPTSetEncryptCounter(int EncryptionCounter);
//void Des(char *key, char *in, char *out, int enc);
//void Des3(char *key, char *in, char *out, int enc);

unsigned int DUKPTGetEncryptCounter()
{
	int counter = 0;
	unsigned char bit;

	bit = pStaDUKPTPara->KeySerialNumber[7] & 0x10; // 21bit
	if (bit)
	{
		counter += 0x00100000;
	}
	bit = pStaDUKPTPara->KeySerialNumber[7] & 0x08; // 20bit
	if (bit)
	{
		counter += 0x00080000;
	}
	bit = pStaDUKPTPara->KeySerialNumber[7] & 0x04; // 19bit
	if (bit)
	{
		counter += 0x00040000;
	}
	bit = pStaDUKPTPara->KeySerialNumber[7] & 0x02; // 18
	if (bit)
	{
		counter += 0x00020000;
	}
	bit = pStaDUKPTPara->KeySerialNumber[7] & 0x01; // 17
	if (bit)
	{
		counter += 0x00010000;
	}
	bit = pStaDUKPTPara->KeySerialNumber[8] & 0x80; // 16
	if (bit)
	{
		counter += 0x00008000;
	}
	bit = pStaDUKPTPara->KeySerialNumber[8] & 0x40; // 15
	if (bit)
	{
		counter += 0x00004000;
	}
	bit = pStaDUKPTPara->KeySerialNumber[8] & 0x20; // 14
	if (bit)
	{
		counter += 0x00002000;
	}
	bit = pStaDUKPTPara->KeySerialNumber[8] & 0x10; // 13
	if (bit)
	{
		counter += 0x00001000;
	}
	bit = pStaDUKPTPara->KeySerialNumber[8] & 0x08; // 12
	if (bit)
	{
		counter += 0x00000800;
	}
	bit = pStaDUKPTPara->KeySerialNumber[8] & 0x04; // 11
	if (bit)
	{
		counter += 0x00000400;
	}
	bit = pStaDUKPTPara->KeySerialNumber[8] & 0x02; // 10
	if (bit)
	{
		counter += 0x00000200;
	}
	bit = pStaDUKPTPara->KeySerialNumber[8] & 0x01; // 09
	if (bit)
	{
		counter += 0x00000100;
	}
	bit = pStaDUKPTPara->KeySerialNumber[9] & 0x80; // 08
	if (bit)
	{
		counter += 0x00000080;
	}
	bit = pStaDUKPTPara->KeySerialNumber[9] & 0x40; // 07
	if (bit)
	{
		counter += 0x00000040;
	}
	bit = pStaDUKPTPara->KeySerialNumber[9] & 0x20; // 06
	if (bit)
	{
		counter += 0x00000020;
	}
	bit = pStaDUKPTPara->KeySerialNumber[9] & 0x10; // 05
	if (bit)
	{
		counter += 0x00000010;
	}
	bit = pStaDUKPTPara->KeySerialNumber[9] & 0x08; // 04
	if (bit)
	{
		counter += 0x00000008;
	}
	bit = pStaDUKPTPara->KeySerialNumber[9] & 0x04; // 03
	if (bit)
	{
		counter += 0x00000004;
	}
	bit = pStaDUKPTPara->KeySerialNumber[9] & 0x02; // 02
	if (bit)
	{
		counter += 0x00000002;
	}
	bit = pStaDUKPTPara->KeySerialNumber[9] & 0x01; // 01
	if (bit)
	{
		counter += 0x00000001;
	}
	
	
	return counter;
}

void DUKPTSetEncryptCounter(int EncryptionCounter)
{
	unsigned int bit;
	
	pStaDUKPTPara->KeySerialNumber[7] &= 0xE0;
	pStaDUKPTPara->KeySerialNumber[8] = 0;
	pStaDUKPTPara->KeySerialNumber[9] = 0;
	bit = EncryptionCounter & 0x00100000; // 21
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[7] |= 0x10;
	}
	bit = EncryptionCounter & 0x00080000; // 20
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[7] |= 0x08;
	}
	bit = EncryptionCounter & 0x00040000; // 19
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[7] |= 0x04;
	}
	bit = EncryptionCounter & 0x00020000; // 18
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[7] |= 0x02;
	}
	bit = EncryptionCounter & 0x00010000; // 17
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[7] |= 0x01;
	}
	bit = EncryptionCounter & 0x00008000; // 16
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[8] |= 0x80;
	}
	bit = EncryptionCounter & 0x00004000; // 15
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[8] |= 0x40;
	}
	bit = EncryptionCounter & 0x00002000; // 14
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[8] |= 0x20;
	}
	bit = EncryptionCounter & 0x00001000; // 13 
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[8] |= 0x10;
	}
	bit = EncryptionCounter & 0x00000800; // 12
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[8] |= 0x08;
	}
	bit = EncryptionCounter & 0x00000400; // 11
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[8] |= 0x04;
	}
	bit = EncryptionCounter & 0x00000200; // 10
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[8] |= 0x02;
	}
	bit = EncryptionCounter & 0x00000100; // 09
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[8] |= 0x01;
	}
	bit = EncryptionCounter & 0x00000080; // 08
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[9] |= 0x80;
	}
	bit = EncryptionCounter & 0x00000040; // 07
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[9] |= 0x40;
	}
	bit = EncryptionCounter & 0x00000020; // 06
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[9] |= 0x20;
	}
	bit = EncryptionCounter & 0x00000010; // 05
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[9] |= 0x10;
	}
	bit = EncryptionCounter & 0x00000008; // 04
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[9] |= 0x08;
	}
	bit = EncryptionCounter & 0x00000004; // 03
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[9] |= 0x04;
	}
	bit = EncryptionCounter & 0x00000002; // 02
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[9] |= 0x02;
	}
	bit = EncryptionCounter & 0x00000001; // 01
	if (bit)
	{
		pStaDUKPTPara->KeySerialNumber[9] |= 0x01;
	}
	
}


int SecMemDUKPTLoad(DUKPT_PARAMETER *pDukpt_Para)
{
	return SmemRead(DUKPT_BASE, (unsigned char *) pDukpt_Para, sizeof(DUKPT_PARAMETER));
}

int SecMemDUKPTSave(DUKPT_PARAMETER *pDukpt_Para)
{
	return SmemWrite(DUKPT_BASE, (unsigned char *) pDukpt_Para, sizeof(DUKPT_PARAMETER));
}


/*
"Load Initial Key" (External Command) 
	1) Store the initial PIN encryption key, as received in the externally initiated command,
		into Future Key Register #21. 
	2) Generate and store the LRC on this Future Key Register. 
	3) Write the address of Future Key Register #21 into the Current Key Pointer. 
	4) Store the Key Serial Number, as received in the externally initiated command,
		into the Key Serial Number 	Register. (This register is the concatenation of
		the Initial Key Serial Number Register and the Encryption Counter.) 
	5) Clear the Encryption Counter (the 21 right-most bits of
		the Key Serial Number Register). 
	6) Set bit #1 (the left-most bit) of the Shift Register to "one", 
		setting all of the other bits to "zero". 
	7) Go to "New Key-3". 
*/
int SecLoadInitDupkt(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{	
	memcpy( OutBuff, "70.", 3);
	OutBuff[3]  = ERR_RANGE;				// assume error
	*OutLen = 4;

	//Jason added on 2018/3/6
	pStaDUKPTPara = &staDUKPTPara;
	//Added end
	
	memset(pStaDUKPTPara, 0, sizeof(DUKPT_PARAMETER));
	//Jason added on 2018/3/6
	SecMemDUKPTSave(pStaDUKPTPara);
	//Added end

	if (InLen != (32+20))
	{
		DPRINTF("Len error\n");
		return -1;
	}
	if (AtoHex(&(pStaDUKPTPara->FutureKey[20][0]), (char*)&InBuff[0], 32))
	{
		DPRINTF("Key error\n");
		memset(pStaDUKPTPara,0,sizeof(DUKPT_PARAMETER));
		return -1;
	}

	pStaDUKPTPara->FutureKey[20][16] = CalculateLRC(&(pStaDUKPTPara->FutureKey[20][0]),16);
	pStaDUKPTPara->CurrentKeyPointer = 20;

	if (AtoHex(pStaDUKPTPara->KeySerialNumber, (char*)&InBuff[32], 20))
	{
		DPRINTF("Serinal Number error\n");
		memset(pStaDUKPTPara,0,sizeof(DUKPT_PARAMETER));
		return -1;
	}

	pStaDUKPTPara->KeySerialNumber[7] &= 0xE0;
	pStaDUKPTPara->KeySerialNumber[8]  = 0;
	pStaDUKPTPara->KeySerialNumber[9]  = 0;
	pStaDUKPTPara->EncryptionCounter = 0;

	pStaDUKPTPara->Shift = 0x00080000;
	memcpy(pStaDUKPTPara->KeyRegister, pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer], 16);
//	DerivateInitialKey();

	if(DUKPTNewKey3() == 0)
	{
		OutBuff[3]  = ERR_OK;
	}
	else
	{
		memset(pStaDUKPTPara, 0, sizeof(DUKPT_PARAMETER));
		return -1;
	}
	
//	SaveSmem();//ffffffff
	SecMemDUKPTSave(pStaDUKPTPara);
	memset(pStaDUKPTPara,0,sizeof(DUKPT_PARAMETER));
	
	return 0;

}

  
/*
"New Key" (Local Label) 
	1) Count the number of "one" bits in the 21-bit Encryption Counter. 
		If this number is less than 10, go to "New Key-1". 
	2) Erase the key at ![Current Key Pointer]. 
	3) Set the LRC for ![Current Key Pointer] to an invalid value
		(e.g., increment the LRC by one). 
	4) Add the Shift Register to the Encryption Counter. 
		(This procedure skips those counter values that would 
		have more than 10 "one" bits.) 
	5) Go to "New Key-2". 
*/
int DUKPTNewKey()
{
	int Count = 0;
	int loop = 0;

	// step 1
	pStaDUKPTPara->EncryptionCounter = DUKPTGetEncryptCounter();
	while (loop < 21)
	{
		if (((pStaDUKPTPara->EncryptionCounter >> loop) % 2) == 1)
		{
			Count++;
		}
		loop++;
	}
	if (Count < 10)
	{
		return DUKPTNewKey1();
	}

	// step 2
	
	memset(pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer],0,17);
	// step 3
	pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer][16] = \
		CalculateLRC(pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer],16) + 1;

	// step 4
	//
	if (Count <= 10)
	{
		pStaDUKPTPara->EncryptionCounter = DUKPTGetEncryptCounter();
		pStaDUKPTPara->EncryptionCounter += pStaDUKPTPara->Shift;
		pStaDUKPTPara->EncryptionCounter &= 0x001FFFFF;
		DUKPTSetEncryptCounter(pStaDUKPTPara->EncryptionCounter);
	}
	
	

	// step 5
	return 	DUKPTNewKey2();
}
  
/*
New Key-1" (Local Label) 
	1) Shift the Shift Register right one bit (end-off). 
		(A "zero" is shifted into position #1, the left-most bit of the register.) 
	2) If the Shift Register now contains all zeros 
		(i.e., the single "one" was shifted off), go to "New Key-4",
		else go to ?New Key-3?. 
*/
int DUKPTNewKey1()
{
	pStaDUKPTPara->Shift = pStaDUKPTPara->Shift >> 1;

	// step 2
	if (pStaDUKPTPara->Shift == 0)
	{
		if(DUKPTNewKey4() != 0)
		{
			return -1;
		}
	}
	else
	{
		if (DUKPTNewKey3() != 0)
		{
			return -1;
		}
		
	}
	return 0;
}
  
/*
New Key-3" (Local Label) 
	1) The Shift Register, right justified in 64 bits, padded to the left with zeros, 
		OR'ed with the 64 right-most bits of the Key Serial Number Register, 
		is transferred into Crypto Register-1. 
	2) Copy ![Current Key Pointer] into the Key Register. 
	3) Call the subroutine Non-reversible Key Generation Process. 
	4) Store the contents of Crypto Register-1 into the left half 
		of the Future Key Register indicated by the 
		position of the single "one" bit in the Shift Register. 
	5) Store the contents of Crypto Register-2 into the right half 
		of the Future Key Register indicated by the 
		position of the single "one" bit in the Shift Register. 
	6) Generate and store the LRC on this Future Key Register. 
	7) Go to "New Key-1". 
*/
int DUKPTNewKey3()
{
	char tmpData[8];
	int loop = 0;
	int index = -1;

	// step 1
	loop = 0;
	index = -1;
	memset(tmpData,0,sizeof(tmpData));
	tmpData[5] = (pStaDUKPTPara->Shift << 8) >> 24;
	tmpData[6] = (pStaDUKPTPara->Shift << 16) >> 24;
	tmpData[7] = (pStaDUKPTPara->Shift << 24) >> 24;
	memcpy(pStaDUKPTPara->Crypto1,pStaDUKPTPara->KeySerialNumber+2,sizeof(pStaDUKPTPara->Crypto1));
	for (loop = 0;loop < 8; loop++)
	{
		pStaDUKPTPara->Crypto1[loop] |= \
			tmpData[loop];
	}

	// step 2
	memcpy(pStaDUKPTPara->KeyRegister, pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer], 
		sizeof(pStaDUKPTPara->KeyRegister));

	// step 3
	if (NonReverKeyGenProc() != 0)
	{
		return -1;
	}

	// step 4
	loop = 0;
	index = -1;
	while (loop < 21)
	{
		if ((pStaDUKPTPara->Shift >> loop) == 1)
		{
			index = loop;
			break;
		}
		else
		{
			loop++;
		}
		
	}

	if (index >= 0 && index < 21)
	{
		memcpy(pStaDUKPTPara->FutureKey[index],\
			pStaDUKPTPara->Crypto1,
			sizeof(pStaDUKPTPara->Crypto1));

	}
	// step 5
	if (index >= 0 && index < 21)
	{
		memcpy(pStaDUKPTPara->FutureKey[index]+8,pStaDUKPTPara->Crypto2,
			sizeof(pStaDUKPTPara->Crypto2));

	}
	// step 6
	pStaDUKPTPara->FutureKey[index][16] = \
		CalculateLRC((unsigned char*)(&(pStaDUKPTPara->FutureKey[index][0])),16);
	
	// step 7
	if (DUKPTNewKey1() != 0)
	{
		return -1;
	}


	return 0;
}
  
/*
New Key-4" (Local Label) 
	1) Erase the key at ![Current Key Pointer]. 
	2) Set the LRC for ![Current Key Pointer] to an invalid value 
		(e.g., increment the LRC by one). 
	3) Add one to the Encryption Counter. 
	4) Go to ?New Key-2?. 

*/
int DUKPTNewKey4()
{
	// step 1
	memset(pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer],0,17);

	// step 2
	pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer][16] = \
		CalculateLRC(pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer],16) + 1;
	//
	pStaDUKPTPara->EncryptionCounter = DUKPTGetEncryptCounter();
	pStaDUKPTPara->EncryptionCounter += 1;
	DUKPTSetEncryptCounter(pStaDUKPTPara->EncryptionCounter);

	// step 3
	if (DUKPTNewKey2() != 0)
	{
		return -1;
	}

	return 0;
}

/*
New Key-2" (Local Label) 
	1) If the Encryption Counter contains all zeros, cease operation.
		(The PIN Entry Device is now inoperative, 
		having encrypted more than 1 million PINs.) If not all zeros, go to ?Exit?. 
*/
int DUKPTNewKey2()
{
	pStaDUKPTPara->EncryptionCounter = DUKPTGetEncryptCounter();
	if (pStaDUKPTPara->EncryptionCounter == 0)
	{
		return -2;
	}
	Exit();
	return 0;
}

/*
Exit" (Local Label) 
	1) Return to original calling routine. 
		(Processing of the current externally initiated command is completed, 
		and the PIN Entry Device is ready for the next command. 
		The Current Key Pointer, Account Number Register, 
		Shift Register, and Crypto Pointer may now be used for other purposes.) 

*/
void Exit()
{
	return;
}
  
/*
Request PIN Entry" (External Command) 
    1) Transfer the primary account number as received in the externally
        initiated command into the Account Number Register. 
    2) Activate the PIN Entry Device keyboard and the Enter key. 
    3) If the PIN is not entered, send the encrypted PIN block 
        response message without the PIN-related data elements and go to "Exit". 
    4) If the PIN is entered, use the card holder-entered PIN and the primary account number
        to generate the clear text PIN block and store it in Crypto Register-1. 
    5) Go to "Request PIN Entry 1". 
*/
void SecDupktGetPINBlockEx(byte *PINBlockXorPANBlock, byte *OutBuff, int *OutLen)
{
    // step 1
    
    memcpy( OutBuff, "91.", 3);
    OutBuff[3]    = ERR_RANGE;                // assume error
    *OutLen = 4;
    /*
    pInterParam->SecDupktGetPINBlock_Para.startPos = 0;
    pInterParam->SecDupktGetPINBlock_Para.AccounLen = 0;
    memset(pInterParam->SecDupktGetPINBlock_Para.PinBlock,0,sizeof(pInterParam->SecDupktGetPINBlock_Para.PinBlock));
    
    memcpy( OutBuff, "71.", 3);
    OutBuff[3]  = ERR_RANGE;                // assume error
    *OutLen = 4;

    if (InLen >= 12)
    {
        pInterParam->SecDupktGetPINBlock_Para.startPos = 0;//InLen - 12;
        pInterParam->SecDupktGetPINBlock_Para.AccounLen = 12;
    }
    else
    {
        return;
    }
    memcpy(pStaDUKPTPara->AccountNumber,InBuff+pInterParam->SecDupktGetPINBlock_Para.startPos,\
        pInterParam->SecDupktGetPINBlock_Para.AccounLen);

    // step 2
    
    
    // 4
    if(BuildPinBlock( sPin, iPinLen, pInterParam->SecDupktGetPINBlock_Para.PinBlock)!= 0)
    {
//        printf("Building PIN block failed");
        return;
    }
    memcpy(pStaDUKPTPara->Crypto1,pInterParam->SecDupktGetPINBlock_Para.PinBlock,8);
    if (BuildAcountBlock((unsigned char*) (&(pStaDUKPTPara->AccountNumber[0])), 
        sizeof(pStaDUKPTPara->AccountNumber), pInterParam->SecDupktGetPINBlock_Para.PinBlock)!= 0)
    {
        return;
    }
    Xor(pStaDUKPTPara->Crypto1,(const char*)pInterParam->SecDupktGetPINBlock_Para.PinBlock);
    */

  
	pStaDUKPTPara = &staDUKPTPara;

	SecMemDUKPTLoad(pStaDUKPTPara);
   
    memcpy (pStaDUKPTPara->Crypto1, PINBlockXorPANBlock, 8);

    /*
    memcpy(PINBlockBuff, InBuff, 16);
    AtoHex(pStaDUKPTPara->Crypto1, PINBlockBuff, 16);
    */
    // step 5
    if (DUKPTRequesPINEntry1() != 0)
    {
    	memset(pStaDUKPTPara, 0, sizeof(DUKPT_PARAMETER));
        return;
    }
    OutBuff[3] = ERR_OK;
    //memcpy(OutBuff+4, "01", 2);
    memcpy(OutBuff+4, pStaDUKPTPara->PinBlockRes, 32);
    *OutLen = 36;
	
	SecMemDUKPTSave(pStaDUKPTPara);
	memset(pStaDUKPTPara,0,sizeof(DUKPT_PARAMETER));
//    SaveSmem();//ffffffff
    return;
}

  
/*
Request PIN Entry" (External Command) 
    1) Transfer the primary account number as received in the externally
        initiated command into the Account Number Register. 
    2) Activate the PIN Entry Device keyboard and the Enter key. 
    3) If the PIN is not entered, send the encrypted PIN block 
        response message without the PIN-related data elements and go to "Exit". 
    4) If the PIN is entered, use the card holder-entered PIN and the primary account number
        to generate the clear text PIN block and store it in Crypto Register-1. 
    5) Go to "Request PIN Entry 1". 
*/
void SecDupktGetPANBlockEx(byte *formatPANBlock, byte *OutBuff, int *OutLen)
{
    // step 1
    
    memcpy( OutBuff, "93.", 3);
    OutBuff[3]    = ERR_RANGE;                // assume error
    *OutLen = 4;
    /*
    pInterParam->SecDupktGetPINBlock_Para.startPos = 0;
    pInterParam->SecDupktGetPINBlock_Para.AccounLen = 0;
    memset(pInterParam->SecDupktGetPINBlock_Para.PinBlock,0,sizeof(pInterParam->SecDupktGetPINBlock_Para.PinBlock));
    
    memcpy( OutBuff, "71.", 3);
    OutBuff[3]  = ERR_RANGE;                // assume error
    *OutLen = 4;

    if (InLen >= 12)
    {
        pInterParam->SecDupktGetPINBlock_Para.startPos = 0;//InLen - 12;
        pInterParam->SecDupktGetPINBlock_Para.AccounLen = 12;
    }
    else
    {
        return;
    }
    memcpy(pStaDUKPTPara->AccountNumber,InBuff+pInterParam->SecDupktGetPINBlock_Para.startPos,\
        pInterParam->SecDupktGetPINBlock_Para.AccounLen);

    // step 2
    
    
    // 4
    if(BuildPinBlock( sPin, iPinLen, pInterParam->SecDupktGetPINBlock_Para.PinBlock)!= 0)
    {
//        printf("Building PIN block failed");
        return;
    }
    memcpy(pStaDUKPTPara->Crypto1,pInterParam->SecDupktGetPINBlock_Para.PinBlock,8);
    if (BuildAcountBlock((unsigned char*) (&(pStaDUKPTPara->AccountNumber[0])), 
        sizeof(pStaDUKPTPara->AccountNumber), pInterParam->SecDupktGetPINBlock_Para.PinBlock)!= 0)
    {
        return;
    }
    Xor(pStaDUKPTPara->Crypto1,(const char*)pInterParam->SecDupktGetPINBlock_Para.PinBlock);
    */

  
	pStaDUKPTPara = &staDUKPTPara;

	SecMemDUKPTLoad(pStaDUKPTPara);
   
    memcpy (pStaDUKPTPara->Crypto1, formatPANBlock, 8);

    /*
    memcpy(PINBlockBuff, InBuff, 16);
    AtoHex(pStaDUKPTPara->Crypto1, PINBlockBuff, 16);
    */
    // step 5
    if (DUKPTRequesPINEntry1() != 0)
    {
    	memset(pStaDUKPTPara, 0, sizeof(DUKPT_PARAMETER));
        return;
    }
    OutBuff[3] = ERR_OK;
    //memcpy(OutBuff+4, "01", 2);
    memcpy(OutBuff+4, pStaDUKPTPara->PinBlockRes, 32);
    *OutLen = 36;
	
	SecMemDUKPTSave(pStaDUKPTPara);
	memset(pStaDUKPTPara,0,sizeof(DUKPT_PARAMETER));
//    SaveSmem();//ffffffff
    return;
}



/*
"Request PIN Entry 1" (Local Label) 
	1) Call the subroutine "Set Bit". 
	2) Write into Current Key Pointer the address of that Future Key Register 
		indicated by the position of the "one" bit in the Shift Register. 
	3) Check the LRC on ![Current Key Pointer]. 
		If this byte is correct (valid key), go to "Request PIN Entry 2". 
	4) If the byte is incorrect, add the Shift Register to 
		the Encryption Counter (to skip over the invalid key). ????
	5) If the Encryption Counter contains all zeros, cease operation. 
		(The PIN Entry Device is now inoperative, having encrypted more than 1 million PINs.) 
	6) Go to "Request PIN Entry 1". 
*/
int DUKPTRequesPINEntry1()
{
	int loop = 0;

	// step 1
	DUKPTSetBit();

	// step 2

	while (loop < 21)
	{
		if ((pStaDUKPTPara->Shift >> loop)%2 == 1)
		{
			break;
		}
		else
		{
			loop++;
		}
	}

	if (loop < 21)
	{
		pStaDUKPTPara->CurrentKeyPointer = loop;
	}
	
	// step 3
	if (pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer][16] == \
		CalculateLRC(pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer],16))
	{
		RequesPINEntry2();
	}
	else
	{
		// step 4
		//
		pStaDUKPTPara->EncryptionCounter = DUKPTGetEncryptCounter();
		pStaDUKPTPara->EncryptionCounter += pStaDUKPTPara->Shift;
		pStaDUKPTPara->EncryptionCounter &= 0x001FFFFF;
		DUKPTSetEncryptCounter(pStaDUKPTPara->EncryptionCounter);


		if (pStaDUKPTPara->EncryptionCounter == 0)
		{
			// step 5
			return -2;
		}
		else
		{
			// step 6
			DUKPTRequesPINEntry1();
		}
		
	}
	return 0;
}


/*
"Request PIN Entry 2" (Local Label) 
	1) Copy ![Current Key Pointer] into the Key Register. 
	2) (Optional: Perform this step if you need to generate a key 
		that will be used in a message authentication 
		process; this step does not affect the generation of the PIN encryption key)
		XOR the value in the Key Register with hexadecimal ?
		0000 0000 0000 FF00 0000 0000 0000 FF00? and save 
		this resultant key in the MAC key register. 
	3) XOR the Key Register with hexadecimal ?0000 0000 0000 00FF 0000 0000 0000 00FF?. 
		(This will produce a variant of the key.) 
	4) Call the subroutine ?Triple-DEA Encrypt?. 
	5) Format and transmit the encrypted PIN block response message, which includes: 
		. The data in the Key Serial Number Register with leading hexadecimal 
		"F's" suppressed (includes the 21-bit Encryption Counter). 
		. The encrypted PIN block in Crypto Register-1. 
	6) Go to "New Key". 
*/
int RequesPINEntry2()
{
	
	
	// step 1
	memcpy(pStaDUKPTPara->KeyRegister,pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer],16);

	// step 2
	memcpy(pStaDUKPTPara->MACKeyRegister,pStaDUKPTPara->KeyRegister,16);
	Xor(pStaDUKPTPara->MACKeyRegister,DUKPT_macCode);
	Xor(pStaDUKPTPara->MACKeyRegister+8,DUKPT_macCode+8);
	// step 3
	Xor(pStaDUKPTPara->KeyRegister,DUKPT_varCode);
	Xor(pStaDUKPTPara->KeyRegister+8,DUKPT_varCode+8);

	// step 4
	TripleDEAEncrypt();

	// step 5
	// message out
	HextoA((char *)(pStaDUKPTPara->PinBlockRes),pStaDUKPTPara->Crypto1,sizeof(pStaDUKPTPara->Crypto1));
	HextoA((char *)(pStaDUKPTPara->PinBlockRes+16),pStaDUKPTPara->KeySerialNumber+2,8);

	// step 6
	if (DUKPTNewKey() != 0)
	{
		return -1;
	}
	
	return 0;
}
  
/*
"Cancel PIN Entry" (External Command) 
	1) Deactivate the PIN Entry Device keyboard. 
	2) Go to "Exit" 
		The following routine may be used if the PIN Entry Device is implemented using 
		electrically erasable programmable read-only memory (EEPROM).
		In this case, those storage areas that are most frequently rewritten 
		(e.g., the four or so highest-numbered Future Key Registers and the 
		corresponding bits of the encryption counter) are stored in volatile 
		random access memory (RAM). Even though a power interruption results 
		in the loss of the contents of the registers in volatile RAM, 
		this loss is not significant, (if power to the PIN Entry Device were turned
		off and on again once a day for 10 years, the number of PINs the device 
		could encrypt would be reduced by less than 5% due to "lost" keys). 
		Note: There may be a loss of synchronization if multiple power 
			  on reset cycles are executed without performing any transactions. 
*/
void CancelPINEntry(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{
	return;
}


/*
1) Convert the input string (4012345678909D987) to hex:
34 30 31 32 33 34 35 36 37 38 39 30 39 44 39 38 37
2) XOR the Initial Vector (all zeros) with the first 8 bytes of the hex value from step 1:
3430313233343536
3) Encrypt the result from step 2 with the left half of the MAC Encryption Key.
4) XOR the result from step 3 with the next 8 bytes of the hex value from step 1. If fewer than 8 bytes
remain, left justify the value and fill the remaining places with binary zeros.
5) Encrypt the result from step 4 with the left half of the MAC Encryption Key.
6) Go to step 4 until all input data has been used. After the last block has been encrypted, go to step 7.
7) Decrypt the result from step 5 with the right half of the MAC Encryption Key.
8) Encrypt the result from step 7 with the left half of the MAC Encryption Key.
9) Use the first 4 bytes of the result from step 8 as the MAC value. Show this result as hexadecimal digits.
*/

void SecRequestMacCode(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{
	unsigned char MacKey[16];
	unsigned char MACDataLen;
	unsigned char MACData[512];
	unsigned char ResultMAC[8];
	int  curPos;
	int  loop;

	memset(MacKey,0,sizeof(MacKey));
	MACDataLen = 0;
	memset(MACData,0,sizeof(MACData));
	memset(ResultMAC,0,sizeof(ResultMAC));
	curPos = 0;
	
	memcpy (OutBuff, "72.", 3);
	OutBuff[3] = ERR_RANGE;
	*OutLen = 4;
	if (InLen <= 0 || InLen > sizeof(MACData))
	{
		return;
	}

	pStaDUKPTPara = &staDUKPTPara;

	SecMemDUKPTLoad(pStaDUKPTPara);

	*OutLen = 28;
	// step 1
	DUKPTSetBit();

	// step 2
	loop = 0;
	while (loop < 21)
	{
		if ((pStaDUKPTPara->Shift >> loop)%2 == 1)
		{
			break;
		}
		else
		{
			loop++;
		}
	}

	if (loop < 21)
	{
		pStaDUKPTPara->CurrentKeyPointer = loop;
	}

	memcpy(MacKey,pStaDUKPTPara->FutureKey[pStaDUKPTPara->CurrentKeyPointer],16);
	Xor(MacKey, DUKPT_MacKeyTmp);
	Xor(MacKey+8, DUKPT_MacKeyTmp + 8);
	memset(ResultMAC,0,sizeof(ResultMAC));
	memset(MACData,0,sizeof(MACData));
	strncpy((char *)MACData, (const char*)InBuff, InLen);
	if ((InLen % 8) == 0)
	{
		MACDataLen = InLen;
	}
	else
	{
		MACDataLen = (InLen/8 + 1)*8;
	}

	for (curPos = 0; curPos < MACDataLen; curPos += 8)
	{
		Xor(ResultMAC, MACData + curPos);	
		Des(MacKey, ResultMAC, ResultMAC,1);

	}
	Des(MacKey+8,ResultMAC,ResultMAC,0);	
	Des(MacKey,ResultMAC,ResultMAC,1);

	//translate the Left 4 byte hex MAC to 8 ascii  hex byte
	HextoA((char *)(OutBuff + 4), ResultMAC, 4);
	HextoA((char *)(OutBuff + 12), pStaDUKPTPara->KeySerialNumber+2, 8);
	OutBuff[3] = ERR_OK;

	SecMemDUKPTSave(pStaDUKPTPara);
	memset(pStaDUKPTPara,0,sizeof(DUKPT_PARAMETER));
}


/*
Power On Reset" (External Command) "
	1) Set to "one" those bits of the Encryption Counter
		that correspond to the Future Key Registers lost because 
		of the power interruption. 
	2) Increment the Encryption Counter. 
	3) Go to "Exit" 
		The following are subroutines used in the above processing routines: 
*/

void PowerOnReset(byte *InBuff, int InLen, byte *OutBuff, int *OutLen)
{
	return;
}


/*
"Set Bit" (Local Subroutine) 
	1) Set to "one" that bit in the Shift Register that corresponds 
		to the right-most "one" bit in the Encryption 
		Counter, making all other bits in the Shift Register equal zero. Example: 
		. If Encryption Counter = 0 0010 1100 1101 1100 0011,  
		Shift Register becomes: 0 0000 0000 0000 0000 0001;  
		. If Encryption Counter = 0 0010 1100 1101 1100 0000, 
		Shift Register becomes: 0 0000 0000 0000 0100 0000. 
	2) Return from subroutine to local calling routine. 
*/
void DUKPTSetBit()
{
	int loop = 0;
	
	pStaDUKPTPara->EncryptionCounter = DUKPTGetEncryptCounter();
	while (loop < 21)
	{
		if ((pStaDUKPTPara->EncryptionCounter >> loop)%2 == 1)
		{
			break;
		}
		else
		{
			loop++;
		}
	}

	if (loop < 21)
	{
		pStaDUKPTPara->Shift = (0x00000001 << loop);
	}

}


 /*
Non-reversible Key Generation Process? (Local Subroutine) 
	1) Crypto Register-1 XORed with the right half of the Key Register goes 
		to Crypto Register-2. 
	2) Crypto Register-2 DEA-encrypted using, as the key, 
		the left half of the Key Register goes to Crypto Register-2. 
	3) Crypto Register-2 XORed with the right half of the Key Register goes
		to Crypto Register-2. 
	4) XOR the Key Register with hexadecimal C0C0 C0C0 0000 0000 C0C0 C0C0 0000 0000. 
	5) Crypto Register-1 XORed with the right half of the Key Register goes 
		to Crypto Register-1. 
	6) Crypto Register-1 DEA-encrypted using, as the key, 
		the left half of the Key Register goes to Crypto Register-1. 
	7) Crypto Register-1 XORed with the right half of the Key Register goes
		to Crypto Register-1. 
	8) Return from subroutine. 
*/
int NonReverKeyGenProc(void)
{
	
	// step 1
//	Xor((char*)(&(pStaDUKPTPara->Crypto1[0])),pStaDUKPTPara->KeyRegister+8);
	memcpy(pStaDUKPTPara->Crypto2,pStaDUKPTPara->Crypto1,8);
	Xor((unsigned char*)(&(pStaDUKPTPara->Crypto2[0])),pStaDUKPTPara->KeyRegister+8);

	// step 2
	Des(pStaDUKPTPara->KeyRegister,pStaDUKPTPara->Crypto2,pStaDUKPTPara->Crypto2,1);

	// step 3
	Xor((unsigned char*)(&(pStaDUKPTPara->Crypto2[0])),pStaDUKPTPara->KeyRegister+8);

	// step 4
	Xor(pStaDUKPTPara->KeyRegister,DUKPT_tmpCode);
	Xor(pStaDUKPTPara->KeyRegister+8,DUKPT_tmpCode+8);

	// step 5
	Xor(pStaDUKPTPara->Crypto1,pStaDUKPTPara->KeyRegister+8);

	// step 6
	Des(pStaDUKPTPara->KeyRegister,pStaDUKPTPara->Crypto1,pStaDUKPTPara->Crypto1,1);

	// step 7
	Xor(pStaDUKPTPara->Crypto1,pStaDUKPTPara->KeyRegister+8);

	// step 8
	return 0;
}

/*
Triple-DEA Encrypt? (Local Subroutine) 
	1) Crypto Register-1 DEA-encrypted using the left half of the Key Register as the key goes to Crypto 
	Register-1. 
	2) Crypto Register-1 DEA-decrypted using the right half of the Key Register as the key goes to Crypto 
	Register-1. 

	3) Crypto Register-1 DEA-encrypted using the left half of the Key Register as the key goes to Crypto
	Register-1. 
	4) Return from subroutine. 

*/
void TripleDEAEncrypt()
{
	Des3(pStaDUKPTPara->KeyRegister, pStaDUKPTPara->Crypto1, pStaDUKPTPara->Crypto1, 1);

	return;
}

void TripleDEADecrypt()
{
	Des3(pStaDUKPTPara->KeyRegister, pStaDUKPTPara->Crypto1, pStaDUKPTPara->Crypto1, 0);

	return;
}

/*
Derivation Of The Initial Key
Note: References to the base derivation key in this document deal only with double-length keys.
The initial PIN Entry Device key (the key initially loaded into the PIN Entry Device) is generated by the following
process:
	1) Copy the entire key serial number, including the 21-bit encryption counter, right-justified into a 10-byte
	register. If the key serial number is less than 10 bytes, pad to the left with hex "FF" bytes.
	2) Set the 21 least-significant bits of this 10-byte register to zero.
	3) Take the 8 most-significant bytes of this 10-byte register, and encrypt/decrypt/encrypt these 8 bytes using
	the double-length derivation key, per the TECB mode of Reference 3.
	4) Use the ciphertext produced by Step 3 as the left half of the Initial Key.
	5) Take the 8 most-significant bytes from the 10-byte register of Step 2 and encrypt/decrypt/encrypt these 8
	bytes using as the key the double-length derivation key XORed with hexadecimal C0C0 C0C0 0000 0000
	C0C0 C0C0 0000 0000, per the TECB mode of Reference 3.
	6) Use the ciphertext produced by Step 5 as the right half of the Initial Key.

*/
/*
void DerivateInitialKey()
{
	memcpy(pStaDUKPTPara->Crypto1,pStaDUKPTPara->KeySerialNumber,8);
	TripleDEAEncrypt();
	memcpy(pStaDUKPTPara->Crypto2,pStaDUKPTPara->Crypto1,8);
	memcpy(pStaDUKPTPara->Crypto1,pStaDUKPTPara->KeySerialNumber,8);
	Xor(pStaDUKPTPara->KeyRegister, DUKPT_tmpData);
	Xor(pStaDUKPTPara->KeyRegister+8, DUKPT_tmpData+8);
	TripleDEAEncrypt();
	memcpy(pStaDUKPTPara->KeyRegister,pStaDUKPTPara->Crypto2,8);
	memcpy(pStaDUKPTPara->KeyRegister+8,pStaDUKPTPara->Crypto1,8);
	memcpy(pStaDUKPTPara->FutureKey[20],pStaDUKPTPara->KeyRegister,16);
	pStaDUKPTPara->FutureKey[20][16] = CalculateLRC(pStaDUKPTPara->FutureKey[20],16);

	return;
}
*/

int SecWriteAesFixedKey(byte *aesKey, byte *aesKeySerialNumber)
{
	SmemWrite(AES_FIXED_KEY_BASE, aesKey, 16);

	SmemWrite(AES_FIXED_KEY_BASE + 16, aesKeySerialNumber, 8);

	return 0;
}

int SecReadAesFixedKey(byte *aesKey, byte *aesKeySerialNumber)
{
	SmemRead(AES_FIXED_KEY_BASE, aesKey, 16);

	SmemRead(AES_FIXED_KEY_BASE + 16, aesKeySerialNumber, 8);

	return 0;
}

/*
8 byte des encrypt

return value: 0 is ok, other is failed.
*/
int ucl_desEncrypt(unsigned char * data, unsigned char * key, unsigned char * result)
{
    Des(key, data, result, SEC_ENC);
 
    return 0;
}

/*
X byte tdes encrypt/decrypt

return value: 0 is ok, other is failed.
*/
int ucl_tdes_ecb(unsigned char * data, int dataLen, 
	unsigned char * key, unsigned char * result, int encOrDec)
{
    Des3ECB(key, data, dataLen, result, encOrDec);
 
    return 0;
}

int ucl_tdes_mac(unsigned char *key, unsigned char *in, int len, unsigned char *mac)
{
	Des3Mac(key, in, len, mac);

	return 0;
}

/*
X byte aes encrypt/decrypt

return value: 0 is ok, other is failed.
*/
int ucl_aes_ecb(unsigned char * data, int dataLen, 
	unsigned char * key, unsigned char * result, int encOrDec)
{
    AesECB(key, data, dataLen, result, encOrDec);
 
    return 0;
}

int Sec_UnitTest()
{
	bool isExist;

	isExist = SmemIsKeyStoreKekExistViaBPK();
	if (isExist == TRUE)
	{
		dbg("isExist == TRUE\n");
	}else
	{
		dbg("isExist == FALSE\n");
	}
	
	return 0;
}

#ifdef OTP_DEBUG_MEMORY
#define OTP_MEMORY_SIZE	(120)

byte testOtpMemory[OTP_MEMORY_SIZE];
int SecWriteOtp(byte *data, int len, int offset)
{
	byte tmpBuf[OTP_MEMORY_SIZE];
	byte tmpFFBuf[OTP_MEMORY_SIZE];

	memcpy(tmpBuf, testOtpMemory + offset, len);

	memset(tmpFFBuf, 0xff, sizeof(tmpFFBuf));
	if (memcmp_ex(tmpBuf, tmpFFBuf, len) != 0)
	{
		dbg("Otp area already be writed, can't write again\n");
		//return -1;
	}

	memcpy(testOtpMemory + offset, data, len);
	return 0;
}

int SecReadOtp(byte *data, int len, int offset)
{
	memcpy(data, testOtpMemory + offset, len);
	return 0;
}

#elif OTP_FLASH == 1
#define OTP_FLASH_SIZE	(120)

int SecWriteOtp(byte *data, int len, int offset)
{
	lfs_file_t fp;
	int ret;
	//int writeLen;
	char szPath[128];
	byte tmpBuf[OTP_FLASH_SIZE + 1];
	byte tmpFFBuf[OTP_FLASH_SIZE + 1];
	byte tmpRead[OTP_FLASH_SIZE + 1];

	memset(tmpFFBuf, 0xff, sizeof(tmpFFBuf));
	sprintf(szPath, "%s/%s", DIR_OTP_FLASH, FILE_NAME_OTP_FLASH);

	ret = sys_lfs_file_open(&fp, szPath, LFS_O_RDWR);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}
	dbg("SecWriteOtp: Offset = %d\n", offset);
	hexdumpEx("SecWriteOtp: list write buffer", data, len);
	
	sys_lfs_file_read(&fp, tmpRead, OTP_FLASH_SIZE);

	memcpy(tmpBuf, tmpRead + offset, len);

	memset(tmpFFBuf, 0xff, sizeof(tmpFFBuf));
	hexdumpEx("SecWriteOtp: list tmpBuf", tmpBuf, len);
	if (memcmp_ex(tmpBuf, tmpFFBuf, len) != 0)
	{
		dbg("Otp area already be writed, can't write again\n");
#ifndef OTP_FLASH_TEST_ENABLE_REWRITE
		sys_lfs_file_close(&fp);
		return -1;
#else
		dbg("Otp area already be writed, enable rewrite for debug\n");
#endif
	}

	if (memcmp_ex(data, tmpFFBuf, len) == 0)
	{
		dbg("Otp area already can't write all 0xFF data\n");
		sys_lfs_file_close(&fp);
		return -2;
	}

	memcpy(tmpRead + offset, data, len);

	sys_lfs_file_rewind(&fp);
	
	sys_lfs_file_write(&fp, tmpRead, OTP_FLASH_SIZE);
	
	sys_lfs_file_close(&fp);
	
	return 0;
}

int SecReadOtp(byte *data, int len, int offset)
{
	lfs_file_t fp;
	int ret;
	//int writeLen;
	char szPath[128];
	//byte tmpBuf[OTP_FLASH_SIZE + 1];
	byte tmpRead[OTP_FLASH_SIZE + 1];
	
	sprintf(szPath, "%s/%s", DIR_OTP_FLASH, FILE_NAME_OTP_FLASH);

	ret = sys_lfs_file_open(&fp, szPath, LFS_O_RDWR);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}
	dbg("SecWriteOtp: Offset = %d\n", offset);
	
	sys_lfs_file_read(&fp, tmpRead, OTP_FLASH_SIZE);

	memcpy(data, tmpRead + offset, len);

	sys_lfs_file_close(&fp);

	return 0;
}

static int SmemInitOtpFile(void)
{
	unsigned char tmp[OTP_FLASH_SIZE];
	int ret;
	lfs_file_t fp;
	char szPath[128];

	sprintf(szPath, "%s/%s", DIR_OTP_FLASH, FILE_NAME_OTP_FLASH);
	ret = sys_lfs_file_open(&fp, szPath, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
	if (ret != 0)
	{
		err("sys_lfs_file_open return %d\n", ret);
		return -1;
	}

	memset(tmp, 0xFF, sizeof(tmp));
		
	sys_lfs_file_write(&fp, tmp, OTP_FLASH_SIZE);

	sys_lfs_file_close(&fp);
	
	return 0;
}


#else
#define TEST_OTP_ADDR   0x40008B00

int SecWriteOtp(byte *data, int len, int offset)
{
	uint32_t iArrTmp[16];
	uint32_t index = 0;
	uint32_t ErrorFlag;
	uint32_t writeAddr;
	int ret;
	byte readBuf[64];
	byte tmpFFBuf[64];

	dbg("%s\n", __func__);
	
	if (len > (16*4))
	{
		err("len err\n");
		return -1;
	}

	if (len % 4 != 0)
	{
		err("len err\n");
		return -1;
	}

	ret = SecReadOtp(readBuf, len, offset);
	if (ret != 0)
	{
		err("SecReadOtp err\n");
		return -2;
	}

	memset(tmpFFBuf, 0xff, sizeof(tmpFFBuf));
	if (memcmp_ex(readBuf, tmpFFBuf, len)!= 0)
	{
		err("readBuf is not all 0xff\n");
		return -3;
	}

	//set iArrTmp 0~X
	for (index = 0; index < (len/4); index++)
	{
		Comm_HexToInt_LittleEndian(data + index*4, &(iArrTmp[index]));
		dbg("iArrTmp[%d] = %d\n", index, iArrTmp[index]);
		writeAddr = TEST_OTP_ADDR + offset + index*4;
		
		OTP_PowerOn();
     	OTP_Unlock();
     	OTP_UnProtect(writeAddr);
		if (0 != (ErrorFlag = OTP_WriteWord(writeAddr, iArrTmp[index])))
	    {
	        dbg("write OTP error,flag is %d.\n", ErrorFlag);
	        return -1;
	    }

		OTP_Lock();
    	OTP_SetProtect(writeAddr);
	}

	return 0;
}

int SecReadOtp(byte *data, int len, int offset)
{
	uint32_t iArrTmp[16];
	uint32_t index = 0;
	uint32_t writeAddr;

	dbg("%s\n", __func__);
	
	if (len > (16*4))
	{
		err("len err\n");
		return -1;
	}

	if (len % 4 != 0)
	{
		err("len err\n");
		return -1;
	}

	for (index = 0; index < (len/4); index++)
	{
		writeAddr = TEST_OTP_ADDR + offset + index*4;
		iArrTmp[index] = *((uint32_t*)writeAddr);
		Comm_IntToHex_LittleEndian(iArrTmp[index], data + index*4);
	}

	hexdumpEx("List read OTP", data, len);

	return 0;
}

#endif

int SecWriteServerAuthKey(byte *serverAuthKey)
{
	return SecWriteOtp(serverAuthKey, OTP_TERMINAL_SERVER_AUTH_KEY_LEN, OTP_TERMINAL_SERVER_AUTH_KEY_BASE);
}

int SecReadServerAuthKey(byte *serverAuthKey)
{
	return SecReadOtp(serverAuthKey, OTP_TERMINAL_SERVER_AUTH_KEY_LEN, OTP_TERMINAL_SERVER_AUTH_KEY_BASE);
}

int SecWriteTerminalSN(byte *terminalSN)
{
	return SecWriteOtp(terminalSN, OTP_TERMINAL_SN_LEN, OTP_TERMINAL_SN_BASE);
}

int SecReadTerminalSN(byte *terminalSN)
{
	return SecReadOtp(terminalSN, OTP_TERMINAL_SN_LEN, OTP_TERMINAL_SN_BASE);
}

int SecWriteLocationMacKey(byte *locationMacKey)
{
	return SecWriteOtp(locationMacKey, OTP_TERMINAL_LOCATION_MAC_KEY_LEN, OTP_TERMINAL_LOCATION_MAC_KEY_BASE);
}

int SecReadLocationMacKey(byte *locationMacKey)
{
	return SecReadOtp(locationMacKey, OTP_TERMINAL_LOCATION_MAC_KEY_LEN, OTP_TERMINAL_LOCATION_MAC_KEY_BASE);
}

void SecOtp_UnitTest(void)
{
	byte authKey[OTP_TERMINAL_SERVER_AUTH_KEY_LEN];
	byte macKey[OTP_TERMINAL_LOCATION_MAC_KEY_LEN];
	byte terminalSN[OTP_TERMINAL_SN_LEN];
	byte loadAuthKey[OTP_TERMINAL_SERVER_AUTH_KEY_LEN];
	byte loadMacKey[OTP_TERMINAL_LOCATION_MAC_KEY_LEN];
	byte loadTerminalSN[OTP_TERMINAL_SN_LEN];

	memset(terminalSN, 0x30, sizeof(terminalSN));
	memset(macKey, 0x31, sizeof(macKey));
	memset(authKey, 0x32, sizeof(authKey));
	
	SecWriteServerAuthKey(authKey);
	SecWriteLocationMacKey(macKey);
	SecWriteTerminalSN(terminalSN);

	SecReadLocationMacKey(loadMacKey);
	SecReadServerAuthKey(loadAuthKey);
	SecReadTerminalSN(loadTerminalSN);
	hexdumpEx("List loadMacKey", loadMacKey, OTP_TERMINAL_LOCATION_MAC_KEY_LEN);
	hexdumpEx("List loadAuthKey", loadAuthKey, OTP_TERMINAL_SERVER_AUTH_KEY_LEN);
	hexdumpEx("List loadTerminalSN", loadTerminalSN, OTP_TERMINAL_SN_LEN);
}


int SecCheckKeyStatus(void)
{
	int iRet;
	PINPAD_PARA PINPad_para;

	iRet = LoadSetting(&PINPad_para);
    if (iRet != 0)
    {
    	////Line;
        dbg("LoadSetting error\n");
        return -1;
    }

	if (PINPad_para.KeyMode != KEY_MODE_3_LAYER_D_D)
	{
		////Line;
        return -2;
	}

	////Line;
	return 0;
}


#if defined(APP_LKL) ||defined(APP_CJT)   
static int SmemEraseTusnKey(void)
{
	unsigned char tmp[TUSN_KEY_SIZE];

	memset(tmp, 0, sizeof(tmp));
	
	SmemWrite(TUSN_KEY_BASE, tmp, TUSN_KEY_SIZE);

	return 0;
}
int SecEraseTusnKey(void)
{
	return SmemEraseTusnKey();
}

int SecWriteTusnKey(byte *tusnKey)
{
	byte tmpWriteData[KEY_STORAGE_LEN];

	tmpWriteData[0] = 'T';
	tmpWriteData[1] = '1';
	memcpy(tmpWriteData + 2, tusnKey, 16);

	SmemWrite(TUSN_KEY_BASE, tmpWriteData, sizeof(tmpWriteData));

	return 0;
}

int SecReadTusnKey(byte *tusnKey)
{
	byte tmpReadData[KEY_STORAGE_LEN];

	SmemRead(TUSN_KEY_BASE, tmpReadData, sizeof(tmpReadData));

	if (tmpReadData[0] != 'T' || tmpReadData[1] != '1')
	{
#ifndef RELEASE    
        {
            memset(tusnKey, 0x15, 16);
            return 0;
        }
#else        
		return -1;
#endif        
	}

	memcpy(tusnKey, tmpReadData + 2, 16);

	return 0;
}
#else
int SecEraseTusnKey(void)
{
    byte writeData[OTP_TUSN_KEY_LEN];

    memset(writeData, 0x00, sizeof(writeData));
    writeData[0] = '0';
    writeData[1] = '0';

    return SecWriteOtp(writeData, OTP_TUSN_KEY_LEN, OTP_TUSN_KEY_BASE);
}

int SecWriteTusnKey(byte *tusnKey)
{
    byte writeData[OTP_TUSN_KEY_LEN];

    writeData[0] = 'T';
    writeData[1] = '1';
    memcpy(writeData + 2, tusnKey, 16);

    return SecWriteOtp(writeData, OTP_TUSN_KEY_LEN, OTP_TUSN_KEY_BASE);
}

int SecReadTusnKey(byte *tusnKey)
{
    byte readData[OTP_TUSN_KEY_LEN];

    SecReadOtp(readData, OTP_TUSN_KEY_LEN, OTP_TUSN_KEY_BASE);

    if (readData[0]!= 'T' || readData[1] != '1')
    {
#ifndef RELEASE    
        {
            memset(tusnKey, 0x15, 16);
            return 0;
        }
#else        
		return -1;
#endif
    }

    memcpy(tusnKey, readData + 2, 16);

    return 0;
}
#endif


//////////////////////////////////////////////////////////////////////////

unsigned int mh_rand_p(void *rand, unsigned int bytes, void *p_rng);


int SecCalTusnCiphertext(char *tusn, char *randomFactor, unsigned char *tusnKey, 
		char *szTusnCiphertextOut)
{
	unsigned char tusnWithRandom[32];
	unsigned char MAB1[16], MAB2[16], MABResult[16];
	char szMABResult[32 + 1];
	unsigned char CiphertextTmp[16];
	unsigned char CiphertextFinal[16];
	char szCiphertextFinal[32 + 1];
	unsigned int ret;

	memset(tusnWithRandom, 0x00, sizeof(tusnWithRandom));
	memcpy(tusnWithRandom, tusn, 20);
	memcpy(tusnWithRandom + 20, randomFactor, 6);
	
	memcpy(MAB1, tusnWithRandom, 16);
	memcpy(MAB2, tusnWithRandom + 16, 16);
    
	Xor_ex(MAB2, MAB1, 16);
	memcpy(MABResult, MAB2, 16);
	TranslateHexToChars(MABResult, 16, szMABResult);

	ret = mh_sm4_enc(ECB, CiphertextTmp, 16, szMABResult, 16, tusnKey, NULL, mh_rand_p, NULL);
	dbg("1 mh_sm4_enc return = %d\n", ret);
	
	Xor_ex(CiphertextTmp, szMABResult + 16, 16);
	
	ret = mh_sm4_enc(ECB, CiphertextFinal, 16, CiphertextTmp, 16, tusnKey, NULL, mh_rand_p, NULL);
	dbg("2 mh_sm4_enc return = %d\n", ret);

	TranslateHexToChars(CiphertextFinal, 16, szCiphertextFinal);
	szCiphertextFinal[32] = 0x00;
	dbg("szCiphertextFinal = %s\n", szCiphertextFinal);

	memcpy(szTusnCiphertextOut, szCiphertextFinal, 8);
	szTusnCiphertextOut[8] = 0x00;

	return 0;
}

/*
void mh_SM4_test_with_known_data1(void)
{
	uint32_t t;
	uint8_t Plain[16 * 4] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
							0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
							0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,
							0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f};
	uint8_t Key[16] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10};
	uint8_t Iv[16] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10};
	uint8_t Crypt_ECB[16 * 4] = {0x6c,0x1d,0xd6,0xaf,0xca,0xa9,0x43,0x1e,0x4c,0x26,0xb3,0xcd,0xde,0x0a,0x73,0x24,0xd7,0xc6,0x4c,0x78,0x75,0x79,0x83,0xf8,0xb8,0xa2,0x92,0x19,0xf0,0x90,0x5c,0x9e,0x31,0xc2,0xe7,0xfb,0xcb,0x22,0x67,0x3f,0xd5,0xa1,0xcb,0x44,0xa7,0x4b,0x04,0x1b,0x2e,0xe8,0xdc,0xe4,0x63,0x41,0xf0,0x08,0xe6,0xfe,0x1c,0x1a,0x7b,0x14,0x2a,0x30};
	uint8_t Crypt_CBC[16 * 4] = {0xcc,0x86,0x86,0x4b,0xb7,0x9f,0x4e,0x58,0xba,0x83,0x3f,0xb4,0x87,0x21,0x39,0xfd,0x5d,0x04,0x2a,0xa3,0x27,0x29,0x72,0x28,0x76,0x72,0x56,0x60,0x2f,0x47,0x1a,0xc8,0xf9,0x38,0xab,0x9e,0x25,0x4a,0xbc,0x74,0x2c,0x68,0xda,0xd8,0x0f,0x7a,0xa5,0x30,0x5a,0x64,0x03,0xd5,0x93,0x87,0x71,0x15,0x8f,0x49,0x0d,0x52,0x80,0xb8,0x18,0xe6};

	uint8_t Crypt2[16 * 4] = {0};
	uint8_t Plain2[16 * 4] = {0};
	uint32_t ret;
	

	dbg("\n%s\n", __func__);

	//ECB
	memset(Plain2, 0, sizeof(Plain2));
	memset(Crypt2, 0, sizeof(Crypt2));
	
	ret = mh_sm4_enc(ECB, Crypt2, sizeof(Crypt2), Plain, sizeof(Plain), Key, NULL, mh_rand_p, NULL);
	dbg("mh_sm4_enc return = %d\n", ret);
	t = (!memcmp(Crypt_ECB, Crypt2, sizeof(Crypt2)));
	r_printf(t, "SM4 ECB Encrypt Test\n");

	mh_sm4_dec(ECB, Plain2, sizeof(Plain2), Crypt_ECB, sizeof(Crypt_ECB), Key, NULL, mh_rand_p, NULL);
	t = (!memcmp(Plain2, Plain, sizeof(Plain)));
	r_printf(t, "SM4 ECB Decrypt Test\n");

	
	//CBC
	memset(Plain2, 0, sizeof(Plain2));
	memset(Crypt2, 0, sizeof(Crypt2));
			
	mh_sm4_enc(CBC, Crypt2, sizeof(Crypt2), Plain, sizeof(Plain), Key, Iv, mh_rand_p, NULL);
	t = (!memcmp(Crypt_CBC, Crypt2, sizeof(Crypt2)));
	r_printf(t, "SM4 CBC Encrypt Test\n");
	
	mh_sm4_dec(CBC, Plain2, sizeof(Plain2), Crypt_CBC, sizeof(Crypt_CBC), Key, Iv, mh_rand_p, NULL);
	t = (!memcmp(Plain2, Plain, sizeof(Plain)));
	r_printf(t, "SM4 CBC Decrypt Test\n");
}

void mh_SM4_test_with_known_data2(void)
{
	uint32_t t;
	uint8_t Plain[16 * 4] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
							0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,
							0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f,
							0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x3a,0x3b,0x3c,0x3d,0x3e,0x3f};
	uint8_t Key[16] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0xab,0xa1,0xdd,0xef,0xb3,0xaf,0x49,0x15};
	uint8_t Iv[16] = {0x11,0x11,0x11,0x11,0xd1,0xa1,0x6c,0x20,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x10};
	uint8_t Crypt_ECB[16 * 4] = {0x59,0x0b,0xf4,0xfe,0xc7,0x08,0xdb,0x75,0x62,0xca,0x73,0xe3,0xc1,0x5f,0x51,0x35,0x2f,0xb4,0xf3,0x27,0xdd,0xb5,0x54,0xcd,0x75,0xb9,0x9e,0x2c,0x2e,0x45,0x85,0xee,0xc3,0xb7,0xe6,0x45,0xe3,0x94,0x34,0x74,0xbc,0x95,0x72,0x9e,0xe2,0x3b,0xb9,0x19,0xe7,0xd0,0x5b,0x0a,0x57,0x8d,0x8b,0x5b,0x9c,0x0c,0x87,0x5a,0x1a,0x7c,0xa6,0xd2};
	uint8_t Crypt_CBC[16 * 4] = {0x21,0x37,0xe3,0x20,0x95,0xcf,0x11,0x5f,0x4f,0x49,0xf6,0x84,0xfd,0xe2,0x43,0x69,0x9f,0xb5,0xb9,0x4f,0xae,0x41,0xbf,0x39,0x94,0x46,0x8e,0x81,0x0a,0x8e,0xf4,0xb2,0x0f,0xbf,0x6e,0x0c,0x4c,0x89,0x5b,0xe2,0x0c,0x86,0xb5,0x6c,0x89,0x9a,0x10,0x77,0xb9,0x61,0x67,0xb7,0x19,0x50,0x39,0xc0,0x70,0xfb,0xb7,0x33,0xb0,0x1f,0xee,0xdb};

	uint8_t Crypt2[16 * 4] = {0};
	uint8_t Plain2[16 * 4] = {0};

	dbg("\n%s\n", __func__);

	//ECB
	memset(Plain2, 0, sizeof(Plain2));
	memset(Crypt2, 0, sizeof(Crypt2));
	
	mh_sm4_enc(ECB, Crypt2, sizeof(Crypt2), Plain, sizeof(Plain), Key, NULL, mh_rand_p, NULL);
	t = (!memcmp(Crypt_ECB, Crypt2, sizeof(Crypt2)));
	r_printf(t, "SM4 ECB Encrypt Test\n");

	mh_sm4_dec(ECB, Plain2, sizeof(Plain2), Crypt_ECB, sizeof(Crypt_ECB), Key, NULL, mh_rand_p, NULL);
	t = (!memcmp(Plain2, Plain, sizeof(Plain)));
	r_printf(t, "SM4 ECB Decrypt Test\n");

	
	//CBC
	memset(Plain2, 0, sizeof(Plain2));
	memset(Crypt2, 0, sizeof(Crypt2));
			
	mh_sm4_enc(CBC, Crypt2, sizeof(Crypt2), Plain, sizeof(Plain), Key, Iv, mh_rand_p, NULL);
	t = (!memcmp(Crypt_CBC, Crypt2, sizeof(Crypt2)));
	r_printf(t, "SM4 CBC Encrypt Test\n");
	
	mh_sm4_dec(CBC, Plain2, sizeof(Plain2), Crypt_CBC, sizeof(Crypt_CBC), Key, Iv, mh_rand_p, NULL);
	t = (!memcmp(Plain2, Plain, sizeof(Plain)));
	r_printf(t, "SM4 CBC Decrypt Test\n");
}
*/

int SecEraseTransmissionKey(void)
{
	byte tmpWriteData[TRANSMISSION_KEY_SIZE];

	memset(tmpWriteData, 0x00, sizeof(tmpWriteData));

	SmemWrite(TRANSMISSION_KEY_BASE, tmpWriteData, TRANSMISSION_KEY_SIZE);

	return 0;
}

int SecWriteTransmissionKey(byte *transmissionKey)
{
	byte tmpWriteData[KEY_STORAGE_LEN];

	tmpWriteData[0] = 'T';
	tmpWriteData[1] = '1';
	memcpy(tmpWriteData + 2, transmissionKey, 16);

	SmemWrite(TRANSMISSION_KEY_BASE, tmpWriteData, sizeof(tmpWriteData));

	return 0;
}

int SecReadTransmissionKey(byte *transmissionKey)
{
	byte tmpReadData[KEY_STORAGE_LEN];

	SmemRead(TRANSMISSION_KEY_BASE, tmpReadData, sizeof(tmpReadData));

	if (tmpReadData[0] != 'T' || tmpReadData[1] != '1')
	{  
		return -1;
	}

	memcpy(transmissionKey, tmpReadData + 2, 16);

	return 0;
}


