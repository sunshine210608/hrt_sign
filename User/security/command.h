/********************************************************************************

 *************************Copyright (C), 2019, ScienTech Inc.**************************

 ********************************************************************************
 * @file     	 : command.h
 * @brief   		  : command.c header file
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-11-23
 * 
 * 
 * @note History:
 * @note        : Jay 2019-11-23
 * @note        : 
 *   Modification: Created file

********************************************************************************/

#ifndef __COMMAND_H__
#define __COMMAND_H__


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

#define ERRCODE_PARAMETER		'F'
#define ERRCODE_SUCCESS			'0'
#define ERRCODE_FAILED			'1'

void Cmd_GetVersion( unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen );
void Cmd_Internal_PINPad(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen );
void Cmd_Outside_PINPad(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen );
void Cmd_DPA_Test(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen );
void Cmd_Get_TUSN(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen );
void Cmd_Get_KeyStatus(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen );
void Cmd_Get_HardwareTestResult(unsigned char *byArrCmdData, int iCmdlen, unsigned char *byArrRspData, int *piRspLen );


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __COMMAND_H__ */
