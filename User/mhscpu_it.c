/********************************************************************************

 *************************Copyright (C), 2020, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : mhscpu_it.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2020-01-17
 * 
 * 
 * @note History:
 * @note        : Jay 2020-01-17
 * @note        : 
 *   Modification: Created file

********************************************************************************/

#include <mhscpu.h>
#include <mhscpu_kcu.h>
#include <mhscpu_msr.h>
#include <mhscpu_it.h>
#include <mhscpu_exti.h>
#include <mhscpu_timer.h>
#include <mhscpu_dcmi.h>
#include <mhscpu_ssc.h>

#include <string.h>
#include <stdio.h>
#include "SysTick.h"
#include "debug.h"
#include "sensor.h"
#include "pmu.h"
#include "touchpanel.h"
#include "battery.h"
#include "msr.h"
#include "keyboard.h"
#include "usb_dcd_int.h"
#include "buzzer.h"
#include "common.h"
#include "DecodeLib.h"
#include "ProjectConfig.h"
#include "user_projectconfig.h"
#include "serial.h"
#include "rtos_tasks.h"
#include "watchdog.h"
#include "lowpowermode.h"

#include "FreeRTOS.h"
#include "task.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/
/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
    NMIHandler();
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  HardFaultHandler();
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
    MemManageHandler();
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
    BusFaultHandler();
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
    UsageFaultHandler();
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
//void SVC_Handler(void)
//{
//}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
    DebugMonHandler();
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
//void PendSV_Handler(void)
//{
//}


/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
	SysTickHandler();
}

void MSR_IRQHandler(void)
{
    MSRIRQHandler();
}


/*
rtc alarm use for poweroff the mcu.
*/
void RTC_IRQHandler(void)
{
	RTCIRQHandler();
}
void EXTI0_IRQHandler(void)
{
    EXTI0IRQHandler();
}

void EXTI1_IRQHandler(void)
{
    EXTI1IRQHandler();
}

void EXTI2_IRQHandler(void)
{
    EXTI2IRQHandler();
}

void EXTI3_IRQHandler(void)
{
    EXTI3IRQHandler();
}

void EXTI4_IRQHandler(void)
{
    EXTI4IRQHandler();
}

void EXTI5_IRQHandler(void)
{
    EXTI5IRQHandler();
}

void SENSOR_IRQHandler(void)
{
    SENSORIRQHandler();
}

void SSC_IRQHandler(void)
{
    SSCIRQHandler();
}

void DCMI_IRQHandler(void)
{
    DCMIIRQHandler();
}

void CRYPT0_IRQHandler(void)
{ 
	CRYPT0IRQHandler();
}

#ifdef SUPPORT_8W_CAMERA
void TIM0_4_IRQHandler (void)
{
    TIM04_IRQHandler();
}
void DMA0_IRQHandler(void)
{
    DMA0IRQHandler();
}
#endif
