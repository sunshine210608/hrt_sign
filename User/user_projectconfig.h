#ifndef _USER_PROJECTCONFIG_H_
#define _USER_PROJECTCONFIG_H_

#ifdef __cplusplus
extern "C"
{
#endif

///////////////////////////////////////////////////////////////////////

//#define PROJECT_CY20        1
//#define PROJECT_CY21        0

//#if PROJECT_CY20  == 1
//#define DEVICE_MODEL    "CY20"
//#elif PROJECT_CY21  == 1
//#define PROJECT_CY20EA      1
//#define PROJECT_CY20E      1
//#define DEVICE_MODEL    "CY21"
//#endif

//#define PROJECT_CY20E_LITE
//#define PROJECT_CY20P_LITE
#define PROJECT_CY21

///////////////////////////////////////////////////////////////////////
#define USE_1903S

//#define JTAG_DEBUG

//#define NO_QRTRANS      //无扫码(无正扫)
//#define NO_WIFI         //无wifi

#define QPBOC_RETRY         //CY21临界区位置挥卡后读卡失败可重试
#define SUPPORT_8W_CAMERA   //使用8W摄像头

#define USE_LTE_4G       //使用4G通讯(不使用gprs)

#define USE_HIGH_BAUDRATE   //使用高波特率,未定义则默认使用115200
//#define HIGH_BAUDRATE   921600
#define HIGH_BAUDRATE   460800
#define LOW_BAUDRATE    115200
#define CHANGE_MCU_FREQ

//#define NO_SEC_FORDEMO

#define LCD_GC9106    //彩屏160*128
//#define ST7789        //彩屏320*240
//#define ST7571          //黑白屏128*96
//#define ST7567        //黑白屏128*64

#if defined(ST7789) || defined(LCD_GC9106)
#define USE_COLOR_RGB565
#endif

#define USE_GB2312

#ifdef USE_GB2312
	#define REMOTE_GBK
#endif

//#define FUDAN_CARD_DEMO

#ifdef USE_1903S
#define PLATFORM_MH1903S
#endif


#ifndef JTAG_DEBUG
    #define RELEASE
#else
    #define ENABLE_JTAG		1
#endif

#ifdef NO_QRTRANS
#define DISABLE_QRDECODE
#endif

#ifdef USE_LTE_4G
#define COMM_MODULE_4G
#define COMM_MODULE_4G_DEVICE_EC600S
#endif

#ifndef JTAG_DEBUG
#ifdef USE_LTE_4G
#define COMM_MODULE_SLEEPMODE_ENABLE
#endif
#endif

#define USE_RTOS                    (1)

#define USE_HAL_LIB

/*
if power up directlly when pulgin usb changer.
*/
#define POWERUPDIRECT               (1)
//#define SUBBOARD_TEST       0

//#define DISABLE_STATUSBAR


//#define TMS_DEBUG_FIXED_KEY

#define OTP_FLASH	1

#define OTP_FLASH_TEST_ENABLE_REWRITE		

//#define OTP_DEBUG_MEMORY

//#define BPK_STRESS_TEST	

//#define BARCODE_SCANNER_TEST_DEVICE

//#define WATCHDOG_ENABLE

//#define DISABLE_ALLSENSOR

//#define FIXED_LOCATION_ENABLE

//#define ONLINE_ACTIVE_NO_NEED_AGREE

//#define DISABLE_TAMPER_FLASHFLAG

//#define SYS_LFS_MACRO_ENABLE

//#define LFS_DEBUG_ENABLE

//#define MES_DEBUG

//#define DPA_TEST_MODE

#define FACTORY_APP

//#define BCTC_EMVL2_TEST

//#define BCTC_EMVL2_HOST_MODE_UART	

//#define BCTC_EMVL2_HOST_MODE_SOCKET 	

//#define DUP_TEST
//#define USB_DEBUG //没有做压力测试有问题
//#define KEY_TEST	//测试主密钥，不用手输
#ifdef __cplusplus
}
#endif

#endif
