#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "mhscpu.h"
#include "sensor.h"

#include "user_projectconfig.h"
#include "time.h"
#include "battery.h"

#include "VposFace.h"
#include "pub.h"
#ifdef APP_LKL
#include "AppGlobal_lkl.h"
#endif
#ifdef APP_SDJ
#include "AppGlobal_sdj.h"
#endif
#if 1//def APP_CJT
#include "AppGlobal_cjt.h"
#endif

#include "mh1xxx.h"
#include "lcd.h"
#include "SysTick.h"
#include "nfc.h"
#include "des_sec.h"
#include "keyboard.h"
#include "common.h"

#include "test_menu.h"
//#include "TMS.h"
#include "tcpcomm.h"
#include "func.h"
#include "lcdgui.h"
#include "ugui.h"
#include "debug.h"
#include "evntMask.h"
#include "EmvAppUi.h"
#include "gprs_at.h"
#include "protocol.h"
#include "usbvcp.h"
#include "sys_littlefs.h"
#include "printer.h"
#ifdef REMOTE_GBK
#include "gbkext.h"
#endif

extern int endlessloop(void);
extern void EnterDeepSleepMode(void);
extern int gprs_at_ModuleInfo(char* msg);
extern int gprs_at_get_ntptime(struct tm *tmDateTime);
extern int gprs_at_get_cclk(struct tm *tmDateTime);
extern uint _uiSetTime2(struct tm *tm_new);
extern void BuzzerOn(int keepms);
extern int iPosSyncTime(void);
extern int gprsInit(void);
extern void TranslateHexToChars(tti_byte *hexArray, tti_int32 hexArraySize, tti_tchar *tempBuff);
extern int nfc_open(void);
extern int nfc_close(void);
extern int wait_icc_present_event(unsigned char *status);
extern int wait_nfc_event(unsigned char *status);
extern unsigned char emv_getUID(unsigned char*uid);
extern int emv_getType(void);
extern int  readMCUUID(uchar* uid);
extern void vSetQrPreView(int flag);
int mViewATVerInfo(void *p);
int iLteCommStressTest(void *m);
int mViewPLLCLK(void *p);

int mFacTestMagCard(void *m)
{
	uchar szTrack2[100], szTrack3[150];
	uint ret1, ret2;

	_vCls();
	_vFlushKey();

	vDispCenter(1, "测试磁卡", 1);
	_vDisp(2, "请刷卡:");
	vDispCenter(_uiGetVLines(), "--按取消键可跳过--", 0);
	_uiMagReset();
	while(1)
	{
		if(_uiMagTest())
		{
			ret1=_uiMagGet(2, szTrack2);
			ret2=_uiMagGet(3, szTrack3);
			if(ret1==0 || ret2==0)
			{
				if(ret1!=0)
					szTrack2[0]=0;
				//sprintf(szTmp, "T2:%.30s", szTrack2);
				vDispVarArg(3,"T2:%.30s", szTrack2);
				if(ret2!=0)
					szTrack3[0]=0;
				vDispVarArg(4,"T3:%.30s", szTrack3);
				_vDisp(5, "刷卡成功");
				iGetKeyWithTimeout(2);
				break;
			}else
			{
				_vBuzzer();
				_vDisp(3, "刷卡失败,请重刷");
				_uiMagReset();
				iGetKeyWithTimeout(1);
				_vDisp(3, "");
			}
		}
		if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
			break;
		_vDelay(5);
	}
	_uiMagClose();
	return 0;
}

int mFacTestNFCCard(void *m)
{
	uchar szTmp[100], sOut[270];
	uint ret, len;
	uint slot=8;

	_vCls();
	_vFlushKey();

	vDispCenter(1, "测试非接卡(NFC)", 1);
	_vDisp(2, "请挥卡:");
	vDispCenter(_uiGetVLines(), "--按取消键可跳过--", 0);
	while(1)
	{
		ret=_uiTestCard(slot);
		if(ret==9)
		{
			_vBuzzer();
			_vDisp(3, "检测到多张卡,请重试");
			iGetKeyWithTimeout(1);
			_vDisp(3, "");
			continue;
		}else if(ret==2)
		{
			_vDisp(3, "检测到非接卡");
			ret=_uiResetCard(slot, szTmp);
			if(ret>0)
			{
				_vDisp(4, "非接卡复位成功");
				len=0;
				memcpy(szTmp, "\x00\xA4\x04\x00\x02\x3F\x00", 7);
				ret=_uiDoApdu(slot, 7, szTmp, &len, sOut, 0);
				if(ret)
				{
					_vBuzzer();
					_vDisp(5, "IC卡指令失败,请重试");
					iGetKeyWithTimeout(3);
					_vDisp(3, "");
					_vDisp(4, "");
					_vDisp(5, "");
				}else
				{
					dbgHex("apdu rsp:", sOut, len);
					_vDisp(5, "IC卡指令处理成功");
					iGetKeyWithTimeout(2);
					break;
				}
			}else{
				_vBuzzer();
				_vDisp(4, "非接卡复位失败,请重试");
				iGetKeyWithTimeout(3);
				_vDisp(3, "");
				_vDisp(4, "");
			}
		}
		if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
			break;
		_vDelay(5);
	}
	_uiCloseCard(slot);
	return 0;
}

int mFacTestICCard(void *m)
{
	uchar szTmp[100], sOut[270];
	uint ret, len;
	uint slot=0;

	_vCls();
	_vFlushKey();

	vDispCenter(1, "测试接触式IC", 1);
	_vDisp(2, "请插卡:");
	vDispCenter(_uiGetVLines(), "--按取消键可跳过--", 0);
	while(1)
	{
		ret=_uiTestCard(slot);
		if(ret==1)
		{
			_vDisp(3, "检测到卡插入");
			ret=_uiResetCard(slot, szTmp);
			if(ret>0)
			{
				_vDisp(4, "IC卡复位成功");
				len=0;
				memcpy(szTmp, "\x00\xA4\x04\x00\x02\x3F\x00", 7);
				ret=_uiDoApdu(slot, 7, szTmp, &len, sOut, 0);
				if(ret)
				{
					_vBuzzer();
					_vDisp(5, "IC卡指令失败,请重试");
					iGetKeyWithTimeout(3);
					_vDisp(3, "");
					_vDisp(4, "");
					_vDisp(5, "");
				}else
				{
					dbgHex("apdu rsp:", sOut, len);
					_vDisp(5, "IC卡指令处理成功");
					iGetKeyWithTimeout(2);
					break;
				}
			}else{
				_vBuzzer();
				_vDisp(4, "IC卡复位失败,请重试");
				iGetKeyWithTimeout(3);
				_vDisp(3, "");
				_vDisp(4, "");
			}
		}
		if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
			break;
		_vDelay(5);
	}
	_uiCloseCard(slot);
	return 0;
}

int mFacTestCard(void *m)
{
	mFacTestMagCard(m);
	mFacTestNFCCard(m);
	mFacTestICCard(m);
	return 0;
}

int mFacTestKeyboard(void *m)
{
	ushort uiKey, uiLastKey=0;
	char szBuf[30];

	_vCls();
	vDispCenter(1, "按键测试", 1);
	vDispCenter(2, "请按按键!", 0);
	vDispCenter(_uiGetVLines(), "连续按[取消]退出", 0);
    _vFlushKey();
    
	while(1)
	{
#if 0        
		uiKey=_uiGetKey();
		if(uiKey==_KEY_ESC && uiLastKey==_KEY_ESC)
			break;

		memset(szBuf, 0, sizeof(szBuf));
		strcpy(szBuf, "按键值为: ");
		switch (uiKey)
		{
		case _KEY_0:
		case _KEY_1:
		case _KEY_2:
		case _KEY_3:
		case _KEY_4:
		case _KEY_5:
		case _KEY_6:
		case _KEY_7:
		case _KEY_8:
		case _KEY_9:
			szBuf[strlen(szBuf)]=uiKey-_KEY_0+'0';
			break;
		case _KEY_FN:
			strcat(szBuf, "功能键");
			break;
		case _KEY_ESC:
			strcat(szBuf, "取消键");
			break;
		case _KEY_ENTER:
			strcat(szBuf, "确认键");
			break;
		case _KEY_BKSP:
			strcat(szBuf, "清除键");
			break;
		case _KEY_UP:
			strcat(szBuf, "上箭头");
			break;
		case _KEY_DOWN:
			strcat(szBuf, "下箭头");
			break;
		case _KEY_OFF:
			strcat(szBuf, "电源键");
			break;
		default:
			sprintf(szBuf, "其它键:%02X", uiKey);
			break;
		}
		_vDisp(4, szBuf);
		uiLastKey=uiKey;
#else
        if(KeyBoardRead(&uiKey)==0 && uiKey<=0xFF)
        {
            if(uiKey==KEY_CANCEL && uiLastKey==KEY_CANCEL)
                break;

            //memset(szBuf, 0, sizeof(szBuf));
            strcpy(szBuf, "按键值为: ");
            switch (uiKey)
            {
            case KEY_0:
                strcat(szBuf, "0");
                break;
            case KEY_1:
                strcat(szBuf, "1");
                break;
            case KEY_2:
                strcat(szBuf, "2");
                break;
            case KEY_3:
                strcat(szBuf, "3");
                break;
            case KEY_4:
                strcat(szBuf, "4");
                break;
            case KEY_5:
                strcat(szBuf, "5");
                break;
            case KEY_6:
                strcat(szBuf, "6");
                break;
            case KEY_7:
                strcat(szBuf, "7");
                break;
            case KEY_8:
                strcat(szBuf, "8");
                break;
            case KEY_9:
                strcat(szBuf, "9");
                break;
            case KEY_L:
                strcat(szBuf, "功能键");
                break;
            case KEY_CANCEL:
                strcat(szBuf, "取消键");
                break;
            case KEY_OK:
                strcat(szBuf, "确认键");
                break;
            case KEY_CLR:
                strcat(szBuf, "清除键");
                break;
            case KEY_F1:
                strcat(szBuf, "上箭头");
                break;
            case KEY_F2:
                strcat(szBuf, "下箭头");
                break;
            case KEY_POWER:
                strcat(szBuf, "电源键");
                break;
            default:
                sprintf(szBuf, "其它键:0x%02X", uiKey);
                break;
            }
            _vDisp(4, (uchar*)szBuf);
            uiLastKey=uiKey;
        }
#endif
	}
    return 0;
}

int mFacTpAdjust(void *m)
{
    extern int touchpanel_adjust(void);
    
	touchpanel_adjust();
	return 0;
}

extern uchar g_ucGetTSNFlag;
int mFacLoadTSN(void *m)
{
#ifndef OTP_FLASH_TEST_ENABLE_REWRITE    
	if(_uiGetSerialNo(szBuf)==0 && szBuf[0])
	{
		_vCls();
		_vDisp(2, "POS序列号:");
		_vDisp(3, szBuf);
		iGetKeyWithTimeout(5);
		return 0;
	}
#endif
#ifdef APP_SDJ    
    g_ucGetTSNFlag=1;
#endif    
	return endlessloop();
}

int mFacTestPrinter(void *p)
{
    uchar gbk1[30];
    uchar gbk2[30];
    int gbkFlag=0;

#ifndef USE_GB2312
    gbkFlag=1;
#endif
#ifdef REMOTE_GBK
    strcpy((char*)gbk1, "啰");
    if(iGetGBKFontDat(gbk1, NULL, NULL)==0)
        gbkFlag=1;
#endif
    
    if(gbkFlag==0)
    {
        strcpy((char*)gbk1, "...");
        strcpy((char*)gbk2, "...");
    }else
    {
        //改为常用字中的GBK汉字
        strcpy((char*)gbk1, "啰瞭墈峣嵎惇獴玕玠玦");
        strcpy((char*)gbk2, "疍眬祼窸箓袪萩赟鹮昇");
    }
    
#ifndef ENABLE_PRINTER
    _vCls();
	vDispCenter(1, "打印机测试", 1);
    _vDisp(2, "春眠不觉晓,处处闻啼鸟.");
    _vDisp(3, gbk1);
    _vDisp(4, gbk2);
    vMessageLine(_uiGetVLines(), "不支持打印");
    return 0;
#else    
	int ret;  
	uchar szBuf[100];
    
	_vCls();
	vDispCenter(1, "打印机测试", 1);

    _vDisp(2, "abcdefghijklmnopqrstuvwxyz12345,");
    _vDisp(3, "ABCDEFGHIJKLMNOPQRSTUVWXYZ67890.");
    _vDisp(4, "春眠不觉晓,处处闻啼鸟.");   
    _vDisp(5, gbk1);
    _vDisp(6, gbk2);

    ret=_uiPrintOpen();
	if(ret)
	{
		return 1;
	}
 	
	_uiPrintSetFont(1);
	_uiPrintEx(2, "打印测试单", 0);
	_uiPrintSetFont(2);

	strcpy(szBuf, "abcdefghijklmnopqrstuvwxyz12345,");
	_uiPrint(szBuf, 0);
	strcpy(szBuf, "ABCDEFGHIJKLMNOPQRSTUVWXYZ67890.");
	_uiPrint(szBuf, 0);
    
    _uiPrintSetFont(3);
    strcpy(szBuf, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcd12345678901234567890");
	_uiPrint(szBuf, 0);
    _uiPrintSetFont(2);
	
    _uiPrintSetFont(1);
    strcpy(szBuf, "ABCDEF1234567890/:(M)");
	_uiPrint(szBuf, 0);
    _uiPrintSetFont(2);
    
	strcpy(szBuf, "春眠不觉晓,处处闻啼鸟.");
	_uiPrint(szBuf, 0);

	strcpy(szBuf, "两个黄鹂鸣翠柳，一行白鹭上青天。");
	_uiPrint(szBuf, 0);

    _uiPrint(gbk1, 0);
    _uiPrint(gbk2, 0);
 
	memset(szBuf, '-', _uiGetPCols());
	szBuf[_uiGetPCols()]=0;
	memcpy(szBuf+(_uiGetPCols()-3)/2, "End", 3);
    //memcpy(szBuf+2, "\x8D\x69", 2);
    //memcpy(szBuf+8, "峣峥文化", 8);
	_uiPrint(szBuf, 0);
    
    //_uiPrintSetFont(3);
    sprintf(szBuf, "%s %s %s", _pszGetModelName(NULL), _pszGetAppVer(NULL), _pszGetInternalVer(NULL));
	_uiPrint(szBuf, 0);
    //_uiPrintSetFont(2);

	_uiPrint("", 0);
	_uiPrint("", 0);
	_uiPrint("", 0);
	_uiPrint("", 1);
	_uiPrintClose();
    
    vClearLines(2);
	vDispCenter(3, "打印结束!", 0);
	iGetKeyWithTimeout(2);
    return 0;
#endif    
}

int mFacTestGprs(void *m)
{
	extern int iGprsFacTest(char *pszIMEI, char *pszICCID);
	char szICCID[30], szIMEI[30];
	int ret=0;
    char szModuleInfo[200];
    int line;
    char *p0, *p;
	
	_vCls();
	vDispCenter(1, "sim卡模块检测", 1);
    
    line=2;
#ifdef USE_LTE_4G
    szModuleInfo[0]=0;
    ret=gprs_at_ModuleInfo(szModuleInfo);
    if(ret==0)
    {
        /*
        for(ret=0; ret<20; ret++)
        {
            if(ret==10)
            {
                _vDisp(2, szICCID);                
            }
            sprintf(szICCID+ret%10*2, "%02X", szModuleInfo[ret]);
        }
        _vDisp(3, szICCID); 
        _uiGetKey();
        */
        p0=szModuleInfo;
        //跳过初始的回车换行
        for(;*p0==0x0D || *p0==0x0A || *p0==' '; p0++)
        {
        }
        p=strchr(p0, 0x0D);
        if(p)
        {
            //厂商后面的回车换行替换为空格,将厂商和型号变成一行
            for(;*p==0x0D || *p==0x0A || *p==' '; p++)
            {
                *p=' ';
            }
            p=strchr(p0, '\r');
            if(p)
            {
                *p=0;
                p++;
                _vDisp(line++, p0);   //厂商 型号
                
                p0=strstr(p, "Revision: ");
                if(p0)
                    p=p0+strlen("Revision: ");
                p0=strstr(p, "Version: ");
                if(p0)
                    p=p0+strlen("Version: ");
                
                {
                    for(;*p==0x0D || *p==0x0A || *p==' '; p++)
                    {
                    }
                }
                p0=strchr(p, '\r');
                if(p0)
                    *p0=0;
                _vDisp(line++, p);              //版本信息
            }
        }
    }
#endif    
	ret=iGprsFacTest(szIMEI, szICCID);
	if(ret<0)
	{
		_vBuzzer();
		if(ret==-99){
			vDispCenter(line++, "模块上电失败", 0);
		}else{
			vDispCenter(line++, "模块上电成功", 0);
			vDispVarArg(line++, "模块获取信息失败:%d", ret);
		}
	}else
	{
		vDispCenter(line++, "模块上电成功", 0);
		vDispVarArg(line++, "IMEI:%s", szIMEI);
        if(strlen(szICCID)+6<=_uiGetVCols())
            vDispVarArg(line++, "ICCID:%s", szICCID);
        else
            vDispVarArg(line++, "%s", szICCID);
		if(ret>0)
			vDispCenter(line++, "sim卡不存在或不正常", 0);
		else
			vDispCenter(line++, "sim卡正常", 0);
		//vDispCenter(6,"gprs模块检测成功",0);
	}
    if(gl_SysInfo.iCommType!=VPOSCOMM_GPRS)
		iCommLogout(VPOSCOMM_GPRS, 1);
	iGetKeyWithTimeout(5);
	return 0;
}

int mFacTestWifi(void *m)
{
#ifndef NO_WIFI    
	extern int iWifiFacTest(char *pszWifiVer, int iOutSize);
	char szWifiVer[300];
	int ret=0;
	char *p, *p0;
	int i;
	
	_vCls();
	vDispCenter(1, "wifi模块检测", 1);
  
	ret=iWifiFacTest(szWifiVer, sizeof(szWifiVer));
	if(ret)
	{
		_vBuzzer();
		if(ret==1){
			vDispCenter(2, "wifi模块上电失败", 0);
		}else{
			vDispCenter(2, "wifi模块上电成功", 0);
			vDispCenter(3, "wifi模块获取版本失败", 0);
		}
	}else
	{
		vDispCenter(2, "wifi模块上电成功", 0);
		p0=strstr(szWifiVer, "AT version");
        if(p0==NULL)
            p0=szWifiVer;
		for(i=0; i<3 && p0; i++)
		{
			p=strchr(p0, 0x0D);
			if(p==NULL)
				break;
			*p=0;
			_vDisp(3+i, p0);
			p0=p+1;
			if(*p0==0x0A)
				p0++;
		}
		vDispCenter(6,"Wifi模块检测成功",0);
	}
    //_uiGetKey();
#else
    _vCls();
    vDispCenter(1, "wifi模块检测", 1);
    _vDisp(3, "不支持WIFI");
#endif
	//if(gl_SysInfo.iCommType!=VPOSCOMM_WIFI)
	//	iCommLogout(VPOSCOMM_WIFI, 1);
	iGetKeyWithTimeout(3);
	return 0;
}

int mFacTestQrCode(void *m)
{
	extern int iGetQrCode(char *pszPrompt, char *pszQrCode, int size);
	int ret;
	char szQrCode[300];
	int i;
	int linesize;

	_vCls();
#ifndef  NO_QRTRANS
    if(iPosHardwareModule(CHK_HAVE_CAM, NULL, 0)<=0)
    {
        _vDisp(3, "不支持扫码");
        iGetKeyWithTimeout(5);
        return 0;
    }
	vDispCenter(1, "扫码测试", 1);
	vSetQrPreView(1);
	ret = iGetQrCode(NULL, szQrCode, sizeof(szQrCode));
	_vCls();
	vDispCenter(1, "扫码测试", 1);
	if(ret==0)
	{
		_vDisp(2, "扫码成功:");
		linesize=_uiGetVCols();
		for(i=3; i<linesize; i++)
		{
			if((i-3)*linesize>=strlen(szQrCode))
				break;
			_vDisp(i, szQrCode+(i-3)*linesize);
		}
	}else
	{
		_vDisp(2, "扫码失败");
		_vBuzzer();
	}
#else
    _vDisp(3, "不支持扫码");
#endif
	iGetKeyWithTimeout(5);
	return 0;
}

int iGet4GModuleVer(char *pszModuleVar)
{
    int ret;
    char info[200]={0};
    char *p, *p0;
    
    pszModuleVar[0]=0;
    ret=gprs_at_ModuleInfo(info);
    if(ret)
        return ret;
    
    p0=info;
    //跳过回车换行或空格
    for(;*p0==0x0D || *p0==0x0A || *p0==' '; p0++)
    {
    }
    p=strchr(p0, 0x0D);         //第一行厂商,p指向行尾
    if(p==NULL)
        return 1;
    
    p0=p+1;
    //跳过回车换行或空格
    for(;*p0==0x0D || *p0==0x0A || *p0==' '; p0++)
    {
    }
    p=strchr(p0, 0x0D);         //第二行型号,p指向行尾
    if(p==NULL)
        return 1;
    
    p0=p+1;
    //跳过回车换行或空格
    for(;*p0==0x0D || *p0==0x0A || *p0==' '; p0++)
    {
    }
    p=strchr(p0, 0x0D);         //第三行版本,p指向行尾
    if(p)
        *p=0;
    p=strstr(p0, "Revision: ");
    if(p)
    {
        p0=p+strlen("Revision: ");
    }
    strcpy(pszModuleVar, p0);
    return 0;
}

extern int LoadTMK(uchar *tmk);
extern int readMainBoardSN(char *sn);
extern int checkIfFullTestPass(void);
extern void readBurnInTestResult(uint *curmins, uint *ttmins);
extern int iDispTransQrCode(uchar *pszPrompt, char *pszQrCodeStr);
//二维码格式(各项依次串接,英文分号分隔): 整机完成(0/1);煲机分钟数;整机SN;cpuid;ICCID;IMEI;软件版本&内部版本;主密钥存在(0/1);TUSNKEY存在(0/1);4G模块版本;电压;
//例: 1;6789;00007302120001000253;123456789012345;12345678901234567890;V2020101601_SDJ
void vDispTermQrCode(int iFlag)
{
    char szBuf[200]={0};
    //char mbsn[40]={0};
    uint curm, ttm;
    char tmp[32+1];
    int ret;
    
    _vCls();
    _vFlushKey();
    
    //整机完成标志
    szBuf[0]=checkIfFullTestPass()+'0';
    szBuf[1]=';';
    
    //煲机时间
    readBurnInTestResult(&curm, &ttm);
    sprintf(szBuf+2, "%u;", ttm/60);
    
    //POS序号
    _uiGetSerialNo((uchar*)szBuf+strlen(szBuf));
    strcat(szBuf, ";");
    /*
    //主板序号
    readMainBoardSN(tmp);
    if(_uiGetVCols()>=7+strlen(tmp))
    {
        sprintf(mbsn, "主板SN:%s", tmp);
        strcat(szBuf, mbsn+7);
    }else
    {
        sprintf(mbsn, "主板:%s", tmp);
        strcat(szBuf, mbsn+5);
    }
    strcat(szBuf, ";");
    */
    //主板序号更换为cpuid
    ret=readMCUUID((uchar*)tmp);
    if(ret==0)
    {
        vOneTwo0((uchar*)tmp, 16, (uchar*)(szBuf+strlen(szBuf)));
    }
    strcat(szBuf, ";");
    
    //ICCID
    iGetICCID(szBuf+strlen(szBuf));
    strcat(szBuf, ";");
    
    //IMEI
    iGetIMEI(szBuf+strlen(szBuf));
    strcat(szBuf, ";");
    
    //软件版本&内部版本
    _pszGetAppVer(szBuf+strlen(szBuf));   
    strcat(szBuf, "&");
    _pszGetInternalVer(szBuf+strlen(szBuf));
    strcat(szBuf, ";");
        
    //tmk是否正常
    if(LoadTMK((uchar*)tmp)!=0)
        strcat(szBuf, "0");
    else
        strcat(szBuf, "1");
    strcat(szBuf, ";");
    
    //tusnkey是否正常
    if(SecReadTusnKey((uchar*)tmp)!=0)
        strcat(szBuf, "0");
    else
        strcat(szBuf, "1");
    strcat(szBuf, ";");
    
    //4G模块版本
    iGet4GModuleVer(tmp);
    strcat(szBuf, tmp);
    strcat(szBuf, ";");
    
    //电量
    sprintf(szBuf+strlen(szBuf), "%d;", battery_get_voltage());

    //if(iFlag!=1)
    //    mbsn[0]=0;
    
    _vFlushKey();
    _vSetBackLight(1);      //高亮模式,外部退出时切换为普通模式
    iDispTransQrCode("", szBuf);
    
    if(iFlag!=2)
    {
        //iGetKeyWithTimeout(15*60);
        iOK(15*60);
    }
    _vSetBackLight(0);
    return;
}
int mViewTermInfo2(void *p)
{
    //return mViewTermInfo(2);
    vDispTermQrCode(0);
    return 0;
}

int mSyncPosTime(void *p)
{
    struct tm rtcTm;
    int ret;
    ulong t0;
    int mode=1;
    
    _vCls();
    vDispCenter(1, "同步时间", 1);
    
#ifdef USE_LTE_4G    
    t0=_ulGetTickTime();
    ret=gprs_at_get_ntptime(&rtcTm);
#else
    ret=1;
#endif     
    if(ret)
    {
        mode=2;
        t0=_ulGetTickTime();
        ret=gprs_at_get_cclk(&rtcTm);
    }
   
    if(ret==0)
    {
        t0=_ulGetTickTime()-t0;
        if(rtcTm.tm_year>120) //时间需大于2020年
        {
            _uiSetTime2(&rtcTm);
            vDispVarArg(_uiGetVLines()-1, "mode=%d,tick:%lu", mode,t0);
            vMessage("同步时间成功");
        }else
        {
            vMessage("时间不正确");
        }
    }else
        vMessage("同步时间失败");
    return 0;
}

int mSetPosTime(void *p)
{
    uchar buf[30];
    int ret;
    
    _vCls();
    vDispCenter(1, "设置时间", 1);
    _vDisp(2, "请输入日期时间:");
    vDispCenter(_uiGetVLines(), "格式:YYYYMMDDhhmmss", 0);
    _vDisp(3, "  [20            ]");
    _vGetTime(buf);
    memcpy(buf, "20", 2);
    ret=iInput(INPUT_NORMAL|INPUT_INITIAL, 3, 5, buf+2, 12, 12, 12, 60);
    if(ret<=0)
        return 0;
    if(_uiSetTime(buf)==0)
		vMessage("设置时间成功");
	else
		vMessage("设置时间失败");
    return 0;
}

int iTestMaxMalloc(void *m)
{
    unsigned int alloc_size[] = { 32 * 1024, 1024, 32};
    int i,count;
    unsigned int maximum = 0;
    char buf[50];

    vDispCenter(1, "可用内存", 1);
    
    
    _vDisp(2, "检测内存中,请稍候...");
    vClearLines(3);
    for( i = 0; i < 3; i++ )
    {
        for( count = 1; ; count++)
        {
            void * block = malloc( maximum + alloc_size[i] * count);
            if(block)
            {
                maximum += alloc_size[i] * count;
                free(block);
            }
            else
            {
                break;
            }
        }
    }
    //printf("maximum alloc size = %u bytes \n", maximum);
    //vDispVarArg(_uiGetVLines()-1, "heap:%d, miniHeap:%d", xPortGetFreeHeapSize(), xPortGetMinimumEverFreeHeapSize());
    sprintf(buf, "可用内存:%u", maximum);
    vMessageEx(buf, 15*100);
    return 0;
}
#ifdef REMOTE_GBK
extern int iTestGetGBK(void *m);
int iViewGBKInfo(void *p)
{
    int num, col;
    int i;
    uchar hz[300+1]={0};
    uchar linebuf[30]={0};

    _vCls();    
    num=iGetGBKNum();
    vDispMidVarArg(1, "GBK字数:%d", num);
    
    for(i=0; i<num && i<sizeof(hz)/2; i++)
    {
        if(iGetGBKFontIdxDat(i, hz+i*2, NULL, NULL)!=0)
        {
            vMessageVarArg("get gbk %d fail.", i);
            break;
        }
    }
    col=_uiGetVCols()/2*2;
    for(i=0; i+2<=_uiGetVLines() && i<(num*2+col-1)/col; i++)
    {
        memcpy(linebuf, hz+col*i, col);
        _vDisp(i+2, linebuf);
    }
    
    iOK(10);
    return 0;
}

int iClearGBKFile(void *p)
{
    _vCls();
    iClearGBK();
    vMessage("清除成功");
    return 0;
}

SMenu menu_FacGBKMemu =
{"GBK", 0, 1, 1, 
	{		 
		{"测试GBK", iTestGetGBK, NULL},
		{"查看GBK", iViewGBKInfo, NULL},
        {"清除GBK", iClearGBKFile},         
		NULL
	}
};
#endif

extern int iTestGetGBK(void *m);
extern int enterdownloadmode(void *m);
extern int mInitAllParam(void *m);
int iCalUseSpace(void *p);
int iPrintParam(void *p);

int iTestDispWaitQrImg(void *p)
{
    extern void vDispWaitQrImg(int init);
    
    int init=1;
    while(1)
    {
        vDispWaitQrImg(init);
        init=0;
        if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
            break;
        _vDelay(10);
    }
    return 0;
}
#ifdef TEST_4G_SLEEP
int iTest4GSleep(void *p)
{
    extern void vUserSleep(void);

    _vCls();
    vUserSleep();
    vMessage("call vUserSleep end.");
    
    return 0;
}
int iTest4GWakeUp(void *p)
{
    extern void vUserWakeUp(void);    

    _vCls();
    vUserWakeUp();
    vMessage("call vUserWakeUp end.");
    
    return 0;    
}
#endif
int iOpenUsbDebug(void *m)
{
    int ret;
    char buf[20];
    
    _vCls();
    vDispMid(2, "请输入密码");
    ret=iInput(INPUT_PIN|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, (_uiGetVCols()-8)/2, buf, 8, 2, 8, 30);
    if(ret<=0)
    {
        return 1;
    }            
    if(strcmp(buf, "0801"))
    {
        vMessageLine(-1, "密码错误");
        return 1;
    }
	
    _vCls();
    vSetUsbDebug(1);
    vMessage("UsbDebug Opened.");
    return 1;
}

int iViewPosTypeInfo(void *m)
{
    int ret;
    lfs_file_t file;
    char buf[100];
    uchar tmp[110];
    
    _vCls();
    vDispCenter(1, "型号信息", 1);
    
    memset(buf, 0, sizeof(buf));
    _pszGetModelName(buf);
    vDispVarArg(2, "型号:%s", buf);
    
    memset(buf, 0, sizeof(buf));
    ret = sys_lfs_file_open(&file, "ProductCode", LFS_O_RDONLY);
    if(ret==0)
    {
        ret=sys_lfs_file_read(&file, buf, sizeof(buf)-1);
        sys_lfs_file_close(&file);
    }
    sprintf((char*)tmp, "产品代码:%s", buf);
    _vDisp(3, tmp);
    if(strlen((char*)tmp)>_uiGetVCols())
        _vDisp(4, tmp+_uiGetVCols());
    if(strlen((char*)tmp)>2*_uiGetVCols())
        _vDisp(5, tmp+2*_uiGetVCols());
    vDispVarArg(_uiGetVLines(), "%s %s", __DATE__, __TIME__);
	
    iOK(15);
    return 0;
}

extern int iTestPrintQrCode(void *p);
extern int iAiLianOTAUp(void *p);
extern int iAiLianOTAHttpUp(void *m);
SMenu menu_FacOtherMemu =
{"其它", 0, 1, 1, 
	{		 
		{"同步时间", mSyncPosTime, NULL},
		{"设置时间", mSetPosTime, NULL},
		{"型  号", iViewPosTypeInfo, 0}, 
        {"初始化POS", mInitAllParam}, 
#ifdef USE_1903S
        {"下载模式", enterdownloadmode, NULL},
#endif
        {"空间", iCalUseSpace, 0},
        {"内存", iTestMaxMalloc, 0},
#ifdef ENABLE_PRINTER		
        {"打印参数", iPrintParam, 0},
#endif
	{"UsbDebug", iOpenUsbDebug},
#ifdef JTAG_DEBUG        
        {"Ai4G OTAUp", iAiLianOTAUp, 0},
        {"Ai4G HttpUp", iAiLianOTAHttpUp, 0},
#endif        
#ifdef REMOTE_GBK
        {"GBK", 0, &menu_FacGBKMemu},
#endif
#ifdef TEST_4G_SLEEP        
        {"4G休眠", iTest4GSleep},
        {"4G唤醒", iTest4GWakeUp},
#endif
//#ifdef JTAG_DEBUG
        //{"打印Qr", iTestPrintQrCode},
        //{"扫码动画", iTestDispWaitQrImg},
//#endif
	{"4G通讯压测", iLteCommStressTest},
	{"4G模块AT版本", mViewATVerInfo}, 
	{"芯片主频", mViewPLLCLK},
		NULL
	}
};

int mViewPLLCLK(void *p)
{
	SYSCTRL_PLL_TypeDef iCPUCLK;

	_vCls();
	vDispCenter(1, "芯片主频", 1);
	
	iCPUCLK = HALGetPLLCLK();
	dbg("HALGetPLLCLK():%02x\n",iCPUCLK);

	if(iCPUCLK == SYSCTRL_PLL_108MHz)
		_vDisp(2, "主频:108M");
	else if(iCPUCLK ==SYSCTRL_PLL_120MHz)
		_vDisp(2, "主频:120M");
	else if(iCPUCLK ==SYSCTRL_PLL_132MHz)	
		_vDisp(2, "主频:132M");
	else if(iCPUCLK ==SYSCTRL_PLL_144MHz)	
		_vDisp(2, "主频:144M");
	else if(iCPUCLK ==SYSCTRL_PLL_156MHz)	
		_vDisp(2, "主频:156M");
	else if(iCPUCLK ==SYSCTRL_PLL_168MHz)	
		_vDisp(2, "主频:168M");
	else if(iCPUCLK ==SYSCTRL_PLL_180MHz)	
		_vDisp(2, "主频:180M");
	else if(iCPUCLK ==SYSCTRL_PLL_192MHz)
		_vDisp(2, "主频:192M");
	else if(iCPUCLK ==SYSCTRL_PLL_204MHz)
		_vDisp(2, "主频:204M");

	iOK(15);
	return 0;				
}

int iViewATVerInfo(void)
{
	char szBuf[50];
	ulong ultimer;
	uint uiKey;
	char bCheck;

	_vCls();
	vDispCenter(1, "4G模块AT版本", 1);

	pszGet4GVer(szBuf);
	vDispVarArg(2, "AT版本:%s", szBuf);

	iOK(15);
	return 0;
    }

int mViewATVerInfo(void *p)
{
	return iViewATVerInfo();	
 }

extern int testtp_drawloop(void *m);

SMenu menu_FacTPMemu =
//{"触摸屏", 0, 1, 1, 
	{"手写屏", 0, 1, 1, 
	{		 
		{"屏幕校准", mFacTpAdjust, NULL},
		{"屏幕签名", testtp_drawloop, NULL},
		NULL
	}
};

//工厂菜单
SMenu menu_FactoryMemu =
{"工厂模式", 0, 1, 2, 
	{
		{"银行卡", mFacTestCard, NULL}, 
		{"键盘", mFacTestKeyboard, NULL},
		{"4G/GPRS", mFacTestGprs, NULL}, 
		{"wifi模块", mFacTestWifi, NULL}, 
		{"打印机", mFacTestPrinter, NULL}, 
		{"扫码", mFacTestQrCode, NULL}, 		 
		{"手写屏", 0, &menu_FacTPMemu},
        {"其它", 0, &menu_FacOtherMemu},
		{"终端序号", mFacLoadTSN, NULL},
        {"二维码", mViewTermInfo2, NULL},
		NULL
	}
};

int iFactorySimpleTest(void)
{
	EnterTestMenu(&menu_FactoryMemu);
	return 0;
}

int mHardwareTestMagCard(void *m)
{
	uchar szTrack2[100], szTrack3[150];
	uchar szTmp[100];
	uint ret1, ret2;
	uint totalTimes;
	uint successTimes;
    uint uiKey;
    char cUpdateFlag=1;
    ulong ulTimer=0;
    ulong ulTimerOut;

	_vCls();
	_vFlushKey();
    
    _vSetTimer(&ulTimerOut, 10*60*100);     //10分钟
	_uiMagReset();
	totalTimes = 0;
	successTimes = 0;
	while(!_uiTestTimer(ulTimerOut))
	{
        if(cUpdateFlag)
        {
            vDispCenter(1, "测试磁卡", 1);
            if(ulTimer==0)
                _vDisp(2, "请刷卡!");
            
            vDispVarArg(4, "刷卡成功率:%d/%d", successTimes, totalTimes);
            vDispCenter(_uiGetVLines(), "\"取消\"退出,\"确认\"重测", 0);
            cUpdateFlag=0;
        }
        if(ulTimer && _uiTestTimer(ulTimer))
        {
            _vDisp(2, "请刷卡!");
            _vDisp(3, "");
            ulTimer=0;
        }
		if(_uiMagTest())
		{
            cUpdateFlag=1;
            
			totalTimes++;
			ret1=_uiMagGet(2, szTrack2);
			ret2=_uiMagGet(3, szTrack3);
			if(ret1==0 || ret2==0)
			{
				successTimes++;
				if(ret1!=0)
					szTrack2[0]=0;
				if(ret2!=0)
					szTrack3[0]=0;

                sprintf((char*)szTmp, "T2:%.25s", szTrack2);
                _vDisp(2, szTmp);
                vDispVarArg(3, "T3:%.25s", szTrack3);
                _vSetTimer(&ulTimer, 200);                
			}else
            {
                _vDisp(2, "请刷卡!");
                _vDisp(3, "");
                ulTimer=0;
            }
            _uiMagReset();
		}
		if(_uiKeyPressed())
        {
            uiKey=_uiGetKey();
            if(uiKey==_KEY_ESC)
                break;
            if(uiKey==_KEY_ENTER)
            {
                cUpdateFlag=1;
                successTimes=0;
                totalTimes=0;
            }
        }
		_vDelay(5);
	}
	_uiMagClose();
	return 0;
}

int mHardwareTestLcdColor(void *m)
{
#ifdef ST7789    
	UG_COLOR colors[]={C_BLACK,C_WHITE,C_RED,C_GREEN,C_BLUE};
#else
    UG_COLOR colors[]={C_BLACK,C_WHITE};
#endif    
    char i;
    
	_vCls();
	_vFlushKey();
	
    //lcd_SetBackLight(100);
    for(i=0;i<(sizeof(colors)/sizeof(colors[0]));i++)
	{
        dbg("color=%x\n",colors[i]);
        LcdClean(colors[i]);
       	while(1)
		{
			if (_uiKeyPressed() && _uiGetKey()==_KEY_ENTER)
			{
				break;
			}else
			{
				_vDelay(5);
			}
		}
    }
	
	return 0;
}

int mHardwareTestLcdBacklight(void *m)
{
	char i ;
    char tmp[32];
    int  light;
	
	_vCls();
    
    light=lcd_GetBackLight();
    for(i=10;i<=100;i+=10){
        sprintf(tmp,"当前背光值 %d", i/10);
        vDispCenter(2, tmp, 0);
        lcd_SetBackLight(i);
        mdelay(800);
    }
    
    lcd_SetBackLight(light);
	return 0;
}

int mHardwareTestKeyboard(void *m)
{
	return mFacTestKeyboard(m);
}


void testledonebyone(void)
{
#ifdef ENABLE_NFCLED
    char i;
    char on = 0x0f;

	NFCOpen();
	
	for(i=0;i<4;i++){
        //on &= ~(1<<(3-i));
        on = ~(1<<i);
        setnfcleds(on);
		//lcdflush();
        mdelay(2000);
    }
    //mdelay(500);
    setnfcleds(0xF0);
    mdelay(2000);
	NFCClose();
#else
    mdelay(1000);
#endif    
}


int mHardwareTestNfcLed(void *m)
{
#ifndef ENABLE_NFCLED  
    _vCls();
	vDispCenter(1, "非接Led测试", 1);    
    vMessage("终端不支持此功能");
    return 0;
#else
    uint key;
    _vCls();
	vDispCenter(1, "非接Led测试", 1); 
    
	vDispCenter(3, "按\"确认\"键继续", 0);
	vDispCenter(4, "按\"取消\"键退出", 0);	

	while(1)
	{
		if(_uiKeyPressed() == TRUE)
		{
			key = _uiGetKey();
			if (key == _KEY_ESC)
			{
				break;
			}else if (key == _KEY_ENTER)
			{
				testledonebyone();
			}else
			{
				_vDelay(10);
				continue;
			}
		}
	}
    return 0;
#endif
}

int mHardwareTestICCard(void *m)
{
	uchar atr[100];
	char szAtr[100];
	uint ret;
	uint slot=0;
    uint uiKey;
	uint totalTimes;
	uint successTimes;
    char cUpdateFlag=1;
    char cardevent=0;
    ulong ulTimerOut;
    
	_vCls();
	_vFlushKey();

    _vSetTimer(&ulTimerOut, 10*60*100);     //10分钟
	totalTimes = 0;
	successTimes = 0;
	while(!_uiTestTimer(ulTimerOut))
	{
        _vDelay(10);
        if(cUpdateFlag)
        {
            vDispCenter(1, "IC卡测试", 1);
            if(cardevent==0)
                _vDisp(2, "请插卡:");
            vDispVarArg(4, "IC卡成功率:%d/%d", successTimes, totalTimes);
            vDispCenter(_uiGetVLines(), "\"取消\"退出,\"确认\"重测", 0);
            cUpdateFlag=0;
        }
		ret=_uiTestCard(slot);
		if(cardevent==0 && ret==1)
		{
            cardevent=1;
            cUpdateFlag=1;
			totalTimes++;
			ret=_uiResetCard(slot, atr);
			if(ret>0)
			{
				successTimes++;
                
				_vDisp(2, "测试成功!");
				vOneTwo0(atr, ret, (uchar*)szAtr);
				vDispVarArg(3, "ATR:%s", szAtr);                
			}else{
				_vBuzzer();
                _vDisp(2, "上电失败");
                _vDisp(3, "");
			}
            
            continue;
		}
        //拔卡了
        if(cardevent==1 && ret==0)
        {
            cardevent=0;
            //_uiCloseCard(slot);
            _vDisp(2, "请插卡:");
            _vDisp(3, "");
        }
        
		if(_uiKeyPressed())
        {
            uiKey=_uiGetKey();
            if(uiKey==_KEY_ESC)
                break;
            if(uiKey==_KEY_ENTER)
            {
                cUpdateFlag=1;
                successTimes=0;
                totalTimes=0;
            }
        }
	}
	_uiCloseCard(slot);
	return 0;
}

int mHardwareTestQrCode(void *m)
{
	return mFacTestQrCode(m);
}

int mHardwareTestDispQrCode(void *m)
{
	//vDispTermQrCode(0);
	char szBuf[100];
	//char szDispBuf[100];
	_vCls();

	vDispCenter(1, "显码测试", 1);
	 _uiGetSerialNo((uchar*)szBuf);
	//sprintf();

	//vDispTermQrCode(0);
	iDispTransQrCode(szBuf, szBuf);	

	if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
	{
		//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
		return -1;
	}

	return 0;
}


int mHardwareTestNotSupport(void *m)
{
	_vCls();

	vDispCenter(2, "终端不支持此功能", 0);

	iGetKeyWithTimeout(5);

	return 0;
}

int mHardwareTestBeep(void *m)
{
	uint key;

	_vCls();

	while(1)
	{
		vDispCenter(1, "蜂鸣器测试", 1);
		vDispCenter(_uiGetVLines()-1, "按\"确认\"键继续", 0);
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出", 0);

		if(_uiKeyPressed() == TRUE)
		{
			key = _uiGetKey();
			if (key == _KEY_ESC)
			{
				break;
			}else if (key == _KEY_ENTER)
			{
				vDispCenter(2, "10S鸣响", 0);
				vDispCenter(_uiGetVLines()-1, "", 0);
				vDispCenter(_uiGetVLines(), "", 0);
				BuzzerOn(10*1000);
				_vDelay(10*100);
				vDispCenter(2, "", 0);
			}else
			{
				_vDelay(10);
				continue;
			}
			
		}
	}
	return 0;
}

int mHardwareTestRTC(void *m)
{
	mSetPosTime(m);
	iPosSyncTime();

	return 0;
}

extern int iGprsFacTest(char *pszIMEI, char *pszICCID);

int mHardwareTest4gSignal(void *m)
{
	char szICCID[30], szIMEI[30];
	int ret=0;
	int signal;
	int retry = 0;
    char buf[40];

	while(1)
	{
		_vCls();
		vDispCenter(1, "信号强度", 1);
		vDispCenter(2, "处理中", 0);
		
		ret=iGprsFacTest(szIMEI, szICCID);
		if(ret<0)
		{
			_vBuzzer();
			if(ret==-99){
				vDispCenter(2, "模块上电失败", 0);
			}else{
				vDispCenter(2, "模块上电成功", 0);
				vDispVarArg(3, "模块获取信息失败:%d", ret);
			}
		}else
		{
			//vDispCenter(2, "sim卡模块上电成功", 0);
			//vDispVarArg(3, "IMEI:%s", szIMEI);
			//vDispVarArg(4, "ICCID:%s", szICCID);
			if(ret>0)
				vDispCenter(2, "sim卡不存在或状态不正常", 0);
			else
				//vDispCenter(2, "sim卡正常", 0);
				while(((signal = gprs_at_GetSignalQuality()) == 0)&&
					(retry < 20))
				{
					retry++;
					dbg("Call gprs_at_GetSignalQuality again\n");
					_vDelay(100);
				}
				vDispCenter(2, "", 0);
                sprintf(buf, "信号强度范围(%d)", signal);
                vDispCenter(3, buf, 0);
			//vDispCenter(6,"gprs模块检测成功",0);
		}
		
		
		//if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
		//	break;
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
		if (iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
		{
			//dbg("iGetSpecialKeyWithTimeout return _KEY_ESC\n");
			break;
		}
		
	}
	if(gl_SysInfo.iCommType!=VPOSCOMM_GPRS)
	{
		dbg("gl_SysInfo.iCommType!=VPOSCOMM_GPRS\n");
		_vCls();
		vDispCenter(2, "关闭无线模块中", 0);
		iCommLogout(VPOSCOMM_GPRS, 1);
	}
	//iGetKeyWithTimeout(3);
	
	return 0;
}


int mHardwareTestPPP(void *m)
{
	//char szICCID[30], szIMEI[30];
	int ret=0;
	//int signal;
	//int retry = 0;

	while(1)
	{
		_vCls();
		vDispCenter(1, "PPP拨号测试", 1);
		_vDisp(2, "PPP拨号处理中......");
		
		ret = gprsInit();
		if(ret != 0)
		{
			
			_vDisp(2, " ");;
			vDispCenter(2, "PPP拨号失败", 0);
			
		}else
		{
			_vDisp(2, " ");;
			vDispCenter(2, "PPP拨号成功", 0);
		}

		vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
		if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
		{
			//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
			break;
		}
		
	}

	if(gl_SysInfo.iCommType!=VPOSCOMM_GPRS)
	{	
		dbg("gl_SysInfo.iCommType!=VPOSCOMM_GPRS\n");
		_vCls();
		vDispCenter(2, "关闭无线模块中", 0);
		iCommLogout(VPOSCOMM_GPRS, 1);
	}

	return 0;
}

int mHardwareTestSecurity(void *m)
{
	int ret;
	//uchar byArrRand[5];
	//char szRand[20];
	char buf[32];

	_vCls();
	vDispCenter(1, "安全测试", 1);
	
	
	ret = checkIfSensorEnabled();
    if (ret == 0)
    {
        sprintf(buf, "%s%s", "安全传感器", "未开启");
        vDispCenter(2, buf, 0);
		lcdflush();
        _vDelay(200);
        return 0;
    }

	while(1)
	{
		ret = GetTamperStatus();
		switch(ret)
		{
			case TAMPER_STATUS_OK:
				//rand_hardware_byte_ex(byArrRand, 4);
				//TranslateHexToChars(byArrRand, 4, szRand);
				vDispCenter(2, "无触发", 0);
				//vDispCenter(3, szRand, 0);
				vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
				if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
				{
					//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
					return 0;
				}
				break;

			default:
				vDispCenter(2, "机器故障：10000", 0);
				vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
				if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
				{
					//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
					return 0;
				}
				//vDispCenter(3, szRand, 0);
				break;
				
		}
	}

	//return 0;
}


SMenu menu_HardwareLcdMemu =
{"液晶测试", 0, 1, 2, 
	{
		{"颜色测试", mHardwareTestLcdColor, NULL}, 
		{"背光测试", mHardwareTestLcdBacklight, NULL},
		NULL
	}
};

SMenu menu_HardwareTest4GMemu =
{"无线测试", 0, 1, 2, 
	{
		{"信号强度", mHardwareTest4gSignal, NULL}, 
		{"PPP拨号", mHardwareTestPPP, NULL},
		NULL
	}
};

int mHardwareNfcFieldOn(void *m)
{
	//int ret;
	//char buf[32];

	_vCls();
	vDispCenter(1, "非接卡场强测试", 1);
	vDispCenter(2, "非接场强开启", 0);
	NFCOpen();
	while(1)
	{
		//vDispCenter(3, szRand, 0);
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
		if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
		{
			NFCClose();
			//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
			return 0;
		}
	}
}


int waitNfcInEvent()
{
  	//int ret;
	//int ifd;
	int iloop = 0;
	unsigned char tmpStatus;

	nfc_open();
	while(iloop++ < 10)
	{
		nfc_detect(&tmpStatus);

		if (tmpStatus == '0')
		{
			nfc_close();
			return 0;
		}
        _vDelay(1);
	}
	
	nfc_close();

    return -1;
}

int waitNfcOutEvent()
{
/*
  	//int ret;
	unsigned char tmpStatus;

	nfc_open();
    nfc_detect(&tmpStatus);
    nfc_close();
    if (tmpStatus == '0')
    {
        return -1;
    }else
    {
        return 0;
    }
*/
#ifdef NFC_IC_MH1XXX
    mh1xxx_reset_field();
#else
    YC_NFC_Field_Reset();
#endif   
    mdelay(10);
    if(_uiTestCard(8)==0)
    {
        mdelay(10);
        if(_uiTestCard(8)==0)
            return 0;
    }
    return -1;
}
int mHardwareNfcDetectCard(void *m)
{
	int nfcCardType;
	uchar UIDLen;
	uchar byUID[100];
	char szNfcCardType[100] = "";
	char szUID[100] = "";
	uint ret;
    uint uiKey;
	uint totalTimes;
	uint successTimes;
    char cUpdateFlag=1;
    char cardevent=0;
#if 1
	_vCls();
	_vFlushKey();

	totalTimes = 0;
	successTimes = 0;
    //nfc_open();
    vDispCenter(1, "非接卡寻卡测试", 1);
    _vDisp(2, "寻卡中......");
	while(1)
	{
        if(cUpdateFlag)
        {                         
            vDispVarArg(4, "寻卡成功率:%d/%d", successTimes, totalTimes);
            vDispCenter(_uiGetVLines(), "\"取消\"退出,\"确认\"重测", 0);
            cUpdateFlag=0;
        }
        if(_uiKeyPressed())
        {
            uiKey=_uiGetKey();
            if(uiKey==_KEY_ESC)
                break;
            if(uiKey==_KEY_ENTER)
            {
                cUpdateFlag=1;
                successTimes=0;
                totalTimes=0;

                cardevent=0;
                _vDisp(2, "寻卡中......");
                _vDisp(3, "");
            }
        }
        if(cardevent==0)
        {
            ret=_uiTestCard(8);
            if(ret==2 || ret==9)
            {
                cUpdateFlag=1;
                totalTimes++;
                cardevent=1;
                
                if(ret==9)
                {
                    _vBuzzer();
                    _vDisp(2, "多卡冲突");
                    _vDelay(5);
                    continue;
                }          

                //nfcCardType = emv_getType();
                if(NFCGetCardType(&nfcCardType) != NFC_ERR_NONE)
                {
                    _vBuzzer();
                    _vDisp(2, "读取卡片类型失败");
                    _vDelay(5);
                    continue;
                }
                szNfcCardType[0]=0;
                if(nfcCardType==NFC_CARDTYPEA || nfcCardType==NFC_CARDTYPEB)
                {
                    if (nfcCardType == NFC_CARDTYPEA)
                    {
                        strcpy(szNfcCardType, "TYPE A");
                    }else if (nfcCardType == NFC_CARDTYPEB)
                    {
                        strcpy(szNfcCardType, "TYPE B");
                    }
                    if(NFCGetUID(byUID,&UIDLen) != NFC_ERR_NONE)
                    {
                        _vBuzzer();
                        _vDisp(2, "读取UID失败");
                        _vDelay(200);
                        continue;
                    }
                    TranslateHexToChars(byUID, UIDLen, szUID);
                    successTimes++;

                    vDispVarArg(2, "卡片类型: %s", szNfcCardType);
                    vDispVarArg(3, "卡片 UID: %s", szUID);
                }else{
                    _vBuzzer();
                    _vDisp(2, "");
                    _vDisp(3, "");
                }
                _vDelay(5);
                continue;
            }
		}
        if(cardevent==1 && waitNfcOutEvent()==0)
        {
            cardevent=0;
            _vDisp(2, "寻卡中......");
            _vDisp(3, "");
        }
		_vDelay(5);
	}
    _uiCloseCard(8);
#endif    
	return 0;

	//int ret;
	//char buf[32];
/*
	_vCls();
	vDispCenter(1, "非接卡寻卡测试", 1);
	NFCOpen();
	while(1)
	{
		//vDispCenter(3, szRand, 0);
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
		if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
		{
			NFCClose();
			//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
			return 0;
		}
	}
    */
}
#if 0
int mHardwareNfcDetectCard(void *m)
{
	int nfcCardType;
	uchar UIDLen;
	uchar byUID[100];
	char szNfcCardType[100] = "";
	char szUID[100] = "";
	uint ret;
    uint uiKey;
	uint totalTimes;
	uint successTimes;
    char cUpdateFlag=1;
    char cardevent=0;

	_vCls();
	_vFlushKey();

	totalTimes = 0;
	successTimes = 0;
    //nfc_open();
    vDispCenter(1, "非接卡寻卡测试", 1);
    _vDisp(2, "寻卡中......"); 
	while(1)
	{
        if(cUpdateFlag)
        {                         
            vDispVarArg(4, "寻卡成功率:%d/%d", successTimes, totalTimes);
            vDispCenter(_uiGetVLines(), "\"取消\"退出,\"确认\"重测", 0);
            cUpdateFlag=0;
        }

        if(cardevent==0 && waitNfcInEvent()==0)
        {
            cUpdateFlag=1;
            totalTimes++;
            //ret=waitNfcInEvent();
            dbg("waitNfcEvent return = %d\n", ret);

            cardevent=1;
            nfcCardType = emv_getType();
            if(nfcCardType==1 || nfcCardType==2)
            {
                if (nfcCardType == 1)
                {
                    strcpy(szNfcCardType, "TYPE A");
                }else if (nfcCardType == 2)
                {
                    strcpy(szNfcCardType, "TYPE B");
                }
                UIDLen = emv_getUID(byUID);
                TranslateHexToChars(byUID, UIDLen, szUID);
                
                successTimes++;

                vDispVarArg(2, "卡片类型: %s", szNfcCardType);
                vDispVarArg(3, "卡片 UID: %s", szUID);
            }else{
                _vBuzzer();
                _vDisp(2, "");
                _vDisp(3, "");
            }
            _vDelay(5);
            continue;
		}
        if(cardevent==1 && waitNfcOutEvent()==0)
        {
            cardevent=0;
            _vDisp(2, "寻卡中......");
            _vDisp(3, "");
        }
		if(_uiKeyPressed())
        {
            uiKey=_uiGetKey();
            if(uiKey==_KEY_ESC)
                break;
            if(uiKey==_KEY_ENTER)
            {
                cUpdateFlag=1;
                successTimes=0;
                totalTimes=0;

                cardevent=0;
            }
        }
		_vDelay(5);
	}
    //nfc_close();
	return 0;

	//int ret;
	//char buf[32];
/*
	_vCls();
	vDispCenter(1, "非接卡寻卡测试", 1);
	NFCOpen();
	while(1)
	{
		//vDispCenter(3, szRand, 0);
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
		if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
		{
			NFCClose();
			//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
			return 0;
		}
	}
    */
}
#endif
int mHardwareNfcMifareCard(void *m)
{
	_vCls();
	vDispCenter(1, "Mifare卡寻卡测试", 1);
	NFCOpen();
	while(1)
	{
		//vDispCenter(3, szRand, 0);
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
		if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
		{
			NFCClose();
			//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
			return 0;
		}
	}
}

int gBurnTestTimeHour = 2;

int mBurnTestSetTime(void *m)
{
	TEvntMask pressedKeyEvt;

	_vCls();
	vDispCenter(1, "拷机时间设置", 1);
	if (gBurnTestTimeHour > 0)
	{
		vDispVarArg(2, "当前拷机时间为%d小时", gBurnTestTimeHour);
	}else
	{
		vDispVarArg(2, "当前拷机时间为%s", "不间断");
	}
	
		//vDispCenter(3, szRand, 0);
		vDispCenter(3, "1. 2小时   2. 4小时 ", 0);
		vDispCenter(4, "3. 12小时  4. 24小时", 0);
		vDispCenter(5, "5. 不间断              ", 0);
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
		

	pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
										KEYBORADEVM_MASK_KEY_2 |
										KEYBORADEVM_MASK_KEY_3 |
										KEYBORADEVM_MASK_KEY_4 |
										KEYBORADEVM_MASK_KEY_5 |
										KEYBORADEVM_MASK_KEY_CANCEL, 0);

	switch(pressedKeyEvt)
	{
		case KEYBORADEVM_MASK_KEY_1:
			gBurnTestTimeHour = 2;
			break;
			
		case KEYBORADEVM_MASK_KEY_2:
			gBurnTestTimeHour = 4;
			break;

		case KEYBORADEVM_MASK_KEY_3:
			gBurnTestTimeHour = 12;
			break;

		case KEYBORADEVM_MASK_KEY_4:
			gBurnTestTimeHour = 24;
			break;

		case KEYBORADEVM_MASK_KEY_5:
			gBurnTestTimeHour = 0;
			break;

		default:
			break;
	}

	return 0;
}

int mBurnTestLcd(void *m)
{
#ifdef USE_COLOR_RGB565    
	UG_COLOR colors[]={C_BLACK,C_WHITE,C_RED,C_GREEN,C_BLUE};
#else
    UG_COLOR colors[]={C_BLACK,C_WHITE};
#endif    
    char i;

	_vCls();

	vDispCenter(1, "拷机测试", 1);
	vDispCenter(2, "液晶", 0);

	for(i=0;i<(sizeof(colors)/sizeof(colors[0]));i++)
	{
        dbg("color=%x\n",colors[i]);
        LcdClean(colors[i]);
		//_vDelay(2*100);
		if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
		{
			//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
			return -1;
		}
	}

	//_vCls();
	//vDispCenter(1, "拷机测试", 0);
	//vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
	

	return 0;
}

int mBurnTestMagCard(void *m)
{
	ulong ulTimer;
	
	_vCls();

	vDispCenter(1, "拷机测试", 1);
	vDispCenter(2, "磁卡", 0);
	vDispCenter(3, "等待读卡", 0);
	
	_vSetTimer(&ulTimer, 2 * 100L);

    _uiMagReset();
	while(_uiTestTimer(ulTimer) == 0)
	{
		/*
        wait_msr_event(status,data,&datalen);
		if (status[0] == '0')
		{
			vDispCenter(4, "读卡成功", 0);
		}
        */
        if(_uiMagTest())
        {
            vDispCenter(4, "读卡成功", 0);
            break;
        }
        _vDelay(10);
	}
    _uiMagClose();
    
	//_vCls();
	//vDispCenter(1, "拷机测试", 0);
	//vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
	if(iGetSpecialKeyWithTimeout(1, _KEY_ESC) == 0)
	{
		//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
		return -1;
	}

	return 0;
}

int mBurnTestICCard(void *m)
{
	ulong ulTimer;
	
	_vCls();

	vDispCenter(1, "拷机测试", 1);
	vDispCenter(2, "IC卡", 0);
	vDispCenter(3, "等待读卡", 0);
	
	_vSetTimer(&ulTimer, 2 * 100L);
	while(_uiTestTimer(ulTimer) == 0)
	{
        /*
		wait_icc_present_event(status);
		if (status[0] == '1')
		{
			vDispCenter(4, "读卡成功", 0);
		}
        */
        if(_uiTestCard(0)==1 && _uiResetCard(0, NULL)>0)
        {
            vDispCenter(4, "读卡成功", 0);
            break;
        }
        _vDelay(10);
	}
    _uiCloseCard(0);
    
	//_vCls();
	//vDispCenter(1, "拷机测试", 0);
	//vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
	if(iGetSpecialKeyWithTimeout(1, _KEY_ESC) == 0)
	{
		//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
		return -1;
	}

	return 0;
}

int mBurnTestNfcCard(void *m)
{
	ulong ulTimer;
	
	_vCls();

	vDispCenter(1, "拷机测试", 1);
	vDispCenter(2, "非接卡", 0);
	vDispCenter(3, "等待读卡", 0);
	
	_vSetTimer(&ulTimer, 2 * 100L);
	while(_uiTestTimer(ulTimer) == 0)
	{
        /*
		wait_nfc_event(status);
		if (status[0] == '3')
		{
			vDispCenter(4, "读卡成功", 0);
		}
        */
        if(_uiTestCard(8)==2 && _uiResetCard(8, NULL)>0)
        {
            vDispCenter(4, "读卡成功", 0);
            break;
        }
        _vDelay(10);
	}
    _uiCloseCard(8);

	//_vCls();
	//vDispCenter(1, "拷机测试", 0);
	//vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
	if(iGetSpecialKeyWithTimeout(1, _KEY_ESC) == 0)
	{
		//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
		return -1;
	}

	return 0;
}

int mBurnTestNfcLed(void *m)
{
#ifdef ENABLE_NFCLED
	_vCls();

	vDispCenter(1, "拷机测试-LED灯", 1);

	testledonebyone();

	if(iGetSpecialKeyWithTimeout(1, _KEY_ESC) == 0)
	{
		//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
		return -1;
	}
#endif
	return 0;
}
void vWriteBurnLog(char *pszLog);
int iGetQrCodeWithTimeout(char *pszQrCode, int size, ulong timeoutSecond)
{ 
	uint ret;
	ulong ulTimeOut;

    vSetQrPreView(1);
    //vWriteBurnLog("_uiQrStart......");
    ret=_uiQrStart();
    if(ret)
    {
        //_vQrEnd();
        
        BuzzerOn(2*1000);
        vMessage("初始化扫码模块失败");
        return 1;
    }
    
    //vWriteBurnLog("_uiGetQrCode......");
#ifndef SUPPORT_8W_CAMERA    
    _vSetTimer(&ulTimeOut, timeoutSecond*100);     //设置超时
    while(_uiTestTimer(ulTimeOut)==0)
    {
        ret=_uiGetQrCode(pszQrCode, size);
        if(ret)
            break;
        _vDelay(1);
    }
#else
    _vCls();
    ret=_iGetQrCodeWithTime(pszQrCode, size, 1, timeoutSecond*1000, NULL);
#endif
    //vWriteBurnLog("_vQrEnd......");
    _vQrEnd();
    
    _vCls();
    //flushStatusBar(1);
    if(ret>0 && pszQrCode[0])
        return 0;
    else  
        return 2;
}


int mBurnTestQrCode(void *m)
{
	int ret;
	char szQrcode[200];
	
	_vCls();

	vDispCenter(1, "拷机测试", 1);
	vDispCenter(2, "扫码", 0);
	
	ret = iGetQrCodeWithTimeout(szQrcode, sizeof(szQrcode), 2);
	if (ret == 0)
	{
		if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
		{
			//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
			return -1;
		}
	}

	return 0;
}

int mBurnTestDispQrCode(void *m)
{
	char szBuf[100];
	_vCls();

	vDispCenter(1, "拷机测试", 1);
	 _uiGetSerialNo((uchar*)szBuf);
	//sprintf();

	//vDispTermQrCode(0);
    //szBuf[0]=0;
    strcpy(szBuf, "1234567890ABCDEF");
	iDispTransQrCode(szBuf, szBuf);	

	if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
	{
		//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
		return -1;
	}

	return 0;
}

int mBurnTest4G(void *m)
{
	char szICCID[30], szIMEI[30];
	int ret=0;
	int signal;
	int retry = 0;
	int iloop = 0;
    char buf[40];

	while(iloop < 1)
	{
		iloop++;
		_vCls();
		vDispCenter(1, "拷机测试", 1);
		vDispCenter(2, "无线测试进行中", 0);
		
		ret=iGprsFacTest(szIMEI, szICCID);
		if(ret<0)
		{
			_vBuzzer();
			if(ret==-99){
				vDispCenter(2, "模块上电失败", 0);
				ret = 1;
			}else{
				vDispCenter(2, "模块上电成功", 0);
				vDispVarArg(3, "模块获取信息失败:%d", ret);
				ret = 2;
			}
		}else
		{
			//vDispCenter(3, "sim卡模块上电成功", 0);
			vDispVarArg(4, "IMEI:%s", szIMEI);
			if(strlen(szICCID)+6<=_uiGetVCols())
                vDispVarArg(5, "ICCID:%s", szICCID);
            else
                vDispVarArg(5, "%s", szICCID);
			if(ret>0)
			{
				vDispCenter(2, "sim卡不存在或不正常", 0);
				ret = 3;
			}
			else
			{
				vDispCenter(6, "获取信号强度中", 0);
				while(((signal = gprs_at_GetSignalQuality()) == 0)&&
					(retry < 20))
				{
					retry++;
					dbg("Call gprs_at_GetSignalQuality again\n");
					_vDelay(100);
				}
                sprintf(buf, "信号强度范围(%d)", signal);
				vDispCenter(6, buf, 0);
				if (iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
				{
					//_vCls();
					//vDispCenter(1, "关闭无线模块中", 0);
					//iCommLogout(VPOSCOMM_GPRS, 1);
					return -1;
				}
				ret = 0;
			}	
		}
	}

	//_vCls();
	//vDispCenter(1, "关闭无线模块中", 0);
	//iCommLogout(VPOSCOMM_GPRS, 1);

	return 0;
}

int mBurnTestSecurity(void *m)
{
	int ret;
	uchar byArrRand[5];
	char szRand[20];
	char buf[32];
	ulong ulTimeOut;
    
	_vCls();
	vDispCenter(1, "拷机测试", 1);
	vDispCenter(2, "安全测试", 0);
	
	ret = checkIfSensorEnabled();
    if (ret == 0)
    {
        sprintf(buf, "%s%s", "安全传感器", "未开启");
        vDispCenter(3, buf, 0);
		lcdflush();
        _vDelay(200);
        return 1;
    }

	_vSetTimer(&ulTimeOut, 2*100);     //设置超时
    while(_uiTestTimer(ulTimeOut)==0)
	{
		ret = GetTamperStatus();
		switch(ret)
		{
			case TAMPER_STATUS_OK:
				rand_hardware_byte_ex(byArrRand, 4);
				TranslateHexToChars(byArrRand, 4, szRand);
				vDispCenter(3, "无触发", 0);
				vDispCenter(4, szRand, 0);
				if (iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
				{
					return -1;
				}
				return 0;

			default:
				vDispCenter(3, "机器故障：10000", 0);
				vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);
				return 2;	
		}
	}

	return 0;
}

int mBurnTestRtc(void *m)
{
	char buf[100] = {0};

	_vCls();

	vDispCenter(1, "拷机测试-RTC", 1);

	iPosSyncTime();
	_vGetTime(buf);
	dbg("buf = %s\n", buf);
	vDispCenter(2, buf, 0);

	if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
	{
		//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
		return -1;
	}

	return 0;
}


int mBurnTestBeep(void *m)
{
	_vCls();

	vDispCenter(1, "拷机测试-蜂鸣器", 1);

	BuzzerOn(2*1000);
	_vDelay(1*100);

	if(iGetSpecialKeyWithTimeout(1, _KEY_ESC) == 0)
	{
		//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
		return -1;
	}

	return 0;
}

#ifdef ENABLE_PRINTER
int mBurnTestPrinter(void *m)
{
    _vCls();

	vDispCenter(1, "拷机测试-打印机", 1);
    
    PrintOpen();
    if(iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
	{
		//dbg("iGetSpecialKeyWithTimeout == _KEY_ESC\n");
        PrintClose();
		return -1;
	}
    PrintClose();
    
    return 0;
}
#endif

void vWriteBurnLog(char *pszLog)
{
#if 0
    lfs_file_t file;
    char szFileName[30];
    
    strcpy(szFileName, "/SysMem/test.log");
    sys_lfs_file_open(&file, szFileName, LFS_O_CREAT | LFS_O_WRONLY);
    sys_lfs_file_seek(&file, 0, LFS_SEEK_SET);
    sys_lfs_file_write(&file, pszLog, strlen(pszLog));
    sys_lfs_file_close(&file);
#endif     
    mdelay(20);
    return;
}

int mBurnTest(void *m)
{
	ulong ulTimer;
    int i4GNum=0, iSecNum=0;
    int ret;
    int QrNum=0;
    ulong tick;
    int hour, dothour;

    tick=_ulGetTickTime();
#if 0
    {
        lfs_file_t file;
        char szFileName[30];
        char szData[50]={0};
        
        strcpy(szFileName, "/SysMem/test.log");
        ret = sys_lfs_file_open(&file, szFileName, LFS_O_RDONLY);
        if(ret==0)
        {
            _vCls();
            _vFlushKey();
            sys_lfs_file_read(&file, szData, 20);
            sys_lfs_file_close(&file);
            _vDisp(2, "file info:");
            _vDisp(3, szData);
            _vDisp(4, "continue?");
            if(_uiGetKey()==_KEY_ESC)
                return 0;
        }
    }
#endif

	if (gBurnTestTimeHour > 0)
	{
		dbg("gBurnTestTimeHour = %d\n", gBurnTestTimeHour);
		_vSetTimer(&ulTimer, gBurnTestTimeHour *60*60*100);
	}
	while(1)
	{
        mdelay(10);
		if (gBurnTestTimeHour > 0)
		{
			if(_uiTestTimer(ulTimer) == 1)
			{
				//Timeout
				break;
			}
		}
        
        vWriteBurnLog("TestLcd start......");
		if (mBurnTestLcd(NULL) != 0)
		{
			break;
		}
        
        vWriteBurnLog("TestNFC start......");
		if (mBurnTestNfcCard(NULL) != 0)
		{
			break;
		}
        
        vWriteBurnLog("TestMag start......");
		if (mBurnTestMagCard(NULL) != 0)
		{
			break;
		}
        
        vWriteBurnLog("TestICC start......");
		if (mBurnTestICCard(NULL) != 0)
		{
			break;
		}
        
#ifdef ENABLE_NFCLED
		if (mBurnTestNfcLed(NULL) != 0)
		{
			break;
		}
#endif      
        if(1 || ++QrNum<20)
        {
            vWriteBurnLog("TestQR  start......");    
            if (mBurnTestQrCode(NULL) != 0)
            {
                break;
            }
        }
        
        vWriteBurnLog("TestDQR start......");
		if (mBurnTestDispQrCode(NULL) != 0)
		{
			break;
		}
        
        vWriteBurnLog("Test4G  start......");
        ret=mBurnTest4G(NULL);
        if(ret)
        {
            if ( ret<0)
            {
                break;
            }
            ++iSecNum;
        }

        vWriteBurnLog("TestSeC start......");
        ret=mBurnTestSecurity(NULL);
		if(ret)
        {
            if ( ret<0)
            {
                break;
            }
            ++i4GNum;
        }
#if 0
		if (mBurnTestBeep(NULL) != 0)
		{
			break;
		}
#endif
        vWriteBurnLog("TestRtC start......");
		if (mBurnTestRtc(NULL) != 0)
		{
			break;
		}

#ifdef ENABLE_PRINTER
        vWriteBurnLog("TestPrt start......");
		if (mBurnTestPrinter(NULL) != 0)
		{
			break;
		}        
#endif
        
        _vDelay(5);
	}
#if 0    
    _vCls();
    if(i4GNum || iSecNum)
    {
        if(i4GNum)
            vDispVarArg(2, "4G测试失败:%d", i4GNum);
        if(iSecNum)
            vDispVarArg(3, "安全测试失败:%d", i4GNum);
    }
    
    vDispVarArg(3, "breakout:%d", iBreakFlag);
    BuzzerOn(2*1000);
    _vFlushKey();
    _uiGetKey();
#else
    _vCls();
    
    tick=(_ulGetTickTime()-tick+1000)/1000;
    hour=tick/3600;
    dothour=(tick%3600)/60*100/60;
    vDispVarArg(2, "设置拷机:%d小时", gBurnTestTimeHour);
    vDispVarArg(3, "实际拷机:%d.%02d小时", hour, dothour);
    vDispMid(4, "拷机结束,按任意键退出");
    BuzzerOn(500);
    _vFlushKey();
    _uiGetKey();
#endif
    return 0;
}

int mTerminalInfo(void *p)
{
	char szTUSN[30];
	char szICCID[30], szIMEI[30];
    int page=1;
    uint uiKey;
    ulong ulTimer;
    int timeout=30*100;         //30秒
    char hw[15], fw[15];
    int line;
	
	_vCls();
    
    iGprsFacTest(szIMEI, szICCID);
    _uiGetSerialNo(szTUSN);
    
    vGetPbocHwVerAndFwVer(hw, fw);
    
    _vSetTimer(&ulTimer, timeout);
    while(!_uiTestTimer(ulTimer))
    {
        if(page==1)
        {
            line=1;
            if(_uiGetVLines()>=6)
                vDispCenter(line++, "终端信息", 1);
            vDispVarArg(line++, "型号:%s", _pszGetModelName(NULL));
    
            _vDisp(line++, "TUSN:");
            vDispVarArg(line++, "%s", szTUSN);
            
            _vDisp(line++, "IMEI:");
            vDispVarArg(line++, "%s", szIMEI);
        }
        if(page==2)
        {
            line=1;
            if(_uiGetVLines()>=6)
                vDispCenter(line++, "终端信息", 1);
            _vDisp(line++, "ICCID:");
            vDispVarArg(line++, "%s", szICCID);
            vDispVarArg(line++, "HW: %s", hw);
            vDispVarArg(line++, "FW: %s", fw);
            vDispVarArg(line++, "SW: %s", _pszGetAppVer(NULL));
        }
        if(_uiKeyPressed())
        {
            _vSetTimer(&ulTimer, timeout);
            uiKey=_uiGetKey();
            if(uiKey==_KEY_UP && page>1)
            { 
                page--;
                continue;
            }
            if(uiKey==_KEY_DOWN && page<2)
            { 
                page++;
                continue;
            }
             if(uiKey==_KEY_ESC)
                 break;
        }
        _vDelay(1);
	}
    return 0;
}

int mPowerTest(void *p)
{
	Boolean usbcon;
	int BatteryVoltage;
	Battery_vol BatteryPercent;
	char szDispUsbCon[100];
	char szDispBatteryVoltage[100];
	char szDispBatteryPercent[100];
	char *szPercentOnly;
    
	_vCls();
	while(1)
	{		
		BatteryVoltage = battery_get_voltage();
		BatteryPercent = getBatteryVol();
		usbcon = checkIfUSBCon();
		
		if (usbcon == TRUE)
		{
			strcpy(szDispUsbCon, "电池状态:充电中");
		}else
		{
			strcpy(szDispUsbCon, "电池状态:放电中");
		}
		dbg("BatteryVoltage = %d\n", BatteryVoltage);
		dbg("BatteryPercent = %d\n", BatteryPercent);
		
		sprintf(szDispBatteryVoltage, "电池电压:%d.%02dV", BatteryVoltage/1000, (BatteryVoltage%1000)/10);
#ifdef ST7567
	 	if(BatteryPercent==VOL_PHASE_0) szPercentOnly = "10";
	    if(BatteryPercent==VOL_PHASE_1) szPercentOnly = "30";
	    if(BatteryPercent==VOL_PHASE_2) szPercentOnly = "60";
	    if(BatteryPercent==VOL_PHASE_3) szPercentOnly = "80";        
        if(BatteryPercent==VOL_PHASE_4) szPercentOnly = "100";
#else
        if(BatteryPercent==VOL_PHASE_0) szPercentOnly = "10";
	    if(BatteryPercent==VOL_PHASE_1) szPercentOnly = "30";
	    if(BatteryPercent==VOL_PHASE_2) szPercentOnly = "70";
	    if(BatteryPercent==VOL_PHASE_3) szPercentOnly = "100";        
#endif        
		sprintf(szDispBatteryPercent, "电池电量:%s%%", szPercentOnly);

		vDispCenter(1, "电池状态", 1);
		_vDisp(2, szDispUsbCon);
		_vDisp(3, szDispBatteryVoltage);
		_vDisp(4, szDispBatteryPercent);
		_vDisp(5, "备份电池电压:3.0V");
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);

		if (iGetSpecialKeyWithTimeout(2, _KEY_ESC) == 0)
		{
			break;
		}
	}

	return 0;
}
int displayCountdownUI(int countdownSeconds)
{
	ulong ulTimer;
	_vSetTimer(&ulTimer, countdownSeconds * 100L + 50);

	_vCls();
	while(_uiTestTimer(ulTimer) == 0)
	{
		
		vDispCenter(1, "休眠测试", 1);
		vDispVarArg(2, "休眠倒计时%d秒", countdownSeconds--);		
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出倒计时", 0);
		
		if (iGetSpecialKeyWithTimeout(1, _KEY_ESC) == 0)
		{
			return -1;
		}
	}

	return 0;
}
int mSleepTest(void *p)
{
	TEvntMask pressedKeyEvt;

	while(1)
	{
		_vCls();

		vDispCenter(1, "休眠测试", 1);
		_vDisp(2, "1.立即休眠 2.十秒休眠");
		_vDisp(3, "3.一小时后休眠");
		vDispCenter(_uiGetVLines() - 1, "按任意键退出休眠", 0);
		vDispCenter(_uiGetVLines(), "按\"取消\"键退出测试", 0);

		pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
											KEYBORADEVM_MASK_KEY_2 |
											KEYBORADEVM_MASK_KEY_3 |
											KEYBORADEVM_MASK_KEY_CANCEL, 0);

		
		switch(pressedKeyEvt)
		{
			case KEYBORADEVM_MASK_KEY_1:
				EnterDeepSleepMode();
				break;
				
			case KEYBORADEVM_MASK_KEY_2:
				if (displayCountdownUI(10) == 0)
				{
					EnterDeepSleepMode();
				}
				break;

			case KEYBORADEVM_MASK_KEY_3:
				if (displayCountdownUI(60*60) == 0)
				{
					EnterDeepSleepMode();
				}
				break;

			default:
				return -1;
		}
	}

    //return 0;
}

#ifdef ENABLE_PRINTER
int mHardwareTestPrintReipt(void *p)
{
    int ret;
    
    _vCls();
    vDispCenter(1, "小票打印", 1);
    
    vDispMid(3, "开始打印...");
    
    ret=_uiPrintOpen();
    if(ret)
        return 1;
    
    _uiPrintSetFont(2);
    _uiPrintEx(2, "重打印凭证(DUPLICATED)", 0);
    _uiPrint("", 0);
    _uiPrint("商户名称(MERCHANT NAME):", 0);
    _uiPrint("商户编号(MERCHANT NO):", 0);
    _uiPrint("终端编号(TERMINAL NO):", 0);
    _uiPrint("收单机构号(ACQUIRER):", 0);
    _uiPrint("卡号(CARD NO):", 0);
    _uiPrint("XXXX XXXX XXXX XXXXXXX(刷卡方式)", 0);
    _uiPrint("卡序列号(CARD SN):XXX", 0);
    _uiPrint("卡类别(CARD TYPE):", 0);
    _uiPrint("有效期(EXP DATE):", 0);
    _uiPrint("日期/时间(DATA/TIME):", 0);
    _uiPrint("授权号(AUTH NO):", 0);
    _uiPrint("批次号(BATCH NO):", 0);
    _uiPrint("凭证号(VOUCHER NO):", 0);
    _uiPrint("检索参考号(REFER NO):", 0);
    _uiPrint("外卡检索参考号(FREFER NO):", 0);
    _uiPrint("交易类型(TRANS TYPE):", 0);
    _uiPrint("交易金额(AMOUNT):", 0);
    _uiPrint("小费(TIP):", 0);
    _uiPrint("总计(SUM TOTAL):", 0);
    _uiPrint("备注(REFERENCE):", 0);
    _uiPrint("原授权号(AUTH NO):", 0);
    _uiPrint("原凭证号(VOUCHER NO):", 0);
    _uiPrint("原检索参考号(REFER NO):", 0);
    _uiPrint("交易证书(TC):", 0);
    _uiPrint("AID:", 0);
    _uiPrint("应用标签(APP LABEL):", 0);
    _uiPrint("应用首选名(APP PREFERRED NAME):", 0);
    
    #if 0
    {
        extern int iPrintQrCodeStr(char *pszQrCodeStr);
        char url[]="https://cpay.chanpay.com/h5-cashier-ui/posWxCode/index?id=C74AA4CAD3F92243E0539202FF0A8D03";
        iPrintQrCodeStr(url);
        _uiPrint("", 0);
    }
    #endif
    
    _uiPrint("持卡人签名(CARDHOLDER SIGNATURE):", 0);
    _uiPrint("", 0);
    _uiPrintEx(11, "本人确认以上交易，同意将其记入本卡账户", 0);
    _uiPrintEx(11, "I ACKNOWLEDGE SATISFACTORY RECEIPT OF RELATIVE GOODS/SERVICES", 0);
    
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 1);
    
    _uiPrintClose();
    
    vMessage("打印结束");
    
    return 0;
}
int mHardwareTestPrintFill(void *p)
{
    uchar buf[560*48];
    
    _vCls();
    vDispCenter(1, "填充打印", 1);
    
    vDispMid(3, "开始打印...");
    
    memset(buf, 0xFF, sizeof(buf));
    _uiPrintOpen();
    PrintImage(buf, sizeof(buf));
/*    
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 1);
*/    
    _uiPrintClose();
    
    vMessage("打印结束");
    return 0;
}

int mHardwareTestPrintDepth(void *m)
{
    int iDepth;
    uint key;
    int flush=1;
    
    //浓度1-2000,此处按1-1500范围做调节
    
    iDepth=0;
    while(1)
    {
        if(flush)
        {
            vDispCenter(1, "扫码浓度测试", 1);
            vDispMid(2, "按↑↓调节浓度(1~10)");
            PrintGetDepth(&iDepth);
            iDepth=(iDepth+149)/150;
            if(iDepth>10)
                iDepth=10;
            if(iDepth<1)
                iDepth=1;
            vDispVarArg(3, "当前浓度值:%d", iDepth);
            
            vDispMid(_uiGetVLines()-1, "按\"确认\"键打印小票");
            vDispMid(_uiGetVLines(), "按\"取消\"键退出测试");
            flush=0;
        }
        key=_uiGetKey();
        if(key==_KEY_ESC)
            break;
        if(key==_KEY_ENTER)
        {
            mHardwareTestPrintReipt(NULL);
            _vCls();
            flush=1;
            continue;
        }
        if(key==_KEY_UP)
        {
            if(iDepth<10)
            {
                PrintSetDepth((++iDepth)*150);
                vDispVarArg(3, "当前浓度值:%d", iDepth);
            }
            continue;
        }
        if(key==_KEY_DOWN)
        {
            if(iDepth>1)
            {
                PrintSetDepth((--iDepth)*150);
                vDispVarArg(3, "当前浓度值:%d", iDepth);
            }
            continue;
        }
    }
    return 0;
}

SMenu menu_HardwarePrinterMemu =
{"打印测试", 0, 1, 1, 
	{
		{"小票打印", mHardwareTestPrintReipt, NULL}, 
		{"填充打印", mHardwareTestPrintFill, NULL},
		{"打印浓度", mHardwareTestPrintDepth, NULL},
		NULL
	}
};
#endif

SMenu menu_HardwareNfcMemu =
{"非接卡测试", 0, 1, 2, 
	{
		{"场强测试", mHardwareNfcFieldOn, NULL}, 
		{"寻卡测试", mHardwareNfcDetectCard, NULL},
		{"Mifare卡", mHardwareTestNotSupport, NULL},
		NULL
	}
};

//硬件测试菜单
SMenu menu_HardwareTestMemu =
{"硬件测试", 0, 1, 2|0x40, 
	{
		{"按键", mHardwareTestKeyboard, NULL}, 
		{"液晶", 0, &menu_HardwareLcdMemu},
		{"手写屏", 0, &menu_FacTPMemu}, 
		{"磁卡", mHardwareTestMagCard, NULL}, 
		{"IC卡", mHardwareTestICCard, NULL}, 
		{"非接卡", 0, &menu_HardwareNfcMemu},
		{"LED灯", mHardwareTestNfcLed, NULL},
		{"扫码", mHardwareTestQrCode, NULL},
#ifndef ST7567      
		{"显码", mHardwareTestDispQrCode, NULL},
#ifdef ENABLE_PRINTER
		{"打印机", 0, &menu_HardwarePrinterMemu},
#else
        {"打印机", mHardwareTestNotSupport, 0},
#endif
#endif        
		NULL,
	}
};

SMenu menu_HardwareTestMemu2 =
{"硬件测试", 0, 1, 2|0x80, 
	{
#ifndef ST7567
		{"无线通信", 0, &menu_HardwareTest4GMemu},
		{"WIFI通信", mHardwareTestNotSupport, NULL},
		{"蓝牙通信", mHardwareTestNotSupport, NULL},
		{"安全", mHardwareTestSecurity, NULL},
		{"蜂鸣器", mHardwareTestBeep, NULL},
		{"语音", mHardwareTestNotSupport, NULL},
		{"RTC测试", mHardwareTestRTC, NULL},
#else        
		{"显码", mHardwareTestDispQrCode, NULL},
#ifdef ENABLE_PRINTER
		{"打印机", 0, &menu_HardwarePrinterMemu},
#else
        {"打印机", mHardwareTestNotSupport, 0},
#endif
		{"无线通信", 0, &menu_HardwareTest4GMemu},
		{"WIFI通信", mHardwareTestNotSupport, NULL},
		//{"蓝牙通信", mHardwareTestNotSupport, NULL},
		{"安全", mHardwareTestSecurity, NULL},
		{"蜂鸣器", mHardwareTestBeep, NULL},
		{"语音", mHardwareTestNotSupport, NULL},
		{"RTC测试", mHardwareTestRTC, NULL},        
#endif        

		NULL,
	}
};

SMenu menu_BurnTestMemu =
{"拷机测试", 0, 1, 2, 
	{
		{"时间设置", mBurnTestSetTime, NULL}, 
		{"拷机测试", mBurnTest, NULL},
		NULL
	}
};

SMenu menu_PowerTestMemu =
{"电源测试", 0, 1, 2, 
	{
		{"电池状态", mPowerTest, NULL}, 
		{"休眠测试", mSleepTest, NULL},
		NULL
	}
};


//POS自检菜单
SMenu menu_PosSelfCheckMemu =
{"POS自检程序", 0, 1, 2, 
	{
		{"终端信息", mTerminalInfo, NULL}, 
		{"硬件测试", 0, &menu_HardwareTestMemu},
		{"拷机测试", 0, &menu_BurnTestMemu}, 
		{"电源测试", 0, &menu_PowerTestMemu}, 
		{"其他测试", mHardwareTestNotSupport, NULL}, 
		
		NULL
	}
};

SMenuItem menu_HardwareTestHide={
	//"清除交易", mCleanTrans, 0
    "", 0, &menu_HardwareTestMemu2
};

int iPosSelfCheckTest(void *p)
{    
	gl_SysInfo.iCommType = VPOSCOMM_GPRS;
	EnterTestMenu(&menu_PosSelfCheckMemu);
	return 0;
}

//#define HARDWARE_PWD    "#300773#"
#define HARDWARE_PWD    "#609898#"
#define JSDFACTORY_PWD    "#98#"
int iDoHardWareTest(void)
{
    ulong ulKeyTimer;
    char keyBuf[12];
    int i;
    uint key;
    int timeInt=200;            //两次按键间隔不超过2秒
    
    memset(keyBuf, 0, sizeof(keyBuf));

    i=0;
    keyBuf[i++]='#';
    _vSetTimer(&ulKeyTimer, timeInt);       
    while(!_uiTestTimer(ulKeyTimer))
    {
        if(_uiKeyPressed())
        {
            _vSetTimer(&ulKeyTimer, timeInt);
            key=_uiGetKey();
            if(key==_KEY_DOWN)
                key='#';
            keyBuf[i++]=key;
            if(memcmp(HARDWARE_PWD, keyBuf, i) && memcmp(JSDFACTORY_PWD, keyBuf, i))
                break;
            if(strcmp(HARDWARE_PWD, keyBuf)==0)
            {
                iPosSelfCheckTest(NULL);
                break;
            }
            if(strcmp(JSDFACTORY_PWD, keyBuf)==0)
            {
                iFactorySimpleTest();
                break;
            }
            if(i>=sizeof(keyBuf)-1)
                break;
        }
        _vDelay(1);
    }
    return 0;
}

extern uint _uiGetSpace(int line);
int iCalUseSpace(void *p)
{
    char buf[50];
    
    _vCls();
    
    sprintf(buf,"单记录:银行卡%d",  sizeof(stTransRec));
    _vDisp(1, (uchar*)buf);
    //sprintf(buf,"单记录:扫码%d", sizeof(stQrTransRec));
    //_vDisp(2, (uchar*)buf);    
    
    sprintf(buf,"inf:%d,dat:%d",  sizeof(stSysInfo), sizeof(stSysData));
    _vDisp(2, (uchar*)buf);
    sprintf(buf,"8583:%d",  sizeof(st8583));
    _vDisp(3, (uchar*)buf);
    
    _uiGetSpace(4);
    iOK(30);
    
    return 0;
}

extern void vGetActiveUrl(char *pszUrl);
int iPrintParam(void *p)
{
    int ret;
    char buf[200];
    
    _vCls();
    ret=_uiPrintOpen();
    if(ret)
    {
        vMessage("打印机故障或缺纸");
        return 0;
    }
    
    _uiPrintEx(1, "银行卡商终信息:", 0);
	_uiPrintEx(1, gl_SysInfo.szMerchName, 0);
	_uiPrintEx(1, gl_SysInfo.szPosId, 0);
    _uiPrintEx(1, gl_SysInfo.szMerchId, 0);
    
 //   _uiPrintEx(1, "扫码商终信息:", 0);
 //   _uiPrintEx(1, gl_SysInfo.szQrPosId, 0);
 //   _uiPrintEx(1, gl_SysInfo.szQrMerchId, 0);

    sprintf(buf, "tms:[%s:%u]", gl_SysInfo.szTmsHost, gl_SysInfo.uiTmsHostPort);
    _uiPrintEx(11, buf, 0);

    _uiPrintEx(1, "交易地址:", 0);
    _uiPrintEx(11, gl_SysInfo.szSvrIp, 0);
    _uiPrintEx(11, gl_SysInfo.szQrTranUrl, 0);
//    _uiPrintEx(1, "证书下载地址:", 0);
//    _uiPrintEx(11, gl_SysInfo.szPubGetCertUrl, 0);
    
//    vGetActiveUrl(buf);
    _uiPrintEx(1, "激活地址:", 0);
    _uiPrintEx(11, buf, 0);

    _uiPrint("----END----", 0);
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 0);
    _uiPrint("", 1);
    
    _uiPrintClose();
	
    return 0;
}
extern int gprs_at_cmd_sndrcv(char *pszReq, char *pszCheckRsp, int iTimeOut, int icrflag, int iclearflag, char *pszOutRsp, int iOutSize);
//爱联4G模块Http升级
int iAiLianOTAHttpUp(void *m)
{
    char buf[200+1];
    int ret;
    int loop;
    char *p;
    
    _vCls();
    vDispCenter(1, "爱联OTA Http", 1);
    vDispMid(2, "开始?");
    if(iOK(30)!=1)
        return 0;
    _vDisp(2, "");
    
    vSetCommSleep(0);
    
    vDispMid(2, "模块上电...");
    ret=gprs_poweron();
    if(ret==1)
        _vDelay(6*100);
    loop=0;
    do{
        ret=gprs_at_MoudleIsActive(1);
        if(ret==1)
            break;
        _vDelay(100);
    }while(++loop<5);
#if 0
    ret=gprs_at_ModuleInfo(buf);
    if(ret || strstr(buf, "Ai-Link")==NULL)
    {
        gprs_at_cmd_sndrcv("AT$MYMINISYS=0\r", "OK\r", 2000, 1, 1, NULL, 0);
        
        do{
            ret=gprs_at_MoudleIsActive(1);
            if(ret==1)
                break;
            _vDelay(100);
        }while(++loop<30);
    }
    
    ret=gprs_at_cmd_sndrcv("AT$MYMINISYS?\r", "OK\r", 3000, 1, 1, buf, sizeof(buf)-1);
    
    ret=gprs_at_cmd_sndrcv("at^AIOTARUN=1\r", "OK\r", 2000, 1, 1, NULL, 0);
    if(ret)
    {
        vDispMid(3, "MYMINISYS fail, continue?");
        if(iOK(15)!=1)
            return 1;
        _vDisp(3, "");
    }
    
    while(1)
    {
        ret=gprs_at_cmd_sndrcv(NULL, "^AIOTA:", 10*1000, 1, 1, buf, sizeof(buf)-1);
        if(ret)
        {
            vDispMid(3, "exit?");
            if(iOK(15)!=1)
                return 1;
            _vDisp(3, "");
        }
        if(strstr(buf, "^AIOTA: UPDATE SUCCESS"))
            break;
        if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
            break;
    }

#else

#if 0    
    memset(buf, 0, sizeof(buf));
    ret=gprs_at_ModuleInfo(buf);
    if(ret || strstr(buf, "Ai-Link")==NULL)
    {
        vMessage("非爱联模块,不可OTA");
        return 0;
    }
#endif

    
AL_OTA:    
    _vDisp(2, "");
    _vDisp(3, "");
    loop=0;
    do
    {
        ret=gprs_at_cmd_sndrcv("AT$MYMINISYS?\r", "OK\r", 3000, 1, 1, buf, sizeof(buf)-1);
        if(ret)
        {
            _vDisp(2, "AT$MYMINISYS? fail");
            _vDisp(3, "Continue?");
            if(iOK(30)==1)
            {
                _vDisp(2, "");
                _vDisp(3, "");
                _vDelay(500);
                continue;
            }
            return 1;
        }
        p=strstr(buf, "$MYMINISYS: ");
        _vDisp(2, p);
        if(strstr(buf, "$MYMINISYS: 1, 1") || strstr(buf, "$MYMINISYS: 1, 2"))
            break;
        if(strstr(buf, "$MYMINISYS: 0"))
        {
            _vDisp(3, "enter minisys...");
            ret=gprs_at_cmd_sndrcv("AT$MYMINISYS=1\r", "OK\r", 2000, 1, 1, NULL, 0);
            if(ret && loop)
            {
                _vDisp(4, "wait reset?");
                if(iOK(30)!=1)
                    return 1;
            }
            vDispMid(4, "wait reset...");
            _vDelay(10*100);
            _vDisp(4,"");
            continue;
        }
        _vDelay(500);
    }while(++loop<5);
    
    if(strstr(buf, "$MYMINISYS: 1, 1"))
    {
        vClearLines(3);
        vDispMid(2, "patch1...");
        strcpy(buf, "AT$MYFOTA=1,1,\"http://36.40.67.205:10080/POS_TEST/system_patch.bin_1\",\"\",\"\"\r");
        ret=gprs_at_cmd_sndrcv(buf, "OK\r", 3*1000, 1, 1, NULL, 0);
        if(ret)
            return -1;
        
        loop=0;
        do
        {
            _vDelay(300);
            ret=gprs_at_cmd_sndrcv("AT$MYFOTA?\r", "OK\r", 3*1000, 1, 1, buf, sizeof(buf)-1);
            if(ret)
            {
                vDispMid(3, "MYFOTA1 fail, continue?");
                if(iOK(30)!=1)
                    return 1;
                goto AL_OTA;
            }
            if(strstr(buf, "MYFOTA: 100")!=NULL || strstr(buf, "MYFOTA: 0")!=NULL)
                break;
        }while(++loop<10);
        
        if(strstr(buf, "MYFOTA: 100") || strstr(buf, "MYFOTA: 0"))
        {
            vClearLines(2);
            p=strstr(buf, "MYFOTA: ");
            if(p)
                _vDisp(2, p);
            vDispMid(3, "patch1 ok, up 2?");
            if(iOK(30)!=1)
                return -1;
            ret=gprs_poweron();
            if(ret==1)
                _vDelay(10*100);
            goto AL_OTA;
        }else
        {
            vMessage("patch! FAIL!");
            return 3;
        }
    }
    
    
    if(strstr(buf, "$MYMINISYS: 1, 2"))
    {
        vDispMid(2, "patch2...");
        strcpy(buf, "AT$MYFOTA=1,1,\"http://36.40.67.205:10080/POS_TEST/system_patch.bin_2\",\"\",\"\"\r");
        ret=gprs_at_cmd_sndrcv(buf, "OK\r", 3000, 1, 1, NULL, 0);
        if(ret)
            return -1;
        
        loop=0;
        do
        {
            _vDelay(300);
            ret=gprs_at_cmd_sndrcv("AT$MYFOTA?\r", "OK\r", 3*1000, 1, 1, buf, sizeof(buf)-1);
            if(ret)
            {
                vDispMid(3, "MYFOTA2 fail, continue?");
                if(iOK(30)==1)
                    continue;
                else
                    break;
            }
            if(strstr(buf, "MYFOTA: 100")!=NULL || strstr(buf, "MYFOTA: 0")!=NULL)
                break;
        }while(++loop<30);
        
        vClearLines(2);
        p=strstr(buf, "MYFOTA: ");
        if(p)
            _vDisp(2, p);
        if(strstr(buf, "MYFOTA: 100")!=NULL || strstr(buf, "MYFOTA: 0")!=NULL)
            vDispMid(3, "patch2 OK!");
        else
            vDispMid(3, "patch2 FAIL!");
        iOK(15);
    }
#endif    
    return 0;
}

//爱联OTA: 先下载50%,然后重启到小系统,再继续下载到100%,然后再重启
int iAiLianOTAUp(void *m)
{
    char buf[200+1];
    int ret;
    int loop;
    char *p;
    
    _vCls();
    vDispCenter(1, "爱联OTA", 1);
    vDispMid(2, "开始?");
    if(iOK(30)!=1)
        return 0;
    _vDisp(2, "");
    
    vSetCommSleep(0);
    
    gprs_poweron();
    loop=0;
    do{
        ret=gprs_at_MoudleIsActive(1);
        if(ret==1)
            break;
        _vDelay(100);
    }while(++loop<10);

    ret=gprs_at_ModuleInfo(buf);
    if(ret || strstr(buf, "Ai-Link")==NULL)
    {
        ret=gprs_at_cmd_sndrcv("AT$MYMINISYS?\r", "OK\r", 3000, 1, 1, buf, sizeof(buf)-1);
        if(ret==0 && (p=strstr(buf, "$MYMINISYS:"))!=NULL)
        {
            _vDisp(2, p);
            _vDisp(3, "Continue?");
            if(iOK(30)!=1)
                return 0;
        }
    }

    _vDisp(2, "AIOTARUN...");
    
    ret=gprs_at_cmd_sndrcv("at^AIOTARUN=1\r", "OK\r", 2000, 1, 1, NULL, 0);
    if(ret)
    {
        vDispMid(3, "MYMINISYS fail, continue?");
        if(iOK(15)!=1)
            return 1;
        _vDisp(3, "");
    }
    
    while(1)
    {
        ret=gprs_at_cmd_sndrcv(NULL, "^AIOTA:", 10*1000, 1, 1, buf, sizeof(buf)-1);
        vClearLines(2);
        if(ret)
        {
            vDispMid(3, "continue?");
            if(iOK(15)!=1)
                return 1;
            _vDisp(3, "");
        }
        vDispMul(3, buf);
        p=strstr(buf, "^AIOTA:");
        if(p)
            _vDisp(5, p);
        if(strstr(buf, "^AIOTA: UPDATE SUCCESS"))
            break;
        if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
            break;
        mdelay(100);
    }
   
    return 0;
}

//4G通讯压测
int iLteCommStressTest(void *m)
{
    uchar  data[2048];
    char buf[300]={0};
    int  len, ret;
    uchar crc;
    char errmsg[100];
    int  result;
    int iNumSucc, iNumFail;
    ulong tkEnd;
    int band, rsrp, sinr;
    int i;
    char *p, *p0;
    int rsrpMax=0, rsrpMin=-140, sinrMax=0, sinrMin=0;
    
    
    _vCls();
    _vFlushKey();
    vDispCenter(1, "4G通讯压测", 1);
    
    if(gl_SysInfo.iCommType==VPOSCOMM_WIFI)
    {
        vMessage("请设置为4G通讯方式");
        return 1;
    }
    
    iNumSucc=0;
    iNumFail=0;
    while(1)
    {
        tkEnd=_ulGetTickTime()+20*1000;
        while(1)
        {
            if(iNumSucc+iNumFail!=0)
            {
                if(_ulGetTickTime()>=tkEnd) //超时需发起通讯
                    break;
                ret=iOK(5);
                if(ret==0)       //用户取消
                {
                    if(iGetKeyWithTimeout(1)==_KEY_ESC) //1秒内再次按下取消键
                    {
                        vMessage("退出测试");
                        return 1;
                    }
                }
                if(ret==1)      //确认键立即发起通讯测试
                    break;
            }
            
            vDispMid(3, "等待...");
            band=0;
            rsrp=0;
            sinr=0;
            ret=gprs_at_QFENG(buf);
            if(ret==0 && (p0=strstr(buf, "servingcell"))!=NULL)
            {
                //+QENG: "servingcell",<state>,"LTE",<is_tdd>,<mcc>,<mnc>,<cellid>,<pcid>,<earfcn>,<freq_band_ind>,<ul_bandwidth>,<dl_bandwidth>,<tac>,<rsrp>,<rsrq>,<rssi>,<sinr>,<srxlev> OK
                for(i=0; i<=16; i++)
                {
                    p=strchr(p0, ',');
                    if(p==NULL)
                        break;
                    if(i==9)
                    {
                        *p=0;
                        band=atoi(p0);
                    }
                    if(i==13)
                    {
                        *p=0;
                        rsrp=atoi(p0);
                    }
                    if(i==16)
                    {
                        *p=0;
                        sinr=atoi(p0);
                    }
                    
                    p0=p+1;
                    continue;
                }
            }
            sprintf(buf, "B%d,rsrp%d,sinr%d", band, rsrp, sinr);
            vDispMid(2, buf);
            
            if(iNumSucc+iNumFail==0)
            {
                rsrpMax=rsrp;
                rsrpMin=rsrp;
                sinrMax=sinr;
                sinrMin=sinr;
            }
            if(rsrpMax<rsrp)
                rsrpMax=rsrp;
            if(rsrpMin>rsrp)
                rsrpMin=rsrp;
            if(sinrMax<sinr)
                sinrMax=sinr;
            if(sinrMin>sinr)
                sinrMin=sinr;
            sprintf(buf, "r:%d~%d,s:%d~%d", rsrpMin, rsrpMax, sinrMin, sinrMax);
            vDispMid(5, buf);
            
            if(iNumSucc+iNumFail==0)
                break;
        }        
        
        while(1)
        {
            //联网
            //vDispMid(2, "联网");
            vDispMid(3, "连接服务器...");
            ret=iCommTcpConn("47.106.13.245", "6666", 30*1000);
            if(ret)
            {
                dbg("iCommTcpConn error:%d\n", ret);
                strcpy(errmsg, "连接服务器失败");
                result=1;
                break;
            }

            //格式:STX+cmd[1]+len[2]+data[n]+ETX+CRC[1]
            //cmd A0为测试tcp收发。请求的data前两字节为要求返回报文的数据长度n，响应的data第一个字节为rspcode[1],后面填充n个0xAB
            //memcpy(rcvData, "\x02\xA0\x00\x02\x01\x00\x03\xA2", 8);     //\x01\x00要求返回256字节的数据
            
            len=500;        //发送500字节
            ret=500;        //接收500字节
            
            data[0]=0x02;
            data[1]=0xA0;
            data[2]=len/256;
            data[3]=len%256;
            memset(data+4, 0x00, len);
            data[4+len]=0x03;
            data[4+len+1]=0x00;      //crc,A0指令服务端不做校验
            
            //将data前两字节置为要求返回的长度
            data[4]=ret/256;
            data[5]=ret%256;
            
            vDispMid(3, "发送数据...");
            ret = iCommTcpSend(data, len+6);
            if (ret!=len+6)
            {
                dbg("gprs_at_TcpSnd error:%d\n", ret);
                strcpy(errmsg, "发送失败");
                result=2;
                break;
            }
                    
            vDispMid(3, "接收数据...");
            ret = iCommTcpRecv(data, 4, 30 * 1000);
            if (ret!=4)
            {
                dbg("gprs_at_TcpRcv error:ret=%d, need %d.\n", ret, 4);
                strcpy(errmsg, "接收失败1");
                result=3;
                break;
            }
            len = data[2] * 256 + data[3];
            if (len + 6 > sizeof(data))
            {
                dbg("recv too long.\n");
                strcpy(errmsg, "接收长度错");
                result=4;
                break;
            }
            ret = iCommTcpRecv(data + 4, len + 2, 1000);
            if (ret!=len+2)
            {
                dbg("gprs_at_TcpRcv2 error:ret=%d, need %d.\n", ret, len+2);
                strcpy(errmsg, "接收失败2");
                result=5;
                break;
            }
            
            //校验crc
            crc = data[0];
            for (int i = 1; i < 4+len+2 - 1; i++)
                crc ^= data[i];
            if(crc!=data[4+len+2-1])
            {
                dbg("rcv data crc error. calCRC=%02X, rcvCRC=%02X\n", crc, data[4+len+2-1]);
                strcpy(errmsg, "接收数据校验失败");
                result=6;
                break;
            }
            dbg("recv data ok.\n");
            
            result=0;
            strcpy(errmsg, "成功");
            break;
        }
        iCommTcpDisConn();
        
        //显示结果
        vDispMid(3, errmsg);
        if(result==0)
            ++iNumSucc;
        else
            ++iNumFail;
        sprintf(buf, "成功:%d, 失败:%d", iNumSucc, iNumFail);
        vDispMid(4, buf);
    }
    return 0;
}

