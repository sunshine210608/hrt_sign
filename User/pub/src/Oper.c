#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "VposFace.h"
#include "Pub.h"
#include "Oper.h"
#ifdef APP_LKL
#include "AppGlobal_lkl.h"
#include "MemMana_lkl.h"
#endif
#ifdef APP_SDJ
#include "AppGlobal_sdj.h"
#include "MemMana_sdj.h"
#endif

#if 1//def APP_CJT
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#endif
#include "debug.h"

extern int iMainOperCheck(char *pwd);
extern int iGetNewPin(uchar *pszPrompt, int iPinLen, uchar *pszNewPin);

// 1. 初始化操作员系统环境
// 终端只需要初始化一次
void vOperInit(void)
{
	uchar szOper[OPER_REC_SIZE + 1];
	int i;

	//uiMemManaEraseOperList();

	//初始化增加操作员01-05
	for (i = 0; i < 5; i++)
	{
        sprintf(szOper, "%02d%s", i+1, "0000");
        uiMemManaPutOper(i, szOper);
	}
}

// 2. 列出当前操作员
// 输入参数: iTimeout : 超时时间(秒), 0表示无超时
// 返    回: 0 : 正常返回
//           1 : 超时
//           2 : 取消
// 说    明: 在屏幕上列出当前操作员列表
uint uiOperList(int iTimeout)
{
	int iNoOper; // 1:no oper
	uint uiRet, i;
	uchar sBuf[OPER_REC_SIZE];
	ulong ulTimer;
	int iLine;

	_vCls();
	vDispCenter(1, "操作员列表", DISP_REVERSE);
	iLine = 2;
	iNoOper = 1;
	_vSetTimer(&ulTimer, iTimeout * 100L);
	for (i = 0; i < MAX_OPER_NUM; i++)
	{
		uiMemManaGetOper(i, sBuf);
		if (sBuf[0] == 0 || sBuf[0] == 0xFF)
			continue;
		sBuf[OPER_NO_LEN] = 0;
		iNoOper = 0;
		_vDisp(iLine++, sBuf);
		dbg("_uiGetVLines():%d\n",_uiGetVLines());
		if (iLine > (int)_uiGetVLines())
		{
			while (1)
			{
				if (_uiTestTimer(ulTimer))
					return (1);
				if (_uiKeyPressed())
				{
					uiRet = _uiGetKey();
					if (uiRet == _KEY_ESC)
						return (2);
					break;
				}
			}
			iLine = 2;
			vClearLines(2);
		}
	}
	if (iNoOper)
		vMessage("无操作员");
	if (iLine != 2)
	{
		while (1)
		{
			if (_uiTestTimer(ulTimer))
				return (1);
			if (_uiKeyPressed())
			{
				uiRet = _uiGetKey();
				if (uiRet == _KEY_ESC)
					return (2);
				break;
			}
		}
	}

	return (0);
}

// 3. 检查操作员是否存在
// 输入参数: pszOperNo : 操作员号码
// 返    回: 0         : 无此操作员
//           1         : 该操作员存在
// 说    明: 检查该操作员存在否
uchar ucOperExist(uchar *pszOperNo)
{
	uint i;
	uchar sBuf[OPER_REC_SIZE];
	if (strlen((char *)pszOperNo) != OPER_NO_LEN)
		return (1);
	for (i = 0; i < MAX_OPER_NUM; i++)
	{
		uiMemManaGetOper(i, sBuf);
		if (sBuf[0] == 0 || sBuf[0] == 0xFF)
			continue;
		if (memcmp(pszOperNo, sBuf, OPER_NO_LEN) == 0)
			return (1);
	}
	return (0);
}

// 4. 增加操作员
// 输入参数: iTimeout : 超时时间(秒), 0表示无超时
// 说    明: 增加一个操作员
// 返    回: 0 : 正常返回
//           1 : 超时
//           2 : 取消
uint uiOperAdd(int iTimeout)
{
	uint iIndex;
	uchar sBuf[OPER_REC_SIZE];
	uchar szBuf[OPER_REC_SIZE+1];
	int iRet;
        uchar ucPinPos;
		
	_vCls();
	vDispCenter(1, "增加操作员", DISP_REVERSE);

        if(memcmp(gl_SysData.szCurOper, "00", OPER_NO_LEN) != 0)
        {
		if(iMainOperCheck((char*)gl_SysInfo.szMainOperPwd))
           		 return (1);
		vClearLines(2);
        }	 

	for (iIndex = 0; iIndex < MAX_OPER_NUM; iIndex++)
	{
		uiMemManaGetOper(iIndex, sBuf);
		if (sBuf[0] == 0 || sBuf[0] == 0xFF)
			break;
	}
	if (iIndex >= MAX_OPER_NUM)
	{
		vMessage("操作员已满");
		return (0);
	}

	_vDisp(2,"操作员号:");
	ucPinPos = _uiGetVCols();
	iRet = iInput(INPUT_NORMAL|INPUT_LEFT, 4, ucPinPos, szBuf, OPER_NO_LEN, OPER_NO_LEN, OPER_NO_LEN, iTimeout);
	if (iRet != OPER_NO_LEN)
	{
		if (iRet == -1)
			return (2);
		if (iRet == -3)
			return (1);
		return (2); // 内部错误, 当做被取消
	}

	if (memcmp(szBuf, "00", OPER_NO_LEN) == 0 || memcmp(szBuf, "99", OPER_NO_LEN) == 0)
	{
		vMessage("不允许增加特定人员");
		return (0);
	}

	if (ucOperExist(szBuf))
	{
		vMessage("操作员已存在");
		return (0);
	}

	memset(szBuf + OPER_NO_LEN, '0', OPER_PIN_LEN);
	uiMemManaPutOper(iIndex, szBuf);
	vMessage("操作员加入成功");
	return (0);
}

// 5. 删除操作员
// 输入参数: iTimeout : 超时时间(秒), 0表示无超时
// 返    回: 0 : 正常返回
//           1 : 超时
//           2 : 取消
// 说    明: 删除一个操作员
uint uiOperSub(int iTimeout)
{
	uint iIndex;
	uchar sBuf[OPER_REC_SIZE];
	uchar szBuf[OPER_REC_SIZE];
	int iRet;
        uchar ucPinPos;
		
	_vCls();
	vDispCenter(1, "删除操作员", DISP_REVERSE);

        if(memcmp(gl_SysData.szCurOper, "00", OPER_NO_LEN) != 0)
        {
		if(iMainOperCheck((char*)gl_SysInfo.szMainOperPwd))
           		 return (1);
		vClearLines(2);
        }	
		
	_vDisp(2, "操作员号:");
	ucPinPos = _uiGetVCols();
	iRet = iInput(INPUT_NORMAL|INPUT_LEFT, 4, ucPinPos, szBuf, OPER_NO_LEN, OPER_NO_LEN, OPER_NO_LEN, iTimeout);
	if (iRet != OPER_NO_LEN)
	{
		if (iRet == -1)
			return (2);
		if (iRet == -3)
			return (1);
		ASSERT(0);
		return (2); // 内部错误, 当做被取消
	}

    if(memcmp(gl_SysData.szCurOper, szBuf, OPER_NO_LEN)==0)
    {
        vMessage("已签到操作员不可删除");
		return (0);
    }
    
	for (iIndex = 0; iIndex < MAX_OPER_NUM; iIndex++)
	{
		uiMemManaGetOper(iIndex, sBuf);
		if (memcmp(szBuf, sBuf, OPER_NO_LEN) == 0)
			break;
	}
	if (iIndex >= MAX_OPER_NUM)
	{
		vMessage("无此操作员");
		return (0);
	}

	memset(sBuf, 0xFF, OPER_REC_SIZE);
	uiMemManaPutOper(iIndex, sBuf);
	vMessage("操作员删除成功");
	return (0);
}

// 6. 检查操作员密码
// 输入参数: pszOperNo  : 操作员号码
//           pszOperPwd : 操作员新密码
// 返    回: 0          : OK
//           1          : 错误
// 说    明: 检查操作员密码
uint uiOperCheckPwd(uchar *pszOperNo, uchar *pszOperPwd)
{
	uint iIndex;
	uchar sBuf[OPER_REC_SIZE];

	if (strlen((char *)pszOperNo) != OPER_NO_LEN)
		return (1);
	for (iIndex = 0; iIndex < MAX_OPER_NUM; iIndex++)
	{
		uiMemManaGetOper(iIndex, sBuf);
		if (memcmp(pszOperNo, sBuf, OPER_NO_LEN) == 0)
			break;
	}
	dbg("sBuf111:%s\n",sBuf);
	dbg("pszOperNo:%s\n",pszOperNo);
	if (iIndex >= MAX_OPER_NUM)
    {
        //vMessage("无此操作员");
		return (1); // 无此操作员, 认为密码错误
    }
	if (strlen((char *)pszOperPwd) != OPER_PIN_LEN)
    {
        //vMessage("密码长度错");
		return (1);
    }
	if (memcmp(pszOperPwd, sBuf + OPER_NO_LEN, OPER_PIN_LEN))
		return (1);
	return (0);
}

// 7. 修改操作员密码
// 说    明: 修改操作员密码
uint uiOperChangePwd(void)
{
	uchar szOperNo[10] = {0}, szPin[10] = {0};
	int iRet;
	uint iIndex;
	uchar sBuf[OPER_REC_SIZE];
	uchar ucPinPos;

        _vCls();
	vDispCenter(1, "操作员改密", DISP_REVERSE);		

	if(memcmp(gl_SysData.szCurOper, "00", OPER_NO_LEN) != 0)
        {
		if(iMainOperCheck((char*)gl_SysInfo.szMainOperPwd))
           		 return (1);
		vClearLines(2);
        }	
			
	ucPinPos = _uiGetVCols();
	for (;;)
	{
		vDispVarArg(2, "请输入操作员号:%.*s", OPER_NO_LEN, "    ");
		iRet = iInput(INPUT_NORMAL|INPUT_LEFT, 4, ucPinPos, szOperNo, OPER_NO_LEN, OPER_NO_LEN, OPER_NO_LEN, gl_SysInfo.uiUiTimeOut);
		if (iRet == -1)
			return 1;
		if (iRet == -3)
			return 1;		
	        if (ucOperExist(szOperNo))
	        	break;
			vDispMid(4,"无此操作员"); //
			 iGetKeyWithTimeout(3);
			 vClearLines(4);
	} // for(;;

	vClearLines(2);
   	 _vDisp(2, "请输入原密码:");	 
	ucPinPos = _uiGetVCols();
	iRet = iInput(INPUT_PIN | INPUT_LEFT, 4, ucPinPos, szPin, 4, 4, 4, gl_SysInfo.uiUiTimeOut);
	if (iRet == -1)
		return 1;
	if (iRet == -3)
		return 1;
	dbg("szPin:%s\n",szPin);
	if (uiOperCheckPwd(szOperNo, szPin) != 0)
	{
		vMessage("密码错误");
		return 1;
	}

	memset(szPin,0x00,sizeof(szPin));
        iRet = iGetNewPin((uchar *)"操作员改密", 4, szPin);
	if (iRet != 0)
		return 1;

	for (iIndex = 0; iIndex < MAX_OPER_NUM; iIndex++)
	{
		uiMemManaGetOper(iIndex, sBuf);
		if (memcmp(szOperNo, sBuf, OPER_NO_LEN) == 0)
			break;
	}

	memcpy(sBuf + OPER_NO_LEN, szPin, OPER_PIN_LEN);
	uiMemManaPutOper(iIndex, sBuf);
	
	vMessage("密码修改成功");
	return 0;		
}

