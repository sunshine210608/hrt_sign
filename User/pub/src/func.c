/**************************************
Module name     : VPOS公用程序模块
File name       : PUB.C
Function        : 提供在POS设计中在所有应用中最常用的函数支持
Design          : 该模块只提供最常用,最基本的通用函数.
                  不常用的,有特点的函数不要添加到这个模块.
Based on        : 本模块需要VPOS2.0以上支持
**************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <stdarg.h>
#include <stdlib.h>
#include "VposFace.h"
//#include "VposExt.h"
#include "pub.h"
#include "debug.h"
#include "func.h"
#ifdef APP_LKL
#include "AppGlobal_lkl.h"
#endif
#ifdef APP_SDJ
#include "AppGlobal_sdj.h"
extern void vAppPackPinBlock(uchar *pszCardId, uchar *pszPin, uchar *psKey, int iKeyLen, uchar *psOut);
#endif
#if 1//def APP_CJT
#include "AppGlobal_cjt.h"
#endif

#include "myHttp.h"
#include "tcpcomm.h"
#ifdef REMOTE_GBK
#include "gbkext.h"
#endif
//#ifdef USE_HAL_LIB
//#include "halmain.h"
//#endif

//CY21没有功能键,使用_KEY_FN时用_KEY_UP代替
#ifdef PROJECT_CY21
#undef _KEY_FN
#define _KEY_FN _KEY_UP
#endif

extern int iGetICCID(char *pszICCID);
extern int readMainBoardSN(char *sn);
extern void vDispTermQrCode(int iFlag);
extern int iFactorySimpleTest(void);
extern uint uiMemManaPutTransRec(int iIndex, stTransRec *rec);
extern int iSignDraw(uchar *pszFileName, uchar *pszSpecCode);
extern int iUploadSignFile(int envFlag, int iIdx, stTransRec *recIn);
extern uint uiMemManaIfExist8583(int iFlag);
extern uint uiMemManaPutSysData(void);
extern int iSendFailSignOper(int mode, int *piNum);
extern int  readMCUUID(uchar* uid);
extern uint uiMemManaPutSysInfo(void);
extern void vAppPackPinBlock(uchar *pszCardId, uchar *pszPin, uchar *psKey, int iKeyLen, uchar *psOut);
extern char* _pszGetCLIVer(char *pszVer);

#ifdef _AUTO_TEST 
static int sg_iAutoTestFlag=0;
#endif
int iGetAmount(char *pszTitle, char *pszPrompt, char *pszPromptMsg, char *pszAmount)
{
    char buf[40];
    char szAmount[15];
    int ret;
    int col;
    uint uiInputMode;
#ifdef _AUTO_TEST
    static uint uiAutoAmt=0;
#endif

    if(gl_SysInfo.ucAmtInPutMode==1)
        uiInputMode=INPUT_DOT_MONEY;
    else
        uiInputMode=INPUT_MONEY;
    
    if(pszTitle)
        vDispCenter(1, pszTitle, 1);
    if(pszPrompt)
    {
        //vDispCenter(2, pszPrompt, 0);
        //提示语改为左对齐
        _vDisp(2, (uchar*)pszPrompt);
    }
    
     //col=(_uiGetVCols()-12)/2;
    //金额改为右对齐
    col=_uiGetVCols()-10-2;
    
     //memset(buf, ' ', col);
     //strcpy(buf+col, "[          ]");
     //_vDisp(3, buf);
     if(_uiGetVLines()>=4)
     {
        if(pszPromptMsg==NULL)
            vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
        else
            vDispCenter(_uiGetVLines(), pszPromptMsg, 0);
     }
     memset(buf, ' ', _uiGetVCols());
     strcpy(buf+_uiGetVCols()-2, "元");
     _vDisp(3, (uchar*)buf);
     
#ifdef _AUTO_TEST
    if(iGetAutoTestFlag())
    {
        sprintf(pszAmount, "%u", ++uiAutoAmt);
        sprintf(buf, "%lu.%02lu", uiAutoAmt/100, uiAutoAmt%100);
        _vDispAt(3, col+1, buf);
        _vDelay(100);
        return strlen(pszAmount);
    }
#endif
     if(uiInputMode==INPUT_DOT_MONEY)
        _uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
     while(1)
     {
        ret=iInput(uiInputMode, 3, col, (uchar*)szAmount, 10, 2, 10, gl_SysInfo.uiUiTimeOut);
        if(ret>0 && strlen(szAmount)>9)     //INPUT_DOT_MONEY时输满无小数点自动补00,会造成金额超过ulong
        {
            vDispCenter(4, "金额超限,请重新输入", 0);
            iOK(1);
            if(_uiGetVLines()==4)
            {
                if(pszPromptMsg==NULL)
                    vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
                else
                    vDispCenter(_uiGetVLines(), pszPromptMsg, 0);
            }else
                _vDisp(4, "");
            continue;
        }
        if(ret>0 && atoi(szAmount)==0)     //输入金额0或者直接确认
        {
            vDispCenter(4, "金额不能为0,请重输", 0);
            iOK(1);
            if(_uiGetVLines()==4)
            {
                if(pszPromptMsg==NULL)
                    vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
                else
                    vDispCenter(_uiGetVLines(), pszPromptMsg, 0);
            }else
                _vDisp(4, "");
            continue;
        }
	dbg("ret:%d\n",ret);	
        break;
     }
     if(uiInputMode==INPUT_DOT_MONEY)
        _uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
    if(ret>0)
         strcpy(pszAmount, szAmount);
     if(pszPrompt)
        _vDisp(2, " ");
     _vDisp(3, " ");
     if(_uiGetVLines()>=4)
         _vDisp(_uiGetVLines(), " ");
     return ret;
}

int iMainOperCheck(char *pwd)
{
    int col;
    char szBuf[40];

   if(gl_SysInfo.ucMainOperPwd == 0)
   	return 0;
   	
    vDispCenter(2, "请输入主管密码:", 0);    
    col=(_uiGetVCols()-6)/2;
    memset(szBuf, ' ', col);
    //strcpy(szBuf+col, "[      ]");
    //_vDisp(4, (uchar*)szBuf);
    vDispCenter(_uiGetVLines(), "输错请按[清除]", 0);
    
    szBuf[0]=0;
    if(iInput(INPUT_PIN|INPUT_CENTER_SPACE|INPUT_DOT_SPACE, 3, col+1, szBuf, 6, 6, 6, 30)!=6)
        return -1;
    if(strcmp(pwd, szBuf))
    {
        vMessage("密码错");
        return -1;
    }
    vClearLines(2);
    return 0;
}

void vDispCancelAndConfirm(char cancel, char confirm)
{
    char buf[40];
    
    memset(buf, ' ', _uiGetVCols());
    buf[_uiGetVCols()]=0;
    if(cancel)
        memcpy(buf, "取消", 4);
    if(confirm)
        memcpy(buf+_uiGetVCols()-4, "确认", 4);
    _vDisp(_uiGetVLines(), (uchar*)buf);
    return;
}

int iInputTTC(char *prompt, char *out, unsigned long *pulTTC)
{
    int col;
    unsigned char szBuf[40];
    
    if(prompt)
        _vDisp(2, (uchar*)prompt);    
    col=(_uiGetVCols()-6)/2;
    //memset(szBuf, ' ', col);
    //strcpy((char*)szBuf+col, "[      ]");
    //_vDisp(3, szBuf);
    vDispCancelAndConfirm(1, 1);
    
    szBuf[0]=0;
    while(1)
    {
        if(iInput(INPUT_NORMAL, 3, col, szBuf, 6, 1, 6, 30)<1)
            return -1;

        if(atol((char*)szBuf)==0)
        {
            if(prompt)
                _vDisp(2, "");
			vClearLines(3);
            vMessage("无效凭证号,请重新输入");
            vClearLines(3);
            if(prompt)
                _vDisp(2, (uchar*)prompt);  
			vDispCancelAndConfirm(1, 1);
            continue;
        }
        break;
    }
    if(out)
        strcpy(out, szBuf);
    if(pulTTC)
        *pulTTC=atol((char*)szBuf);

    if(prompt)
        _vDisp(2, "");
    vClearLines(3);
    
    return 0;    
}

//二选一
//ret: -1:取消 -2:超时  1:选1  2:选2
int iSelectOne(char *prompt, char *pszSel1, char *pszSel2, char *promptBottom, int curr, uint uiTimeOut, char bCancel)
{
    ulong ulTimeOut;
    uint uiKey;
    int ret;
    int iLine;
    
    vClearLines(5);
    iLine=2;
    if(prompt)
    {
        _vDisp(iLine++, (uchar*)prompt);
    }
    _vDisp(iLine++, (uchar*)pszSel1);
    _vDisp(iLine++, (uchar*)pszSel2);

    if(curr!=1 && curr!=2)
        _vDisp(iLine, (uchar*)"当前状态:[ ]");
    else
        vDispVarArg(iLine, (uchar*)"当前状态:[%d]", curr);
    iLine++;
    
    if(promptBottom && promptBottom[0] && iLine<=_uiGetVLines())
    {
        if(strlen((char*)promptBottom)>_uiGetVCols())
        {
            _vDisp(_uiGetVLines()-1, (uchar*)promptBottom);
            _vDisp(_uiGetVLines(), (uchar*)promptBottom+_uiGetVCols());
        }else
        {
            _vDisp(_uiGetVLines(), (uchar*)promptBottom);     
        }
    }
    
    if(uiTimeOut)
        _vSetTimer(&ulTimeOut, uiTimeOut*100);
    while(1)
    {
        if(uiTimeOut && _uiTestTimer(ulTimeOut))
        {
            ret=-2;
            break;
        }
        if(_uiKeyPressed())
        {
            uiKey=_uiGetKey();	
            if(bCancel && uiKey==_KEY_ESC)
            {
                ret=-1;
                break;
            }
            if((curr==1 || curr==2) && uiKey==_KEY_ENTER)
            {
                ret=curr;
                break;
            }
            if(uiKey==_KEY_1 || uiKey==_KEY_2)
            {
                ret=uiKey-_KEY_0;
                break;
            }
        }
        _vDelay(10);
    }
    //if(prompt)
    //    _vDisp(2, (uchar*)"");
    vClearLines(2);
    return ret;
}

//压缩卡号为10字节,不足位后补F
void vPackPan(uchar *pszPan, uchar *psOut)
{
    uchar sBuf[20];
    memset(sBuf, 'F', 20);
    memcpy(sBuf, pszPan, strlen((char*)pszPan));
    vTwoOne(sBuf, 20, psOut);
}

void vUnpackPan(uchar *psIn, uchar *pszPan)
{
    uchar szBuf[20+1];
    vOneTwo0(psIn, 10, szBuf);
    rtrimF(szBuf);
    strcpy(pszPan, szBuf);
}

//提示拔卡
void vTakeOutCard(void)
{
    int n=0;
    
    if(_uiTestCard(0)==0)
        return;
    vClearLines(2);
    vDispCenter((_uiGetVLines()-1)/2+1, "请拔卡...", 0);
    
 #ifdef _AUTO_TEST
    if(iGetAutoTestFlag())
    {
        _vDelay(80);
        _uiCloseCard(0);
        return;
    }
#endif    
    _vBuzzer();    
    while(_uiTestCard(0))
    {
        _vDelay(10);
        n++;
        if(n>=10)
        {
            n=0;
            _vBuzzer();
        }
    }
    _uiCloseCard(0);
    return;
}


//提示拔卡
void vTakeOutCardMain(void)
{
    int n=0;
    
    if(_uiTestCard(0)==0)
        return;
    _vCls();
    vDispCenter((_uiGetVLines()-1)/2+1, "请拔卡...", 0);
    
 #ifdef _AUTO_TEST
    if(iGetAutoTestFlag())
    {
        _vDelay(80);
        _uiCloseCard(0);
        return;
    }
#endif    
    _vBuzzer();    
    while( _uiTestCard(0))
    {
        _vDelay(10);
        n++;
        if(n>=10)
        {
            n=0;
            _vBuzzer();
        }
    }
    _uiCloseCard(0);
    return;
}

void vGetBankCardTransName(uint uiTransType, uchar ucVoidFlag, char *pszTrName)
{
    char *buf;
    buf=pszTrName;
    
    buf[0]=0;
    switch (uiTransType)
    {
    case TRANS_TYPE_SALE:
	    if(gl_TransRec.ucMobileFlag == 1)
			strcpy(buf, "手机pay");	
	    else	
	        	strcpy(buf, "消费");
	    if(ucVoidFlag)
	            strcat(buf, "(已撤)");
        break;			
   case TRANS_TYPE_DAIRY_SALE:
        strcpy(buf, "日结消费");
        if(ucVoidFlag)
            strcat(buf, "(已撤)");
        break;		
    case TRANS_TYPE_SALEVOID:
        strcpy(buf, "消费撤销");
        break;
    case TRANS_TYPE_PREAUTH:
        strcpy(buf, "预授权");
        if(ucVoidFlag)
            strcat(buf, "(已撤)");
        break;
    case TRANS_TYPE_PREAUTHVOID:
        strcpy(buf, "预授权撤销");
        break;
    case TRANS_TYPE_PREAUTH_COMP:
        strcpy(buf, "预授权完成");
        if(ucVoidFlag)
            strcat(buf, "(已撤)");
        break;
    case TRANS_TYPE_PREAUTH_COMPVOID:
        strcpy(buf, "预授权完成撤销");
        break;
    case TRANS_TYPE_REFUND:
        strcpy(buf, "退货");
        break;
    default:
        strcpy(buf, "未知交易");
        break;
    }

    return;
}

#define FACTORY_KEY		"98"
extern int endlessloop(void);

#ifdef USE_HAL_LIB
extern char *GetHALVersion(void);
#endif

#if defined(ST7571) || defined(ST7567) ||defined(LCD_GC9106)
//分页浏览
int mViewTermInfoPage(int flag)
{
	//int line;
	char szBuf[50],uid[20];
	ulong ultimer;
	uint uiKey;
    char bCheck;
    int page=1;
    int iMaxPage=4;
    int iMinPage=1;
    int ret;

	_vCls();
	//vDispCenter(1, "终端信息", 0);
	
	//line=1;
    page=iMinPage;
    while(1)
    {
        vDispCenterEx(1, "终端信息", 1, iMinPage, page, iMaxPage);

        switch(page)
        {
        case 1:
        {
            #ifdef APP_LKL
            len=strlen(gl_SysInfo.szMerchName);
            if(len<=_uiGetVCols())
            {
                _vDisp(2, "商户名:");
                _vDisp(3, gl_SysInfo.szMerchName);
            }else
            {
                if(len+7>2*_uiGetVCols())
                {
                    strcpy(szBuf, gl_SysInfo.szMerchName);
                    len=iSplitGBStr(szBuf, _uiGetVCols());
                    szBuf[len]=0;
                    _vDisp(2, szBuf);
                    _vDisp(3, gl_SysInfo.szMerchName+len);
                }else
                {
                    sprintf(szBuf, "商户名:%s", gl_SysInfo.szMerchName);
                    len=iSplitGBStr(szBuf, _uiGetVCols());
                    szBuf[len]=0;
                    _vDisp(2, szBuf);
                    _vDisp(3, gl_SysInfo.szMerchName+len-7);
                }
            }
            
            vDispVarArg(4, "商户号:");
            vDispVarArg(5, gl_SysInfo.szMerchId);
            #else
            vDispVarArg(2, "商户号:");
	    vDispVarArg(3, gl_SysInfo.szMerchId);	
	    vDispVarArg(4, "终端号:");	
            vDispVarArg(5, gl_SysInfo.szPosId);
            #endif
        }
        break;     
        case 2:
        {
            _vDisp(2, "终端序列号:");            
            szBuf[0]=0;
#ifdef TESTTUSN
	    memcpy(szBuf, TESTTUSN, strlen(TESTTUSN));
#else
            _uiGetSerialNo((uchar*)szBuf);
#endif
            _vDisp(3, szBuf);
            
            _vDisp(4, "ICCID:");
            iGetICCID(szBuf);
            _vDisp(5, szBuf);
        }
        break;
        case 3:
        {
#ifdef LCD_GC9106
	    _pszGetCLIVer(szBuf);			
            vDispVarArg(2, "应用:%s", szBuf);
            _pszGetInternalVer(szBuf);
            vDispVarArg(3, "厂商:%s", szBuf);
            _pszGetAppVer(szBuf);
            vDispVarArg(4, "TMS :%s", szBuf);
			#ifdef USE_HAL_LIB
			szBuf[0] = 0;
			strcpy(szBuf, GetHALVersion());
			vDispVarArg(5, "HAL:%s", szBuf);
			#else
            _vDisp(5, "");
			#endif
#else
	    _pszGetCLIVer(szBuf);			
            vDispVarArg(2, "应用版本:%s", szBuf);
            _pszGetInternalVer(szBuf);
            vDispVarArg(3, "厂商版本:%s", szBuf);
            _pszGetAppVer(szBuf);
            vDispVarArg(4, "TMS 版本:%s", szBuf);
			#ifdef USE_HAL_LIB
			strncpy(szBuf, GetHALVersion(), 5);
			szBuf[5] = 0x00;
			vDispVarArg(5, "HAL 版本:%s", szBuf);
			#else
            _vDisp(5, "");
			#endif
#endif			
        }
        break;
        case 4:
        {
            vClearLines(2);
            _vDisp(2, "IMEI:");
            iGetIMEI(szBuf);            
            _vDisp(3, szBuf);
            
            strcpy(szBuf, "cpuid:");
            ret=readMCUUID((uchar*)uid);
            if(ret==0)
            {
                vOneTwo0((uchar*)uid, 16, szBuf+6);
            }
            _vDisp(4, szBuf);
            if(strlen(szBuf)>_uiGetVCols())
                _vDisp(5, szBuf+_uiGetVCols());
            
            //vDispVarArg(2, "内部版本:%s", _pszGetInternalVer(NULL));            
        }
        break;
        default:
        break;
        }
        if(page!=4 && _uiGetVLines()>5)
            vDispCenter(_uiGetVLines(), "[取消]退出 [↑↓]翻页", 0);
        
        _vFlushKey();
        uiKey=0;
        bCheck=0;
        memset(szBuf, 0, sizeof(szBuf));
        _vSetTimer(&ultimer, 30*100);
        while (!_uiTestTimer(ultimer))
        {
            if(_uiKeyPressed())
            {
                _vSetTimer(&ultimer, 30*100);
                uiKey=_uiGetKey();
                #ifdef PROJECT_CY21
                if(uiKey==_KEY_BKSP)
                #else
                if(uiKey==_KEY_FN || uiKey==_KEY_BKSP)
                #endif
                {
                    szBuf[0]=0;
                    bCheck=1;
                    continue;
                }
                if(uiKey==_KEY_ENTER || uiKey==_KEY_DOWN)
                {
                    bCheck=0;
                    if(page<iMaxPage)
                    {
                        page++;
                        break;
                    }
                    continue;
                }
                if(uiKey==_KEY_UP)
                {
                    bCheck=0;
                    if(page>iMinPage)
                    {
                        if(page==iMaxPage)      //最后一屏显示过二维码需清屏
                            _vCls();
                        page--;
                        break;
                    }
                    continue;
                }
                if(uiKey==_KEY_ESC)
                    return 0;
                
                if(bCheck==0)       //没有按Fn则不响应其它键
                    continue;
                
                szBuf[strlen(szBuf)]=uiKey;
                if(memcmp(FACTORY_KEY, szBuf, strlen(szBuf)))
                {
                    bCheck=0;
                    szBuf[0]=0;
                    continue;
                }
                if(strcmp(FACTORY_KEY, szBuf)==0)
                {
                    iFactorySimpleTest();
                    return 0;
                }
            }
        }
        if(_uiTestTimer(ultimer))
            break;
    }
	return 0;
}
#endif
int mViewTermInfo(int flag)
{
#if defined(ST7571) || defined(ST7567) ||defined(LCD_GC9106) 
    mViewTermInfoPage(flag);
#else
    int line;
	char szBuf[50];
	ulong ultimer;
	uint uiKey;
    char bCheck;
    
	_vCls();
	//vDispCenter(1, "终端信息", 0);
	
	line=1;

	if(strlen(gl_SysInfo.szMerchName)+7<=_uiGetVCols())
		vDispVarArg(line++, "商户名:%s", gl_SysInfo.szMerchName);	//商户名称
	else
    	vDispVarArg(line++, "%s", gl_SysInfo.szMerchName);			//商户名称
	vDispVarArg(line++, "商户号:%s", gl_SysInfo.szMerchId);
	vDispVarArg(line++, "银行卡终端号:%s", gl_SysInfo.szPosId);
#ifdef APP_LKL    
	vDispVarArg(line++, "扫码终端号:%s", gl_SysInfo.szQrPosId);   
#endif    
	_uiGetSerialNo((uchar*)szBuf);
    vDispVarArg(line++, "TUSN:%s", szBuf);
    
	//szBuf[0]=0;
	//iGetIMEI(szBuf);		
	//vDispVarArg(line++, "IMEI:%s", szBuf);
    
    szBuf[0]=0;
    iGetICCID(szBuf);
    vDispVarArg(line++, "ICCID:%s", szBuf);

    _pszGetAppVer(szBuf);
    vDispVarArg(line++, "应用版本:%s", szBuf);
	//vDispVarArg(line++, "厂商版本:%s", szBuf);

    //readMainBoardSN(szBuf);
	//vDispVarArg(line++, "主板序号:%s", szBuf);
    vDispVarArg(line++, "  %s %s", __DATE__, __TIME__); 

#if 1
	//3秒内按功能键+"9898"进入装载终端序号界面
	_vFlushKey();
	uiKey=0;
    bCheck=0;
	memset(szBuf, 0, sizeof(szBuf));
	_vSetTimer(&ultimer, 15*100);
	while (!_uiTestTimer(ultimer))
	{
		if(_uiKeyPressed())
		{
            uiKey=_uiGetKey();
            if(/*flag==1 && */uiKey==_KEY_FN && szBuf[0]==0)
            {
                //memset(szBuf, 0, sizeof(szBuf));
                bCheck=1;
                continue;
            }
            if(bCheck==0 && szBuf[0]==0 && (uiKey==_KEY_ENTER || uiKey==_KEY_DOWN))
            {
                vDispTermQrCode(flag);
                break;
            }
            
            if(bCheck==0)
                break;
            
            szBuf[strlen(szBuf)]=uiKey;
            if(memcmp(FACTORY_KEY, szBuf, strlen(szBuf)))
            {
                break;
            }
            if(strcmp(FACTORY_KEY, szBuf)==0)
            {
                iFactorySimpleTest();
                break;
            }
		}
	}
#endif
#endif    
	return 0;
}

int iTermPrintAppVer(void)
{   
    char szBuf[100], szTmp[100];

    vClearLines(2);

	if(_uiPrintOpen())
	return 1;
    
	_uiPrintSetFont(2);

        memset(szTmp,0x00,sizeof(szTmp));
        _pszGetAppVer(szTmp);	
        sprintf(szBuf, "版本号:%s", szTmp);	
	_uiPrintEx(1,szBuf,0);    

	_uiPrint("", 0);
	_uiPrint("", 0);
	_uiPrint("", 0);
	_uiPrint("", 1);   
    	_uiPrintClose();

    return 0;
}

int iTermPrintParam(void)
{   
    char szBuf[100], szTmp[100];

	if(_uiPrintOpen())
	return 1;
    
	_uiPrintSetFont(2);
	_uiPrintEx(2, "POS参数打印", 0);

        memset(szTmp,0x00,sizeof(szTmp));
        _pszGetAppVer(szTmp);	
        sprintf(szBuf, "版本号:%s", szTmp);	
	_uiPrintEx(1,szBuf,0);	
       
	sprintf(szBuf, "终端型号:%s", _pszGetModelName(NULL)); 
	_uiPrintEx(1, szBuf, 0);

	memset(szTmp,0x00,sizeof(szTmp));
	_uiGetSerialNo((uchar*)szTmp);
	sprintf(szBuf, "终端序列号:%s", szTmp); 
	 

	sprintf(szBuf, "商户名称:%s", gl_SysInfo.szMerchName); 
	_uiPrintEx(11, szBuf, 0);

	sprintf(szBuf, "商户编号:%.15s", gl_SysInfo.szMerchId); 
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "终端编号:%s", gl_SysInfo.szPosId);
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "当前批次号:%06lu", gl_SysData.ulBatchNo);
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "当前流水号:%06lu", gl_SysData.ulTTC);
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "打印张数:%d", gl_SysInfo.ucPrinterAttr);
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "最大交易笔数:%d", gl_SysInfo.ucMaxTradeCnt);
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "最大退货金额%lu.%02lu", gl_SysInfo.ulRefund_limit/100, gl_SysInfo.ulRefund_limit%100);
	_uiPrintEx(1, szBuf, 0);

	memset(szTmp,0x00,sizeof(szTmp));
	vOneTwo0(gl_SysInfo.sTPDU, 5, szTmp); 
	sprintf(szBuf, "TPDU:%.10s", szTmp);	
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "通讯方式:%s", "GPRS"); //gl_SysInfo.iCommType 
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "超时时间:%d", gl_SysInfo.uiCommTimeout);
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "交易重发次数:%d", gl_SysInfo.ucCommRetryCount);
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "消费:%s",  (gl_SysInfo.ucSaleSwitch == 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "撤销:%s",  (gl_SysInfo.ucVoidSwitch == 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "退货:%s",  (gl_SysInfo.ucRefundSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);

	sprintf(szBuf, "余额查询:%s",  (gl_SysInfo.ucBalanceSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);	

	sprintf(szBuf, "预授权:%s",  (gl_SysInfo.ucAuthSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);  

	sprintf(szBuf, "预授权撤销:%s",  (gl_SysInfo.ucAuthCancelSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);  

	sprintf(szBuf, "预授权完成请求:%s",  (gl_SysInfo.ucAuthCompleteSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);   

	sprintf(szBuf, "预授权完成通知:%s",  (gl_SysInfo.ucAuthCompletenoticeSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);   

	sprintf(szBuf, "预授权完成撤销:%s",  (gl_SysInfo.ucAuthCompleteVoidSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);   

	sprintf(szBuf, "电子签名:%s",  (gl_SysInfo.ucSignNameSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0);   

	sprintf(szBuf, "扫码交易:%s",  (gl_SysInfo.ucQrSaleSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0); 

	sprintf(szBuf, "扫码退款:%s",  (gl_SysInfo.ucQrRefundSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0); 

	sprintf(szBuf, "扫码撤销:%s",  (gl_SysInfo.ucQrVoidSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0); 

    	sprintf(szBuf, "扫码预授权:%s",  (gl_SysInfo.ucQrPreAuthSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0); 

	sprintf(szBuf, "扫码预授权撤销:%s",  (gl_SysInfo.ucQrPreAuthCancelSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0); 

	sprintf(szBuf, "扫码预授权完成:%s",  (gl_SysInfo.ucQrAuthCompleteSwitch== 1)? "支持":"不支持");
	_uiPrintEx(1, szBuf, 0); 

	_uiPrint("", 0);
	_uiPrint("", 0);
	_uiPrint("", 0);
	_uiPrint("", 1);   
    	_uiPrintClose();

    return 0;
}


int iTermViewParam(void)
{
	int ret;
	uchar ucPinPos;   
        char szBuf[50];
			
	_vCls();
	vDispCenter(1, "POS参数显示", 1);
	_vDisp(2,"终端型号:");
	ucPinPos = _uiGetVCols() - 6;
	_vDispAt(3, ucPinPos, _pszGetModelName(NULL));		
	ret=iGetKeyWithTimeout(15);
	if(ret<0 || ret==_KEY_ESC)
		return -1;
		 
	vClearLines(2);
	_vDisp(2, "序列号:");            
	memset(szBuf,0x00,sizeof(szBuf));
	_uiGetSerialNo((uchar*)szBuf);
	ucPinPos = _uiGetVCols() - strlen(szBuf);
	_vDispAt(3, ucPinPos,szBuf);
	ret=iGetKeyWithTimeout(15);
	if(ret<0 || ret==_KEY_ESC)
		return -1;

	vClearLines(2);
	_vDisp(2, "版本号:");    
	memset(szBuf,0x00,sizeof(szBuf));		 
	_pszGetAppVer(szBuf);
	ucPinPos = _uiGetVCols() - strlen(szBuf);
	_vDispAt(3, ucPinPos,szBuf); 
	ret=iGetKeyWithTimeout(15);
	if(ret<0 || ret==_KEY_ESC)
		return -1;
			
        vClearLines(2);
	_vDisp(2, "商户名称:");
	ucPinPos = _uiGetVCols() - strlen( gl_SysInfo.szMerchName);
	_vDispAt(3, ucPinPos,gl_SysInfo.szMerchName);
	    ret=iGetKeyWithTimeout(15);
   	 if(ret<0 || ret==_KEY_ESC)
        	return -1;

	vClearLines(2);
	_vDisp(2, "商户编号:");
	ucPinPos = _uiGetVCols() - strlen( gl_SysInfo.szMerchId);
	_vDispAt(3, ucPinPos,gl_SysInfo.szMerchId);
	ret=iGetKeyWithTimeout(15);
	if(ret<0 || ret==_KEY_ESC)
	return -1;

	 vClearLines(2);
	_vDisp(2, "终端编号:");
	ucPinPos = _uiGetVCols() - strlen( gl_SysInfo.szPosId);
	_vDispAt(3, ucPinPos,gl_SysInfo.szPosId);
	ret=iGetKeyWithTimeout(15);
	if(ret<0 || ret==_KEY_ESC)
	return -1;
	
	vClearLines(2);
	_vDisp(2, "当前批次号:");
	memset(szBuf,0x00,sizeof(szBuf));
	sprintf(szBuf, "%06lu", gl_SysData.ulBatchNo);	
	ucPinPos = _uiGetVCols() - strlen( szBuf);
	_vDispAt(3, ucPinPos,szBuf);
	ret=iGetKeyWithTimeout(15);
	if(ret<0 || ret==_KEY_ESC)
		return -1;

	vClearLines(2);
	_vDisp(2, "当前批流水号:");
	memset(szBuf,0x00,sizeof(szBuf));
	sprintf(szBuf, "%06lu", gl_SysData.ulTTC);	
	ucPinPos = _uiGetVCols() - strlen( szBuf);
	_vDispAt(3, ucPinPos,szBuf);
	ret=iGetKeyWithTimeout(15);
	if(ret<0 || ret==_KEY_ESC)
		return -1;
	return 0;
}

#ifdef APP_LKL
extern void vLklPackPinBlock(uchar *pszCardId, uchar *pszPin, uchar *psKey, int iKeyLen, uchar *psOut);
#endif

//pszPan为null时, pszEncPin输出为明文pin，否则输出密文pin
// -1：取消 -3：超时 >=0:密码长度(0为直接按确认键)
int iInputPinAndEnc(uchar ucPass, unsigned long ulAmount, char *pszPrompt, char *pszPan, char *pszEncPin)
{
    char szTmp[50], szPin[12+1],Nbuff[12+1];
    int iRet;
    int offset=0;
    int col;
    int timeout=gl_SysInfo.uiUiTimeOut;
    uchar ucKeyIn;                                
    uchar nMode;
    int InputCnt;
    ulong ulTimer;
	
#ifdef ST7789
    offset=1;
#endif
    vClearLines(2);
    if(ulAmount)
    {
        sprintf(szTmp, "金额:%lu.%02lu元", ulAmount / 100, ulAmount % 100);
        vDispCenter(2, szTmp, 0);
    }
    if(pszPrompt)
        vDispCenter(3+offset, pszPrompt, 0);
    //_vDisp(4+offset, "    [      ]");
    //col=(_uiGetVCols()-6)/2;
    //memset(szTmp, ' ', col);
    //strcpy(szTmp+col, "[      ]");
    //_vDisp(4+offset, (uchar*)szTmp);
    _vDisp(4+offset, "");

#ifdef _AUTO_TEST
    if(iGetAutoTestFlag())
    {
        timeout=1;
        strcpy(szPin, "123456");
    }
#endif
        
    if(ucPass)      //允许跳过
        vDispCenter(_uiGetVLines(), "无密码请按[确认]", 0);
    szPin[0] = 0;

	_vFlushKey();
	nMode = 0xaa;
	InputCnt = 0;
	memset(Nbuff,0x00,sizeof(Nbuff));
	memset(szPin,0x00,sizeof(szPin));
	_vSetTimer(&ulTimer, timeout * 100L); // 设置超时时间
	  while(1)
    	{
		if(nMode)
		{
			nMode=0x00;
			_vDisp(4," ");
			col =(_uiGetVCols()-6)/2;			
			if(InputCnt == 0)
			{			       
				_vDispAt(4, col, "_");
			}	
			else 
			{					
				_vDispAt(4,col,Nbuff);
			}					
		}
		if (_uiTestTimer(ulTimer))
		{
		        iRet = -3;      // 超时
			break;
		}
    		if(_uiKeyPressed())
		{
			ucKeyIn = (uchar)_uiGetKey();
			if(_KEY_ENTER== ucKeyIn)
			{		
			        iRet = InputCnt;
			        if(ucPass && InputCnt == 0)
					break;
			        else if (InputCnt == 6)
					break;	
			}
			else if(_KEY_CANCEL== ucKeyIn)
			{
			       iRet = -1;
				break;
			}
			else if((ucKeyIn >= _KEY_0)&&(ucKeyIn <= _KEY_9)&&(InputCnt < 6))
			{
				nMode=0xaa;
				InputCnt++;	
				szPin[InputCnt-1] = ucKeyIn;
				Nbuff[InputCnt-1] = '*';	
				_vSetTimer(&ulTimer, timeout * 100L); // 设置超时时间
			}
			else if(_KEY_BKSP== ucKeyIn && InputCnt > 0)
			{
				nMode=0xaa;	
				szPin[InputCnt-1] = 0;
				Nbuff[InputCnt-1] = 0;
				InputCnt--;
			}
			else
			{
				nMode=0x00;
			}
		}
    	}

	dbg("Nbuff:%s\n",Nbuff);
	dbg("szPin:%s\n",szPin);

    vClearLines(2);
    
#ifdef _AUTO_TEST  
    if(iGetAutoTestFlag())
    {
        iRet=6;
        strcpy(szPin, "123456");
    }
#endif  
    
    if(iRet<0)
        return iRet;

    if(pszPan==NULL)
    {
        //输出明文pin
        strcpy(pszEncPin, szPin);
        return iRet;
    }

    //加密pin
    if(iRet)
    {
#ifdef APP_SDJ        
        vAppPackPinBlock(pszPan, szPin, gl_SysData.sPinKey, 16, szTmp);
#endif
#ifdef APP_LKL
        vLklPackPinBlock(pszPan, szPin, gl_SysData.sPinKey, 16, szTmp);
#endif    
#ifdef APP_CJT 
        vAppPackPinBlock(pszPan, szPin, gl_SysData.sPinKey, 16, szTmp);
#endif
	if(gl_SysInfo.ucSmFlag == 0)
        	vOneTwo0(szTmp, 8, pszEncPin);
	else if(gl_SysInfo.ucSmFlag == 1)
		vOneTwo0(szTmp, 16, pszEncPin);
    }else
    {
        //pin长度为0
        pszEncPin[0]=0;
    }
    return iRet;
}

//flag:1-交易结束时调用 0-补签调用  
int iBillPreSign(int flag,void *pRec)
{
    uchar szFileName[30];
    uchar szCode[20], buf[10];
    int i, ret;    
    stTransRec *rec;
    
    rec=(stTransRec *)pRec;
   
#if 0//PROJECT_CY21  == 1     
    {
        if(rec->ucTransAttr&0x02)
        {
            rec->ucTransAttr&=(0xFF-0x02);
            uiMemManaPutTransRec(iRecIdx, rec);
            dbg("rec->ucTransAttr:%02X", rec->ucTransAttr);
        }
        return 0;
    }
#endif    

#ifdef ENABLE_NFCLED
    ret=(rec->uiEntryMode%100)/10;
    if(flag && ret!=2 && ret!=5)        //非接交易,led绿灯亮1秒
    {
        //vClearLines(2);
        vDispMid(3, "正在处理中...");
        _vDelay(100);               //交易完成后关闭NFC前使led灯亮1秒以上
        _uiCloseCard(8);
        _vDisp(_uiGetVLines()/2, "");
    }
#else
    vDispMid(3, "正在处理中...");
    _uiCloseCard(8);
#endif
    
#ifdef _AUTO_TEST
    if(iGetAutoTestFlag())
    {
        vClearLines(2);
        _vDisp(3, "自动测试,不做签名");
        
        //当前交易改为免签免上传
        rec->ucTransAttr&=(0xFF-0x02);
        rec->ucSignupload=0xFF;              //无需上传签名   
        return 0;
    }
#endif    
    
    //允许免签+额度+交易类型
#if 1//def APP_SDJ    
    if(flag && gl_SysInfo.ucNoSignFlag && rec->ulAmount<=gl_SysInfo.ulNoSignLimit && rec->ucUploadFlag==0xFF && rec->uiEntryMode/10==7
		&& (rec->uiTransType == TRANS_TYPE_SALE || rec->uiTransType == TRANS_TYPE_DAIRY_SALE || rec->uiTransType == TRANS_TYPE_PREAUTH))
#elif APP_LKL    
    if(flag && gl_SysData.ucNonSignNonPinFlag && gl_SysData.ucForceSign==0 && rec->ulAmount<=gl_SysData.ulNonSignNonPinLimit && rec->ucUploadFlag==0xFF && (rec->uiEntryMode%100)/10==7)
	        && (rec->uiTransType==TRANS_TYPE_SALE || rec->uiTransType==TRANS_TYPE_PREAUTH))
#endif    
    {   
        if((rec->ucTransAttr&0x02) || (rec->ucSignupload!=0xFF))
        {
            dbg("set no sign.\n");
            //设置免签
            rec->ucTransAttr&=(0xFF-0x02);
            rec->ucSignupload=0xFF;              //无需上传签名
        }
        return 0;
    }
    
    vOneTwo0(rec->sDateTime+1, 2, szFileName);
    sprintf((char*)szFileName+4, "_%06lu.jbg", rec->ulTTC);

    if(rec->ucUploadFlag==0xFF)
    {
        //联机交易: 15域清算日期[4]+37域参考号[12]
        memset(szCode, '0', 16);
        szCode[16]=0;
     vOneTwo(rec->sDateTime+1, 2, szCode);
    //if(rec->sReferenceNo[0])
    //    memcpy(szCode+4, rec->sReferenceNo, 12);
    sprintf(szCode+4, "%06lu%06lu", gl_SysData.ulBatchNo,rec->ulTTC);
    }else{
        //脱机交易：批次号[6]+ttc[6]+0000 (目前脱机交易无需签名)
        sprintf(szCode, "%06lu%06lu0000", gl_SysData.ulBatchNo, rec->ulTTC);
    }

   dbg("code=[%.16s]\n", szCode);	
    //前后8字节,压缩BCD,异或,再转16进制字符串
    vTwoOne(szCode, 16, buf);
    for(i=0; i<4; i++)
        buf[i]^=buf[4+i];
    vOneTwo0(buf, 4, szCode);
    dbg("speccode=[%.8s]\n", szCode);
	 
    #ifdef APP_CJT  
    if(flag==1)
    {
        gl_SysData.ucFailSignNum++;         //先增加失败电签笔数,成功上送后减去
        uiMemManaPutSysData();
    }
    #endif
    
    //dbg("before iSignDraw: rec->ucTransAttr=%02X\n", rec->ucTransAttr);
    /*
    do{
        ret=iSignDraw(szFileName, szCode);
#ifdef APP_CJT
        if(ret)
        {
            _vBuzzer();
            vDispCenter(3, "交易成功", 0);
            vDispCenter(4, "请在屏幕上签名", 0);
            _vDelay(100);
        }
#else
        break;
#endif        
    }while(ret!=0);
 */   
    ret=iSignDraw(szFileName, szCode); 
    if(ret)
    {
        dbg("iSignDraw ret=%d\n", ret);
        //rec->ucTransAttr|=0x02;
        //rec->ucTransAttr|=0x04;  //b3置位,需要纸签
        //uiMemManaPutTransRec(iRecIdx, rec);
        
        dbg("after iSignDraw fail: rec->ucTransAttr=%02X\n", rec->ucTransAttr);
       
        if(ret<0 && flag)
        {
#ifdef ENABLE_PRINTER
            vDispCenter(3, "电子签名超时或取消", 0);
            vDispCenter(4, "请在小票上签字", 0);
#else
            vDispCenter(3, "签名超时", 0);
            vDispCenter(4, "请上送失败电签时重签", 0);
#endif            
            iGetKeyWithTimeout(5);

	    return ret;
        }
    }else
    {
        vDispMid(3, "正在处理中...");
        
        rec->ucTransAttr&=(0xFF-0x04);       //电签成功,取消需纸签标志
          
        dbg("after iSignDraw succ: rec->ucTransAttr=%02X\n", rec->ucTransAttr);
        if(flag)
        {
            //上送
            if(rec->ucUploadFlag==0xFF)          //联机交易实时上送电签
            {
                ret=iUploadSignFile(0, -1, rec);
		gl_ucSignuploadFlag = 0;		
#ifndef APP_CJT				
                if(ret==0 && gl_SysData.ucFailSignNum)
                {
                    iSendFailSignOper(1, NULL);
                }
    #endif
            }else
                dbg("offline trans, no uploadsignfile.\n");
        }
    }

    return ret;
}

//flag:1-交易结束时调用 0-补签调用  
int iBillSign(int flag, int iRecIdx, void *pRec)
{
    uchar szFileName[30];
    uchar szCode[20], buf[10];
    int i, ret;    
    stTransRec *rec;
    
    if(iRecIdx<0 || pRec==NULL)
    {
        iRecIdx=gl_SysData.uiTransNum-1;
        rec=&gl_TransRec;
    }else
        rec=(stTransRec *)pRec;
   
#if 0//PROJECT_CY21  == 1     
    {
        if(rec->ucTransAttr&0x02)
        {
            rec->ucTransAttr&=(0xFF-0x02);
            uiMemManaPutTransRec(iRecIdx, rec);
            dbg("rec->ucTransAttr:%02X", rec->ucTransAttr);
        }
        return 0;
    }
#endif    

#ifdef ENABLE_NFCLED
    ret=(rec->uiEntryMode%100)/10;
    if(flag && ret!=2 && ret!=5)        //非接交易,led绿灯亮1秒
    {
        //vClearLines(2);
        vDispMid(3, "正在处理中...");
        _vDelay(100);               //交易完成后关闭NFC前使led灯亮1秒以上
        _uiCloseCard(8);
        _vDisp(_uiGetVLines()/2, "");
    }
#else
    vDispMid(3, "正在处理中...");
    _uiCloseCard(8);
#endif
    
#ifdef _AUTO_TEST
    if(iGetAutoTestFlag())
    {
        vClearLines(2);
        _vDisp(3, "自动测试,不做签名");
        
        //当前交易改为免签免上传
        rec->ucTransAttr&=(0xFF-0x02);
        rec->ucSignupload=0xFF;              //无需上传签名
        uiMemManaPutTransRec(iRecIdx, rec);
    
        return 0;
    }
#endif    
    
    //允许免签+额度+交易类型
#if 1//def APP_SDJ    
    if(flag && gl_SysInfo.ucNoSignFlag && rec->ulAmount<=gl_SysInfo.ulNoSignLimit && rec->ucUploadFlag==0xFF && rec->uiEntryMode/10==7
		&& (rec->uiTransType == TRANS_TYPE_SALE || rec->uiTransType == TRANS_TYPE_DAIRY_SALE || rec->uiTransType == TRANS_TYPE_PREAUTH))
#elif APP_LKL    
    if(flag && gl_SysData.ucNonSignNonPinFlag && gl_SysData.ucForceSign==0 && rec->ulAmount<=gl_SysData.ulNonSignNonPinLimit && rec->ucUploadFlag==0xFF && (rec->uiEntryMode%100)/10==7)
	        && (rec->uiTransType==TRANS_TYPE_SALE || rec->uiTransType==TRANS_TYPE_PREAUTH))
#endif    
    {   
        if((rec->ucTransAttr&0x02) || (rec->ucSignupload!=0xFF))
        {
            dbg("set no sign.\n");
            //设置免签
            rec->ucTransAttr&=(0xFF-0x02);
            rec->ucSignupload=0xFF;              //无需上传签名
            uiMemManaPutTransRec(iRecIdx, rec);
        }
        return 0;
    }
    
    vOneTwo0(rec->sDateTime+1, 2, szFileName);
    sprintf((char*)szFileName+4, "_%06lu.jbg", rec->ulTTC);

    if(rec->ucUploadFlag==0xFF)
    {
        //联机交易: 15域清算日期[4]+37域参考号[12]
        memset(szCode, '0', 16);
        szCode[16]=0;
        vOneTwo(rec->sSettleDate, 2, szCode);
        if(rec->sReferenceNo[0])
            memcpy(szCode+4, rec->sReferenceNo, 12);
    }else{
        //脱机交易：批次号[6]+ttc[6]+0000 (目前脱机交易无需签名)
        sprintf(szCode, "%06lu%06lu0000", gl_SysData.ulBatchNo, rec->ulTTC);
    }
    //前后8字节,压缩BCD,异或,再转16进制字符串
    vTwoOne(szCode, 16, buf);
    for(i=0; i<4; i++)
        buf[i]^=buf[4+i];
    vOneTwo0(buf, 4, szCode);
    
    //预先保存,防止等待电签时断电
    if((rec->ucTransAttr&0x06)!=0x06)
    {
        rec->ucTransAttr|=0x06;     //b2需要签名,b3电签不成功/需纸签
        uiMemManaPutTransRec(iRecIdx, rec);
    }
    #ifdef APP_CJT  
    if(flag==1)
    {
        gl_SysData.ucFailSignNum++;         //先增加失败电签笔数,成功上送后减去
        uiMemManaPutSysData();
    }
    #endif
    
    //dbg("before iSignDraw: rec->ucTransAttr=%02X\n", rec->ucTransAttr);
    /*
    do{
        ret=iSignDraw(szFileName, szCode);
#ifdef APP_CJT
        if(ret)
        {
            _vBuzzer();
            vDispCenter(3, "交易成功", 0);
            vDispCenter(4, "请在屏幕上签名", 0);
            _vDelay(100);
        }
#else
        break;
#endif        
    }while(ret!=0);
 */   
    ret=iSignDraw(szFileName, szCode); 
    if(ret)
    {
        dbg("iSignDraw ret=%d\n", ret);
        //rec->ucTransAttr|=0x02;
        //rec->ucTransAttr|=0x04;  //b3置位,需要纸签
        //uiMemManaPutTransRec(iRecIdx, rec);
        
        dbg("after iSignDraw fail: rec->ucTransAttr=%02X\n", rec->ucTransAttr);
       
        if(ret<0 && flag)
        {
#ifdef ENABLE_PRINTER
            vDispCenter(3, "电子签名超时或取消", 0);
            vDispCenter(4, "请在小票上签字", 0);
#else
            vDispCenter(3, "签名超时", 0);
            vDispCenter(4, "请上送失败电签时重签", 0);
#endif            
            iGetKeyWithTimeout(5);

	    return ret;
        }
    }else
    {
        vDispMid(3, "正在处理中...");
        
        rec->ucTransAttr&=(0xFF-0x04);       //电签成功,取消需纸签标志
        uiMemManaPutTransRec(iRecIdx, rec);
        
        dbg("after iSignDraw succ: rec->ucTransAttr=%02X\n", rec->ucTransAttr);
        if(flag)
        {
            //上送
            if(rec->ucUploadFlag==0xFF)          //联机交易实时上送电签
            {
                ret=iUploadSignFile(0, iRecIdx, rec);
		gl_ucSignuploadFlag = 0;		
#ifndef APP_CJT				
                if(ret==0 && gl_SysData.ucFailSignNum)
                {
                    iSendFailSignOper(1, NULL);
                }
    #endif
            }else
                dbg("offline trans, no uploadsignfile.\n");
        }
    }

    return ret;
}

void vIncTTC(void)
{
    if (++gl_SysData.ulTTC > 999999L)
        gl_SysData.ulTTC = 1;
    uiMemManaPutSysData();
}

//Field60/62定义为b型,实际很多时候按ans使用,当按ans使用时,此处填入前两字节长度
void vFillFieldLen(uchar *psField)
{
    int len=strlen((char *)psField+2);
    
    psField[0]=len/256;
    psField[1]=len%256;
}

void vQrPayModeName(char ucSimpleFlag, char *pszPayMode, char *pszName)
{
    if(strcmp(pszPayMode, "WECHAT")==0)
        strcpy(pszName, "微信");
    else if(strcmp(pszPayMode, "ALIPAY")==0)
        strcpy(pszName, "支付宝");
    else if(strcmp(pszPayMode, "UQRCODEPAY")==0)
    {
        if(ucSimpleFlag)
            strcpy(pszName, "银联码");
        else
            strcpy(pszName, "银联二维码");
    }else if(strcmp(pszPayMode, "BESTPAY")==0)
        strcpy(pszName, "翼支付");
    else if(strcmp(pszPayMode, "SUNING")==0)
    {
        if(ucSimpleFlag)
            strcpy(pszName, "苏宁");
        else
            strcpy(pszName, "苏宁易付宝");
    }else
        strcpy(pszName, "扫一扫");
    return;
}

void vShowCommErrMsg(int code)
{
    char buf[40];
    
    if(code<0)
        code=-1*code;
    
    buf[0]=0;
    switch(code)
    {
        case COM_PARAMETER:     //解析域名失败
            buf[0]=0;       //已有提示信息
            break;
        case COM_ERR_NO_CONNECT:
            strcpy(buf, "连接服务器失败");
            break;
        case COM_ERR_SEND:
            strcpy(buf, "发送报文失败");
            break;
        case COM_ERR_RECV:
            strcpy(buf, "接收报文失败");
            break;
        case COM_ERR_RECV_HTTPHEAD:
            strcpy(buf, "http响应报文错");
            break;
        case COM_ERR_PACK:
            strcpy(buf, "报文打包失败");
            break;
        case COM_ERR_UNPACK:
            strcpy(buf, "报文解包失败");
            break;
        case COM_ERR_ENCRYPT:
            //strcpy(buf, "报文加密或签名失败");
            buf[0]=0;       //已有提示信息
            break;
        default:
            strcpy(buf, "通讯失败");
            break;
    }
    if(buf[0])
    {
        vClearLines(2);
        vMessage(buf);
    }
}

int iInputIP(char *pszPrompt, char *pszIP)
{
	int ret;
	char szTmp[20];

    pszIP[0]=0;
	vClearLines(2);
	if(pszPrompt)
        _vDisp(2, (uchar*)pszPrompt);
    
	_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
	ret=iInput(INPUT_ALL_CHAR, 3, 6, (uchar *)szTmp, 15, 2, 15, 30);
	_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	if(ret>0)
	{
		strcpy(pszIP, szTmp);
		return 0;
	}else
	{
		dbg("iInput return = %d\n", ret);
		//vDispVarArg(5, "input fail: [%d]", ret);
		return -1;
	}
}

//检查IP地址是否合法
//0-合法 1-非法
int iCheckIpFormat(char *pszIP)
{
    char ip[15+1];
    char *p, *p1;
    int i;
    int  num;
    
    if(pszIP==NULL || strlen(pszIP)<7 || strlen(pszIP)>15)
        return 1;
    strcpy(ip, pszIP);
    
    //检查是否有非法字符
    for(i=0; i<strlen(ip); i++)
    {
        if(ip[i]=='.' || (ip[i]>='0' && ip[i]<='9'))
            continue;
        else
            return 1;
    }
    p=ip;
    for(i=0; i<4; i++)
    {
        
        p1=strchr(p, '.');
        if(i<3)
        {
            if(p1==NULL)
                return 1;
            *p1=0;
        }else if(p1)        //i==3, 有多余的'.'号
        {
            return 1;
        }

        if(strlen(p)<=0 || strlen(p)>3)
            return 1;
        num=atoi(p);
        if(num<0 || num>255 || (i==0 && num==0))
            return 1;
        p=p1+1;
    }
    
    return 0;
}

//解析url，获取域名并显示修改IP
int iParseUrlAndSetIP(char *pszPrompt, int iParselFlag, char *pszUrl, char *pszIP)
{
	int ret;
    char szHostName[50], *pszHost;
	char szTmp[20];
    uint uiPort;
    uchar ucMinLen;

    pszIP[0]=0;
    szTmp[0]=0;
    
	vClearLines(2);
	if(pszPrompt)
        _vDisp(2, (uchar*)pszPrompt);
    
    szHostName[0]=0;
    if(iParselFlag)
    {
        iParseHttpUrl(pszUrl, szHostName, NULL, &uiPort);
        pszHost=szHostName;
    }else if(pszUrl)
    {
        pszHost=pszUrl;
    }
    
    if(pszHost[0])
    {
        iGetHostByName(pszHost, szTmp);
    }
    
    ucMinLen=8;
    if(szTmp[0]==0)
        ucMinLen=0;
	_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
    while(1)
    {
        ret=iInput(INPUT_ALL_CHAR|INPUT_INITIAL, 3, 6, (uchar *)szTmp, 15, ucMinLen, 15, 30);
        if(ucMinLen==0 && ret==0)
            break;
        if(ret<0)
            break;
        if(ret>0 && iCheckIpFormat(szTmp))
        {
            vMessageEx("IP地址非法", 100);
            _vDisp(_uiGetVLines(), "");
            continue;
        }
        break;
    }
	_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	if(ret>0)
	{
		strcpy(pszIP, szTmp);
		return 0;
	}else
	{
		dbg("iInput return = %d\n", ret);
		//vDispVarArg(5, "input fail: [%d]", ret);
		return -1;
	}
}

//自动测试标志
int iGetAutoTestFlag(void)
{
#ifdef _AUTO_TEST    
    return sg_iAutoTestFlag;
#else
    return 0;
#endif    
}

void vSetAutoTestFlag(int flag)
{
#ifdef _AUTO_TEST
    sg_iAutoTestFlag=flag;
#endif    
}

#ifdef REMOTE_GBK
#define GBK_PKG_NUM   10        //GBK每包最大汉字数
extern int executecmd_ex(char cmd,int timeout,unsigned char *cmddata, int cmdlen,unsigned char *res,int *rlen);
extern int TMS_Connect(void);
extern int TMS_Disconnet(void);
int iGetSvrGBKFont(unsigned char *hz)
{
    int ret;
    uchar rcv[GBKFILE_LINESIZE*GBK_PKG_NUM*2+100];
    int  len, rcvLen=0;
    
    if(hz==NULL)
        return -1;
    len=strlen((char*)hz);
    if(len<2 || len%2!=0)
        return -1;
    
    vDispMid(3, "处理中,请稍候...");
    ret=TMS_Connect();
    if(ret==0)
    {
        ret=executecmd_ex(0xA2, 5*1000, hz, len, rcv, &rcvLen);
        TMS_Disconnet();
    }
    vDispMid(3, "");
    
    if(ret)
        return -2;

    dbgHex("rcv", rcv, rcvLen);
            
    if(rcv[0]!=0 || rcvLen-1<GBKFILE_LINESIZE || ((rcvLen-1)%GBKFILE_LINESIZE)!=0)
    {
        dbg("gbg rcv len err. len=%d.\n", rcvLen-1);
        return -3;
    }
    
    ret=iSaveGBKFonts(rcv+1, rcvLen-1);
    if(ret)
    {
        dbg("save font err:%d\n", ret);
        return -4;
    }

    return 0;
}

int iGetStringGBK(unsigned char *pszStr, int iStrLen)
{
    uchar *p;
    int ret;
    char szGbkHz[GBK_PKG_NUM*2+1]={0};
    char tmp[3];
    
    if(iStrLen<0)
        iStrLen=strlen((char*)pszStr);
    
    
    for(p=pszStr; p<pszStr+iStrLen; )
    {
        ret=iGetCharType(p);
        if(ret==0)  //acscii
        {
            p++;
            continue;
        }
        if(ret!=2)  //gb2312
        {
            p+=2;
            continue;
        }
        
        //GBK ret==2
        //检查是否有字模
        ret=iGetGBKFontDat(p, NULL, NULL);
        if(ret<0)
            return ret;
        
        //需取字模
        memcpy(tmp, p, 2);
        tmp[2]=0;
        if(ret>0 && strstr(szGbkHz, tmp)==NULL)         //strstr防止取重复字
        {
            strcat(szGbkHz, tmp);
            if(strlen(szGbkHz)>=GBK_PKG_NUM*2)     //每GBK_PKG_NUM个gbk字联网取一次
            {
                ret=iGetSvrGBKFont((uchar*)szGbkHz);
                if(ret)
                {
                    return ret;
                }
                memset(szGbkHz, 0, sizeof(szGbkHz));
            }
        }
        
        p+=2;
    }
    if(strlen(szGbkHz)>0)
    {
        ret=iGetSvrGBKFont((uchar*)szGbkHz);
        if(ret)
            return ret;
    }
    
    return 0;
}

int iTestGetGBK(void *m)
{
    int ret;
    //uchar str[]="测试gbk峣昇获取";
    uchar str[]="啰瞭墈峣嵎惇獴玕玠玦疍眬祼窸箓袪萩赟鹮昇，测试gbk峣昇获取。";

    _vCls();
    ret=iGetStringGBK(str, -1);
    vDispMid(2, str);
    vMessageVarArg("GetStringGBK:%d", ret);
    
    return 0;
}
#endif
