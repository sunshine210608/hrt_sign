/**************************************
Module name     : VPOS公用程序模块
File name       : PUB.C
Function        : 提供在POS设计中在所有应用中最常用的函数支持
Design          : 该模块只提供最常用,最基本的通用函数.
                  不常用的,有特点的函数不要添加到这个模块.
Based on        : 本模块需要VPOS2.0以上支持
**************************************/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>

#include "user_projectconfig.h"
#include "lcdgui.h"
#include "debug.h"

#include "VposFace.h"
//#include "VposExt.h"
#include "pub.h"
#include "nfcImg.h"
#include "AppGlobal_cjt.h"
#define MESSAGE_MID         //show和message显示在屏幕中部

//#define DEFAULT_INPUT_FILL '_' // iInput()函数被调用时输入区域缺省的填充字符
#define DEFAULT_INPUT_FILL ' ' // iInput()函数被调用时输入区域缺省的填充字符
#define MAX_INPUT_LENGTH 85    // iInput()函数输入区域的最大长度,格式化输入 \
                               // 的总长度不能超过它
#define FORMAT_CHAR '^'        // 格式化输入串用该字符表示的位为输入位
#define CURSE_FLASH_TIME 50    // 光标闪烁时间格, 单位 0.01 秒
#define ALPHA_INPUT_TIME 100   // 字母输入认定时间间隔, 单位 0.01 秒.

extern void vTakeOutCard(void);

static uchar sg_ucInputFill;                     // 记录当前的输入填充字符.
static uchar *sg_pszInputFormat;                 // 记录当前格式化输入串
static int (*sg_piLoop)(uchar *);                // 保存iInput()函数中调用的外部嵌入函数指针.
static uchar **sg_ppszAlphaTable;                // 指向字符串指针的指针,用来记录外部设置的
                                                 // 字母表.必须有连续十个字符串指针,分别对应
                                                 // 键码'0'-'9'.
static int (*sg_piCCInput)(stCCInput *pCCInput); // 输入法函数
static int sg_iShowWaitInitFlag;                 // vShowWait()初始化标志, 0:未初始化, 1:已经初始化
static char sg_cSupportSpecAlphaKey;            // 字母输入时支持独立按键(.*#)

static uchar sg_ucUsbDebugLog=0;

// Function  : 初始化输入函数
// Design    : 用于初始化iInput()函数中用到的几个静态全局变量
// Reference : Var:sg_ucInputFill,sg_pszInputFormat,sg_piLoop,sg_ppszAlphaTable
// Attention : 如需调用iInput()函数，必须先做此初始化
void vInitInput(void)
{
    sg_ucInputFill = DEFAULT_INPUT_FILL; // 设置缺省输入填充字符
    sg_pszInputFormat = (uchar *)0;      // 清除格式化串指针
    sg_piLoop = 0;                       // 清除嵌入函数指针
    sg_ppszAlphaTable = (uchar **)0;     // 清除字母表
    sg_piCCInput = 0;                    // 清除输入法函数指针
    sg_iShowWaitInitFlag = 0;
    sg_cSupportSpecAlphaKey=0;
}

// Function  : 设置在iInput()函数中调用的嵌入函数
// Design    : iInput()在执行过程中会不停的调用该函数，并将按键代码传给该函数
//             按键代码为0表示没有键按下，该键码可以在外部改变
//             该函数返回非0将导致iInput()函数退出并返回该值
//             使用完后要撤销外部函数 -> vSetLoop(0)
// Reference : Var : sg_piLoop
// In        : pFunc : 嵌入函数指针, 为0表示不使用嵌入函数
void vSetLoop(int (*pFunc)(uchar *))
{
    sg_piLoop = pFunc; // 设定当前嵌入函数指针.
}

// Function  : 设置输入法函数
// Reference : Var : sg_piCCInput
// In        : pFunc : 输入法函数指针
void vSetCCInput(int (*pFunc)(stCCInput *pCCInput))
{
    sg_piCCInput = pFunc; // 设定当前嵌入函数指针.
}

// Function  : 设置iInput()函数输入区的填充字符
// Reference : Var : sg_ucInputFill
// In        : ucInputFill : 新的输入区的填充字符, 0恢复缺省值
void vSetInputFill(uchar ucInputFill)
{
    if (ucInputFill == 0)
        sg_ucInputFill = DEFAULT_INPUT_FILL;
    else
    {
        if (ucInputFill < 0x20 || ucInputFill > 0x7f)
            sg_ucInputFill = DEFAULT_INPUT_FILL; // 如果参数不可用,使用缺省填充字符
        sg_ucInputFill = ucInputFill;
    }
}

// Function  : 设置在iInput()函数中格式化输入的格式串
// Design    : 当sg_pszInputFormat有效时(非空),启用该格式化字符串
//             即非格式化字符将被显示到显示器,格式化字符位置留给输入用
//             使用完后要撤销格式串 -> vSetInputFormat(0)
// Reference : Var : sg_pszInputFormat
// In        : pszFormat : 格式化串
void vSetInputFormat(uchar *pszFormat)
{
    sg_pszInputFormat = (uchar *)0; // 首先清除格式化串
    if (pszFormat)
        if (pszFormat[0])
            sg_pszInputFormat = pszFormat; // 如果格式化串有效则设置格式化串指针
}

// Function  : 设置在iInput()函数中字母输入模式时的数字字母对应表
// Design    : ppszAlphaTable[0] -- ppszAlphaTable[9] 分别表示 '0' -- '9'
//             键对应的字母序列, 字母序列以 '\0' 结尾
// Reference : Var : sg_ppszAlphaTable
// In        : ppszAlphaTable : 数字字母对应表
void vSetAlphaTable(uchar **ppszAlphaTable)
{
    sg_ppszAlphaTable = ppszAlphaTable;
}

//字母输入时是否支持独立的.*#三个按键
void vSetSupportSpecAlphaKey(char flag)
{
    sg_cSupportSpecAlphaKey=flag;
}

// 键盘输入函数

// Function  : 键盘输入函数
// Design    : 光标用两元素的字符数组，交替显示其中内容的方法实现
//             字母输入：每一数字键对应一字符串(\0结尾)，如果连续敲入同一数字键
//                       则循环依次选取该字符串内各字符。也可以输入非字母符号
//             如果设置了嵌入函数，会不断地调用它。按键也会传入嵌入函数。嵌入
//             函数可改变按键值并控制iInput()的终止与否
// Reference : Var:sg_ucInputFill,sg_pszInputFormat,sg_piLoop,sg_ppszAlphaTable
///////////////////////////////////////////////////////////////////////////////
// In  : uiFlag    : 属性，可为下述各属性的组合
//                   INPUT_NORMAL    : 输入数字、需回车、左靠齐、不设初始值
//                   INPUT_PIN       : 输入密码，不要与INPUT_ALPHA属性同时使用
//                   INPUT_BY_LEN    : 达到最大长度后自动返回，不需要回车
//                   INPUT_RIGHT     : 右靠齐
//                   INPUT_INITIAL   : 使用初始值
//                   INPUT_MONEY     : 输入金额，其它属性全部失效, 需输入窗口
//                                     大小不能小于4字节，最大与最小长度无效
//                   INPUT_DOT_MONEY : 允许输入小数点的金额输入方式
//                   INPUT_ALPHA     : 字母输入属性，必须先设置字母数字对应表
//                                     vSetAlphaTable(字母数字对应表)
//                                     不能与INPUT_ALL_CHAR同时使用
//                   INPUT_ALL_CHAR  : 接受任何字符
//                   INPUT_NO_CANCEL : 不允许取消
//                   INPUT_CC        : 汉字输入属性
//       ucRow     : 输入区域行位置
//       ucCol     : 输入区域列位置
//       pszResult : INPUT_INITIAL方式时传入初始值
//       ucAreaLen : 输入窗口大小
//       ucMinLen  : 需输入的最小长度, uiFlag指明为INPUT_MONEY或INPUT_DOT_MONEY时, 表示小数点后数字个数(0-3)
//       ucMaxLen  : 允许输入的最大长度
//       uiTimeout : 超时时间(单位:秒)
// Out : pszResult : 输入内容, 输入金额时返回分为单位、不含小数点的串
// Ret : >=0       : 输入内容的长度
//       -1        : 用户取消
//       -2        : 参数错误
//       -3        : 超时
// Attention : 接收输入区域不能超出屏幕范围，输入最大长度不能超过
//             MAX_INPUT_LENGTH定义的长度
int iInput(uint uiFlag, uchar ucRow, uchar ucCol, uchar *pszResult,
           uchar ucAreaLen, uchar ucMinLen, uchar ucMaxLen, uint uiTimeout)
{
//本函数分多次实现,所以定义两个宏。若去掉一个或全部，则不支持所定义的功能，但可节省空间
#define INPUT_SUPPORT_FORMAT
#define INPUT_SUPPORT_MONEY
#define INPUT_SUPPORT_SPEC_ALPHAKEY
    
    static const uchar aucCurseStyle[2] = {'_', ' '}; // 光标字符
    static uchar szTxt[MAX_INPUT_LENGTH + 1];         // 屏幕显示内容缓冲区
    static uchar sBuffer[MAX_INPUT_LENGTH];           // 用户输入内容缓冲区
    static uchar ucTmp[MAX_INPUT_LENGTH];           // 
    ulong ulCurseTimer;                               // 光标闪烁间隔计时器
    ulong ulAlphaTimer;                               // 字母输入确定计时器
    uchar ucKeyIn;                                    // 接受输入的字符
    uchar ucCursePos = 0;                             // 当前光标位置,在szTxt[]中, 0xff表示禁止光标
    uchar ucCurseCounter = 0;                         // 记录光标字符下标
    uchar ucInLength = 0;                             // 当前输入的字符串长度
    uchar ucAlphaFlag = 0;                            // 处理字母输入标志
    uchar ucCCInputFlag = 0;                          // 汉字输入方式标志
    uchar ucLastKey = 0;                              // 记录上次敲入的键值
    uchar ucAlphaPointer = 0;                         // 在连续敲同一键时，依次指向该键对应的各个字母
    uchar ucDecimalPos = 0;                           // INPUT_MONEY 或 INPUT_DOT_MONEY 时, 小数点位置(0-3)
    uchar ucAlphaPinHideFlag = 0;                     // 字母表方式输入密码时最后一个字符隐藏标志, 0:未隐藏 1:已隐藏
    int i1, i2;
    ulong ulTimer;
    uchar ucRefreshFlag = 1; // 提高速度用，1 表示需刷新屏幕 0 不用
    uchar szCC[3];           // 输入汉字用
    stCCInput CCInput;       // 汉字输入结构
   uchar ucLength = 0; 
   uchar uctmp[20+1] ;
   uchar ucColum;
   uchar uckeydown = 0;
   
    if (ucRow < 1 || ucRow > (uchar)_uiGetVLines() )
        return (-2); // 参数非法    
    if (uiFlag & INPUT_MONEY)
    {
        if (ucMinLen > 3)
            return (-2); // 输入金额时, 小数点后数字不能大于3
        if (ucAreaLen < ucMinLen + 2)
            return (-2); // 输入金额时, 输入区域不能小于小数点后数字位数+2
                         //             如果小数点后数字位数为0, 则输入区域长度至少为2	
    }
    if (sg_pszInputFormat)
    {
        // 格式化输入时不能右靠齐、不能输入金额
        uiFlag &= ~INPUT_RIGHT;
        uiFlag &= ~INPUT_MONEY;
        uiFlag &= ~INPUT_DOT_MONEY;
    }
#ifdef INPUT_SUPPORT_MONEY
    if (uiFlag & INPUT_MONEY)
    {
        ucDecimalPos = ucMinLen;
        if (ucDecimalPos > 3)
            ucDecimalPos = 2;
        ucMinLen = 0;
        if (ucDecimalPos)
            ucAreaLen--; // 小数点要占一位
        ucMaxLen = ucAreaLen;
        // INPUT_MONEY 只能与 INPUT_NO_CANCEL、INPUT_INITIAL配合
        uiFlag = INPUT_MONEY | (uiFlag & INPUT_NO_CANCEL) | (uiFlag & INPUT_INITIAL);
    }
    if (uiFlag & INPUT_DOT_MONEY)
    {
        ucDecimalPos = ucMinLen;
        if (ucDecimalPos > 3)
            ucDecimalPos = 2;
        ucMinLen = 1;
        ucMaxLen = ucAreaLen;
        // INPUT_DOT_MONEY 只能与 INPUT_NO_CANCEL、INPUT_INITIAL配合
        uiFlag = INPUT_DOT_MONEY | (uiFlag & INPUT_NO_CANCEL) | (uiFlag & INPUT_INITIAL);
    }
    if (uiFlag & INPUT_CC)
    {
        ucCCInputFlag = 1;
    }
    if (ucAreaLen > ucMaxLen)
        ucAreaLen = ucMaxLen;
#endif
    if (uiFlag & INPUT_ALL_CHAR)
        uiFlag &= ~INPUT_ALPHA; // 如果允许输入所有字符，则不能以字母表的形式输入
    if (uiFlag & INPUT_ALPHA)
    {
        if (sg_ppszAlphaTable)
        {
            // 只有设置了字母数字对应表后才允许字母输入
            ucAlphaFlag = 1;
            uiFlag &= ~INPUT_BY_LEN;
        }
        else
        {
            // 如果没设置字母数字对应表，设置成可输入所有字符
            uiFlag &= ~INPUT_ALPHA;
            uiFlag |= INPUT_ALL_CHAR;
        }
    }
#ifdef INPUT_SUPPORT_FORMAT
    //////// 该段只为格式化输入用，删除不影响其它方式
    if (sg_pszInputFormat && !(uiFlag & INPUT_MONEY) && !(uiFlag & INPUT_DOT_MONEY))
    {
        // 格式串可用且不是输入金额

        // 检查格式化串中可供输入的区域的长度
        ucAreaLen = 0;
        for (i1 = 0; i1 < (int)strlen((char *)sg_pszInputFormat); i1++)
            if (sg_pszInputFormat[i1] == FORMAT_CHAR)
                ucAreaLen++;

        if (!ucAreaLen)
            return (-2); // 如果可供输入的区域的长度为0则报错
        ucMaxLen = ucAreaLen;
        if (ucMinLen > ucMaxLen)
            ucMinLen = ucMaxLen;
    }
//////// end of 该段只为格式化输入用
#endif
    if (ucAreaLen == 0 || ucMinLen > ucMaxLen || ucMaxLen > MAX_INPUT_LENGTH)
        return (-2); // 参数非法

    if ((ucRow - 1) * _uiGetVCols() + ucCol + ucAreaLen > (_uiGetVLines() * _uiGetVCols()))
        return (-2); // 输入区域超界
    memset(sBuffer, 0, sizeof(sBuffer));
    if (uiFlag & INPUT_INITIAL)
    {
        strncpy((char *)sBuffer, (char *)pszResult, ucMaxLen); // 填入输入缓冲区缺省值
        ucInLength = strlen((char *)sBuffer);
    }
    else
        memset(sBuffer, 0, ucMaxLen); // 清输入缓冲区

    // 初始化显示缓冲区，首先全部填入填充字符
    for (i1 = 0; i1 < ucAreaLen; i1++)
        szTxt[i1] = sg_ucInputFill;
    szTxt[ucAreaLen] = 0;

//    _vSetTimer(&ulCurseTimer, CURSE_FLASH_TIME); // 设置光标闪烁计时器
    _vSetTimer(&ulAlphaTimer, 0);                // 设置字母输入确定计时器
    if (uiTimeout)
        _vSetTimer(&ulTimer, uiTimeout * 100L); // 设置超时时间

    // 输入及处理循环, 两处标号是为加快按键响应速度而设的
    for (;;)
    {
         if(ucInLength == 0 && !(uiFlag & INPUT_MONEY) && !(uiFlag & INPUT_DOT_MONEY) && !(uiFlag & INPUT_DOT_SPACE) )
	{
		_vDispAt(ucRow, _uiGetVCols()-1, "_");	
	}

	if(ucInLength == 0 && uiFlag & INPUT_CENTER_SPACE)	
	{
		_vDispAt(ucRow, ucCol, "_");	
	}
	
        ucKeyIn = 0;
        if (_uiKeyPressed())
            goto LabelKeyIn;
        if (uiTimeout && _uiTestTimer(ulTimer))
            return (-3); // 超时
  //      if (_uiTestTimer(ulCurseTimer))
 //           ucRefreshFlag = 1; // 光标闪烁计时器到时，需刷新屏幕
        if (!ucRefreshFlag)
            continue; // 如果不需刷新屏幕，不必处理输入数据

        // 判断是否是格式化输入方式, 本段只处理非格式化、非金额输入
        if (!sg_pszInputFormat &&
            !(uiFlag & INPUT_MONEY) && !(uiFlag & INPUT_DOT_MONEY))
        {
            // 显示输入内容并计算光标位置
            if (ucInLength == ucMaxLen)
            { // 到达最大长度，需禁止光标
                // 构造显示缓冲区                
                for (i1 = 0; i1 < ucAreaLen; i1++)
                {
                    szTxt[ucAreaLen - i1 - 1] = (uiFlag & INPUT_PIN) ? '*' : sBuffer[ucInLength - i1 - 1];
                }
                if ((uiFlag & INPUT_PIN) && ucAlphaFlag)
                {
                    if (!_uiTestTimer(ulAlphaTimer))
                    {
                        szTxt[ucAreaLen - 1] = sBuffer[ucInLength - 1];
                        ucAlphaPinHideFlag = 0; // 字母表方式输入密码时最后一个字符隐藏标志, 0:未隐藏 1:已隐藏
                    }
                    else
                        ucAlphaPinHideFlag = 1; // 字母表方式输入密码时最后一个字符隐藏标志, 0:未隐藏 1:已隐藏
                }
                ucCursePos = 0xff; // 禁止光标
            }
            else if ((ucInLength < ucAreaLen) && !(uiFlag & INPUT_RIGHT))
            {
                // 左靠齐且没有达到最大长度

                // 构造显示缓冲区
                for (i1 = 0; i1 < ucAreaLen; i1++)
                {
                    if (i1 < ucInLength)
                        szTxt[i1] = (uiFlag & INPUT_PIN) ? '*' : sBuffer[i1];
                    else
                        szTxt[i1] = sg_ucInputFill;
                }
                if ((uiFlag & INPUT_PIN) && ucAlphaFlag && ucInLength)
                {
                    if (!_uiTestTimer(ulAlphaTimer))
                    {
                        szTxt[ucInLength - 1] = sBuffer[ucInLength - 1];
                        ucAlphaPinHideFlag = 0; // 字母表方式输入密码时最后一个字符隐藏标志, 0:未隐藏 1:已隐藏
                    }
                    else
                        ucAlphaPinHideFlag = 1; // 字母表方式输入密码时最后一个字符隐藏标志, 0:未隐藏 1:已隐藏
                }
                ucCursePos = ucInLength; // 将光标设到下一个字符输入位
            }
            else
            { // 右靠齐且没有达到最大长度
                // 构造显示缓冲区
                for (i1 = 0; i1 < ucAreaLen - 1; i1++)
                {
                    if (i1 < ucInLength)
                        szTxt[ucAreaLen - i1 - 2] = (uiFlag & INPUT_PIN) ? '*' : sBuffer[ucInLength - i1 - 1];
                    else
                        szTxt[ucAreaLen - i1 - 2] = sg_ucInputFill;
                }
                if ((uiFlag & INPUT_PIN) && ucAlphaFlag && ucInLength)
                {
                    if (!_uiTestTimer(ulAlphaTimer))
                    {
                        szTxt[ucAreaLen - 2] = sBuffer[ucInLength - 1];
                        ucAlphaPinHideFlag = 0; // 字母表方式输入密码时最后一个字符隐藏标志, 0:未隐藏 1:已隐藏
                    }
                    else
                        ucAlphaPinHideFlag = 1; // 字母表方式输入密码时最后一个字符隐藏标志, 0:未隐藏 1:已隐藏
                }
                ucCursePos = ucAreaLen - 1; // 将光标设到下一个字符输入位，即最右边                     
            }
        } // if(!sg_pszInputFormat && 处理非格式化、非金额输入结束
#ifdef INPUT_SUPPORT_FORMAT
        //////// 该段只为格式化输入用，删除不影响其它方式
        // 判断是否是格式化输入方式, 本段只处理格式化输入
        if (sg_pszInputFormat &&
            !(uiFlag & INPUT_MONEY) && !(uiFlag & INPUT_DOT_MONEY))
        {
            if (ucInLength == ucAreaLen)
                ucCursePos = 0xff; // 到达最大长度，禁止光标

            // 构造显示缓冲区, i1为显示缓冲区指针, i2为输入缓冲区指针
            for (i1 = 0, i2 = 0; i1 < (int)strlen((char *)sg_pszInputFormat); i1++)
            {
                if (sg_pszInputFormat[i1] == FORMAT_CHAR)
                {
                    if (i2 == ucInLength)
                    {
                        // 到达输入缓冲区尾，设置光标
                        ucCursePos = (uchar)i1;
                        i2++;
                    }
                    else if (i2 < ucInLength)
                        szTxt[i1] = sBuffer[i2++]; // 用输入内容替换格式字符
                    else
                        szTxt[i1] = sg_ucInputFill; // 超出输入缓冲区,填入填充符
                }
                else
                    szTxt[i1] = sg_pszInputFormat[i1]; //不是格式字符，原样拷贝
            }                                          // for( i1=0,i2=0; i1<strlen(sg_pszInputFormat); i1++
            szTxt[i1] = 0;                             // 添加尾部0
        }                                              // if( sg_pszInputFormat && 处理格式化输入结束
//////// end of 该段只为格式化输入用
#endif
/*
        if (ucCursePos != 0xff)
        { // 0xff means disable curse
            // 如果光标没被禁止，在光标位填入当前应显示的光标字符
            szTxt[ucCursePos] = aucCurseStyle[ucCurseCounter & 0x01];
            if (_uiTestTimer(ulCurseTimer))
            {
                ucCurseCounter++;
                _vSetTimer(&ulCurseTimer, CURSE_FLASH_TIME);
            }
        }
*/		
#ifdef INPUT_SUPPORT_MONEY
        //////// 该段只为金额输入用，删除不影响其它方式
        if (uiFlag & INPUT_MONEY)
        {
            // 金额输入，小数点位固定，不需键入小数点
            if (ucInLength == 0)
            {
                sBuffer[0] = '0';
                ucInLength = 1;
            }
            if (sBuffer[0] == '0' && ucInLength > 1)
            {
                sBuffer[0] = sBuffer[1];
                ucInLength = 1;
            }

            if (ucDecimalPos == 0)
            {
                // 小数点后数字个数为0
                memset(szTxt, ' ', ucAreaLen);
                szTxt[ucAreaLen] = 0;
                memcpy(szTxt + ucAreaLen - ucInLength, sBuffer, ucInLength);
            }
            else
            {
                // 小数点后数字个数为1、2、3
                // 由于小数点留了一个位置, ucAreaLen比实际输入区域小了1个字节,计算位置时要给加回去
                memset(szTxt, '0', ucAreaLen + 1);
                szTxt[ucAreaLen + 1] = 0;
                memcpy(szTxt + ucAreaLen + 1 - ucInLength, sBuffer, ucInLength);
                memmove(szTxt, szTxt + 1, ucAreaLen + 1 - ucDecimalPos - 1);
                szTxt[ucAreaLen + 1 - ucDecimalPos - 1] = '.';
                for (i1 = 0; i1 < ucAreaLen + 1 - ucDecimalPos - 2; i1++)
                {
                    if (szTxt[i1] != '0')
                        break;
                    szTxt[i1] = ' ';
                }
            }
        }
        if (uiFlag & INPUT_DOT_MONEY)
        {
            // 金额输入，小数点位不固定，需键入小数点
            
            //初始值填0
            if (ucInLength == 0)
            {
                sBuffer[0] = '0';
                ucInLength = 1;
            }
            
            // 以自由格式设置屏幕缓冲区
            memset(szTxt, ' ', ucAreaLen);
            szTxt[ucAreaLen] = 0;
            memcpy(&szTxt[ucAreaLen - ucInLength], sBuffer, ucInLength);
        }
//////// end of 该段只为金额输入用
#endif

        // 显示屏幕缓冲区内容
#if 0       
        ucColum = _uiGetVCols();
        if(strlen((char *)szTxt) > ucColum)
        {
                
		if(szTxt[ucColum-1] >= 0x80)	
		{
		      memset(uctmp,0x00,sizeof(uctmp));
		       memcpy(uctmp,szTxt,ucColum-1);
		       szTxt[ucInLength] = 0;  
		       ucCol = 0;
			_vDispAt((int)ucRow, (int)ucCol, uctmp);
			dbgHex("gl_uctmp:",uctmp,strlen(uctmp));
			dbgHex("gl_szTxt:",szTxt,strlen(szTxt));
			dbg("sztext:%d\n",strlen(szTxt));
			dbg("uctmp:%d\n",strlen(uctmp));
		//	ucCol = _uiGetVCols() - (strlen(szTxt) - strlen(uctmp));
		//	_vDispAt( (int)ucRow + 1,(int)ucCol,szTxt +ucColum-1);
		}	
		else	
		{
		        szTxt[ucInLength] = 0;  
			ucCol = 0;
			_vDispAt((int)ucRow, (int)ucCol, szTxt);
			ucCol = _uiGetVCols() - (strlen(szTxt) - _uiGetVCols());
			_vDispAt( (int)ucRow + 1,(int)ucCol,szTxt +ucColum);			
		}  

        }	
	else
        	_vDispAt((int)ucRow, (int)ucCol, szTxt);
#endif	 
         if(uiFlag & INPUT_TWO_LINE)
	{ 
		szTxt[ucInLength] = 0;
	        if(strlen((char *)szTxt) > _uiGetVCols())
		{
		       if(uckeydown == 1)
		       {
			       _vDisp(ucRow, " ");
				if(szTxt[_uiGetVCols()-1] >= 0x80)	
				{
					ucColum = _uiGetVCols() - strlen(szTxt+_uiGetVCols()-1);
					_vDispAt( (int)ucRow ,(int)ucColum,szTxt +_uiGetVCols()-1);
				}	
				else	
				{
					ucColum = _uiGetVCols() - strlen(szTxt+_uiGetVCols());
					_vDispAt((int)ucRow, (int)ucColum, szTxt+_uiGetVCols());	
				}	
	        	}
			if(uckeydown == 0)
			{
				_vDisp(ucRow, " ");
				_vDispAt((int)ucRow, (int)0, szTxt);
			}
	        }	
		else
	        {
	        	ucCol  = _uiGetVCols() - strlen(szTxt);
	        	_vDisp(ucRow, " ");
	        	_vDispAt((int)ucRow, (int)ucCol, szTxt);
		}
         }
         else
      		_vDispAt((int)ucRow, (int)ucCol, szTxt);
        ucRefreshFlag = 0;

        // 如果到达最大长度且指明不需回车则立即返回
        if ((uiFlag & INPUT_BY_LEN) && ucInLength == ucMaxLen)
        {
            memcpy(pszResult, sBuffer, ucInLength);
            pszResult[ucInLength] = 0;
            return (ucInLength);
        }

        // 如果设置了嵌入函数，执行嵌入函数
        if (sg_piLoop)
        {
            i1 = (*sg_piLoop)(&ucKeyIn);
            if (i1)
                return (i1); // 嵌入函数返回非0，结束输入
            if (ucKeyIn)
                goto LabelKeyEntered; // 嵌入函数返回键值，等同键盘输入
        }
        if (!_uiKeyPressed())
            continue; // 没有键按下，继续输入循环

        // 用户按下键，处理...
    LabelKeyIn:
        if (uiTimeout)
            _vSetTimer(&ulTimer, uiTimeout * 100L); // 有按键，重新设置超时时间
        ucKeyIn = (uchar)_uiGetKey();

        if (sg_piLoop)
        {
            // 如果设置了嵌入函数，执行嵌入函数
            i1 = (*sg_piLoop)(&ucKeyIn); // 注意：嵌入函数可以改变键值
            if (i1)
                return (i1); // 嵌入函数返回非0，结束输入
        }
    LabelKeyEntered:
        ucRefreshFlag = 1; // 有键按下，需刷新屏幕
        if((uiFlag & INPUT_NULL) && (ucKeyIn != _KEY_ENTER) && (ucKeyIn != _KEY_CANCEL))
		continue;
	if(( ( uiFlag & INPUT_TWO_LINE ) == 0 && ( uiFlag & INPUT_FACTORY ) == 0 ) &&  (ucKeyIn == _KEY_UP) && (ucKeyIn == _KEY_DOWN))
		continue;
	
        switch (ucKeyIn)
        {
	case _KEY_UP:
		 uckeydown = 0;
		break;
	case _KEY_DOWN:
	//	 memset(ucTmp,0x00,sizeof(ucTmp));
	//	memcpy(ucTmp , szTxt + _uiGetVCols(),	strlen(szTxt) - _uiGetVCols()); 
       //        dbg("ucTmp:%s\n",ucTmp);
	//	memset(szTxt,0x00,sizeof(szTxt));
	//	 memcpy(szTxt , ucTmp,	strlen(ucTmp));
		 if(uiFlag & INPUT_FACTORY && ucInLength == 0)
				return -4;
		 uckeydown = 1;
		break;
        case _KEY_ENTER: // 回车
            if (ucInLength < ucMinLen)
                continue; // 长度不足
            
            if (uiFlag & INPUT_MONEY)
			{
				//不支持0金额输入
				if(ucInLength==1 && sBuffer[0]=='0')
					continue;
			}
            
            if (uiFlag & INPUT_DOT_MONEY)
            {
                // 可变小数点式金额输入，处理成以分为单位的字符串

                //不支持0金额输入
				if(ucInLength<=4 && memcmp(sBuffer, "0.00", ucInLength)==0)
					continue;
                
                // 查找小数点位置
                sBuffer[ucInLength] = 0;
                for (i1 = 0; i1 < ucInLength; i1++)
                {
                    if (sBuffer[i1] == '.')
                        break;
                }

                if (i1 >= ucInLength)
                {
                    // 无小数点，直接填00
                    strcat((char *)sBuffer, "00");
                    ucInLength += 2;
                }
                else if (i1 == ucInLength - 1)
                {
                    // 小数点后无数据，去掉小数点再填00
                    sBuffer[ucInLength - 1] = 0;
                    strcat((char *)sBuffer, "00");
                    ucInLength += 1;
                }
                else if (i1 == ucInLength - 2)
                {
                    // 小数点后有一位数，将该位数提前一位，填0
                    sBuffer[ucInLength - 2] = sBuffer[ucInLength - 1];
                    sBuffer[ucInLength - 1] = '0';
                }
                else
                {
                    // 小数点后有两位数，将该两位数各自提前一位
                    sBuffer[ucInLength - 3] = sBuffer[ucInLength - 2];
                    sBuffer[ucInLength - 2] = sBuffer[ucInLength - 1];
                    sBuffer[ucInLength - 1] = 0;
                    ucInLength--;
                }
            } // if(uiFlag & INPUT_DOT_MONEY
            if ((uiFlag & INPUT_PIN) && ucAlphaFlag && ucAlphaPinHideFlag == 0)
            {
                _vSetTimer(&ulAlphaTimer, 0);
                continue; // ucAlphaPinHideFlag:字母表方式输入密码时最后一个字符隐藏标志, 0:未隐藏 1:已隐藏
            }

            // 填结果
            memcpy(pszResult, sBuffer, ucInLength);
            pszResult[ucInLength] = 0;
            if ((uiFlag & INPUT_MONEY) && (ucInLength == 0))
            {
                // 数字输入空的话，认为输入为0
                ucInLength = 1;
                strcpy((char *)pszResult, "0");
            }
            return (ucInLength);
        case _KEY_CANCEL: // 取消
            if (uiFlag & INPUT_NO_CANCEL)
                continue; // 如果不允许取消，继续
            return (-1);
        case _KEY_BKSP: // 退格
            if (!ucInLength)
                continue;
	   ucLength = ucInLength -1;
	   if(sBuffer[ucLength] >= 0x80)	
	   	ucInLength -= 2;
            else
            	ucInLength--;	   
            ucLastKey = 0xff;	
	/*		
	 if ((!(uiFlag & INPUT_MONEY)) && (!(uiFlag & INPUT_ALPHA)))	
	  { 	
	 	// _vDisp(ucRow, " ");
     		//	 ucCol ++;  
                  if((uiFlag & INPUT_PIN) && (! (uiFlag & INPUT_LEFT)))
				;
	        else
	       	{
			 _vDisp(ucRow, " ");
     			 ucCol ++;     
	        }			 
           }
	 */
	 if(uiFlag & INPUT_TWO_LINE)
	 {
	 	if(ucInLength > _uiGetVCols())
	 		uckeydown = 1;
		else if(ucInLength <=_uiGetVCols())
			uckeydown = 0;
	 
		 if((uiFlag & INPUT_LEFT) && uckeydown == 0)
		 {
			_vDisp(ucRow, " ");
			if(sBuffer[ucLength] >= 0x80)	
				ucCol += 2;
			else	
				ucCol ++;  
		 }	 	
        }else if((uiFlag & INPUT_LEFT))
	{
			_vDisp(ucRow, " ");
			if(sBuffer[ucLength] >= 0x80)	
				ucCol += 2;
			else	
				ucCol ++;  
	}	
		
            break;
        case _KEY_FN:
            if (ucCCInputFlag == 0)
                ucCCInputFlag = 1;
            else
                ucCCInputFlag = 0;
            break;
        case _KEY_0:
        case _KEY_1:
        case _KEY_2:
        case _KEY_3:
        case _KEY_4:
        case _KEY_5:
        case _KEY_6:
        case _KEY_7:
        case _KEY_8:
        case _KEY_9: // 数字      
            if ((uiFlag & INPUT_CC) && ucCCInputFlag && sg_piCCInput)
            {
                // 汉字输入状态
                CCInput.ucFirstCode = ucKeyIn;
                CCInput.ulTimer = ulTimer;
                CCInput.pszCC = szCC;
                i1 = (*sg_piCCInput)(&CCInput);
                if (i1 == -2)
                    return (-3); // timeout
                if (i1 == -3)
                {
                    ucCCInputFlag = 0;
                    continue;
                }
                if (i1 == -4)
                {
                    if (!(uiFlag & INPUT_NO_CANCEL))
                        return (-1); // cancel
                }
                if (i1)
                    continue;
                i2 = strlen((char *)szCC); // 输入字符长度,可能是1或2
                if (ucInLength + i2 > ucMaxLen)
                    continue; // 超出缓冲区长度
                memcpy(sBuffer + ucInLength, szCC, i2);
                ucInLength += i2;
                break;
            }
            // 处理在字母输入模式下、在锁定字母前、按了同一键的情况
            if (ucAlphaFlag && (ucLastKey == ucKeyIn))
            {  
                if (!_uiTestTimer(ulAlphaTimer))
                {
                    if (!sg_ppszAlphaTable[ucKeyIn - _KEY_0][ucAlphaPointer])
                    {
                        if (ucAlphaPointer == 0)
                            continue;       // 该数字键没定义字母表
                        ucAlphaPointer = 0; // 绕回字母表首
                    }
                    // 取出当前指定的字母
                    sBuffer[ucInLength - 1] =
                        sg_ppszAlphaTable[ucKeyIn - _KEY_0][ucAlphaPointer++];
                    _vSetTimer(&ulAlphaTimer, ALPHA_INPUT_TIME);	
                    break;
                }
            } // if( ucAlphaFlag && (ucLastKey == ucKeyIn

            if (ucInLength == ucMaxLen)
                continue; // 已达到输入最大长度

            // 处理新字母输入
            if (ucAlphaFlag)
            {
                if (sg_ppszAlphaTable[ucKeyIn - _KEY_0][0] == 0)
                {
                    ucLastKey = 0; // 未定义字母表
                    break;
                }

                ucAlphaPointer = 0;
                sBuffer[ucInLength++] =
                    sg_ppszAlphaTable[ucKeyIn - _KEY_0][ucAlphaPointer++];
                ucLastKey = ucKeyIn;

                if (sg_ppszAlphaTable[ucKeyIn - _KEY_0][1] == 0)
                    ucLastKey = 0; // 如果字母表中只有一个字母，直接选中它
                    	 if(uiFlag & INPUT_TWO_LINE)
	 {
	 	if(ucInLength > _uiGetVCols())
	 		uckeydown = 1;
		else 
			uckeydown = 0;
	 }
                if(uiFlag & INPUT_LEFT)
	 	{
	       		_vDisp(ucRow, " ");
	        	ucCol --;	     
	 	 }
                _vSetTimer(&ulAlphaTimer, ALPHA_INPUT_TIME);
                break;
            } // if(ucAlphaFlag

            if (uiFlag & INPUT_DOT_MONEY)
            {
                // 检查是否会导致前导0
                if (ucInLength == 1 && sBuffer[0] == '0')
                {
                    if (ucKeyIn == '0')
                        continue;
                    else
                        ucInLength = 0;
                }

                // 检查小数点后是否多于两个数字
                for (i1 = 0; i1 < ucInLength; i1++)
                {
                    if (sBuffer[i1] == '.')
                        break;
                }
                if (i1 + ucDecimalPos < ucInLength)
                    continue;
            } // if(uiFlag & INPUT_DOT_MONEY
             
            sBuffer[ucInLength++] = ucKeyIn;	

			/*
	 if ((!(uiFlag & INPUT_MONEY)) && (!(uiFlag & INPUT_ALPHA)))	
	    {
	        if((uiFlag & INPUT_PIN) && (! (uiFlag & INPUT_LEFT)))
				;
	        else
	       	{
	       		_vDisp(ucRow, " ");
	        	ucCol --;	     
	        }		
            }
	 */
	 dbg("uiFlag:%d\n",uiFlag);
	 if(uiFlag & INPUT_TWO_LINE)
	 {
	 	if(ucInLength > _uiGetVCols())
	 		uckeydown = 1;
		else 
			uckeydown = 0;
	 }	
	 if(uiFlag & INPUT_LEFT)
	 {
	       	_vDisp(ucRow, " ");
	        ucCol --;	     
	  }
            break;
        case _KEY_00: // 双0
            ucLastKey = 0xff;
            if (uiFlag & INPUT_ALPHA)
                continue; // 字母输入方式不允许输入双0
            if ((ucInLength >= ucMaxLen - 1))
                continue; // 输入缓冲区不足以容纳双0
            if (uiFlag & INPUT_DOT_MONEY)
            {
                // 检查是否会导致前导0
                if (ucInLength == 0 || (ucInLength == 1 && sBuffer[0] == '0'))
                    continue; // 不允许起始就打入两个0, 或起始已经是0了再打入两个0
                // 检查小数点后是否多于规定数字
                for (i1 = 0; i1 < ucInLength; i1++)
                {
                    if (sBuffer[i1] == '.')
                        break;
                }
                if (i1 + ucDecimalPos - 1 < ucInLength)
                    continue;
            } // if(uiFlag & INPUT_DOT_MONEY
            sBuffer[ucInLength++] = '0';
            sBuffer[ucInLength++] = '0';
            break;
#ifdef INPUT_SUPPORT_SPEC_ALPHAKEY            
        case _KEY_WELL:
		case _KEY_STAR:
#endif            
        case _KEY_DOT: // 小数点
        									 if(uiFlag & INPUT_LEFT)
	 {	        
	       		_vDisp(ucRow, " ");
	        	ucCol --;	     
	  }
            if ((uiFlag & INPUT_DOT_MONEY) && _KEY_DOT==ucKeyIn)
            {
                // 检查允不允许输入小数点
                if ((ucInLength >= ucMaxLen))
                    continue;
                for (i1 = 0; i1 < ucInLength; i1++)
                {
                    if (sBuffer[i1] == '.')
                        break; // 小数点已经输入，不允许再次输入
                }
                if (i1 >= ucInLength)
                    sBuffer[ucInLength++] = '.';
                break;
            }
#ifdef INPUT_SUPPORT_SPEC_ALPHAKEY		//支持特定字母键
			if (sg_cSupportSpecAlphaKey && (uiFlag & INPUT_ALPHA))
			{
				if ((ucInLength >= ucMaxLen))
                    continue;

				ucAlphaPointer = 0;
                sBuffer[ucInLength++] = ucKeyIn;

				ucLastKey = 0; // 如果字母表中只有一个字母，直接选中它
                _vSetTimer(&ulAlphaTimer, ALPHA_INPUT_TIME);
                break;
			}
#endif            
            // 不要在此加入 break
        default: // 任何其他字符
            ucLastKey = 0xff;
            if ((uiFlag & INPUT_ALL_CHAR) != INPUT_ALL_CHAR)
                continue; // 不允许所有字符的输入模式
            if (ucInLength == ucMaxLen)
                continue; // 到达最大长度
            if (ucKeyIn < 0x20 || ucKeyIn >= 0x7f)
                continue; // 不允许输入控制字符
            sBuffer[ucInLength++] = ucKeyIn;
            break;
        } // switch(ucKeyIn
    }     // for(;; 输入及处理循环
}

// Function  : 等待用户确认或取消
// Ret       : true  : 确认
//             false : 取消
uchar bOK(void)
{
    uint uiTmp;
    for (;;)
    {
        uiTmp = _uiGetKey();
        if (uiTmp == _KEY_ENTER)
        {
            return (1);
        }
        if (uiTmp == _KEY_CANCEL)
        {
            return (0);
        }
    }
}

// Function  : 带超时等待用户确认或取消
//             timeout-超时时间,单位秒
// Ret       : 1:确认 0:取消 -1:超时
int iOK(ushort timeout)
{
    ulong ulTimer;
    uint uiTmp;

    _vFlushKey();
    if(timeout==0)
        timeout=5;
    _vSetTimer(&ulTimer, timeout*100); //5秒
    while (!_uiTestTimer(ulTimer))
    {
        if (_uiKeyPressed())
        {
            uiTmp = _uiGetKey();
            if (uiTmp == _KEY_ENTER)
            {
                return (1);
            }
            if (uiTmp == _KEY_CANCEL)
            {
                return (0);
            }
        }
    }
    return(-1);
}

// Function  : 等待用户按键
// Ret       : 按键值
//
uint bPressKey(void)
{
    uint uiTmp;
    for (;;)
    {
        uiTmp = _uiGetKey();
        return uiTmp;
    }
}

// Function : 在Pos的最后一行显示消息
// In       : pszMessage : 要显示的字符串
void vShow(char *pszMessage)
{
    int line;
    
#ifdef MESSAGE_MID
    line=(_uiGetVLines()+1)/2;
#else
    line=_uiGetVLines();
    if(_uiGetVLines()>=8)
        line--;
#endif
    //_vDisp(_uiGetVLines(), (uchar *)pszMessage);
    vDispCenter(line, pszMessage, 0);
}
// 变长参数显示内容
// Function : 在Pos的最后一行显示消息,等待2.5秒钟或按键返回
// In  : uiLine    : 显示行
//       pszFormat : 显示格式串
void vShowVarArg(char *pszFormat, ...)
{
    uchar szBuf[100];
    va_list args;
    va_start(args, pszFormat);
    vsprintf((char *)szBuf, (char *)pszFormat, args);
    va_end(args);
    if (strlen((char *)szBuf) > _uiGetVCols())
        strcpy((char *)(szBuf + _uiGetVCols() - 3), "...");
    vShow((char *)szBuf);
}

//多行显示。若为单行则显示在line行，若超过一行则显示在line-1和line行(只支持两行)
void vDispMul(int line, char *pszMessage)
{
    int col=0;
    uchar buf[40];
    
    if(strlen(pszMessage)>_uiGetVCols())
    {
        col=iSplitGBStr((uchar*)pszMessage, _uiGetVCols());
        vMemcpy0(buf, (uchar*)pszMessage, col);
        _vDisp(line-1, buf);
    }
    _vDisp(line, (uchar*)pszMessage+col);
}

void vMessageLine(int line, char *pszMessage)
{
    ulong ulTimer;
    
    if(line<0)
    {
        line=_uiGetVLines();
        if(_uiGetVLines()>8)
            --line;
    }

    _vFlushKey();
    if(pszMessage)
    {
        vDispCenter(line, pszMessage, 0);
    }
    _vSetTimer(&ulTimer, 500L); //5秒
    while (!_uiTestTimer(ulTimer))
    {
        if (_uiKeyPressed())
        {
            _uiGetKey();
            break;
        }
    }
}

// Function : 在Pos的最后一行显示消息,等待2.5秒钟或按键返回
// In       : pszMessage : 要显示的字符串

void vMessage(char *pszMessage)
{
    ulong ulTimer;
    int line;
    
#ifdef MESSAGE_MID
    line=(_uiGetVLines()+1)/2;
#else
    line=_uiGetVLines();
    if(_uiGetVLines()>=8)
        line--;
#endif
    
    _vFlushKey();
    if(pszMessage)
    {
        vDispCenter(line, pszMessage, 0);
    }
    _vSetTimer(&ulTimer, 300L); //3秒500
    while (!_uiTestTimer(ulTimer))
    {
        if (_uiKeyPressed())
        {
            _uiGetKey();
            break;
        }
    }
}

void vMessageMulLast2(char *pszMessage)
{
    int col=0;
    uchar buf[40];
    int line = 4;
    
    if(strlen(pszMessage)>_uiGetVCols())
    {
        col=iSplitGBStr((uchar*)pszMessage, _uiGetVCols());
        vMemcpy0(buf, (uchar*)pszMessage, col);
        _vDisp(line, buf);
	line ++;	
    }
    _vDisp(line,pszMessage+col);
}

void vMessageMulLast3(char *pszMessage)
{
    int col1=0,col2=0;
    uchar buf[40];
    int line = 3;
    
    if(strlen(pszMessage)>2*_uiGetVCols())
    {
        col1=iSplitGBStr((uchar*)pszMessage, _uiGetVCols());
        vMemcpy0(buf, (uchar*)pszMessage, col1);
        _vDisp(line, buf);
	line ++;	

	col2=iSplitGBStr((uchar*)(pszMessage+col1), _uiGetVCols());
        vMemcpy0(buf, (uchar*)(pszMessage+col1), col2);
        _vDisp(line, buf);
	line ++;
    }
    _vDisp(line,pszMessage+col1+col2);
}

//多行
void vMessageMul(char *pszMessage)
{
    int col=0;
    uchar buf[40];
    int line;
    
#ifdef MESSAGE_MID
    line=(_uiGetVLines()+1)/2;
#else
    line=_uiGetVLines();
    if(_uiGetVLines()>=8)
        line--;
#endif
    if(strlen(pszMessage)>_uiGetVCols())
    {
        col=iSplitGBStr((uchar*)pszMessage, _uiGetVCols());
        vMemcpy0(buf, (uchar*)pszMessage, col);
        _vDisp(--line, buf);
    }
    _vDisp(++line,pszMessage+col);	
    vMessage(NULL);
}

//多行
void vMessageMulEx(char *pszMessage)
{
    int col=0,col1=0;
    uchar buf[40];
    int line = 2;
    char *p;

    p = pszMessage;
    while(strlen(p)>_uiGetVCols())
    {
        col=iSplitGBStr(p, _uiGetVCols());
        vMemcpy0(buf, p, col);
        _vDisp(line++, buf);
	p += col;
	col1 += col;
    }
    _vDisp(line, pszMessage+col1);	
}

//指定时间等待显示
//lWaitTime: <0默认超时时间5秒 =0无超时,按键才退 >0指定时间退出(单位0.01秒)
void vMessageEx(char *pszMessage, long lWaitTime)
{
    ulong ulTimer;
    int line;
    
#ifdef MESSAGE_MID
    line=(_uiGetVLines()+1)/2;
#else
    line=_uiGetVLines();
    if(_uiGetVLines()>=8)
        line--;
#endif
    
    _vFlushKey();
    if(pszMessage)
    {
        vDispCenter(line, pszMessage, 0);
    }
    if(lWaitTime<0)         //默认时间5秒
        lWaitTime=500;
    if(lWaitTime)
        _vSetTimer(&ulTimer, lWaitTime);
    while (1)
    {
        if (_uiKeyPressed())
        {
            _uiGetKey();
            break;
        }
        if(lWaitTime && _uiTestTimer(ulTimer))
            break;
    }
}

// 变长参数显示内容
// Function : 在Pos的最后一行显示消息,等待2.5秒钟或按键返回
// In  : pszFormat : 显示格式串
void vMessageVarArg(char *pszFormat, ...)
{
    uchar szBuf[100];
    va_list args;
    va_start(args, pszFormat);
    vsprintf((char *)szBuf, (char *)pszFormat, args);
    va_end(args);
    if (strlen((char *)szBuf) > _uiGetVCols())
        strcpy((char *)(szBuf + _uiGetVCols() - 3), "...");
    vMessage((char *)szBuf);
}

// Function : 在Pos的最后一行显示消息, 并且至少等待ulWaitTime/100秒
// In   : pszMessage : 要显示的字符串
//        lWaitTime  : 等待的时间, 0.01秒为单位, -1表示等待缺省时间
// Note : 提高效率原因,提供本函数,本函数会确保等待上次调用本函数时传入的时间长度
//        但本次调用会立即返回, 再由下一次调用本函数来确保信息持续时间.
void vShowWait(char *pszMessage, long lWaitTime)
{
    static ulong ulTimer;
    if (lWaitTime == -1)
        lWaitTime = 500; // 缺省等待5秒
    if (sg_iShowWaitInitFlag == 1)
    {
        // 本函数已经被调用过, 确保上次调用信息保持时间
        while (_uiTestTimer(ulTimer) == 0)
            ;
    }
    sg_iShowWaitInitFlag = 1;
    if (pszMessage)
        vShow(pszMessage);
    _vSetTimer(&ulTimer, lWaitTime);
    while (1)
    {
        if (_uiTestTimer(ulTimer) == 1)
            break;
    }
}

// 使用缺省等待时间
void vShowWaitVarArg(char *pszFormat, ...)
{
    uchar szBuf[100];
    va_list args;
    va_start(args, pszFormat);
    vsprintf((char *)szBuf, (char *)pszFormat, args);
    va_end(args);
    vShowWait((char *)szBuf, -1);
}

//显示并立即退出,二次调用时等待时间为0可检查超时时间并按键键退出
//lWaitTime >0:等待时间,单位0.01秒 -1:使用缺省值500(5秒) 0:检查等待是否超时(用于二次调用)
//keyflag: lWaitTime=0时本参数有效, 0-不处理按键 0x01-取消键退出 0x02-确认键退出 0x04-任意键退出
void vShowWaitEx(char *pszMessage, long lWaitTime, int keyFlag)
{
    static ulong ulTimer;
    uint uiKey;
    int line;
    
#ifdef MESSAGE_MID
    line=(_uiGetVLines()+1)/2;
#ifdef ST7567
    line=2;
#endif    
#else
    line=_uiGetVLines();
    if(_uiGetVLines()>=8)
        line--;
#endif    
    
    if (lWaitTime == -1)
        lWaitTime = 500; // 缺省等待5秒
    if (pszMessage)
        vDispCenter(line, pszMessage, 0);
    if(lWaitTime)
    {
        _vSetTimer(&ulTimer, lWaitTime);
    }else
    {
        while (1)
        {
            if (_uiTestTimer(ulTimer) == 1)
                break;
            if(keyFlag && _uiKeyPressed())
            {
                uiKey=_uiGetKey();
                if(keyFlag&0x04)
                    break;
                if((keyFlag&0x01) && uiKey==_KEY_ESC)
                    break;
                if((keyFlag&0x02) && uiKey==_KEY_ENTER)
                    break;
            }
        }
    }
    return;
}


/*
// Function : 等待插卡
// In       : uiSlot : 虚拟卡座号
// Ret      : 0      : 有卡片插入
//            1      : 用户取消
uint uiWaitCard(uint uiSlot)
{
    while(1) {
        if(_uiTestCard(uiSlot))
            return(0);
        if(_uiKeyPressed())
            if(_uiGetKey() == _KEY_ESC)
                return 1;
    }
}
*/

// Function : 在某行居中显示信息
// In       : uiLine : 行号(1-n)
//            pszMsg : 内容
//            ucAttr : 属性
void vDispCenter(uint uiLine, char *pszMsg, uchar ucAttr)
{
    uchar szBuf[100];

    if (strlen((char *)pszMsg) > _uiGetVCols())
    {
        _vDispX((uchar)uiLine, (uchar *)pszMsg, ucAttr);
        return;
    }
    memset(szBuf, ' ', sizeof(szBuf));
    szBuf[_uiGetVCols()] = 0;
    memcpy(szBuf + (_uiGetVCols() - strlen((char *)pszMsg)) / 2, (char *)pszMsg, strlen((char *)pszMsg));
    _vDispX((uchar)uiLine, szBuf, ucAttr);
}

void vDispMid(uint uiLine, char *pszMsg)
{
    vDispCenter(uiLine, pszMsg, 0);
}

void vDispRight(uint uiLine, char *pszMsg)
{
    uchar szBuf[100];

    if (strlen((char *)pszMsg) > _uiGetVCols())
    {
        _vDispX((uchar)uiLine, (uchar *)pszMsg, 0);
        return;
    }
    memset(szBuf, ' ', sizeof(szBuf));
    szBuf[_uiGetVCols()] = 0;
    memcpy(szBuf + (_uiGetVCols() - strlen((char *)pszMsg)), (char *)pszMsg, strlen((char *)pszMsg));
    _vDispX((uchar)uiLine, szBuf, 0);
}

//带箭头的居中显示
void vDispCenterEx(uint uiLine, char *pszMsg, uchar ucAttr, int min, int curr, int max)
{
    uchar szBuf[100];

    if (strlen((char *)pszMsg) > _uiGetVCols())
    {
        _vDispX((uchar)uiLine, (uchar *)pszMsg, ucAttr);
        return;
    }
    memset(szBuf, ' ', sizeof(szBuf));
    szBuf[_uiGetVCols()] = 0;
    memcpy(szBuf + (_uiGetVCols() - strlen((char *)pszMsg)) / 2, (char *)pszMsg, strlen((char *)pszMsg));
    
    if(curr>=0 && strlen(pszMsg)+4<=_uiGetVCols())
    {
        if(curr>min)
            memcpy(szBuf, "↑", 2);
        if(curr<max)
            memcpy(szBuf+_uiGetVCols()-2, "↓", 2);
    }
    
    _vDispX((uchar)uiLine, szBuf, ucAttr);
}

// 变长参数显示内容, 如果一行显示不下, 尾部表示为"..."
// In  : uiLine    : 显示行
//       pszFormat : 显示格式串
void vDispVarArg(uint uiLine, char *pszFormat, ...)
{
    uchar szBuf[200];
    va_list args;
    va_start(args, pszFormat);
    vsprintf((char *)szBuf, (char *)pszFormat, args);
    va_end(args);
    if (strlen((char *)szBuf) > _uiGetVCols())
        strcpy((char *)(szBuf + _uiGetVCols() - 3), "...");
    _vDisp(uiLine, szBuf);
}

void vDispMidVarArg(uint uiLine, char *pszFormat, ...)
{
    uchar szBuf[200];
    va_list args;
    va_start(args, pszFormat);
    vsprintf((char *)szBuf, (char *)pszFormat, args);
    va_end(args);
    if (strlen((char *)szBuf) > _uiGetVCols())
        strcpy((char *)(szBuf + _uiGetVCols() - 3), "...");
    vDispMid(uiLine, szBuf);
}

// 变长参数显示内容到辅助显示器
// In  : uiLine    : 显示行
//       pszFormat : 显示格式串
void vDispVarArg2(uint uiLine, char *pszFormat, ...)
{
    /*
	uchar szBuf[32];
    va_list args;
    va_start(args, pszFormat);
    vsprintf((char*)szBuf, (char*)pszFormat, args);
    va_end(args);
	_vDisp2(uiLine, szBuf);
	*/
}

/*
// 变长参数打印内容
// In  : pszFormat : 显示格式串
// Ret : 同_uiPrint()
uint uiPrintVarArg(char *pszFormat, ...)
{
	uchar szBuf[300];
	uint  uiRet;

    va_list args;
    va_start(args, pszFormat);
    vsprintf((char*)szBuf,(char*)pszFormat, args);
    va_end(args);
	uiRet = _uiPrint(szBuf);
	return(uiRet);
}
*/

// 清除部分显示行
// In  : uiLine : 该行以后会被清除
void vClearLines(uint uiLine)
{
    uint i;
    for (i = uiLine; i <= _uiGetVLines(); i++)
        _vDisp(i, " ");
}

// 14. 基础函数
// 14.1 将两个串按字节异或
// In       : psVect1  : 目标串
//            psVect2  : 源串
//            iLength  : 字节数
void vXor(uchar *psVect1, const uchar *psVect2, int iLength)
{
    int i;

    for (i = 0; i < iLength; i++)
        psVect1[i] ^= psVect2[i];
}

// 14.2 将两个串按字节或
// In       : psVect1  : 目标串
//            psVect2  : 源串
//            iLength  : 字节数
void vOr(uchar *psVect1, const uchar *psVect2, int iLength)
{
    int i;

    for (i = 0; i < iLength; i++)
        psVect1[i] |= psVect2[i];
}

// 14.3 将两个串按字节与
// In       : psVect1  : 目标串
//            psVect2  : 源串
//            iLength  : 字节数
void vAnd(uchar *psVect1, const uchar *psVect2, int iLength)
{
    int i;

    for (i = 0; i < iLength; i++)
        psVect1[i] &= psVect2[i];
}

// 14.4 将二进制源串分解成双倍长度可读的16进制串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
void vOneTwo(const uchar *psIn, int iLength, uchar *psOut)
{
    static const uchar aucHexToChar[17] = "0123456789ABCDEF";
    int iCounter;

    for (iCounter = 0; iCounter < iLength; iCounter++)
    {
        psOut[2 * iCounter] = aucHexToChar[(psIn[iCounter] >> 4)];
        psOut[2 * iCounter + 1] = aucHexToChar[(psIn[iCounter] & 0x0F)];
    }
}

// 14.5 将二进制源串分解成双倍长度可读的16进制串, 并在末尾添'\0'
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : pszOut   : 目标串
void vOneTwo0(const uchar *psIn, int iLength, uchar *pszOut)
{
    vOneTwo(psIn, iLength, pszOut);
    if (iLength < 0)
        iLength = 0;
    pszOut[2 * iLength] = 0;
}
/*
static uchar toupper(uchar c)
{
    if (c >= 'a' && c <= 'z')
        c -= 0x20;
    return (c);
}
*/

// 14.6 将可读的16进制表示串压缩成其一半长度的二进制串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
// Attention: 源串必须为合法的十六进制表示，大小写均可
//            长度如果为奇数，函数会靠近到比它大一位的偶数
void vTwoOne(const uchar *psIn, int iLength, uchar *psOut)
{
    uchar ucTmp;
    int i;

    for (i = 0; i < iLength; i += 2)
    {
        ucTmp = psIn[i];
        if (ucTmp > '9')
            ucTmp = toupper(ucTmp) - 'A' + 0x0a;
        else
            ucTmp &= 0x0f;
        psOut[i / 2] = ucTmp << 4;

        ucTmp = psIn[i + 1];
        if (ucTmp > '9')
            ucTmp = toupper(ucTmp) - 'A' + 0x0a;
        else
            ucTmp &= 0x0f;
        psOut[i / 2] += ucTmp;
    } // for(i=0; i<uiLength; i+=2) {
}

// 14.7 将二进制串转变成长整数
// In       : psBinString : 二进制串，高位在前
//            iLength     : 二进制串长度，可为{1,2,3,4}之一
// Ret      : 转变后的长整数
ulong ulStrToLong(const uchar *psBinString, int iLength)
{
    ulong l;
    int i;

    for (i = 0, l = 0l; i < iLength; i++)
        l = l * 256 + (long)psBinString[i];
    return (l);
}

// 14.8 将长整数转变成二进制串
// In       : ulLongNumber : 要转变的长整数
//            iLength      : 转变之后二进制串长度
// Out      : psBinString  : 转变后的二进制串，高位在前
void vLongToStr(ulong ulLongNumber, int iLength, uchar *psBinString)
{
    int i;

    for (i = iLength - 1; i >= 0; i--, ulLongNumber /= 256l)
        psBinString[i] = (uchar)(ulLongNumber % 256);
}

// 14.9 将十六进制可读串转变成长整数
// In       : psHexString : 十六进制串
//            iLength     : 十六进制串长度，0-8
// Ret      : 转变后的长整数
ulong ulHexToLong(const uchar *psHexString, int iLength)
{
    ulong l;
    int i;
    ulong ulDigit;

    for (i = 0, l = 0l; i < iLength; i++)
    {
        if (psHexString[i] >= '0' && psHexString[i] <= '9')
            ulDigit = psHexString[i] - '0';
        else if (psHexString[i] >= 'A' && psHexString[i] <= 'Z')
            ulDigit = psHexString[i] - 'A' + 10;
        else if (psHexString[i] >= 'a' && psHexString[i] <= 'z')
            ulDigit = psHexString[i] - 'a' + 10;
        l = l * 16 + ulDigit;
    }
    return (l);
}

unsigned long long ullHexToLongLong(const uchar *psHexString, int iLength)
{
    unsigned long long l;
    int i;
    ulong ulDigit;

    for (i = 0, l = 0L; i < iLength; i++)
    {
        if (psHexString[i] >= '0' && psHexString[i] <= '9')
            ulDigit = psHexString[i] - '0';
        else if (psHexString[i] >= 'A' && psHexString[i] <= 'Z')
            ulDigit = psHexString[i] - 'A' + 10;
        else if (psHexString[i] >= 'a' && psHexString[i] <= 'z')
            ulDigit = psHexString[i] - 'a' + 10;
        l = l * 16 + ulDigit;
    }
    return (l);
}

// 14.10 将长整数转变成十六进制可读串
// In       : ulLongNumber : 要转变的长整数
//            iLength      : 转变之后十六进制串长度
// Out      : psHexString  : 转变后的十六进制串
void vLongToHex(ulong ulLongNumber, int iLength, uchar *psHexString)
{
    uchar sBuf[4], szBuf[8];
    vLongToStr(ulLongNumber, 4, sBuf);
    vOneTwo(sBuf, 4, szBuf);
    memset(psHexString, '0', iLength);
    if (iLength > 8)
        memcpy(psHexString + iLength - 8, szBuf, 8);
    else
        memcpy(psHexString, szBuf + 8 - iLength, iLength);
}
void vLongToHex0(ulong ulLongNumber, int iLength, uchar *psHexString)
{
    vLongToHex(ulLongNumber, iLength, psHexString);
    psHexString[iLength] = 0;
}

// 14.11 将数字串转变成长整数
// In       : psString : 数字串，必须为合法的数字，不需要'\0'结尾
//            iLength  : 数字串长度
// Ret      : 转变后的长整数
ulong ulA2L(const uchar *psString, int iLength)
{
    ulong l;
    int i;

    for (i = 0, l = 0l; i < iLength; i++)
    {
        if (psString[i] < '0' || psString[i] > '9')
            break;
        l = l * 10 + psString[i] - '0';
    }
    return (l);
}

// 14.12 内存复制, 类似memcpy()，在目标串末尾添'\0'
// In       : psSource  : 源地址
//            iLength   : 要拷贝的串长度
// Out      : pszTarget : 目标地址
void vMemcpy0(uchar *pszTarget, const uchar *psSource, int iLength)
{
    memcpy(pszTarget, psSource, iLength);
    pszTarget[iLength] = 0;
}

// 14.13 将压缩BCD码串转变成长整数
// In       : psBcdString : BCD码串，必须为合法的BCD码，不需要'\0'结尾
//            iLength     : BCD码串长度，必须小于等于12
// Ret      : 转变后的长整数
ulong ulBcd2L(const uchar *psBcdString, int iLength)
{
    uchar sTmp[24];

    ASSERT(iLength <= 12 && iLength >= 0);
    if (iLength > 12 || iLength < 0)
        return (0);
    vOneTwo(psBcdString, iLength, sTmp);
    return (ulA2L(sTmp, iLength * 2));
}

// 14.14 将长整数转变成压缩BCD码串
// In       : ulLongNumber : 要转变的长整数
//            iLength      : 转变之后BCD码串长度, 必须小于等于12
// Out      : psBcdString  : 转变后的BCD码串，高位在前
void vL2Bcd(ulong ulLongNumber, int iLength, uchar *psBcdString)
{
    uchar sTmp[24 + 1];

    ASSERT(iLength <= 12 && iLength > 0);
    if (iLength > 12 || iLength < 0)
        return;
    sprintf((char *)sTmp, "%024lu", ulLongNumber);
    vTwoOne(sTmp + 24 - iLength * 2, iLength * 2, psBcdString);
}

// 14.15 将二进制源串分解成双倍长度可读的3X格式串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
void vOneTwoX(const uchar *psIn, int iLength, uchar *psOut)
{
    int iCounter;

    for (iCounter = 0; iCounter < iLength; iCounter++)
    {
        psOut[2 * iCounter] = 0x30 + (((psIn[iCounter] >> 4)) & 0x0F);
        psOut[2 * iCounter + 1] = 0x30 + (psIn[iCounter] & 0x0F);
    }
}

// 14.16 将二进制源串分解成双倍长度可读的3X格式串, 并在末尾添'\0'
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : pszOut   : 目标串
void vOneTwoX0(const uchar *psIn, int iLength, uchar *pszOut)
{
    vOneTwoX(psIn, iLength, pszOut);
    if (iLength < 0)
        iLength = 0;
    pszOut[2 * iLength] = 0;
}

// 14.17 将3X格式串压缩一半长度的二进制串
// In       : psIn     : 源串
//            iLength  : 源串长度
// Out      : psOut    : 目标串
void vTwoOneX(const uchar *psIn, int iLength, uchar *psOut)
{
    int i;
    for (i = 0; i < iLength; i += 2)
        psOut[i / 2] = (psIn[i] << 4) | (psIn[i + 1] & 0x0F);
}

// 14.18 检测存储内容
// In       : pMem     : 内存指针
//            ucChar   : 检测字符
//            iLength  : 检测长度
// ret      : 0        : 内存区域全部是字符ucChar
//            1        : 内存区域不全是字符ucChar
int iMemTest(void *pMem, uchar ucChar, int iLength)
{
    uchar *p;
    p = (uchar *)pMem;
    while (iLength-- > 0)
        if (*p++ != ucChar)
            return (1);
    return (0);
}

//去除字符串尾部空格,字符串须'\0'结尾
char *rtrim(char *s)
{
    int i;

    if (s == NULL)
        return NULL;

    i = strlen(s) - 1;
    while (i >= 0 && (s[i] == ' ' || s[i] == '\t'))
    {
        i--;
    };
    s[i + 1] = '\0';

    return s;
}

//去除字符串尾部空格和F,字符串须'\0'结尾
char *rtrimF(char *s)
{
    int i;

    if (s == NULL)
        return NULL;

    i = strlen(s) - 1;
    while (i >= 0 && (s[i] == 'F'))
    {
        i--;
    };
    s[i + 1] = '\0';

    return s;
}

// 带超时的读取按键
// in  : iTimeout : 超时时间(秒), 0表示没有超时
// ret : >=0      : 按键
//       -3       : 超时
int iGetKeyWithTimeout(uint uiTimeout)
{
    ulong ulTimer;
    _vSetTimer(&ulTimer, uiTimeout * 100L);
    while (uiTimeout == 0 ? 1 : _uiTestTimer(ulTimer) == 0)
    {
        if (_uiKeyPressed())
            return ((int)_uiGetKey());
    }
    return (-3); // 超时
}

// 带超时的读取特定按键
// in  : iTimeout : 超时时间(秒), 0表示没有超时
// ret : =0      : 按键
//       -3       : 超时
int iGetSpecialKeyWithTimeout(uint uiTimeout, uint uiSpeKey)
{
    ulong ulTimer;

	Line;
    _vSetTimer(&ulTimer, uiTimeout * 100L);
	Line;
	while (uiTimeout == 0 ? 1 : _uiTestTimer(ulTimer) == 0)
    {
        if (_uiKeyPressed() && uiSpeKey == _uiGetKey())
            //return ((int)_uiGetKey());
			return 0;
	}
	Line;
    return (-3); // 超时
}

/*
ucMode:   0x01-接触式IC卡   WAIT_IC
          0x02-非接IC卡     WAIT_NFC
          0x04-磁条卡       WAIT_MAG
          0x08-手输		     WAIT_MAN
          0x10-不允许取消   WAIT_NOESC
          0x20-不允许升降级,仅磁条和IC都允许时有效 WAIT_NOTURN
uiTimeOut:  超时时间,秒
return:  1:接触IC卡 2:非接IC卡 3:磁条卡 4:IC失败后的磁条卡(fallback) 5:手输卡号
        -1:取消    -2:超时    -3:失败
*/
int iWaitCard(uchar ucMode, uchar *pszTitle, uchar *pszPrompt, uint uiTimeOut)
{
    uchar ucModeBak;
    uchar ucDispFlag = 1;
    char szPrompt1[30];
    char szPrompt2[30];
    uchar szTmp[50];
    char *p;
    ulong ulTimer;
    uchar ucFallbackFlag=0;
    int retCode=-3;
    char ledflag=0;
    uint uiRet;
    uchar ucPicFlag=0;
    uchar ucPinPos,ucPinPosFlag=1;	
    int iRet;
    uint uiKey;
    uint ucInLength = 0;
    uchar  ucSPan[23+1] = {0}; //手输卡号
    int ucI,ucJ;
    //char cIncard=0;

//    ulong ultk1, ultk2;
    //ultk1=get_tick();

    if ((ucMode & 0x0F) == 0)
        return -3;

    ucMode |=  0x20;//不允许降级
	
    //自动升降级,原定0x80为允许升降级,后改为默认允许
    if ((ucMode & 0x04) && (ucMode & 0x03) && (ucMode & 0x20) == 0)
        ucMode |= 0x80;
    ucMode &= 0x9F; //去除无效设置位
    ucModeBak = ucMode;

    if(ucMode & 0x02)
    {
        _vSetAllLed(1, 0, 0, 0);
        ucPicFlag=1;
    }
    
#ifdef ENABLE_PRINTER
    ucPicFlag=0;
#endif


    memset(gl_ucSPan,0x00,sizeof(gl_ucSPan));
    _vFlushKey();
    if (ucMode & 0x04)
        _uiMagReset();
    if (uiTimeOut)
        _vSetTimer(&ulTimer, uiTimeOut * 100);
    while (1)
    {
        if (ucDispFlag)
        {            
            switch (ucMode & 0x0F)
            {
            case 0x01:
                strcpy(szPrompt1, "请插卡");
                break;
            case 0x02:
		if(gl_ucMobileTransFlag == 1)
			strcpy(szPrompt1, "请使用手机pay");
		else	
                	strcpy(szPrompt1, "请挥卡");
                break;
            case 0x04:
                strcpy(szPrompt1, "请刷卡");
                break;
            case 0x01 | 0x02:
                strcpy(szPrompt1, "请挥卡或插卡");
                break;
            case 0x01 | 0x04:
                strcpy(szPrompt1, "请插卡或刷卡");
                break;
            case 0x02 | 0x04:
                strcpy(szPrompt1, "请挥卡或刷卡");
                break;
            case 0x01 | 0x02 | 0x04:
                strcpy(szPrompt1, "请挥卡、插卡或刷卡");
                break;
           case 0x01 | 0x02 | 0x04 | 0x08:
                strcpy(szPrompt1, "请挥卡、插卡或刷卡");
	        strcpy(szPrompt2, "或输入卡号");
                break;				
            default:
                szPrompt1[0] = 0;
                break;
            }
            
            if(ucPicFlag)
            {
                _vCls();
                if (pszTitle)
                    vDispCenter(1, (char *)pszTitle, 1);
            }else
                vClearLines(2);

#ifdef  ST7789   
#else
                dbg("ucPicFlag:%d\n",ucPicFlag);
                if(ucPicFlag)
                {
                	/*
                	if(gl_ucMobileTransFlag == 1)			
                   		LCDDisplayPicture((uchar *)gImage_log, 45, 25, 36, 27);  
			else
				LCDDisplayPicture((uchar *)gImage_nfc, 55, 38, 36, 40+13);  
			*/
#ifdef LCD_GC9106 
		 if(gl_ucMobileTransFlag == 1)	
                    LCDDisplayPicture((uchar *)gImage_nfc, 60, 38, (160-60)/2, 128-63);
#else			
#ifdef ST7571                    
                    //LCDDisplayPicture((uchar *)gImage_nfc, 55, 38, 36, 64-38);
#else                 
                    if(gl_ucMobileTransFlag == 1)	
                    	LCDDisplayPicture((uchar *)gImage_nfc, 36, 25, (128-36)/2, 64-25-12);
#endif
#endif
                }			
                if (pszPrompt)
                    vDispCenter(2, (char*)pszPrompt, 0);
		if(gl_ucMobileTransFlag == 1)
			vDispCenter(5, szPrompt1, 0);
		else	
               {
               		vDispCenter(3, szPrompt1, 0);
			if(ucMode & WAIT_MAN)
			{     
				vDispCenter(4,szPrompt2, 0);
			}
        	}
#endif
            ucDispFlag = 0;
        }

        if(_uiKeyPressed())
        {
                 uiKey=_uiGetKey();
        	if((ucMode & 0x10) == 0 && uiKey == _KEY_ESC)
		{
			retCode=-1;
			break;
		}

		if(ucMode & WAIT_MAN && (uiKey >= _KEY_0 && uiKey  <= _KEY_9 ) )	
		{
		//	 ucPinPos = _uiGetVCols()-1;	
		       	vClearLines(2);			
		         _vDisp(2, "请输入卡号:");
			gl_ucSPan[0] = uiKey;
			ucInLength ++;
			_vFlushKey();
			while(1)
			{		
				if(ucPinPosFlag)
				{      
				       ucPinPos = 0;
					for(ucI= 0,ucJ = 0;ucI < ucInLength;ucI++,ucJ++)
					{
						if((!(ucI%4)) && (ucI != 0))
						{
						ucSPan[ucJ] = ' ';
						++ucJ;
						}
						ucSPan[ucJ] = gl_ucSPan[ucI];
					}
					ucSPan[ucJ] = 0;                                      

				       if(strlen(ucSPan)>_uiGetVCols())
					{
						_vDispAt((int)3, (int)0, ucSPan); 
          				  	_vDispAt((int)4, (int)0, ucSPan+_uiGetVCols());	
					}else {
						ucPinPos = _uiGetVCols()-strlen(ucSPan);
                                       		_vDispAt((int)3, (int)ucPinPos, ucSPan);  
					}
					   
					ucPinPosFlag = 0;				   
				}
		               if(ucInLength == 0)
				{
					_vDispAt(3, _uiGetVCols()-1, "_");	
				}
				if(_uiKeyPressed())
				{
					uiKey=_uiGetKey();
					if(uiKey >=  _KEY_0 && uiKey <= _KEY_9 && ucInLength < 19)
					 {
					        gl_ucSPan[ucInLength++] = uiKey;	
						_vDisp(3, " ");
						_vDisp(4, " ");
						ucPinPosFlag = 1;
						continue;
					}
					if(uiKey == _KEY_BKSP && ucInLength > 0)
					{     
						ucInLength--;			
						gl_ucSPan[ucInLength] = 0;
						_vDisp(3, " ");
						_vDisp(4, " ");
						ucPinPosFlag = 1;	
						continue;
					}
					if(uiKey == _KEY_ESC)
					{
						retCode =  -1; //取消
						return retCode;
					}
					if(ucInLength >= 16 && uiKey == _KEY_ENTER)	
					{
						retCode = 5; 
						return retCode;
					}
				}				
			}
		}
        }
		
        if (uiTimeOut && _uiTestTimer(ulTimer))
        {
            retCode=-2;
            break;
        }
#if 0
        if (cIncard==0 && (ucMode & 0x02) && _uiTestCard(0) && (ucMode & 0x01)==0) //允许挥卡且不允许插卡时, 若插卡了提醒做挥卡交易
        {
            _vCls();
            if (pszTitle)
                vDispCenter(1, (char *)pszTitle, 1);
            cIncard=1;
            if(ucMode&0x04)
                vMessageEx("请挥卡或刷卡", 100);
            else
                vMessageEx("请挥卡", 100);
            
            /*
            if (uiTimeOut)
                _vSetTimer(&ulTimer, uiTimeOut * 100);
            else
                _vSetTimer(&ulTimer, 30 * 100);            
            
            _vDisp(_uiGetVLines(), "请挥卡/使用云闪付");            
            while(!_uiTestTimer(ulTimer) && _uiTestCard(0))
            {
                _vDelay(10);
            }
            
            if(_uiTestTimer(ulTimer))
            {
                retCode=-2;
                break;
            }
            */
            
            if (uiTimeOut)
                _vSetTimer(&ulTimer, uiTimeOut * 100);
            ucDispFlag = 1; //刷新显示
            continue;
        }
#endif        
        if ((ucMode & 0x01) && _uiTestCard(0)) //插卡
        {
            if (_uiResetCard(0, NULL) > 0)
            {
                retCode=1;
                break;
            }
            else
            {
                if ((ucModeBak & 0x04) == 0 || (ucMode & 0x80) == 0)
                {
                    if(ucPicFlag)
                    {
                        _vCls();
                        if (pszTitle)
                            vDispCenter(1, (char *)pszTitle, 1);
                    }else
                        vClearLines(2);
                    vMessageEx("读取IC卡错误", 200);
                    retCode=-3;
                    break;
                }
                else
                {
                    if(ucPicFlag)
                    {
                        _vCls();
                        if (pszTitle)
                            vDispCenter(1, (char *)pszTitle, 1);
                    }else
                        vClearLines(2);
                    _vDisp(2, "读取IC卡错误");
                    _vDisp(3, "请刷卡");
                    vMessageEx(NULL, 150);
                    
                    if(ucMode&0x02)
                        _vSetAllLed(0, 0, 0, 0);
                    ucPicFlag=0;
                    ucMode = 0x04;
                    ucFallbackFlag=1;                    
                    ucDispFlag=1;
                    if (uiTimeOut)
                        _vSetTimer(&ulTimer, uiTimeOut * 100);
                    continue;
                }
            }
        }
        if ((ucMode & 0x02)) //挥卡
        {
            uiRet=_uiTestCard(8);
            if(uiRet==9)
            {
                if(ucPicFlag)
                {
                    _vCls();
                    if (pszTitle)
                        vDispCenter(1, (char *)pszTitle, 1);
                }else
                    vClearLines(2);
                _vDisp(3, "多卡冲突,请重试");
                vMessageEx(NULL, 150);
                
                ucDispFlag = 1; //刷新显示
                if (uiTimeOut)
                    _vSetTimer(&ulTimer, uiTimeOut * 100);

                continue;
            }
            //第一次,打开led灯
            if(uiRet==2)	//检测到非接卡
            {
                _vSetAllLed(1, 0, 0, 0);
                ledflag=1;
                retCode=2;
                break;
            }
            if(!ledflag)
            {
                _vSetAllLed(1, 0, 0, 0);
                ledflag=1;
            }
        }
        if ((ucMode & 0x04) && _uiMagTest())
        {
            if (_uiMagGet(2, szTmp) == 0)
            {
                dbg("mag2:[%s]\n", szTmp);
                p = strchr(szTmp, '=');
                if(p)
                {
                    if (ucFallbackFlag==0 && (p[5] == '2' || p[5] == '6')) //带有IC芯片，不允许刷卡交易
                    {
                        if ((ucMode & 0x03))
                        {
                            if(ucPicFlag)
                            {
                                _vCls();
                                if (pszTitle)
                                    vDispCenter(1, (char *)pszTitle, 1);
                            }else
                                vClearLines(2);
                            
                            _vDisp(2, "需使用IC芯片交易");
                            if ((ucMode&0x03)==0x03)
                                _vDisp(3, "请挥卡或插卡");
                            else if ((ucMode&0x01))
                                _vDisp(3, "请插卡");
                            else if ((ucMode&0x02))
                                _vDisp(3, "请挥卡");                                
                            vMessageEx(NULL, 150);
                            
                            ucDispFlag = 1; //刷新显示
                            ucMode &= 0x83; //去掉磁条允许标志
                            if (uiTimeOut)
                                _vSetTimer(&ulTimer, uiTimeOut * 100);
                            continue;
                        }
                    }
                    //刷卡成功, 返回3或4
                    retCode=3+ucFallbackFlag;
                    break;
                }
            }
  //          dbg("read mag2 fail.\n");
            _uiMagReset();
            continue;
        }
    }
//_WAIT_END:
    if (ucModeBak & 0x04)
        _uiMagClose();
    if((ucModeBak & 0x02))
    {        
        if(retCode!=2)
        {
            //_vSetAllLed(0, 0, 0, 0);      //关闭NFC,led灯会全灭
            _uiCloseCard(8);
        }
    }
    
    if(ucPicFlag)
    {
        _vCls();
        if (pszTitle)
            vDispCenter(1, (char *)pszTitle, 1);
    }else
        vClearLines(2);
    
    if((ucModeBak & 0x01)&&(retCode!=1))
        _uiCloseCard(0);
    
    if(retCode==-2)
    {
        vMessage("操作超时");
    }
    
    return retCode;
}

/*
//打印长日志,不超过1024*4.(原dbg无法打印超过960的日志)
void dbgLTxt(char *pszFormat, ...)
{
#ifdef JTAG_DEBUG
    char szBuf[4*1024+1], szTmp[810];
    int iLen;

    va_list args;
    va_start(args, pszFormat);
    vsnprintf(szBuf, sizeof(szBuf),pszFormat, args);
    va_end(args);
    
    iLen=strlen(szBuf);
    for(int i=0; i<iLen; i+=800)
    {
        vMemcpy0(szTmp, szBuf+i, (i+800<iLen)?800:iLen-i);
        //if(i+800<iLen)
        //    dbg("%s\n", szTmp);
        //else
            dbg("%s", szTmp);
        if(iLen>800)
            _vDelay(10);
    }
#endif
}

//urlcode编码后带有%号, 用var方式会出错
void dbgLTxt2(char *pszHead, char *pszMsg)
{
#ifdef JTAG_DEBUG    
    char szTmp[810];
    int iLen;

    dbg(pszHead);
    iLen=strlen(pszMsg);
    for(int i=0; i<iLen; i+=800)
    {
        vMemcpy0(szTmp, pszMsg+i, (i+800<iLen)?800:iLen-i);
        dbg("%s", szTmp);
        if(iLen>800)
            _vDelay(10);
    }
	dbg("\n");
#endif    
}

void dbgHex(uchar *head, uchar *in, uint len)
{
#ifdef JTAG_DEBUG    
    int i;
    uchar line[60];

    dbg("%s(%d):\n", head, len);

    for (i = 0; i < len; i++)
    {
        sprintf(line + (i%16)*3, "%02X ", in[i]);
        if((i+1)%16==0)
        {
            strcat(line, "\n");
            dbg(line);
            
            if((i+1)%160==0)
                _vDelay(2);         //打印10行后停顿一下,便于电脑端显示
        }
    }
    if(i%16)
    {
        strcat(line, "\n");
        dbg(line);
    }
#endif    
}
*/

//打印长日志,不超过1024*4.(原dbg无法打印超过960的日志)
void dbgLTxt(char *pszFormat, ...)
{
    int mode=0;
#ifdef JTAG_DEBUG
    mode=1;
#endif    
    if(mode || sg_ucUsbDebugLog)
    {
        char szBuf[4*1024+1], szTmp[810];
        int iLen;

        va_list args;
        va_start(args, pszFormat);
        vsnprintf(szBuf, sizeof(szBuf),pszFormat, args);
        va_end(args);
        
        iLen=strlen(szBuf);
        for(int i=0; i<iLen; i+=800)
        {
            vMemcpy0(szTmp, szBuf+i, (i+800<iLen)?800:iLen-i);
            //if(i+800<iLen)
            //    dbg("%s\n", szTmp);
            //else
                dbg("%s", szTmp);
            if(iLen>800)
                _vDelay(2);
        }
    }
}

//urlcode编码后带有%号, 用var方式会出错
void dbgLTxt2(char *pszHead, char *pszMsg)
{
    int mode=0;
#ifdef JTAG_DEBUG
    mode=1;
#endif
    if(mode || sg_ucUsbDebugLog)
    {
        char szTmp[810];
        int iLen;

        dbg(pszHead);
        iLen=strlen(pszMsg);
        for(int i=0; i<iLen; i+=800)
        {
            vMemcpy0(szTmp, pszMsg+i, (i+800<iLen)?800:iLen-i);
            dbg("%s", szTmp);
            if(iLen>800)
                _vDelay(2);
        }
        dbg("\n");
    }
}

void dbgHex(uchar *head, uchar *in, uint len)
{
    int mode=0;
#ifdef JTAG_DEBUG
    mode=1;
#endif
    if(mode || sg_ucUsbDebugLog)
    {
        int i;
        uchar line[60];

        dbg("%s(%d):\n", head, len);

        for (i = 0; i < len; i++)
        {
            sprintf(line + (i%16)*3, "%02X ", in[i]);
            if((i+1)%16==0)
            {
                strcat(line, "\n");
                dbg(line);
                
                if((i+1)%160==0)
                    _vDelay(2);         //打印10行后停顿一下,便于电脑端显示
            }
        }
        if(i%16)
        {
            strcat(line, "\n");
            dbg(line);
        }
    }
}
	
void dbgHexEx(uchar *head, uchar *in, uint len,uchar const *pasFun, int displine)
{
#ifdef JTAG_DEBUG    
    int i;
    uchar line[60];

    dbg("%s(%d) %s %d\n", head, len,pasFun,displine);

    for (i = 0; i < len; i++)
    {
        sprintf(line + (i%16)*3, "%02X ", in[i]);
        if((i+1)%16==0)
        {
            strcat(line, "\n");
            dbg(line);
            
            if((i+1)%160==0)
                _vDelay(2);         //打印10行后停顿一下,便于电脑端显示
        }
    }
    if(i%16)
    {
        strcat(line, "\n");
        dbg(line);
    }
#endif    
}


//按指定长度切割带gbk/gb中文的字符串, 返回切割长度(len或len-1)
int iSplitGBStr(unsigned char *in, unsigned int len)
{
    unsigned char *p;

    if(in==NULL || in[0]==0 || len<2)
        return 0;

    if(strlen((char*)in)<=len)
        return strlen((char*)in);

#if 0       //GB2312
    if(in[len-1]<0x80)
        return len;
#endif

    p=in;
    while(p<in+len-2)
    {
        if(*p>=0x80)
        {
            p+=2;
        }else
			p++;
        continue;
    }

    if(p==in+len-2)
    {
        if(p[0]>=0x80)
            return len;
        if(p[0]<0x80 && p[1]<0x80)
            return len;
        else
            return len-1;
    }else
    {
        if(p[0]>=0x80)
            return len-1;
        else
            return len;  
    }
}

//拆分指定字符分隔的字符串
//out为out[m][n]二维数组
//返回得到的字符串个数
int iSplitSepStr(char *in, unsigned char sep, char *out, int m, int n)
{
    char *p0, *p1;
    int i, len;

    p0=in;
    for(i=0; i<m; i++)
    {
        p1=strchr(p0, sep);
        if(p1==NULL)
        {
            strncpy(out+i*n, p0, n-1);
            out[i*n+n-1]=0;
            return i;
        }
        len=(p1-p0)>n-1?n-1:p1-p0;
        memcpy(out+i*n, p0, len);
        out[i*n+len]=0;
        p0=p1+1;
        continue;
    }
    return i;
}
/*
void vSetFailTransLed(void)
{
    _vSetAllLed(0, 0, 0, 1);
}
*/

int iMagCard(void)
{
	uchar szTmp[50];
	uint uiRet;
	uint uiKey;
	char *p;
	ulong ulTimer;
	
    	if ( _uiMagTest())
    	{
            if (_uiMagGet(2, szTmp) == 0)
            {   
                dbg("mag2:[%s]\n", szTmp);
                p = strchr(szTmp, '=');		
                if(p)
                {  
                    if ((p[5] == '2' || p[5] == '6')) //带有IC芯片，不允许刷卡交易
                    {             
                        _vCls();
                        vDispMid(2, "本卡为IC卡");
			vDispMid(3, "请挥卡或插卡");   
			_vFlushKey();
			_vSetTimer(&ulTimer, 60*100);
			while(1) 
			{
				if (_uiTestCard(0)) //插卡
				{
					if(gl_SysInfo.ucNFCTransPrior && gl_SysInfo.ucDefaultTrans == 1)
					{
					       vClearLines(2);
						vDispMid(3, "请挥卡,请使用云闪付"); 
						vMessageEx(NULL, 3000);  
						vTakeOutCard();
						return -1;
					}
					if (_uiResetCard(0, NULL) > 0)
					{
					 //        _uiCloseCard(0);
						return 1;	 
					
					}   
					else
			            	{
			            	       vClearLines(2);
						vDispMid(2, "读取IC卡错误");
						vDispMid(3, "请刷卡");
						vMessageEx(NULL, 3000);  
						vTakeOutCard();
						return -1;
			                }			
			         }

				uiRet=_uiTestCard(8);
				if(uiRet==9)
				{
					vClearLines(2);
					vDispMid(3, "多卡冲突,请重试");
					vMessageEx(NULL, 150);
					continue;
				}
				//第一次,打开led灯
				if(uiRet==2)	//检测到非接卡
				{
					_vSetAllLed(1, 0, 0, 0);
					return 2;
				}   	

				if(_uiKeyPressed())
				{
					uiKey =_uiGetKey();
					if(uiKey ==_KEY_CANCEL)						    
					{
						return -1;	
					}
				}

				if(_uiTestTimer(ulTimer))
					return -1;
			}
		}				
                    //刷卡成功
                    _uiMagClose();		    
		    return 3;
            } 				
         }
   }
         
//	dbg("read mag2 fail.\n");
	_uiMagReset();
	return 0;
}


/*
ucMode:   0x01-接触式IC卡   WAIT_IC
          0x02-非接IC卡     WAIT_NFC
          0x04-磁条卡       WAIT_MAG
          0x10-不允许取消   WAIT_NOESC
          0x20-不允许升降级,仅磁条和IC都允许时有效 WAIT_NOTURN
uiTimeOut:  超时时间,秒
return:  1:接触IC卡 2:非接IC卡 3:磁条卡 4:IC失败后的磁条卡(fallback)
        -1:取消    -2:超时    -3:失败
*/

int iInsertCard(void)
{
	if (_uiTestCard(0)) //插卡
	{
	       _vCls();
		if(gl_SysInfo.ucNFCTransPrior && gl_SysInfo.ucDefaultTrans == 1)
		{
		       vClearLines(2);
			vDispMid(3, "请挥卡,请使用云闪付"); 
			vMessageEx(NULL, 3000);  
			vTakeOutCard();
			return -1;
		}
		if (_uiResetCard(0, NULL) > 0)
		{
		 //        _uiCloseCard(0);
			return 1;	 
		
		}   
		else
            	{
            	       vClearLines(2);
			vDispMid(2, "读取IC卡错误");
			vDispMid(3, "请刷卡");
			vMessageEx(NULL, 3000);  
			vTakeOutCard();
			return -1;
                }			
            }	
	return 0;
}

void UsbDbgTxt(char *pszFormat, ...)
{
    extern uint8_t usbvcp_IsConnenct(void);
    if(sg_ucUsbDebugLog && usbvcp_IsConnenct())
    {
        char szBuf[4*1024+1], szTmp[810];
        int iLen;

        va_list args;
        va_start(args, pszFormat);
        vsnprintf(szBuf, sizeof(szBuf),pszFormat, args);
        va_end(args);
        
        iLen=strlen(szBuf);
        for(int i=0; i<iLen; i+=800)
        {
            vMemcpy0(szTmp, szBuf+i, (i+800<iLen)?800:iLen-i);
            _ucAsyncSendBuf((uchar*)szTmp, strlen(szTmp));
            if(iLen>800)
                _vDelay(10);
        }
    }
}

void vSetUsbDebug(uchar ucDebug)
{
    sg_ucUsbDebugLog=ucDebug;
}

uchar ucGetUsbDebug(void)
{
    return sg_ucUsbDebugLog;
}

