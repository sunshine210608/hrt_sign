#include <string.h>
#include <stdio.h>
#include <stdlib.h>
//#include <limits.h>
#include <mhscpu.h>

#include "ProjectConfig.h"
#include <FreeRTOS.h>
#include <task.h>
//user taks api
#include <rtos_tasks.h>
#include <semphr.h>
#include <time.h>
#include "VposFace.h"
#include "tcpcomm.h"
#include "mbedssl_cli.h"
#include "pub.h"
#include "preconn.h"

#ifdef SUPPORT_PRECONN

//单独屏蔽日志
#undef  dbg
#define dbg(x...)		do{}while(0)

static int      sg_iPreConnRunFlag=0;     //预拨号运行: 0-关  1-开(普通tcp) 2-开(TLS) 0x11-预拨号结束(tcp) 0x12-预拨号结束(TLS)
static int      sg_iPreConnStatus=0;      //预拨号结果: 1-连接成功 2-正在连接中 -1:连接失败

static char     sg_szHost[50+1];
static char     sg_szPort[5+1];
static char     sg_szBakHost[50+1];
static char     sg_szBakPort[5+1];


static TaskHandle_t handlePreConnTask;
static xSemaphoreHandle xSemaphore_preconn = NULL;
void preconn_lockinit(void)
{
    xSemaphore_preconn = xSemaphoreCreateMutex();
    if (xSemaphore_preconn == NULL)
    {
        err("xSemaphoreCreateMutex error\n");
    }
    else
    {
        if (xSemaphoreGive(xSemaphore_preconn) != pdTRUE)
        {
            err("xSemaphoreGive error\n");
        }
    }
}

void preconn_lock(void)
{
    if (xSemaphore_preconn != NULL)
    {
        xSemaphoreTake(xSemaphore_preconn, 60*1000);
    }
}

void preconn_unlock(void)
{
    if (xSemaphore_preconn != NULL)
    {
        xSemaphoreGive(xSemaphore_preconn);
    }
}

void vSetPreConnRunFlag(int flag)
{
    if(sg_iPreConnRunFlag==1 || sg_iPreConnRunFlag==2)
    {
        preconn_lock();
        preconn_unlock();
    }
    sg_iPreConnRunFlag=flag;
}
int iGetPreConnRunFlag(void)
{
	return sg_iPreConnRunFlag;
}

void vSetPreConnStatus(int status)
{
    sg_iPreConnStatus=status;
}
int iGetPreConnStatus(void)
{
    return sg_iPreConnStatus;
}
void vSetPreConnParam(char *pszHost, char *pszPort, char *pszBakHost, char *pszBakPort)
{
    if(pszHost && pszPort)
    {
        strncpy(sg_szHost, pszHost, sizeof(sg_szHost)-1);
        strncpy(sg_szPort, pszPort, sizeof(sg_szPort)-1);
    }
    
    if(pszBakHost && pszBakPort)
    {
        strncpy(sg_szBakHost, pszBakHost, sizeof(sg_szBakHost)-1);
        strncpy(sg_szBakPort, pszBakPort, sizeof(sg_szBakPort)-1);
    }
}

extern void vSetCommInitDisp(uchar flag);
void vPreConnWork(void)
{
    int ret=1;
    
    if(sg_iPreConnRunFlag!=1 && sg_iPreConnRunFlag!=2)       //未开预拨号或预拨号已结束
        return;
    if(sg_iPreConnStatus!=0)
        return;
    
    //关闭预拨号时通讯初始化错误显示
    vSetCommInitDisp(0);
    
    dbg("start preconn...\n");
    preconn_lock();
    
    //设置状态连接中
    vSetPreConnStatus(2);
    if(sg_iPreConnRunFlag==1)
    {
        //vDispMid(3, "连接服务器...");
        ret=iCommTcpConn(sg_szHost, sg_szPort, 60*1000);
        if(ret>0 && sg_szBakHost[0] && sg_szBakPort[0])
        {
            //vDispMid(3, "连接服务器2...");
            ret=iCommTcpConn(sg_szBakHost, sg_szBakPort, 60*1000);
            if(ret>0)
            {
                //vDispMid(3, "连接服务器...");
                ret=iCommTcpConn(sg_szHost, sg_szPort, 60*1000);
            }
        }
    }else if(sg_iPreConnRunFlag==2)
    {
        ret=iSSL_Init(NULL, NULL, NULL); 	//CA证书和 客户端证书和key
        //dbg("iSSL_Init ret=%d\n", ret);
        if(ret)
        {
            vMessageVarArg("SSL初始化失败:%d", ret);
            //iSSL_Free();
        }else
        {
            //vDispMid(3, "连接服务器...");
            ret=iSSL_Connect(sg_szHost, atoi(sg_szPort), 60);
            if(ret && sg_szBakHost[0] && sg_szBakPort[0])
            {
                //vDispMid(3, "连接服务器2...");
                ret=iSSL_Connect(sg_szBakHost, atoi(sg_szBakPort), 60);
                if(ret)
                {
                    //vDispMid(3, "连接服务器...");
                    ret=iSSL_Connect(sg_szHost, atoi(sg_szPort), 60);
                }
            }
        }
    }
    //设置连接结果
    if(ret==0)
        vSetPreConnStatus(1);
    else
        vSetPreConnStatus(-1);
    
    sg_iPreConnRunFlag=(sg_iPreConnRunFlag&0x0F)|0x10;      //预拨号结束
    
    preconn_unlock();

    //还原通讯初始化信息显示
    vSetCommInitDisp(1);
    
    dbg("preconn ret:%d\n", ret);
}

//ret:0-成功 1-失败
int iWaitPreConnResult(void)
{
    if(sg_iPreConnRunFlag==1 || sg_iPreConnRunFlag==2)
    {
        vDispMid(3, "连接服务器...");
        preconn_lock();         //等待预连接结束
        preconn_unlock();
    }

    dbg("sg_iPreConnStatus:%d\n", sg_iPreConnStatus);
    return (sg_iPreConnStatus==1?0:1);
}

void vPreConnReset(void)
{
    if(sg_iPreConnRunFlag==1 || sg_iPreConnRunFlag==2)
    {
        vClearLines(2);
        vDispMid(3, "处理中...");
        
        preconn_lock();         //等待预连接结束
        preconn_unlock();
        
        _vDisp(3, "");
    }
    
    //dbg("disconn %d\n", sg_iConnType);
    if((sg_iPreConnRunFlag&0x0F)==2)
    {
        //vMessage("ssl free");
        iSSL_DisConnect();
        iSSL_Free();
    }else
    {
        iCommTcpDisConn();
    }
    
    sg_iPreConnStatus=0;
    sg_iPreConnRunFlag=0;
}

void preconn_task(void *p)
{
    while (1)
    {
        vPreConnWork();
        vTaskDelay(100);
    }
}

void vStartPreConnTask(void)
{
    if(handlePreConnTask==NULL)
    {
        preconn_lockinit();
        xTaskCreate((TaskFunction_t)preconn_task,  "preconn_task", 1024, NULL, 2, (TaskHandle_t *)&handlePreConnTask);   
    }
}
#else

void vSetPreConnRunFlag(int flag) {}
int iGetPreConnRunFlag(void) {return 0;}
//void vSetPreConnStatus(int status) {}
int iGetPreConnStatus(void) {return -1;}
void vSetPreConnParam(char *pszHost, char *pszPort, char *pszBakHost, char *pszBakPort) {}

void vStartPreConnTask(void) {}

int iWaitPreConnResult(void) {return 1;}
void vPreConnReset(void) {}


#endif

int iConnReset(int flag)
{
    if(iGetPreConnRunFlag()==0)
    {
        //未开启预拨号
        if(flag==2)
        {
            iSSL_DisConnect();
            iSSL_Free();
        }else
            iCommTcpDisConn();
    }
    else
    {
        vPreConnReset();
    }
    return 0;
}
/*
int iIsPreConnTask(void)
{
#ifdef SUPPORT_PRECONN    
    if(xTaskGetCurrentTaskHandle()==handlePreConnTask)
        return 1;
    else
        return 0;
#else
    return 0;
#endif    
}
*/
