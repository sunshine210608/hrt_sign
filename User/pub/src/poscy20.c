/**************************************
File name     : POSCY20.C
Function      : Fit cy20 to virtual pos protocol
**************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mhscpu.h>

#include "user_projectconfig.h"
#include "SysTick.h"
#include "adc.h"
#include "battery.h"
#include "pmu.h"
#include "qspiflash.h"
#include "msr.h"
#include "rng.h"
#include "usbvcp.h"
#include "nfc.h"
#include "bpk.h"
#include "buzzer.h"
#include "ltp245.h"
#include "lcd.h"
#include "sci.h"
#include "rtc.h"
#include "nfc.h"
#include "mh_rand.h"
#include "mh_des.h"
#include "mh_sm4.h"
#include "rtos_tasks.h"

#include "MH1XXX.H"
#include <iso7816_3.h>
#include "iso14443a.h"
#include "iso14443_4.h"
#include "lowpowermode.h"
#include "keyboard.h"
#include "SysInit.h"

#include "st7789.h"
#include "st7571.h"
#include "st7567.h"

//#define DEBUG
#include "gprs.h"
#include "gprs_at.h"
#include "wifi.h"
#include "wifi_at.h"
#include "sys_littlefs.h"
#include "des_sec.h"
#include "dcmi.h"
#include "common.h"
#include "displaybuf.h"
#include "DecodeLib.h"
#include "qrdecode.h"
#include "EMV_CONTECTLESS.h"
#include "serial.h"
#include "mbedtls/des.h"
#include "printer.h"
#include "EmvAppUi.h"

#include "vposface.h"
#include "debug.h"
#include "Util.h"
#include "NFC.h"

extern void vDispWaitQrImg(int init);

//是否支持硬件配置文件
#ifdef USE_LTE_4G
#define USE_HW_CONFIGFILE
#endif


#ifdef APP_LKL
//外部版本
//#define APP_VER     "V210309A"
//#define APP_VER     "V200426A"
//#define APP_VER     "V210325A"
//#define APP_VER     "V210426A"
//#define APP_VER     "V210519A"
#define APP_VER     "V210520A"

//生产版本确认 LKL_PROD_ENV 宏定义
//JSD内部版本号(LKL)
//#define INTERNALVER "1.0.9(L)"
#define INTERNALVER "1.1.8(L)"
#endif


#ifdef APP_CJT
//01:全版本 02:只有gprs 03:gprs+wifi
//#define APP_VER     "V2021011204"
//#define APP_VER     "V2020121702"
//#define APP_VER     "V2020101601"
//#define APP_VER     "V2020092302"
//#define APP_VER     "V2020121704"    //测试使用
//#define APP_VER       "V2021012004"
//#define APP_VER     "V2021022601"
//#define APP_VER     "V2021040701"
//#define APP_VER       "V2022122401"//"V2022011001" //01->02->03
#define CLI_VER       "JSDV1.1 "//客户版本
//JSD内部版本号(SDJ)
//#define INTERNALVER "HRTV22122401"//"HRTV22011001"
static const char* COMPLETEVER="CompleteVer=[V2023022401&HRTV23022401]";
#endif

static char sg_szAppVer[20]={0};
static char sg_szInternalVer[20]={0};

//远程更新之前，先结算，清数据，然后更新

#define XMEM_FILE_DIR "/SysMem"
#define XMEM_FILE_NAME "/SysMem/XMEM.DAT"
#define MAX_XMEM_SIZE (256 * 1024) //定义256K的内存文件
#define XMEM_JBGFILE_DIR "/SysMem/jbg"

#define KEY_MAP_SIZE 10

#ifdef ST7571               // 黑白屏128*96
#define SCREEN_WIDTH 21
#define SCREEN_HEIGHT 6
#endif

#ifdef ST7567               // 黑白屏128*64
#define SCREEN_WIDTH 21       
#define SCREEN_HEIGHT 5
#endif

#ifdef ST7789               // 彩屏320*240
#define SCREEN_WIDTH 26
#define SCREEN_HEIGHT 8
#endif

#ifdef LCD_GC9106               // 彩屏160*128
#define SCREEN_WIDTH 20       
#define SCREEN_HEIGHT 5
#endif

static uchar gl_ucKeyMap[KEY_MAP_SIZE + 1][2];
static uint gl_uiKeyBuf; // 最后一次按键, 0表示无按键

//extern const struct font_desc font_16x32;

//const char HARDTEST_VERSION[] = "1.1.8";
static track_data sg_msrdata[3];

//static unsigned char *qrpool;
static DecodeConfigTypeDef DecodeCfg;

extern ulong ulA2L(const uchar *psString, int iLength);
extern int iOK(ushort timeout);
extern void vMessage(char *pszMessage);
extern void MyApplication(void);

void vUserSleep(void);
void vUserWakeUp(void);
//extern UserFunc g_pUserSleepFunc;

// 1.   系统管理函数
// 1.1. 初始化POS系统，只能在程序起始处调用一次。
void _vPosInit(void)
{
    memset(&gl_ucKeyMap[0][0], 0, sizeof(gl_ucKeyMap));

    //_uiMagReset();
    srand(get_tick());

    KeyBoardClean();

    //pcd_poweron();
    //pcd_config('A');

    //LcdSetFont(&FONT_12X20);
    LcdClear();

#ifdef USE_LTE_4G    
    g_pUserSleepFunc = vUserSleep;
    g_pUserWakeUpFunc = (UserFunc)vUserWakeUp;    
#endif    

#ifdef ENABLE_PRINTER
    PrintSetDepth(500);//pl 500 
#endif    

#ifdef LCD_GC9106
    UG_FontSetVSpace(2);
    lcd_SetBackLight(100);
#endif

    _vBuzzer();
}

// 1.2. POS停机，不同的POS可能的表现有：下电、重启动、仅停止运行。
void _vPosClose(void)
{
    pmu_poweroff();
    for (;;)
        _uiGetKey();
}

// 1.3. 取得POS型号
// 返    回：POS型号
uint _uiGetPosType(void)
{
#if PROJECT_CY20 == 1
    return (POS_TYPE_CY20);
#else
    return (POS_TYPE_CY21);
#endif
}

// 2.   显示
// 2.1. 取屏幕可显示行数
// 返    回：屏幕可显示行数
uint _uiGetVLines(void)
{
    return (SCREEN_HEIGHT);
}

// 2.2. 取屏幕可显示列数
// 返    回：屏幕可显示列数
uint _uiGetVCols(void)
{
    return (SCREEN_WIDTH);
}

// 2.3. 清除整个屏幕
void _vCls(void)
{
    LcdClear();
}

// 2.4. 在某行显示字符串
// 输入参数：uiLine  : 要显示的行数，从第一行开始
//           pszText : 要显示的字串
// 说    明：如不存在该行，立即返回。如字串长度超出屏幕宽度，截去超出的部分。
void _vDisp(uint uiLine, const uchar *pszText)
{
    _vDispX(uiLine, pszText, 0);
}

// 2.5. 在某行显示某属性的字符串
// 输入参数：uiLine  : 要显示的行数，从第一行开始
//           pszText : 要显示的字串
//           ucAttr  : 显示属性，可以是DISP_NORMAL、DISP_REVERSE、DISP_UNDERLINE的组合
// 说    明：如不存在该行，立即返回。如字串长度超出屏幕宽度，截去超出的部分。
void _vDispX(uint uiLine, const uchar *pszText, uchar ucAttr)
{
    int iLength;
    uchar szDispBuf[SCREEN_WIDTH + 1];

    iLength = strlen((char *)pszText) > SCREEN_WIDTH ? SCREEN_WIDTH : strlen((char *)pszText);
    memset(szDispBuf, ' ', SCREEN_WIDTH);
    memcpy(szDispBuf, pszText, iLength);
    szDispBuf[SCREEN_WIDTH] = 0;
    _vDispAtX(uiLine, 0, szDispBuf, ucAttr);
}

// 2.6. 在某行某列显示字符串
// 输入参数：uiLine  : 要显示的行数，从第1行开始
//           uiCol   : 要显示的列数，从第0列开始
//           pszText : 要显示的字串
// 说    明：如不存在该行，立即返回。如字串长度超出屏幕宽度，则绕到下一行。
//           如字串超出屏幕最后一行，截去超出的部分。
void _vDispAt(uint uiLine, uint uiCol, const uchar *pszText)
{
    _vDispAtX(uiLine, uiCol, pszText, 0);
}

// 2.7. 在某行某列带属性显示字符串
// 输入参数：uiLine  : 要显示的行数，从第1行开始
//           uiCol   : 要显示的列数，从第0列开始
//           pszText : 要显示的字串
//           ucAttr  : 显示属性，可以是DISP_NORMAL、DISP_REVERSE、DISP_UNDERLINE的组合
// 说    明：如不存在该行，立即返回。如字串长度超出屏幕宽度，则绕到下一行。
//           如字串超出屏幕最后一行，截去超出的部分。
void _vDispAtX(uint uiLine, uint uiCol, const uchar *pszText, uchar ucAttr)
{
    if (uiLine == 0 || uiLine > SCREEN_HEIGHT || uiCol >= SCREEN_WIDTH)
        return;
    if (ucAttr == DISP_REVERSE)
    {
        //设置反显
        //UG_SetBackcolor(C_BLACK);
        UG_SetBackcolor(C_BLUE);    //反显背景为蓝色
        UG_SetForecolor(C_WHITE);

        LcdPuts((char *)pszText, --uiLine, uiCol);

        //恢复正常设置
        UG_SetBackcolor(C_WHITE);
        UG_SetForecolor(C_BLACK);
    }
    else
    {
        LcdPuts((char *)pszText, --uiLine, uiCol);
    }
}

// 3. 键盘
// 3.1. 检测有否按键，立即返回
// 返    回：0 ：没有按键
//           1 ：检测到按键，可通过_uiGetKey()读取
uint _uiKeyPressed(void)
{
    uint16_t keyram;
    if (gl_uiKeyBuf)
        return (1);
    
    mdelay(5);
    if (KeyBoardRead(&keyram) == 0 && keyram <= 0xFF)
    {
        gl_uiKeyBuf = keyram;
        return 1;
    }
    else
        return 0;
}

// 3.2. 等待按键，直到读取到一个按键为止
// 返    回：0x00 - 0xff ：按键的ASCII码
uint _uiGetKey(void)
{
    int nPos;
    uchar ucKeyOut;
    uint16_t uiKey;
	
	while (!_uiKeyPressed())
	{
		//mdelay(10);
	}	  
	
	
    uiKey = (uint16_t)gl_uiKeyBuf;
    gl_uiKeyBuf = 0;
    switch (uiKey)
    {
    case KEY_0:
        ucKeyOut = _KEY_0;
        break;
    case KEY_1:
        ucKeyOut = _KEY_1;
        break;
    case KEY_2:
        ucKeyOut = _KEY_2;
        break;
    case KEY_3:
        ucKeyOut = _KEY_3;
        break;
    case KEY_4:
        ucKeyOut = _KEY_4;
        break;
    case KEY_5:
        ucKeyOut = _KEY_5;
        break;
    case KEY_6:
        ucKeyOut = _KEY_6;
        break;
    case KEY_7:
        ucKeyOut = _KEY_7;
        break;
    case KEY_8:
        ucKeyOut = _KEY_8;
        break;
    case KEY_9:
        ucKeyOut = _KEY_9;
        break;
    case KEY_OK:
        ucKeyOut = _KEY_ENTER;
        break;
    case KEY_CLR:
        ucKeyOut = _KEY_BKSP;
        break;
    case KEY_CANCEL:
        ucKeyOut = _KEY_CANCEL;
        break;
    case KEY_F1:
        ucKeyOut = _KEY_UP;
        break;
    case KEY_F2:
        ucKeyOut = _KEY_DOWN;
        break;
    case KEY_L:
        ucKeyOut = _KEY_FN;
        break;
    case KEY_POWER:
        ucKeyOut = _KEY_OFF;
        break;
    default:
        dbg("press other key:%u", uiKey);
        ucKeyOut = _KEY_OTHER;
    }
    for (nPos = 0; gl_ucKeyMap[nPos][0]; nPos++)
    {
        if (gl_ucKeyMap[nPos][0] == ucKeyOut)
        {
            ucKeyOut = gl_ucKeyMap[nPos][1];
            break;
        }
    }
    return (ucKeyOut);
}

// 3.3. 清除键盘缓冲区
void _vFlushKey(void)
{
    gl_uiKeyBuf = 0;
    KeyBoardClean();
}

// 3.4. 重新定义一个按键，使该键返回另外一个键码
// 输入参数：ucSource : 键盘上真正存在的键码
//           ucTarget : 重定义后该建返回的键码, 如果该键码与ucSource相同，则删除该键原先的定义
// 返    回：0        : 成功
//           1        : 重定义的键码太多
// 说    明：最多只能重定义10个键码
uint _uiMapKey(uchar ucSource, uchar ucTarget)
{
    int nPos;

    for (nPos = 0;; nPos++)
    {
        if ((gl_ucKeyMap[nPos][0] == ucSource) || (gl_ucKeyMap[nPos][0] == 0))
            break;
    }
    ASSERT(nPos <= KEY_MAP_SIZE);
    if (nPos >= KEY_MAP_SIZE)
        return (1);
    if (gl_ucKeyMap[nPos][0] == 0)
    {
        if (ucSource != ucTarget)
        {
            gl_ucKeyMap[nPos][0] = ucSource;
            gl_ucKeyMap[nPos][1] = ucTarget;
        }
    }
    else
    {
        if (ucSource != ucTarget)
        {
            gl_ucKeyMap[nPos][1] = ucTarget;
        }
        else
        {
            while (nPos < KEY_MAP_SIZE)
            {
                gl_ucKeyMap[nPos][0] = gl_ucKeyMap[nPos + 1][0];
                gl_ucKeyMap[nPos][1] = gl_ucKeyMap[nPos + 1][1];
                nPos++;
            }
        }
    }
    return (0);
}
/*
void vCheckKeyMap(void)
{
    int nPos=0;
    char buf[100];
    for (nPos = 0;nPos<=KEY_MAP_SIZE; nPos++)
    {
        sprintf(buf, "%d %02X->%02X", nPos, gl_ucKeyMap[nPos][0], gl_ucKeyMap[nPos][1]);
        _vDisp(_uiGetVLines(), buf);
        _uiGetKey();
    }
    _vDisp(_uiGetVLines(), "");
}
*/
//3.5
void _vSetKeyVoice(char OnOff)
{
    KeyBoardBeepOnOff((Boolean)(OnOff==0?FALSE:TRUE));
}

char _cGetKeyVoice(void)
{
    return KeyBoardBeepStatus()==FALSE?0:1;
}


// 4. IC卡
static uchar sg_NFCOpen = 0;
static uchar sg_ICCOpen = 0;
// 4.1. 检测卡片是否插入
// 输入参数：uiReader : 虚拟卡座号0-3接触式卡座 4-7SAM卡座 8-非接卡座
// 返    回：0        : 无卡
//           1        : 检测到接触卡
//           2        : 检测到非接卡
//           9        : 检测到多张卡(非接)
// 说    明：只能检测用户卡座,SAM卡座不支持
uint _uiTestCard(uint uiReader)
{
    int ret;
    if (uiReader != 0 && uiReader != 8)
        return 0;

    if (uiReader == 0)
    {
        if(sg_ICCOpen==0)
        {
            ICCOpen();
            //iso7816_device_init();
            sg_ICCOpen=1;
        }
        ret = iso7816_detect(SMARTCARD_SLOT);
        if (ret == 0)
            return 1;
        else
            return 0;
    }
    else
    {
#if 1
        //uiReader==8 非接卡
        if (sg_NFCOpen == 0)
        {
            NFCOpen();
            sg_NFCOpen = 1;
        }

        //ret = picc_detect();
        ret = NFCDetect();
		if (ret == NFC_ERR_NONE)
        {
            sg_NFCOpen=2;
            return 2;
        }
        else if(ret == NFC_ERR_COLLISION)
        {
			 //当卡离较远时可能会返回NFC_ERR_COLLISION,稍延迟后再检测一次
			mdelay(50);
			ret = NFCDetect();
			if (ret == NFC_ERR_NONE)
	        {
	            sg_NFCOpen=2;
	            return 2;
	        }
            if(ret == NFC_ERR_COLLISION)
                return 9;
        }
#endif
        return 0;
    }
}

// 4.2. 卡片复位
// 输入参数：uiReader    : 虚拟卡座号
// 输出参数：psResetData : ATR 数据
// 返    回：0           : 复位失败
//           >0          : 复位成功，返回ATR长度
uint _uiResetCard(uint uiReader, uchar *psResetData)
{
    int ret;
    uchar sTmp[50];
    //uchar mv=1;     //1:3v 2:1.8v

    if (uiReader != 0 && uiReader != 8)
        return 0;
    if (uiReader == 0)
    {
        if(sg_ICCOpen==0)
        {
            ICCOpen();            
            sg_ICCOpen=1;
        }
        SCI_SetMV(VCC_3000mV);
        iso7816_device_init();
        ret = iso7816_init(SMARTCARD_SLOT, VCC_3000mV | SPD_1X, sTmp);
        if(ret || sTmp[0]==0 || sTmp[0]> sizeof(sTmp) - 1)
        {
            iso7816_close(SMARTCARD_SLOT);
            _vDelay(5);         //iso7816_close后需适当延时才能调用iso7816_device_init
            SCI_SetMV(VCC_1800mV);
            iso7816_device_init();
            ret = iso7816_init(SMARTCARD_SLOT, VCC_1800mV | SPD_1X, sTmp);
        }
        dbg("iso7816_init ret:%d, %02X %02X %02X %02X\n", ret, sTmp[0], sTmp[1], sTmp[2], sTmp[3]);
        if (ret == 0 && sTmp[0]>0 && sTmp[0] < sizeof(sTmp) - 1)
        {
            if (psResetData)
                memcpy(psResetData, sTmp + 1, sTmp[0]);
            return sTmp[0];
        }
        else
            return 0;
    }
    else
    {
        if(sg_NFCOpen==2)
        {
            if(psResetData)
                memcpy(psResetData, "\x3B\x68\x00\x00", 4);
            return 4;
        }
        dbg("*** reset contactless card. ***\n\n");
    }

    return 0;
}

// 4.3. 卡片下电
// 输入参数：uiReader    : 虚拟卡座号
// 返    回：0           : 成功
//           1           : 失败
uint _uiCloseCard(uint uiReader)
{
    if (uiReader != 0 && uiReader != 8)
        return 1;

    if (uiReader == 0)
    {
        if(sg_ICCOpen)
        {
            iso7816_close(SMARTCARD_SLOT);
            ICCClose();
            sg_ICCOpen=0;
        }
    }else
    {
        if(sg_NFCOpen)
            NFCClose();
        sg_NFCOpen = 0;
    }

    return 0;
}

// 4.4. 执行IC卡指令
// 输入参数：uiReader : 虚拟卡座号
//           pIn      : IC卡指令结构
// 输出参数：pOut     : IC卡返回结果
// 返    回：0        : 成功
//           1        : 失败
uint _uiExchangeApdu(uint uiReader, APDU_IN *pIn, APDU_OUT *pOut)
{
    return 1;
}

static int doApdu(uint uiReader, uchar *psIn, int iInLen, uchar *psOut, int *piOutLen)
{
    ST_APDU_RSP apdu_rsp;
    ST_APDU_REQ apdu_req;
    int ret;
    int slot;
    unsigned short outLen;

    if (uiReader == 8)
    {
        hexdumpEx("apduin:", psIn, iInLen);
        //ret=picc_apdu(psIn, iInLen, psOut, piOutLen);
        ret=NFCSendAPDU(psIn, iInLen, psOut, &outLen);
        dbg("picc_apdu ret:%d\n", ret);
        if(ret==0)
        {
            *piOutLen=outLen;
            hexdumpEx("apduout:", psOut, *piOutLen);
        }
        return ret;
    }else
    {
        if (uiReader < 4)
            slot = SMARTCARD_SLOT;
        else
            slot = SAMCARD_SLOT;

        memset(&apdu_rsp, 0, sizeof(apdu_rsp));
        memset(&apdu_req, 0, sizeof(apdu_req));
        if (iInLen == 4)
        {
            apdu_req.lc = 0;
            apdu_req.le = 0;
        }
        else if (iInLen == 5)
        {
            apdu_req.lc = 0;
            apdu_req.le = psIn[4];
        }
        else if (iInLen == 4 + 1 + psIn[4])
        {
            apdu_req.lc = psIn[4];
            apdu_req.le = 0;
        }
        else if (iInLen == 4 + 1 + psIn[4] + 1)
        {
            apdu_req.lc = psIn[4];
            apdu_req.le = psIn[iInLen - 1];
        }
        memcpy(apdu_req.cmd, psIn, 4);
        if (apdu_req.lc)
            memcpy(apdu_req.data_in, psIn + 5, apdu_req.lc);

        ret = iso7816_exchange(slot, AUTO_GET_RSP, &apdu_req, &apdu_rsp);
        if (ret == 0)
        {
            memcpy(psOut, apdu_rsp.data_out, apdu_rsp.len_out);
            psOut[apdu_rsp.len_out] = apdu_rsp.swa;
            psOut[apdu_rsp.len_out + 1] = apdu_rsp.swb;
            *piOutLen = apdu_rsp.len_out + 2;
        }
        return ret;
    }
}

// 4.5. 执行IC卡指令
// 输入参数：uiReader   : 虚拟卡座号
//           uiInLen    : Command Apdu指令长度
//           psIn       : Command APDU, 标准case1-case4指令结构
//           uiProtocol : 协议类型
//                        APDU_NORMAL        标准APDU, 不自动处理61xx、6Cxx
//                        APDU_AUTO_GET_RESP 标准APDU, 自动处理61xx、6Cxx
//                        APDU_EMV           执行EMV协议
//                        APDU_SI            执行社保2.0协议
//           puiOutLen  : Response APDU长度
// 输出参数：psOut      : Response APDU, RespData[n]+SW[2]
// 返    回：0          : 成功
//           1          : 失败
uint _uiDoApdu(uint uiReader, uint uiInLen, uchar *psIn, uint *puiOutLen, uchar *psOut, uint uiProtocol)
{
    int ret;
    uchar swa, swb;
    uchar sApduIn[300], sApduOut[300];
    uint  uiApduInLen, uiApduOutLen;

    if (uiReader != 0 && uiReader!=4 && uiReader != 8)
        return 1;

    uiApduInLen = uiInLen;
	memcpy(sApduIn, psIn, uiApduInLen);
	uiApduOutLen = 0;
	memset(sApduOut, 0, sizeof(sApduOut));
    ret = doApdu(uiReader, sApduIn, uiApduInLen, sApduOut, &uiApduOutLen);
    if (ret || uiApduOutLen<2)
        return 1;

    if (uiProtocol == APDU_AUTO_GET_RESP || uiProtocol == APDU_EMV)
    {
        swa=sApduOut[uiApduOutLen-2];
        swb=sApduOut[uiApduOutLen-1];
        switch (swa)
        {
        case 0x61:
            memcpy(sApduIn, "\x00\xC0\x00\x00", 4);
            sApduIn[4]=swb;
            uiApduInLen=5;
            ret = doApdu(uiReader, sApduIn, uiApduInLen, sApduOut, &uiApduOutLen);
            break;
        case 0x6c:
            sApduIn[uiApduInLen-1] = swb;
            ret = doApdu(uiReader, sApduIn, uiApduInLen, sApduOut, &uiApduOutLen);
            break;
        default:
            //uiInLen>=6  : case4
            if (uiProtocol == APDU_EMV && uiApduOutLen==2 && uiInLen>=6 &&
                    (swa == 0x63 || swa == 0x62 || (swa == 0x90 && swb != 0x00)) )  
            {
                memcpy(sApduIn, "\x00\xc0\x00\x00\x00", 5);
                uiApduInLen = 5;
                ret = doApdu(uiReader, sApduIn, uiApduInLen, sApduOut, &uiApduOutLen);
                if (ret || uiApduOutLen<2)
                    return 1;
                if (sApduOut[uiApduOutLen-2] == 0x61 || sApduOut[uiApduOutLen-2] == 0x6c)
                {
                    sApduIn[uiApduInLen-1] = sApduOut[uiApduOutLen-1];
                    ret = doApdu(uiReader, sApduIn, uiApduInLen, sApduOut, &uiApduOutLen);
                }
            }
            break;
        }
        if (ret != 0 || uiApduOutLen<2)
            return 1;
    }
    memcpy(psOut, sApduOut, uiApduOutLen);
    *puiOutLen = uiApduOutLen;

    return 0;
}

// 5. 打印机
#ifdef ENABLE_PRINTER
extern const struct font_desc Font_12x24;
extern const struct font_desc Font_18x36;
extern const struct font_desc Font_6x12;
static int sg_iFontNo=2;
#endif

// 5.1. 取打印机宽度
// 返    回：0  : 无打印机
//           >0 : 打印机可打印的最大宽度
uint _uiGetPCols(void)
{
#ifdef ENABLE_PRINTER
    int col;
    switch (sg_iFontNo)
    {
#if 1        
    case 1:
        col=21;
        break;
#endif    
    case 3:
        col=62;
        break;
    case 2:
    default:
        col=32;
        break;
    }
    return col;
#else
    return 0;
#endif    
}

void  _uiSetSpaceCols(int col,uchar *OutData)
{
#ifdef ENABLE_PRINTER	
    int i = 0;
    char space[62+1];
   	
    switch (sg_iFontNo)
    {       
    case 1:
	i = 21 - col;
        break;  
    case 3:
        i = 62 - col;
        break;
    case 2:
    default:
	i = 32 - col;	
        break;
    } 

     memset(space,' ',i);
     space[i ] = '\0';
     strcpy(OutData,space); 
#endif
     return;
}

extern void vDispCancelAndConfirm(char cancel, char confirm);
extern void vDispMid(uint uiLine, char *pszMsg);
int iDoPrintException(char bCancel)
{
#ifdef ENABLE_PRINTER
    uint status;
    int  ret;
    int  bExit=0;
  
    while(1)
    {
        _vDelay(10);
        status=_uiPrintGetStatus();
        if(status)
        { 
            dbg("PrintGetStatus:%u", status);
            if(bExit)
                return PRT_ERROR;
            if(status==PRT_STATUS_NO_PAPER)
            {
                vDispMid(3, "打印机缺纸,请装纸");                
            }else if(status==PRT_ERROR_BATLOW)
            {
                vDispMid(3, "电池电量低,请先充电");
            }else
            {
                vDispMid(3, "打印机状态异常");
            }
            
            if(bCancel)
            {
                //_vDisp(4, "[确认]重新检测,[取消]退出");
                vDispCancelAndConfirm(1, 1);
            }else
            {
                //_vDisp(_uiGetVLines(), "按[确认]重新检测");
                vDispCancelAndConfirm(0, 1);
            }
            ret=iOK(3*60);               //超时3分钟
            if(ret!=1 && bCancel)
            {
                if(ret==0)      //取消直接退出
                {
                    _vDisp(3, "");
                    _vDisp(_uiGetVLines(), "");
                    return PRT_ERROR;
                }
                bExit=1;        //超时或取消,再检测一次打印状态后退出
            }
            _vDisp(3, "");
            _vDisp(_uiGetVLines(), "");
            continue;
        }else
            return 0;
    }
#else
    return 0;
#endif    
}

// 5.2. 打印数据
// 输入参数：pszText : 要打印的字串
// 返    回：0 : 打印成功
//           PRT_STATUS_NO_PAPER	// 缺纸
//           PRT_STATUS_LESS_PAPER	// 纸少
//           PRT_ERROR_NO_PRINTER	// 没发现打印机
//           PRT_ERROR_OFFLINE		// 打印机脱机
//           PRT_ERROR				// 故障
// 说    明：打印字串超过打印机最大宽度时反绕到下一行。
//           '\n'字符会导致换行。
uint _uiPrint(uchar const *pszText, int iPrintFlag)
{
#ifdef ENABLE_PRINTER
    int ret;

    //if(pszText && pszText[0])
    if(pszText)
    {
        PrintPuts((char*)pszText);
    }
    ret=PRT_STATUS_OK;
    if(iPrintFlag)
    {
        while(1)
        {
            ret=PrintWrite();
            if(ret>=0)
            {
                ret=PRT_STATUS_OK;
                break;
            }
            if(iPrintFlag==2)   //不重打
                break;
            ret=iDoPrintException(1);
            if(ret)     //用户取消
                break;
        };
        if(ret)
            ret=PRT_ERROR;
    }
    return ret;
#else
    return 1;
#endif    
}

extern void vMemcpy0(uchar *pszTarget, const uchar *psSource, int iLength);
extern int iSplitGBStr(unsigned char *in, unsigned int len);

//format: 1-靠左 2-居中 3-靠右 11-靠左自动换行(打印多行) 12-靠右自动换行
//iPrintFlag: 0-不提交 1-提交打印,出错重打 2-提交打印,不重打
uint _uiPrintEx(int format, uchar const *pszText, int iPrintFlag)
{
 #ifdef ENABLE_PRINTER
    uchar szBuf[80],szBuf1[80];
    int col, len,len1,length;
    int ret;
    //uint uiStatus;

    if(pszText)
    {
        col=_uiGetPCols();
        len=strlen((char*)pszText);
        if(len<=col)
        {
            switch (format)
            {
            case 2:
                memset(szBuf, ' ', col);
                memcpy(szBuf+(col-len)/2, pszText, len);
                szBuf[len+(col-len)/2]=0;
                PrintPuts(szBuf);
                break;
            case 3:
	    case 12:			
                memset(szBuf, ' ', col);
                memcpy(szBuf+(col-len), pszText, len);
                szBuf[col]=0;
                PrintPuts(szBuf);
                break;
            case 1:
            default:
                PrintPuts((char*)pszText);
                break;
            }
        }else
        {
            if(format<=3)
            {
                ret=iSplitGBStr((uchar*)pszText, col);
                memcpy(szBuf, pszText, ret);
                szBuf[ret]=0;
                PrintPuts(szBuf);
            }else
            {
                uchar *p;
                //int line=0;

                dbg("print 2+ lines...\n");
                p=(uchar*)pszText;
		length = strlen(pszText);	
		len1 = 0;
                while(p[0]>0)
                {
                    len=iSplitGBStr(p, col);
                    if(len==0)
                        break;
                    /*
                    if(++line>5 && iPrintFlag)
                    {
                        PrintWrite();
                        line=1;
                    }*/                   
                    if(format == 11)
                    {   
                        vMemcpy0(szBuf, p, len);
                    	PrintPuts(szBuf);
                    }else if(format == 12)
                    {
			vMemcpy0(szBuf, p, len);			
			if((length - len1) < col)
			{
				memset(szBuf1, ' ', col);
				memcpy(szBuf1+(col-len), szBuf, len);
				szBuf1[col]=0;
				PrintPuts(szBuf1);
			}else			
                    		PrintPuts(szBuf);			
                    }
                    p+=len;  
		    len1 += len;			
                }
            }
        }
    }
    ret=PRT_STATUS_OK;
    if(iPrintFlag)
    {
        while(1)
        {
            ret=PrintWrite();
            if(ret>=0)
            {
                ret=PRT_STATUS_OK;
                break;
            }
            if(iPrintFlag==2)   //不重打
                break;
            ret=iDoPrintException(1);
            if(ret)     //用户取消
                break;
        };
        if(ret)
            ret=PRT_ERROR;
    }
    
    return ret;
#else
    return 1;
#endif
}

// 5.3 取得打印机状态
// 返    回: PRT_STATUS_OK			// OK
//           PRT_STATUS_NO_PAPER	// 缺纸
//           PRT_STATUS_LESS_PAPER	// 纸少
//           PRT_ERROR_NO_PRINTER	// 没发现打印机
//           PRT_ERROR_OFFLINE		// 打印机脱机
//           PRT_ERROR_BUSY  		// 打印机忙
//           PRT_ERROR_BATLOW  		// 终端电池电量低
//           PRT_ERROR				// 故障
uint _uiPrintGetStatus(void)
{
#ifdef ENABLE_PRINTER
    int stat = 0;
    stat = PrintGetStatus();
    if (stat)
    {
        if (stat & PRINTER_NOPAPER_MASK)
            return PRT_STATUS_NO_PAPER;
        if ((stat & PRINTER_HIGHTEMP_MASK) || (stat & PRINTER_SENSOR_MASK))
            return PRT_ERROR;
        if (stat & PRINTER_Writing_MASK)
            return PRT_ERROR_BUSY;
        if (stat & PRINTER_BATTERY_MASK)
            return PRT_ERROR_BATLOW;
        return PRT_ERROR;
    }
    return PRT_STATUS_OK;
#else
    return PRT_ERROR_NO_PRINTER;
#endif    
}

uint _uiPrintOpen(void)
{
#ifdef ENABLE_PRINTER
    struct font_desc *font_p = (struct font_desc *)&Font_12x24;
		
    int ret;  

#if 0//def PROJECT_CY20P_LITE
//打印的时候,必须保证4g模块处于非休眠状态,并且其 AT+QSCLK = 0
	if(gprs_getpowerstate() == VCC_POWERUP)
	{
		if (gprs_sleepenablestate() == 1)
		{
			//模块未唤醒，进行硬件唤醒
			gprs_wakeup();
		}
		ret = gprs_at_EnterSleepMode(0);
		if (ret != 0)
		{
			 dbg("printer open error %d",ret);
	    	_vCls();
	    	vMessage("打印机异常");
	    	return PRT_ERROR_4G_MODULE;
		}
	}
#endif

    ret= PrintOpen();
    if (ret < 0) {
        dbg("printer open error %d",ret);
        _vCls();
        vMessage("打印机异常");
        return PRT_ERROR_NO_PRINTER;
    }
    
    ret=iDoPrintException(1);
    if(ret)
    {
        PrintClose();
        return ret;
    }
    
    /*
    while(1)
    {
        _vDelay(10);
        _vFlushKey();
        ret=_uiPrintGetStatus();
        if(ret)
        {
            if(ret==PRT_STATUS_NO_PAPER)
            {
                _vDisp(3, "打印机缺纸");                
            }else if(ret==PRT_ERROR_BATLOW)
                _vDisp(3, "电池电量低,请先充电");
            else
                _vDisp(3, "打印机状态异常");
            _vDisp(4, "[确认]重新检测,[取消]退出");
            if(iOK(60)!=-1)
            {
                _vDisp(3, "");
                _vDisp(4, "");
                continue;
            }else
            {
                _vDisp(3, "");
                _vDisp(4, "");
                _uiPrintClose();
                return PRT_ERROR;
            }
        }
        break;
    }
    */

	//PrintSetDepth(800);
    PrintLoadFont(font_p);    
    return 0;
#else
    vMessage("本机不支持打印");
    return 1;
#endif
}

uint _uiPrintClose(void)
{
#ifdef ENABLE_PRINTER
    PrintClose();
#endif
    updateLastEventTimestamp();         //更新休眠时间戳
    return 0;
}

//用1,2,3表示大中小三字体.
uint _uiPrintSetFont(int iFont)
{
#ifdef ENABLE_PRINTER    
    struct font_desc *font_p;

    int fontGBKCodeType;
    
#ifndef USE_GB2312   
    fontGBKCodeType=FONT_CODE_TYPE_GBK;
#else
    fontGBKCodeType=FONT_CODE_TYPE_GB2312;
#endif
    
    switch (iFont)
    {
#if 1//ndef USE_GB2312
    case 1:
        font_p = (struct font_desc *)&Font_18x36;
        FontSetChnCodeType(FONT_CODE_TYPE_GB2312);      //大字体为GB2312
        break;
#endif
    case 3:
        font_p = (struct font_desc *)&Font_6x12;
        FontSetChnCodeType(fontGBKCodeType);
        break;
    case 2:
    default:        //非CY20P时, default包含case 1
        font_p = (struct font_desc *)&Font_12x24;
        FontSetChnCodeType(fontGBKCodeType);
        break;
    }
    sg_iFontNo=iFont;
    PrintLoadFont(font_p);
#endif
    return 0;
}

// 6. 时间

// 6.1. 设置当前系统时间
// 输入参数：pszTime : 要设置的时间，格式:YYYYMMDDHHMMSS
// 返    回：0       : 成功
//           1       : 失败
uint _uiSetTime(uchar const *pszTime)
{
    struct tm tm_new;
    int year;
    int day;

    if(pszTime==NULL || strlen((char*)pszTime)!=14)
        return 1;
	rtc_gettime(&tm_new);
	
	year = ulA2L(pszTime, 4);
	tm_new.tm_mon = ulA2L(pszTime+4, 2);
	tm_new.tm_mday = ulA2L(pszTime+6, 2);

	tm_new.tm_hour = ulA2L(pszTime+8, 2);
	tm_new.tm_min = ulA2L(pszTime+10, 2);
	tm_new.tm_sec = ulA2L(pszTime+12, 2);

    if(year<2020 || year>2049)
        return 1;
    if(tm_new.tm_mon<1 || tm_new.tm_mon>12)
        return 1;
    if(tm_new.tm_mday<1 || tm_new.tm_mday>31)
        return 1;
    if((tm_new.tm_mon==4 || tm_new.tm_mon==6 || tm_new.tm_mon==9 || tm_new.tm_mon==11)
         && tm_new.tm_mday>30)
        return 1;
    
    //判断2月(是否闰年)
    if(tm_new.tm_mon==2)
    {
        day=(((0 == year%4)&&(0 != year%100)) ||(0 == year %400))?1:0 + 28;
        if(tm_new.tm_mday>day)
            return 1;
    }
            
    if(tm_new.tm_hour<0 || tm_new.tm_hour>23 || tm_new.tm_min<0 || tm_new.tm_min>59
        || tm_new.tm_sec<0 || tm_new.tm_sec>59)
        return 1;
    
    tm_new.tm_year = year - 1900;
    tm_new.tm_mon--;        //月份0-11

    dbg("settime:%d-%d-%d %02d:%02d:%02d\n",
        tm_new.tm_year, tm_new.tm_mon, tm_new.tm_mday, tm_new.tm_hour, tm_new.tm_min, tm_new.tm_sec);
    return rtc_settime(&tm_new);
}

uint _uiSetTime2(struct tm *tm_new)
{
    dbg("settime:%d-%d-%d %02d:%02d:%02d\n",
        tm_new->tm_year, tm_new->tm_mon, tm_new->tm_mday, tm_new->tm_hour, tm_new->tm_min, tm_new->tm_sec);
    
    if(tm_new->tm_year<2020-1900 || tm_new->tm_year>2049-1900)
        return 1;
	rtc_settime(tm_new);
	dbg("settime ok.\n");
	return 0;
}

// 6.2. 读取系统时间
// 输出参数：pszTime : 当前系统时间，格式:YYYYMMDDHHMMSS
void _vGetTime(uchar *pszTime)
{
    struct tm t1;

    rtc_gettime(&t1);
    sprintf((char *)pszTime, "%04d%02d%02d%02d%02d%02d",
            t1.tm_year + 1900, t1.tm_mon + 1, t1.tm_mday, t1.tm_hour, t1.tm_min, t1.tm_sec);
}

ulong _ulGetTickTime(void)
{
    return get_tick();
}

long _ulCTime2Stamp(char *pszTime)  
{  
    struct tm stm;
    char tmp[30];
    //int iY, iM, iD, iH, iMin, iS;  
  
    memset(&stm,0,sizeof(stm));  
  
    if(strlen(pszTime)!=14)
        return 0;

    sprintf(tmp, "%.4s-%.2s-%.2s %.2s:%.2s:%.2s", pszTime, pszTime+4, pszTime+6, pszTime+8, pszTime+10, pszTime+12);
    stm.tm_year=atoi(tmp)-1900;  
    stm.tm_mon=atoi(tmp+5)-1;  
    stm.tm_mday=atoi(tmp+8);  
    stm.tm_hour=atoi(tmp+11);  
    stm.tm_min=atoi(tmp+14);  
    stm.tm_sec=atoi(tmp+17);

    return mktime(&stm)-8*60*60;  
} 

void _vTimeStamp2CTime(ulong ulTimeStamp, uchar *pszDateTime)
{
    struct tm* ptm;
    
    ulTimeStamp+=(8*60*60);
    ptm = localtime((time_t *)&ulTimeStamp); //gmtime
    if(ptm==NULL){
        dbg("gmtime error\n");
        pszDateTime[0]=0;
        return;
    }
    sprintf((char *)pszDateTime, "%04d%02d%02d%02d%02d%02d",
            ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
}

ulong _ulGetTimeStamp(void)
{
    #if 0
    {   
        uchar time[14+1], time2[14+1];
        ulong ttt;
        ulong ts=RTC_GetCounter() + RTC_GetRefRegister() - (8*60*60);
        _vGetTime(time);
        _vTimeStamp2CTime(ts, time2);
        ttt=_ulCTime2Stamp(time2);
        dbg("aa TimeStamp=%lu, GetTime=%s, GetTime2=%s, tt=%lu\n",
            ts, time, time2, ttt);
    }
    #endif
    return RTC_GetCounter() + RTC_GetRefRegister() - (8*60*60);  //减8小时时区   
}

// 6.3.设置计时器
// 输入参数：ulNTick   : 超时时间，单位为0.01秒
// 输出参数：pulTimer  : 计时器变量
// 说    明：精确到0.01秒
void _vSetTimer(ulong *pulTimer, ulong ulNTick)
{
    *pulTimer = get_tick() + ulNTick * 10;
}

// 6.4. 判断是否到达计时器变量所标明的时间
// 输入参数：ulTimer : 计时器变量
// 返    回：0       : 没有超时
//           1       : 超时
uint _uiTestTimer(ulong ulTimer)
{
    if (ulTimer < get_tick())
        return (1);
    return (0);
}

//检测一个指定定时器的当前值(1000毫秒的个数)。
// 输入参数：ulTimer : 计时器变量
// 返    回：定时器的剩余时间	(单位：1000ms)
uint _uiCheckTimer(ulong ulTimer)
{
      if (ulTimer < get_tick())
		return 0;
      return  (ulTimer - get_tick())/1000;
}

// 6.5. 延迟一段时间
// 输入参数：uiNTick : 延迟的时间，单位为0.01秒
void _vDelay(uint uiNTick)
{
    mdelay(uiNTick * 10);
}

// 7. 扩展内存

static void vGetMemFile(uint uiMemId, uchar *szFileName)
{
    switch(uiMemId)
    {
        case MEM_SYSINFO:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "sysinfo.dat");
            break;
        case MEM_SYSDATA:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "sysdata.dat");
            break;
        case MEM_OPER:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "oper.dat");
            break;            
        case MEM_CAPUBKEY:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "pubkey.dat");
            break;
        case MEM_AID:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "aid.dat");
            break;
        case MEM_SCR8583:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "scr8583.dat");
            break;
        case MEM_REV8583:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "rev8583.dat");
            break;
        case MEM_SETTLEINFO:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "settle.dat");
            break;        
        case MEM_QRTRANSREC:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "qrtrans.dat");
            break;
	case MEM_CARDBLACK:
            sprintf((char*)szFileName, "%s/%s", XMEM_FILE_DIR, "cardblack.dat");
            break;		
        default:
            if(uiMemId>=MEM_TRANSREC_0 && uiMemId<MEM_TRANSREC_0+TRANS_FILE_NUM)
            {
                sprintf((char*)szFileName, "%s/%s%d.dat", XMEM_FILE_DIR, "trans", uiMemId-MEM_TRANSREC_0);
            }else
                szFileName[0]=0;
            break;
    }
    return;
}

uint _uiCheckXMem2(void)
{
    uchar szFileName[100];
    lfs_file_t file;
    int i, iRet;

    int fileids[]={MEM_QRTRANSREC, 
        MEM_TRANSREC_0, MEM_TRANSREC_1, MEM_TRANSREC_2, MEM_TRANSREC_3, MEM_TRANSREC_4, -1};

    //sys_lfs_remove("/SysMem/trans.dat");        //老的无效文件,删除
    for(i=0; fileids[i]>0; i++)
    {
        vGetMemFile(fileids[i], szFileName);
        if(szFileName[0]==0)
            return 2;
        
        //创建文件
        iRet = sys_lfs_file_open(&file, szFileName, LFS_O_CREAT | LFS_O_WRONLY);
        if (iRet != 0)
        {
            dbg("create xmem file fail:[%s] %d\n", szFileName, iRet);
            return 2;
        }else {
            dbg("create xmem file ok:[%s] %d\n", szFileName, iRet);
        }
        sys_lfs_file_close(&file);
    }
    return 0;
}

// 7.0. 检查/初始化扩展内存
// 输入参数：ucFlag : 1-检查扩展内存文件 0-c初始化扩展内存文件
// 返    回：0      :  成功
//           1      : 需要初始化
//           2      : 申请失败
//           3      : 其它错误
uint _uiCheckXMem(uchar ucFlag)
{
    uchar szFileName[100];
    struct lfs_info info;
    lfs_file_t file;
    int i, iRet;
    int fileids[]={MEM_SYSINFO, MEM_SYSDATA, MEM_OPER, MEM_CAPUBKEY, MEM_AID, MEM_REV8583, MEM_SCR8583, MEM_QRTRANSREC, 
        MEM_SETTLEINFO, MEM_TRANSREC_0, MEM_TRANSREC_1, MEM_TRANSREC_2, MEM_TRANSREC_3, MEM_TRANSREC_4,MEM_CARDBLACK, -1};

    if (ucFlag > 2)
        return (3);
  
    if(ucFlag)
    {
        //_uiCheckXMem2();
        for(i=0; fileids[i]>0; i++)
        {
            vGetMemFile(fileids[i], szFileName);
            if(sys_lfs_stat(szFileName, &info))
            {
                dbg("need init xmem file.\n");
                return 1;
            }
        }
        return 0;
    }

    //ucFlag==0, 以下为初始化内存文件操作

    //创建目录
    //sys_lfs_mkdir(XMEM_FILE_DIR);
    {
        struct lfs_info info;
        if (sys_lfs_stat(XMEM_FILE_DIR, &info))
        {
            if (sys_lfs_mkdir(XMEM_FILE_DIR))
            {
                err("sys_lfs_mkdir xmem dir\n");
                return 2;
            }
            dbg("create xmem dir %s ok\n", XMEM_FILE_DIR);
        }

        if (sys_lfs_stat(XMEM_JBGFILE_DIR, &info))
        {
            if (sys_lfs_mkdir(XMEM_JBGFILE_DIR))
            {
                err("sys_lfs_mkdir xmem dir\n");
                return 2;
            }
            dbg("create xmem dir %s ok\n", XMEM_JBGFILE_DIR);
        }
    }

    //sys_lfs_remove("/SysMem/XMEM.DAT");         //老的无效文件,删除
    //sys_lfs_remove("/SysMem/trans.dat");        //老的无效文件,删除

    //创建文件
    for(i=0; fileids[i]>0; i++)
    {
        vGetMemFile(fileids[i], szFileName);
        if(szFileName[0]==0)
            return 2;
        /*
        //先删除
        if(sys_lfs_stat(szFileName, &info)==0)
        {
            dbg("remove xmem file [%s].\n", szFileName);
            sys_lfs_remove(szFileName);
        }
        */
        //创建文件
        iRet = sys_lfs_file_open(&file, szFileName, LFS_O_CREAT | LFS_O_WRONLY);
        if (iRet != 0)
        {
            dbg("create xmem file fail:[%s] %d\n", szFileName, iRet);
            return 2;
        }else {
            dbg("create xmem file ok:[%s] %d\n", szFileName, iRet);
        }
        sys_lfs_file_close(&file);
    }
    return (0);
}

#ifdef FUDAN_CARD_DEMO
uint _uiCheckSysInfo(void)
{
    uchar szFileName[100];
    struct lfs_info info;
    lfs_file_t file;
    int i, iRet;
    int fileids[]={MEM_SYSINFO, MEM_SYSDATA, MEM_OPER, MEM_CAPUBKEY, MEM_AID, MEM_REV8583, MEM_SCR8583, MEM_QRTRANSREC, 
        MEM_SETTLEINFO, MEM_TRANSREC_0, MEM_TRANSREC_1, MEM_TRANSREC_2, MEM_TRANSREC_3, MEM_TRANSREC_4, -1};

    vGetMemFile(MEM_SYSINFO, szFileName);
    if(sys_lfs_stat(szFileName, &info)==0)
    {
        return 0;
    }

    //ucFlag==0, 以下为初始化内存文件操作

    //创建目录
    //sys_lfs_mkdir(XMEM_FILE_DIR);
    {
        struct lfs_info dirinfo;
        if (sys_lfs_stat(XMEM_FILE_DIR, &dirinfo))
        {
            if (sys_lfs_mkdir(XMEM_FILE_DIR))
            {
                err("sys_lfs_mkdir xmem dir\n");
                return 2;
            }
            dbg("create xmem dir %s ok\n", XMEM_FILE_DIR);
        }
    }

    //创建文件
    iRet = sys_lfs_file_open(&file, szFileName, LFS_O_CREAT | LFS_O_WRONLY);
    if (iRet != 0)
    {
        dbg("create xmem file fail:[%s] %d\n", szFileName, iRet);
        return 2;
    }else {
        dbg("create xmem file ok:[%s] %d\n", szFileName, iRet);
    }
    sys_lfs_file_close(&file);

    return (0);
}
#endif

// 7.1. 设置扩展内存大小
// 输入参数：ulSize : 要设置的扩展内存大小，单位为字节
// 返    回：0      : 设置成功
//           1      : 参数错误, 申请的空间必须大于等于32
//           2      : 申请失败
//           >=32   : 扩展内存不足，返回值为最大可用扩展内存大小
ulong _ulSetXMemSize(ulong ulSize)
{
    return 1;
}

// 7.2. 读扩展内存
// 输入参数：ulOffset : 扩展内存以字节为单位的偏移量
//           uiLen    ：以字节为单位的长度
// 输出参数：pBuffer  : 读取得数据
// 返    回：0        : 成功
//           1        : 失败
uint _uiXMemRead(uint uiMemId, ulong ulOffset, void *pBuffer, uint uiLen)
{
    lfs_file_t file;
    int iRet;
    uchar szFileName[100];

    vGetMemFile(uiMemId, szFileName);
    if(szFileName[0]==0)
        return 1;

    iRet = sys_lfs_file_open(&file, szFileName, LFS_O_RDONLY);
    if (iRet != 0)
    {
        dbg("_uiXMemRead, open file fail:[%s] %d\n", szFileName, iRet);
        return 1;
    }

    if(ulOffset)
    {
        iRet = sys_lfs_file_seek(&file, ulOffset, LFS_SEEK_SET);
        if (iRet != ulOffset)
        {
            dbg("_uiXMemRead sys_lfs_file_seek [%s] off:%lu ret=%d\n", szFileName, ulOffset, iRet);
            sys_lfs_file_close(&file);
            return 1;
        }
    }

    iRet = sys_lfs_file_read(&file, pBuffer, uiLen);
    sys_lfs_file_close(&file);

    if (iRet != uiLen)
    {
        dbg("_uiXMemRead [%s] off=%lu, len=%lu, read ret=%d, err.\n", szFileName, ulOffset, uiLen, iRet);
        return (1);
    }
    return (0);
}

// 7.3. 写扩展内存
// 输入参数：pBuffer  : 要写的数据
//           ulOffset : 扩展内存以字节为单位的偏移量
//           uiLen    ：以字节为单位的长度
// 返    回：0        : 成功
//           1        : 失败
uint _uiXMemWrite2(uint uiMemId, ulong ulOffset, const void *pBuffer, uint uiLen)
{
    lfs_file_t file;
    int iRet;
    uchar szFileName[100];

    vGetMemFile(uiMemId, szFileName);
    if(szFileName[0]==0)
        return 1;
    
    iRet = sys_lfs_file_open(&file, szFileName, LFS_O_WRONLY);
    if (iRet != 0)
    {
        dbg("_uiXMemWrite, open file fail:[%s] %d\n", szFileName, iRet);
        return 1;
    }
   
    //if(ulOffset)
    {
        iRet = sys_lfs_file_seek(&file, ulOffset, LFS_SEEK_SET);
        if (iRet != ulOffset)
        {
            sys_lfs_file_close(&file);
            dbg("_uiXMemWrite sys_lfs_file_seek [%s] off:%lu ret=%d\n", szFileName, ulOffset, iRet);
            return 1;
        }
    }
    
    iRet = sys_lfs_file_write(&file, pBuffer, uiLen);
    if(iRet!=uiLen)        //若写文件失败,尝试重写一遍
    {
        mdelay(50);
        sys_lfs_file_seek(&file, ulOffset, LFS_SEEK_SET);
        iRet = sys_lfs_file_write(&file, pBuffer, uiLen);
    }
    sys_lfs_file_close(&file);
    if (iRet != uiLen)
    {
        dbg("_uiXMemWrite [%s] off=%lu, len=%lu, write ret=%d, err.\n", szFileName, ulOffset, uiLen, iRet);
        return (1);
    }

    return (0);
}
uint _uiXMemWrite(uint uiMemId, ulong ulOffset, const void *pBuffer, uint uiLen)
{
    uint ret;
    ret=_uiXMemWrite2(uiMemId, ulOffset, pBuffer, uiLen);
    if(ret)
    {
        _vDelay(10);
        ret=_uiXMemWrite2(uiMemId, ulOffset, pBuffer, uiLen);
    }
    ASSERT(ret==0);
    return ret;
}

void _vClearTransFile(void)
{
    lfs_file_t file;
    int iRet;
    char szFileName[100];
    int i;
    for(i=0; i<TRANS_FILE_NUM; i++)
    {
        sprintf(szFileName, "%s/%s%d.dat", XMEM_FILE_DIR, "trans", i);        
        iRet = sys_lfs_file_open(&file, szFileName, LFS_O_WRONLY|LFS_O_TRUNC);
        if(iRet==0)
            sys_lfs_file_close(&file);
    }
    
    #ifdef APP_SDJ
    sprintf(szFileName, "%s/%s", XMEM_FILE_DIR, "qrtrans.dat");  
    iRet = sys_lfs_file_open(&file, szFileName, LFS_O_WRONLY|LFS_O_TRUNC);
    if(iRet==0)
        sys_lfs_file_close(&file);
    #endif
}

uint _uiXMemReadJbgFile(char *pszFile, uchar *psData, int size, int *piLen)
{
    lfs_file_t file;
    int iRet;
    char szFileName[100];

    *piLen=0;
    sprintf(szFileName, "%s/%s", XMEM_JBGFILE_DIR, pszFile);
    iRet = sys_lfs_file_open(&file, szFileName, LFS_O_RDONLY);
    if (iRet != 0)
    {
        dbg("_uiXMemReadJbgFile, open file fail:[%s] %d\n", szFileName, iRet);
        return 1;
    }
    iRet=sys_lfs_file_read(&file, psData, size);
    sys_lfs_file_close(&file);
    if (iRet<=0)
    {
        dbg("_uiXMemReadJbgFile [%s] ret=%d, err.\n", szFileName, iRet);
        return (1);
    }
    *piLen=iRet;

    return (0);
}

uint _uiXMemWriteJbgFile(char *pszFile, uchar *psData, int iLen)
{
    lfs_file_t file;
    int iRet;
    char szFileName[100];

#if 0
    {
        struct lfs_info info;
        if (sys_lfs_stat(XMEM_JBGFILE_DIR, &info))
        {
            if (sys_lfs_mkdir(XMEM_JBGFILE_DIR))
            {
                err("sys_lfs_mkdir xmem dir\n");
                return 2;
            }
            dbg("create xmem dir %s ok\n", XMEM_JBGFILE_DIR);
        }
    }
#endif

    sprintf(szFileName, "%s/%s", XMEM_JBGFILE_DIR, pszFile);
    iRet = sys_lfs_file_open(&file, szFileName, LFS_O_WRONLY|LFS_O_CREAT|LFS_O_TRUNC);
    if (iRet != 0)
    {
        dbg("_uiXMemWriteJbgFile, open file fail:[%s] %d\n", szFileName, iRet);
        return 1;
    }
    iRet=sys_lfs_file_write(&file, psData, iLen);
    sys_lfs_file_close(&file);
    if (iRet != iLen)
    {
        dbg("_uiXMemWriteJbgFile [%s] len:%d ret=%d, err.\n", szFileName, iLen, iRet);
        return (1);
    }

    return (0);
}

uint _uiXMemDelJbgFile(char *pszFile)
{
    char szFileName[100];

    sprintf(szFileName, "%s/%s", XMEM_JBGFILE_DIR, pszFile);
    sys_lfs_remove(szFileName);
    return 0;
}

uint _uiXMemClearJbgDir(void)
{
    sys_lfs_filesindir_remove(XMEM_JBGFILE_DIR);
    return 0;
}

// 8. 磁条卡
static int sg_iMagOpen = 0;
// 8.1. 清磁卡缓冲区
// 返    回：0 : 成功
//           1 : 失败
uint _uiMagReset(void)
{
    //msr_open();
    if (sg_iMagOpen == 0)
    {
        msr_powerup();
        mdelay(150);
        reset_adc();

        sg_iMagOpen = 1;
    }
    msrClean();
    memset(&sg_msrdata, 0, sizeof(sg_msrdata));
    
    return 0;
}

// 8.2. 检测是否有磁卡刷过
// 返    回：0 : 无卡
//           1 : 有磁卡刷过
uint _uiMagTest(void)
{
    int ret;

    if (sg_iMagOpen == 0)
        _uiMagReset();
    memset(&sg_msrdata, 0, sizeof(sg_msrdata));
#if 0  //for test
    strcpy(sg_msrdata[1].buf, "6227614850220023=19052010000051000000");
	sg_msrdata[1].len=strlen(sg_msrdata[1].buf);
    return 1;
#endif
    ret = readMSRData(&sg_msrdata);
    if (ret == 0)
        return 1;
    else
        return 0;
}

// 8.3. 读磁道信息
// 输入参数：uiTrackNo : 磁道号，1-3
// 输出参数：pszBuffer : 磁道信息
// 返    回：0 : 读取信息正确
//           1 : 没有数据
//           2 : 检测到错误
// 说    明：没有起始符、结束符、校验位，字符编码为: 0x30-0x3f
uint _uiMagGet(uint uiTrackNo, uchar *pszBuffer)
{
    char *p1, *p2;
    if (uiTrackNo < 1 || uiTrackNo > 3 || pszBuffer == NULL)
        return 2;
    if (sg_msrdata[uiTrackNo - 1].len)
    {
        p1=(char*)sg_msrdata[uiTrackNo - 1].buf;
        if(p1[0]=='%' || p1[0]==';')          //跳过起始符
            p1++;
        p2=strchr(p1, '?');
        if(p2)
        {
            memcpy(pszBuffer, p1, p2-p1);
            pszBuffer[p2-p1]=0;
        }else
            strcpy(pszBuffer, p1);
        return 0;
    }
    return 1;
}

uint _uiMagClose(void)
{
    msr_powerdown();
    sg_iMagOpen = 0;

    return 0;
}

// 9. 通讯
uint sg_uiComBuf[1];
// 9.1. 设置当前串口号
// 输入参数：ucPortNo : 新串口号, 从串口1开始
// 返    回：原先的串口号
uchar _ucAsyncSetPort(uchar ucPortNo)
{
    return 0;
}

// 9.2. 打开当前串口
// 输入参数：ulBaud   : 波特率
//           ucParity : 奇偶校验标志，COMM_EVEN、COMM_ODD、COMM_NONE
//           ucBits   : 数据位长度
//           ucStop   : 停止位长度
// 返    回：0        : 成功
//           1        : 参数错误
//           2        : 失败
uchar _ucAsyncOpen(ulong ulBaud, uchar ucParity, uchar ucBits, uchar ucStop)
{
    sg_uiComBuf[0]=0;
    return 0;
}

// 9.3. 关闭当前串口
// 返    回：0        : 成功
//           1        : 失败
uchar _ucAsyncClose(void)
{
    sg_uiComBuf[0]=0;
    return 0;
}

// 9.4. 复位当前串口，清空接收缓冲区
// 返    回：0        : 成功
//           1        : 失败
uchar _ucAsyncReset(void)
{
    while(_ucAsyncTest())
        _ucAsyncGet();
    
    return 0;
}

// 9.5. 发送一个字节
// 输入参数：ucChar : 要发送的字符
// 返    回：0      : 成功
//           1      : 失败
uchar _ucAsyncSend(uchar ucChar)
{
    return _ucAsyncSendBuf(&ucChar, 1);
}

// 9.6. 检测有没有字符收到
// 返    回：0 : 没有字符收到
//           1 : 有字符收到
uchar _ucAsyncTest(void)
{
    uchar ch;
    int ret;
    
    if(sg_uiComBuf[0])
        return 1;
    ret=serialread(UARTUSB, &ch, 1, 100);
    if(ret>0)
    {
        sg_uiComBuf[0]=0xFF00|ch;
        return 1;
    }
    return 0;
}

// 9.7. 接收一个字符，没有则一直等待
// 返    回：接收到的字符
uchar _ucAsyncGet(void)
{
    int ret;
    uchar ch;
    
    if(sg_uiComBuf[0])
    {
        ch=sg_uiComBuf[0];      //sg_uiComBuf[0]&0x00FF
        sg_uiComBuf[0]=0;
        return ch;
    }
    
    while(1)
    {
        ret=serialread(UARTUSB, &ch, 1, 10*1000);
        if(ret>0)
            return ch;
        mdelay(5);
    }
}

// 9.8.	发送一串数据
// 输入参数：pBuf  : 要发送的字符
//           uiLen : 数据长度
// 返    回：0     : 成功
//           1     : 失败
uchar _ucAsyncSendBuf(uchar *pBuf, uint uiLen)
{
    if(serialwrite(UARTUSB, pBuf, uiLen)==uiLen)
        return 0;
    else
        return 1;
}

// 9.9.	接收指定长度的一串数据
// 输入参数：pBuf      : 接收的数据
//           uiLen     : 数据长度
//           uiTimeOut : 以秒为单位的超时时间
// 返    回：0         : 成功
//           1         : 超时
uchar _ucAsyncGetBuf(uchar *pBuf, uint uiLen, uint uiTimeOut)
{
    int len, iRcvLen;
    ulong ulTimer;

    _vSetTimer(&ulTimer, uiTimeOut*100);
    iRcvLen=0;
    if(sg_uiComBuf[0])
    {
        pBuf[iRcvLen++]=sg_uiComBuf[0];
        sg_uiComBuf[0]=0;
    }
    
    while(iRcvLen<uiLen && !_uiTestTimer(ulTimer))
    {
        len = serialread(UARTUSB, pBuf+iRcvLen, uiLen-iRcvLen, uiTimeOut*1000);
        if(len>0)
            iRcvLen+=len;
        else
            mdelay(5);
    }
    
    if(iRcvLen==uiLen)
        return 0;
    else
        return 1;
}

// 10. 其它
// 10.1. 发一短声
void _vBuzzer(void)
{
    //BuzzerSetFreq(1000);
    BuzzerOn(50);
}

// 10.2. 取一随机数
// 返    回：0-255的随机数
uchar _ucGetRand(void)
{
    return ((uchar)rand());
}

// 输入参数：uiMode   : ENCRYPT -> 加密，DECRYPT -> 解密
//                      TRI_ENCRYPT -> 3Des加密，TRI_DECRYPT -> 3Des解密
//           psSource : 源
//           psKey    : 密钥
// 输出参数：psResult : 目的
void _vDes(uint uiMode, uchar *psSource, uchar *psKey, uchar *psResult)
{
#if 0    
    mh_tdes_key_def mh_key3;
    mh_des_iv_def mh_iv;
    mh_rng_callback f_rng = mh_rand_p;
    int ret=-1;

    memset(mh_iv, 0, sizeof(mh_iv));

    switch (uiMode)
    {
    case ENCRYPT:
        ret = mh_des_enc(ECB, psResult, 8, psSource, 8, psKey, NULL, f_rng, NULL);
        break;
    case DECRYPT:
        ret = mh_des_dec(ECB, psResult, 8, psSource, 8, psKey, NULL, f_rng, NULL);
        break;
    case TRI_ENCRYPT:  //两倍长密钥
    case TRI3_ENCRYPT: //三倍长密钥
        memcpy(mh_key3.k1, psKey, 8);
        memcpy(mh_key3.k2, psKey + 8, 8);
        if (uiMode == TRI_ENCRYPT)
            memcpy(mh_key3.k3, psKey, 8);
        else
            memcpy(mh_key3.k3, psKey + 16, 8);
        ret = mh_tdes_enc(ECB, psResult, 8, psSource, 8, &mh_key3, mh_iv, f_rng, NULL);
        break;
    case TRI_DECRYPT:
    case TRI3_DECRYPT:
        memcpy(mh_key3.k1, psKey, 8);
        memcpy(mh_key3.k2, psKey + 8, 8);
        if (uiMode == TRI_DECRYPT)
            memcpy(mh_key3.k3, psKey, 8);
        else
            memcpy(mh_key3.k3, psKey + 16, 8);
        ret = mh_tdes_dec(ECB, psResult, 8, psSource, 8, &mh_key3, mh_iv, f_rng, NULL);
        break;
    default:
        break;
    }
    if (ret != MH_RET_DES_SUCCESS) {
		err("mh_des err, return %d, %04X\n ", ret, ret);
    }
    ASSERT(ret==MH_RET_DES_SUCCESS);
#else
    mbedtls_des_context ctx_des;
    mbedtls_des3_context ctx_des3;
    switch (uiMode)
    {
    case ENCRYPT:
        mbedtls_des_init( &ctx_des );
        mbedtls_des_setkey_enc(&ctx_des, psKey);
        mbedtls_des_crypt_ecb( &ctx_des, psSource, psResult );
        mbedtls_des_free( &ctx_des );
        break;
    case DECRYPT:
        mbedtls_des_init( &ctx_des );
        mbedtls_des_setkey_dec(&ctx_des, psKey);
        mbedtls_des_crypt_ecb( &ctx_des, psSource, psResult );
        mbedtls_des_free( &ctx_des );
        break;
    case TRI_ENCRYPT:  //两倍长密钥
        mbedtls_des3_init( &ctx_des3 );
        mbedtls_des3_set2key_enc(&ctx_des3, psKey);
        mbedtls_des3_crypt_ecb( &ctx_des3, psSource, psResult );
        mbedtls_des3_free( &ctx_des3 );
        break;
    case TRI_DECRYPT:
        mbedtls_des3_init( &ctx_des3 );
        mbedtls_des3_set2key_dec(&ctx_des3, psKey);
        mbedtls_des3_crypt_ecb( &ctx_des3, psSource, psResult );
        mbedtls_des3_free( &ctx_des3 );
        break;
    case TRI3_ENCRYPT: //三倍长密钥, 
        mbedtls_des3_init( &ctx_des3 );
        mbedtls_des3_set3key_enc(&ctx_des3, psKey);
        mbedtls_des3_crypt_ecb( &ctx_des3, psSource, psResult );
        mbedtls_des3_free( &ctx_des3 );
        break;
    case TRI3_DECRYPT:
        mbedtls_des3_init( &ctx_des3 );
        mbedtls_des3_set3key_dec(&ctx_des3, psKey);
        mbedtls_des3_crypt_ecb( &ctx_des3, psSource, psResult );
        mbedtls_des3_free( &ctx_des3 );
        break;
    default:
        ASSERT(0);
        break;
    }
#endif
    return;
}

//加解密8倍数长数据
void _vDesPlus(uint uiMode, uchar *psSource, int iSourceLen, uchar *psKey, uchar *psResult)
{
    int i;
    mbedtls_des_context ctx_des;
    mbedtls_des3_context ctx_des3;

    if(iSourceLen<=0)
        return;

    switch (uiMode)
    {
    case ENCRYPT:
        mbedtls_des_init( &ctx_des );
        mbedtls_des_setkey_enc(&ctx_des, psKey);
        break;
    case DECRYPT:
        mbedtls_des_init( &ctx_des );
        mbedtls_des_setkey_dec(&ctx_des, psKey);
        break;
    case TRI_ENCRYPT:  //两倍长密钥
        mbedtls_des3_init( &ctx_des3 );
        mbedtls_des3_set2key_enc(&ctx_des3, psKey);
        break;
    case TRI_DECRYPT:
        mbedtls_des3_init( &ctx_des3 );
        mbedtls_des3_set2key_dec(&ctx_des3, psKey);
        break;
    case TRI3_ENCRYPT: //三倍长密钥
        mbedtls_des3_init( &ctx_des3 );
        mbedtls_des3_set3key_enc(&ctx_des3, psKey);
        break;
    case TRI3_DECRYPT:
        mbedtls_des3_init( &ctx_des3 );
        mbedtls_des3_set3key_dec(&ctx_des3, psKey);
        break;
    default:
        ASSERT(0);
        break;
    }

    for(i=0; i<iSourceLen; i+=8)
    {
        if (uiMode==ENCRYPT ||  uiMode==DECRYPT)
            mbedtls_des_crypt_ecb( &ctx_des, psSource+i, psResult+i );
        else
            mbedtls_des3_crypt_ecb( &ctx_des3, psSource+i, psResult+i );
    }
    if (uiMode==ENCRYPT ||  uiMode==DECRYPT)
        mbedtls_des_free( &ctx_des );
    else
        mbedtls_des3_free( &ctx_des3 );
    return;
}

int _iSm4(uint uiMode, uchar *psSource, uchar *psKey, uchar *psResult)
{
    uint ret;
    mh_rng_callback f_rng = mh_rand_p;
    
    if ((uiMode!=ENCRYPT && uiMode!=DECRYPT) || psSource==NULL || psKey==NULL || psResult==NULL)
    {
        ASSERT(0);
        return -1;
    }
    
    if(uiMode==ENCRYPT)
    {
        ret=mh_sm4_enc(ECB, 
					 psResult, 16, 
					 psSource, 16, 
					 psKey, NULL, 
                     f_rng, NULL);
    }else{
        ret=mh_sm4_dec(ECB, 
					 psResult, 16, 
					 psSource, 16, 
					 psKey, NULL, 
                     f_rng, NULL);
    }
    if(ret!=MH_RET_SM4_SUCCESS)
    {
        ASSERT(ret==MH_RET_SM4_SUCCESS);
        return -2;
    }else
        return 0;
}

// 10.4 读取终端序列号
// 输出参数 ：pszSerialNo : 终端序列号
// 返    回 : 0 : OK
//            1 : ERROR
uint _uiGetSerialNo(uchar *pszSerialNo)
{
    extern int SecReadTerminalSN(uchar *terminalSN);
    int ret;
#ifndef NO_SEC_FORDEMO 
    pszSerialNo[0] = 0;
    ret = SecReadTerminalSN(pszSerialNo);
    
    if( ret==0 && ((pszSerialNo[0]>='0' && pszSerialNo[0]<='9') || (pszSerialNo[0]>='a' && pszSerialNo[0]<='z')
        || (pszSerialNo[0]>='A' && pszSerialNo[0]<='Z') ) )
    {
            return 0;
    }else
    {
        pszSerialNo[0]=0;
#ifdef JTAG_DEBUG
#if PROJECT_CY20EA==1     
        {
        //strcpy((char *)pszSerialNo, "00007302120001000253");
        strcpy((char *)pszSerialNo, "00007202CT0100000002");
        return 0;
        }
#endif
#endif        
        return 1;
    }
#else
#ifdef APP_LKL
    //strcpy((char *)pszSerialNo, "00007202CT0100000001");      //for test
    #ifdef JTAG_DEBUG
    strcpy((char *)pszSerialNo, "00007202CT0100000002");      //for test
    #else
    strcpy((char *)pszSerialNo, "00007202CT0100000003");      //for test
    #endif
#else
    //strcpy((char *)pszSerialNo, "00007302120001000235");
    //strcpy((char *)pszSerialNo, "00007302120001000240");
    //strcpy((char *)pszSerialNo, "00007302120001000302");
    //strcpy((char *)pszSerialNo, "00007302120001000253");
    strcpy((char *)pszSerialNo, "00007202CT0100000004");
#endif
    return 0;
#endif
}

char* _pszGetCLIVer(char *pszVer)
{
    if(pszVer)
        strcpy(pszVer, CLI_VER);
    return CLI_VER;
}

char* _pszGetAppVer(char *pszVer)
{
#if 0    
#ifdef APP_LKL  
    extern uchar ucGetLKLPosType(void);
    
    strcpy(sg_szAPPVer, APP_VER);
    //押金版版本号末尾为B
    if(ucGetLKLPosType())
        sg_szAPPVer[strlen(sg_szAPPVer)-1]='B';
    if(pszVer)
        strcpy(pszVer, sg_szAPPVer);
    return sg_szAPPVer;   
#else
    if(pszVer)
        strcpy(pszVer, APP_VER);
    return APP_VER;    
#endif    
#else
    char *p0, *p1;
    
    if(sg_szAppVer[0]==0)
    {
        p0=strchr(COMPLETEVER, '[');
        if(p0)
        {
            p0++;
            p1=strchr(p0, '&');
            if(p1 && p1-p0<sizeof(sg_szAppVer)-1)
            {
                memcpy(sg_szAppVer, p0, p1-p0);
                sg_szAppVer[p1-p0]=0;
            }
        }
    }
    #ifdef APP_LKL
    if(ucGetLKLPosType())
        sg_szAPPVer[strlen(sg_szAPPVer)-1]='B';
    #endif
    if(pszVer)
        strcpy(pszVer, sg_szAppVer);
    return sg_szAppVer;
#endif
}

char* _pszGetFirmwareVer(char *pszVer)
{
#if 0    
    if(pszVer)
        strcpy(pszVer, APP_VER);
    return APP_VER;
#endif
    return _pszGetAppVer(pszVer);
}


void vGetPbocHwVerAndFwVer(char *pszHWVer, char *pszFWVer)
{
    char szHWVer[15], szFWVer[15];
    
#if PROJECT_CY20  == 1
    //CY20P
    strcpy(szHWVer, "V1.0.0");
    strcpy(szFWVer, "V1.0.0");  
#endif
    
#ifdef PROJECT_CY21
    //CY20E
    strcpy(szHWVer, "V2.0.0");
    strcpy(szFWVer, "V1.0.0");  
#endif    

#ifdef PROJECT_CY20P_LITE
    //CY20P Lite
    strcpy(szHWVer, "P4G V1.1.0");
    strcpy(szFWVer, "V1.1");  
#endif
    
#ifdef PROJECT_CY20E_LITE
    //CY20E Lite
    strcpy(szHWVer, "4G V2.1.0");
    strcpy(szFWVer, "V2.1");    
#endif
    if(pszHWVer)
        strcpy(pszHWVer, szHWVer);
    if(pszFWVer)
        strcpy(pszFWVer, szFWVer);
    
    return;
}

char* _pszGetParamVer(char *pszVer)
{
    if(pszVer)
        strcpy(pszVer, "V0000000000");
    return "V0000000000";
}

char* _pszGetVendor(char *pszVendor)
{
    if(pszVendor)
        strcpy(pszVendor, "JD");
    return "JD";
}

char* _pszGetModelName(char *pszModel)
{
#if PROJECT_CY20==1 
     if(pszModel)
        strcpy(pszModel, "CY20P");
    return "CY20P";
#endif

#if defined(PROJECT_CY20P_LITE)
if(pszModel)
        strcpy(pszModel, "CY20PL");
    return "CY20PL";
#endif

#if defined(PROJECT_CY20E_LITE)
if(pszModel)
        strcpy(pszModel, "CY20EL");
    return "CY20EL";
#endif

#if defined(PROJECT_CY21)
     if(pszModel)
        strcpy(pszModel, "CY21");
    return "CY21";
#endif
}

char* _pszGetInternalVer(char *pszInternalVer)
{
#if 0    
    if(pszInternalVer)
        strcpy(pszInternalVer, INTERNALVER);
    return INTERNALVER;
#else
    char *p0, *p1;
    
    if(sg_szInternalVer[0]==0)
    {
        p0=strchr(COMPLETEVER, '&');
        if(p0)
        {
            p0++;
            p1=strchr(p0, ']');
            if(p1 && p1-p0<sizeof(sg_szInternalVer)-1)
            {
                memcpy(sg_szInternalVer, p0, p1-p0);
                sg_szInternalVer[p1-p0]=0;
            }
        }
    }
    if(pszInternalVer)
        strcpy(pszInternalVer, sg_szInternalVer);
    return sg_szInternalVer;
#endif    
}

//1:电压低不允许交易 2:电压低不允许打印 3:电压低不允许更新 9:电压正常
uint _uiCheckBattery(void)
{
    //int val=0;
    //if(battery_charger_getstate() == BATTERY_IS_CHARGING)
    //    val=70;
    if(battery_get_voltage()<BATTERY_1_MIN)
        return 1;
    //if(battery_get_voltage()<BATTERY_1_MIN)
    //    return 2;
    //if(battery_get_voltage()<BATTERY_2_MIN)
    //    return 3;
    return 9;
}

int mGetBattery(void *p)
{
    char buf[30];
    
    _vFlushKey();
    _vCls();
    if(battery_charger_getstate() == BATTERY_IS_CHARGING)
    {
        _vDisp(2, "充电中...");
    }
    sprintf(buf, "getBatteryVol:%d", getBatteryVol());
    _vDisp(3, buf);
    sprintf(buf, "battery_get_voltage:%d", battery_get_voltage());
    _vDisp(4, buf);
    _uiGetKey();
    return 0;
}

#define ENABLE_QRCODE_PREVIEW

#ifdef ENABLE_QRCODE_PREVIEW
//static unsigned short *p_qrcodeImage = NULL;//[240*320];// = NULL;
extern const char* GetImageBuffAddr(void);
#endif

#ifndef USE_1903S
static Boolean lcdmode;
#endif

#ifdef ST7789
extern unsigned short * getLcdBufferPoint(void); 
#endif

#define GRAYTORGB16(t)  (((t >> 3) << 11)|((t >> 2) << 5)|((t>>3)))

int sg_iQrPreViewFlag=1;
void vSetQrPreView(int flag)
{
    sg_iQrPreViewFlag=flag;
}
int iGetQrPreView(void)
{
    return sg_iQrPreViewFlag;
}

void vSetQrPreViewParam(int flag, short x, short y)
{
    sg_iQrPreViewFlag=flag;
    
#if defined(USE_COLOR_RGB565) && defined(USE_1903S)
    #ifndef  LCD_GC9106
    if(y<=35)
        _vDisp(1, (uchar*)"");
    
    LcdPreviewQrdecodeParas(x, y, DISPLAY_WIDTH-x*2, DISPLAY_HEIGHT-y);
    #endif
#endif
}

#ifndef SUPPORT_8W_CAMERA
//开始扫码
uint _uiQrStart(void)
{
#ifndef NO_QRTRANS    
    int ret;
    
#ifndef USE_1903S
    lcdmode=(Boolean)TRUE;
#endif
    
    ret = qrcodeOpen();
    if(ret){
        qrcodeClose();
        return ret;
    }
#ifndef USE_1903S
    lcdmode = lcdgetfullflush();
    if(lcdmode == FALSE)
    {
        lcdsetfullflush((Boolean)TRUE);

        LcdStatusCLear();
        lcdflushstatusbar();
    }
#endif

#if defined(USE_COLOR_RGB565) && !defined(USE_1903S)
/*    
    p_qrcodeImage = (unsigned short *)malloc(320*240*2);
    if(p_qrcodeImage == NULL)
    {
        qrcodeClose();
        err("malloc qrcode preview buffer \n");
        return 1;
    }
*/
    p_qrcodeImage =  (unsigned short*)getLcdBufferPoint() + (DISPLAY_WIDTH * STATUSBAR_H);    
#endif

    QRLedOn();
    memset(&DecodeCfg, 0, sizeof(DecodeCfg));
    //配置解码信息
    /**
     * 解码默认配置
     *
     * cfg->cfgCODE128 = (1u | (0u << DE_MIN_LEN) | (0xffu << DE_MAX_LEN));
     * cfg->cfgCODE39 = (1u | (0u << DE_MIN_LEN) | (0xffu << DE_MAX_LEN));
     * cfg->cfgCODE93 = (1u | (0u << DE_MIN_LEN) | (0xffu << DE_MAX_LEN));
     * cfg->cfgEAN13 = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgUPC_A = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgEAN8 = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgUPC_E0 = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgUPC_E1 = (1u | (1u << DE_EMIT_CHECK));
     * cfg->cfgISBN13 = 0u;
     * cfg->cfgInterleaved2of5 = (0u | (6u << DE_MIN_LEN) | (0xffu << DE_MAX_LEN));
     * 
     * cfg->cfgQRCode = 1u | 1u << (DEQR_MISSINGCORNER) | (1u << DEQR_CURVE);
     * cfg->cfgPDF417 = 0u;
     * cfg->cfgDataMatrix = 0u;
     * 
     * cfg->cfgGlobal = cfg->cfgGlobal = (1u << DEBAR_PRECISION);
     * 
     */
    
    DecodeConfigInit(&DecodeCfg);

#if defined(LCD_GC9106)
    //使能采图
    if(GetGrayImageSize() == IMAGE640X480)
    {
        DecodeDcmiStart();
    }
    else
    {
        //8w qrcode we use the double decode
        if(GetDecodeBuffFlag() == FALSE)
        {
            DecodeDcmiStart();
        }
    }
    sg_ulDecodeStartTk=get_tick();
#else
    DecodeDcmiStart();
#endif
    
    dbg("start get QrCode...\n");
#endif    
    return 0;
}



//循环调用,判断返回值
//0-无扫码或扫码失败 >0:扫码成功,返回长度
//扫码成功后,若要继续扫码需要end后重新start开始
uint _uiGetQrCode(uchar *pszQrCode, int size)
{
#ifndef NO_QRTRANS    
    //定义保存解码结果的数组
    int32_t resnum;    //返回解码结果数量，为0，表示解码失败，为-1表示解码数组偏小
#ifdef USE_COLOR_RGB565    
    uint8_t result[2100] = {0};
#else
    uint8_t result[512] = {0};
#endif
    //DecodeConfigTypeDef DecodeCfg = {0};  //设置解那种码
    DecodeResultTypeDef res = {.result = result, .maxn = sizeof(result)}; //初始化译码结构体

    dbg("check DecodeDcmiFinish...\n");
    if(DecodeDcmiFinish()==0)
    {
#if defined(LCD_GC9106)
        dbg("check time...\n");
        // max 1000ms
        if(get_tick()-sg_ulDecodeStartTk > 200 && GetGrayImageSize() == IMAGE240X320)
        {
            /*
            if(reset++ > 5)
            {
                dbg("reset too much, EXIT");
                return -41;
            }
            */
            dbg("qrcodeReset...\n");
            qrcodeReset();
            
            //it's import for reset.
            mdelay(50);
            //goto retry;
            
            DecodeConfigInit(&DecodeCfg);
            //8w qrcode we use the double decode
            if(GetDecodeBuffFlag() == FALSE)
            {
                dbg("GetDecodeBuffFlag->DecodeDcmiStart...\n");
                DecodeDcmiStart();
            }
            sg_ulDecodeStartTk=get_tick();
        }
#endif        
        return 0;
    }
    
    dbg("DecodeDcmiFinish().\n");
#if defined(LCD_GC9106)
    //采集完第一张图后解码前使能采集下一个buff图
    if(GetGrayImageSize() == IMAGE240X320)
    {
        dbg("Finish->DecodeDcmiStart...\n");
        DecodeDcmiStart();
    }
#endif
    
    if(sg_iQrPreViewFlag)
    {
        const uint8_t *pImage = NULL;
        pImage = (const uint8_t *)GetImageBuffAddr();
        if (pImage)
        {
        #if defined(USE_COLOR_RGB565) && defined(USE_1903S)
            dbg("LcdPreviewQrdecode...\n");
            LcdPreviewQrdecode((uchar*)pImage);
        #else
            uint8_t pointY = 0;
			int i, j;
            #ifdef USE_COLOR_RGB565
            for (i = 0; i < 240-STATUSBAR_H; i++)
            {
                for (j = 0; j < 320; j++)
                {
                    pointY = *(pImage + (i * 2) * 640 + (j * 2));
                    p_qrcodeImage[i*320 + j] = GRAYTORGB16(pointY);
                }
            }
            #else
            for (i = STATUSBAR_H; i < DISPLAY_HEIGHT; i++)
            {
                for (j = 0; j < DISPLAY_WIDTH; j++)
                {
                    //pointY = *(pImage + ((i + 70) * 2) * (320*2) + (j * 2) + 175);
                    //p_qrcodeImage[i*DISPLAY_WIDTH + j] = GRAYTORGB16(pointY);
                    //if(pointY > 100)
                    pointY = *(pImage + ((i + 60) * 2) * (320*2) + (j * 2) + 300);
                    if(pointY > 64)
                    {
                        Display_PSet(j,i,0);
                    }
                    else
                    {
                        Display_PSet(j,i,1);
                    }
                }
            }
            mdelay(10);
            #endif
            #endif
            //HW_DrawImage(0, 20, 320 - 1,240 - 1,(UG_COLOR*)((uint8_t*)p_qrcodeImage + (320*20*2)));
            lcdflush();
        }
    }   
    
    resnum = DecodeStart(&DecodeCfg, &res);
    if(resnum<=0)
    {   /*
        if(resnum == DecodeResOverflowError)
        {
            printf("解码结果溢出\n");
        }
        else if(resnum == DecodeImageNoDoneError)
        {
            printf("图片未采集完成\n");
        }
        else if(resnum == DecodeLibNoInitError)
        {
            printf("库未初始化\n");
        }
        */
        // 0 解码失败
        // DecodeResOverflowError 代表解码结果超过给定的buffer范围，或者给定解码buff小于256byte
        // DecodeImageNoDoneError 采图未完成
        // DecodeLibNoInitError 未初始化或者初始化失败

        dbg("decode fail:%d, retry...\n", resnum);
        //重新采图
        DecodeDcmiStart();
        return 0;
    }
    
    //解码成功将两个buff状态置为空闲，防止重复解码         
    CleanDecodeBuffFlag();
    //QRLedOff();
    beepone(200);

    //result[resnum]=0;
    if(resnum>=size)
        resnum=size-1;
    memcpy(pszQrCode, result, resnum);  //有可能为二进制数据
    pszQrCode[resnum]=0;

    dbg("Get QrCode ok:[%s]\n", pszQrCode);
    return resnum;
#else
    return 0;
#endif
}

//void vWriteBurnLog(char *pszLog);
//终止扫码检测释放资源或扫码成功后释放资源
void _vQrEnd(void)
{
#ifndef NO_QRTRANS    
#if 0//def ENABLE_QRCODE_PREVIEW
    if(p_qrcodeImage)
    {
        free(p_qrcodeImage);
        p_qrcodeImage = NULL;
    }
#endif    
#ifndef USE_1903S
    if(lcdmode == FALSE)
    {
        lcdsetfullflush((Boolean)FALSE);
    }
#endif   
    //vWriteBurnLog("QRLedOff......");
    QRLedOff();   
    //vWriteBurnLog("qrcodeClose......");
    qrcodeClose();
    //vWriteBurnLog("qrcodeClose end...");
    _vCls();
    lcdflush();
#endif
    return;
}
#else
//开始扫码
uint _uiQrStart(void)
{
#ifndef NO_QRTRANS    
    int ret;
    
#ifndef USE_1903S
    lcdmode=(Boolean)TRUE;      //初始值
#endif    
    
    ret = qrcodeOpen();
    if(ret){
        qrcodeClose();
    }else
        QRLedOn();
    
#ifndef USE_1903S
    lcdmode = lcdgetfullflush();
    if(lcdmode == FALSE)
    {
        lcdsetfullflush((Boolean)TRUE);

        LcdStatusCLear();
        lcdflushstatusbar();
    }
#endif    
    
    return ret;
#else
    return 1;
#endif    
}

//循环调用,判断返回值
//回调函数fWaitQrCallback   关预览时有效,该函数返回值小于0时停止扫码并退出
//返回值  >0:扫码成功,返回长度 -1:cacel -2:timeout
//扫码成功后,若要继续扫码需要end后重新start开始
int _iGetQrCodeWithTime(uchar *pszQrCode, int size, int bEscFlag, int iTimeoutMs, int (*fWaitQrCallback)())
{
#ifndef NO_QRTRANS

    int32_t resnum;    //返回解码结果字节数
    int first,iPreView;
	
#ifdef USE_COLOR_RGB565    
    uint8_t result[2100] = {0};
#else
    uint8_t result[512] = {0};
#endif
    DecodeConfigTypeDef DecodeCfg = {0};  //设置解那种码
    DecodeResultTypeDef res = {.result = result, .maxn = sizeof(result)}; //初始化译码结构体
    
    ulong tk, tkTimeout;
    int reset=0;
    uint8_t *pImage = NULL;
    int ret;
    
#ifndef USE_1903S
    lcdmode = lcdgetfullflush();
    if(lcdmode == FALSE)
    {
        lcdsetfullflush((Boolean)TRUE);

        LcdStatusCLear();
        lcdflushstatusbar();
    }
    
#if defined(USE_COLOR_RGB565)
    p_qrcodeImage =  (unsigned short*)getLcdBufferPoint() + (DISPLAY_WIDTH * STATUSBAR_H);    
#endif
#endif

    tkTimeout=get_tick()+iTimeoutMs;

    first=1;
    iPreView=iGetQrPreView();		
QRCODE_RETRY:    
    DecodeConfigInit(&DecodeCfg);   
    while(get_tick()<tkTimeout)
    {
        if(bEscFlag && _uiKeyPressed() && _uiGetKey()==_KEY_ESC)
        {
            return -1;
        }

	if(!iPreView)
	{
		vDispWaitQrImg(first);
		first=0;
	}
			
        if(GetGrayImageSize() == IMAGE640X480 || GetDecodeBuffFlag() == FALSE)
        {
            DecodeDcmiStart();
        }
        
        tk=get_tick();
        while(!DecodeDcmiFinish())
        {
            #ifdef SUPPORT_8W_CAMERA
            // max 1000ms
            if(get_tick()-tk > 200 && GetGrayImageSize() == IMAGE240X320)
            {
                if(reset++ > 5)
                {
                    dbg("reset too much, EXIT");
                    return -41;
                }

                qrcodeReset();
                dbg("qrcodeReset %d",reset);
                
                mdelay(50);
                
                goto QRCODE_RETRY;
            }
            #endif
        }

        //采集完第一张图后解码前使能采集下一个buff图
        if(GetGrayImageSize() == IMAGE240X320)
        {
            DecodeDcmiStart();
        }
        reset = 0;
        
        if(sg_iQrPreViewFlag)
        {
            pImage = (uint8_t *)GetImageBuffAddr();
            if (pImage)
            {
            #if (defined(USE_COLOR_RGB565) && defined(USE_1903S)) || defined(SUPPORT_8W_CAMERA)
                LcdPreviewQrdecode((uchar*)pImage);
            #else
                uint8_t pointY = 0;
                int i, j;
                #ifdef USE_COLOR_RGB565
                for (i = 0; i < 240-STATUSBAR_H; i++)
                {
                    for (j = 0; j < 320; j++)
                    {
                        pointY = *(pImage + (i * 2) * 640 + (j * 2));
                        p_qrcodeImage[i*320 + j] = GRAYTORGB16(pointY);
                    }
                }
                #else
                for (i = STATUSBAR_H; i < DISPLAY_HEIGHT; i++)
                {
                    for (j = 0; j < DISPLAY_WIDTH; j++)
                    {
                        //pointY = *(pImage + ((i + 70) * 2) * (320*2) + (j * 2) + 175);
                        //p_qrcodeImage[i*DISPLAY_WIDTH + j] = GRAYTORGB16(pointY);
                        //if(pointY > 100)
                        pointY = *(pImage + ((i + 60) * 2) * (320*2) + (j * 2) + 300);
                        if(pointY > 64)
                        {
                            Display_PSet(j,i,0);
                        }
                        else
                        {
                            Display_PSet(j,i,1);
                        }
                    }
                }
                mdelay(10);
                #endif
                #endif
                
                lcdflush();
            }
        }else if(fWaitQrCallback)
        {
            if(fWaitQrCallback()<0)
            {
                return ret;
            }
        }
        
        resnum = DecodeStart(&DecodeCfg, &res);
        if(resnum<=0)
        {   /*
            if(resnum == DecodeResOverflowError)
            {
                printf("解码结果溢出\n");
            }
            else if(resnum == DecodeImageNoDoneError)
            {
                printf("图片未采集完成\n");
            }
            else if(resnum == DecodeLibNoInitError)
            {
                printf("库未初始化\n");
            }
            */
            // 0 解码失败
            // DecodeResOverflowError 代表解码结果超过给定的buffer范围，或者给定解码buff小于256byte
            // DecodeImageNoDoneError 采图未完成
            // DecodeLibNoInitError 未初始化或者初始化失败

            dbg("decode fail:%d, retry...\n", resnum);
            
            continue;
        }

        //解码成功将两个buff状态置为空闲，防止重复解码         
        CleanDecodeBuffFlag();
        //QRLedOff();
        beepone(200);

        //result[resnum]=0;
        if(resnum>=size)
            resnum=size-1;
        memcpy(pszQrCode, result, resnum);  //有可能为二进制数据
        pszQrCode[resnum]=0;

        dbg("Get QrCode ok:[%s]\n", pszQrCode);
        return resnum;
    }

    return -2;
#else
    return 0;
#endif
}

//void vWriteBurnLog(char *pszLog);
//终止扫码检测释放资源或扫码成功后释放资源
void _vQrEnd(void)
{
#ifndef NO_QRTRANS    
   
#ifndef USE_1903S
    if(lcdmode == FALSE)
    {
        lcdsetfullflush((Boolean)FALSE);
    }
#endif   

    //vWriteBurnLog("QRLedOff......");
    QRLedOff();   
    //vWriteBurnLog("qrcodeClose......");
    qrcodeClose();
    //vWriteBurnLog("qrcodeClose end...");
    _vCls();
    lcdflush();
#endif
    return;
}
#endif

/*
void vTestQr(void)
{
    int ret;
    uchar szQrCode[500];
    
    _vCls();
    _vDisp(2, "start qrtest?");
    if(_uiGetKey()!=_KEY_ENTER)
        return;
    
    
    while(1)
    {
        _vFlushKey();
        ret=_uiQrStart();
        if(ret)
        {
            dbg("qrstart err:%d\n", ret);
            //_vQrEnd();
            return;
        }
        while(1)
        {
            if(_uiKeyPressed() && _uiGetKey()==_KEY_ENTER)
            {
                _vQrEnd();
                return;
            }
            
            ret=_uiGetQrCode(szQrCode, sizeof(szQrCode));
            if(ret<=0)
            {
                _vDelay(10);
                continue;
            }
            _vQrEnd();
            break;
        }
        if(ret>sizeof(szQrCode))
        {
            dbg("qrcode too long.\n");
            break;
        }
        
        if(szQrCode[0]<0x20 || szQrCode[0]>0x7E || szQrCode[1]<0x20 || szQrCode[1]>0x7E)
            dbgHex("QRCODE hex", szQrCode, ret);
        else
            dbg("qrcode(%d):[%s]\n", ret, szQrCode);
        _vDisp(3, "扫码成功,继续？");
        if(_uiGetKey()!=_KEY_ENTER)
        {
            break;  
        }
        _vDisp(3, "");
    }
    
    dbg("testQr End\n");
    return;
}
*/

#define MAINBOARDSN_FILENAME     "mainboardsn"
#define MAINBOARDSN_LEN     15
int readMainBoardSN(char *sn)
{
    lfs_file_t lfs_file;
    char buf[MAINBOARDSN_LEN];
    
    sn[0]=0;
    if (sys_lfs_file_open(&lfs_file, MAINBOARDSN_FILENAME, LFS_O_RDONLY))
    {
        return -1;
    }

    if(sys_lfs_file_read(&lfs_file, buf, MAINBOARDSN_LEN) != MAINBOARDSN_LEN)
    {
        sys_lfs_file_close(&lfs_file);
        return -2;
    }
    sys_lfs_file_close(&lfs_file);
    
    memcpy(sn,buf,MAINBOARDSN_LEN);
    sn[MAINBOARDSN_LEN]=0;
    
    return 0;
}

#define FULLTESTPASS_FILENAME   "fulltestpass"
/**
 * @fn          int checkIfFullTestPass(void)
 * @brief       check if full test is passed.
 * 
 * @param[in]   void  
 * @return      0 : NONE PASS
 *              1 : PASS
 */
int checkIfFullTestPass(void)
{
    struct lfs_info info;

    //check if files exist
    if (sys_lfs_stat(FULLTESTPASS_FILENAME, &info))
    {
        return 0;
    }
    return 1;
}

#define BURNINTEST_RESULT_FILENAME          "BurnInTestResult"
static int readResultFromFlash(const char *filename, void *result, uint16_t resultsize)
{
    int ret = 0;
    struct lfs_info info;
    lfs_file_t lfs_file;

    //1.check if files exist
    ret = sys_lfs_stat(filename, &info);
    if (ret)
    {
        return 1;
    }

    //2.read back
    ret = sys_lfs_file_open(&lfs_file, filename, LFS_O_RDONLY);
    if (ret)
    {
        err("sys_lfs_file_open,code=%d\n", ret);
        return 1;
    }

    do
    {
        ret = sys_lfs_file_read(&lfs_file, result, resultsize);
        if (ret != resultsize)
        {
            err("sys_lfs_file_read,code=%d\n", ret);
            ret = -2;
            break;
        }
        else
        {
            ret = 0;
        }
    }
    while (0);

    if (sys_lfs_file_close(&lfs_file))
    {
        err("sys_lfs_file_close\n");
    }
    return ret;
}

void readBurnInTestResult(uint32_t *curmins, uint32_t *ttmins)
{
    unsigned char buf[8];
    int ret = readResultFromFlash(BURNINTEST_RESULT_FILENAME, (void *)buf, sizeof(uint32_t)*2);
    if(ret){
        *curmins = *ttmins = 0;
    }else{
        memcpy((uint8_t *)curmins,buf,sizeof(uint32_t));
        memcpy((uint8_t *)ttmins,buf + sizeof(uint32_t),sizeof(uint32_t));
    }
    return;
}

//返回POS总空间、已用空间和空闲可用空间,单位k
uint _uiGetPosSpace(ulong *pulTotal, ulong *pulUsed, ulong *pulFree)
{
    lfs_block_state_t state;
    int ret;
        
    ret = sys_lfs_block_state(&state);
    if(ret)
        return ret;
    if(pulTotal)
        *pulTotal = (ulong)((long long)state.block_count*state.block_size/1024);
    if(pulUsed)
        *pulUsed = (ulong)((long long)state.block_use*state.block_size/1024);
    if(pulFree)
        *pulFree = (ulong)((long long)state.block_free*state.block_size/1024);
    return 0;
}

uint _uiGetSpace(int line)
{
    lfs_block_state_t state;
    char buf[50];
        
    sys_lfs_block_state(&state);
    
    sprintf(buf,"总计空间: %dK",state.block_count * state.block_size/1024 );
    _vDisp(line++, (uchar*)buf);
    
    sprintf(buf,"使用空间: %dK",state.block_use * state.block_size/1024);
    _vDisp(line++, (uchar*)buf);
    
    sprintf(buf,"未使用空间: %dK",state.block_free * state.block_size/1024);
    _vDisp(line++, (uchar*)buf);
    
    return 0;
}


//格式(各项依次串接,英文分号分隔): 1或0(整机完成标志);整数(煲机完成分钟数);整机SN;主板SN;ICCID;软件版本号(末尾加上“_SDJ”)
//例: 1;6789;00007302120001000253;123456789012345;12345678901234567890;V2020101601_SDJ

int iTestFile(void)
{
    uchar szFileName[100];
    struct lfs_info info;
    char szBuf[100];
    int i;
    int fileids[]={MEM_SYSINFO, MEM_SYSDATA, MEM_OPER, MEM_CAPUBKEY, MEM_AID, MEM_REV8583, MEM_SCR8583, MEM_QRTRANSREC, 
        MEM_SETTLEINFO, MEM_TRANSREC_0, MEM_TRANSREC_1, MEM_TRANSREC_2, MEM_TRANSREC_3, MEM_TRANSREC_4, -1};

    _vCls();
    for(i=0; fileids[i]>0; i++)
    {
        vGetMemFile(fileids[i], szFileName);
        if(sys_lfs_stat(szFileName, &info))
        {
            dbg("need init xmem file.\n");
            return 1;
        }
        _vDisp(2, szFileName);
        sprintf(szBuf, "len=%d", info.size);
        _vDisp(3, szBuf);
        _uiGetKey();
    }
    return 0;

}

void _vSetBackLight(int val)
{
    //此处按普通和高亮两情况
    if(val<=0)
        val = LCDBACKLIGHT_DEFAULT;
    else
        val = LCDBACKLIGHT_DEFAULT+30;  //高亮 for 显示二维码
    lcd_SetBackLight(val);     
}


//g_pUserFlushIcon = HardwareTestFlushIcon;

extern int gprs_sleepenablestate(void);
extern int gprs_at_sleep(int mode);

int sg_iWakeUpFlag=0;
int sg_iCommType=VPOSCOMM_GPRS;     //1-wifi 2-grps/4g

void vSetCommTypeforSleep(int type)
{
    sg_iCommType=type;
}
void vSetWakeUpEventFlag(int flag)
{
    sg_iWakeUpFlag=flag;
}
int iGetWakeUpEventFlag(void)
{
    return sg_iWakeUpFlag;
}

int sleepret[6];
/**
 * @fn          void gprs_at_EnterSleepMode(void)
 * @brief       send AT + QSCLK and pullup the WAKEUP pin.
 * 
 * @param[in]   void  
 * @return      void
 */
int gprs_at_EnterSleepMode(int mode)
{
    int retry = 3;
    int ret;
    do
    {
        ret=gprs_at_sleep(mode);
        if(mode==1)
            sleepret[3-retry]=ret;
        else
            sleepret[3+3-retry]=ret;
        if( ret== 0)
        {            
            return 0;
        }
        mdelay(100);
    }while(--retry);    
    return -1;
}

int iCheck4GSleep(void *p)
{
    uchar buf[50];
    int i;

    _vFlushKey();
    for(i=0; i<6; i++)
    {
        if(i<3)
        {
            sprintf(buf, "休眠%d:%d", i+1, sleepret[i]);
        }else
        {
            sprintf(buf, "唤醒%d:%d", i+1-3, sleepret[i]);
        }   
        _vDisp(i+1, buf);
    }
    _uiGetKey();
    return 0;
}

void gprs_entersleepmode(void)
{
    if (gprs_getpowerstate() == VCC_POWERUP)
    {
        if(gprs_sleepenablestate() == 0)
        {
            if(gprs_at_EnterSleepMode(1))
            {
                err("gprs enter sleep mode error\n");
            }else
            {
                gprs_sleep();
                dbg("gprs enter sleep mode success\n");
            }
        }
    }
}
extern void vSetCommSleep(char sleep);
void vUserSleep(void)
{
    dbg("user sleep.\n");
    memset(sleepret, 0, sizeof(sleepret));
    vSetCommSleep(1);
    
    //sleep in for gprs
#ifdef COMM_MODULE_SLEEPMODE_ENABLE
    if(sg_iCommType==VPOSCOMM_GPRS)
        gprs_entersleepmode();
#endif
}

extern void vCommModuleWakeUpAt(void);
void vUserWakeUp(void)
{
    dbg("user wakeup1.\n");
#ifdef COMM_MODULE_SLEEPMODE_ENABLE
    if(sg_iCommType==VPOSCOMM_GPRS && gprs_sleepenablestate())
    {
#if 0//ndef PROJECT_CY20P_LITE
        gprs_at_EnterSleepMode(0);
        gprs_wakeup();
#else
		gprs_wakeup();
        
        vCommModuleWakeUpAt();      //唤醒时先发一条AT指令,不接收响应,兼容需要任意指令唤醒休眠的模块
        dbg("gprs wakeup2.\n");
        
		gprs_at_EnterSleepMode(0);        
#endif
    }
#endif
    
    if(sg_iCommType==VPOSCOMM_WIFI)
    {
        vCommModuleWakeUpAt();      //唤醒时先发一条AT指令,不接收响应,兼容需要任意指令唤醒休眠的模块
        dbg("wifi wakeup2.\n");
    }
    
    vSetCommSleep(0);
    
    //flushIcons();
    
    _vFlushKey();
    vSetWakeUpEventFlag(1);
}

#ifdef USE_1903S
extern int setUSBdownloadFlag(void);
int enterdownloadmode(void *m)
{
    int ret;
    int retry = 10;
    char buf[32];
    
    LcdClear();
    LcdPutscc("是否进入下载模式?");
    if(_uiGetKey()!=_KEY_ENTER)
        return 1;
    
    do{
        ret = setUSBdownloadFlag();
        if(ret)
        {
            sprintf(buf,"Code=%d,重试",ret);
            LcdPutscc(buf);
            mdelay(200);
        }else
        {
            systemReboot();
        }
    }while(retry-- > 0);

    LcdPutscc("设置失败!");
    _uiGetKey();
    return 1;
}
#endif

char sg_ProductInfo[4][20];
//module: 0x01-扫码 0x02:wifi 0x03-gprs/4g通讯
int iPosHardwareModule(uchar module, char *pszModuleInfo, int size)
{
    int ret=0;
#ifdef USE_HW_CONFIGFILE  
    //读取配置
    lfs_file_t file;
    char buf[100], info[4][20];
    char *p0, *p1;
    char ch; 
#endif
    
    if(pszModuleInfo)
        pszModuleInfo[0]=0;
    
    #ifdef NO_QRTRANS
    if(module==CHK_HAVE_CAM)
    {
        return 0;
    }
    #endif
    
    #ifdef NO_WIFI
    if(module==CHK_HAVE_WIFI)
    {
        return 0;
    }
    #endif
    
    #ifdef NO_TP
    if(module==CHK_HAVE_TP)
    {
        return 0;
    }
    #endif
    
#if 0   //debug
    if(module==CHK_HAVE_CAM)
        return 1;
#endif
    
#ifdef USE_HW_CONFIGFILE  
    if(sg_ProductInfo[0][0]==0)
    {
        ret = sys_lfs_file_open(&file, "ProductCode", LFS_O_RDONLY);
        if(ret)
            return -1;
        
        memset(buf, 0, sizeof(buf));
        ret=sys_lfs_file_read(&file, buf, sizeof(buf)-1);
        sys_lfs_file_close(&file);
        
        if(ret<=0)
        {
            dbg("open ProductCode file fail.\n");
            return -2;
        }
        if(pszModuleInfo && size>0)
        {
            strncpy(pszModuleInfo, buf, size-1);
            pszModuleInfo[size-1]=0;
        }
    /*
    CY20P-1TNB-060NS1
    具体信息如下：
    CY20传统POS打印版；
    主MCU为兆讯1903 121 PIN芯片，通讯方式为4G，黑白屏，金士盾内部尺寸1，分辨率128*96；
    产品配置为实体SIM卡，不支持PSAM，两轨磁头，摄像头不支持，GPS不支持，NFC支持，TP支持，外壳颜色为标准色，客户定制码为盛迪嘉版本1。    
        
    格式：info[0]: 产品名
        info[1]: MCU[1]+2G/3G/4G[1]+WIFI/蓝牙[1]+屏幕[1]
        info[2]: 第一字节(0-F): 对应的4个bit从左到右依次为:sim;psam;磁头;摄像头
                第二字节(0-F): 对应的4个bit从左到右依次为:GPS;NFC;TP;预留
                第三字节(0-F): 预留
                第四字节: 颜色
                第五字节: 客户， S-盛迪嘉 L-拉卡拉
                第六字节: 序号, 该客户的不同硬件版本，从1开始递增
    */
        memset(info, 0, sizeof(info));
        p0=buf;
        for(ret=0; ret<4; ret++)
        {
            p1=strchr(p0, '-');
            if(p1)
            {
                memcpy(info[ret], p0, p1-p0);
                p0=p1+1;
            }else
            {
                strcpy(info[ret], p0);
                break;
            }
        }
        
        memcpy(sg_ProductInfo, info, sizeof(sg_ProductInfo));
    }else
    {
        memcpy(info, sg_ProductInfo, sizeof(sg_ProductInfo));
        sprintf(buf, "%s-%s-%s", info[0], info[1], info[2]);
        if(info[3])
            sprintf(buf+strlen(buf), "-%s", info[3]);
        if(pszModuleInfo && size>0)
        {
            strncpy(pszModuleInfo, buf, size-1);
            pszModuleInfo[size-1]=0;
        }
    }
    
    if(module==0)
        return 0;

    dbg("ProductCode:[%s]\n", buf);
    dbg(" info[0]:[%s]\n info[1]=%s\n info[2]=%s\n info[3]=%s\n", info[0], info[1], info[2], info[3]);

    ret=0;
    switch(module)
    {
        case CHK_HAVE_CAM:      //0x01
            ch=info[2][0];
            if(ch<'0' || ch>'F')
                break;
            if(ch>='A')
                ch=ch-'A'+10;
            else
                ch-='0';
            
            if(ch&0x01)
                ret=1;
            dbg("CHK_HAVE_CAM:info[2][0]=[%02X], ret=%d\n", ch, ret);
            break;
        case CHK_HAVE_WIFI:     //0x02
            if(info[1][2]=='W' || info[1][2]=='A')
                ret=1;
            break;
        case CHK_HAVE_GPRS:
            if(info[1][1]!='N')     //'G':GPRS 'T':4G
                ret=1;
            break;
        case CHK_HAVE_TP:
            ch=info[2][1];
            if(ch<'0' || ch>'F')
                break;
            if(ch>='A')
                ch=ch-'A'+10;
            else
                ch-='0';
            
            if(ch&0x02)
                ret=1;
            dbg("CHK_HAVE_TP:info[2][1]=[%02X], ret=%d\n", ch, ret);
            break;
         case CHK_HAVE_EXTLED:
            ch=info[2][2];
            if(ch<'0' || ch>'F')
                break;
            if(ch>='A')
                ch=ch-'A'+10;
            else
                ch-='0';
            
            if(ch&0x02)
                ret=1;
            dbg("CHK_HAVE_EXTLED:info[2][2]=[%02X], ret=%d\n", ch, ret);
            break;
         case CHK_HAVE_BATTERY:
            ch=info[2][2];
            if(ch<'0' || ch>'F')
                break;
            if(ch>='A')
                ch=ch-'A'+10;
            else
                ch-='0';
            
            if(ch&0x08)     //1表示无电池
                ret=0;
            else
                ret=1;
            dbg("CHK_HAVE_BATTERY:info[2][2]=[%02X], ret=%d\n", ch, ret);
            break;
        default:
            break;
    }
    return ret;
#else
    return 1;
#endif
}

void vFixCY21ProductCode20230208(void)
{
#ifdef PROJECT_CY21    
    char tusn[30];
    char buf[100];
    lfs_file_t file;
    
    _uiGetSerialNo(tusn);
    if(tusn[0]==0)
        return;
        
    iPosHardwareModule(CHK_HAVE_GPRS, NULL, 0);		//用CHK_HAVE_GPRS或CHK_HAVE_BATTERY确保有读取配置文件的操作
    if(memcmp(sg_ProductInfo[3], "LB", 2)!=0)
        return;
    if(strcmp(sg_ProductInfo[0], "CY21")!=0)
        return;
        
    //22年从22484批次开始至22531批次,排除22491和22492批次
    //23年只处理23064批次
    //CY21-22484: 00007302422484000001-00007302422484010000(22年需处理起始批次)
    //CY21-22531: 00007302422531000001-00007302422531003000(22年需处理终止批次)
    //CY21-22491: 00007302422491000001-00007302422491003000(排除)
    //CY21-22492: 000073024HYBL0040101-000073024HYBL0050100(排除22492, 也就是HYBL只处理22年50101至70300的序号)
    //CY21_23064: 00007302423064000001-00007302423064010000(23年需处理)    
    if( (memcmp(tusn, "00007302422484", 14)>=0 && memcmp(tusn, "00007302422531", 14)<=0 && memcmp(tusn, "00007302422491", 14)!=0) || 
        (strcmp(tusn, "000073024HYBL0050101")>=0 && strcmp(tusn, "000073024HYBL0070300")<=0) ||
         memcmp(tusn, "00007302423064", 14)==0 )
    {
        sprintf(buf, "%s-%s-%s-%s", sg_ProductInfo[0], sg_ProductInfo[1], sg_ProductInfo[2], "LC"/*sg_ProductInfo[3]*/);
        
        if(sys_lfs_file_open(&file, "ProductCode", LFS_O_WRONLY))
            return;
        sys_lfs_file_write(&file, buf, strlen(buf));
        sys_lfs_file_close(&file);
        
        strcpy(sg_ProductInfo[3], "LC");
        
        //POS重启
        //systemReboot();
    }
#endif
    return;
}

void _vSetAllLed(uchar blue, uchar yellow, uchar green, uchar red)
{
#ifdef ENABLE_NFCLED    
    uchar led;
    
    dbg("set led in:%d %d %d %d\n", blue, yellow, green, red);
    
    //底层0为亮灯,1为灭灯
    blue=!blue;
    yellow=!yellow;
    green=!green;
    red=!red;
    
    //blue:bit0(0x01), yellow:bit1(0x02), green:bit2(0x04), red:(bit3(0x08)
    led=(blue)|(yellow<<1)|(green<<2)|(red<<3);
    dbg("set led:%d %d %d %d, %02X\n", blue, yellow, green, red, led);

    setnfcleds(led);
#endif
}
/*
void _vSetLed(uchar color, uchar onoff)
{
    int led;
    
    led=getnfcledstas();
    dbg("getnfcledstas:%02X\n", led);
    if(onoff)
    {
        
    }else
    {

    }
    
    dbg("set Led:%02X\n", led);
}
*/

void _vSetAutoPowerOff(ulong ulTimeSec)
{
    #if 1
    autoPowerdown_str autopd;
    
    if(ulTimeSec==0)
    {
        //设定低压自动关机
        autopd.mode=SHUTDOWN_AUTO;
        autopd.seconds=0;
    }else
    {
        //休眠指定时间后关机
        autopd.mode=SHUTDOWN_SETTIME;
        autopd.seconds=ulTimeSec;
    }
    setAutoPowerDown(autopd);
    //autoPowerdown_str autopd = {SHUTDOWN_SETTIME, 3600}; 
    //setAutoPowerDown(autopd);      // 设定休眠3600秒后关机.
#endif
}

void _vSetDispDataIcon(unsigned short val)
{
#ifdef ST7567    
    if(st7576geticon(ICON_DATA)!=val)
        st7576seticon(ICON_DATA, val);
#endif    
}

void vDispWaitQrImg(int iInitFlag)
{
    int len=5;              //四角框短线长度
    int x0,y0;
    int width=30;           //扫码框的宽度
    
    static int offset=2;
    static ulong t0=0;
    ulong t1;
        
    t1=_ulGetTickTime();
    if(!iInitFlag && t1-t0<300)
    {
        return;
    }
    t0=t1;
    
    //128*64
    x0=(128-width)/2;
    y0=(64-width)/2;
    
    if(iInitFlag)
    {
        //_vCls();
    
        //vDispMid(1, "金额:100.00");
        
        //左上角
        UG_DrawLine(x0, y0, x0+len, y0, C_BLACK);
        UG_DrawLine(x0, y0, x0, y0+len, C_BLACK);
        UG_DrawLine(x0-1, y0-1, x0+len, y0-1, C_BLACK);
        UG_DrawLine(x0-1, y0, x0-1, y0+len, C_BLACK);
        
        //左下角
        UG_DrawLine(x0, y0+width, x0+len, y0+width, C_BLACK);
        UG_DrawLine(x0, y0+width-len, x0, y0+width, C_BLACK);
        UG_DrawLine(x0-1, y0+1+width, x0+len, y0+1+width, C_BLACK);
        UG_DrawLine(x0-1, y0+width-len, x0-1, y0+width, C_BLACK);
        
        //右上角
        UG_DrawLine(x0+width-len, y0, x0+width, y0, C_BLACK);
        UG_DrawLine(x0+width, y0, x0+width, y0+len, C_BLACK);
        UG_DrawLine(x0+width-len, y0-1, x0+width+1, y0-1, C_BLACK);
        UG_DrawLine(x0+1+width, y0, x0+1+width, y0+len, C_BLACK);
        
        //右下角
        UG_DrawLine(x0+width, y0+width-len, x0+width, y0+width, C_BLACK);
        UG_DrawLine(x0+width-len, y0+width, x0+width, y0+width, C_BLACK);
        UG_DrawLine(x0+1+width, y0+width-len, x0+1+width, y0+width, C_BLACK);
        UG_DrawLine(x0+width-len, y0+1+width, x0+width+1, y0+1+width, C_BLACK);
        
        //vDispMid(5, "请扫客户的付款码");
        
        //中间线
        offset=2;
        UG_DrawLine(x0+2, y0+len+offset, x0+width-3, y0+len+offset, C_BLACK);
    }else
    {
        //消除原线
        UG_DrawLine(x0+2, y0+len+offset, x0+width-3, y0+len+offset, C_WHITE);
        
        //画新线
        offset++;
        if(y0+len+offset>y0+width-len-2)
            offset=2;
        UG_DrawLine(x0+2, y0+len+offset, x0+width-3, y0+len+offset, C_BLACK);
    }
}




