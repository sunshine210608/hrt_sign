/***********************************************************************
pack8583.c
Define serial functions of 8583 package for unix
Created at 03/2000
Modified at 20070112
    增加一个宏TYPE_Z_FILL_F，定义该宏后变长的Z型域后补F
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
//#include "vposface.h"
#include "Pack8583.h"

//#define VarAttrN_RightJustify // 如果定义了该宏，N型可变长域采取右靠齐 (缺省不定义该宏)
//#define TYPE_Z_FILL_F         // 如果定义了该宏，Z型后补F              (缺省不定义该宏)

static int iPackElement(const FIELD_ATTR *pAttr, unsigned char *pusIn,
                        unsigned char *pusOut, unsigned int *puiOutLen);

static int iUnPackElement(const FIELD_ATTR *pAttr, unsigned char *pusIn,
                          unsigned char *pusOut, unsigned int *puiInLen);

/*********************************************************************
Function      :   change 8583 struct to 8583 packet
Param in      :
   pMsgAttr   :   struct array to define attribute of message
   pDataAttr  :   struct array to define attribute of bitmap and
                  data elements
   pSt8583    :   unsigned char pointer to point to struct 8583
		     eg. ST_8583 st8583;
		         pSt8583 = (unsigned char *)&st8583;
Param out     :
   pusOut     :   8583 packet
   puiOutLen  :   bytes of 8583 packet
Return Code   :
   0          :   success
   <0         :   fail,  number of error field
                  > -1000 : length of msg field overflow
                  > -2000 : length of data field overflow
                     else : attribute of field define error
*********************************************************************/
int iPack8583(const FIELD_ATTR *pMsgAttr, const FIELD_ATTR *pDataAttr,
              unsigned char *pSt8583, unsigned char *pusOut, unsigned int *puiOutLen)
{
   unsigned char *pusI, *pusO, *pusB;
   int i, iMsgLen, iDataLen, iFieldLen, iMsgNum, iRet;
   unsigned int uiBitmap;

   pusI = pSt8583;
   pusO = pusOut;
   iMsgLen = 0;
   iDataLen = 0;
   *puiOutLen = 0;

   /*** generate message of 8583 packet ***/
   iMsgNum = 0;
   while ((pMsgAttr + iMsgNum)->eElementAttr != Attr_Over)
      iMsgNum++;

   for (i = 0; i < iMsgNum; i++)
   {
      if ((pMsgAttr + i)->eElementAttr == Attr_UnUsed)
         continue;
      iRet = iPackElement(pMsgAttr + i, pusI, pusO, (unsigned int *)&iFieldLen);
      if (iRet <= 0)
         return ((-1) * (i + 1));
      pusI += ((pMsgAttr + i)->uiLength + 2);
      pusO += iFieldLen;
      iMsgLen += iFieldLen;
   }

   /*** generate data and bitmap of 8583 packet ***/
   uiBitmap = (pDataAttr + 0)->uiLength;
   pusI += (uiBitmap * 2);
   pusB = pusO;

   pusO += (uiBitmap * 2);
   iDataLen = uiBitmap * 2;
   memset(pusB, 0, uiBitmap * 2);

   for (i = 1; i < (int)uiBitmap * 2 * 8; i++)
   {
      if ((pDataAttr + i)->eElementAttr == Attr_Over)
         break;
      if ((pDataAttr + i)->eElementAttr == Attr_UnUsed)
         continue;
      iRet = iPackElement(pDataAttr + i, pusI, pusO, (unsigned int *)&iFieldLen);
      if (iRet < 0)
         return ((-1) * (1000 + (i + 1)));
      if (iRet > 0)
      {
         pusO += iFieldLen;
         iDataLen += iFieldLen;
         *(pusB + (i / 8)) |= (0x80 >> (i % 8));
      }
      pusI += ((pDataAttr + i)->uiLength + 2);
   }
   if (i % ((pDataAttr + 0)->uiLength * 8))
      return ((-1) * (2000 + (i + 1)));

   for (i = uiBitmap * 2 - 1; i >= 0; i--)
      if (*(pusB + i))
         break;
   if (i >= 8)
      *pusB |= 0x80;
   else
   {
      memmove(pusB + uiBitmap, pusB + uiBitmap * 2, iDataLen - uiBitmap * 2);
      iDataLen -= uiBitmap;
   }

   *puiOutLen = iMsgLen + iDataLen;
   return (0);
}

/***********************************************************************
Function      :   change 8583 packet to 8583 struct
Param in      :
   pMsgAttr   :   struct array to define attribute of message
   pDataAttr  :   struct array to define attribute of bitmap and
   pusIn      :   8583 packet
   uiInLen    :   bytes of 8583 packet
Param out     :
   pSt8583    :   unsigned char pointer to point to struct 8583
		     eg. ST_8583 st8583;
		         pSt8583 = (unsigned char *)&st8583;
Return Code   :
   0          :   success
   <0         :   number of error field
                  > -1000 : length of msg field overflow
                  > -2000 : length of data field overflow
                     else : attribute of field define error
*********************************************************************/
int iUnPack8583(const FIELD_ATTR *pMsgAttr, const FIELD_ATTR *pDataAttr,
                unsigned char *pusIn, unsigned int uiInLen, unsigned char *pSt8583)
{
   unsigned int uiBitmap;
   unsigned char *pusI, *pusO, *pusB;
   int i, iLen, iMsgNum, iFieldLen, iRet;

   pusI = pusIn;
   pusO = pSt8583;
   iLen = 0;

   /*** generate message of 8583 struct ***/
   iMsgNum = 0;
   while ((pMsgAttr + iMsgNum)->eElementAttr != Attr_Over)
      iMsgNum++;

   for (i = 0; i < iMsgNum; i++)
   {
      if ((pMsgAttr + i)->eElementAttr == Attr_UnUsed)
         continue;
      iRet = iUnPackElement(pMsgAttr + i, pusI, pusO, (unsigned int *)&iFieldLen);
      if (iRet < 0)
         return ((-1) * (i + 1));
      pusI += iFieldLen;
      pusO += ((pMsgAttr + i)->uiLength + 2);
      iLen += iFieldLen;
   }

   /*** generate data and bitmap of 8583 struct ***/
   pusB = pusI;
   if (pusB[0] & 0x80)
      uiBitmap = (pDataAttr + 0)->uiLength * 2;
   else
      uiBitmap = (pDataAttr + 0)->uiLength;
   memcpy(pusO, pusB, uiBitmap);
   pusI += uiBitmap;
   pusO += (pDataAttr + 0)->uiLength * 2;
   iLen += uiBitmap;

   for (i = 1; i < (int)(uiBitmap * 8); i++)
   {
      if ((pDataAttr + i)->eElementAttr == Attr_Over)
         return ((-1) * (2000 + (i + 1)));
      if (*(pusB + i / 8) & (0x80 >> i % 8))
      {
         if ((pDataAttr + i)->eElementAttr == Attr_UnUsed)
            return ((-1) * (2000 + (i + 1)));
         iRet = iUnPackElement(pDataAttr + i, pusI, pusO,
                               (unsigned int *)&iFieldLen);
         if (iRet < 0)
            return ((-1) * (1000 + (i + 1)));
         pusI += iFieldLen;
         iLen += iFieldLen;
      }

      if ((pDataAttr + i)->eElementAttr == Attr_UnUsed)
         continue;
      pusO += ((pDataAttr + i)->uiLength + 2);
   } /*** for ***/

   if ((int)uiInLen != iLen)
      return (UnPackLenErr);

   return (0);
}

/*********************************************************************
Function      :   generate data element of 8583 packet
Param in      :
   pAttr      :   struct pointer to define attribute of data element
   pusIn      :   source data
Param out     :
   pusOut     :   object data
   puiOutLen  :   bytes of object data
Return Code   :
   = 0        :   absent
   = 1        :   present
   < 0        :   length overflow
*********************************************************************/
int iPackElement(const FIELD_ATTR *pAttr, unsigned char *pusIn,
                 unsigned char *pusOut, unsigned int *puiOutLen)
{
   int i, j, iInLen, iOutLen = 0;

   *puiOutLen = 0;

   if (pAttr->eElementAttr != Attr_b)
      iInLen = strlen((char *)pusIn);
   else
   {
      iInLen = ((unsigned int)pusIn[0] << 8) | (unsigned int)pusIn[1];
      pusIn += 2;
   }

   if (iInLen <= 0)
      return (0);
   if (iInLen > (int)(pAttr->uiLength))
      return (-1);

   switch (pAttr->eLengthAttr)
   {
   case Attr_fix:
      iOutLen = pAttr->uiLength;
      break;
   case Attr_var1:
      pusOut[0] = ((iInLen / 10) << 4) | (iInLen % 10);
      pusOut++;
      iOutLen = 1 + iInLen;
      break;
   case Attr_var2:
      pusOut[0] = iInLen / 100;
      pusOut[1] = (((iInLen % 100) / 10) << 4) | (iInLen % 10);
      pusOut += 2;
      iOutLen = 2 + iInLen;
      break;
   } /*** switch ***/

   switch (pAttr->eElementAttr)
   {
   case Attr_n:
   case Attr_ln:
      switch (pAttr->eLengthAttr)
      {
      case Attr_fix:
         iOutLen = (pAttr->uiLength + 1) / 2;
         memset(pusOut, 0, iOutLen);
         if (pAttr->eElementAttr == Attr_n)
         {
            for (i = 0, j = 0; i < iInLen; i += 2, j++)
            {
               if (i == iInLen - 1)
               {
                  if (toupper(pusIn[iInLen - i - 1]) < 'A')
                     pusOut[iOutLen - j - 1] = (pusIn[iInLen - i - 1] & 0x0f);
                  else
                     pusOut[iOutLen - j - 1] = toupper(pusIn[iInLen - i - 1]) - 'A' + 0x0A;
               }
               else
               {
                  if (toupper(pusIn[iInLen - i - 2]) < 'A')
                     pusOut[iOutLen - j - 1] = (pusIn[iInLen - i - 2] & 0x0f) << 4;
                  else
                     pusOut[iOutLen - j - 1] = (toupper(pusIn[iInLen - i - 2]) - 'A' + 0x0A) << 4;

                  if (toupper(pusIn[iInLen - i - 1]) < 'A')
                     pusOut[iOutLen - j - 1] |= (pusIn[iInLen - i - 1] & 0x0f);
                  else
                     pusOut[iOutLen - j - 1] |= (toupper(pusIn[iInLen - i - 1]) - 'A' + 0x0A);
               }
            } /*** for ***/
         }
         else if (pAttr->eElementAttr == Attr_ln)
         {
            // 将字符串pusIn(转换前长度=iInLen) twoone 到pusOut (左靠齐)
            for (i = 0; i < iInLen; i++)
            {
               j = toupper(pusIn[i]);
               if (j >= 'A')
                  j = j - 'A' + 0x0A;
               else
                  j = j - '0';
               if (i % 2 == 0)
                  pusOut[i / 2] = j << 4;
               else
                  pusOut[i / 2] |= j;
            }
         }
         break;

      case Attr_var1:
      case Attr_var2:
         iOutLen = iOutLen - iInLen + (iInLen + 1) / 2;

#ifdef VarAttrN_RightJustify
         memset(pusOut, 0, (iInLen + 1) / 2);
         for (i = 0, j = 0; i < iInLen; i += 2, j++)
         {
            if (i == iInLen - 1)
            {
               if (toupper(pusIn[iInLen - i - 1]) < 'A')
                  pusOut[(iInLen + 1) / 2 - j - 1] = (pusIn[iInLen - i - 1] & 0x0f);
               else
                  pusOut[(iInLen + 1) / 2 - j - 1] = toupper(pusIn[iInLen - i - 1]) - 'A' + 0x0A;
            }
            else
            {
               if (toupper(pusIn[iInLen - i - 2]) < 'A')
                  pusOut[(iInLen + 1) / 2 - j - 1] = (pusIn[iInLen - i - 2] & 0x0f) << 4;
               else
                  pusOut[(iInLen + 1) / 2 - j - 1] = (toupper(pusIn[iInLen - i - 2]) - 'A' + 0x0A) << 4;

               if (toupper(pusIn[iInLen - i - 1]) < 'A')
                  pusOut[(iInLen + 1) / 2 - j - 1] |= (pusIn[iInLen - i - 1] & 0x0f);
               else
                  pusOut[(iInLen + 1) / 2 - j - 1] |= (toupper(pusIn[iInLen - i - 1]) - 'A' + 0x0A);
            }
         } /*** for ***/
#else
         for (i = 0; i < (iInLen + 1) / 2; i++)
         {
            if (toupper(pusIn[2 * i]) < 'A')
               pusOut[i] = (pusIn[2 * i] & 0x0f) << 4;
            else
               pusOut[i] = (toupper(pusIn[2 * i]) - 'A' + 0x0A) << 4;

            if (toupper(pusIn[2 * i + 1]) < 'A')
               pusOut[i] |= (pusIn[2 * i + 1] & 0x0f);
            else
               pusOut[i] |= (toupper(pusIn[2 * i + 1]) - 'A' + 0x0A);
         }
#endif
         break;
      } /*** switch(pAttr->eLengthAttr) ***/
      break;

   case Attr_z:
      switch (pAttr->eLengthAttr)
      {
      case Attr_fix:
         iOutLen = (pAttr->uiLength + 1) / 2;
         memset(pusOut, 0, iOutLen);
         for (i = 0, j = 0; i < iInLen; i += 2, j++)
         {
            if (i == iInLen - 1)
               pusOut[iOutLen - j - 1] = (pusIn[iInLen - i - 1] & 0x0f);
            else
               pusOut[iOutLen - j - 1] = ((pusIn[iInLen - i - 2] & 0x0f) << 4) |
                                         (pusIn[iInLen - i - 1] & 0x0f);
         } /*** for ***/
         break;

      case Attr_var1:
      case Attr_var2:
         iOutLen = iOutLen - iInLen + (iInLen + 1) / 2;
         memset(pusOut, 0, iOutLen);
         for (i = 0; i < (iInLen + 1) / 2; i++)
            pusOut[i] = ((pusIn[2 * i] & 0x0f) << 4) | (pusIn[2 * i + 1] & 0x0f);
#ifdef TYPE_Z_FILL_F
         if (iInLen % 2 == 1)
            pusOut[i - 1] |= 0x0f;
#endif
         break;
      } /*** switch(pAttr->eLengthAttr) ***/
      break;

   case Attr_b:
      switch (pAttr->eLengthAttr)
      {
      case Attr_fix:
         memset(pusOut, 0, iOutLen);
         memcpy(pusOut, pusIn, iInLen);
         break;

      case Attr_var1:
      case Attr_var2:
         memcpy(pusOut, pusIn, iInLen);
         break;
      }
      break;

   case Attr_a:
      switch (pAttr->eLengthAttr)
      {
      case Attr_fix:
         memset(pusOut, ' ', iOutLen);
         memcpy(pusOut, pusIn, iInLen);
         break;

      case Attr_var1:
      case Attr_var2:
         memcpy(pusOut, pusIn, iInLen);
         break;
      }
      break;
   } /*** switch(pAttr->eElementAttr) ***/

   *puiOutLen = iOutLen;
   return (1);
}

/*********************************************************************
Function      :   generate data element of 8583 struct
Param in      :
   pAttr      :   struct pointer to define attribute of data element
   pusIn      :   source data
Param out     :
   pusOut     :   object data
   puiInLen   :   bytes of source data to be used
Return Code   :
   = 0        :   success
   < 0        :   length overflow
*********************************************************************/
int iUnPackElement(const FIELD_ATTR *pAttr, unsigned char *pusIn,
                   unsigned char *pusOut, unsigned int *puiInLen)
{
   int i, j, iInLen = 0, iTmpLen = 0;

   memset(pusOut, 0, pAttr->uiLength);
   *puiInLen = 0;

   switch (pAttr->eLengthAttr)
   {
   case Attr_fix:
      iInLen = pAttr->uiLength;
      iTmpLen = iInLen;
      break;
   case Attr_var1:
      iTmpLen = (pusIn[0] >> 4) * 10 + (pusIn[0] & 0x0f);
      pusIn++;
      iInLen = 1 + iTmpLen;
      break;
   case Attr_var2:
      iTmpLen = (pusIn[0] & 0x0f) * 100 + (pusIn[1] >> 4) * 10 +
                (pusIn[1] & 0x0f);
      pusIn += 2;
      iInLen = 2 + iTmpLen;
      break;
   } /*** switch ***/

   if (iTmpLen > (int)(pAttr->uiLength))
      return (-1);
   if (pAttr->eElementAttr == Attr_b)
   {
      pusOut[0] = (unsigned char)(iTmpLen >> 8);
      pusOut[1] = (unsigned char)iTmpLen;
      pusOut += 2;
   }

   switch (pAttr->eElementAttr)
   {
   case Attr_n:
   case Attr_ln:
      switch (pAttr->eLengthAttr)
      {
      case Attr_fix:
         iInLen = (pAttr->uiLength + 1) / 2;
         if (pAttr->eElementAttr == Attr_n)
         {
            for (i = 0, j = 0; i < (int)(pAttr->uiLength); i += 2, j++)
            {
               if ((pusIn[iInLen - j - 1] & 0x0f) < 0x0A)
                  pusOut[pAttr->uiLength - i - 1] = (pusIn[iInLen - j - 1] & 0x0f) | 0x30;
               else
                  pusOut[pAttr->uiLength - i - 1] = (pusIn[iInLen - j - 1] & 0x0f) - 0x0A + 'A';

               if (i != (int)(pAttr->uiLength - 1))
               {
                  if ((pusIn[iInLen - j - 1] >> 4) < 0x0A)
                     pusOut[pAttr->uiLength - i - 2] = (pusIn[iInLen - j - 1] >> 4) | 0x30;
                  else
                     pusOut[pAttr->uiLength - i - 2] = (pusIn[iInLen - j - 1] >> 4) - 0x0A + 'A';
               }
            }
         }
         else if (pAttr->eElementAttr == Attr_ln)
         {
            // 将二进制串pusIn(左靠齐) onetwo(转换后长度=iInLen) 到pusOut
            // by yujun 20070105
            for (i = 0; i < (int)(pAttr->uiLength); i++)
            {
               if (i % 2 == 0)
                  j = (pusIn[i / 2] >> 4) & 0x0f;
               else
                  j = pusIn[i / 2] & 0x0f;
               if (j < 10)
                  pusOut[i] = j + '0';
               else
                  pusOut[i] = j - 10 + 'A';
            }
         }
         break;

      case Attr_var1:
      case Attr_var2:
         iInLen = iInLen - iTmpLen + (iTmpLen + 1) / 2;

#ifdef VarAttrN_RightJustify
         for (i = 0, j = 0; i < iTmpLen; i += 2, j++)
         {
            if ((pusIn[(iTmpLen + 1) / 2 - j - 1] & 0x0f) < 0x0A)
               pusOut[iTmpLen - i - 1] = (pusIn[(iTmpLen + 1) / 2 - j - 1] & 0x0f) | 0x30;
            else
               pusOut[iTmpLen - i - 1] = (pusIn[(iTmpLen + 1) / 2 - j - 1] & 0x0f) - 0x0A + 'A';

            if (i != (int)pAttr->uiLength - 1)
            {
               if ((pusIn[(iTmpLen + 1) / 2 - j - 1] >> 4) < 0x0A)
                  pusOut[iTmpLen - i - 2] = (pusIn[(iTmpLen + 1) / 2 - j - 1] >> 4) | 0x30;
               else
                  pusOut[iTmpLen - i - 2] = (pusIn[(iTmpLen + 1) / 2 - j - 1] >> 4) - 0x0A + 'A';
            }
         }
#else
         for (i = 0; i < (iTmpLen / 2); i++)
         {
            if ((pusIn[i] >> 4) < 0x0A)
               pusOut[2 * i] = (pusIn[i] >> 4) | 0x30;
            else
               pusOut[2 * i] = (pusIn[i] >> 4) - 0x0A + 'A';

            if ((pusIn[i] & 0x0f) < 0x0A)
               pusOut[2 * i + 1] = (pusIn[i] & 0x0f) | 0x30;
            else
               pusOut[2 * i + 1] = (pusIn[i] & 0x0f) - 0x0A + 'A';
         }

         if (iTmpLen % 2)
         {
            if ((pusIn[i] >> 4) < 0x0A)
               pusOut[2 * i] = (pusIn[i] >> 4) | 0x30;
            else
               pusOut[2 * i] = (pusIn[i] >> 4) - 0x0A + 'A';
         }
#endif
         break;
      } /*** switch(pAttr->eLengthAttr) ***/
      break;

   case Attr_z:
      switch (pAttr->eLengthAttr)
      {
      case Attr_fix:
         iInLen = (pAttr->uiLength + 1) / 2;

         for (i = 0, j = 0; i < (int)(pAttr->uiLength); i += 2, j++)
         {
            pusOut[pAttr->uiLength - i - 1] = (pusIn[iInLen - j - 1] & 0x0f) | 0x30;
            if (i != (int)(pAttr->uiLength - 1))
               pusOut[pAttr->uiLength - i - 2] = (pusIn[iInLen - j - 1] >> 4) | 0x30;
         }
         break;

      case Attr_var1:
      case Attr_var2:
         iInLen = iInLen - iTmpLen + (iTmpLen + 1) / 2;

         for (i = 0; i < (iTmpLen / 2); i++)
         {
            pusOut[2 * i] = (pusIn[i] >> 4) | 0x30;
            pusOut[2 * i + 1] = (pusIn[i] & 0x0f) | 0x30;
         }
         if (iTmpLen % 2)
            pusOut[2 * i] = (pusIn[i] >> 4) | 0x30;
         break;
      } /*** switch(pAttr->eLengthAttr) ***/
      break;

   case Attr_b:
      memcpy(pusOut, pusIn, iTmpLen);
      break;

   case Attr_a:
      memcpy(pusOut, pusIn, iTmpLen);
      break;
   } /*** switch(pAttr->eElementAttr) ***/

   *puiInLen = iInLen;
   return (0);
}
