/********************************************************************************

 *************************Copyright (C), 2019, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : screendraw.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2019-11-14
 *
 *
 * @note History:
 * @note        : Jay 2019-11-14
 * @note        :
 *   Modification: Created file

********************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <mhscpu.h>
#include "lcd.h"
#include "systick.h"
#include "touchpanel.h"
#include "keyboard.h"
#include "battery.h"
#include "timer.h"
#include "ugui.h"
//#include "hardwaretest.h"
#include "rtos_tasks.h"

#include "VposFace.h"
#include "pub.h"
#ifdef APP_LKL
#include "MemMana_lkl.h"
#endif
#ifdef APP_SDJ
#include "MemMana_sdj.h"
#endif
#ifdef APP_CJT
#include "MemMana_cjt.h"
#endif


#include "st7789.h"
#include "st7571.h"
#include "st7567.h"

#define SIGNTIMEOUTS       120

#define SIGN_NO_TITLE

#ifdef ST7789
#define SCR_W       320     //屏幕宽度
#define SCR_H       240

//签字区域(图片)起始XY和宽高
#define XSTART      16
#define YSTART      82
#define SIGN_W      288         //为便于计算每行字节数,须为8的倍数
#define SIGN_H      96          //约为宽度的1/3
#endif

#ifdef LCD_GC9106
#define SCR_W       160     //屏幕宽度
#define SCR_H       128

//签字区域(图片)起始XY和宽高
#define XSTART      4
#ifndef SIGN_NO_TITLE
#define YSTART      46
#define SIGN_W      152         //为便于计算每行字节数,须为8的倍数
#define SIGN_H      56          //约为宽度的1/3
#else
#define YSTART      27
#define SIGN_W      152
#define SIGN_H      65
#endif
#endif

#ifdef ST7571
#define MONO_SCR    //单色小屏,分辨率不够,保存文件时扩大一倍保存
#define SCR_W       128     //屏幕宽度
#define SCR_H       96

#define XSTART      4
#define YSTART      32
#define SIGN_W      120         //为便于计算每行字节数,须为8的倍数
#define SIGN_H      40          //约为宽度的1/3
#endif

#ifdef ST7567
#define MONO_SCR    //单色小屏,分辨率不够,保存文件时扩大一倍保存
#define SCR_W       128     //屏幕宽度
#define SCR_H       64

#define XSTART      4
#define YSTART      13
#define SIGN_W      120         //为便于计算每行字节数,须为8的倍数
#define SIGN_H      40          //约为宽度的1/3

//畅捷黑白屏,上传数据固定为的97高度
//#define SIGN_H97

#endif


//#define SIGNSIZE    ((SIGN_W+7)/8*SIGN_H)
#define SIGNSIZE    (SIGN_W/8*SIGN_H)           //SIGN_W固定为8的倍数,不需要+7

#ifdef ST7789
extern unsigned short * getLcdBufferPoint(void);
#endif
extern int bmp2jbg(unsigned char *image, int width, int height, unsigned char **out, int plane_size, int *outlen);
extern int jbg2bmp(unsigned char *jbgData, unsigned int uiJbgLen, unsigned char *bmpData, unsigned long ulOutsize, int *piWidth, int *piHeight, int *piBmpLen);
extern int PrintImage(unsigned char*data,int dalen);
extern void flushStatusBar(int iFlushNow);
#if 0//def ST7571
void LcdBitMapEnable(char set)
{}
#else
extern void LcdBitMapEnable(char set);
#endif

#if defined(PROJECT_CY21)
#undef _KEY_FN
#define _KEY_FN _KEY_UP
#endif

void cleansign(void);
//int xyislegal(uint16_t x, uint16_t y);
//int updateTestPoint(uint16_t x, uint16_t y);

static unsigned char *pGsigndata = NULL;

void cleansign(void)
{
    UG_FillFrame(XSTART-1, YSTART-1, XSTART+SIGN_W, YSTART+SIGN_H, C_GRAY);
    if (pGsigndata)
        memset(pGsigndata, 0, SIGNSIZE);
}

/*
    0F         81
0000 1111  1000 0001
all bit like MSB
*/
void recordsigndata(UG_S16 x, UG_S16 y)
{
    UG_S16 inx;
    unsigned char bitinx;
    
    if(pGsigndata==NULL)
        return;
    if (x < 0 || y < 0)
        return;
    if (x >= SIGN_W || y >= SIGN_H)
        return;
    inx = x / 8 + y * ((SIGN_W+7) / 8);
    bitinx = x % 8;
    pGsigndata[inx] |= (1 << (7 - bitinx)); //MSB
    #if 0//def JTAG_DEBUG
    dbg("x=%d, X(x/8)=%d, y=%d, inx=%d, val=%02X, data=%02X\n", x, x/8, y,  inx, (1 << (7 - bitinx)), pGsigndata[inx]);
    mdelay(20);
    #endif
}


void recordsign_Line(UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2)
{
    UG_S16 n, dx, dy, sgndx, sgndy, dxabs, dyabs, x, y, drawx, drawy;

    dx = x2 - x1;
    dy = y2 - y1;
    dxabs = (dx > 0) ? dx : -dx;
    dyabs = (dy > 0) ? dy : -dy;
    sgndx = (dx > 0) ? 1 : -1;
    sgndy = (dy > 0) ? 1 : -1;
    x = dyabs >> 1;
    y = dxabs >> 1;
    drawx = x1;
    drawy = y1;

    //gui->pset(drawx, drawy,c);
    recordsigndata(drawx, drawy);
    if (dxabs >= dyabs)
    {
        for (n = 0; n < dxabs; n++)
        {
            y += dyabs;
            if (y >= dxabs)
            {
                y -= dxabs;
                drawy += sgndy;
            }
            drawx += sgndx;
            //gui->pset(drawx, drawy,c);
            recordsigndata(drawx, drawy);
        }
    }
    else
    {
        for (n = 0; n < dyabs; n++)
        {
            x += dxabs;
            if (x >= dyabs)
            {
                x -= dyabs;
                drawx += sgndx;
            }
            drawy += sgndy;
            //gui->pset(drawx, drawy,c);
            recordsigndata(drawx, drawy);
        }
    }
}

void lcdcleanAll(void)
{
    //LcdStatusCLear();
    LcdClear();
}

/*
文件名: 日期mmdd[4]_ttc[6].jbg
交易特征码:共8个字节（ASCII表示），构成方式如下：
    对于联机交易：数据块（BLOCK）为清算日期（N4，来自15域应答）+检索参考号（N12，来自37域应答），若终端没有获取到15域，用0000代替。
    对于离线交易 ：数据块（BLOCK）为批次号（N6，来自60.2域请求）+流水号（N6，来自11域请求）＋0000
    上述数据块（BLOCK）的高8字节和低8字节压缩BCD异或后得出的4字节结果, 转为8字节ASCII后为交易特征码。

    拉卡拉为交易日期(13域应答)+检索参考号(37域应答)
*/
int iSignDraw(uchar *pszFileName, uchar *pszSpecCode)
{
    unsigned short key;
    int lastX = 0, lastY = 0;
    short uplift = 0;
    short pointcnt = 0;
#if 0//def ST7571  
    uint8_t rx, ry;
#else    
    int rx, ry;
#endif    
    int x, y;
    int ret = -1;
    
    int timeout=3*60;
    //int dxabs, dyabs;
    int i,j;
#if defined(ST7789) && !defined(USE_1903S)      //彩屏版CY20P
    Boolean lcdmode;
    unsigned short*pLcdBuf;
#else    
    uchar *pMonoLcdBuf;
#endif    
	uchar *sSignBuf=NULL;       //bitmap每行字节数需为4的整数倍((SIGN_W+7)/8+3)/4*4*SIGN_H,此处非标准bmp格式
	uchar *sSignBufNew=NULL;    //单色屏需放大一倍
	uchar *jbg=NULL;
	int   doubleSize=1;
	
    ulong ulTimer;
    char cDrawFlag=0;
    int  result;
    int  plane_size;
    int  firstRun=1;
    uchar ucBmEnFlag=0;
#ifdef MONO_SCR
    int iNewBufSize;
    int iNewOffset=0;
#endif
    int iNewHeight=SIGN_H;
    unsigned short lastkey=0, lastkey2=0;
    
#if defined(ST7567) || defined(JTAG_DEBUG)
    //unsigned short idx;
    uchar tmp[SIGN_W*2+100];
#endif 
#ifndef ST7567
    int k=0;
#endif
    uchar c0, c1;
#ifdef MONO_SCR
    //长宽各扩大一倍
    uchar c2;
    ushort iDrawColor=C_BLACK;
#else
    ushort iDrawColor=C_BLUE;
#endif

#ifdef APP_LKL
    timeout=24*60*60;      //拉卡拉要求必须签名，暂定24小时
#endif
    int iJbgLen=0;
    
    _vCls();
    if(TPOpen()){
        dbg("TPOpen fail.\n");
        vMessage("触屏初始化失败");
        return 1;
    }
    
    sSignBuf=malloc(SIGNSIZE);
#ifdef 	MONO_SCR
    doubleSize=2;
    iNewHeight=SIGN_H*doubleSize;
    iNewBufSize=SIGNSIZE*doubleSize*doubleSize;
    iNewOffset=0;
    #ifdef SIGN_H97
    if(SIGN_H*doubleSize<97)
    {
        iNewHeight=97;
        iNewOffset=(SIGN_W*doubleSize) / 8*((97-SIGN_H*doubleSize)/2);        //顶部空白数据
        iNewBufSize+=(SIGN_W*doubleSize) / 8*(97-SIGN_H*doubleSize);
    }
    #endif
    sSignBufNew=malloc(iNewBufSize);
#else
	sSignBufNew=sSignBuf;
#endif  
    if(sSignBuf==NULL || sSignBufNew==NULL)
    {
        vMessage("内存不足");
        result=-1;
        goto SIGN_OUT;
    }
    pGsigndata=sSignBuf;
    
#if defined(ST7789) && !defined(USE_1903S)    
    lcdmode = lcdgetfullflush();
    if(lcdmode == FALSE)
        lcdsetfullflush((Boolean)TRUE);
    //注意:lcd开flush后(SDJ 2G彩屏),屏幕显示字符需调用lcdflush()才生效
#endif

    lcdcleanAll();//flushStatusBar(1);
	
    _vFlushKey();
    result=-1;
    firstRun=1;
	jbg=NULL;
    do
    {
        //CHECKTIMEOUT;
        /* if(((SMenuItem*)m)->result->autotest && checkIfTestTimeout()){ 
            goto SIGN_OUT; 
        }
        else */{ 
            mdelay(10);
        }
                
        if (_uiKeyPressed() || firstRun==1)
        {
            if(firstRun==1)
                key=_KEY_BKSP;
            else
                key=_uiGetKey();
            
            dbg("lastkey=%d, lastkey2=%d, key=%d[%04X]\n", lastkey, lastkey2, key, key);
            //dbg("KEY_L=%d, KEY_F2=%d, KEY_9=%d\n", KEY_L, KEY_F2, KEY_9);
            _vSetTimer(&ulTimer, timeout*100);
            //签名时依次按功能键+向下键+数字9,询问是否触屏校准,5秒内按确认可进校准
            if(key == _KEY_9 && lastkey==_KEY_FN && lastkey2==_KEY_DOWN)
            {
                vDispCenter(_uiGetVLines(), "进行触屏校准?", 0);
                lcdflush();     //2G彩屏,显示字符后需调用lcdflush
                if(iOK(5)==1)
                {
                    touchpanel_adjust();                    
                    firstRun=1;
                }else
                {
                    #ifndef LCD_GC9106
                    vDispCenter(_uiGetVLines(), "[清除]重签,[确认]保存", 0);
                    #else
                    vDispCenter(_uiGetVLines(), "[清除]重签[确认]保存", 0);
                    #endif
                    lcdflush();
                }
                lastkey=0;
                continue;
            }
            if(key == _KEY_FN)
            {
                lastkey=_KEY_FN;
                lastkey2=0;
                continue;
            }
            if(key == _KEY_DOWN && lastkey==_KEY_FN)
            {
                lastkey2=_KEY_DOWN;
                continue;
            }

            lastkey=0;
            lastkey2=0;

            if(key == _KEY_BKSP)
            {
                memset(sSignBuf, 0, SIGNSIZE);
                lastX = lastY = 0;
                
                pointcnt=0;
                cDrawFlag=0;
                
                lcdcleanAll();//flushStatusBar(1);
                
                if(ucBmEnFlag==0)
                {
                    LcdBitMapEnable(1);
                    ucBmEnFlag=1;
                }
                #ifndef SIGN_NO_TITLE
                vDispCenter(1, "持卡人签名", 0);
                #endif
                #ifndef LCD_GC9106
                vDispCenter(_uiGetVLines(), "[清除]重签,[确认]保存", 0);
                #else
                vDispCenter(_uiGetVLines(), "[清除]重签[确认]保存", 0);
                #endif
                
                dbg("charwidth=%d,char_height=%d\n", LcdGetFont()->char_width, LcdGetFont()->char_height);
                //dbg("DISPLAY_WIDTH=%d,YSTART=%d,SIGN_H=%d,STATUSBAR_H=%d\n", DISPLAY_WIDTH, YSTART, SIGN_H, STATUSBAR_H);
                //_vDispAt((_uiGetVLines()+1)/2, (_uiGetVCols()-strlen((char*)pszSpecCode))/2, pszSpecCode);
                //用UG_PutString确保在框内上下居中
                UG_PutString((DISPLAY_WIDTH-strlen((char*)pszSpecCode)*LcdGetFont()->char_width)/2, 
                    YSTART-1+(SIGN_H-LcdGetFont()->char_height)/2, (char*)pszSpecCode);
                
            #ifndef ST7567
                UG_DrawFrame( XSTART-1, YSTART-1, XSTART+SIGN_W, YSTART+SIGN_H, iDrawColor ); 

				lcdflush();
                #if defined(ST7789) && !defined(USE_1903S)      //彩屏版CY20P
                //取出签字区域内的点阵数据, 生成为单色格式数据
                pLcdBuf =  (unsigned short*)getLcdBufferPoint();
                for(j=YSTART; j<YSTART+SIGN_H; j++)
                {      
                    for(i=XSTART; i<XSTART+SIGN_W; i++)
                    {
                        if(pLcdBuf[j*SCR_W+i]!=C_WHITE)
                        {
                            //k=(j-YSTART)*(((SIGN_W+7)/8+3)/4*4) +(i-XSTART)/8;
                            #if 0
                            k=(j-YSTART)*((SIGN_W+7)/8) +(i-XSTART)/8;
                            sSignBuf[k]|=(0x01<<(7-(i-XSTART)%8));
                            #else
                            recordsigndata(i-XSTART, j-YSTART);
                            #endif
                            //dbg("point:(%d,%d), %04X\n", i-XSTART, j-YSTART, pLcdBuf[j*SCR_W+i]);
                        }
                    }
                    //mdelay(100);
                }
                #endif                
            #endif
            #ifdef ST7567 
                UG_DrawFrame( XSTART-1, YSTART, XSTART+SIGN_W, YSTART+SIGN_H-2, iDrawColor );
				lcdflush();
				
                //取出签字区域内的点阵数据
                pMonoLcdBuf =  (unsigned char*)getLcdBufferPoint();      
                for(j=YSTART+5; j<YSTART+SIGN_H-10; j++)
                {
                    memset(tmp, 0, sizeof(tmp));
                    for(i=XSTART+10; i<XSTART+SIGN_W-20; i++)       //特征码区域较小,+10和-20减少计算量
                    {
                        c0=pMonoLcdBuf[(j / 8) * LCD_ROW_NUM + i];      
                        if(c0&(0x01<<(j%8)))                           //(i,j)点的值
                        {
                            recordsigndata(i-XSTART, j-YSTART);
                            tmp[i-XSTART-10]='*';
                        }else
						{
						
                            tmp[i-XSTART-10]=' ';
						}
                    }
            #ifdef JTAG_DEBUG
                    dbg("%d:%s|\n", j, tmp);            
                    mdelay(2);
            #endif
                }
                dbg("%d<x<%d, %d<y<%d\n", XSTART+1, XSTART+SIGN_W-1, YSTART+1, YSTART+SIGN_H-4);
                dbg("--------end-------\n");
            #endif
                firstRun=0;
            }
            //打印POS允许退出(拉卡拉除外);预签名允许退出
#if defined(PRE_SIGN) || (defined(ENABLE_PRINTER) && !defined(APP_LKL) && !defined(APP_HKXYF))
            else if(key==_KEY_ESC)
            {
//#if defined(APP_SDJ) && !defined(ENABLE_PRINTER)
#if defined(PRE_SIGN)
                result = -2;
                break;
#else
                lcdcleanAll();
                vDispCenter(4,"取消签名,直接退出?", 0);
                lcdflush();
                if(iOK(15)==1)
                {
                    result = -2;
                    break;
                }else
                {
                    _vFlushKey();
                    firstRun=1;
                    continue;
                }
#endif                
            }
#endif
            else if(key==_KEY_ENTER)
            {
                dbg("pointcnt=%d\n", pointcnt);
                if(cDrawFlag==0 || pointcnt<12)
                {
                    lcdcleanAll();
                    //vMessage("签名无效,请重新签名");
                    vDispCenter(4,"签名无效,请重新签名", 0);
                    lcdflush();
                    iOK(5);
                    _vFlushKey();
                    firstRun=1;
                    continue;
                }
                
                LcdBitMapEnable(0);
                ucBmEnFlag=0;

#ifndef ST7567
    #if defined(ST7789) && !defined(USE_1903S)
                pLcdBuf =  (unsigned short*)getLcdBufferPoint();
                for(j=YSTART; j<YSTART+SIGN_H; j++)
                {      
                    for(i=XSTART; i<XSTART+SIGN_W; i++)
                    {
                        if(pLcdBuf[j*SCR_W+i]!=C_WHITE)
                        {
                            //k=(j-YSTART)*(((SIGN_W+7)/8+3)/4*4) +(i-XSTART)/8;
                            #if 0
                            k=(j-YSTART)*((SIGN_W+7)/8) +(i-XSTART)/8;
                            sSignBuf[k]|=(0x01<<(7-(i-XSTART)%8));
                            #else
                            recordsigndata(i-XSTART, j-YSTART);
                            #endif
                            //dbg("point:(%d,%d), %04X\n", i-XSTART, j-YSTART, pLcdBuf[j*SCR_W+i]);
                        }
                    }
                    //mdelay(100);
                }
    #else
                //取出签字区域内的点阵数据,并转换排列格式
				//pMonoLcdBuf =  (unsigned char*)getLcdBufferPoint();
                pMonoLcdBuf =  (unsigned char*)LcdGetBitMapBuffer();
                for(j=YSTART; j<YSTART+SIGN_H; j++)
                {
                    for(i=XSTART/8+1; i<(XSTART+SIGN_W)/8; i++)
                    {
                        c0=pMonoLcdBuf[j*SCR_W/8+i];
                        if(c0)
                        {
                            c1=0;
                            for(k=0; k<8; k++)
                            {
                                if(c0&(0x01<<k))
                                    c1|=0x01<<(7-k);
                            }
                            sSignBuf[(j-YSTART)*SIGN_W/8+i-XSTART/8-1]=c1;
                            c0=c1;
                        }
                        //dbg("%02X ", c0);
                    }
				}
    #endif                
#endif
#ifdef MONO_SCR     //将120*40放大为240*80
                memset(sSignBufNew, 0, iNewBufSize);
                
                x=SIGN_W/8;
                y=SIGN_H;
                
#ifdef JTAG_DEBUG
                {
                    int n;
                    for(j=0; j<y; j++)
                    {
                        memset(tmp, 0, sizeof(tmp));
                        for(i=0; i<x; i++)
                        {
                            c0 = sSignBuf[i+j*x];
                            for(n=0; n<8; n++)
                            {
                                if((c0&(0x01<<(7-n)))!=0)
                                    strcat((char*)tmp, "*");
                                else
                                    strcat((char*)tmp," ");
                            }
                        }
                        dbg("%s|\n", tmp);
                        mdelay(2);
                    }
                }
#endif                
                
                for(j=0; j<y; j++)
                {
                    //dbg("line:%d\n", j);
                    for(i=0; i<x; i++)
                    {   
                        c0 = sSignBuf[i+j*x];
                        c1=0;
                        c2=0;
                        if(c0&0x80)
                        {
                            c1|=0xC0;
                        }
                        if(c0&0x40)
                        {
                            c1|=0x30;
                        }
                        if(c0&0x20)
                        {
                            c1|=0x0C;
                        }
                        if(c0&0x10)
                        {
                            c1|=0x03;
                        }

                        if(c0&0x08)
                        {
                            c2|=0xC0;
                        }
                        if(c0&0x04)
                        {
                            c2|=0x30;
                        }
                        if(c0&0x02)
                        {
                            c2|=0x0C;
                        }
                        if(c0&0x01)
                        {
                            c2|=0x03;
                        }

                        sSignBufNew[iNewOffset+i*2+x*2*j*2]=c1;
                        sSignBufNew[iNewOffset+i*2+1+x*2*j*2]=c2;
                        //dbg("%02x->%02X%02X\n", c0, c1, c2);
                    }
                    memcpy(&sSignBufNew[iNewOffset+x*2*(j*2+1)], &sSignBufNew[iNewOffset+x*2*j*2], x*2);
                }
#else
                sSignBufNew=sSignBuf;
#endif
                //plane_size = (SIGN_W*doubleSize + 7) / 8 * SIGN_H*doubleSize;
                plane_size = (SIGN_W*doubleSize) / 8 * iNewHeight;
                
                if(jbg==NULL)
                    jbg=(unsigned char*)malloc(plane_size);		//先预分配一个较大的空间,一般简单图片实际压缩后会小于该值
                if(jbg==NULL)
                {
                    lcdcleanAll();
                    //vMessage("内存不足");
                    vDispCenter(4,"内存不足", 0);
                    lcdflush();
                    iOK(5);
                    result=-1;
                    goto SIGN_OUT;
                }
                ret=bmp2jbg(sSignBufNew, SIGN_W*doubleSize, iNewHeight, &jbg, plane_size, &iJbgLen);
                if(ret!=0 || iJbgLen<=0 || iJbgLen>=990)     //银联要求签字数据长度小于1000
                {
                    dbg("bmp2jbg fail:%d, iJbgLen=%d\n", ret, iJbgLen);
                    
                    lcdcleanAll();
                    //vMessage("签名无效,请重新签名");
                    vDispCenter(4,"签名无效,请重新签名", 0);
                    lcdflush();
                    iOK(5);
                    _vFlushKey();
                    firstRun=1;                    
                    continue;
                }
                dbgHex("jbgbuf:", jbg, iJbgLen);
                #if 0
                {
                    char out[2000], tmp[300];
                    int outlen;
                    iBase64_encode(out, sizeof(out), &outlen, jbg, iJbgLen);
                    dbg("base64:[%d],[%d]\n", strlen(out), outlen);
                    for(i=0; i<strlen(out); i+=200)
                    {
                        vMemcpy0(tmp, out+i, 200);
                        dbg("%s\n", tmp);
                        _vDelay(15);
                    }                
                    dbg("base64 end.\n");
                }
                #endif

                //保存文件
                if(pszFileName && pszFileName[0])
                {
                    ret=uiMemPutJbg((char*)pszFileName, jbg, iJbgLen);
                    dbg("save jbg end:%d.[%s][%d]\n", ret, (char*)pszFileName, iJbgLen);
                }
                ret=0;
                result=0;
                goto SIGN_OUT;
            }
            key = 0;
        }

        UG_Update();
        ret = readTPState();
        if (ret != 1 && ret != 2) 
        {
            ret=-1;
            continue;
        }

        if (ret == 1)
        {
            ret = tp_readXY(&rx, &ry);
            if (ret) continue;
            
            //dbg("tp_readXY:%d,%d\n", rx, ry);
            //20210914 修改
            #if 0
            dxabs = (rx > lastX) ? rx-lastX : lastX-rx;
            dyabs = (ry > lastY) ? ry-lastY : lastY-ry;
            if(dxabs+dyabs<2)
            {
                ret=-1;
                continue;
            }
            #endif
            
            x = rx;
            y = ry;
            tpAdjustXY(&x, &y);
            //dbg("tpAdjustXY:%d->%d,%d->%d\n", rx, x, ry, y);
            if(x<XSTART || x>XSTART+SIGN_W || y<YSTART || y>YSTART+SIGN_H)
            {
                ret=-1;
                continue;
            }
            
            lastkey=0;
            cDrawFlag=1;
            UG_TouchUpdate(x, y, TOUCH_STATE_PRESSED);
            if (lastX != x || lastY != y)
            {
                if (lastX && lastY)
                {
                    //20210914 add-start
                    if(abs(lastX - x) > 80 && abs(lastY- y) > 80)
                    {
                        dbg("abs continue....%d, %d\n", lastX - x, lastY- y);
                        continue;
                    }
                    //20210914 add-end
                    
                    //UG_DrawLine(lastX,lastY,x,y,xyislegal(x,y)?C_GREEN:C_RED);
                    UG_DrawLine(lastX, lastY, x, y, C_BLACK);
					#ifdef ST7567
                    recordsign_Line(lastX - XSTART, lastY - YSTART, x - XSTART, y - YSTART);
					#endif
                }
                else
                {
                    //UG_DrawPixel(x,y,xyislegal(x,y)?C_GREEN:C_RED);
                    UG_DrawPixel(x, y, C_BLACK);
					#ifdef ST7567
                    recordsigndata(x - XSTART, y - YSTART);
					#endif
                }
                lastX = x;
                lastY = y;
                pointcnt++;
            }
            uplift = 0;
            _vSetTimer(&ulTimer, timeout*100);
        }
        else
        {
            UG_TouchUpdate(-1, -1, TOUCH_STATE_RELEASED);
            uplift++;

            if (/*uplift>10 && */lastX && lastY)
            {
                dbg("touch release cnt = %d\n", uplift);
                lastX = lastY = 0;
            }
        }
        //mdelay(30);
    }while (_uiTestTimer(ulTimer)==0);

SIGN_OUT:
    TPClose();

    dbg("signdraw end.\n");
    
#if defined(ST7789) && !defined(USE_1903S)   
    if(lcdmode == FALSE)
        lcdsetfullflush((Boolean)FALSE); 
    lcdflush();
#endif
    if(ucBmEnFlag)
    {
        LcdBitMapEnable(0);
        ucBmEnFlag=0;
    }
    if(sSignBuf)
    {
        free(sSignBuf);
        sSignBuf=NULL;
    }
#ifdef MONO_SCR
	if(sSignBufNew)
    {
		free(sSignBufNew);
        sSignBufNew=NULL;
    }
#endif
	if(jbg)
    {
		free(jbg); 
        jbg=NULL;
    }        
    _vCls();
    return result;
}

int iPrintSignFile(uchar *pszJbgFile);
int testtp_drawloop(void *m)
{
#if 0
    iSignDraw((uchar *)"test.jbg", (uchar *)"1234ABCD");
    //打印签名
    _uiPrintOpen();
    _uiPrint("------------------------------------",1);
    iPrintSignFile("test.jbg");
    _uiPrint("------------------------------------",0);
    _uiPrint("",0);
    _uiPrint("",0);
    _uiPrint("",1);
    _uiPrintClose();
#else
    iSignDraw((uchar *)"", (uchar *)"6012ABCD");
#endif    
    return 0;
}

//ret: 0成功 <0打印机故障 >0文件错,手工签名
int iPrintSignFile(uchar *pszJbgFile)
{
#ifdef ENABLE_PRINTER    
    int i, ret;
    uint uiRet;
    uchar sJbg[1000];
    int iJbgLen=0;
    int w, h, len;
#ifdef MONO_SCR    
    //uchar sBmp[SIGNSIZE*4];
	//uchar prtbuf[48*SIGN_H*2];
    uchar sBmp[SIGN_W*2/8*97];
    uchar prtbuf[48*97];
#else
    uchar sBmp[SIGNSIZE];
    uchar prtbuf[48*SIGN_H];
#endif

    dbg("prt sign file:[%s]\n", pszJbgFile);
    uiRet=uiMemGetJbg(pszJbgFile, sJbg, sizeof(sJbg), &iJbgLen);
    if(uiRet)
    {
        dbg("uiMemGetJbg err:[%d]\n", uiRet);
        return 1;
    }

    ret=jbg2bmp(sJbg, iJbgLen, sBmp, sizeof(sBmp), &w, &h, &len);
    if(ret)
    {
        dbg("jbg2bmp len=%d err:[%d]\n", iJbgLen, ret);
        return 2;
    }
    dbg("jbg2bmp:w=%d,h=%d,len=%d\n", w, h, len);
    
#if 0//def JTAG_DEBUG
    {
        int n;
        int i,j;
        int x, y;
        char tmp[w+100];
        unsigned char c0;
        
        x=w/8;
        y=h;
        
        for(j=0; j<y; j++)
        {
            memset(tmp, 0, sizeof(tmp));
            for(i=0; i<x; i++)
            {
                c0 = sBmp[i+j*x];
                for(n=0; n<8; n++)
                {
                    if((c0&(0x01<<(7-n)))!=0)
                        strcat((char*)tmp, "*");
                    else
                        strcat((char*)tmp," ");
                }
            }
            dbg("%s|\n", tmp);
            mdelay(2);
        }
    }
#endif    
    
    w/=8;
    if(w>48)
        return 3;

    //打印图片,要求先转为固定宽度48字节
    for(i=0; i<h; i++)
    {
        memset(prtbuf+i*48, 0, 48);
        memcpy(prtbuf+i*48+(48-w)/2, sBmp+i*w, w);
    }
    
    ret=PrintImage(prtbuf, 48*h);
    
    if(ret>0)       //>0成功 <0失败
        ret=0;
    return ret;
#else
    return -1;
#endif
}

