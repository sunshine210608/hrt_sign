#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "vposface.h"
#include "pub.h"
#include "qrcodegen.h"
#include "lcd.h"
#include "ugui.h"

void drawQr(/*int xs, int ys,*/ const uint8_t qrcode[]) 
{
	int size = qrcodegen_getSize(qrcode);
    int xs=10, ys=5;
    int x,y;
    int x0=xs;
    
	dbg("size = %d\n", size);

	for (y = 0; y < size; y++, ys++)
	{
		for (x = 0, xs = x0; x < size; x++, xs++) 
		{
			if ( qrcodegen_getModule(qrcode, x, y) == true)
			{
                UG_DrawPixel(xs, ys, C_BLACK);
			}
		}
	}
}

// Creates a single QR Code, then prints it to the console.
int doPosStrDemo(void *m) {
    dbg("doPosStrDemo\n");

    const char *text = "https://star.testpnr.com/api/appmerc/signImage/show?signToken=e2eea2ae80349d4baecb8311b26021a391b9688109582646e6950052b78c758f&transType=1000&oemId=410081200000001&merId=410081610042377";                // User-supplied text
    //const char *text = "https://fyao.cloudpnr.com/h5/#/pages/index/?devsId=866508f3-8c2b-41a0-a925-880dda990faa";
    //const char *text = "20220222211514022087159629";
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level

    // Make and print the QR Code symbol
    uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];
    bool ok = qrcodegen_encodeText(text, tempBuffer, qrcode, errCorLvl,
    qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);

    _vCls();
    vDispMid(3, "显示二维码?");
    if(iOK(10))
    {
        _vDisp(3, "");
        drawQr(qrcode);
        iOK(30);
        _vCls();
    }
    return 0;
}

#ifdef ST7571
extern void vSetQrDispMode(char mode);
#else
void vSetQrDispMode(char mode)
{
}
#endif

#ifdef ST7789
#define DRAW_COLOR  C_LIGHT_STEEL_BLUE
#else
#define DRAW_COLOR  C_BLACK
#endif

/**   
  * @brief  qr_draw,LCD二维码显示
  * @param  position: 0居中 1-左靠
  * @retval None
  */
//void qr_draw(int position, int startLine, int endLine)
void qrdraw(int position, int startLine, int endLine, const uint8_t qrcode[])
{
	int size = qrcodegen_getSize(qrcode);    
	ushort p_size, p_offset;
	uint xs=0,ys=0,xe=0,ye=0;
	ushort i,j;
	uint x0, y0;
	int iLcdBorder;
	int iLCDX, iLCDY;

#if defined(ST7571) 
	//128x96的屏
	iLCDX=128;
	iLCDY=96;
	x0=0, y0=12;
	if(startLine)
		y0+=14;
    if(endLine)
        iLCDY-=12;
#endif

#if defined(ST7567) 
	//128x64的屏
    iLCDX=128;
    iLCDY=64;
    x0=0, y0=0;
    if(startLine)
        y0+=12;
    if(endLine)
        iLCDY-=12;
#endif 

#if defined(LCD_GC9106) 
	//160x128小彩屏
    iLCDX=160;
    iLCDY=128;
    x0=0, y0=STATUSBAR_H;
    if(startLine)
        y0+=20;
    if(endLine)
        iLCDY-=20;
#endif

#if defined(ST7789)
	//320x240的屏
	iLCDX=320;
	iLCDY=240;
	x0=0, y0=STATUSBAR_H;
    if(startLine)
        y0=STATUSBAR_H+LcdGetFont()->char_height+UG_FontGetVSpace()/2;
    if(endLine)
        iLCDY=STATUSBAR_H+(LcdGetFont()->char_height+UG_FontGetVSpace())*(_uiGetVLines()-1)-UG_FontGetVSpace()/2;
    else
        iLCDY-=LcdGetFont()->char_height/2;
    
    if(startLine==0 && endLine==0)
    {
        y0+=LcdGetFont()->char_height/2;
        iLCDY-=LcdGetFont()->char_height;
    }
#endif
    
    iLcdBorder=(iLCDY-y0>iLCDX-x0)?iLCDX-x0:iLCDY-y0;
    if(position && iLcdBorder>iLCDX/2)
        iLcdBorder=iLCDX/2;
    iLcdBorder=iLcdBorder/2*2;  //取偶数

	p_size   = iLcdBorder / size;				/* 此参数是放大倍数，根据 iLcdBorder 自适应后，放大到最大 */    
	if(startLine==0 && endLine==0 && p_size>=2 && (iLcdBorder-p_size*size<5))
            p_size--;
	
	p_offset = (iLCDY - y0 - p_size*size)/2;	/* 此参数是为了让二维码居中显示，所留出的y偏移量 */
    
	//自动调整x0
    if(position==0)
        x0=(iLCDX-p_size*size)/2;
    else  if(position==1)
    {
        x0=(iLCDX/2-p_size*size)/2;
    }else  if(position==2)
    {
        x0=iLCDX/2+(iLCDX/2-p_size*size)/2;
    }
    
#if 0
    vDispVarArg(2, "SymbleSize=%d", size);
    vDispVarArg(3, "x0=%d,y0=%d, iLB=%d", x0, y0, iLcdBorder);
    vDispVarArg(4, "ps=%d,po=%d", p_size, p_offset);
    _uiGetKey();
    vClearLines(2);
#endif
    
#if 1
	dbgLTxt("x0=%d,y0=%d, iLcdBorder=%d, SymbleSize=%d\n", x0, y0, iLcdBorder, size);
	dbgLTxt("ps=%d,po=%d\n", p_size, p_offset);
    
    //vDispVarArg(2, "x0=%d,y0=%d,Bd=%d", x0, y0, iLcdBorder);
    //vDispVarArg(3, "ps=%d,po=%d,ss=%d", p_size, p_offset,size);
    //_vCls();
#endif

	xs = x0;	//xs = p_offset + x0;   自动调整后无需加上p_offset
	ys = p_offset + y0;
	xe = xs + p_size;
	ye = ys + p_size;
#if defined(CY20E)
    vSetQrDispMode(1);
#endif
	for(j=0; j<size; j++)
	{
		ys = p_offset + j*p_size + y0;
		ye = ys + p_size;
		for(i=0; i<size; i++)
		{
			xs = i*p_size + x0;		//xs = p_offset + i*p_size + x0;
			xe = xs + p_size;
            if(qrcodegen_getModule(qrcode, i, j) == true)
			{
				//LCD_Fill(xs,ys,xe,ye,BLACK);			/* 此函数功能是在LCD屏幕上的任意位置画一个点或矩形 */
                if(p_size>=2)
                    UG_FillFrame(xs, ys, xe, ye, C_BLACK);	// 0x0000 C_BLACK
                else
                    UG_DrawPixel(xs, ys, C_BLACK);
			}
			else
			{
				//LCD_Fill(xs,ys,xe,ye,WHITE);			/* 如果LCD背景是白色此行可省略 */
			}
		}
	}
#if defined(CY20E)
    vSetQrDispMode(0);
#endif    
	//lcdflush();
}

int iDispTransQrCode(char *pszPrompt, char *pszQrCodeStr)
{
    int startLine=0, endLine=0;
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level

    uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];

    bool ok = qrcodegen_encodeText(pszQrCodeStr, tempBuffer, qrcode, errCorLvl,
                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);
    
    _vCls();
	if(ok)
	{
        
		if(pszPrompt && pszPrompt[0])
        {
            startLine=1;
			vDispCenter(1, pszPrompt, 0);
        }
#ifdef ST7789
        if(strlen(pszPrompt)<30)
            endLine=1;
#endif        
		qrdraw(0, startLine, endLine, qrcode);
		return 0;
	}else
        vMessage("二维码生成失败");
	return 1;
}

int iDispTransQrCodeEx(char *pszPrompt, char *pszEndPrompt, char *pszQrCodeStr)
{
    int startLine=0, endLine=0;
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level

    uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];

    bool ok = qrcodegen_encodeText(pszQrCodeStr, tempBuffer, qrcode, errCorLvl,
                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);
    
    _vCls();
	if(ok)
	{
		if(pszPrompt && pszPrompt[0])
        {
            startLine=1;
			#ifdef ST7789
            vDispCenter(1, pszPrompt, 0);
            #else
            UG_PutString((DISPLAY_WIDTH-strlen(pszPrompt)*LcdGetFont()->char_width)/2, 0, pszPrompt);
            #endif
        }
        if(pszEndPrompt && pszEndPrompt[0])
        {
            endLine=1;
			vDispCenter(_uiGetVLines(), pszEndPrompt, 0);
        }
		qrdraw(0, startLine, endLine, qrcode);
		return 0;
	}else
        vMessage("二维码生成失败");
	return 1;
}

int iDispTransQrCodeLeft(char *pszPrompt, char *pszEndPrompt, char *pszQrCodeStr)
{
    int len=0;
    uchar buf[40];
    uchar *p;
    int line=0;

    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level

    uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];

    bool ok = qrcodegen_encodeText(pszQrCodeStr, tempBuffer, qrcode, errCorLvl,
                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);
    
    _vCls();
	if(ok)    
	{
		qrdraw(1, 0, 0, qrcode);
        if(pszPrompt && pszPrompt[0])
        {
            //UG_PutString((DISPLAY_WIDTH-strlen(pszPrompt)*LcdGetFont()->char_width)/2, 0, pszPrompt);
            p=(uchar*)pszPrompt;
            #ifdef ST7789
            //line=1;
            #endif
            while(p<(uchar*)pszPrompt+strlen(pszPrompt))
            {
                len=iSplitGBStr(p, (_uiGetVCols()-1)/2);
                vMemcpy0(buf, p, len);
                //_vDispAt(line++, (_uiGetVCols()+1)/2, buf);
                UG_PutString((DISPLAY_WIDTH+1)/2, STATUSBAR_H+LcdGetFont()->char_height/2+line*(LcdGetFont()->char_height+1), buf);
                p+=len;
                line++;
            };
        }
		return 0;
	}/* else
            vMessage("二维码生成失败");
    */
    
	return 1;
}

int iDispTransQrCodeLeftSingle(int startline, int endline, char *pszQrCodeStr)
{
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level

    uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];

    bool ok = qrcodegen_encodeText(pszQrCodeStr, tempBuffer, qrcode, errCorLvl,
                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);
    
	if(ok)
	{
		qrdraw(1, startline, endline, qrcode);
		return 0;
	}
	return 1;
}

int DispTransQrCode(char *pszQrCodeStr)
{
    int startLine=13, endLine=0;
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level

    uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];

    bool ok = qrcodegen_encodeText(pszQrCodeStr, tempBuffer, qrcode, errCorLvl,
                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);
	
    if(ok)
	{ 
		qrdraw(1, startLine, endLine, qrcode);
		return 0;
	}else
        vMessage("二维码生成失败");
	return 1;
}
    
/**   
  * @brief  qr_draw,LCD二维码显示
  * @param  None
  * @retval None
  */
void updateparam_qr_draw(const uint8_t qrcode[])
{
	int size = qrcodegen_getSize(qrcode);    
	ushort p_size, p_offset;
	uint xs=0,ys=0,xe=0,ye=0;
	ushort i,j;
	uint x0, y0;
	int iLcdBorder;
	int iLCDX, iLCDY;

#if defined(ST7567) 
	//128x64的屏
    iLCDX=128;
    iLCDY=64;
    x0=0, y0=0;
    iLCDY-=24;
#endif 

#if defined(LCD_GC9106) 
	//160x128小彩屏
    iLCDX=160;
    iLCDY=128;
    x0=0, y0=STATUSBAR_H;   //彩屏状态栏高度
     iLCDY-=60; //一行20
#endif

#if defined(ST7789)
	//320x240的屏
	iLCDX=320;
	iLCDY=240;
	x0=38, y0=20+2;       //状态栏实际高度20
    if(lineflag)
        y0=(27+24+2);   //字体高度24,行间距3。(第一行从27高开始显示)
#endif
	
    dbg("size1:%d\n",size);

    iLcdBorder=(iLCDY-y0>iLCDX-x0)?iLCDX-x0:iLCDY-y0;
    iLcdBorder=iLcdBorder/2*2;  //取偶数
    
	p_size   = iLcdBorder / size;				/* 此参数是放大倍数，根据 iLcdBorder 自适应后，放大到最大 */    
	p_offset = (iLCDY - y0 - p_size*size+1)/2;	/* 此参数是为了让二维码居中显示，所留出的偏移量 */
          
	//自动调整x0
	x0=(iLCDX-p_size*size)/2;

	   dbg("iLcdBorder1:%d\n",iLcdBorder);
	   dbg("p_size1:%d\n",p_size);
	   dbg("p_offset:%d\n",p_offset);
#if 1
{
	p_offset = 0;
 //       x0 = 40;
 #ifdef LCD_GC9106
 	x0 = (160-p_size*size)/2;
 #else
        x0 = (128-p_size*size)/2;
 #endif
 	y0+=0;		
}
	   dbg("p_offset1:%d\n",p_offset);	
	   dbg("x01:%d\n",x0);
	   dbg("y01:%d\n",y0);
#endif
#if 0
    vDispVarArg(2, "SymbleSize=%d", m_nSymbleSize);
    vDispVarArg(3, "x0=%d,y0=%d, iLB=%d", x0, y0, iLcdBorder);
    vDispVarArg(4, "ps=%d,po=%d", p_size, p_offset);
    _uiGetKey();
    vClearLines(2);
#endif
    
#if 0
	dbgLTxt("x0=%d,y0=%d, iLcdBorder=%d, SymbleSize=%d", x0, y0, iLcdBorder, m_nSymbleSize);
	dbgLTxt("ps=%d,po=%d", p_size, p_offset);
    
    //vDispVarArg(2, "x0=%d,y0=%d,Bd=%d", x0, y0, iLcdBorder);
    //vDispVarArg(3, "ps=%d,po=%d,ss=%d", p_size, p_offset,m_nSymbleSize);
    //_vCls();
#endif

	xs = x0;	//xs = p_offset + x0;   自动调整后无需加上p_offset
	ys = p_offset + y0;
	xe = xs + p_size;
	ye = ys + p_size;
#if PROJECT_CY20E==1	
    vSetQrDispMode(1);
#endif
    
	for(j=0;j<size;j++)
	{
		ys = p_offset + j*p_size + y0;
		ye = ys + p_size;
		for(i=0;i<size;i++)
		{
			xs = i*p_size + x0;		//xs = p_offset + i*p_size + x0;
			xe = xs + p_size;
	            if(qrcodegen_getModule(qrcode, i, j) == true)
			{
				//LCD_Fill(xs,ys,xe,ye,BLACK);			/* 此函数功能是在LCD屏幕上的任意位置画一个点或矩形 */
		                if(p_size>=2)
		                    UG_FillFrame(xs, ys, xe, ye, C_BLACK);	// 0x0000 C_BLACK
		                else
		                    UG_DrawPixel(xs, ys, C_BLACK);
			}
			else
			{
				//LCD_Fill(xs,ys,xe,ye,WHITE);			/* 如果LCD背景是白色此行可省略 */
			}
		}
	}
#if PROJECT_CY20E==1    
    vSetQrDispMode(0);
#endif    
	//lcdflush();
}

int DispUpdateParamQrCode(char *pszQrCodeStr)
{
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level

    uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];

    bool ok = qrcodegen_encodeText(pszQrCodeStr, tempBuffer, qrcode, errCorLvl,
                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);
	
    if(ok)
	{ 
		updateparam_qr_draw(qrcode);
		return 0;
	}else
        vMessage("二维码生成失败");
	return 1;
}
int iTestDispQrcode(void *m)
{
	//char str[]="http://www.lakala.com/abc/def/12345";
    char str[]="https://api-test.jiajiepay.com/sdj-merchant-api/s/31012021042914011045110716398/ef17603cc23d7505/";
    //char str[]="https://api-test.jiajiepay.com/sdj-merchant-api/s/31012021042914011045110716398/ef17603cc23d7505/111ab9kAM";
	char buf[40];
	ulong ulAmount=10225;

    _vCls();
    _vDisp(1, "disp qrcode");
    _uiGetKey();
    _vDisp(1, "");
    
	sprintf(buf, "金额:%lu.%02lu,请扫码%d", ulAmount/100, ulAmount%100, strlen(str));
    _vSetBackLight(1);
	iDispTransQrCode(buf, str);
    _uiGetKey();
    iDispTransQrCode(NULL, str);
    _uiGetKey();
    _vSetBackLight(0);
    
    return 0;
}

extern int PrintImage(unsigned char*data,int dalen);
extern int PrintPutImage(unsigned char *img,int imgsize);
extern int PrintGetDepth(int *depth);
int PrintSetDepth(int depth);
//ret: 0成功 <0错
int iPrintQrCode(const uint8_t qrcode[])
{
#ifdef ENABLE_PRINTER    
    int ret;
    int w, h;
    int n, N;
    int i, j;
    int x0;
    uchar c0;
    int iPrtDepth;
    int size = qrcodegen_getSize(qrcode);
    uchar prtbuf[48*size*8+10];    //最大8倍
    
    memset(prtbuf, 0, sizeof(prtbuf));
    
    w=(size+7)/8;
    h=size;
    
    N=4;
    if(w>8)
        N=2;
    if(w>16)
        N=1;
    if(w==6)
        N=5;
    if(w<=5)
        N=6;    
    if(w<=3)
        N=8;

    x0=(48-w*N)/2;
    if(x0<0)
        return -2;
    
    //prtbuf=malloc(48*size*8+10);     //改用malloc后打印不正常,原因暂未知
    //if(prtbuf==NULL)
    //    return -3;
    
    //将点阵数据转为宽度48的bmp图片数据
    for(j=0;j<size;j++)
	{
		for(i=0;i<size;i++)
		{
            if(qrcodegen_getModule(qrcode, i, j) == true)
            {
                //prtbuf[j*48+x0+i/8]|=(0x01<<(7-i%8));     //不扩大,压缩为bit
                //一个点变N个点
                if(N==4)
                {
                    //扩大为4倍                
                    if(i%2)
                    {
                        c0=0x0F;
                    }else
                    {
                        c0=0xF0;
                    }
                    prtbuf[j*4*48+x0+i/2]|=c0;
                }else if(N==8)
                {
                    //扩大8倍
                    prtbuf[j*N*48+x0+i]=0xFF;
                }else /* if(N==6) */
                {
                    //扩大N倍(N<=8)
                    int m=(i*N)/8;
                    int b=(i*N)%8;
                    ushort t=0;
                    
                    int k;
                    for(k=0; k<N; k++)
                    {
                        t|=(0x01<<(15-k-b));                            
                    }
                    prtbuf[j*N*48+x0+m]|=(uchar)(t>>8);
                    prtbuf[j*N*48+x0+m+1]|=(uchar)t;
                }
            }
            //放大N倍，后N-1行与第一行完全一样         
            for(n=1; n<N; n++)
                memcpy(&prtbuf[(j*N+n)*48], &prtbuf[(j*N+0)*48], 48);

        }
    }

    PrintGetDepth(&iPrtDepth);
    PrintSetDepth(1);
    ret=PrintImage(prtbuf, 48*h*N);
    //ret=PrintPutImage(prtbuf, 48*h*N);
    //free(prtbuf);
    PrintSetDepth(iPrtDepth);
    if(ret>0)       //>0成功 <0失败
        ret=0;
    return ret;
#else
    return -1;
#endif
}

int iPrintQrCodeStr(unsigned char *pszQrCodeStr)
{
    int ret=1;
    
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level

    uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];

    bool ok = qrcodegen_encodeText(pszQrCodeStr, tempBuffer, qrcode, errCorLvl,
                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);
    
    if(ok)
    {
        ret=iPrintQrCode(qrcode);
    }
    return ret;
}

int iTestPrintQrCode(void *p)
{
#ifdef ENABLE_PRINTER
    char *pszQrCodeStr;
    char code[200]="0;0;00007302199999000020;3139303353513838540000000025990E;89860619120030072854;862419052648309;V210712A&1.1.8g(L);1;1;ML160_1.5.10.1906_21033009_R;4015;";
    char buf[200], tmp[100];
    int  arrlen[]={15,30,60,90,120,150};
    int i;
        
    if(strlen(code)<150)
        vMemcpy0(code+strlen(code), code, 150-strlen(code));
    
    enum qrcodegen_Ecc errCorLvl = qrcodegen_Ecc_LOW;  // Error correction level

    uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
    uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];
    
    _uiPrintOpen();
    pszQrCodeStr=buf;
    for(i=0; i<sizeof(arrlen)/sizeof(arrlen[0]); i++)
    {
        vMemcpy0(buf, code, arrlen[i]);

        qrcodegen_encodeText(buf, tempBuffer, qrcode, errCorLvl,
                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX, qrcodegen_Mask_AUTO, true);
        
        _vCls();
        qrdraw(0, 0, 0, qrcode);
        
        _uiPrintEx(2, "打印测试", 1);
        iPrintQrCode(qrcode);
        _uiPrint("",0);
        sprintf(tmp, "code len:%d,size=%d", strlen(pszQrCodeStr), qrcode[0]);
        _uiPrintEx(2, tmp, 0);
        _uiPrint("",1);
        //_vDelay(10);
        if(iOK(5)==0)
            break;

    }
    _uiPrint("",0);
    _uiPrint("",0);
    _uiPrint("",1);
    _uiPrintClose();
#endif    
	return 0;
}
