#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "VposFace.h"
#include "Pub.h"
//#include "AppGlobal.h"
#include "tcpcomm.h"
#include "debug.h"
#include "sys_littlefs.h"
#include "myBase64.h"
#include "MemMana_cjt.h"
#include "TMS.h"
#include "mbedssl_cli.h"
#include "preconn.h"
#include "myhttp.h"

#define PRT_HTTP_LOG

#if defined(APP_CS_ABC) || defined(APP_CS_ARCU)
#define SUPPORT_HTTP_CHUNK
#endif

int iParseHttpUrl(char *pszUrlAddr, char *pszHostName, char *pszSubUrl, int *piPort);

//为便于显示错误信息的同时进行语音播报,httpssendrecv中的vMessage改为保存错误信息供外部读取
static char sg_szHttpErrMsg[40+1]={0};
void vGetHttpErrMsg(char *pszErrMsg)
{
    if(pszErrMsg)
        strcpy(pszErrMsg, sg_szHttpErrMsg);
    return;
}

uchar sg_cNoDisp=0;
//static uchar sg_cBKSvrFlag=0;          //银行卡服务器:0-使用正常服务器 1-使用备用服务器
void vSetCommNoDisp(uchar disp)
{
    sg_cNoDisp=disp;
}
uchar ucGetCommNoDisp(void)
{
    return sg_cNoDisp;
}

#ifndef APP_LKL
void vPrtLklDebugLog(char cPrtHeadTail, char cTxtType, char *title, char *psLog, int len)
{}
#endif

void vReplaceStr(char *pszString, const char *pszSrc, const char *pszDest)
{
	char *p0, *p1;
	int iLen, iRemainLen;

	if (NULL == pszString || NULL == pszSrc || NULL == pszDest)
	{
		return;
	}
	p0 = pszString;
	while (1)
	{
		p1 = strstr(p0, pszSrc);
		if (NULL != p1)
		{
			if (strlen(pszSrc) == strlen(pszDest))
			{
				memcpy(p1, pszDest, strlen(pszDest));
			}
			else
			{
				iLen = strlen(p0) - strlen(pszSrc) + strlen(pszDest);
				iRemainLen = strlen(p1 + strlen(pszSrc));
				memmove(p1 + strlen(pszDest), p1 + strlen(pszSrc), iRemainLen);
				memcpy(p1, pszDest, strlen(pszDest));
				p0[iLen] = 0;
			}
			p0 = p1 + strlen(pszDest);
			continue;
		}
		else
		{
			break;
		}
	}
}

uchar sg_ucLinkStaus=0;	//连接状态: 0-短链接 1-长链接 2-长链接且链接已通
int iSetConnStatus(char cLinkFlag, char *pszUrl)
{
	uchar szHostName[256 + 1], szSubUrl[256 + 1];
	uint uiPort=0;
	char szHostIp[20], szHostPort[10];
	int iRet;

	if(sg_ucLinkStaus==2 && cLinkFlag==0)	//长链接改短链接,若已链接则先挂断
		iCommTcpDisConn();

	if(cLinkFlag==0 || cLinkFlag==1)
		sg_ucLinkStaus=cLinkFlag;
	
	if(cLinkFlag && pszUrl)
	{
		iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
		if (iRet)
			return -1*COM_PARAMETER;
        if(!sg_cNoDisp)
            vDispMid(3, "解析域名...");
		if( iGetHostByName(szHostName, szHostIp) )
		{
			vMessage("解析域名失败");
			return -1*COM_PARAMETER;
		}
		sprintf(szHostPort, "%d", uiPort);
        if(!sg_cNoDisp)
            vDispMid(3, "连接服务器...");
		iRet=iCommTcpConn(szHostIp, szHostPort, 60*1000);
		if(iRet)
		{
            if(!sg_cNoDisp)
                _vDisp(3, "");
			return -1*COM_ERR_NO_CONNECT;
		}
		sg_ucLinkStaus=2;
	}
	return 0;
}

static uchar sg_bSSLMode=0;
void vSetSSLFlag(uchar sslmode)
{
    sg_bSSLMode=sslmode;
}
void vAutoSSLFlag(char *url)
{
#ifdef APP_SUPPORT_HTTPS
    char *p0;
    
    //url非http开头,则不改变ssl标志
    if(memcmp(url, "http", 4) && memcmp(url, "HTTP", 4))
        return;
    
    p0 = strstr(url, "https://");
    if(p0==NULL)
        p0 = strstr(url, "HTTPS://");
    if(p0)
        sg_bSSLMode=1;
    else
        sg_bSSLMode=0;
#else
    sg_bSSLMode=0;
#endif    
}

int iParseHttpUrl(char *pszUrlAddr, char *pszHostName, char *pszSubUrl, int *piPort)
{
    char url[256]={0};
	char *p0, *p1;

    if(pszUrlAddr==NULL || pszHostName==NULL)
        return -1;
    strncpy(url, pszUrlAddr, sizeof(url)-1);
    
    //默认值
    pszHostName[0]=0;
    if(pszSubUrl)
        pszSubUrl[0] = 0;
    if(piPort)
    {
        vAutoSSLFlag(url);
        if(sg_bSSLMode)
            *piPort = 443;
        else
            *piPort = 80;
	}
    
    vReplaceStr(url, "\\", "/");

	p0 = strstr(url, "://"); //搜索"http://"
	if (NULL == p0)
		p0 = url;
	else
		p0 += 3;
    
	p1 = strstr(p0, "/");
	if (NULL == p1)
	{
		strcpy(pszHostName, p0);
	}
	else
	{
		vMemcpy0((uchar *)pszHostName, (uchar *)p0, p1 - p0);
        if(pszSubUrl)
            strcpy(pszSubUrl, p1);
	}
    if(piPort)
    {
        p1 = strstr(pszHostName, ":");
        if (p1)
        {
            *p1 = 0;
            *piPort = atoi(p1 + 1);
        }
    }

    //dbg("HttpAddr:%s|%s|%s|%d\n", pszUrlAddr, pszHostName, pszSubUrl, *piPort);
    dbg("url:[%s] -> host:[%s],port:[%u],suburl:[%s]\n", url, pszHostName,
            piPort!=NULL?(*piPort):0, pszSubUrl!=NULL?pszSubUrl:"");
        
	return 0;
}

void vGetHttpReqHead(uchar ucReqHeadType, uchar *pszHostName, uint uiPort, uchar *pszSubUrl, uchar *pszBody, int iLen, 
        char *pszHttpHead)
{
	char szUrlPort[10];
    
	if (uiPort == 80 || uiPort == 0 || uiPort==443)
		szUrlPort[0] = 0;
	else
		sprintf(szUrlPort, ":%u", uiPort);
	
    if(ucReqHeadType & HTTP_GET)
    {
        if(pszBody && pszBody[0])
            sprintf(pszHttpHead, "GET %s?%s HTTP/1.1\r\n", pszSubUrl, pszBody);
        else
            sprintf(pszHttpHead, "GET %s HTTP/1.1\r\n", pszSubUrl);
        
        sprintf(pszHttpHead+strlen(pszHttpHead), "Host: %s%s\r\n",
                pszHostName, szUrlPort);
    }else
    {
        sprintf(pszHttpHead, "POST %s HTTP/1.1\r\n"
                        "Host: %s%s\r\n"
                        //"Accept-Encoding: identity\r\n"
                        "Connection: keep-alive\r\n"
						"Content-Length: %d\r\n",
                    pszSubUrl, pszHostName, szUrlPort, iLen);
    }
    
    switch(ucReqHeadType&0x7F)
    {
        case HTTP_TYPE_FORMURL:
            strcat(pszHttpHead, "Content-Type: application/x-www-form-urlencoded\r\n");
            break;
        case HTTP_TYPE_JSON:
            strcat(pszHttpHead, "Content-Type: application/json;charset=utf-8\r\n");
            break;
        case HTTP_TYPE_XML:
            strcat(pszHttpHead, "Content-Type: application/xml;charset=utf-8\r\n");
            break;
        case HTTP_TYPE_TEXT:
            strcat(pszHttpHead, "Content-Type: text/plain\r\n");
            break;
        case HTTP_TYPE_HTML:
            strcat(pszHttpHead, "Content-Type: text/html\r\n");
            break;        
        case HTTP_TYPE_OTHER1:
            #ifdef APP_LKL
            strcat(pszHttpHead, "X-V: 2.0\r\n");     //application/octet-stream
            #endif
            break;
        default:
            break;
    }
    strcat(pszHttpHead, "\r\n");     //head与body分隔行
    
    return;
}

//type:0-银行卡http  1-扫码http 2-查询费用
//Host: 服务器地址和端口,格式: xxx.xxx.xxx.xxx:nnnn	当端口为80时可省略端口部分
int iHttpSend(uchar ucReqHeadType, uchar *pszHostName, uint uiPort, uchar *pszSubUrl, uchar *pszBody, int iLen)
{
	//char szHttpHead[300];
    char *pszHttpHead;
    int  iHeadSize;
    int  iRet;
	char szTmp[50];    
    
    if(ucReqHeadType&HTTP_GET)
        iHeadSize=2048;     //get方式的body放在head中,需要较大内存
    else
        iHeadSize=500;     
    
    pszHttpHead=malloc(iHeadSize);    
    if(pszHttpHead==NULL)
        return -1;
    
    /*
    if(ucReqHeadType==1)     //拉卡拉扫码应用
    {
        //application/octet-stream
        strcat(szHttpHead, "X-V: 2.0\r\n");
    }else if(cReqHeadType==2)     //json格式(拉卡拉查询费用,其它应用)
    {
        //application/octet-stream
        strcat(szHttpHead, "Content-Type: application/json;charset=utf-8\r\n");
    }else if(cReqHeadType==0)   //拉卡拉银行卡应用
    {
        strcat(szHttpHead, "Content-Type: application/x-www-form-urlencoded\r\n");
    }
    */
    if(iLen<0 && pszBody)
        iLen=strlen((char *)pszBody);
    vGetHttpReqHead(ucReqHeadType, pszHostName, uiPort, pszSubUrl, pszBody, iLen, pszHttpHead);
    
    if(strlen(pszHttpHead)>=iHeadSize)
    {
        vMessage("报文头超长");
        return -2;
    }
    
#ifdef PRT_HTTP_LOG
	dbgLTxt("\n*** Http Req Head(%d):[%s]\n", strlen(pszHttpHead), pszHttpHead);
    
    if( (ucReqHeadType & HTTP_GET)==0 ) //不为HTTP_GET
    {
        sprintf(szTmp, "*** Http Req Body(%d):", iLen);
        #ifdef LKL_APP
        if((ucReqHeadType&0x7F)==HTTP_TYPE_OTHER1)
        {
            dbgHex("body sign", pszBody, 16);
            dbgLTxt2(szTmp, pszBody+16);
        }else   
        #endif
            dbgLTxt2(szTmp, pszBody);
    }
#endif

	iRet = iCommTcpSend((uchar *)pszHttpHead, strlen(pszHttpHead));
	if (iRet == strlen(pszHttpHead))
	{
        if(pszHttpHead)
        {
            free(pszHttpHead);
            pszHttpHead=NULL;
        }        
        
        if(ucReqHeadType & HTTP_GET)
            return 0;
        
		iRet = iCommTcpSend(pszBody, iLen);
		if (iRet == iLen)
			return 0;
	}
    if(pszHttpHead)
    {
        free(pszHttpHead);
        pszHttpHead=NULL;
    }
    
	return -1;
}

extern void vSetQuickRcv(int flag);

//接收 Transfer-Encoding: chunked 格式的body
int iRecvChunkedBody(uchar *psBodyTmp, uint uiBodySize, uchar *psBody, uint *puiRcvLen)
{
    uint uiLen;
    uint blklen;
    char *p0, *p;
    int iRet;
    uchar tmp[10]={0};
    
    dbg("chunk0(%d)=[%s]\n", strlen((char*)psBodyTmp), psBodyTmp);
    
    //当前只需要接收一块
    uiLen=strlen((char*)psBodyTmp);
    if(uiLen>sizeof(tmp)-1)
    {
        p0=(char*)psBodyTmp;
        p=strstr(p0, "\r\n");
        if(p==NULL)
            return -1;
    }else
    {
        dbg("chunk1\n");
        strcpy((char*)tmp, (char*)psBodyTmp);
        p0=(char*)tmp;
        p=strstr(p0, "\r\n");
        if(p==NULL)
        {
            dbg("chunk2\n");
            uiLen=strlen(p0);
            vSetQuickRcv(1);
            iRet = iCommTcpRecv(tmp+uiLen, sizeof(tmp)-1-uiLen, 300);
            vSetQuickRcv(0);
            if(iRet<=0)
            {
                vSetQuickRcv(0);            
                return -1;
            }            
            uiLen+=iRet;
            tmp[uiLen]=0;
            
            dbg("chunk3 ret:%d, new len:%d\n", iRet, uiLen);
            
            p=strstr(p0, "\r\n");
            if(p==NULL)
                return -1;
        }
    }
    blklen=ulHexToLong((uchar*)p0, p-p0);
    p+=2;       //跳过\r\n, 指向blk
    
    if(blklen>=uiBodySize)
    {
        return -2;
    }
    
    uiLen=strlen(p);
    vMemcpy0(psBody, p, uiLen);
    dbg("chunk4 blklen=%d, rcvlen=%d:%d\n", blklen, uiLen);
    dbg("psbody=[%s]\n", psBody);
    if(uiLen<blklen)
    {
        //还需要继续接收
        vSetQuickRcv(1);       
        iRet = iCommTcpRecv(psBody+uiLen, blklen-uiLen+2, 1000);      //+2+3是为收取末尾的回车换行
        vSetQuickRcv(0);
        if(iRet<=0)
        {
            return -3;
        }
        uiLen+=iRet;
        psBody[uiLen]=0;
        
        dbg("chunk5 ret:%d, new len:%d\n", iRet, uiLen);
        dbg("recv(%d):[%s], len=%d, blklen=%d\n", uiLen, psBody, strlen((char*)psBody), blklen);
    }
    *puiRcvLen=blklen;
    
    return 0;
}

#define RECV_HTTPHEAD_LEN       100
#define RECV_HTTPHEAD_BIGLEN    800
#define RECV_HTTPHEAD_ADDLEN    40
int iHttpRecv(char bigData, uchar *pszBody, uint uiBodySize, uint *puiRcvLen, uint uiTimeOut, char *pszContentRange)
{
	char szBuff[RECV_HTTPHEAD_BIGLEN*2+1], szTmp[50];
	char *pszBuf, *pBody, *pTmp;
	uint uiRecvLen, uiBodyLen = 0;
	int iRet, iExpRcvLen;
    uchar ucHttpRspNotOK=0;

	if (uiBodySize < 30)
	{
		dbg("uiBodySize=[%d], too small !!!\n", uiBodySize);
		return 1;
	}

	//收取HttpHead
	memset(szBuff, 0, sizeof(szBuff));
	uiRecvLen = 0;
	//iExpRcvLen = sizeof(szBuff) - 1;
	if(bigData)
        iExpRcvLen=RECV_HTTPHEAD_BIGLEN;
    else
    	iExpRcvLen=RECV_HTTPHEAD_LEN; 
    
	pszBuf = szBuff;
	pBody = NULL;
	while (iExpRcvLen > 0)
	{
		iRet = iCommTcpRecv((uchar *)pszBuf, iExpRcvLen, uiTimeOut * 1000);
		if (iRet <= 0)
		{
			dbg("RecvData iRet=[%d], uiRecvLen=%d\n", iRet, uiRecvLen);
            //dbgHex("recv", (uchar*)szBuff, uiRecvLen);
			if (iRet == 0)
				iRet = -9;
			return iRet;
		}
		pszBuf[iRet] = 0;
		uiTimeOut = 5;

		uiRecvLen += iRet;

		//搜索http head与body空行(分隔符"\r\n\r\n")
		if ((pBody = strstr(szBuff, "\r\n\r\n")) == NULL)
		{
			pszBuf = szBuff + uiRecvLen;
			iExpRcvLen = sizeof(szBuff) - 1 - uiRecvLen;
			if(bigData==0 && iExpRcvLen>RECV_HTTPHEAD_ADDLEN)
				iExpRcvLen=RECV_HTTPHEAD_ADDLEN;
			continue;
		}

#ifdef PRT_HTTP_LOG
		sprintf(szTmp, "\n*** Http Rsp Head(%d)=[%%.%ds]\n", pBody - szBuff, pBody - szBuff);
        if(bigData==0)
            dbgLTxt(szTmp, szBuff);
        else
            dbg("*** Http Rsp Head(%d)\n", pBody - szBuff);
#endif
#if 1      
		if (strstr(szBuff, " 200 OK") == NULL && strstr(szBuff, " 206 Partial Content") == NULL)
		{
            //dbgLTxt(szTmp, szBuff);
            dbgHex("head", szBuff, 20);
            
            //部分服务器只返回数字,则只搜索200或206,并且在第一行中
            char *p0;
            p0=strstr(szBuff, "HTTP/");     //"HTTP/1.1"或"HTTP/1.0"或"HTTP/2.0", 后跟" 200"或" 206"为成功
            if(p0==NULL || (memcmp(p0+9, "200", 3)!=0 && memcmp(p0+9, "206", 3)!=0))
            {
                //http返回不成功
                dbg("http head NOT OK!!!\n");
                //vMessage("http响应报文失败");
                //return -2;
                ucHttpRspNotOK=1;
            }
		}
#endif        
		pBody[2] = 0;

		pBody += 4;
		break;
	}

	if (pBody == NULL)
	{
		dbg("RecvData Head=[%s], uiRecvLen=%d, no body err!\n", szBuff, uiRecvLen);
		return 1;
	}    
    
	//搜索Content-Length
	if ((pszBuf = strstr(szBuff, "Content-Length:")) == NULL)
	{
#ifdef SUPPORT_HTTP_CHUNK
        //创识农行http可能没有Content-Length返回,判断是否chunked格式
        if (strstr(szBuff, "Transfer-Encoding: chunked"))
        {
            iRet=iRecvChunkedBody((uchar*)pBody, uiBodySize, pszBody, &uiBodyLen);
            if(iRet)
                return iRet;
            
            goto HTTP_RECV_BODY_END;
        }
		dbg("Content-Length err, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
        if(ucHttpRspNotOK)
            return -2;
		return 2;
#endif        
	}

	pszBuf = pszBuf + strlen("Content-Length:");

	if ((pTmp = strstr(pszBuf, "\r\n")) == NULL)
	{
		dbg("Content-Length err2, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 3;
	}
	if (pTmp - pszBuf > 10)
	{
		dbg("Content-Length err3, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 3;
	}
	vMemcpy0((uchar *)szTmp, (uchar *)pszBuf, pTmp - pszBuf);

	//跳过空格,找到数字起始位置
	pszBuf = szTmp;
	//atoi会自动跳过前部空格
	/*
	while (*pszBuf)
	{
		if (*pszBuf >= '0' && *pszBuf <= '9')
			break;
		pszBuf++;
	}
*/
	uiBodyLen = atoi(pszBuf);
	if (uiBodyLen + 1 > uiBodySize)
	{
		dbg("Content-Length err4, uiBodySize=[%d], BodyLen=[%d]\n", uiBodySize, uiBodyLen);
		return 1;
	}
	
	if (pszContentRange!=NULL && (pszBuf = strstr(szBuff, "Content-Range: ")) != NULL)
	{
		pszContentRange[0]=0;
		pszBuf+=strlen("Content-Range: ");
        if(memcmp(pszBuf, "bytes ", 6)==0)
            pszBuf+=6;
		if ((pTmp = strstr(pszBuf, "\r\n")) != NULL)
		{
			vMemcpy0(pszContentRange, pszBuf, pTmp-pszBuf);
		}
	}
    
	uiRecvLen = uiRecvLen - (pBody - szBuff);
	memcpy(pszBody, pBody, uiRecvLen);
	
    if(bigData)
        dbg("bodylen=%d, rcv %d...\n", uiBodyLen, uiBodyLen - uiRecvLen);
	pszBuf = (char *)pszBody + uiRecvLen;
	while (uiBodyLen > uiRecvLen)
	{
		iRet = iCommTcpRecv((uchar *)pszBuf, uiBodyLen - uiRecvLen, uiTimeOut * 1000);
		if (iRet <= 0)
		{
			dbg("body RecvData uiRet=[%d], uiRecvLen=%d\n", iRet, uiRecvLen);
			return 1;
		}

		pszBuf += iRet;
		uiRecvLen += iRet;
	}
HTTP_RECV_BODY_END:    
	pszBody[uiBodyLen] = 0;
	*puiRcvLen=uiBodyLen;
	    
#ifdef PRT_HTTP_LOG
	//dbgLTxt("\n*** Http Rsp Body(%d):[%s]\n\n", uiBodyLen, pszBody);
	sprintf(szTmp, "*** Http Rsp Body(%d):", uiBodyLen);
	if(bigData==0)
		dbgLTxt2(szTmp, pszBody);
	else
		dbg("%s\n", szTmp);
#endif

#if 0    
    if(ucHttpRspNotOK)
        return -2;
#endif    
	return 0;
}

extern void vGetLklReqHost(char *pszHost);
// ==0:成功 <0:参数失败或者发送失败 >0:接收失败
int iHttpSendRecv(unsigned char ucType, char *pszUrl, char *pszBakHost, int retry, char *pszBody, int iBodyLen, char *pszRsp, uint uiRspSize, uint uiTimeOut)
{
	uchar szHostName[256 + 1], szSubUrl[256 + 1];
	uint uiPort=0, uiLen;
	char szHostIp[20], szHostPort[10];
	int iRet;
	char buf[50];
    int connNum;
//    char *pszBakDomain;
    int iUseBakFlag=0;
    
    if(retry<=0)
        retry=1;

    vSetSSLFlag(0);
    vPrtLklDebugLog(0, 2, "url", pszUrl, 0);
	iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
	if (iRet)
	{
		vPrtLklDebugLog(0, 1, "", "get domain url err.\n", 0);
        vMessage("url格式错");
		return -1*COM_PARAMETER;
	}

    if(sg_ucLinkStaus!=2)
    {
#ifdef SUPPORT_PRECONN        
        if(iGetPreConnRunFlag())   //开了预连接
        {
            if(!sg_cNoDisp)
                vDispMid(3, "连接服务器...");
            iRet=iWaitPreConnResult();
            if(iRet)
                return -1*COM_ERR_NO_CONNECT;
            if(sg_ucLinkStaus==1)	
                sg_ucLinkStaus=2;	//修改状态为已链接
        }else
#endif
#if 0        
        {       
            if(!sg_cNoDisp)
                vDispMid(3, "解析域名...");

            iRet=iGetHostByName((char*)szHostName, szHostIp);
            
    RE_CONN:        
            if(iRet && pszBakHost && pszBakHost[0])
            {
                //使用备份域名
                vDispMid(3, "使用备份服务器...");
                iRet=iParseHttpUrl(pszBakHost, szHostName, NULL, &uiPort);
                if (iRet)
                {
                    vPrtLklDebugLog(0, 1, "", "get domain url err.\n", 0);
                    vMessage("解析域名失败");
                    return -1*COM_PARAMETER;
                }
                iUseBakFlag=1;
                iRet=iGetHostByName(szHostName, szHostIp);
            }
            if(iRet)
            {
                vPrtLklDebugLog(0, 2, "get domain iP err", (char*)szHostName, 0);
                vMessage("解析域名失败");
                return -1*COM_PARAMETER;
            }
            vPrtLklDebugLog(0, 2, "get domain iP ok", szHostName, 0);

            sprintf(szHostPort, "%d", uiPort);

            connNum=0;
            sprintf(buf, "%s:%s", szHostIp, szHostPort);
            while(connNum<retry)
            {
                if(!sg_cNoDisp)
                {
                    if(connNum)
                        vDispMidVarArg(3, "连接服务器第%d次...", connNum+1);
                    else
                        vDispMid(3, "连接服务器...");
                }
                iRet=iCommTcpConn(szHostIp, szHostPort, 60*1000);
                if(iRet==0)
                    break;
                _vDelay(100);
                connNum++;
            }
            if(iRet && pszBakHost && pszBakHost[0] && iUseBakFlag==0)
            {
                goto RE_CONN;
            }
            if(iRet)
            {
                if(!sg_cNoDisp)
                    _vDisp(3, "");
                vPrtLklDebugLog(0, 2, "conn svr err", buf, 0);
                return -1*COM_ERR_NO_CONNECT;
            }
            vPrtLklDebugLog(0, 2, "conn svr OK", buf, 0);
            if(sg_ucLinkStaus==1)	
                sg_ucLinkStaus=2;	//修改状态为已链接
            if(iUseBakFlag)
                _vDisp(3,"");
        }
#else
        {
            //不解析域名,直接用域名连接
            char *pszHost;
            pszHost=(char*)szHostName;
            
            sprintf(szHostPort, "%d", uiPort);
            for(connNum=0; connNum<retry; connNum++)
            {
                sprintf(buf, "%s:%s", pszHost, szHostPort);
                if(!sg_cNoDisp)
                {
                    if(connNum)
                        vDispMidVarArg(3, "连接服务器第%d次...", connNum+1);
                    else
                        vDispMid(3, "连接服务器...");
                }
                iRet=iCommTcpConn(pszHost, szHostPort, 30*1000);
                if(iRet==0)
                    break;
                vPrtLklDebugLog(0, 2, "conn svr FAIL", buf, 0);
                _vDelay(200);

                if(iUseBakFlag==0)
                {
                    if(pszBakHost && pszBakHost[0])
                    {
                        iUseBakFlag=1;
                        pszHost=pszBakHost;
                    }
                }else
                {
                    iUseBakFlag=0;
                    pszHost=(char*)szHostName;
                }
                continue;
            }
            
            if(iRet)
            {
                if(!sg_cNoDisp)
                    _vDisp(3, "");
                return -1*COM_ERR_NO_CONNECT;
            }
            
            vPrtLklDebugLog(0, 2, "conn svr OK", buf, 0);
            if(sg_ucLinkStaus==1)	
                sg_ucLinkStaus=2;	//修改状态为已链接         
        }   
#endif            
    }
    
    if(!sg_cNoDisp)
        vDispMid(3, "发送请求...");
	vPrtLklDebugLog(0, 1, "", "send http req msg...\n", 0);
    
    #ifdef APP_LKL
    //注意，拉卡拉用ip地址联网时,http请求报文头host还是必须要填写域名,否则会返回http 410 Gone
    if((ucType&0x7F)==HTTP_TYPE_OTHER2)    //激活url,不需变域名
        ucType=(ucType&0x80)|HTTP_TYPE_FORMURL;
    else
        vGetLklReqHost((char*)szHostName);
    #endif
	iRet=iHttpSend(ucType, szHostName, uiPort, szSubUrl, pszBody, iBodyLen);
	dbg("iHttpSend ret:%d\n", iRet);
	if(iRet==0)
	{
        if(!sg_cNoDisp)
            vDispMid(3, "接收响应...");
		vPrtLklDebugLog(0, 1, "", "recv http rsp msg...\n", 0);
		iRet=iHttpRecv(0, pszRsp, uiRspSize, &uiLen, uiTimeOut, NULL);
		dbg("iHttpRecv ret:%d\n", iRet);
        if(iRet)
        {
            if(iRet<0)
            {
                if(iRet==-2)
                    iRet=-1*COM_ERR_RECV_HTTPHEAD;
                else
                    iRet=-1*COM_ERR_RECV;
            }else
                iRet=COM_ERR_RECV;
        }
	}else
		iRet=-1*COM_ERR_SEND;

	if(sg_ucLinkStaus==0)
    {
#ifdef  SUPPORT_PRECONN
		iConnReset(1);
#else        
        iCommTcpDisConn();
#endif        
    }
    if(!sg_cNoDisp)
        _vDisp(3, "");
	return iRet;
}

#ifdef APP_SUPPORT_HTTPS
#define HTTPS_RECV_SIZE 9*1024

#ifdef APP_CS_CCB
const char *HttpsPostHeaders = 	"User-Agent: Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)\r\n"
								"Cache-Control: no-cache\r\n"
								"Accept: */*\r\n"
								"Content-type: application/json\r\n";
#endif
int iHttpsSendRecv(uchar ucReqHeadType, char *pszUrl, char *pszBakHost, int retry, char *pszBody, int iBodyLen, char *pszRsp, uint uiRspSize, uint uiTimeOut)
{
	int ret;
	unsigned char send[8192];	
	int data_len = 0;
    uint line;
    //char buf[40];
    
    //unsigned char recv[HTTPS_RECV_SIZE];
    unsigned char *recv=NULL;

	uchar szHostName[256 + 1], szSubUrl[256 + 1];
	uint uiPort=0;
	char szHostIp[20]={0};
	int iRet;
	//char buf[50];
  	//int connNum;
    //int iUseBakFlag=0;
    char  *pBody;
    char *p;
		
    if(retry<=0)
        retry=1;
    
    sg_szHttpErrMsg[0]=0;       //错误信息置0
    
    line=sg_cNoDisp?_uiGetVLines():(_uiGetVLines()+1)/2;
    
    vPrtLklDebugLog(0, 2, "url", pszUrl, 0);
    vSetSSLFlag(1);
	iRet = iParseHttpUrl(pszUrl, (char*)szHostName, (char*)szSubUrl, (int*)&uiPort);
	if (iRet)
	{
		vPrtLklDebugLog(0, 1, "", "get domain url err.\n", 0);
        //vMessageLine(line, "解析域名失败");
        strcpy(sg_szHttpErrMsg, "解析域名失败");

        #ifdef  SUPPORT_PRECONN
        iConnReset(2);
        #endif
        
		return -1*COM_PARAMETER;
	}
	
	//if(!sg_cNoDisp)
    //    vClearLines(2);
#if 0
    vDispMid(line, "解析域名...");

    iRet=iGetHostByName((char*)szHostName, szHostIp);
          
    if(iRet && pszBakHost && pszBakHost[0])
    {
        //使用备份域名
        vDispMid(line, "使用备份服务器...");
        iRet=iParseHttpUrl(pszBakHost, (char*)szHostName, NULL, (int*)&uiPort);
        if (iRet)
        {
            vPrtLklDebugLog(0, 1, "", "get domain url err.\n", 0);
            vMessageLine(line, "解析域名失败");
            return -1*COM_PARAMETER;
        }
        iUseBakFlag=1;
        iRet=iGetHostByName((char*)szHostName, szHostIp);
    }
		
	if(iRet)
	{
		vPrtLklDebugLog(0, 2, "get domain iP err", (char*)szHostName, 0);
		vMessageLine(line, "解析域名失败");
		return -1*COM_PARAMETER;
	}
	vPrtLklDebugLog(0, 2, "get domain iP ok", (char*)szHostName, 0);

	connNum=0;
	sprintf(buf, "%s:%d", szHostIp, uiPort);
#endif

    ret=1;
#ifdef SUPPORT_PRECONN
    if(iGetPreConnRunFlag())   //开了预连接
    {
        vDispMid(line, "连接服务器...");
        ret=iWaitPreConnResult();
        dbg("iWaitPreConnResult ret:%d\n", ret);
        //预拨号若失败则挂断尝试重新连接(可能是内存分配失败,重连大概率正常)
        if(ret)
        {
            iConnReset(2);
            //vDispMid(4, "预拨号失败,尝试重连...");
            //_vDelay(100);
            //_vDisp(4, "");
            _vDelay(50);
        }
    }
#endif
    if(ret)
    {
        //if(!sg_cNoDisp)
        //    vClearLines(2);
        vDispMid(line, "SSL初始化...");
        
        ret=iSSL_Init(NULL, NULL, NULL); 	//CA证书和 客户端证书和key
        dbg("iSSL_Init ret=%d\n", ret);
        if(ret)
        {
            sprintf(sg_szHttpErrMsg, "SSL初始化失败:%d", ret);
            //vMessageLine(line, sg_szHttpErrMsg);
            
            iSSL_Free();
            return COM_ERR_NO_CONNECT;
        }
        
        vDispMid(line, "连接服务器...");
        //ret=iSSL_Connect(szHostIp, uiPort, 60);
        ret=iSSL_Connect2(szHostName, szHostIp, uiPort, 60);
        dbg("iSSL_Connect ret=%d\n", ret);       
    }
    if(ret)
    {        
        sprintf(sg_szHttpErrMsg, "连接服务器失败:%d", ret);
        //vMessageLine(line, sg_szHttpErrMsg);
        
#ifdef  SUPPORT_PRECONN
        iConnReset(2);
#else
        iSSL_Free();
#endif
        return COM_ERR_NO_CONNECT;
    }
    
    if(iBodyLen<0 && pszBody)
        iBodyLen=strlen(pszBody);
    vGetHttpReqHead(ucReqHeadType, szHostName, uiPort, szSubUrl, (uchar*)pszBody, iBodyLen, (char*)send);
    data_len=strlen((char*)send);
    
    if( (ucReqHeadType&HTTP_GET)==0 )
    {
        memcpy(send+data_len, pszBody, iBodyLen);
        data_len+=iBodyLen;
        send[data_len]=0;
    }

    //接收内存
    recv=malloc(HTTPS_RECV_SIZE);
    if(recv==NULL)
    {
        //vMessageLine(line, "接收数据内存不足");
        strcpy(sg_szHttpErrMsg, "接收数据内存不足");
        
#ifdef  SUPPORT_PRECONN
        iConnReset(2);
#else
        iSSL_DisConnect();
        iSSL_Free();
#endif
        return -1*COM_ERR_SEND;
    }

    vDispMid(line, "发送报文...");
	dbg("send data(%d)=[%s]\n", strlen((char*)send), send);
	
	ret=iSSL_Send(send, data_len);
	dbg("iSSL_Send ret=%d\n", ret);
	if(ret<=0)
	{
        if(recv) {free(recv); recv=NULL;}
        
        sprintf(sg_szHttpErrMsg, "SSL发送报文失败:%d", ret);
        //vMessageLine(line, sg_szHttpErrMsg);
        
#ifdef  SUPPORT_PRECONN
        iConnReset(2);
#else
        iSSL_DisConnect();
        iSSL_Free();
#endif
		return -1*COM_ERR_SEND;
	}

	vDispMid(line, "接收报文...");
	ret=iSSL_Recv(recv, HTTPS_RECV_SIZE-1, uiTimeOut*1000);
    if(ret>=0)
        recv[ret]=0;
    
    dbg("iSSL_Recv ret=%d\n", ret);
    dbg("recv1[%d]=[%s]\n", strlen((char*)recv), recv);
    
    pBody=NULL;
    if(ret>1000)
    {
        char *p;
        int explen=0;
        int rcvlen=0;
        int headlen=0;

        pBody= strstr((char*)recv, "\r\n\r\n");
        if(pBody)
        {
            pBody += 4;
            headlen=(uchar*)pBody-recv;
            rcvlen=ret-headlen;
            
            p=strstr((char*)recv, "Content-Length: ");
            if(p)
            {
                p+=strlen("Content-Length: ");
                explen=atoi(p);
            }else
            {
        #ifdef SUPPORT_HTTP_CHUNK
                //创识农行http可能没有Content-Length返回,判断是否chunked格式
                if (strstr((char*)recv, "Transfer-Encoding: chunked"))
                {
                        //创识农行http可能没有Content-Length返回,判断是否chunked格式
                        iRet=-1;
                        p=strstr(pBody, "\r\n");
                        if(p)
                        {
                            data_len=ulHexToLong((uchar*)pBody, p-pBody);
                            dbg("data_len=%d, firstLine len=%d, rcvlen=%d\n", data_len, p-pBody+2, rcvlen);
                            if(data_len<uiRspSize)
                            {
                                if(data_len+(p-pBody+2)>rcvlen)
                                {
                                    dbg("continue recv....\n");
                                    //还需要继续接收
                                    vSetQuickRcv(1);
                                    iRet=iSSL_Recv((uchar*)pBody+rcvlen, data_len+(p-pBody+2)-rcvlen+2, 1000);
                                    if(iRet>0)
                                    {
                                        rcvlen+=iRet;
                                        pBody[rcvlen]=0;
                                    }
                                    vSetQuickRcv(0);
                                    //if(iRet<=0)
                                    //
                                }
                                vMemcpy0(pszRsp, p+2, data_len);
                                iRet=0;
                            }else
                            {
                                #ifdef  SUPPORT_PRECONN
                                iConnReset(2);
                                #else
                                    iSSL_DisConnect();
                                    iSSL_Free();
                                #endif
                                if(recv) {free(recv); recv=NULL;}
                                
                                //vMessageLine(line, "报文长度异常");
                                strcpy(sg_szHttpErrMsg, "报文长度异常");
                                return -1*COM_ERR_RECV;
                            }
                        }
                        
                        #ifdef  SUPPORT_PRECONN
                        iConnReset(2);
                        #else
                            iSSL_DisConnect();
                            iSSL_Free();
                        #endif

                        if(recv) {free(recv); recv=NULL;}
                        
                        dbg("A.pszRsp(%d)=[%s]\n", strlen(pszRsp), pszRsp);
                        return 0;
                }
                dbg("Content-Length err, uiRecvLen=%d, rcv=[%s]\n", strlen((char*)recv), recv);
        #endif                
            }
        }
        if(explen>0 && rcvlen>=0 && explen>rcvlen)
        {
            while(explen>rcvlen)
            {
                ret=iSSL_Recv(recv+headlen+rcvlen, HTTPS_RECV_SIZE-1-headlen-rcvlen, 2*1000);
                if(ret>0)
                    rcvlen+=ret;
                else
                    break;
            }
            ret=headlen+rcvlen;
        }
    }
    
#ifdef  SUPPORT_PRECONN
    iConnReset(2);
#else
    iSSL_DisConnect();
    iSSL_Free();
#endif
    
	if(ret<=0)
	{
        if(recv) {free(recv); recv=NULL;}

        sprintf(sg_szHttpErrMsg, "SSL接收报文失败:%d", ret);
        //vMessageLine(line, sg_szHttpErrMsg);
		return -1*COM_ERR_RECV;
	}
	recv[ret]=0;
	dbg("recv data(%d)(%d)=[%s]\n", ret, strlen((char*)recv), recv);

	//if(!sg_cNoDisp)
    //    vClearLines(2);

	//搜索http head与body空行(分隔符"\r\n\r\n")
    if(pBody==NULL)
    {
        pBody= strstr((char*)recv, "\r\n\r\n");
        if(pBody==NULL)
        {
            if(recv) {free(recv); recv=NULL;}
            
            sprintf(sg_szHttpErrMsg, "SSL接收报文失败:%d", ret);
            //vMessageLine(line, sg_szHttpErrMsg);
            return -1*COM_ERR_RECV;
        }
        pBody += 4;
	}
    
#ifdef SUPPORT_HTTP_CHUNK
    //创识农行http可能没有Content-Length返回,判断是否chunked格式
    if (strstr((char*)recv, "Transfer-Encoding: chunked"))
    {
        iRet=-1;
        p=strstr(pBody, "\r\n");
        if(p)
        {
            data_len=ulHexToLong((uchar*)pBody, p-pBody);
            if(data_len>=uiRspSize)
                data_len=uiRspSize-1;
            vMemcpy0(pszRsp, p+2, data_len);
            iRet=0;
        }
        if(recv) {free(recv); recv=NULL;}
        
        if(iRet)
        {            
            sprintf(sg_szHttpErrMsg, "SSL接收报文失败:%d", ret);
            //vMessageLine(line, sg_szHttpErrMsg);
            return -1*COM_ERR_RECV;
        }
        dbg("chunk RspBody(%d)=[%s]\n", strlen(pszRsp), pszRsp);
        return 0;
    }
    //dbg("Content-Length err, uiRecvLen=%d, rcv=[%s]\n", strlen((char*)recv), recv);
#endif

	strncpy(pszRsp, pBody, uiRspSize-1);
    pszRsp[uiRspSize-1]=0;
	
    if(recv) {free(recv); recv=NULL;}
    
	dbg("RspBody(%d)=[%s]\n", strlen(pszRsp), pszRsp);
	
	return 0;
}
#endif

#if 0
int iHttpRecvFile(uchar *pszBody, uint uiRecvSize, uint *puiRecvLen, uint *puiFileSize, uint uiTimeOut)
{
	char szBuff[1024], szTmp[30];
	char *pszBuf, *pBody, *pTmp;
	uint uiRecvLen, uiBodyLen = 0;
	int iRet, iExpRcvLen;

	if (uiRecvSize < 30)
	{
		dbg("uiBodySize=[%d], too small !!!\n", uiBodySize);
		return 1;
	}

	//收取HttpHead
	memset(szBuff, 0, sizeof(szBuff));
	uiRecvLen = 0;
	iExpRcvLen = sizeof(szBuff) - 1;
	pszBuf = szBuff;
	pBody = NULL;
	while (iExpRcvLen > 0)
	{
		iRet = iCommTcpRecv((uchar *)pszBuf, iExpRcvLen, uiTimeOut * 1000);
		if (iRet <= 0)
		{
			dbg("RecvData iRet=[%d], uiRecvLen=%d\n", iRet, uiRecvLen);
			if (iRet == 0)
				iRet = -9;
			return iRet;
		}
		pszBuf[iRet] = 0;
		uiTimeOut = 5;

		uiRecvLen += iRet;

		//搜索http head与body空行(分隔符"\r\n\r\n")
		if ((pBody = strstr(szBuff, "\r\n\r\n")) == NULL)
		{
			pszBuf = szBuff + uiRecvLen;
			iExpRcvLen = sizeof(szBuff) - 1 - uiRecvLen;
			continue;
		}

		sprintf(szTmp, "header=[%%.%ds]\n", pBody - szBuff);
		//dbg(szTmp, szBuff);
		dbgLTxt(szTmp, szBuff);

		if (strstr(szBuff, " 200 OK") == NULL && strstr(szBuff, " 206 Partial Content") == NULL)
		{
			//http返回不成功
			dbg("http head NOT OK!!!\n");
			//return -2;
		}
		pBody[2] = 0;

		pBody += 4;
		break;
	}

	if (pBody == NULL)
	{
		dbg("RecvData Head=[%s], uiRecvLen=%d, no body err!\n", szBuff, uiRecvLen);
		return 1;
	}

	//搜索Content-Range
	if(*puiFileSize==0)
	{
		if ((pszBuf = strstr(szBuff, "Content-Range:")) != NULL)
		{
			pTmp=strchr(pszBuf, '/');
			if(pTmp)
			{
				*puiFileSize=atoi(pTmp+1);
			}
		}
	}

	//搜索Content-Length
	if ((pszBuf = strstr(szBuff, "Content-Length:")) == NULL)
	{
		dbg("Content-Length err, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 2;
	}

	pszBuf = pszBuf + strlen("Content-Length:");

	if ((pTmp = strstr(pszBuf, "\r\n")) == NULL)
	{
		dbg("Content-Length err2, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 3;
	}
	if (pTmp - pszBuf > 10)
	{
		dbg("Content-Length err3, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szBuff);
		return 3;
	}
	vMemcpy0((uchar *)szTmp, (uchar *)pszBuf, pTmp - pszBuf);

	//跳过空格,找到数字起始位置
	pszBuf = szTmp;
	/*	
	//atoi会自动跳过前部空格
	while (*pszBuf)
	{
		if (*pszBuf >= '0' && *pszBuf <= '9')
			break;
		pszBuf++;
	}
*/
	uiBodyLen = atoi(pszBuf);
	if (uiBodyLen + 1 > uiRecvSize)
	{
		dbg("Content-Length err4, uiBodySize=[%d], BodyLen=[%d]\n", uiRecvSize, uiBodyLen);
		return 1;
	}
	memcpy(pszBody, pBody, uiRecvLen - (pBody - szBuff));
	uiRecvLen = uiRecvLen - (pBody - szBuff);

	pszBuf = (char *)pszBody + uiRecvLen;
	while (uiBodyLen > uiRecvLen)
	{
		iRet = iCommTcpRecv((uchar *)pszBuf, uiBodyLen - uiRecvLen, uiTimeOut * 1000);
		if (iRet <= 0)
		{
			dbg("body RecvData uiRet=[%d], uiRecvLen=%d\n", iRet, uiRecvLen);
			return 1;
		}

		pszBuf += iRet;
		uiRecvLen += iRet;
	}
	pszBody[uiBodyLen] = 0;

	dbgLTxt("Recv Rsp(%d):[%s]\n", uiBodyLen, pszBody);

	return 0;
}
#endif

extern void vSetTcpTAMode(int mode);
//http下载文件
/*
	pszUrl:	下载地址
	pszFile:	文件名或数据空间，根据ucSaveFlag做处理
	puiFileSize:	文件长度
	puiDownloadLen:	实际已下载的文件长度(不含文件头长度), 同时做输入和输出参数。输入时作为断点续传起始位置。
	ucSaveFlag:	保存标志.若为1,则pszFile为文件名,需保存到文件中。为0则下载内容直接缓存在pszFile变量中。
    psFileHead: 文件头信息,拉卡拉下载的应用文件头部有多余数据
    uiFileHeadLen: 拉卡拉下载的应用文件头部信息长度。参数文件和其它非拉卡拉应用文件此处填0
    saveFunc: 保存断点续传长度变量的函数指针, 可为NULL
ret:	0-下载成功  <0下载失败  1-部分下载
*/
int iHttpDownloadFile(uchar *pszUrl, uchar *pszFile, uint *puiFileSize, uint *puiDownloadLen, 
            uchar ucSaveFlag, uchar *psFileHead, uint uiFileHeadLen, void (*saveFunc)(ulong, uchar *, uint))
//int iHttpDownloadFile(char *pszUrl, char *file, uint uiFileSize)
{
	uchar szHostName[256 + 1], szSubUrl[256 + 1];
	uint uiPort = 0;
	int iRet;
	uchar szUrlPort[10];
	uint uiHaveLen = 0;         //已下载字节数(含文件头长度)
	char szHttpHead[400];
#if 1//def USE_HIGH_BAUDRATE
	uint uiPackSize = 15000; //每包大小
	uchar rcv[uiPackSize + 300];
#else
	uint uiPackSize = 8000; //每包大小
	uchar rcv[8000 + 300];
#endif    
	uint uiLen, uiFileSize;
	uchar szHostIp[20], szHostPort[10];
	lfs_file_t file;
	long lFileSizetmp=0;
    uchar first=1;
    char szContentRange[20+1];
    char fileStat=0;
    
	//检查文件
	if(ucSaveFlag)
	{
		if(*puiDownloadLen==0)
			iRet = sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
		else
			iRet = sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY);
		if (iRet != 0)
		{
			dbg("iHttpDownloadFile, open file fail:[%s] %d\n", pszFile, iRet);
			vMessage("打开文件失败");
			return -101;
		}
        fileStat=1;
        
		if(*puiDownloadLen)
		{
			/*
			//检查文件大小
			lFileSizetmp = sys_lfs_file_seek(&file, 0, LFS_SEEK_END);
			if(lFileSizetmp<*puiDownloadLen)
			{
				*puiDownloadLen=0;
				sys_lfs_file_close(&file);
				sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
			}
			*/
			//定位到指定位置
			if(sys_lfs_file_seek(&file, *puiDownloadLen, LFS_SEEK_SET)!=*puiDownloadLen)
			{
				sys_lfs_file_close(&file);
				*puiDownloadLen=0;
				sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
			}
			if(*puiFileSize>0 && (*puiDownloadLen+uiFileHeadLen)==*puiFileSize)
			{
				sys_lfs_file_close(&file);
				return 0;
			}
		}
	}

	iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
	if (iRet)
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("解析下载地址失败");
		return -1;
	}

	if (uiPort == 80 || uiPort == 0)
		szUrlPort[0] = 0;
	else
		sprintf(szUrlPort, ":%u", uiPort);

	if( iGetHostByName(szHostName, szHostIp) )
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("解析域名失败");
		return -1;
	}
    
	//连接服务器
	vDispMid(3, "连接服务器");
	sprintf(szHostPort, "%d", uiPort);
    uiFileSize=*puiFileSize;
	iRet = iCommTcpConn(szHostIp, szHostPort, 60*1000);
	if(iRet)
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("连接服务器失败");
		return -2;
	}
    
    vClearLines(2);
    _vFlushKey();
    
	szContentRange[0]=0;
	uiHaveLen=*puiDownloadLen;
    if(uiHaveLen)
        uiHaveLen+=uiFileHeadLen;
	while (uiHaveLen < uiFileSize)
	{
		//vDispVarArg(4, "下载文件: %d/%d", uiHaveLen, uiFileSize);        
        //_vDisp(3, "下载文件:");
        //vDispVarArg(4, "文件:%d/%d", uiHaveLen, uiFileSize);
        
        vDispMid(3, "下载中");
        vDispVarArg(4, "    进度:%3lu%%", (uiHaveLen*100)/uiFileSize);
        
		uiLen=(uiHaveLen+uiPackSize>=uiFileSize)?uiFileSize:uiHaveLen+uiPackSize;
		sprintf(szHttpHead, "GET %s HTTP/1.1\r\n"
							"Host: %s%s\r\n"
							"Range: bytes=%u-%u\r\n"
							"\r\n",
				szSubUrl, szHostName, szUrlPort, uiHaveLen, uiLen - 1);

		//vDispMid(3, "发送请求...");
		iRet = iCommTcpSend((uchar *)szHttpHead, strlen(szHttpHead));
		if (iRet != strlen(szHttpHead))
		{
			if(first)
				iRet=-4;
			else
				iRet=4;
			break;
		}
        
		//vDispMid(3, "接收响应...");
		uiLen = 0;
		if(first)
			iRet = iHttpRecv(1, rcv, sizeof(rcv), &uiLen, 10, szContentRange);
		else
			iRet = iHttpRecv(1, rcv, sizeof(rcv), &uiLen, 10, NULL);
		if (iRet)
		{
			if(first)
				iRet=-5;
			else
				iRet=5;	
			break;
		}

		if(first && szContentRange[0])
		{
			dbg("Content-Range: bytes %s\n", szContentRange);
			//得到服务器返回的文件大小
			char *p;
			p=strchr(szContentRange, '/');
			if(p)
			{
				lFileSizetmp=atol(p+1);
				if(lFileSizetmp!=*puiFileSize)
				{
                    //if(fileStat)
                    //    sys_lfs_file_close(&file);
					//iCommTcpDisConn();
					//vDispVarArg(_uiGetVLines(), "filelen %ld!=%ld", lFileSizetmp, *puiFileSize);
					vMessage("文件长度信息不正确");
					//return -102;
                    iRet=-102;
                    break;
				}
			}
		}

		//写文件
		if(ucSaveFlag)
		{
            if(uiHaveLen==0)        //从0开始下,有文件头
            {
                if(uiLen<uiFileHeadLen || (uiFileHeadLen && rcv[0]!=0x01))      //0x01:单个文件
                {
                    iRet=-103;
                    break;
                }
                if(psFileHead && uiFileHeadLen)
                {
                    dbg("uiFileHeadLen=%d\n", uiFileHeadLen);
                    memcpy(psFileHead, rcv, uiFileHeadLen);
                }

                sys_lfs_file_write(&file, (char *)rcv+uiFileHeadLen, uiLen-uiFileHeadLen);                
                *puiDownloadLen+=(uiLen-uiFileHeadLen);
            }
            else
            {
                sys_lfs_file_write(&file, (char *)rcv, uiLen);
                *puiDownloadLen+=uiLen;
            }
            #if 1
            //文件关闭以实际写入
            {
                //_vDisp(3, "");
                sys_lfs_file_close(&file);
                //sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY | LFS_O_APPEND);
				sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY);
                sys_lfs_file_seek(&file, *puiDownloadLen, LFS_SEEK_SET);
            }
            #endif
            if(saveFunc)
            {
                if(uiHaveLen==0)
                    saveFunc(*puiDownloadLen, rcv, uiFileHeadLen);
                else
                    saveFunc(*puiDownloadLen, NULL, 0);
            }
		}else
		{
            dbg("uiLen=%d\n", uiLen);
			memcpy(pszFile, rcv, uiLen);            
		}
        first=0;
		uiHaveLen += uiLen;
        
        iRet=0;
	}
	if(fileStat)
	{
		sys_lfs_file_close(&file);
        fileStat=0;
	}
	iCommTcpDisConn();
	/*
	if(iRet)
		vMessage("下载失败");
	else 
    +-
		vMessage("下载成功");
	*/
	return iRet;
}

#if 1
//http下载文件(透传模式)
/*
	pszUrl:	下载地址
	pszFile:	文件名或数据空间，根据ucSaveFlag做处理
	puiFileSize:	文件长度
	puiDownloadLen:	实际已下载的文件长度(不含文件头长度), 同时做输入和输出参数。输入时作为断点续传起始位置。
	ucSaveFlag:	保存标志.若为1,则pszFile为文件名,需保存到文件中。为0则下载内容直接缓存在pszFile变量中。
    psFileHead: 文件头信息,拉卡拉下载的应用文件头部有多余数据
    uiFileHeadLen: 拉卡拉下载的应用文件头部信息长度。参数文件和其它非拉卡拉应用文件此处填0
    saveFunc: 保存断点续传长度变量的函数指针, 可为NULL
ret:	0-下载成功  <0下载失败  1-部分下载
*/
int iHttpDownloadFileTA(uchar *pszUrl, uchar *pszFile, uint *puiFileSize, uint *puiDownloadLen, 
            uchar ucSaveFlag, uchar *psFileHead, uint uiFileHeadLen, void (*saveFunc)(ulong, uchar *, uint))
{
	uchar szHostName[256 + 1], szSubUrl[256 + 1];
	uint uiPort = 0;
	int iRet;
	uchar szUrlPort[10];
	uint uiHaveLen = 0;         //已下载字节数(含文件头长度)
	char szHttpHead[400];
	//uint uiPackSize = 8000; //每包大小
	//uchar rcv[8000 + 100];
    
    const uint uiPackSize = 20*1000; //每包大小
    uchar *rcv;
    
	uint uiLen, uiFileSize;
	uchar szHostIp[20], szHostPort[10];
	lfs_file_t file;
	long lFileSizetmp=0;
    uchar first=1;
    char szContentRange[20+1];
    char fileStat=0;
    //uint uiOriLen;
    //uiOriLen=*puiDownloadLen;
    
	//检查文件
	if(ucSaveFlag)
	{
		if(*puiDownloadLen==0)
			iRet = sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
		else
			iRet = sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY);
		if (iRet != 0)
		{
			dbg("iHttpDownloadFile, open file fail:[%s] %d\n", pszFile, iRet);
			vMessage("打开文件失败");
			return -101;
		}
        fileStat=1;
        
		if(*puiDownloadLen)
		{
			/*
			//检查文件大小
			lFileSizetmp = sys_lfs_file_seek(&file, 0, LFS_SEEK_END);
			if(lFileSizetmp<*puiDownloadLen)
			{
				*puiDownloadLen=0;
				sys_lfs_file_close(&file);
				sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
			}
			*/
			//定位到指定位置
			if(sys_lfs_file_seek(&file, *puiDownloadLen, LFS_SEEK_SET)!=*puiDownloadLen)
			{
				sys_lfs_file_close(&file);
				*puiDownloadLen=0;
				sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
			}
            //已全部下载完成
			if(*puiFileSize>0 && (*puiDownloadLen+uiFileHeadLen)==*puiFileSize)
			{
				sys_lfs_file_close(&file);
				return 0;
			}
		}
	}

	iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
	if (iRet)
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("解析下载地址失败");
		return -1;
	}

	if (uiPort == 80 || uiPort == 0)
		szUrlPort[0] = 0;
	else
		sprintf(szUrlPort, ":%u", uiPort);

	if( iGetHostByName(szHostName, szHostIp) )
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("解析域名失败");
		return -1;
	}
    
    vSetTcpTAMode(1);
    
	//连接服务器
	vDispMid(3, "连接服务器");
	sprintf(szHostPort, "%d", uiPort);
    uiFileSize=*puiFileSize;
	iRet = iCommTcpConn(szHostIp, szHostPort, 60*1000);
	if(iRet)
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("连接服务器失败");
		return -2;
	}
    
    rcv=NULL;
    rcv=(uchar*)malloc(uiPackSize+300);
    if(rcv==NULL)
    {
        iCommTcpDisConn();
        if(fileStat)
            sys_lfs_file_close(&file);
        vMessage("内存不足");
        return -99;
    }

    vClearLines(2);
    _vFlushKey();
    
	szContentRange[0]=0;
	uiHaveLen=*puiDownloadLen;
    if(uiHaveLen)
        uiHaveLen+=uiFileHeadLen;
	while (uiHaveLen < uiFileSize)
	{
		//vDispVarArg(4, "下载文件: %d/%d", uiHaveLen, uiFileSize);        
        //_vDisp(3, "下载文件:");
        //vDispVarArg(4, "文件:%d/%d", uiHaveLen, uiFileSize);
        vDispMid(3, "下载中");
        vDispVarArg(4, "    进度:%3lu%%", (uiHaveLen*100)/uiFileSize);
        
		uiLen=(uiHaveLen+uiPackSize>=uiFileSize)?uiFileSize:uiHaveLen+uiPackSize;
		sprintf(szHttpHead, "GET %s HTTP/1.1\r\n"
							"Host: %s%s\r\n"
							"Range: bytes=%u-%u\r\n"
							"\r\n",
				szSubUrl, szHostName, szUrlPort, uiHaveLen, uiLen - 1);

		//vDispMid(3, "发送请求...");
		iRet = iCommTcpSend((uchar *)szHttpHead, strlen(szHttpHead));
		if (iRet != strlen(szHttpHead))
		{
			if(first)
				iRet=-4;
			else
				iRet=4;
			break;
		}
        
		//vDispMid(3, "接收响应...");
		uiLen = 0;
		if(first)
			iRet = iHttpRecv(1, rcv, uiPackSize+300-1, &uiLen, 10, szContentRange);
		else
			iRet = iHttpRecv(1, rcv, uiPackSize+300-1, &uiLen, 10, NULL);
		if (iRet)
		{
			if(first)
				iRet=-5;
			else
				iRet=5;	
			break;
		}

        if(uiLen<uiPackSize && uiLen<(uiFileSize-uiHaveLen))
        {
            //下载的长度不正确
            dbg("reqLen:%lu, revLen:%lu", uiPackSize>(uiFileSize-uiHaveLen)?uiPackSize:(uiFileSize-uiHaveLen), uiLen);
        }
        
		if(first && szContentRange[0])
		{
			dbg("Content-Range: bytes %s\n", szContentRange);
			//得到服务器返回的文件大小
			char *p;
			p=strchr(szContentRange, '/');
			if(p)
			{
				lFileSizetmp=atol(p+1);
				if(lFileSizetmp!=*puiFileSize)
				{
                    //if(fileStat)
                    //    sys_lfs_file_close(&file);
					//iCommTcpDisConn();
					//vDispVarArg(_uiGetVLines(), "filelen %ld!=%ld", lFileSizetmp, *puiFileSize);
					vMessage("文件长度信息不正确");
					//return -102;
                    iRet=-102;
                    break;
				}
			}
		}

		//写文件
		if(ucSaveFlag)
		{
            if(uiHaveLen==0)        //从0开始下,有文件头
            {
                if(uiLen<uiFileHeadLen || (uiFileHeadLen && rcv[0]!=0x01))      //0x01:单个文件
                {
                    iRet=-103;
                    break;
                }
                if(psFileHead && uiFileHeadLen)
                {
                    dbg("uiFileHeadLen=%d\n", uiFileHeadLen);
                    memcpy(psFileHead, rcv, uiFileHeadLen);
                }

                sys_lfs_file_write(&file, (char *)rcv+uiFileHeadLen, uiLen-uiFileHeadLen);                
                *puiDownloadLen+=(uiLen-uiFileHeadLen);
            }
            else
            {
                sys_lfs_file_write(&file, (char *)rcv, uiLen);
                *puiDownloadLen+=uiLen;
            }
            #if 1
            //文件关闭以实际写入,支持断点
            {
                //_vDisp(3, "");
                sys_lfs_file_close(&file);
                //sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY | LFS_O_APPEND);
				sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY);
                sys_lfs_file_seek(&file, *puiDownloadLen, LFS_SEEK_SET);
            }
            #endif
            if(saveFunc)
            {
                if(uiHaveLen==0)
                    saveFunc(*puiDownloadLen, rcv, uiFileHeadLen);
                else
                    saveFunc(*puiDownloadLen, NULL, 0);
            }
		}else
		{
            dbg("uiLen=%d\n", uiLen);
			memcpy(pszFile, rcv, uiLen);            
		}
        first=0;
		uiHaveLen += uiLen;
        
        iRet=0;
	}
    if(rcv)
    {
        free(rcv);
        rcv=NULL;
    }
	if(fileStat)
	{
		sys_lfs_file_close(&file);
        fileStat=0;
	}
	iCommTcpDisConn();
	/*
	if(iRet)
		vMessage("下载失败");
	else
		vMessage("下载成功");
	*/
    
    //透传模式网络断开，最后一大包有可能收到网络断开的状态上报保存在其中，去掉该包
    /*
    if(iRet==4 || iRet==5)
    {
        if(*puiDownloadLen-uiOriLen>=uiPackSize)
        {
            dbg("download len %lu -> %lu\n", *puiDownloadLen, *puiDownloadLen-uiPackSize);
            *puiDownloadLen-=uiPackSize;
            saveFunc(*puiDownloadLen, NULL, 0);
        }
    }
    */
    
	return iRet;
}

#else
//内部tcp连接时会开启透传模式,外部调用iHttpDownloadFileTA之后最好调用一次关闭透传防止出错
int iHttpDownloadFileTA(uchar *pszUrl, uchar *pszFile, uint *puiFileSize, uint *puiDownloadLen, 
            uchar ucSaveFlag, uchar *psFileHead, uint uiFileHeadLen, void (*saveFunc)(ulong, uchar *, uint))
//int iHttpDownloadFile(char *pszUrl, char *file, uint uiFileSize)
{
	uchar szHostName[256 + 1], szSubUrl[256 + 1];
	uint uiPort = 0;
	int iRet;
	uchar szUrlPort[10];
	uint uiHaveLen = 0;         //已下载字节数(含文件头长度)
	char szHttpHead[8001];
	uint uiPackSize = 60*1000; //每包大小
	//uchar rcv[80*1000 + 100];
    uchar *rcv;
	uint uiLen, uiFileSize;
	uchar szHostIp[20], szHostPort[10];
	lfs_file_t file;
	//long lFileSizetmp=0;
    //char szContentRange[20+1];
    char fileStat=0;
    
    int uiRecvLen=0;
    uchar ucHttpRspNotOK=0;
    
    char *p, *pTmp;
    char szTmp[20+1];
    uint uiBodyLen=0;
        
    rcv=NULL;
    
	//检查文件
	if(ucSaveFlag)
	{
		if(*puiDownloadLen==0)
			iRet = sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
		else
			iRet = sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY);
		if (iRet != 0)
		{
			dbg("iHttpDownloadFile, open file fail:[%s] %d\n", pszFile, iRet);
			vMessage("打开文件失败");
			return -101;
		}
        fileStat=1;
        
		if(*puiDownloadLen)
		{
			/*
			//检查文件大小
			lFileSizetmp = sys_lfs_file_seek(&file, 0, LFS_SEEK_END);
			if(lFileSizetmp<*puiDownloadLen)
			{
				*puiDownloadLen=0;
				sys_lfs_file_close(&file);
				sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
			}
			*/
			//定位到指定位置;
			if(sys_lfs_file_seek(&file, *puiDownloadLen, LFS_SEEK_SET)!=*puiDownloadLen)
			{
				sys_lfs_file_close(&file);
				*puiDownloadLen=0;
				sys_lfs_file_open(&file, pszFile, LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
			}
			if(*puiFileSize>0 && (*puiDownloadLen+uiFileHeadLen)==*puiFileSize)
			{
				sys_lfs_file_close(&file);
				return 0;
			}
		}
	}

	iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
	if (iRet)
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("解析下载地址失败");
		return -1;
	}

	if (uiPort == 80 || uiPort == 0)
		szUrlPort[0] = 0;
	else
		sprintf(szUrlPort, ":%u", uiPort);

	if( iGetHostByName(szHostName, szHostIp) )
	{
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("解析域名失败");
		return -1;
	}
    
    //开透传模式
    vSetTcpTAMode(1);
    
	//连接服务器
	vDispMid(3, "连接服务器");
	sprintf(szHostPort, "%d", uiPort);
    uiFileSize=*puiFileSize;
	iRet = iCommTcpConn(szHostIp, szHostPort, 60*1000);
	if(iRet)
	{
        vSetTcpTAMode(0);
        
        if(fileStat)
            sys_lfs_file_close(&file);
		vMessage("连接服务器失败");
		return -2;
	}

	//szContentRange[0]=0;
	uiHaveLen=*puiDownloadLen;
    if(uiHaveLen)
        uiHaveLen+=uiFileHeadLen;
    
    vDispVarArg(4, "文件:%d/%d", uiHaveLen, uiFileSize);
        
    //uiLen=(uiHaveLen+uiPackSize>=uiFileSize)?uiFileSize:uiHaveLen+uiPackSize;
    
    if(uiHaveLen && uiHaveLen<*puiFileSize)
    {
        sprintf(szHttpHead, "GET %s HTTP/1.1\r\n"
                        "Host: %s%s\r\n"
                        "Range: bytes=%u-%u\r\n"
                        "\r\n",
            szSubUrl, szHostName, szUrlPort, uiHaveLen, *puiFileSize - 1);
    }else
    {
        sprintf(szHttpHead, "GET %s HTTP/1.1\r\n"
                        "Host: %s%s\r\n"
                        "\r\n",
            szSubUrl, szHostName, szUrlPort);        
    }
    vDispMid(3, "发送请求...");
    iRet = iCommTcpSend((uchar *)szHttpHead, strlen(szHttpHead));
    if (iRet != strlen(szHttpHead))
    {
        iRet=-4;
        goto DOWNLOADFILE_END;
    }
    
    //接收http响应头
    memset(szHttpHead, 0, sizeof(szHttpHead));
    iRet = iCommTcpRecv((uchar *)szHttpHead, sizeof(szHttpHead)-1, 10 * 1000);
    if (iRet <= 0)
    {
        dbg("RecvData iRet=[%d], uiRecvLen=%d\n", iRet, sizeof(szHttpHead)-1);
        if (iRet == 0)
            iRet = -9;
        goto DOWNLOADFILE_END;
    }
    szHttpHead[iRet] = 0;

    //搜索http head与body空行(分隔符"\r\n\r\n")
    if ((p = strstr(szHttpHead, "\r\n\r\n")) == NULL)
    {
		dbg("RecvData Head=[%s], RcvLen=%d, no body err!\n", szHttpHead, iRet); 
        iRet=-99;
        goto DOWNLOADFILE_END;
    }
    
    rcv=malloc(uiPackSize+100);
    if(rcv==NULL)
    {
        iRet=-99;
        goto DOWNLOADFILE_END;
    }
    
    *p=0;
    p+=strlen("\r\n\r\n");
    uiLen=iRet-(p-szHttpHead);
    memcpy(rcv, p, uiLen);
    dbg("body:[%02X][%02X][%02X]...\n", rcv[0], rcv[1], rcv[2]);
    
#ifdef PRT_HTTP_LOG
    //sprintf(szTmp, "\n*** Http Rsp Head(%d)=[%%.%ds]\n", pBody - szBuff, pBody - szBuff);
    //dbgLTxt(szTmp, szBuff);
#endif
#if 1      
    if (strstr(szHttpHead, " 200 OK") == NULL && strstr(szHttpHead, " 206 Partial Content") == NULL)
    {
        //http返回不成功
        dbg("http head NOT OK!!!\n");
        //vMessage("http响应报文失败");
        //return -2;
        ucHttpRspNotOK=1;
    }
#endif 

	//搜索Content-Length
	if ((p = strstr(szHttpHead, "Content-Length:")) == NULL)
	{
		dbg("Content-Length err, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szHttpHead);
        if(ucHttpRspNotOK)
        {
            iRet=-2;
        }
		iRet=2;
        goto DOWNLOADFILE_END;
	}

	p += strlen("Content-Length:");

	if ((pTmp = strstr(p, "\r\n")) == NULL)
	{
		dbg("Content-Length err2, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szHttpHead);
        iRet=3;
		goto DOWNLOADFILE_END;
	}
	if (pTmp - p > 10)
	{
		dbg("Content-Length err3, uiRecvLen=%d, rcv=[%s]\n", uiRecvLen, szHttpHead);
		iRet=3;
        goto DOWNLOADFILE_END;
	}
	vMemcpy0((uchar *)szTmp, (uchar *)p, pTmp - p);

	//跳过空格,找到数字起始位置
	p = szTmp;
	//atoi会自动跳过前部空格
	/*
	while (*pszBuf)
	{
		if (*pszBuf >= '0' && *pszBuf <= '9')
			break;
		pszBuf++;
	}
*/
	uiBodyLen = atoi(p);
	/*
    if (uiBodyLen + 1 > uiBodySize)
	{
		dbg("Content-Length err4, uiBodySize=[%d], BodyLen=[%d]\n", uiBodySize, uiBodyLen);
		return 1;
	}
    */
    dbg("FileHeadLen:%d, HaveDownLen=%d, bodyLen=%d\n", uiFileHeadLen, *puiDownloadLen, uiBodyLen);
    if(*puiDownloadLen+uiBodyLen!=uiFileSize && uiFileHeadLen+*puiDownloadLen+uiBodyLen!=uiFileSize)
    {
        dbg("Content-Length err5, uiBodySize=[%d], uiFileSize=[%d]\n", uiBodyLen, uiFileSize);
		//return 1;
    }
    
    #if 1
    //写文件
    if(ucSaveFlag)
    {
        if(uiHaveLen==0 && uiFileHeadLen)        //从0开始下,有文件头
        {
            if(uiLen<uiFileHeadLen || (uiFileHeadLen && rcv[0]!=0x01))      //0x01:单个文件
            {
                iRet=-103;
                goto DOWNLOADFILE_END;
            }
            if(psFileHead)
            {
                dbg("uiFileHeadLen=%d\n", uiFileHeadLen);
                memcpy(psFileHead, rcv, uiFileHeadLen);
            }
            sys_lfs_file_write(&file, (char *)rcv+uiFileHeadLen, uiLen-uiFileHeadLen);                
            *puiDownloadLen+=(uiLen-uiFileHeadLen);
        }
        else
        {
            sys_lfs_file_write(&file, (char *)rcv, uiLen);
            *puiDownloadLen+=uiLen;
        }
        #if 1
        //文件关闭以实际写入
        {
            //_vDisp(3, "");
            sys_lfs_file_close(&file);
            //sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY | LFS_O_APPEND);
			sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY);
			sys_lfs_file_seek(&file, *puiDownloadLen, LFS_SEEK_SET);
        }
        #endif
        if(saveFunc)
        {
            if(uiHaveLen==0)
                saveFunc(*puiDownloadLen, rcv, uiFileHeadLen);
            else
                saveFunc(*puiDownloadLen, NULL, 0);
        }
    }else
    {
        dbg("uiLen=%d\n", uiLen);
        memcpy(pszFile, rcv, uiLen);            
    }
    #endif
    
    uiHaveLen+=uiLen;
    if(uiPackSize>sizeof(rcv)-100)
        uiPackSize=sizeof(rcv)-100;
	while (uiHaveLen < uiFileSize)
	{
		//vDispVarArg(4, "下载文件: %d/%d", uiHaveLen, uiFileSize);        
        //_vDisp(3, "下载文件:");

		vDispMid(3, "接收响应...");
        vDispVarArg(4, "文件:%d/%d", uiHaveLen, uiFileSize);
		uiLen = uiFileSize-uiHaveLen>uiPackSize?uiPackSize:uiFileSize-uiHaveLen;
        iRet = iCommTcpRecv(rcv, uiLen, 10 * 1000);
		if (iRet<=0)
		{
            iRet=5;	
			break;
		}
        
        uiLen=iRet;        
        
        uiRecvLen=0;
        #if 1
		//写文件
		if(ucSaveFlag)
		{
            sys_lfs_file_write(&file, (char *)rcv, uiLen);
            *puiDownloadLen+=uiLen;
            
            #if 1
            //文件关闭以实际写入
            {
                //_vDisp(3, "");
                sys_lfs_file_close(&file);
                //sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY | LFS_O_APPEND);
				sys_lfs_file_open(&file, pszFile, LFS_O_WRONLY);
				sys_lfs_file_seek(&file, *puiDownloadLen, LFS_SEEK_SET);
            }
            #endif
            if(saveFunc)
            {
                if(uiHaveLen==0)
                    saveFunc(*puiDownloadLen, rcv, uiFileHeadLen);
                else
                    saveFunc(*puiDownloadLen, NULL, 0);
            }
		}else
		{
			memcpy(pszFile, rcv, uiLen);            
		}
        #endif
		uiHaveLen += uiLen;
        dbg("rcv len:%d, total:%d/%d\n", uiLen, uiHaveLen, uiFileSize);
        
        //szHttpHead[0]=0;
        //sprintf(szHttpHead, "rcv[%d]=", uiHaveLen-9);
        //for(int n=9; n>0; n--)
        //    sprintf(szHttpHead+strlen(szHttpHead), " %02X", rcv[uiLen-n]);
        //strcat(szHttpHead, "\n");
        //dbg(szHttpHead);
        
        iRet=0;
	}
    vDispVarArg(4, "文件:%d/%d", uiHaveLen, uiFileSize);
    
DOWNLOADFILE_END:

    if(rcv)
        free(rcv);
    rcv=NULL;
    
	if(fileStat)
	{
		sys_lfs_file_close(&file);
	}
	iCommTcpDisConn();
    vSetTcpTAMode(0);
	/*
	if(iRet)
		vMessage("下载失败");
	else
		vMessage("下载成功");
	*/
	return iRet;
}
#endif



int iTcpConnSvr(char *pszSvrIp, char *pszSvrPort)
{
	int iRet;

	if(sg_ucLinkStaus!=2)
	{
        if(!sg_cNoDisp)
            vDispMid(3, "连接服务器...");
		iRet=iCommTcpConn(pszSvrIp, pszSvrPort, 60*1000);
		if(iRet)
		{
            if(!sg_cNoDisp)
                _vDisp(3, "");
			return -1*COM_ERR_NO_CONNECT;
		}
		if(sg_ucLinkStaus==1)	
			sg_ucLinkStaus=2;	//修改状态为已链接
		return 0;
	}else
		return 0;
}

static char sg_szCurSvrIp[15+1], sg_szCurSvrPort[5+1];
void vSetCurSvrIP(char *ip, char *port)
{
    strcpy(sg_szCurSvrIp, ip);
    strcpy(sg_szCurSvrPort, port);
}
void vGetCurSvrIP(char *ip, char *port)
{
    strcpy(ip, sg_szCurSvrIp);
    strcpy(port, sg_szCurSvrPort);
}

// ==0:成功 <0:参数失败或者发送失败 >0:接收失败
int iTcpSendRecv(char *pszSvrIp, char *pszSvrPort, char *pszBakSvrIp, char *pszBakSvrPort,
        uchar *psSnd, uint uiSndLen, uchar *psRsp, uint uiRspSize, uint *puiRcvLen, uint iTimeOutMs,uchar ucRevFlag)
{
	//uchar szHostName[256 + 1], szSubUrl[256 + 1];
	//uint uiPort=0;
    uint uiLen;
	char szHostIp[20], szHostPort[10];
	int iRet;
/*
	iRet = iParseHttpUrl(pszUrl, szHostName, szSubUrl, &uiPort);
	if (iRet)
		return -1*COM_PARAMETER;
*/
	if(sg_ucLinkStaus!=2)
	{
		#if 0
        vDispMid(3, "解析域名...");
		if( iGetHostByName(szHostName, szHostIp) )
		{
			vMessage("解析域名失败");
			return -1*COM_PARAMETER;
		}
		sprintf(szHostPort, "%d", uiPort);
        #else
        strcpy(szHostIp, pszSvrIp);
        strcpy(szHostPort, pszSvrPort);
        #endif
        
        vSetCurSvrIP("", "");
        if(!sg_cNoDisp)
            vDispMid(3, "连接服务器...");
		iRet=iCommTcpConn(szHostIp, szHostPort, 60*1000);
        if(iRet>0)
        {
            if(!sg_cNoDisp)
                vDispMid(3, "连接服务器2...");
            strcpy(szHostIp, pszBakSvrIp);
            strcpy(szHostPort, pszBakSvrPort);
            iRet=iCommTcpConn(szHostIp, szHostPort, 60*1000);
            if(iRet>0)
            {
                if(!sg_cNoDisp)
                    vDispMid(3, "连接服务器1...");
                strcpy(szHostIp, pszSvrIp);
                strcpy(szHostPort, pszSvrPort);
                iRet=iCommTcpConn(szHostIp, szHostPort, 60*1000);
            }
        }
		if(iRet)
		{
            if(!sg_cNoDisp)
                _vDisp(3, "");
			return -1*COM_ERR_NO_CONNECT;
		}
        vSetCurSvrIP(szHostIp, szHostPort);
        
		if(sg_ucLinkStaus==1)	
			sg_ucLinkStaus=2;	//修改状态为已链接
	}
    if(!sg_cNoDisp)
        vDispMid(3, "发送请求...");
    iRet = iCommTcpSend(psSnd, uiSndLen);
	dbg("iCommTcpSend ret:%d\n", iRet);
    if(iRet!=uiSndLen)
        iRet=-1*COM_ERR_SEND;
    else
    {  
            #if 1
    if(ucRevFlag)
    {	
        //冲正报文		 
        memcpy(gl_Send8583.Msg01, "0400", 4);
	gl_Send8583.Field11[0] = 0;
	gl_Send8583.Field26[0] = 0;
	gl_Send8583.Field35[0] = 0;
	gl_Send8583.Field36[0] = 0;
        memcpy(gl_Send8583.Field52, "\x00\x00", 2);
	gl_Send8583.Field53[0] = 0;	
//        memcpy(gl_Send8583.Field39, "98", 2);
	memcpy(gl_Send8583.Field58, "\x00\x00", 2);
	memcpy(gl_Send8583.Field59, "\x00\x00", 2);

       sprintf((char *)gl_Send8583.Field61, "%06lu%03s%06lu", gl_SysData.ulBatchNo, gl_SysData.szCurOper , gl_SysData.ulTTC);

         sprintf((char *)gl_Send8583.Field62+2, "%.4s%06lu%.10s", "0200",gl_SysData.ulTTC,"0000000000");
	 vLongToStr(strlen(gl_Send8583.Field62+2), 2, gl_Send8583.Field62);
	  dbg("gl_Send8583.Field62:%s\n",gl_Send8583.Field62);   
	  
			 
   //     sprintf((char *)gl_Send8583.Field61, "%06lu", gl_SysData.ulBatchNo); //gl_TransRec.ulVoidTTC);
   //     strcpy(gl_Send8583.Field61+6, gl_Send8583.Field11);
	
/*
	else if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
	{
	vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
	sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC, szBuf);
	}else if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID )
	{
	//sprintf((char *)gl_Send8583.Field61, "%06lu%06lu0000", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
	vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
	sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", 0L, 0L, szBuf);
	}	
	*/
        uiMemManaPut8583(MSG8583_TYPE_REV, &gl_Send8583);
    }
#endif   

    if(!sg_cNoDisp)
            vDispMid(3, "接收响应...");
        dbg("rcv timeout:%d\n", iTimeOutMs);
        iRet=iCommTcpRecv(psRsp, 2, iTimeOutMs);
        dbg("iCommTcpRecv1 ret:%d\n", iRet);
        if(iRet==2)
        {
            uiLen=psRsp[0]*256+psRsp[1];
            dbg("rcv len1:%u,[%02X][%02x]\n", uiLen, psRsp[0], psRsp[1]);
            if(2+uiLen<=uiRspSize)
            {
                iRet=iCommTcpRecv(psRsp+2, uiLen, 800+uiLen);
                dbg("iCommTcpRecv2 ret:%d\n", iRet);
                if(iRet==uiLen)
				{
					dbgHex("rsp data:", psRsp, 2+uiLen);
					*puiRcvLen=2+uiLen;
                    iRet=0;
				}else
                    iRet=COM_ERR_RECV;
            }else
                iRet=COM_ERR_RECV;                
        }else
            iRet=COM_ERR_RECV;
    }
	if(sg_ucLinkStaus==0)
		iCommTcpDisConn();
	if(!sg_cNoDisp)
        _vDisp(3, "");
	return iRet;
}

void vTestDownFile(void)
{
	int ret=0;
	char url[260], szFileName[100];
	uint uiFilelen, uiDownlen;
	ulong ultick;
    
    vDispCenter(1, "下载文件测试", 1);
    vClearLines(2);
    
    _vDisp(2, "按确认键开始下载");
    if(_uiGetKey()!=_KEY_ENTER)
        return;
    _vDisp(2, "");
    
	strcpy(szFileName, "/SysMem/testfile1.jpg");
    uiDownlen=0;
    
    ultick=_ulGetTickTime();
#if 0
	strcpy(url, "http://car3.autoimg.cn/cardfs/product/g24/M05/4A/CF/1024x0_1_q95_autohomecar__ChsEeV9d_Q6AAFnPACUoYkPwToQ526.jpg");
    uiFilelen=385229;
    
    ret = iHttpDownloadFile(url, szFileName, &uiFilelen, &uiDownlen, 1, NULL, 0, NULL);       //非拉卡拉应用文件    
#else
    strcpy(url, "http://lkl-bu-mer-basic-public.oss-cn-shanghai.aliyuncs.com/tms/task/merged_file/825aaba8-8cb9-41de-bb4d-1191069dcef8?Expires=4739844196&OSSAccessKeyId=LTAIOiB5t5vi8ivT&Signature=QyLoq50MwAiUZ%2BMOWskJxS9JHpU%3D");
	uiFilelen=648028;   //0x09e35c
    
	ret = iHttpDownloadFile(url, szFileName, &uiFilelen, &uiDownlen, 1, NULL, 1+3+8, NULL);
#endif    
	dbg("download %d time=%lu\n", uiFilelen, _ulGetTickTime()-ultick);    
	{
		lfs_file_t file;
		ret = sys_lfs_file_open(&file, szFileName, LFS_O_RDONLY);
		if(ret==0)
		{
			ret=sys_lfs_file_seek(&file, 0, LFS_SEEK_END);
			sys_lfs_file_close(&file);
			dbg("downloadfile testfile1.jpg ok. len=%d.\n", ret);
		}else
		{
			dbg("open downloadfile: testfile1.jpg fail.\n");
		}
	}

	//ultick=_ulGetTickTime();
	//ret=iMd5File(szFileName, NULL, 0, url);
	//dbg("iMd5File %d time=%lu\n", uiFilelen, _ulGetTickTime()-ultick);
	//dbg("iMd5File ret=%d\n", ret);
	//dbgHex("file md5", url, 16);

	sys_lfs_remove(szFileName);		//删除文件
    
    if(uiDownlen==uiFilelen)
    {
        vMessage("文件下载成功");
    }else
		vMessage("文件下载失败");

	return;
}


