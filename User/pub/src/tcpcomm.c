#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>

#include "vposface.h"
#include "pub.h"
#include "SysTick.h"
#include "debug.h"
#include "define.h"
#include "wifi.h"
#include "gprs.h"
#include "wifi_at.h"
#include "gprs_at.h"
//#include "sysparam.h"
#include "tcpcomm.h"
#include "utf2gb.h"
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "func.h"
#include "myhttp.h"

#ifdef USE_LTE_4G
#define NetName	"4G"
#else
#define NetName	"gprs"
#endif

extern int gprsInit(void);
extern int gprs_hardware_reset(void);
extern int wifiInitAndConnAP2(char *wifiName, char *wifiPwd);
extern int wifiInitAndConnAP(void);
extern int gprs_at_cmd_sndrcv(char *pszReq, char *pszCheckRsp, int iTimeOut, int icrflag, int iclearflag, char *pszOutRsp, int iOutSize);
extern void flushStatusBar(int iFlushNow);
extern void vSetIconFlushNow(void);
extern void vClearWilrelessIcon(void);
extern uint _uiCheckBattery(void);
extern int gprs_at_CheckSIMQuick(void);
extern void updateLastEventTimestamp(void);
extern void _vSetDispDataIcon(unsigned short val);

extern void vSetCommTypeforSleep(int type);

int iGetNetExtIp(char *pszIp, char *pszCity);
int iGetLACandCell(char *pszLAC, char *pszCellId);
void vSetSignalValZero(void);

//tatic int iCommInitFlag = 0;
static int sg_iConnFlag=0;

static int sg_iCsqVal=0;
static char sg_szICCID[20+1]={0};
static char sg_szIMEI[17+1]={0};
static char sg_szLocalIP[15+1]={0};

static int startComm=0;			//开机调用 iCommLogin 后才能取信号强度,避免一开机状态栏进程取信号强度发指令
static char sg_csleep=0;
/*
gprs_write[9] = AT+QIACT
gprs_read [17] = [
+CME ERROR: 3
]
可能是余额不足
*/

void (*gl_CommCallbackFunc)(void);

static ulong ulCellTime=0L;
//static char sg_szLAC[10], sg_szCellId[10];
static char sg_szCellInfo[30];
#ifdef APP_LKL
static char sg_szCellLoc[25];
#endif
static uchar sg_szLastSetTime[14+1];

//开机初始化时,配合二维码显示
static uint  sg_uiCommInitEvent=0;
static char sg_szWarnBuf[80]={0};
void vSetCommInitEvent(uint uiEvent)
{
    sg_uiCommInitEvent=uiEvent;
    //if(uiEvent)
    //    sg_szWarnBuf[0]=0;
}
void vDispCommInitResult(void)
{
    if(sg_szWarnBuf[0]==0)
        return;
    vDispMul(_uiGetVLines(), sg_szWarnBuf);
    iGetKeyWithTimeout(5);
    _vDisp(_uiGetVLines()-1, (uchar*)"");
    _vDisp(_uiGetVLines(), (uchar*)"");
    
    sg_szWarnBuf[0]=0;
}

//预拨号时不显示失败信息
static uchar sg_ucCommInitDispFlag=1;
void vSetCommInitDisp(uchar flag)
{
    sg_ucCommInitDispFlag=flag;
}
uchar ucGetCommInitDisp(void)
{
    return sg_ucCommInitDispFlag;
}

void vFlushStatusBar(void)
{
    vSetIconFlushNow();
    flushStatusBar(1);
}

int iCommConfig(void)
{
	uint uiKey, iOldCommType = 0;	
	ulong ulTimer;
    int ret;
    int timeout=60;
    
    _vCls();
	vDispCenter(1, "通讯设置", 1);

#if !defined(NO_WIFI)    
    if(iPosHardwareModule(CHK_HAVE_GPRS, NULL, 0)==0)       //没有4G(只有wifi)
    {
        iOldCommType=gl_SysInfo.iCommType;
        if(gl_SysInfo.iCommType != VPOSCOMM_WIFI)
        {
            gl_SysInfo.iCommType = VPOSCOMM_WIFI;
            uiMemManaPutSysInfo();
        }
        vSetCommTypeforSleep(gl_SysInfo.iCommType);
        if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
        {
            iCommLogout(iOldCommType, 1);
            uiMemManaPutSysInfo();
            vClearWilrelessIcon();
            vSetSignalValZero();
            vFlushStatusBar();
        }
        ret = iWifiConfig(NULL);
        return 0;
    }

    if(iPosHardwareModule(CHK_HAVE_WIFI, NULL, 0)>0)
    {
        _vDisp(2, (uchar *)"请选择通讯方式:");    
        vDispVarArg(3, (uchar *)"1-%s 2-WIFI", NetName);
        if (gl_SysInfo.iCommType == VPOSCOMM_GPRS || gl_SysInfo.iCommType == VPOSCOMM_WIFI)
        {
            if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
                ret = 1;
            else
                ret = 2;
            vDispVarArg(4, "通讯方式:[%d]", ret);
            iOldCommType = gl_SysInfo.iCommType;
        }
        else
            _vDisp(4, (uchar *)"通讯方式:[ ]");
        _vSetTimer(&ulTimer, timeout*100);
        while (1)
        {
            if(_uiTestTimer(ulTimer))
                return -1;
            if(!_uiKeyPressed())
                continue;
            _vSetTimer(&ulTimer, timeout*100);
            uiKey = _uiGetKey();
            if (uiKey == _KEY_1)
            {	
            	//选择通讯方式	     
	       if(!gl_SysInfo.szPosId[0] && !gl_SysInfo.szMerchId[0])
	 	{
			_vCls();
		 	vMessageMul("请先使用WIFI初始化设备");	
	         }
		else  
                	gl_SysInfo.iCommType = VPOSCOMM_GPRS;
                break;
            }
            if (uiKey == _KEY_2)
            {
                gl_SysInfo.iCommType = VPOSCOMM_WIFI;
                break;
            }
            if (uiKey == _KEY_ENTER && iOldCommType)
                break;
            if(uiKey == _KEY_ESC)
                break;
        }
        if(uiKey == _KEY_ESC && iOldCommType==0)		//取消设置
            return 1;
        
        if(uiKey == _KEY_ESC && iOldCommType==gl_SysInfo.iCommType)
            return 0;
            
        vClearLines(2);
        _vDisp(3, "通讯设置中,请稍候...");
        if (iOldCommType && iOldCommType != gl_SysInfo.iCommType)
        {
            iCommLogout(iOldCommType, 1);
            uiMemManaPutSysInfo();
            vClearWilrelessIcon();
            vSetSignalValZero();
            vFlushStatusBar();
        }
        
        vSetCommTypeforSleep(gl_SysInfo.iCommType);
        if (gl_SysInfo.iCommType == VPOSCOMM_WIFI)
            ret = iWifiConfig(NULL);
        else
        {
            uiMemManaPutSysInfo();
            iCommLogin();
        }
    }else
#endif
    {
        _vDisp(2, (uchar *)"通讯方式:");
        vDispVarArg(3, (char *)"1.%s", NetName);
        iOldCommType=gl_SysInfo.iCommType;
        if(gl_SysInfo.iCommType != VPOSCOMM_GPRS)
        {
            gl_SysInfo.iCommType = VPOSCOMM_GPRS;
            uiMemManaPutSysInfo();
        }
        vSetCommTypeforSleep(gl_SysInfo.iCommType);
        _vSetTimer(&ulTimer, timeout*100);
        while (1)
        {
            if(_uiTestTimer(ulTimer))
                return -1;
            if(!_uiKeyPressed())
                continue;
            //_vSetTimer(&ulTimer, timeout*100);
            uiKey = _uiGetKey();
            if (uiKey == _KEY_1 || uiKey == _KEY_ENTER || uiKey == _KEY_ESC)
                break;
        }
        if(uiKey == _KEY_ESC && iOldCommType==gl_SysInfo.iCommType)
            return 0;

        vClearLines(2);
        _vDisp(3, "通讯设置中,请稍候...");
        ret=iCommLogin();
        dbg("iCommLogin ret:%d\n", ret);
    }
	return 0;    
}
#ifdef FUDAN_CARD_DEMO
int iWifiConfig(void *p)
{

	AP_INFO APInfos[30];
	int ret;
	int size, n;
	int i, page, pageSize;
	uint uiKey;
	int sel;
	char wifiName[40 + 1], wifiPwd[20 + 1];
	char wifiNameUtf8[40 + 1];
	ulong ulTimer;
    int timeout=60;

	char *apszAlphaTable[10] = {
		"0.!@#$%^&*()-=_+;':,/<>?`~",
		"1qzQZ",
		"2abcABC",
		"3defDEF",
		"4ghiGHI",
		"5jklJKL",
		"6mnoMNO",
		"7prsPRS",
		"8tuvTUV",
		"9wxyWXY"};

	vSetAlphaTable((uchar **)apszAlphaTable);

	_vCls();
	if (gl_SysInfo.iCommType == VPOSCOMM_WIFI && gl_SysInfo.szWifiName[0] != 0)
	{
		vDispCenter(1, "WIFI Setting", 1);
		vDispVarArg(3, "ssid:%s", gl_SysInfo.szWifiName);
		_vDisp(5, "reset wifi?");
		if(iOK(8)!=1)
		{
			return -1;
		}
		_vFlushKey();
		_vCls();
	}

	vDispCenter(1, "WIFI Setting", 1);
	_vDisp(2, "1. enter  wifi");
	_vDisp(3, "2. select wifi");
	_vSetTimer(&ulTimer, timeout*100);
	while (1)
	{
		if(_uiTestTimer(ulTimer))
			return -1;
		if(!_uiKeyPressed())
			continue;
		_vSetTimer(&ulTimer, timeout*100);
		uiKey = _uiGetKey();
		if (uiKey == _KEY_1 || uiKey == _KEY_2)
			break;
		if(uiKey == _KEY_ESC)
			return -1;
	}
	_vFlushKey();

	wifiInitAndConnAP2(NULL, NULL);
	vDispCenter(1, "WIFI Setting", 1);
	vClearLines(2);
	if (uiKey == _KEY_1)
	{
		_vDisp(3, "enter wifi name:");
		_vDisp(4, "[                    ]");
		ret = iInput(INPUT_ALPHA, 4, 1, wifiName, 20, 1, 20, 60);
		if (ret <= 0)
			return -1;
		strcpy(wifiNameUtf8, wifiName);
	}
	else
	{
		_vDisp(2, "search wifi...");
		size = sizeof(APInfos) / sizeof(AP_INFO);
		ret = wifi_at_ListAP(APInfos, size);
		if (ret)
		{
			vClearLines(2);
			vMessage("search WiFi failed");
			return 1;
		}
		//n:取得的wifi个数
		for (n = 0; n < size && APInfos[n].ssid[0] != 0; n++)
			;

		sel = -1;
		page = 0;
		wifiName[0] = 0;
		pageSize = _uiGetVLines() >= 11 ? 9 : _uiGetVLines() - 2;
		dbg("n=%d, pagesize=%d.\n", n, pageSize);
		while (1)
		{
			vClearLines(2);
			for (i = page * pageSize; i < (page + 1) * pageSize && i < n; i++)
			{
				Utf8ToGB(APInfos[i].ssid, strlen(APInfos[i].ssid), wifiName);
				vDispVarArg(2 + i % pageSize, "%d. %s", 1 + i % pageSize, wifiName);
			}
			if (n <= pageSize)
				vDispCenter(_uiGetVLines(), (uchar *)"--number key to select--", 0);
			else
				vDispCenter(_uiGetVLines(), (uchar *)"-turn page Up/down-", 0);

			dbg("i=%d, page=%d\n", i, page);
			_vSetTimer(&ulTimer, timeout*100);
			while (1)
			{
				if(_uiTestTimer(ulTimer))
					return -1;
				if(!_uiKeyPressed())
					continue;
				_vSetTimer(&ulTimer, timeout*100);
				uiKey = _uiGetKey();
				if (uiKey >= _KEY_1 && uiKey <= _KEY_0 + 1 + (i - 1) % pageSize)
				{
					sel = page * pageSize + uiKey - _KEY_0 - 1;
					break;
				}

				if (uiKey == _KEY_UP && page > 0)
				{
					--page;
					dbg("--page, currpage=%d\n", page);
					break;
				}
				if (uiKey == _KEY_DOWN && page < (n + pageSize - 1) / pageSize)
				{
					++page;
					dbg("++page, currpage=%d\n", page);
					break;
				}
				if (uiKey == _KEY_ESC)
				{
					return -1;
				}

				continue;
			}
			if (sel >= 0)
				break;
		}
		dbg("sel:%d\n", sel);
		strcpy(wifiNameUtf8, APInfos[sel].ssid);
		Utf8ToGB(APInfos[sel].ssid, strlen(APInfos[sel].ssid), wifiName);
	}

	vDispCenter(1, "WIFI Setting", 1);
	vClearLines(5);
	vDispVarArg(2, "ssid:%s", wifiName);
	_vDisp(3, (uchar *)"enter wifi password:");
	_vDisp(4, (uchar *)"[                    ]");
	ret = iInput(INPUT_ALPHA, 4, 1, wifiPwd, 20, 0, 20, 60);
	if (ret < 0)
		return -1;

	vClearLines(3);

	gl_SysInfo.iCommType=VPOSCOMM_WIFI;
	strcpy(gl_SysInfo.szWifiName, wifiName);
	strcpy(gl_SysInfo.szWifiPwd, wifiPwd);
	strcpy(gl_SysInfo.szWifiNameUtf8, wifiNameUtf8);
	uiMemManaPutSysInfo();

    iCommLogin();

	return 0;
}
#else
#ifndef NO_WIFI
int iWifiConfig(void *p)
{
	AP_INFO APInfos[30];
	int ret;
	int size, n;
	int i, page, pageSize;
	uint uiKey;
	int sel;
	char wifiName[40 + 1], wifiPwd[20 + 1];
	char wifiNameUtf8[40 + 1];
	ulong ulTimer;
    char buf[100];
    char cConnFlag=0;
    int timeout=60;

	char *apszAlphaTable[10] = {
		"0.!@#$%^&*()-=_+;':,/<>?`~",
		"1qzQZ",
		"2abcABC",
		"3defDEF",
		"4ghiGHI",
		"5jklJKL",
		"6mnoMNO",
		"7prsPRS",
		"8tuvTUV",
		"9wxyWXY"};

	vSetAlphaTable((uchar **)apszAlphaTable);

	_vCls();
	if (gl_SysInfo.iCommType == VPOSCOMM_WIFI && gl_SysInfo.szWifiName[0] != 0)
	{
        ret=0;
		vDispCenter(1, "WIFI设置", 1);
        if(startComm)
        {
            ret=wifi_at_QueryAPConnStatus((char*)gl_SysInfo.szWifiNameUtf8, NULL);
            if(ret==-1)
            {
                wifi_at_DisconnAp();
                vSetSignalValZero();
                ret=0;
            }
        }
        
        sprintf(buf, "wifi:%s ", gl_SysInfo.szWifiName);
        if(strlen(buf)+6>_uiGetVCols())
        {
            n=iSplitGBStr(buf, _uiGetVCols()-6);
            buf[n]=0;
        }
        if(ret==1)
        {
            strcat(buf, "已连接");
            vDispMid(2, buf);
            vDispMid(3, "是否重新设置wifi?");
            if(iOK(15)!=1)
            {
                _vCls();
                return -1;
            }
            cConnFlag=1;
        }else
        {
            vSetSignalValZero();
            strcat(buf, "未连接");
            ret=iSelectOne(buf, "1.连接wifi", "2.重新设置wifi", NULL, 0, 30, 1);
            _vCls();
            if(ret<0)
                return ret;
            if(ret==1)
            {
                iCommLogin();
                return 0;
            }
        }
        _vFlushKey();
		_vCls();
	}

	vDispCenter(1, "WIFI设置", 1);
	_vDisp(2, "1. 手输wifi");
	_vDisp(3, "2. 选择wifi");
	_vSetTimer(&ulTimer, timeout*100);
	while (1)
	{
		if(_uiTestTimer(ulTimer))
			return -1;
		if(!_uiKeyPressed())
			continue;
		_vSetTimer(&ulTimer, timeout*100);
		uiKey = _uiGetKey();
		if (uiKey == _KEY_1 || uiKey == _KEY_2)
			break;
        if (uiKey == _KEY_ESC)
            return -1;
	}
	_vFlushKey();

    _vCls();
	ret=wifiInitAndConnAP2(NULL, NULL);
	vDispCenter(1, "WIFI设置", 1);
	vClearLines(2);
	if (uiKey == _KEY_1)
	{
		_vDisp(3, "请输入wifi名称:");
		//_vDisp(4, "[                    ]");
		//ret = iInput(INPUT_ALPHA, 4, 1, wifiName, 20, 1, 20, 60);
        ret = iInput(INPUT_ALPHA, 4, 0, wifiName, _uiGetVCols(), 1, _uiGetVCols(), 60);
		if (ret <= 0)
			return -1;
		strcpy(wifiNameUtf8, wifiName);
	}
	else
	{
		_vDisp(2, "搜索wifi热点...");
        memset(&APInfos, 0, sizeof(APInfos));
		size = sizeof(APInfos) / sizeof(AP_INFO);
		ret = wifi_at_ListAP(APInfos, size);
		if (ret)
		{
			vClearLines(2);
			vMessage("搜索WiFi热点失败");
			return -1;
		}
		//n:取得的wifi个数
		for (n = 0; n < size && APInfos[n].ssid[0] != 0; n++)
			;

		sel = -1;
		page = 0;
		wifiName[0] = 0;
		pageSize = _uiGetVLines() >= 11 ? 9 : _uiGetVLines() - 2;
		dbg("n=%d, pagesize=%d.\n", n, pageSize);
		while (1)
		{
			vClearLines(2);
			for (i = page * pageSize; i < (page + 1) * pageSize && i < n; i++)
			{
				Utf8ToGB(APInfos[i].ssid, strlen(APInfos[i].ssid), wifiName);
                #if 0//def FOR_JSD_TEST
                vDispVarArg(2 + i % pageSize, "%d[%d] %s", 1 + i % pageSize, APInfos[i].rssi, wifiName);
                #else
				vDispVarArg(2 + i % pageSize, "%d. %s", 1 + i % pageSize, wifiName);
                #endif
			}
			if (n <= pageSize)
				_vDisp(_uiGetVLines(), (uchar *)"--请按数字键选择--");
			else
				_vDisp(_uiGetVLines(), (uchar *)"--数字键选择,箭头键翻页--");

			dbg("i=%d, page=%d\n", i, page);
			_vSetTimer(&ulTimer, timeout*100);
			while (1)
			{
				if(_uiTestTimer(ulTimer))
					return -1;
				if(!_uiKeyPressed())
					continue;
				_vSetTimer(&ulTimer, timeout*100);
				uiKey = _uiGetKey();
				if (uiKey >= _KEY_1 && uiKey <= _KEY_0 + 1 + (i - 1) % pageSize)
				{
					sel = page * pageSize + uiKey - _KEY_0 - 1;
					break;
				}

				if (uiKey == _KEY_UP && page > 0)
				{
					--page;
					dbg("--page, currpage=%d\n", page);
					break;
				}
				if (uiKey == _KEY_DOWN && page < (n + pageSize - 1) / pageSize - 1)
				{
					++page;
					dbg("++page, currpage=%d\n", page);
					break;
				}
				if (uiKey == _KEY_ESC)
				{
					return -1;
				}

				continue;
			}
			if (sel >= 0)
				break;
		}
		dbg("sel:%d\n", sel);
		strcpy(wifiNameUtf8, APInfos[sel].ssid);
		Utf8ToGB(APInfos[sel].ssid, strlen(APInfos[sel].ssid), wifiName);
	}

	vDispCenter(1, "WIFI设置", 1);
	vClearLines(5);
	vDispVarArg(2, "ssid:%s", wifiName);
	_vDisp(3, (uchar *)"请输入wifi密码:");
	_vDisp(4, (uchar *)"");
    #ifdef APP_CS_ABC
    vDispMid(_uiGetVCols(), "连续按[0]切换特殊字符");
    #endif
	ret = iInput(INPUT_ALPHA, 4, 1, wifiPwd, 20, 0, 20, 60);
	if (ret < 0)
		return -1;

	vClearLines(3);

	gl_SysInfo.iCommType=VPOSCOMM_WIFI;
	strcpy(gl_SysInfo.szWifiName, wifiName);
	strcpy(gl_SysInfo.szWifiPwd, wifiPwd);
	strcpy(gl_SysInfo.szWifiNameUtf8, wifiNameUtf8);
	uiMemManaPutSysInfo();

    //重设wifi前断开AP
    if(cConnFlag)
    {
        wifi_at_DisconnAp();
        vSetSignalValZero();
    }
    
	//wifiInitAndConnAP();
    //if(p!=NULL)
        iCommLogin();

	return 0;
}
#endif
#endif

extern uint _uiSetTime2(struct tm *tm_new);
extern int wifi_at_time(char *pszDateTime);

//签到同步时间后可调用
void vSetLastSyncTime(char *pszTime)
{
    strcpy((char*)sg_szLastSetTime, pszTime);
}

//每天同步时间
//ret: 0-未触发同步操作 1-有同步操作
int iPosSyncTime(void)
{
	uchar buf[30+1];

	_vGetTime(buf);
	if(memcmp(sg_szLastSetTime, buf, 10))
	{
		if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
		{
			struct tm rtcTm;
			if(gprs_at_get_cclk(&rtcTm)==0)
			{
				dbg("year:%d\n", rtcTm.tm_year);
				if(rtcTm.tm_year>120) //时间需大于2020年
				{
					_uiSetTime2(&rtcTm);
					_vGetTime(sg_szLastSetTime);
				}
			}else
            {
                if(startComm && gprs_at_MoudleIsActive(1)==0)
                {
                    startComm=0;
                }
            }
		}else
		{
		#ifndef NO_WIFI
			buf[0]=0;
			wifi_at_time(buf);
			if(buf[0])
			{
				if(memcmp(buf, "2020", 4)>0)
				{
					_uiSetTime(buf);
					_vGetTime(sg_szLastSetTime);
				}
			}else
            {
                if(startComm && wifi_at_MoudleIsActive()==0)
                {
                    startComm=0;
                }
            }
		#endif
		}
        return 1;
	}
    return 0;
}

int iCommLogin(void)
{
	int ret=1;
	//if(iCommInitFlag==1)
	//	return 0;
#ifdef JTAG_DEBUG    
	ulong tk=_ulGetTickTime();
#endif    
    
    dbg("iCommLogin iCommType=%d\n", gl_SysInfo.iCommType);
	if (gl_SysInfo.iCommType != VPOSCOMM_GPRS && gl_SysInfo.iCommType != VPOSCOMM_WIFI)
	{
		dbg("comm param err.\n");
		return -1;
	}
    
    vSetCommTypeforSleep(gl_SysInfo.iCommType);
    
	if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
	{
		ret = gprsInit();
        if(ret==0)
        {
            gprs_at_iccid(sg_szICCID);
            gprs_at_imei(sg_szIMEI);
            gprs_at_localip(sg_szLocalIP);
            
			//iGetLACandCell(NULL, NULL);
			//iGetGprsCellInfo(NULL, NULL);
  
    #if (defined(JTAG_DEBUG) && defined(USE_LTE_4G))
			gprs_at_QFENG(NULL);	//test
    #endif
            
        }else
        {
            gprs_at_imei(sg_szIMEI);
            if(ret!=-100)
                gprs_at_iccid(sg_szICCID);
        }
	}
	else
	{
	#ifndef NO_WIFI
		ret = 999;
        
        if (gl_SysInfo.szWifiName[0]==0)
        {
            vDispMid(3, "wifi为空,请先设置wifi");
            iOK(5);
            _vDisp(3, "");
        }else
        {
		//if (gl_SysInfo.szWifiName[0])
			ret = wifiInitAndConnAP2(gl_SysInfo.szWifiNameUtf8, gl_SysInfo.szWifiPwd);
        }
	#else
		return -1;
	#endif
	}
        
    startComm=0;
	if(ret==0)
	{
		//网络连接成功则自动同步时间
		sg_szLastSetTime[0]=0;
		iPosSyncTime();
        startComm=1;
        /*
        {
            char buf[20];
            iGetNetExtIp(buf, NULL);
        }
        */
	}

	iGetWirelessSignalQuality();
	vFlushStatusBar();

	sg_iConnFlag=0;
	dbg("iCommLogin ret=%d, startComm=%d.\n", ret, startComm);
    
	if(ret)
	{
        char buf[40];
        _vFlushKey();
		//vMessage("通讯模块初始化失败");
        
        vSetSignalValZero();
        updateLastEventTimestamp();         //更新休眠时间戳
        if(gl_SysInfo.iCommType == VPOSCOMM_GPRS)
        {
            switch(ret)
            {
                case -1:
                    strcpy(buf, "通讯模块无响应");
                    break;
                case -2:
                    strcpy(buf, "SIM卡状态不正常");
                    break;
                case -3:
                    strcpy(buf, "网络注册失败");
                    break;
                case -4:
                    strcpy(buf, "网络附着失败");
                    break;
                case -5:
                    strcpy(buf, "gprs上下文失败");
                    break;
                case -6:
                    strcpy(buf, "APN参数设置失败");
                    break;
                case -7:
                    sprintf(buf, "%s激活失败", NetName);
                    break;
                case -8:
                    strcpy(buf, "tcp接收模式设置失败");
                    break;
                case -9:
                    strcpy(buf, "tcp格式设置失败");
                    break;
                case -99:
                    strcpy(buf, "通讯模块上电失败");
                    break;
                case -100:
                    strcpy(buf, "未检测到SIM卡");
                    break;
                default:
                    sprintf(buf, "通讯模块初始化失败:%d", ret);
                    break;
            }
        }else
        {
		#ifdef	FUDAN_CARD_DEMO
			switch(ret)
            {			
                case -1:
                    strcpy(buf, "wifi module no response.");
                    break;
                case -2:
                    strcpy(buf, "wifi module setting fail.");
                    break;
                case 2:
                    strcpy(buf, "Wifi connect fail.");
                    break;
                default:
                    sprintf(buf, "wifi init fail:%d.", ret);
                    break;
            }
			if(_uiCheckBattery()<=1)
                _vDisp(_uiGetVLines()-1, (uchar*)"battery is low, pls charge");
            _vDisp(_uiGetVLines(), (uchar*)buf);
		#else
            switch(ret)
            {			
                case -1:
                    strcpy(buf, "通讯模块无响应");
                    break;
                case -2:
                    strcpy(buf, "Wifi模块设置失败");
                    break;
                case 2:
                    strcpy(buf, "Wifi连接失败");
                    break;
                default:
                    sprintf(buf, "通讯模块初始化失败:%d", ret);
                    break;
            }
		#endif
        }
        #ifndef NO_WIFI
        if (gl_SysInfo.iCommType!=VPOSCOMM_WIFI || (gl_SysInfo.iCommType==VPOSCOMM_WIFI && gl_SysInfo.szWifiName[0]!=0))
        #endif
        {
            if(sg_uiCommInitEvent==0)
            {
                if(ucGetCommInitDisp())
                {            
                    if(_uiCheckBattery()<=1)
                        vDispMid(_uiGetVLines()-1, "电池电量低,请先充电");

                    vDispMid(3, buf);

                    iGetKeyWithTimeout(5);
                    vDispMid(_uiGetVLines()-1, "");
                    vDispMid(3, "");
                }
            }else
            {
                sg_szWarnBuf[0]=0;
                if(_uiCheckBattery()<=1)
                {
                    memset(sg_szWarnBuf, ' ', _uiGetVCols());
                    memcpy(sg_szWarnBuf, "电池电量低,请先充电", strlen("电池电量低,请先充电"));
                }
                strcat(sg_szWarnBuf, buf);
            }
        }
	}
    sg_uiCommInitEvent=0;
#ifdef JTAG_DEBUG
    dbg("****** iCommLogin time=%lu ******\n", _ulGetTickTime()-tk);
#endif    
	return ret;
}

int iInitComm(void)
{
	sg_iConnFlag=0;
	return iCommLogin();
}

//返回值: 0-成功 >0连接失败(可切换IP重连) <0无卡或初始化错,无法连接
int iCommTcpConn(char *pszSvrIP, char *pszSvrPort, int iTimeOutMs)
{
	int ret;
	int flag=0;

	if (gl_SysInfo.iCommType != VPOSCOMM_GPRS && gl_SysInfo.iCommType != VPOSCOMM_WIFI)
		return (-1);
	if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
	{
		ret=iTcpConnPreProc(0, NULL);
		if(ret==0)
		{
			flag=1;
			iPosSyncTime();
			ret = gprs_at_TcpConnectHostEx((char *)pszSvrIP, (char *)pszSvrPort, iTimeOutMs);
		}
	}
	else //if(gl_SysInfo.iCommType==VPOSCOMM_WIFI)
	{
#ifndef NO_WIFI	
		ret=0;
        if(startComm && wifi_at_MoudleIsActive()==0)
        {
            wifi_at_poweroff();
            startComm=0;
        }
        if(startComm==0)
        {
            dbg("iCommTcpConn...iCommLogin\n");
            ret=iCommLogin();
        }else
            ret=iTcpConnPreProc(0, NULL);
		if(ret==0)
		{
			flag=1;
            iPosSyncTime();
            ret = wifi_at_tcp_connect((char *)pszSvrIP, atoi((char *)pszSvrPort));
		}
#else
	return (-1);
#endif	
	}

	dbg("iCommTcpConn ret=%d.\n", ret);
	sg_iConnFlag=(ret==0)?1:0;
	
	if(flag)		//flag为1时，表示是连接服务时失败,返回非负数,便于调用端切换ip重连
	{
		if(ret<0)
			ret=-1*ret;
	}else
	{
		if(ret>0)
			ret=-1*ret;
	}
	return ret;
}

int iCommTcpSend(unsigned char *psSendData, int uiSendLen)
{
	int ret;
    int iSndLen;
    int len;
	int iSndMaxSize;
    
	if (gl_SysInfo.iCommType != VPOSCOMM_GPRS && gl_SysInfo.iCommType != VPOSCOMM_WIFI)
		return (-1);

	if(gl_SysInfo.iCommType == VPOSCOMM_GPRS)
		iSndMaxSize=GPRS_AT_TCP_RCV_BUFFER_MAX_SIZE;
	else
		iSndMaxSize=WIFI_TCP_RCV_BUFFER_MAX_SIZE;

    _vSetDispDataIcon(3);
    
    iSndLen=0;
    while(iSndLen<uiSendLen)
    {
        len=(uiSendLen-iSndLen>iSndMaxSize)?iSndMaxSize:uiSendLen-iSndLen;
        if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
            ret = gprs_at_TcpSnd(psSendData+iSndLen, len);
        else
		#ifndef NO_WIFI
            ret = wifi_at_tcp_send(psSendData+iSndLen, len);
		#else
			return (-1);
		#endif
        dbg("iCommTcpSend ret=%d, iSndLen=%d, uiSendLen=%u.\n", ret, iSndLen, uiSendLen);
        if(ret<=0)
        {
            if(iSndLen)
                break;
            else
                return ret;
        }
        iSndLen+=ret;
    }
    dbg("iCommTcpSend out. ret=%d.\n", iSndLen);
	return iSndLen;
}

extern int iGetQueckRcv(void);
extern int iGetTcpTAMode(void);
int iCommTcpRecv2(unsigned char *psRecvData, int iExpRecvLen, int iTimeOutMs)
{
	int ret;
    unsigned char *pszBuf;
    unsigned int uiRecvLen;
    unsigned int uiCurrLen;
    tick timeEnd;
    int timems;
        
    pszBuf=psRecvData;
    uiRecvLen=0;
    timeEnd=get_tick()+iTimeOutMs;

    while (uiRecvLen < iExpRecvLen && get_tick()<timeEnd)
	{
        timems=timeEnd-get_tick();        
        
        if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
		{
            if(iGetTcpTAMode())
                uiCurrLen=iExpRecvLen;
            else
                uiCurrLen=(iExpRecvLen - uiRecvLen)>GPRS_AT_TCP_RCV_BUFFER_MAX_SIZE?GPRS_AT_TCP_RCV_BUFFER_MAX_SIZE:(iExpRecvLen - uiRecvLen);
            ret = gprs_at_TcpRcv(pszBuf, uiCurrLen, timems);
            if(ret==0 && iGetQueckRcv()==2)
            {
                dbg("recv continue...\n");
                _vDelay(10);
                continue;
            }
		}else
		{
#ifndef NO_WIFI		
            #if 0
			uiCurrLen=(iExpRecvLen - uiRecvLen)>WIFI_TCP_RCV_BUFFER_MAX_SIZE?WIFI_TCP_RCV_BUFFER_MAX_SIZE:(iExpRecvLen - uiRecvLen);
            #else
            uiCurrLen=iExpRecvLen-uiRecvLen;
            #endif
            if(uiCurrLen<WIFI_TCP_PACKSIZE)
                ret = wifi_at_tcp_recv(pszBuf, uiCurrLen, timems);
            else
                ret =wifi_at_tcp_recvPack(pszBuf, uiCurrLen, timems);
#else
			ret = -1;
#endif
		}
		if (ret <= 0)
		{
			dbg("iCommTcpRecv2 ret=[%d], uiRecvLen=%d\n", ret, uiRecvLen);
            break;
		}

		pszBuf += ret;
		uiRecvLen += ret;
	}
    
    if(uiRecvLen>0)
        ret=uiRecvLen;
    else
        ret = -1;
	dbg("iCommTcpRecv2 ret=%d.\n", ret);
	return ret;
}

/* ret: >0 recv byte num
				<0 recv err
*/
int iCommTcpRecv(unsigned char *psRecvData, int iExpRecvLen, int iTimeOutMs)
{
	int ret;
	if (gl_SysInfo.iCommType != VPOSCOMM_GPRS && gl_SysInfo.iCommType != VPOSCOMM_WIFI)
		return (-1);
#ifdef USE_HIGH_BAUDRATE
	if ((iGetTcpTAMode()==0 && iExpRecvLen > 20*1024+300) || iExpRecvLen<=0)
		return (-1);
#else
    if ((iGetTcpTAMode()==0 && iExpRecvLen > 8*1024+300) || iExpRecvLen<=0)
		return (-1);
#endif    
    _vSetDispDataIcon(3);
    ret = iCommTcpRecv2(psRecvData, iExpRecvLen, iTimeOutMs);
	dbg("iCommTcpRecv ret=%d.\n", ret);
	return ret;
}

int iCommTcpDisConn(void)
{
	int ret;
    
    _vSetDispDataIcon(0);
	if (gl_SysInfo.iCommType != VPOSCOMM_GPRS && gl_SysInfo.iCommType != VPOSCOMM_WIFI)
		return (COM_ERR_COM_TYPE);
	if(sg_iConnFlag==0)
		return 0;
    
    if((sg_iCsqVal&0xFF) == 0)
    {
        iGetWirelessSignalQuality();
    }
		
	if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
		ret = gprs_at_TcpDisconnectHost();
#ifndef NO_WIFI    
	else
		ret = wifi_at_DisconnTcp();
#endif
	sg_iConnFlag=0;
	dbg("iCommTcpDisConn ret=%d.\n", ret);
	return 0;
}

int iCommLogout(int iCommType, int iPowerOff)
{
	int ret;
	
    _vSetDispDataIcon(0);
	if (iCommType == VPOSCOMM_GPRS)
	{
		ret = gprs_at_CloseGprs();
		if (iPowerOff)
        {
#ifndef NO_WIFI            
			//gprs_at_PowerOff();
            gprs_poweroff();
            startComm=0;
#else            
            gprs_hardware_reset();
#endif            
        }
	}
	else
	{   
#ifndef NO_WIFI
		ret = wifi_at_DisconnAp();
		if (iPowerOff)
        {
			wifi_at_poweroff();
            startComm=0;
        }        
#else
		ret = -1;
#endif
	}
    vSetSignalValZero();
	sg_iConnFlag=0;
	return ret;
}

int iGetHostByName2(char *HostName, char *HostAddr)
{
	int i, iLen;

	iLen=strlen(HostName);
	for(i=0; i<iLen; i++)
	{
		if(HostName[i]!='.' && (HostName[i]<'0' || HostName[i]>'9') )
			break;
	}
	if(i>=iLen)
	{
		strncpy(HostAddr, HostName, 15);
		HostAddr[15]=0;
		return 0;
	}

	if (gl_SysInfo.iCommType != VPOSCOMM_GPRS && gl_SysInfo.iCommType != VPOSCOMM_WIFI)
		return (COM_ERR_COM_TYPE);
		
	//域名解析
	if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
	{
		return gprs_at_domain(HostName, HostAddr);
	}else
	{
	#ifndef NO_WIFI
		return wifi_at_domain(HostName, HostAddr);
	#else
		return -1;
	#endif
	}
}

int iGetHostByName(char *HostName, char *HostAddr)
{
	int ret;
	int loop=0;

	do{
		ret=iGetHostByName2(HostName, HostAddr);
		if(ret)
		{
			vDispMidVarArg(3, "解析域名%d...", loop+1);
		}
	}while(ret!=0 && ++loop<gl_SysInfo.ucCommRetryCount);

	return ret;
}

int iGetICCID(char *pszICCID)
{
    pszICCID[0]=0;
    if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
    {
       if(startComm && sg_szICCID[0]==0)
            gprs_at_iccid(sg_szICCID);
        strcpy(pszICCID, sg_szICCID);
    }else
	{
#if 0//def APP_LKL
        strcpy(pszICCID, "12345678901234567890");
#endif
	}
    return 0;
}
int iGetIMEI(char *pszIMEI)
{
    pszIMEI[0]=0;
    if (gl_SysInfo.iCommType == VPOSCOMM_GPRS && sg_szIMEI[0]==0)
        gprs_at_imei(sg_szIMEI);
    
    strcpy(pszIMEI, sg_szIMEI);
    return 0;
}

static ulong sg_ulSignalTime=0;
//取无线信号强度
int iGetWirelessSignalQuality(void)
{
    int val=0;
    
    dbg("iGetWirelessSignalQuality...\n");
    if(sg_csleep)
        return 0;
    if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
    {
		if(startComm)
		{
			if(gprs_at_CheckSIM())
				val=0;
			else
            {
        		val=gprs_at_GetSignalQuality();
                if(val>0)
                    sg_ulSignalTime=get_tick();
            }
		}        
		if(val<0)
			val=0;
        sg_iCsqVal=0x2000+val;
        
        dbg("iGetWirelessSignalQuality gprs/4g:%04X,%d\n", sg_iCsqVal, sg_iCsqVal);
        return sg_iCsqVal;
    }else if (gl_SysInfo.iCommType == VPOSCOMM_WIFI)
    {
    #ifndef NO_WIFI
		val=0;
		if(startComm)
        {
        	wifi_at_QueryAPConnStatus(NULL, &val);
            if(val==0)
                wifi_at_QueryAPConnStatus(NULL, &val);
        }
        sg_iCsqVal=0x1000+val;
        
        dbg("iGetWirelessSignalQuality wifi:0x%04X,%d\n", sg_iCsqVal, sg_iCsqVal);
        return sg_iCsqVal;
	#else
		return -1;
	#endif
    }
    return 0;
}

void vSetCommSleep(char sleep)
{
    sg_csleep=sleep;
}

int iGetWirelessSinalVal(void)
{
    return sg_iCsqVal;
}

int iGetWirelessSignalVal(void)
{
    return sg_iCsqVal;
}

int iGet4GSignalForTrans(void)
{
    if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
    {
        if(sg_ulSignalTime+2*60*1000>get_tick())
            iGetWirelessSignalQuality();
        if(sg_iCsqVal>0x2000)
        {
            return sg_iCsqVal-0x2000;
        }
    }
    return 0;
}

void vSetWirelessSignalVal(int val)
{
    sg_iCsqVal=val;
}

void vSetSignalValZero(void)
{
    if (gl_SysInfo.iCommType == VPOSCOMM_WIFI)
        sg_iCsqVal=0x1000;
    else if (gl_SysInfo.iCommType == VPOSCOMM_GPRS)
        sg_iCsqVal=0x2000;
    else
        sg_iCsqVal=0;
}

int iWifiFacTest(char *pszWifiVer, int iOutSize)
{
#ifndef NO_WIFI
	int ret;
    
    if(pszWifiVer && iOutSize>0)
        pszWifiVer[0]=0;
	dbg("Before wifi_init \n");
	ret=wifi_appLayer_Init();
    if(ret!=0)
        return 1;
    ret=wifi_at_gmr(pszWifiVer, iOutSize);
    if(ret)
        return 2;
	return 0;
#else
	return 0;
#endif
}

int iGprsFacTest(char *pszIMEI, char *pszICCID)
{
	int ret;
//	int loop;

    if(pszIMEI)
        pszIMEI[0]=0;
    if(pszICCID)
        pszICCID[0]=0;
	ret=gprs_at_poweron();
	if (ret < 0)
	{
		dbg("gprs_at_DisableEcho error\n");
		return -99;
	}
	
	ret = gprs_at_DisableEcho();
	if (ret < 0)
	{
		dbg("gprs_at_DisableEcho error\n");
		return -1;
	}
    
    if(pszIMEI)
    {
        ret=gprs_at_imei(pszIMEI);
        if(ret)
            return -2;
    }
    
	ret = gprs_at_CheckSIM();
	if (ret)
	{
		dbg("gprs_at_CheckSIM error\n");
		return 1;
	}

	if(pszICCID)
	{
		ret=gprs_at_iccid(pszICCID);
		if(ret)
			return 2;
	}

	return 0;
}
/*
int iGetLACandCell(char *pszLAC, char *pszCellId)
{
	char lac[10], cellid[10];

	if(gl_SysInfo.iCommType!=VPOSCOMM_GPRS)
	{
		if(pszLAC)
			pszLAC[0]=0;
		if(pszCellId)
			pszCellId[0]=0;
		return 1;
	}

	if(pszLAC==NULL || ulCellTime==0 || ulCellTime+gl_SysInfo.uiPositTime*60*1000L>=get_tick())
	{
		if(gprs_at_GetLACandCI(lac, cellid)==0)
		{
				ulCellTime=get_tick();
				strcpy(sg_szLAC, lac);
				strcpy(sg_szCellId, cellid);
		}
	}

	if(pszLAC)
		strcpy(pszLAC, sg_szLAC);
	if(pszCellId)
		strcpy(pszCellId, sg_szCellId);

	return 0;
}
*/
int iGetGprsCellInfo(char *pszCellInfo, char *pszCellLoc)
{
	char cell[30];

    if(pszCellInfo)
        pszCellInfo[0]=0;
    if(pszCellLoc)
        pszCellLoc[0]=0;
    
	if(gl_SysInfo.iCommType!=VPOSCOMM_GPRS)
	{
		if(pszCellInfo && sg_szCellInfo[0])
        {
            strcpy(pszCellInfo, sg_szCellInfo);
            return 0;
        }
		return 1;
	}

	if(pszCellInfo==NULL || ulCellTime==0 || ulCellTime+gl_SysInfo.uiPositTime*60*1000L>=get_tick())
	{
		if(gprs_at_GetCellInfo(cell)==0)
		{
				ulCellTime=get_tick();
            
            #if 0       //使用十进制数据
            {
                char mcc[5], mnc[5], lac[12], ci[12];
                sscanf(cell, "%[^,],%[^,],%[^,],%s", mcc, mnc, lac, ci);
                sprintf(sg_szCellInfo, "%s,%s,%lu,%lu", mcc, mnc, ulHexToLong((uchar*)lac, strlen(lac)), ulHexToLong((uchar*)ci, strlen(ci)));
                dbg("HexCell:[%s], DecCell:[%s]\n", cell, sg_szCellInfo);
            }
            #else
				strcpy(sg_szCellInfo, cell);
            #endif
		}
#ifdef APP_LKL
        if(pszCellLoc && gprs_at_location(cell)==0)
		{
				//ulCellTime=get_tick();
				strcpy(sg_szCellLoc, cell);
		}
#endif        
	}

	if(pszCellInfo)
		strcpy(pszCellInfo, sg_szCellInfo);
#ifdef APP_LKL
    if(pszCellLoc)
        strcpy(pszCellLoc, sg_szCellLoc);
#endif    
	return 0;
}

int iGetLocalIP(char *ip)
{
    if(sg_szLocalIP[0])
    {
        strcpy(ip, sg_szLocalIP);
        return 0;
    }else
    {
        ip[0]=0;
        return 1;
    }
}

int gprs_at_testgprssleep(void)
{
	char recv[200];
	int ret;
	char *p0, *p1;
    int line;

	_vCls();
	_vDisp(1, "get gprs stat");
	_uiGetKey();

	line=2;
	_vDisp(line++, "AT+QISTATE");
	ret = gprs_at_connState(recv);
	vDispVarArg(line++, "ret:%d", ret);
	_vDisp(line++, recv);
	_uiGetKey();
	vClearLines(2);

	line=2;
	_vDisp(line++, "AT+CREG?");
	ret = gprs_at_cmd_sndrcv("AT+CREG?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv));
	vDispVarArg(line++, "ret:%d", ret);
	_vDisp(line++, recv);
	if(strlen(recv)>_uiGetVLines())
	{
		_vDisp(line++, recv+_uiGetVLines());
	}
	p0=strstr(recv, "+CREG:");
	if(p0)
		_vDisp(line++, p0);
	_uiGetKey();
	vClearLines(2);

	line=2;
	_vDisp(line++, "AT+CGATT?");
	ret = gprs_at_cmd_sndrcv("AT+CGATT?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv));
	vDispVarArg(line++, "ret:%d", ret);
	_vDisp(line++, recv);
	if(strlen(recv)>_uiGetVLines())
	{
		_vDisp(line++, recv+_uiGetVLines());
	}
	p0=strstr(recv, "+CGATT:");
	if(p0)
		_vDisp(line++, p0);
	_uiGetKey();
	vClearLines(2);

	line=2;
	_vDisp(line++, "AT+QIREGAPP=?");
	ret = gprs_at_cmd_sndrcv("AT+QIREGAPP=?\r", "OK\r", 1000, 1, 1, recv, sizeof(recv));
	vDispVarArg(line++, "ret:%d", ret);
	_vDisp(line++, recv);
	if(strlen(recv)>_uiGetVLines())
	{
		_vDisp(line++, recv+_uiGetVLines());
	}
	p0=strstr(recv, "+QIREGAPP:");
	if(p0)
		_vDisp(line++, p0);
	_uiGetKey();
	vClearLines(2);

	line=2;
	_vDisp(line++, "AT+QENG=? ");
	//开工程模式
	ret = gprs_at_cmd_sndrcv("AT+QENG=1,0\r", "OK\r", 500, 1, 1, NULL, 0);	
	if (ret)
		return ret;
	//取工程模式信息
	ret = gprs_at_cmd_sndrcv("AT+QENG?\r", "OK\r", 2000, 1, 1, recv, sizeof(recv));
	//还原设置
	gprs_at_cmd_sndrcv("AT+QENG=0\r", "OK\r", 500, 1, 1, NULL, 0);
	vDispVarArg(line++, "ret:%d", ret);
	_vDisp(line++, recv);
	p1=recv;
	p0=strstr(p1, "+QENG:");
	if(p0)
	{
		_vDisp(line++, p0);
	
		p1+=10;
		p0=strstr(p1, "+QENG:");
		if(p0)
			_vDisp(line++, p0);
	}
	_uiGetKey();
	vClearLines(2);	

	return ret;
}

//tcp连接前先判断gprs/wifi状态。若网络断开则重连
int iTcpConnPreProc(char cTipFlag, char *pcDispUpFlag)
{
	char recv[100];
	int ret=0;
    char upFlag;
    char *pcUpFlag;
    
    if(pcDispUpFlag==NULL)
        pcUpFlag=&upFlag;
    else
        pcUpFlag=pcDispUpFlag;
    *pcUpFlag=0;
#if 1
    if(startComm==0)
    {
        vSetSignalValZero();
        *pcUpFlag=1;
        if(cTipFlag)
        {
            _vCls();
            vDispMid(3, "网络初始化,请稍候...");            
        }
        dbg("iTcpConnPreProc...iCommLogin\n");
        ret = iCommLogin();
        if(cTipFlag)
            _vDisp(3, "");
        return ret;
    }
#ifndef NO_WIFI    
    if(gl_SysInfo.iCommType==VPOSCOMM_WIFI)
    {
        #if 1        
        ret=wifi_at_connStatus();
        if(ret>=2 && ret<5)
            return 0;
        if(ret==5 && gl_SysInfo.szWifiName[0])
        {
            vSetSignalValZero();
            if(cTipFlag==0)
            {
                _vCls();
                vDispMid(3, "重新连接wifi...");            
            }
            dbg("iTcpConnPreProc...wifi iCommLogin\n");
            ret = iCommLogin();
            if(cTipFlag==0)
                _vDisp(3, "");
        }else
            ret=-1;
        return ret;
        #else
        return 0;
        #endif
    }
#endif   
    ret=gprs_at_CheckSIMQuick();
    if(ret==0)
        ret = gprs_at_connState(recv);
	if(ret)
    {
        *pcUpFlag=1;
        if(ret==-2)
        {            
            if(cTipFlag)
            {
                _vCls();
                vDispMid(3, "网络初始化,请稍候...");                
            }
            
            //gprs_poweroff();
            gprs_hardware_reset();
            startComm=0;
            dbg("iTcpConnPreProc...gprs iCommLogin\n");
            ret = iCommLogin();
            if(cTipFlag)
                _vDisp(3, "");
            return ret;
        }
		if(ret==-100)
		{
			vMessage("未检测到SIM卡");
		}
		return -1;
    }
	/*
	值:
	IP INITIAL, IP START, IP CONFIG, IP IND, IP GPRSACT, IP STATUS,
	TCP CONNECTING/UDP CONNECTING, IP CLOSE, CONNECT OK, PDP DEACT
	说明:
	初始为IP INITIAL, regapp后变为IP START, act后变为IP GPRSACT或IP CONFIG, 取本地ip后变为IP STATUS
	tcp连接时为TCP CONNECTING, 连接后变为CONNECT OK，关闭tcp后变为IP CLOSE
	gprs外部关闭后变为PDP DEACT， 需要closegprs转为IP INITIAL的状态, regapp和act后才能进行tcp连接
	*/
	if(strstr(recv, "GPRSACT") || strstr(recv, "IP CLOSE") || strstr(recv, "IP CONFIG") || strstr(recv, "IP STATUS"))
		return 0;

	if(strstr(recv, "CONNECT"))	//TCP CONNECTING 或者 CONNECT OK 
	{
		//先断开网络?
		return 0;
	}

	//检查gprs附着, 若附着不为1,需要重启gprs
	ret=gprs_at_CheckGprsAttachment();
	if(ret!=0)
	{
        *pcUpFlag=1;
        if(cTipFlag)
        {
            _vCls();
            cTipFlag=0;
        }
		vDispMid(3, "网络连接中,请稍候...");
		//gprs_poweroff();
        gprs_hardware_reset();
        dbg("iTcpConnPreProc...gprs iCommLogin2\n");
		ret=iCommLogin();
        _vDisp(3, "");
		return ret;
	}

	//关闭gprs,再重新reg和act
	//if(strstr(recv, "PDP DEACT"))
	{
        *pcUpFlag=1;
        if(cTipFlag)
        {
            _vCls();
            cTipFlag=0;
        }
		_vDisp(3, "网络连接中,请稍候...");
		gprs_at_CloseGprs();
		ret=gprs_at_InitPDP();
        _vDisp(3, "");
	}
#endif
	return ret;
}

int iTestPowerOffGprs(void)
{
	gprs_poweroff();
	return 0;
}

int iTestCloseGprs(void)
{
	gprs_at_CloseGprs();
	return 0;
}

int iTestGprsInit(void)
{
	int ret;

	_vDisp(3, "重新连接gprs...");
	ret=gprsInit();
	if(ret)
	{
		vDispVarArg(_uiGetVLines()-1, "init:%d", ret);
		vMessage("");
	}
	return 0;
}

int iTestGprsRegApp(void)
{
    extern int gprs_at_RegApp(void);
    
	int ret;

	ret=gprs_at_RegApp();
	if(ret)
	{
		vDispVarArg(_uiGetVLines()-1, "regapp:%d", ret);
		vMessage("");
	}
	return 0;
}

int iTestGprsAct(void)
{
	int ret;

	ret=gprs_at_actgprs(1, 10);
	if(ret)
	{
		vDispVarArg(_uiGetVLines()-1, "actgprs:%d", ret);
		vMessage("");
	}	
	return 0;
}

//113.921443,22.551855
int iTestGprsLoc(void *p)
{
	int ret;
	char loc[30];

	_vCls();
	//ret=gprs_at_location(loc);
    ret=gprs_at_GetCellInfo(loc);
	if(ret==0)
	{
		vDispCenter(3, loc, 0);
	}else
		vDispCenter(3, "获取位置失败", 0);
	_uiGetKey();

	gprs_at_location(loc);
	_uiGetKey();
    return 0;
}

int iTestSim(void *p)
{
    extern int gprs_at_CheckSIM2(char *siminof);
    
	int ret;
	char sim[20]={0};

	_vCls();
	ret=gprs_at_CheckSIM2(sim);
	if(ret)
		vDispCenter(3, "获取sim信息失败", 0);
	else
		vDispVarArg(3, "sim:[%s]", sim);
	_uiGetKey();
	return 0;
}

int iSetApnParam(int *p)
{
    char apn[20], user[20], pwd[20];
    int ret;
	int flag=0;

	char *apszAlphaTable[10] = {
		"0.!@#$%^&*()-=_+;':,/<>?`~",
		"1qzQZ",
		"2abcABC",
		"3defDEF",
		"4ghiGHI",
		"5jklJKL",
		"6mnoMNO",
		"7prsPRS",
		"8tuvTUV",
		"9wxyWXY"};

	vSetAlphaTable((uchar **)apszAlphaTable);

    _vCls();
    vDispCenter(1, "APN参数设置", 1);
    vDispCenter(_uiGetVLines(), "(若参数空,可直接确认)", 0);
#if defined(ST7571) || defined(ST7567)
    strcpy(apn, gl_SysInfo.szApn);
    _vDisp(2, "APN :                ");
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL, 2, 5, apn, 16, 0, 16, 60);
    if(ret<0)
        return 1;

    strcpy(user, gl_SysInfo.szApnUser);
    _vDisp(3, "用户:                ");
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL, 3, 5, user, 16, 0, 16, 60);
	if(ret<0)
        return 1;

    strcpy(pwd, gl_SysInfo.szApnPwd);
    _vDisp(4, "密码:                ");
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL, 4, 5, pwd, 16, 0, 16, 60);
    if(ret<0)
        return 1;
#else
    strcpy(apn, gl_SysInfo.szApn);
    _vDisp(2, "APN名称:");
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL, 3, 2, apn, 16, 0, 16, 60);
    if(ret<0)
        return 1;
    
    vClearLines(2);
    strcpy(user, gl_SysInfo.szApnUser);
    _vDisp(2, "APN用户:");
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL, 3, 2, user, 16, 0, 16, 60);
	if(ret<0)
        return 1;

    vClearLines(2);
    strcpy(pwd, gl_SysInfo.szApnPwd);
    _vDisp(2, "APN密码:");
    ret=iInput(INPUT_ALPHA|INPUT_INITIAL, 3, 2, pwd, 16, 0, 16, 60);
    if(ret<0)
        return 1;
#endif
	if(strcmp(apn, gl_SysInfo.szApn))
    {
        strcpy(gl_SysInfo.szApn, apn);
		flag=1;
    }
	if(strcmp(user, gl_SysInfo.szApnUser))
    {
        strcpy(gl_SysInfo.szApnUser, user);
		flag=1;
    }
    if(strcmp(pwd, gl_SysInfo.szApnPwd))
    {
        strcpy(gl_SysInfo.szApnPwd, pwd);
		flag=1;
    }

	if(flag)
	{
		uiMemManaPutSysInfo();
		if(gl_SysInfo.iCommType == VPOSCOMM_GPRS /* && gprs_at_QueryActStatus() */ )
		{
			_vCls();
			_vDisp(2, "apn修改,重新设置网络...");

			//gprs_at_CloseGprs();		//deact
			gprs_at_SetAPN(gl_SysInfo.szApn, gl_SysInfo.szApnUser, gl_SysInfo.szApnPwd);
			iInitComm();
		}
	}
    return 0;
}

//设置通讯时的回调函数,注意不可设置较耗时的操作,避免通讯模块数据丢失
void vSetCommCallBack(void (*func)(void))
{
    gl_CommCallbackFunc=func;
}

//休眠唤醒后发一条AT指令给模块,兼容需要指令唤醒的模块
void vCommModuleWakeUpAt(void)
{
    char send[10];
    //未休眠直接退出
    if(sg_csleep==0)
        return;
    
    if(gl_SysInfo.iCommType==VPOSCOMM_GPRS)
    {
        strcpy(send, "AT\r");
        gprs_write(send, strlen(send));    
        dbg("gprs_write[%d] = [%s]\n", strlen(send), send);
        mdelay(10);
    }
#ifndef NO_WIFI    
    if(gl_SysInfo.iCommType==VPOSCOMM_WIFI)
    {
        /*
        strcpy(send, "AT\r\n");
        wifi_write(send, strlen(send));    
        dbg("wifi_write[%d] = %s\n", strlen(send), send);
        mdelay(10);
        */
    }
#endif
    return;
}

void vNetSleepProc(void)
{
#ifndef NO_WIFI
    if(gl_SysInfo.iCommType==VPOSCOMM_WIFI)
    {
        wifi_at_poweroff();
        startComm=0;
    }
#endif    
}

int iNetWakeUpProc(void)
{
#ifndef NO_WIFI
    if(gl_SysInfo.iCommType==VPOSCOMM_WIFI && gl_SysInfo.szWifiNameUtf8[0])
    {
        _vCls();
        vSetSignalValZero();
        vFlushStatusBar();
        iInitComm();
        return 1;
    }
#endif

    return 0;
}

int iGetCommInitStatus(void)
{
    dbg("startComm=%d\n", startComm);
    return startComm;
}

void vSetCommInitStatus(int status)
{
    startComm=status;
}

//从http://pv.sohu.com/cityjson?ie=gbk获取ip地址
//pszIp[15+1]:   输出参数, IP地址
//pszCity[40+1]: 输出参数, 城市名称, 可NULL
int iGetNetExtIp(char *pszIp, char *pszCity)
{
    int ret;
    char *p0, *p1;
    char send[200]="GET /cityjson?ie=gbk HTTP/1.1\r\n"
                   "Host: pv.sohu.com\r\n\r\n";
    char recv[500]={0};
    uint uiLen=0;
    
    pszIp[0]=0;
    if(pszCity)
        pszCity[0]=0;
    
    vDispMid(3, "获取网络信息");
    ret=iCommTcpConn("pv.sohu.com", "80", 2*1000);
    if(ret)
        return -1;

    ret=iCommTcpSend((uchar*)send, strlen(send));
    if(ret<=0)
        return 1;

    //ret=iCommTcpRecv((uchar*)recv, sizeof(recv)-1, 2*1000);
    ret=iHttpRecv(0, (uchar*)recv, sizeof(recv), &uiLen, 2, NULL);
    iCommTcpDisConn();
    if(ret==0 && uiLen>0)
    {
        recv[uiLen]=0;
        dbg("recv(%d)=[%s]\n", strlen(recv), recv);
        
        p0=strstr(recv, "\"cip\"");
        if(p0==NULL)
        {
            return 2;
        }
        
        p1=p0+strlen("\"cip\"")+1;
        p0=strchr(p1, '"');
        if(p0==NULL)
        {
            return 3;
        }
        
        p0++;
        p1=strchr(p0, '"');
        if(p1==NULL)
        {
            return 4;
        }
        if(p1-p0<=15)
            vMemcpy0(pszIp, p0, p1-p0);
        
        if(pszCity)
        {
            p0=strstr(recv, "\"cname\"");
            if(p0==NULL)
                return 2;

            p1=p0+strlen("\"cname\"")+1;
            p0=strchr(p1, '"');
            if(p0==NULL)
                return 3;
            
            p0++;
            p1=strchr(p0, '"');
            if(p1==NULL)
                return 4;
            if(p1-p0<=40)
                vMemcpy0(pszCity, p0, p1-p0);
        }
    }
    dbg("ext ip:[%s], cityname:[%s]\n", pszIp, pszCity==NULL?"null":pszCity);
    
    return 0;
}

int iGetLocalPosition(void)
{
	int ret=1;

#ifdef JTAG_DEBUG    
	ulong tk=_ulGetTickTime();
#endif    

	ret = gprsInit();
        
	if(ret==0)
	{
            gprs_at_iccid(sg_szICCID);
            gprs_at_imei(sg_szIMEI);
            
		//iGetLACandCell(NULL, NULL);
		iGetGprsCellInfo(NULL, NULL);
  
    #if (defined(JTAG_DEBUG) && defined(USE_LTE_4G))
			gprs_at_QFENG(NULL);	//test
    #endif
	}

	iGetWirelessSignalQuality();
	flushStatusBar(1);

	dbg("iCommLogin ret=%d.\n", ret);

	if(ret)
	{
        char buf[40];
        _vFlushKey();
		//vMessage("通讯模块初始化失败");
        
        vSetSignalValZero();
        updateLastEventTimestamp();         //更新休眠时间戳

            switch(ret)
            {
                case -1:
                    strcpy(buf, "通讯模块无响应");
                    break;
                case -2:
                    strcpy(buf, "SIM卡状态不正常");
                    break;
                case -3:
                    strcpy(buf, "网络注册失败");
                    break;
                case -4:
                    strcpy(buf, "网络附着失败");
                    break;
                case -5:
                    strcpy(buf, "gprs上下文失败");
                    break;
                case -6:
                    strcpy(buf, "APN参数设置失败");
                    break;
                case -7:
                    sprintf(buf, "%s激活失败", NetName);
                    break;
                case -8:
                    strcpy(buf, "tcp接收模式设置失败");
                    break;
                case -9:
                    strcpy(buf, "tcp格式设置失败");
                    break;
                case -99:
                    strcpy(buf, "通讯模块上电失败");
                    break;
                case -100:
                    strcpy(buf, "未检测到SIM卡");
                    break;
                default:
                    sprintf(buf, "通讯模块初始化失败:%d", ret);
                    break;
            }
			  
		 vDispMid(_uiGetVLines(), (uchar*)buf);
		 iGetKeyWithTimeout(5);        
	        _vDisp(_uiGetVLines()-1, (uchar*)"");
		return ret;
	}
	
#ifdef JTAG_DEBUG
    dbg("****** iCommLogin time=%lu ******\n", _ulGetTickTime()-tk);
#endif    

        //基站信息
        memset(gl_baseStation,0,sizeof(gl_baseStation));
	gprs_at_GetCellInfo(gl_baseStation);
	dbgHex("gl_baseStation",gl_baseStation,strlen(gl_baseStation));
	
	return ret;
}
