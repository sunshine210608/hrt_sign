/**************************************
File name     : VPOSFACE.H
Function      : Define VPOS interface
**************************************/
# ifndef _VPOSFACE_H
# define _VPOSFACE_H

#include "user_projectconfig.h"

# define VPOS_H_VER  0x0400
# define VPOS_DEBUG

# ifndef uint
# define uint unsigned int
# endif

# ifndef uchar
# define uchar unsigned char
# endif

# ifndef ulong
# define ulong unsigned long
# endif

# ifndef ushort
# define ushort unsigned short
# endif

# define POS_TYPE_MASK              0xFF00

# define POS_TYPE_PC                0x0100
# define POS_TYPE_CY20              0x0200
# define POS_TYPE_CY21              0x0201

// for display attribute
# define DISP_NORMAL                0x00
# define DISP_REVERSE               0x01
# define DISP_UNDERLINE             0x02
# define DISP_OVERLAP               0x04

// define keyboard code
# define _KEY_ENTER                 0x0d
# define _KEY_BKSP                  0x08
# define _KEY_ESC                   0x1b
# define _KEY_CANCEL                0x1b
# define _KEY_DOT                   '.'
# define _KEY_WELL                  '#'
# define _KEY_STAR                  '*'

# define _KEY_0                     '0'
# define _KEY_1                     '1'
# define _KEY_2                     '2'
# define _KEY_3                     '3'
# define _KEY_4                     '4'
# define _KEY_5                     '5'
# define _KEY_6                     '6'
# define _KEY_7                     '7'
# define _KEY_8                     '8'
# define _KEY_9                     '9'

# define _KEY_F1                    0xF1
# define _KEY_F2                    0xF2
# define _KEY_F3                    0xF3
# define _KEY_F4                    0xF4
# define _KEY_F5                    0xF5
# define _KEY_F6                    0xF6
# define _KEY_F7                    0xF7
# define _KEY_F8                    0xF8
# define _KEY_F9                    0xF9
# define _KEY_F10                   0xFA
# define _KEY_F11                   0xFB
# define _KEY_F12                   0xFC
# define _KEY_OFF                   0xFE
# define _KEY_OTHER                 0xFF

# define _KEY_UP                    0xE0
# define _KEY_DOWN                  0xE1
# define _KEY_LEFT                  0xE2
# define _KEY_RIGHT                 0xE3
# define _KEY_NEXT                  0xE4
# define _KEY_UP1                   0xE5
# define _KEY_UP2                   0xE6
# define _KEY_UP3                   0xE7
# define _KEY_UP4                   0xE8
# define _KEY_FN1                   0xE9
# define _KEY_FN2                   0xEA
# define _KEY_FN3                   0xEB
# define _KEY_FN4                   0xEC
# define _KEY_MENU                  0xED
# define _KEY_00                    0xEE
# define _KEY_FN                    0xEF

#define MEM_SYSINFO     1
#define MEM_SYSDATA     2
#define MEM_OPER        3
#define MEM_CAPUBKEY    4
#define MEM_AID         5
#define MEM_REV8583     6
#define MEM_SCR8583     7
#define MEM_QRTRANSREC  8
#define MEM_SETTLEINFO  9
#define MEM_CARDBLACK     15

//定义5个交易记录文件, id需连续
#define MEM_TRANSREC_0    10
#define MEM_TRANSREC_1    11
#define MEM_TRANSREC_2    12
#define MEM_TRANSREC_3    13
#define MEM_TRANSREC_4    14

#define TRANS_FILE_SIZE   200   //200条记录一个文件
#define TRANS_FILE_NUM    5     //总共5个记录文件, 1000条记录

// 打印机
#define PRT_STATUS_OK				0 // OK
#define PRT_STATUS_NO_PAPER			1 // 缺纸
#define PRT_STATUS_LESS_PAPER		2 // 纸少
#define PRT_ERROR_NO_PRINTER		3 // 没发现打印机
#define PRT_ERROR_OFFLINE			4 // 打印机脱机
#define PRT_ERROR_BUSY  			5 // 打印机忙
#define PRT_ERROR_BATLOW  			6 // 终端电池电量低
#define PRT_ERROR					9 // 故障
#define PRT_ERROR_4G_MODULE			10//CY20P LITE项目中，打印机和4G模块共用端口

// PINPAD与安全模块
#define SEC_STATUS_OK				0 // OK
#define SEC_STATUS_TIMEOUT			1 // 超时
#define SEC_STATUS_CANCEL			2 // 用户取消
#define SEC_STATUS_BYPASS			3 // 用户Bypass
#define SEC_ERROR_ALGO				4 // 算法不支持
#define SEC_ERROR_INDEX				5 // 索引不支持
#define SEC_ERROR_NO_KEY			6 // 密钥不存在
#define SEC_NO_DEVICE				7 // 无安全模块
#define SEC_ERROR					9 // 故障

// LED灯
#define LED_COLOR_BLACK             0 // 黑色
#define LED_COLOR_RED               1 // 红色
#define LED_COLOR_GREEN             2 // 绿色色
#define LED_COLOR_BLUE              4 // 蓝色
#define LED_COLOR_YELLOW            3 // 黄色色
#define LED_COLOR_MAGENTA           5 // 洋红色
#define LED_COLOR_CYAN              6 // 青色
#define LED_COLOR_WHITE             7 // 白色
#define LED_COLOR_ON                LED_COLOR_RED   // 默认红色
#define LED_COLOR_OFF               LED_COLOR_BLACK // 默认黑色

// for des use
# define ENCRYPT                    1
# define DECRYPT                    2
# define TRI_ENCRYPT                3
# define TRI_DECRYPT                4
# define TRI3_ENCRYPT               5
# define TRI3_DECRYPT               6

// for communication
# define COMM_NONE                  0
# define COMM_EVEN                  1
# define COMM_ODD                   2

# define COMM_SYNC                  1
# define COMM_ASYNC                 2
# define COMM_CCITT                 1
# define COMM_BELL                  2

# define TEL_OK                     0x0000
# define TEL_PARAMETER              0x0100

# define TEL_STATUS_WAIT            0x0200
# define TEL_STATUS_BUSY            0x0201
# define TEL_STATUS_TIMEOUT         0x0202

# define TEL_ERROR_NO_DEVICE        0x0300
# define TEL_ERROR_NO_TONE          0x0301
# define TEL_ERROR_NO_CARRY         0x0302
# define TEL_ERROR_NO_HDLC          0x0303
# define TEL_ERROR_NO_CONNECT       0x0304
# define TEL_ERROR_NO_ANSWER        0x0305
# define TEL_ERROR_NO_LINE          0x0306
# define TEL_ERROR_CANCEL           0x0398
# define TEL_ERROR_UNKNOWN          0x0399

// 网络通讯
# define VPOSCOMM_WIFI              1
# define VPOSCOMM_GPRS              2
# define VPOSCOMM_ETHER             3

// 网络通讯返回码
# define COM_OK                     0x0000 // 成功
# define COM_PARAMETER              0x0100 // 参数错误
# define COM_STATUS_PROCESSING      0x0200 // 操作进行中
# define COM_STATUS_BUZY            0x0201 // 线路忙
# define COM_STATUS_TIMEOUT         0x0202 // 超时
# define COM_ERR_DEVICE             0x0300 // 设备故障或无此设备
# define COM_ERR_NO_CONNECT         0x0301 // 没有连接
# define COM_ERR_COM_TYPE           0x0302 // 通讯类型不支持
# define COM_ERR_NO_REGISTER        0x0303 // 还没有注册网络
# define COM_ERR_SEND               0x0304 // 发送失败
# define COM_ERR_RECV               0x0305 // 接收失败
# define COM_ERR_CANCEL             0x0398 // 用户取消
# define COM_ERR_UNKNOWN            0x0399 // 未知错误
# define COM_ERR_PACK               0x0401  //打包失败
# define COM_ERR_UNPACK             0x0402  //解包失败
# define COM_ERR_RESEND             0x0403  //冲正或重发报文处理失败
# define COM_ERR_RECV_HTTPHEAD      0x0404  //Http响应报文失败，报文头非200或206
# define COM_ERR_ENCRYPT			0x0405  //报文加密失败或证书无效

// IC卡协议类型
# define APDU_NORMAL                0      // 标准APDU, 不自动处理61xx、6Cxx
# define APDU_AUTO_GET_RESP         1      // 标准APDU, 自动处理61xx、6Cxx
# define APDU_EMV                   0x80   // 执行EMV协议
# define APDU_SI                    0x81   // 执行社保2.0协议


#define CHK_HAVE_CAM		0x01
#define CHK_HAVE_WIFI		0x02
#define CHK_HAVE_GPRS		0x03        //GPRS或4G模块
#define CHK_HAVE_TP         		0x04        //触摸屏
#define CHK_HAVE_EXTLED    	0x05        //客显屏
#define CHK_HAVE_BATTERY    	0x06        //电池

#pragma pack(1)
typedef struct {
    uchar sCommand[4];
    uchar ucLengthIn;
    uchar sDataIn[256];
    uchar ucLengthExpected;
} APDU_IN;
typedef struct {
    uchar  ucLengthOut;
    uchar  sDataOut[256];
    ushort uiStatus;
} APDU_OUT;

typedef struct {
    ulong ulBaud;      // 1200, 2400, 4800, 9600
    uchar ucParity;    // COMM_NONE, COMM_ODD, COMM_EVEN
    uchar ucBits;      // 7, 8
    uchar ucStop;      // 1
    uchar szPABX[5];   // 外线号码, 不需要加逗号
    uchar szTelNo[15]; // example: "07553259571", "3237496"
    uchar ucType;      // COMM_CCITT, COMM_BELL
    uchar ucMode;      // COMM_SYNC, COMM_ASYNC
} TEL_CONFIG;

typedef struct {
//	uint  uiCommType;           //网络类型 VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_WIFI
//	uchar szLocalIP[16];	    //本机地址      ETHERNET	GPRS	WIFI
//	uchar szSubNetMask[16];	    //子网掩码      ETHERNET	GPRS	WIFI
//	uchar szGateWay[16];	    //网关地址      ETHERNET	GPRS	WIFI
//	uchar szPriDNS[16];		    //主DNS地址     ETHERNET	GPRS	WIFI
//	uchar szSecDNS[16];		    //次DNS地址     ETHERNET	GPRS	WIFI
//	uchar szApn[64];		    //APN                       GPRS
//	uchar szPhoneNumber[32];    //拨号号码                  GPRS    
	uchar szUserName[32];	    //登录用户名                GPRS	WIFI
	uchar szUserPwd[32];	    //登录密码                  GPRS        WIFI
} VPOSCOMM_CONFIG; // 网络通讯参数

typedef struct {
    uchar szLocalIP[16];	    //本机地址      ETHERNET    GPRS    CDMA    WIFI
    uchar szSubNetMask[16];	    //子网掩码      ETHERNET    GPRS    CDMA    WIFI
    uchar szGateWay[16];	    //网关地址      ETHERNET    GPRS    CDMA    WIFI
    uchar szMyPhID[20];		    //本机电话号码              GPRS    CDMA
    uchar szSignal[4];			//信号强度(0-100)           GPRS    CDMA    WIFI
} VPOSCOMM_INFO;   // 网络信息
#pragma pack()

#ifdef __cplusplus
extern "C" {
#endif

// 1.   系统管理函数

// 1.1. 初始化POS系统，只能在程序起始处调用一次。
void _vPosInit(void);

// 1.2. POS停机，不同的POS可能的表现有：下电、重启动、仅停止运行。
void _vPosClose(void);

// 1.3. 取得POS型号
// 返    回：POS型号
uint _uiGetPosType(void);

// 2.   显示

// 2.1. 取屏幕可显示行数
// 返    回：屏幕可显示行数
uint _uiGetVLines(void);

// 2.2. 取屏幕可显示列数
// 返    回：屏幕可显示列数
uint _uiGetVCols(void);

// 2.3. 清除整个屏幕
void _vCls(void);

// 2.4. 在某行显示字符串
// 输入参数：uiLine  : 要显示的行数，从第一行开始
//           pszText : 要显示的字串
// 说    明：如不存在该行，立即返回。如字串长度超出屏幕宽度，截去超出的部分。
void _vDisp(uint uiLine, const uchar *pszText);

// 2.5. 在某行显示某属性的字符串
// 输入参数：uiLine  : 要显示的行数，从第一行开始
//           pszText : 要显示的字串
//           ucAttr  : 显示属性，可以是DISP_NORMAL、DISP_REVERSE、DISP_UNDERLINE的组合
// 说    明：如不存在该行，立即返回。如字串长度超出屏幕宽度，截去超出的部分。
void _vDispX(uint uiLine, const uchar *pszText, uchar ucAttr);

// 2.6. 在某行某列显示字符串
// 输入参数：uiLine  : 要显示的行数，从第1行开始
//           uiCol   : 要显示的列数，从第0列开始
//           pszText : 要显示的字串
// 说    明：如不存在该行，立即返回。如字串长度超出屏幕宽度，则绕到下一行。
//           如字串超出屏幕最后一行，截去超出的部分。
void _vDispAt(uint uiLine, uint uiCol, const uchar *pszText);

// 2.7. 在某行某列带属性显示字符串
// 输入参数：uiLine  : 要显示的行数，从第1行开始
//           uiCol   : 要显示的列数，从第0列开始
//           pszText : 要显示的字串
//           ucAttr  : 显示属性，可以是DISP_NORMAL、DISP_REVERSE、DISP_UNDERLINE的组合
// 说    明：如不存在该行，立即返回。如字串长度超出屏幕宽度，则绕到下一行。
//           如字串超出屏幕最后一行，截去超出的部分。
void _vDispAtX(uint uiLine, uint uiCol, const uchar *pszText, uchar ucAttr);

// 2.8. 备份某行内容
// 输入参数：uiLine  : 要备份的行号
// 返    回：0       : 成功备份
//           1       : 备份空间满
// 说    明：将指定的行内容连同属性保存起来
uint _uiBackupLine(uint uiLine);

// 2.9. 恢复最后一次行备份
// 说    明：将最后一次备份的行恢复原样
void _vRestoreLine(void);

// 3. 键盘

// 3.1. 检测有否按键，立即返回
// 返    回：0 ：没有按键
//           1 ：检测到按键，可通过_uiGetKey()读取
uint _uiKeyPressed(void);

// 3.2. 等待按键，直到读取到一个按键为止
// 返    回：0x00 - 0xff ：按键的ASCII码
uint _uiGetKey(void);

// 3.3. 清除键盘缓冲区
void _vFlushKey(void);

// 3.4. 重新定义一个按键，使该键返回另外一个键码
// 输入参数：ucSource : 键盘上真正存在的键码
//           ucTarget : 重定义后该建返回的键码, 如果该键码与ucSource相同，则删除该键原先的定义
// 返    回：0        : 成功
//           1        : 重定义的键码太多
// 说    明：最多只能重定义10个键码
uint _uiMapKey(uchar ucSource, uchar ucTarget);

void _vSetKeyVoice(char OnOff);
char _cGetKeyVoice(void);

// 4. IC卡

// 4.1. 检测卡片是否插入
// 输入参数：uiReader : 虚拟卡座号
// 返    回：0        : 无卡
//           1        : 检测到接触卡
//           2        : 检测到非接卡
//           9        : 检测到多张卡(非接)
// 说    明：只能检测用户卡座
uint _uiTestCard(uint uiReader);

// 4.2. 卡片复位
// 输入参数：uiReader    : 虚拟卡座号
// 输出参数：psResetData : ATR 数据
// 返    回：0           : 复位失败
//           >0          : 复位成功，返回ATR长度
uint _uiResetCard(uint uiReader, uchar *psResetData);

// 4.3. 卡片下电
// 输入参数：uiReader    : 虚拟卡座号
// 返    回：0           : 成功
//           1           : 失败
uint _uiCloseCard(uint uiReader);

// 4.4. 执行IC卡指令
// 输入参数：uiReader : 虚拟卡座号
//           pIn      : IC卡指令结构
// 输出参数：pOut     : IC卡返回结果
// 返    回：0        : 成功
//           1        : 失败
uint _uiExchangeApdu(uint uiReader, APDU_IN *pIn, APDU_OUT *pOut);

// 4.5. 执行IC卡指令
// 输入参数：uiReader   : 虚拟卡座号
//           uiInLen    : Command Apdu指令长度
//           psIn       : Command APDU, 标准case1-case4指令结构
//           uiProtocol : 协议类型
//                        APDU_NORMAL        标准APDU, 不自动处理61xx、6Cxx
//                        APDU_AUTO_GET_RESP 标准APDU, 自动处理61xx、6Cxx
//                        APDU_EMV           执行EMV协议
//                        APDU_SI            执行社保2.0协议
//           puiOutLen  : Response APDU长度
// 输出参数：psOut      : Response APDU, RespData[n]+SW[2]
// 返    回：0          : 成功
//           1          : 失败
uint _uiDoApdu(uint uiReader, uint uiInLen, uchar *psIn, uint *puiOutLen, uchar *psOut, uint uiProtocol);

// 4.6. 设置读卡函数, 用于外部提供IC卡接口时
// 输入参数: uiReader     : 卡座号, 0-9, 以后对该卡座号的操作将用所设置的函数进行
//           pfiTestCard  : 检测卡片是否存在函数指针
//           pfiResetCard : 卡片复位函数指针
//           pfiDoApdu    : 执行Apdu指令函数指针
//           pfiCloseCard : 关闭卡片函数指针
// 返    回: 0            : OK
//           1            : 失败
uint _uiSetCardCtrlFunc(uint uiReader,
					    int (*pfiTestCard)(void),
				        int (*pfiResetCard)(uchar *psAtr),
				        int (*pfiDoApdu)(int iApduInLen, uchar *psApduIn, int *piApduOutLen, uchar *psApduOut),
				        int (*pfiCloseCard)(void));

// 4.7. 获取最后一条Apdu指令
// 输出参数：puiReader  : 虚拟卡座号
//           puiInLen   : Command Apdu指令长度, 0表示还没有做过任何指令
//           psIn       : Command APDU, 标准case1-case4指令结构
//           puiOutLen  : Response Apdu长度, 0表示指令执行失败
//           psOut      : Response Apdu, OutData[n]+SW[2]
void _vGetLastApdu(uint *puiReader, uint *puiInLen, uchar *psIn, uint *puiOutLen, uchar *psOut);

// 5. 打印机

// 5.1. 取打印机宽度
// 返    回：0  : 无打印机
//           >0 : 打印机可打印的最大宽度
uint _uiGetPCols(void);

// 5.2. 打印数据
// 输入参数：pszText : 要打印的字串
//           iPrintFlag : 0-只缓存不打印 1-打印
// 返    回：0 : 打印成功
//           PRT_STATUS_NO_PAPER	// 缺纸
//           PRT_STATUS_LESS_PAPER	// 纸少
//           PRT_ERROR_NO_PRINTER	// 没发现打印机
//           PRT_ERROR_OFFLINE		// 打印机脱机
//           PRT_ERROR				// 故障
// 说    明：打印字串超过打印机最大宽度时反绕到下一行。
//           '\n'字符会导致换行。
uint _uiPrint(uchar const *pszText, int iPrintFlag);
//format: 1-靠左 2-居中 3-靠右 11-靠左打印自动换行(打印多行)
uint _uiPrintEx(int format, uchar const *pszText, int iPrintFlag);

// 5.3 取得打印机状态
// 返    回: PRT_STATUS_OK			// OK
//           PRT_STATUS_NO_PAPER	// 缺纸
//           PRT_STATUS_LESS_PAPER	// 纸少
//           PRT_ERROR_NO_PRINTER	// 没发现打印机
//           PRT_ERROR_OFFLINE		// 打印机脱机
//           PRT_ERROR				// 故障
uint _uiPrintGetStatus(void);

uint _uiPrintOpen(void);
uint _uiPrintClose(void);
//用1,2,3表示大中小三字体
uint _uiPrintSetFont(int Font);         


// 6. 时间

// 6.1. 设置当前系统时间
// 输入参数：pszTime : 要设置的时间，格式:YYYYMMDDHHMMSS
// 返    回：0       : 成功
//           1       : 失败
uint _uiSetTime(uchar const *pszTime);

// 6.2. 读取系统时间
// 输出参数：pszTime : 当前系统时间，格式:YYYYMMDDHHMMSS
void _vGetTime(uchar *pszTime);

// 6.3.设置计时器
// 输入参数：ulNTick   : 超时时间，单位为0.01秒
// 输出参数：pulTimer  : 计时器变量
// 说    明：精确到0.01秒
void _vSetTimer(ulong *pulTimer, ulong ulNTick);

// 6.4. 判断是否到达计时器变量所标明的时间
// 输入参数：ulTimer : 计时器变量
// 返    回：0       : 没有超时
//           1       : 超时
uint _uiTestTimer(ulong ulTimer);

// 6.5. 延迟一段时间
// 输入参数：uiNTick : 延迟的时间，单位为0.01秒
void _vDelay(uint uiNTick);

ulong _ulGetTickTime(void);
long _ulCTime2Stamp(char *pszTime);
void _vTimeStamp2CTime(ulong ulTimeStamp, uchar *pszDateTime);

// 7. 扩展内存

// 7.0. 检查/初始化扩展内存
// 输入参数：ucFlag : 1-检查扩展内存文件 0-c初始化扩展内存文件
// 返    回：0      :  成功
//           1      : 需要初始化
//           2      : 申请失败
//           3      : 其它错误
uint _uiCheckXMem(uchar ucFlag);

// 7.1. 设置扩展内存大小
// 输入参数：ulSize : 要设置的扩展内存大小，单位为字节
// 返    回：0      : 设置成功
//           1      : 参数错误, 申请的空间必须大于等于32
//           2      : 申请失败
//           >=32   : 扩展内存不足，返回值为最大可用扩展内存大小
ulong _ulSetXMemSize(ulong ulSize);

// 7.2. 读扩展内存
// 输入参数：ulOffset : 扩展内存以字节为单位的偏移量
//           uiLen    ：以字节为单位的长度
// 输出参数：pBuffer  : 读取得数据
// 返    回：0        : 成功
//           1        : 失败
uint _uiXMemRead(uint uiMemId, ulong ulOffset, void *pBuffer, uint uiLen);

// 7.3. 写扩展内存
// 输入参数：pBuffer  : 要写的数据
//           ulOffset : 扩展内存以字节为单位的偏移量
//           uiLen    ：以字节为单位的长度
// 返    回：0        : 成功
//           1        : 失败
uint _uiXMemWrite(uint uiMemId, ulong ulOffset, const void *pBuffer, uint uiLen);


// 8. 磁条卡

// 8.1. 清磁卡缓冲区
// 返    回：0 : 成功
//           1 : 失败
uint _uiMagReset(void);

// 8.2. 检测是否有磁卡刷过
// 返    回：0 : 无卡
//           1 : 有磁卡刷过
uint _uiMagTest(void);

// 8.3. 读磁道信息
// 输入参数：uiTrackNo : 磁道号，1-3
// 输出参数：pszBuffer : 磁道信息
// 返    回：0 : 读取信息正确
//           1 : 没有数据
//           2 : 检测到错误
// 说    明：没有起始符、结束符、校验位，字符编码为: 0x30-0x3f
uint _uiMagGet(uint uiTrackNo, uchar *pszBuffer);

// 8.4. 写磁道信息
// 输入参数：pszTrack2 : 2磁道信息
//         ：pszTrack3 : 3磁道信息
// 说    明：没有起始符、结束符、校验位，字符编码为: 0x30-0x3f
void _vMagWrite(uchar *pszTrack2, uchar *pszTrack3);

uint _uiMagClose(void);

// 9. 通讯

// 9.1. 设置当前串口号
// 输入参数：ucPortNo : 新串口号, 从串口1开始
// 返    回：原先的串口号
uchar _ucAsyncSetPort(uchar ucPortNo);

// 9.2. 打开当前串口
// 输入参数：ulBaud   : 波特率
//           ucParity : 奇偶校验标志，COMM_EVEN、COMM_ODD、COMM_NONE
//           ucBits   : 数据位长度
//           ucStop   : 停止位长度
// 返    回：0        : 成功
//           1        : 参数错误
//           2        : 失败
uchar _ucAsyncOpen(ulong ulBaud, uchar ucParity, uchar ucBits, uchar ucStop);

// 9.3. 关闭当前串口
// 返    回：0        : 成功
//           1        : 失败
uchar _ucAsyncClose(void);

// 9.4. 复位当前串口，清空接收缓冲区
// 返    回：0        : 成功
//           1        : 失败
uchar _ucAsyncReset(void);

// 9.5. 发送一个字节
// 输入参数：ucChar : 要发送的字符
// 返    回：0      : 成功
//           1      : 失败
uchar _ucAsyncSend(uchar ucChar);

// 9.6. 检测有没有字符收到
// 返    回：0 : 没有字符收到
//           1 : 有字符收到
uchar _ucAsyncTest(void);

// 9.7. 接收一个字符，没有则一直等待
// 返    回：接收到的字符
uchar _ucAsyncGet(void);

// 9.8.	发送一串数据
// 输入参数：pBuf  : 要发送的字符
//           uiLen : 数据长度
// 返    回：0     : 成功
//           1     : 失败
uchar _ucAsyncSendBuf(uchar *pBuf, uint uiLen);

// 9.9.	接收指定长度的一串数据
// 输入参数：pBuf      : 要发送的数据
//           uiLen     : 数据长度
//           uiTimeOut : 以秒为单位的超时时间
// 返    回：0         : 成功
//           1         : 超时
uchar _ucAsyncGetBuf(uchar *pBuf, uint uiLen, uint uiTimeOut);

// 9.10. 拨号建立链接
// 输入参数：pTelConfig : 拨号结构指针，见 TEL_CONFIG 结构
//           uiTimeOut  : 超时时间，单位为秒
// 返    回：0          : 成功
//           1          : 失败，可用_uiTelGetStatus()取得错误
uchar _ucTelConnect(const TEL_CONFIG *pTelConfig, uint uiTimeOut);

// 9.11. 拆除链接
// 返    回：0 : 成功
//           1 : 失败，可用_uiTelGetStatus()取得错误
uchar _ucTelDisconnect(void);

// 9.12. 取回拨号链接当前状态
// 返    回：0x0000 : 正常
//           0x01?? : 参数错误
//           0x02?? : 正常，低位字节表示一种状态
//           0x03?? : 错误，低位字节表示错误原因
uint _uiTelGetStatus(void);

// 9.13. 拨号线发送数据
// 输入参数：uiLength : 发送报文长度
//           pBuffer  : 发送报文内容
// 返    回：0        : 正常
//           1        : 失败，可用_uiTelGetStatus()取得错误
uchar _ucTelSend(uint uiLength, void const *pBuffer);

// 9.14. 拨号线接收数据
// 输入参数：puiLength : 接收报文长度
//           pBuffer   : 接收报文内容
//           uiTimeOut : 超时时间，单位为秒
// 返    回：0         : 正常
//           1         : 失败，可用_uiTelGetStatus()取得错误
uchar _ucTelGet(uint *puiLength, void *pBuffer, uint uiTimeOut);


// 10. 其它

// 10.1. 发一短声
void _vBuzzer(void);

// 10.2. 取一随机数
// 返    回：0-255的随机数
uchar _ucGetRand(void);

// 10.3. 做DES运算
// 输入参数：uiMode   : ENCRYPT -> 加密，DECRYPT -> 解密
//                      TRI_ENCRYPT -> 3Des加密，TRI_DECRYPT -> 3Des解密
//           psSource : 源
//           psKey    : 密钥
// 输出参数：psResult : 目的
void _vDes(uint uiMode, uchar *psSource, uchar *psKey, uchar *psResult);
void _vDesPlus(uint uiMode, uchar *psSource, int iSourceLen, uchar *psKey, uchar *psResult);

// 10.4 读取终端序列号
// 输出参数 ：pszSerialNo : 终端序列号
// 返    回 : 0 : OK
//            1 : ERROR
uint _uiGetSerialNo(uchar *pszSerialNo);

// 10.5 LED灯控制
// 输入参数：ucNo     : LED灯号
//           ucColor  : 颜色
// 返    回：0        : OK
//           1        : ERROR
uint _uiSetLed(uchar ucNo, uchar ucColor);


// 11. 调试宏
// 只有在VPOSFACE.H中有 # define VPOS_DEBUG 时以下宏才起作用

// 11.1. ASSERT(bVal)
// 功能：如果bVal为假，则显示该宏所在的文件名与行数
// 原型：ASSART(BOOL bVal)
# ifdef VPOS_DEBUG
/*
void _vDispAssert(uchar *pszFileName, ulong uiLineNo);
# define ASSERT(expr)                                              \
      if(!(expr)) {                                                \
          _vDispAssert((__FILE__), (ulong)(__LINE__));			   \
      }
*/
#define ASSERT(x) if(!(x)) {                    \
    uchar szTmp[40]; sprintf((char*)szTmp, "ASSERT %d %s\n",__LINE__,__FILE__);    \
    _vDisp(2, szTmp);    _uiGetKey(); }
# else
# define ASSERT(expr)
# endif

/*
// 11.2. DBGINFO()
// 功能：显示该宏所在的行数与pPointer所指内容，内容用16进制数显示6字节
// 原型：DBGINFO(void *pPointer)
# ifdef VPOS_DEBUG
void _vDispDbgInfo(uchar *pszFileName, ulong ulLineNo, void *pPointer);
# define DBGINFO(pPointer)                                         \
      _vDispDbgInfo((__FILE__), (ulong)(__LINE__), (void *)(pPointer));
# else
# define DBGINFO(pPointer)
# endif
*/
    
// 不再支持跟踪功能
// 12. 跟踪
//

// 13. 网络功能函数(也包括了SDLC、异步通讯), 接口参照了VeriFone520 vCommCE接口
// 13.1. 网络初始化
uint _uiCommInit(void);

// 13.2 注册网络
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA
//       pCommConfig : 网络参数
//       uiTimeout   : 超时时间(秒), 0表示不等待,立即返回
// ret : VPOSCOMM_
// Note: 必须先注册网络才可使用网络
uint _uiCommLogin(uint uiCommType, VPOSCOMM_CONFIG *pCommConfig, uint uiTimeout);

// 13.2 注销网络
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA
uint _uiCommLogOut(uint uiCommType, int poweroff);

// 13.3 保持注册状态
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA
// Note: 如果发现丢失了注册, 试图重新注册
uint _uiCommKeepLogin(uint uiCommType);

// 13.4 连接服务器
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA or VPOSCOMM_WIFI
//       psParam1    : 参数1, 对于VPOSCOMM_SDLC VPOSCOMM_ASYNC : 电话号码
//                            对于VPOSCOMM_ETHER VPOSCOMM_GPRS VPOSCOMM_CDMA VPOSCOMM_WIFI: Ip地址
//       psParam2    : 参数2, 对于VPOSCOMM_SDLC VPOSCOMM_ASYNC : 外线号码
//                            对于VPOSCOMM_ETHER VPOSCOMM_GPRS VPOSCOMM_CDMA VPOSCOMM_WIFI: 端口号
//       psParam3    : 参数3, 对于VPOSCOMM_SDLC VPOSCOMM_ASYNC : "0"-不检测拨号音 "1"-检测拨号音
//                            对于VPOSCOMM_ETHER VPOSCOMM_GPRS VPOSCOMM_CDMA VPOSCOMM_WIFI: "0"-TCP "1"-UDP
//       uiTimeout   : 超时时间(秒), 0表示不等待,立即返回
uint _uiCommConnect(uint uiCommType, uchar *psParam1, uchar *psParam2, uchar *psParam3, uint uiTimeout);

// 13.5 检测连通性
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA
uint _uiCommTestConnect(uint uiCommType);

// 13.6 断开连接
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA
uint _uiCommDisconnect(uint uiCommType);

// 13.7 保持连接状态
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA
uint _uiCommKeepConnect(uint uiCommType);

// 13.8 获取网络信息
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA
// out : pCommInfo   : 网络信息
uint _uiCommGetInfo(uint uiCommType, VPOSCOMM_INFO *pCommInfo);

// 13.9 发送数据
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA
//       psSendData  : 发送的数据
//       uiSendLen   : 发送的数据长度
uint _uiCommSendData(uint uiCommType, uchar *psSendData, uint uiSendLen);

// 13.10 接收数据
// in  : uiCommType  : 网络类型 VPOSCOMM_SDLC or VPOSCOMM_ASYNC or VPOSCOMM_ETHER or VPOSCOMM_GPRS or VPOSCOMM_CDMA
//       piRecvLen   : 接收数据缓冲区大小
//       uiTimeout   : 超时时间(秒)
// out : psRecvData  : 接收的数据
//       piRecvLen   : 接收的数据长度
uint _uiCommRecvData(uint uiCommType, uchar *psRecvData, uint *puiRecvLen, uint uiTimeout);


// 15. 密码键盘与安全模块
// 15.1 get pin from pin pad
// In  : psMessage1   : message displayed on line 1
//       psMessage2   : message displayed on line 2
//       psMessage3   : message displayed on line 2 after a key pressed
//       ucBypassFlag : 允许bypass标志, 1:允许 0:不允许
//       ucMinLen     : minimum pin length
//       ucMaxLen     : maximum pin length
//       uiTimeOut    : time out time in second
// Out : pszPin       : entered pin
// Ret : SEC_STATUS_OK      : pin entered
//       SEC_STATUS_TIMEOUT : 超时
//       SEC_STATUS_CANCEL  : 用户取消
//       SEC_STATUS_BYPASS  : 用户Bypass
//       SEC_NO_DEVICE		: 无密码键盘
//       SEC_ERROR		    : 故障
uchar _ucPinPadInput(uchar *psMessage1, uchar *psMessage2, uchar *psMessage3, uchar ucBypassFlag,
				     uchar ucMinLen, uchar ucMaxLen, uint uiTimeOut, uchar *pszPin);

// 15.2 display message on pinpad
// In  : psMessage1 : message displayed on line 1
//       psMessage2 : message displayed on line 2
//       uiTimeOut  : time out time in second
void _vPinPadDisp(uchar *pszMesg1,uchar *pszMesg2, uint uiTimeOut);

// 15.3 安全模块初始化
// Ret : SEC_STATUS_OK      : OK
//       SEC_NO_DEVICE		: 无安全模块
//       SEC_ERROR		    : 故障
uchar _ucSecInit(void);

// 15.4 安全模块检查主密钥存不存在
// in  : iIndex      : 主密钥索引[0-n], 容量取决于POS
// Ret : SEC_STATUS_OK      : OK
//       SEC_ERROR_INDEX	: 索引不支持
//       SEC_ERROR_NO_KEY	: 密钥不存在
//       SEC_NO_DEVICE		: 无安全模块
//       SEC_ERROR		    : 故障
uchar _ucSecCheckMasterKey(int iIndex);

// 15.5 安全模块设置主密钥
// in  : iIndex      : 主密钥索引[0-n], 容量取决于POS
//       psMasterKey : 主密钥[16]
// Ret : SEC_STATUS_OK      : OK
//       SEC_ERROR_INDEX	: 索引不支持
//       SEC_NO_DEVICE		: 无安全模块
//       SEC_ERROR		    : 故障
uchar _ucSecSetMasterKey(int iIndex, uchar *psMasterKey);

// 15.6 安全模块主密钥DES计算
// in  : iIndex      : 主密钥索引[0-n], 容量取决于POS
//       iMode       : 算法标识, TRI_ENCRYPT:3Des加密 TRI_DECRYPT:3Des解密
//       psIn        : 输入数据
// out : psOut       : 输出数据
// Ret : SEC_STATUS_OK      : OK
//       SEC_ERROR_INDEX	: 索引不支持
//       SEC_ERROR_NO_KEY	: 密钥不存在
//       SEC_ERROR_ALGO		: 算法不支持
//       SEC_NO_DEVICE		: 无安全模块
//       SEC_ERROR		    : 故障
uchar _ucSecDes(int iIndex, int iMode, uchar *psIn, uchar *psOut);

// 15.7 安全模块工作密钥Retail-CBC-Mac计算
// in  : iIndex       : 主密钥索引[0-n], 容量取决于POS
//       psWorkingKey : 工作密钥密文
//       psIn         : 输入数据
//       iInLen       : 输入数据长度, 不足8的倍数后会补0
//       psMac        : 初始向量
// out : psMac        : Mac结果[8]
// Ret : SEC_STATUS_OK      : OK
//       SEC_ERROR_INDEX	: 索引不支持
//       SEC_ERROR_NO_KEY	: 密钥不存在
//       SEC_ERROR_ALGO		: 算法不支持
//       SEC_NO_DEVICE		: 无安全模块
//       SEC_ERROR		    : 故障
// Note: 如果做3DES加密, 也可用此函数
uchar _ucSec3DesCbcMacRetail(int iIndex, uchar *psWorkingKey, uchar *psIn, int iInLen, uchar *psMac);

// 15.8 密码键盘输入密文PIN
// In  : psMessage1   : message displayed on line 1
//       psMessage2   : message displayed on line 2
//       psMessage3   : message displayed on line 2 after a key pressed
//       iIndex       : 主密钥索引[0-n], 容量取决于POS
//       psPinKey     : PinKey密文
//       pszPan       : 主帐号
//       ucBypassFlag : 允许bypass标志, 1:允许 0:不允许
//       ucMinLen     : minimum pin length
//       ucMaxLen     : maximum pin length
//       uiTimeOut    : time out time in second
// Out : sPinBlock    : pinblock[8]
// Ret : SEC_STATUS_OK      : OK
//       SEC_STATUS_TIMEOUT : 超时
//       SEC_STATUS_CANCEL  : 用户取消
//       SEC_STATUS_BYPASS  : 用户Bypass
//       SEC_ERROR_INDEX	: 索引不支持
//       SEC_ERROR_NO_KEY	: 密钥不存在
//       SEC_NO_DEVICE		: 无安全模块
//       SEC_ERROR		    : 故障
uchar _ucSecPinPadInput(uchar *psMessage1, uchar *psMessage2, uchar *psMessage3,
                      int iIndex, uchar *psPinKey, uchar *pszPan, uchar ucBypassFlag,
                      uchar ucMinLen, uchar ucMaxLen, uint uiTimeOut, uchar *psPinBlock);


                      
char* _pszGetAppVer(char *pszVer);
char* _pszGetParamVer(char *pszVer);                      
char* _pszGetFirmwareVer(char *pszVer); 
void  vGetPbocHwVerAndFwVer(char *pszHWVer, char *pszFWVer);                   
char* _pszGetVendor(char *pszVendor);
char* _pszGetModelName(char *pszModel);                  
char* _pszGetInternalVer(char *pszInternalVer);
uint _uiCheckBattery(void);                      
uint _uiGetPosSpace(ulong *pulTotal, ulong *pulUsed, ulong *pulFree);

//开始扫码,调用后不管返回成功失败必须配对调用_vQrEnd
uint _uiQrStart(void);
                      
//循环调用,判断返回值
//0-无扫码 >0:扫码成功,返回长度.扫码成功后,若要继续扫码需要end后重新start开始
uint _uiGetQrCode(uchar *pszQrCode, int size);

//释放资源
void _vQrEnd(void); 

void _vSetBackLight(int val);

int iPosHardwareModule(uchar module, char *pszModuleInfo, int size);

void _vSetAllLed(uchar blue, uchar yellow, uchar green, uchar red);

ulong _ulGetTimeStamp(void);
void _vSetAutoPowerOff(ulong ulTimeSec);
#ifdef __cplusplus
}
#endif

# endif // # ifndef VPOSFACE_H
