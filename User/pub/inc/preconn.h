#ifndef PRE_CONN_H
#define PRE_CONN_H


//设置连接参数，pszHost、pszBakHost为域名或IP
void vSetPreConnParam(char *pszHost, char *pszPort, char *pszBakHost, char *pszBakPort);
//开始预拨号:flag 1-普通tcp连接 2-TLS连接
void vSetPreConnRunFlag(int flag);
//获取预拨号运行标志:0-未开启 1-普通tcp连接 2-TLS连接 0x11-预拨号结束(tcp) 0x12-预拨号结束(TLS)
int iGetPreConnRunFlag(void);

//int iGetPreConnStatus(void);
//void vSetPreConnStatus(int status);

//等待并获取预拨号结果
//ret:0-成功 1-失败
int iWaitPreConnResult(void);

void vStartPreConnTask(void);

//关闭连接,重置预拨号参数
//void vPreConnReset(void);

//关闭连接,自动判断是否预拨号并重置预拨号
//flag:1-普通连接 2-TLS
int iConnReset(int flag);

#endif
