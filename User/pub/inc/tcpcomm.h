#ifndef _TCP_COMM_H_
#define _TCP_COMM_H_

#ifndef VPOSCOMM_GPRS
	//in vposface.h
	# define VPOSCOMM_WIFI              1	
	# define VPOSCOMM_GPRS              2
	# define VPOSCOMM_ETHER             3
#endif


int iCommLogin(void);

int iCommTcpConn(char *pszSvrIP, char *pszSvrPort, int iTimeOutMs);

int iCommTcpSend(unsigned char *psSendData, int uiSendLen);

/* ret: >0 recv byte num
		<0 recv err
*/
int iCommTcpRecv(unsigned char *psRecvData, int uiExpRecvLen, int iTimeOutMs);

int iCommTcpDisConn(void);

int iCommLogout(int iCommType, int iPowerOff);

int iCommConfig(void);

int iWifiConfig(void *p);

int iGetHostByName(char *HostName, char *HostAddr);

int iGetICCID(char *pszICCID);

int iGetIMEI(char *pszIMEI);

int iGetLocalIP(char *ip);

//取无线信号强度
int iGetWirelessSignalQuality(void);

int iGetWirelessSignalVal(void);

void vSetWirelessSignalVal(int val);

int iWifiFacTest(char *pszWifiVer, int iOutSize);

int iGprsFacTest(char *pszIMEI, char *pszICCID);

//int iGetGprsCellInfo(char *pszCellInfo);
int iGetGprsCellInfo(char *pszCellInfo, char *pszCellLoc);

int iTcpConnPreProc(char cTipFlag, char *pcUpFlag);

int iGetCommInitStatus(void);
#endif
