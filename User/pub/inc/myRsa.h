#ifndef _MY_RSA_H
#define _MY_RSA_H
#include "mbedtls/rsa.h"

//根据Pem/Der生成RSA公钥. data若为pem数据,必须'\0'结尾
int iParseRsaPubKeyfromData(mbedtls_rsa_context *ctx_rsa, uchar *data, uint dataLen);

//根据Pem/Der生成RSA私钥.
//data若为pem数据,必须'\0'结尾
int iParseRsaPriKeyfromData(mbedtls_rsa_context *ctx_rsa, uchar *data, uint dataLen, uchar *pwd, uint uiPwdLen);

//根据NDE生成rsa,公钥必须有N和E,私钥必须有NDE, P和Q可以为NULL. NDE等参数为可见字符串格式
int iGenRsafromParam(mbedtls_rsa_context *ctx_rsa, uchar *N, uchar *D, uchar *E, uchar *P, uchar *Q);
//根据二进制的NDE生成rsa
int iGenRsafromParamBin(mbedtls_rsa_context *ctx_rsa, uint rsaLen, uchar *N, uchar *D, uchar *E);

//取rsa的长度,例如1024位的rsa,长度为1024/8=128
int iGetRsaLen( const mbedtls_rsa_context *ctx );

//从pem/der中取得rsa的NDE可见字符串格式参数，
//data若为pem数据,必须'\0'结尾
//ucMode: 0-公钥  1-私钥
//NDE需有足够长度空间, N/D需为密钥长度的两倍+5, 例如1024位的密钥, 长度为128，则N、E需有2*128+5的空间
int iGetRsaParamfromData(uchar ucMode, uchar *data, uint dataLen, uchar *pwd, uint uiPwdLen, 
			uchar *N, uchar *D, uchar *E, uchar *P, uchar *Q);

//从rsa中取得NDE16进制格式参数
int iGetRsaParamBin(mbedtls_rsa_context *ctx_rsa, uint *puiRsaLen, uchar *N, uchar *D, uchar *E);

//公钥加密, 源数据长度等于N的长度
int iRsaPubKeyEnc(mbedtls_rsa_context *ctx_rsa, uchar *psData, uchar *psOut);

//私钥解密, 源数据长度等于N的长度
int iRsaPriKeyDec(mbedtls_rsa_context *ctx_rsa, uchar *psData, uchar *psOut);

//公钥加密(源数据做pkcs#1填充)
int iRsaPubKeyEncPkcs1(mbedtls_rsa_context *ctx_rsa, uchar *psData, uint uiDataLen, uchar *psOut, uint *puiOutLen);

//私钥解密(源数据为pkcs#1填充)
int iRsaPriKeyDecPkcs1(mbedtls_rsa_context *ctx_rsa, uchar *psData, uchar *psOut, uint uiOutSize, uint *puiOutLen);

//私钥签名
int iRsaPriKeySignPkcs1(mbedtls_rsa_context *ctx_rsa, uchar *psData, uint uiDataLen, uchar *psOut);

//公钥验签
int iRsaPubKeyVerifyPkcs1(mbedtls_rsa_context *ctx_rsa, uchar *psData, uint uiDataLen, uchar *psSignData);

//根据pem等生成rsa或者根据NDE参数生成rsa，使用完毕后需调用本接口做资源释放
int iRsaFree(mbedtls_rsa_context *ctx_rsa);

#endif
