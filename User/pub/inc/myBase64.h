
#ifndef MY_BASE64_H
#define MY_BASE64_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief          Encode a buffer into base64 format
 *
 * \param dst      destination buffer
 * \param dlen     size of the destination buffer
 * \param olen     number of bytes written
 * \param src      source buffer
 * \param slen     amount of data to be encoded
 *
 * \return         0 if successful, or MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL.
 *                 *olen is always updated to reflect the amount
 *                 of data that has (or would have) been written.
 *                 If that length cannot be represented, then no data is
 *                 written to the buffer and *olen is set to the maximum
 *                 length representable as a size_t.
 *
 * \note           Call this function with dlen = 0 to obtain the
 *                 required buffer size in *olen
 */
int iBase64_encode( unsigned char *dst, size_t dlen, size_t *olen,
                   const unsigned char *src, size_t slen );

/**
 * \brief          Decode a base64-formatted buffer
 *
 * \param dst      destination buffer (can be NULL for checking size)
 * \param dlen     size of the destination buffer
 * \param olen     number of bytes written
 * \param src      source buffer
 * \param slen     amount of data to be decoded
 *
 * \return         0 if successful, MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL, or
 *                 MBEDTLS_ERR_BASE64_INVALID_CHARACTER if the input data is
 *                 not correct. *olen is always updated to reflect the amount
 *                 of data that has (or would have) been written.
 *
 * \note           Call this function with *dst = NULL or dlen = 0 to obtain
 *                 the required buffer size in *olen
 */
int iBase64_decode( unsigned char *dst, size_t dlen, size_t *olen,
                   const unsigned char *src, size_t slen );

//MD5仅一个接口, 暂跟Base64放一起
/**
 * \brief          Output = MD5( input buffer )
 *
 * \param input    buffer holding the data
 * \param ilen     length of the input data
 * \param output   MD5 checksum result
 *
 * \return         0 if successful
 */
int iMd5(const unsigned char *input, size_t ilen, unsigned char output[16]);

//计算MD5, 并转为可见字符
int iMd5Str(const unsigned char *input, size_t ilen, unsigned char output[32+1]);

int iSha1( const unsigned char *input, size_t ilen, unsigned char output[20]);

//计算文件md5值
//输入参数： pszFileName     文件名
//          psHead          文件内容之外需要起始计算的数据部分
//          uiHeadLen       起始数据长度,可为0
//输出参数： psMd5           16字节的md5
//返回值：0-成功 其它-失败
int iMd5File(char *pszFileName, unsigned char *psHead, unsigned int uiHeadLen, unsigned char *psMd5);

/*
//以下md5函数用于大文件分段计算md5,最后必须调用iMd5_Free
int iMd5_Init(void);
int iMd5_Update(unsigned char *input, unsigned int ilen);
int iMd5_Finish(unsigned char *output);
int iMd5_Free(void);
*/

#ifdef __cplusplus
}
#endif

#endif /* base64.h */
