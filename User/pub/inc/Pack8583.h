/*******************************************************************************
COMMENT:
FIELD_ATTR element:
   eElementAttr:
      Attr_n   :  numeric data('0'~'9','A'~'F')
                  each data element represents 1 nibble.
                  (2 data elements = 1 byte)
      Attr_z   :  numeric data(0x30~0x3F)
                  each data element represents 1 nibble.
                  (2 data elements = 1 byte)
      Attr_b   :  binary data, each data element represents 1 bit.
                  (8 data elements = 1 byte)
      Attr_a   :  alpha-number and special characters.
                  each data element represents 1 byte.
      Attr_UnUsed :  the element is no used.
      Attr_Over   :  definiens over.

   eLengthAttr:
      Attr_fix  - fixed length.
      Attr_var1 - variable length, one byte at the beginning of the data
                  element identify the number of positions following to the
                  end of that data element.
                  (0x00 ~ 0x99)
      Attr_var2 - variable length, two bytes at the beginning of the data
                  element identify the number of positions following to the
                  end of that data element.
                  (0x00 0x00 ~ 0x09 0x99)

    uiLength:
      length of the data element.
NOTE:
   All fixed length Attr_n data elements are right justified with leading
   zeroes.
   All fixed length Attr_ln data elements are left justified with trailing
   zero.
   All fixed length Attr_a data elements are left justified with trailing
   spaces.
   All fixed length Attr_b data elements are left justified with trailing
   zeroes.
   All variable length Attr_n data elements are left justified with trailing
   zero.
*******************************************************************************/
#ifndef _PACK8583
#define _PACK8583

//特殊处理定义：可变长的Attr_n型数据元素右靠齐，左补零
#define VarAttrN_RightJustify

#define UnPackLenErr -1000 //解包后数据长度与实际长度不符

enum E_Attr
{
   Attr_UnUsed,
   Attr_n,
   Attr_z,
   Attr_b,
   Attr_a,
   Attr_ln, /*xiaf add at 202003*/
   Attr_Over
};
enum L_Attr
{
   Attr_fix,
   Attr_var1,
   Attr_var2
};

typedef struct
{
   enum E_Attr eElementAttr;
   enum L_Attr eLengthAttr;
   unsigned int uiLength;
} FIELD_ATTR;

/*********************************************************************
Function      :   change 8583 struct to 8583 packet
Param in      :
   pMsgAttr   :   struct array to define attribute of message
   pDataAttr  :   struct array to define attribute of bitmap and
                  data elements
   pSt8583    :   unsigned char pointer to point to struct 8583
		     eg. ST_8583 st8583;
		         pSt8583 = (unsigned char *)&st8583;
Param out     :
   pusOut     :   8583 packet
   puiOutLen  :   bytes of 8583 packet
Return Code   :   none
*********************************************************************/
extern int iPack8583(const FIELD_ATTR *pMsgAttr, const FIELD_ATTR *pDataAttr, 
                  unsigned char *pSt8583, unsigned char *pusOut, unsigned int *puiOutLen);

/***********************************************************************
Function      :   change 8583 packet to 8583 struct
Param in      :
   pMsgAttr   :   struct array to define attribute of message
   pDataAttr  :   struct array to define attribute of bitmap and
   pusIn      :   8583 packet
   uiInLen    :   bytes of 8583 packet
Param out     :
   pSt8583    :   unsigned char pointer to point to struct 8583
		     eg. ST_8583 st8583;
		         pSt8583 = (unsigned char *)&st8583;
Return Code   :
   0          :   success
   <0         :   UnPack8583Fail
*********************************************************************/
extern int iUnPack8583(const FIELD_ATTR *pMsgAttr, const FIELD_ATTR *pDataAttr, 
                  unsigned char *pusIn, unsigned int uiInLen, unsigned char *pSt8583);

#endif
