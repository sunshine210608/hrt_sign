# ifndef  OPER_H
# define  OPER_H

// need allocate X memory
// OPER_NO_LEN can't be larger than 127
// you must allocate X memory for operator management
// X memory size needed is N_OPERS * OPER_REC_SIZE

# define OPER_NO_LEN  2
# define OPER_PIN_LEN 4
# define OPER_REC_SIZE    (OPER_NO_LEN+OPER_PIN_LEN)

// 1. 初始化操作员系统环境
// 只能调用一次
void vOperInit(void);

// 2. 列出当前操作员
// 输入参数: iTimeout : 超时时间(秒), 0表示无超时
// 返    回: 0 : 正常返回
//           1 : 超时
//           2 : 取消
// 说    明: 在屏幕上列出当前操作员列表
uint uiOperList(int iTimeout);

// 3. 检查操作员是否存在
// 输入参数: pszOperNo : 操作员号码
// 返    回: 0         : 无此操作员
//           1         : 该操作员存在
// 说    明: 检查该操作员存在否
uchar ucOperExist(uchar *pszOperNo);

// 4. 增加操作员
// 输入参数: iTimeout : 超时时间(秒), 0表示无超时
// 返    回: 0 : 正常返回
//           1 : 超时
//           2 : 取消
// 说    明: 增加一个操作员
uint uiOperAdd(int iTimeout);

// 5. 删除操作员
// 输入参数: iTimeout : 超时时间(秒), 0表示无超时
// 返    回: 0 : 正常返回
//           1 : 超时
//           2 : 取消
// 说    明: 删除一个操作员
uint uiOperSub(int iTimeout);

// 6. 检查操作员密码
// 输入参数: pszOperNo  : 操作员号码
//           pszOperPwd : 操作员新密码
// 返    回: 0          : OK
//           1          : 错误
// 说    明: 检查操作员密码
uint uiOperCheckPwd(uchar *pszOperNo, uchar *pszOperPwd);

// 7. 修改操作员密码
// 输入参数: pszOperNo  : 操作员号码
//           pszOperPwd : 操作员新密码
// 返    回: 0          : OK
//           1          : 失败
// 说    明: 修改操作员密码
uint uiOperChangePwd(void);

# endif
