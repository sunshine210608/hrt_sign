#ifndef MY_HTTP_H_
#define MY_HTTP_H_

#define HTTP_GET	0x80
#define HTTP_POST	0x00

//以下application默认为UTF-8
#define HTTP_TYPE_FORMURL	0x01		//Content-Type: application/x-www-form-urlencoded
#define HTTP_TYPE_JSON		0x02		//Content-Type: application/json
#define HTTP_TYPE_XML		0x03		//Content-Type: application/xml
#define HTTP_TYPE_TEXT		0x04		//Content-Type: text/plain
#define HTTP_TYPE_HTML		0x05		//Content-Type: text/html

#define HTTP_TYPE_OTHER1		0x0D		//其它1
#define HTTP_TYPE_OTHER2		0x0E		//其它2
#define HTTP_TYPE_OTHER3		0x0F		//其它3

void vGetHttpErrMsg(char *pszErrMsg);

int iParseHttpUrl(char *pszUrlAddr, char *pszHostName, char *pszSubUrl, int *piPort);

//pszHostName: 服务器地址和端口,格式: xxx.xxx.xxx.xxx:nnnn	当端口为80时可省略端口部分
int iHttpSend(uchar ucReqHeadType, uchar *pszHostName, uint uiPort, uchar *pszSubUrl, uchar *pszBody, int iLen);

int iHttpRecv(char bigData, uchar *pszBody, uint uiBodySize, uint *puiRcvLen, uint uiTimeOut, char *pszContentRange);

int iHttpSendRecv(uchar ucType, char *pszUrl, char *pszBakHost, int retry, char *pszBody, int iBodyLen, char *pszRsp, uint uiRspSize, uint uiTimeOut);
int iHttpsSendRecv(uchar ucType, char *pszUrl, char *pszBakHost, int retry, char *pszBody, int iBodyLen, char *pszRsp, uint uiRspSize, uint uiTimeOut);

int iHttpDownloadFile(uchar *pszUrl, uchar *pszFile, uint *puiFileSize, uint *puiDownloadLen, 
            uchar ucSaveFlag, uchar *psFileHead, uint uiFleHeadLen, void (*saveFunc)(ulong, uchar *, uint));

int iHttpDownloadFileTA(uchar *pszUrl, uchar *pszFile, uint *puiFileSize, uint *puiDownloadLen, 
            uchar ucSaveFlag, uchar *psFileHead, uint uiFleHeadLen, void (*saveFunc)(ulong, uchar *, uint));
#endif
