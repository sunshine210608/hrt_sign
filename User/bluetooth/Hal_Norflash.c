/********************************************************************
* 文件名		:		Hal_Norflash.c
* 描  述		:		NOR FLASH应用接口
* 创  建		:		欧阳志强 2017.09.12
* 版  本		:		v1.0
* 修  改		:		----
*********************************************************************/

#define HAL_NORFLASH_DEC
#include "mhscpu_cache.h"
#include "mhscpu.h"
#include "mhscpu_qspi.h"
#include "debug.h"
#include "define.h"
#include "Hal_Norflash.h"

#define ROM_QSPI_EraseSector    (*((uint8_t (*)(QSPI_CommandTypeDef *, uint32_t))(*(uint32_t *)0x0024)))
#define ROM_QSPI_ProgramPage    (*((uint8_t (*)(QSPI_CommandTypeDef *, DMA_TypeDef *, uint32_t, uint32_t, uint8_t *))(*(uint32_t *)0x0028)))   

#define FLASH_SECTOR_SIZE 	        (0x1000)

/*********************************************************************
* 名  称		:		Hal_flashInit
* 描  述		:		内部flash初始化
* 输  入		:		void
* 输  出		:		NULL
* 返  回		:		void
* 修  改		:		----
*********************************************************************/
void Hal_flashInit(void)
{
	CACHE_InitTypeDef test_aes;
	
	SYSCTRL_AHBPeriphClockCmd(SYSCTRL_AHBPeriph_DMA, ENABLE);
	SYSCTRL_AHBPeriphResetCmd(SYSCTRL_AHBPeriph_DMA, ENABLE);	
	
	CACHE_CleanAll(CACHE);
	test_aes.aes_enable = DISABLE;
	CACHE_Init(CACHE, &test_aes);	
	
	//DC_DelayMs(10);
	mdelay(10);
}

/*********************************************************************
* 名  称		:		flash_read
* 描  述		:		读NOR FLASH
* 输  入		:		uiAddr		:	地址
* 						buf			:	数据
* 						ReadLen		:	读数据长度
* 输  出		:		NULL
* 返  回		:		
* 修  改		:		----
*********************************************************************/
static int flash_read(unsigned int uiAddr, unsigned char * buf, unsigned int ReadLen)
{
	int i = 0;
	unsigned int tAddr = uiAddr;
//	CACHE_InitTypeDef CACHE_AddrStruct;

	__disable_irq();
	CACHE_CleanAll(CACHE);
	
/*	
    CACHE_AddrStruct.Address = uiAddr;
    CACHE_AddrStruct.size = ReadLen;       
    CACHE_Clean(CACHE, &CACHE_AddrStruct); 
*/	
	while (i < ReadLen)
	{
		//*(buf + i) = REG08(tAddr++);
		i++;
	}

	while(CACHE->CACHE_AES_CS & CACHE_IS_BUSY)	//cache正在从Flash中取指
	{
		//dbg("reading");
	}
	__enable_irq();
	//DC_DelayMs(10);
	mdelay(10);
	return ReadLen;
}

/*********************************************************************
* 名  称		:		flash_program
* 描  述		:		写NOR FLASH
* 输  入		:		uiAddr		:	地址
* 						pSrc		:	数据
* 						uiWriteLen	:	数据长度
* 输  出		:		NULL
* 返  回		:		数据长度
* 修  改		:		----
*********************************************************************/
static int flash_program(uint32 uiAddr, u8 * pSrc, uint32 uiWriteLen)
{
	uint32 uiAddr_ch = uiAddr;
	uint32 uiPageAddr = 0;
	uint32 uiNumByteToWrite = uiWriteLen;
	uint16 usPageOffset = 0;
	uint16 uiPageRemain = 0;
	uint16 i = 0;
	u8 tmp[X25Q_PAGE_SIZE];
	u8 ucStatus = 0x00;
	u8 * buf = (u8 *)pSrc;
	QSPI_CommandTypeDef cmdType;
	
	if ((uiAddr_ch < NOR_FLASH_ADRR_START) || ((uiAddr_ch + uiWriteLen) - 1 > NOR_FLASH_ADRR_END))
	{
		return 0x00;
	}
	
	//dbg("uiAddr:%08x",uiAddr);	

	uiPageAddr = uiAddr_ch & 0xFFFFFF00;			//uiAddr_ch / NOR_FLASH_PAGE_SIZE;	// 扇区地址
	usPageOffset = uiAddr_ch & (X25Q_PAGE_SIZE-1);		// 在扇区内的偏移
	uiPageRemain = X25Q_PAGE_SIZE - usPageOffset;		// 扇区剩余空间大小
#if 0	
	dbg("uiPageAddr:%08x",uiPageAddr);	
	dbg("usPageOffset:%08x",usPageOffset);
	dbg("uiPageRemain:%d",uiPageRemain);
#endif	
	if (uiNumByteToWrite <= uiPageRemain)
	{
		uiPageRemain = uiNumByteToWrite;					// 不大于4096个字节
	}
	
    cmdType.Instruction = QUAD_INPUT_PAGE_PROG_CMD;
    cmdType.BusMode = QSPI_BUSMODE_114;	  //Q-out mode    
    cmdType.CmdFormat = QSPI_CMDFORMAT_CMD8_ADDR24_PDAT; 
	
	while (1)
	{
		flash_read(uiPageAddr, tmp, X25Q_PAGE_SIZE);
		for (i=0; i<uiPageRemain; i++)						// 复制
		{
			tmp[i + usPageOffset] = buf[i];
		}
		//dbg("uiPageAddr:%08x",uiPageAddr);
		//MH_CommandTypeDef sCommand;		
		__disable_irq();
		ROM_QSPI_ProgramPage(&cmdType, DMA_Channel_0, uiPageAddr, sizeof(tmp), (uint8_t*)(tmp));		
		__enable_irq();
		uiPageAddr += X25Q_PAGE_SIZE;
		usPageOffset = 0;									// 偏移位置为0
		buf += uiPageRemain;								// 指针偏移
	//	uiAddr_ch += uiPageRemain;							// 写地址偏移
		uiNumByteToWrite -= uiPageRemain;					// 字节数递减
		//dbg("uiNumByteToWrite:%d",uiNumByteToWrite);
		if (uiNumByteToWrite <= 0)
		{
			break;
		}
		else if (uiNumByteToWrite > X25Q_PAGE_SIZE)
		{
			uiPageRemain = X25Q_PAGE_SIZE;				// 下一个扇区还是写不完
		}
		else
		{
			uiPageRemain = uiNumByteToWrite;				// 下一个扇区可以写完了
		}
	}
	return uiWriteLen;
}
/*********************************************************************
* 名  称		:		halFlashErase
* 描  述		:		擦除数据
* 输  入		:		uiAddr		:	地址
* 						nLen		:	清长度
* 输  出		:		NULL
* 返  回		:		0  成功 1 失败
* 修  改		:		----
*********************************************************************/
int halFlashErase(u32 uiAddr,u32 nLen)
{
	u16 i = 0;
	u16 nSectorNum = 0;
	u32 uiNumByteToErase = 0;
	int uiNumByteToMoreErase = 0;//后面被多擦掉的字节数
	u8 ucTmp[FLASH_SECTOR_SIZE*2];
	u32 uiSectorAddr = 0;
	u32 uiSectorOffset = 0;
	u32 uiSectorRemain = 0;//每个扇区里剩余的
	
	if ((uiAddr < NOR_FLASH_ADRR_START) || ((uiAddr + nLen) - 1 > NOR_FLASH_ADRR_END))
	{
		return 1;
	}
//
	//dbg("uiAddr:%08x",uiAddr);//0x01060800

	uiSectorAddr = uiAddr & 0xFFFFF000;			// 扇区地址
	uiSectorOffset = uiAddr & (FLASH_SECTOR_SIZE-1);		// 在扇区内的偏移
	uiSectorRemain = FLASH_SECTOR_SIZE - uiSectorOffset;		// 扇区剩余空间大小
	
	//如uiAddr = 0x01067004,
	//则uiSectorAddr = 0x01067000,
	//  uiSectorOffset = 4
	//  uiSectorRemain = 4092
#if 0	
	dbg("uiSectorAddr:%08x",uiSectorAddr);//0x60000
	dbg("uiSectorOffset:%08x",uiSectorOffset);//800
	dbg("uiSectorRemain:%d",uiSectorRemain);//2048
	dbg("nLen:%d",nLen);
#endif	
	
	nSectorNum = nLen/FLASH_SECTOR_SIZE + 1;
		
	uiNumByteToMoreErase = nSectorNum * FLASH_SECTOR_SIZE - nLen - uiSectorOffset;
	
	//dbg("uiNumByteToMoreErase:%d",uiNumByteToMoreErase);
	
	//刚好是扇区整数倍没有被多擦的数据不需要备份	
	if((uiNumByteToMoreErase == FLASH_SECTOR_SIZE)&&(uiSectorOffset == 0))
	{
		for(i = 0;i < nSectorNum - 1;i++)
		{
			__disable_irq();
			ROM_QSPI_EraseSector(NULL, uiAddr+i*FLASH_SECTOR_SIZE);
			__enable_irq();
		}
		
		return 0;
	}
	
	//不是扇区整数倍地址,要把该扇区前面多擦的备份起来
	if(uiSectorOffset != 0)
	{
		//1.先读出前面被多擦掉的部分,擦除后再重新写入
		flash_read(uiSectorAddr,ucTmp,uiSectorOffset);
		//dumpData("erase ahead data:",ucTmp,uiSectorOffset);
	}	
	
	if(uiNumByteToMoreErase > 0)
	{						 	
		//2.读出后面被多擦掉的部分,擦除后再重新写入
		//dbg("uiAddr:%08x,uiAddr + nLen:%08x",uiAddr,uiAddr + nLen);
		flash_read(uiAddr + nLen,ucTmp + uiSectorOffset,uiNumByteToMoreErase);
		//dumpData("ucTmp",ucTmp,uiNumByteToMoreErase);
		//DC_DelayMs(20);
		//3.执行擦除扇区
		for(i = 0;i < nSectorNum;i++)
		{
			__disable_irq();
			ROM_QSPI_EraseSector(NULL, uiAddr+i*FLASH_SECTOR_SIZE);
			__enable_irq();
		}
		//dbg("uiAddr:%08x",uiAddr);
		//3.恢复被多擦的数据
		flash_program(uiSectorAddr,ucTmp,uiSectorOffset);
		flash_program(uiAddr + nLen,ucTmp + uiSectorOffset,uiNumByteToMoreErase);	
	}
	else//跨越2扇区的情况
	{
		//2.读出后面被多擦掉的部分,擦除后再重新写入
		uiNumByteToMoreErase = nSectorNum * FLASH_SECTOR_SIZE + FLASH_SECTOR_SIZE - nLen - uiSectorOffset;
		//dbg("uiNumByteToMoreErase:%d",uiNumByteToMoreErase);
		flash_read(uiAddr + nLen,ucTmp + uiSectorOffset,uiNumByteToMoreErase);
		//3.执行擦除扇区
		for(i = 0;i < nSectorNum + 1;i++)
		{
			__disable_irq();
			ROM_QSPI_EraseSector(NULL, uiAddr+i*FLASH_SECTOR_SIZE);
			__enable_irq();
		}
		//3.恢复被多擦的数据
		flash_program(uiSectorAddr,ucTmp,uiSectorOffset);
		flash_program(uiAddr + nLen,ucTmp + uiSectorOffset,uiNumByteToMoreErase);	
	}
	//dbg("----erase ok--------\r\n");
	return 0;
}
/*********************************************************************
* 名  称		:		flash_erase
* 描  述		:		擦除数据
* 输  入		:		uiAddr		:	地址
* 						nLen		:	清长度
*				:		ucDeValue	:   清为该值
* 输  出		:		NULL
* 返  回		:		0  成功 1 失败
* 修  改		:		----
*********************************************************************/
int flash_erase(u32 uiAddr,u32 nLen, unsigned char ucDeValue)
{
	u32 uiAddr_ch = uiAddr;
	u32 uiPageAddr = 0;
	u32 uiNumByteToWrite = nLen;
	u16 usPageOffset = 0;
	u16 uiPageRemain = 0;
	u16 i = 0;
	u8 tmp[X25Q_PAGE_SIZE];
	u8 ucStatus = 0x00;
	//u8 * buf = (u8 *)pSrc;
	QSPI_CommandTypeDef cmdType;
	
	if ((uiAddr_ch < NOR_FLASH_ADRR_START) || ((uiAddr_ch + nLen) - 1 > NOR_FLASH_ADRR_END))
	{
		return 0x01;
	}
	
	halFlashErase(uiAddr_ch,nLen);
	
	dbg("uiAddr:%08x",uiAddr);	

	uiPageAddr = uiAddr_ch & 0xFFFFFF00;			//uiAddr_ch / NOR_FLASH_PAGE_SIZE;	// 扇区地址
	usPageOffset = uiAddr_ch & (X25Q_PAGE_SIZE-1);		// 在扇区内的偏移
	uiPageRemain = X25Q_PAGE_SIZE - usPageOffset;		// 扇区剩余空间大小
#if 0	
	dbg("uiPageAddr:%08x",uiPageAddr);	
	dbg("usPageOffset:%08x",usPageOffset);
	dbg("uiPageRemain:%d",uiPageRemain);
#endif	
	if (uiNumByteToWrite <= uiPageRemain)
	{
		uiPageRemain = uiNumByteToWrite;					// 不大于4096个字节
	}
	
    cmdType.Instruction = QUAD_INPUT_PAGE_PROG_CMD;
    cmdType.BusMode = QSPI_BUSMODE_114;	  //Q-out mode    
    cmdType.CmdFormat = QSPI_CMDFORMAT_CMD8_ADDR24_PDAT; 
	
	while (1)
	{
		flash_read(uiPageAddr, tmp, X25Q_PAGE_SIZE);
		for (i=0; i<uiPageRemain; i++)						// 复制
		{
			tmp[i + usPageOffset] = ucDeValue;
		}
		//dbg("uiPageAddr:%08x",uiPageAddr);
		//MH_CommandTypeDef sCommand;		
		__disable_irq();
		ROM_QSPI_ProgramPage(&cmdType, DMA_Channel_0, uiPageAddr, sizeof(tmp), (uint8_t*)(tmp));		
		__enable_irq();
		uiPageAddr += X25Q_PAGE_SIZE;
		usPageOffset = 0;									// 偏移位置为0
		//buf += uiPageRemain;								// 指针偏移
	//	uiAddr_ch += uiPageRemain;							// 写地址偏移
		uiNumByteToWrite -= uiPageRemain;					// 字节数递减
		//dbg("uiNumByteToWrite:%d",uiNumByteToWrite);
		if (uiNumByteToWrite <= 0)
		{
			break;
		}
		else if (uiNumByteToWrite > X25Q_PAGE_SIZE)
		{
			uiPageRemain = X25Q_PAGE_SIZE;				// 下一个扇区还是写不完
		}
		else
		{
			uiPageRemain = uiNumByteToWrite;				// 下一个扇区可以写完了
		}
	}
	return 0;
}
bool halFlashRead(unsigned int dwStartAddr, unsigned char * pReadBuff, unsigned int wSize)
{
	flash_read(dwStartAddr,pReadBuff,wSize);

	return 0;
}
bool halFlashWrite(unsigned int dwStartAddr, unsigned char * pWriteBuff, unsigned int wSize)
{
	int nRet = 0;

	nRet = halFlashErase(dwStartAddr,wSize);
	if(nRet != 0)
	{
		return 1;
	}
	//DC_DelayMs(500);
	nRet = flash_program(dwStartAddr,pWriteBuff,wSize);
	if(nRet<0)
	{
		dbg("nRet:%d",nRet);
		return 1;
	}
	else
	{
		return 0;
	}
}

int __flash_program(u32 uiAddr, u8 * pSrc, u32 uiWriteLen)
{
	u32 uiAddr_ch = uiAddr;
	u32 uiPageAddr = 0;
	u32 uiNumByteToWrite = uiWriteLen;
	u16 usPageOffset = 0;
	u16 uiPageRemain = 0;
	u16 i = 0;
	u8 tmp[X25Q_PAGE_SIZE];
	u8 ucStatus = 0x00;
	u8 * buf = (u8 *)pSrc;
	QSPI_CommandTypeDef cmdType;
	
	//if ((uiAddr_ch < NOR_FLASH_ADRR_START) || ((uiAddr_ch + uiWriteLen) - 1 > NOR_FLASH_ADRR_END))
	{
	//	return 0x00;
	}

	uiPageAddr = uiAddr_ch & 0xFFFFFF00;			//uiAddr_ch / NOR_FLASH_PAGE_SIZE;	// 扇区地址
	usPageOffset = uiAddr_ch & (X25Q_PAGE_SIZE-1);		// 在扇区内的偏移
	uiPageRemain = X25Q_PAGE_SIZE - usPageOffset;		// 扇区剩余空间大小
	
	dbg("uiPageAddr:%08x",uiPageAddr);	
	dbg("usPageOffset:%08x",usPageOffset);
	dbg("uiPageRemain:%d",uiPageRemain);
	
	if (uiNumByteToWrite <= uiPageRemain)
	{
		uiPageRemain = uiNumByteToWrite;					// 不大于4096个字节
	}
	
    cmdType.Instruction = QUAD_INPUT_PAGE_PROG_CMD;
    cmdType.BusMode = QSPI_BUSMODE_114;	  //Q-out mode    
    cmdType.CmdFormat = QSPI_CMDFORMAT_CMD8_ADDR24_PDAT; 
	
	while (1)
	{
		flash_read(uiPageAddr, tmp, X25Q_PAGE_SIZE);
		for (i=0; i<uiPageRemain; i++)						// 复制
		{
			tmp[i + usPageOffset] = buf[i];
		}
		//dbg("uiPageAddr:%08x",uiPageAddr);
		//MH_CommandTypeDef sCommand;		
		__disable_irq();
		ROM_QSPI_ProgramPage(&cmdType, DMA_Channel_0, uiPageAddr, sizeof(tmp), (uint8_t*)(tmp));	
		__enable_irq();
		//dbg("");
		uiPageAddr += X25Q_PAGE_SIZE;
		usPageOffset = 0;									// 偏移位置为0
		buf += uiPageRemain;								// 指针偏移
	//	uiAddr_ch += uiPageRemain;							// 写地址偏移
		uiNumByteToWrite -= uiPageRemain;					// 字节数递减
		if (uiNumByteToWrite <= 0)
		{
			break;
		}
		else if (uiNumByteToWrite > X25Q_PAGE_SIZE)
		{
			uiPageRemain = X25Q_PAGE_SIZE;				// 下一个扇区还是写不完
		}
		else
		{
			uiPageRemain = uiNumByteToWrite;				// 下一个扇区可以写完了
		}
	}
	return uiWriteLen;
}


/*
	以下是给应用用的重新封装的接口
*/
unsigned long FLASH_ERASE(u32 uiAddr,u32 uiLen)
{
	halFlashErase(uiAddr, uiLen);

	return 0;
}

unsigned long FLASH_READ(u32 uiStart, u8 *pucBuf, u32 uiReadLen)
{
	bool ret;

	ret=halFlashRead(uiStart, pucBuf, uiReadLen);
	if(!ret) 	return uiReadLen;
	else		return 0;
}

unsigned long FLASH_WRITE(u32 pucTo, u8 *pucFrom, u32 uiInLen)
{
	bool ret;

	ret=halFlashWrite(pucTo, pucFrom, uiInLen);
	if(!ret)	return uiInLen;
	else		return 0;
}













