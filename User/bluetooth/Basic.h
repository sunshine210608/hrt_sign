/**************************************************************************************************
//--------------文件信息---------------------------------------------------------------------------
// 文 件 名:    	BasicLib.h
// 版    本:		Ver1.0
// 创 建 人:		peterzheng
// 创建日期:		2012.12.14
// 文件描述:    	基本库文件，包含基本数据操作，数据格式转换等等
//=================================================================================================
//-----------------修改记录------------------------------------------------------------------------
// 修改内容:
// 当前版本:		Ver1.0
// 修 改 人:
// 修改日期:
// 注    意:
//-------------------------------------------------------------------------------------------------
**************************************************************************************************/
#ifndef _BASIC_LIB_H_
#define _BASIC_LIB_H_

//#include "Type.h"
//#include"TypeDefine.h"
#include "Define.h"

#ifdef WIN32
	#include <stdio.h>
	#include <stdlib.h>
	#include <memory.h>
	#include <string.h>
#endif
#include <string.h>

#ifdef __cplusplus
extern "C"
{
#endif


extern void memXOR(
					uint8* dest,
					uint8 btXOR,
					uint32 ulLen);


extern uint32 strslen(uint8* src);

extern uint16 crc16 (
						   uint8* dest,
				    	   uint32 ulLen);



/*
 *	BCD码转成字节流
 */
extern void Bcd2Bytes(uint8* pbBytes, uint8* pbBcd, uint16 nBcdLen);
/*
 *	字节流转成BCD码（积数个字节，在左侧补0）
 */
extern void Bytes2Bcd(uint8* pbBcd, uint8* pbBytes, uint16 nByteLen);

/*
 *	将BCD表示的银行卡账户转换成字符串（考虑尾部补F）
 *  返回字符串长度
 */
extern uint8 CardNoBCD2Str(uint8 *src,uint8 bSrcLen,uint8 *dst);

/*
*	将10进制数值转化为字符串
*/
extern bool Num2Str(uint32 dwNum, uint16 len, uint8 *dst);

/*
*	计算数据流异或值
*/
extern uint8 LRC(uint8 *pbData, uint16 wLen);

//把字符串转化为BCD码
extern void StringToBCD(uint8* src,uint8 bsrcLen,uint8* dest);

//BCD转二进制
uint32 BCD2Bin(uint8* bSrc, uint8 bLen);

//BCD转化为字符串
void BCD2Str(uint8* pbSrc, uint8 bLen, uint8* pbDest, uint8 bLimit);

void CashBCD2Str(uint8 *src,uint8 *dst);

//空指令
extern void delay(uint32 nCnt);
uint32 BCD2Bin(uint8* bSrc, uint8 bLen);
 unsigned short  CheckCRC(unsigned char * pucBuffer, unsigned int lus_CrcLength);
 
 void BcdToAsc(u8 *Dest,u8 *Src,uint32 Len);
 
 void AscToBcd(u8 *Dest,u8 *Src,uint32 Len);

void delay(uint32 nCnt);

#define SETBIT(A,bit)	A |= (bit)
#define CLRBIT(A,bit)	A &= (~(bit))

#if 1	//HS32
#define IS_EVEN(n)					(~(uint32)(n) & 1)
#define GET_BIT(iword, ibit)		(((iword)>>(ibit)) & 1)
#define SET_BIT(iword, ibit)		((iword) |= (1<<(ibit)))
#define CLEAR_BIT(iword, ibit)		((iword) &= ~(1<<(ibit)))
#define BIT_TEST(array,ibit)		GET_BIT(array[(ibit)/32], (ibit)%32)
#define BIT_SET(array,ibit)			SET_BIT(array[(ibit)/32], (ibit)%32)
#define BIT_CLEAR(array,ibit)		CLEAR_BIT(array[(ibit)/32], (ibit)%32)
#define ROTATE_LEFT(x, n)			(((x) << (n)) | ((x) >> (32-(n))))
#define ROTATE_RIGHT(x, n)			(((x) >> (n)) | ((x) << (32-(n))))
#endif

#ifdef __cplusplus
}
#endif
#endif // !_BASIC_LIB_H_

