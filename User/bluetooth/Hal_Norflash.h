/***********************************************************
* 文件名		:		Hal_Norflash.h
* 描  述		:		NOR FLASH应用接口
* 创  建		:		欧阳志强 2017.09.12
* 版  本		:		v1.0
* 修  改		:		----
***********************************************************/

#ifndef HAL_NORFLASH_H
#define HAL_NORFLASH_H

//#include "TypeDefine.h"
#include "define.h"

/***************************************************************/
/*mh ccid-boot:addr-0x01001000, size-32k
/*boot:		   addr-0x01009000, size-32k
/*firmware:    addr-0x01011000, size-300k
/*sys_param:   addr-0x0105c000, size-8k
/*key_param:   addr-0x0105e000, size-20k (TLS:10K)
/*app_param:   addr-0x01063000, size-40k
/*file_system: addr-0x0106d000, size-(512-4-32-32-300-8-20-40)k//80k
****************************************************************/

//----------------------------------------------------------
#ifdef HAL_NORFLASH_DEC
	#define HAL_NORFLASH_Ext
#else
	#define HAL_NORFLASH_Ext extern 
#endif

// 代码区域300K
#define CODE_BASE_ADDR				0x01011000 //起始68K
//#define CODE_BASE_ADDR				0x01009000 //起始36K

//#define CODE_BASE_SIZE				0x4b000//300k
//#define CODE_BASE_SIZE				0x57800//350k
//#define CODE_BASE_SIZE				0x5f000//380k
//#define CODE_BASE_SIZE				0x69000//420k
//#define CODE_BASE_SIZE				0x66800//410k
#define CODE_BASE_SIZE				0x64000//400k


//----------------------------------------------------------
#define NOR_FLASH_ADRR_START		CODE_BASE_ADDR + CODE_BASE_SIZE
#define NOR_FLASH_ADRR_END			0x0107FFFF


//系统参数,共8K
#define SYS_PARAM_ADDR				NOR_FLASH_ADRR_START + 0x1000////这个起始地址要预留4K避免flash擦除时扇区地址小于这个地址,导致flash_program直接return
#define SYS_PARAM_SIZE				0x2000//8k

//密钥区
#define KEY_PARAM_ADDR				(SYS_PARAM_ADDR + SYS_PARAM_SIZE)
#define KEY_PARAM_SIZE				0x3800//14K
//#define KEY_PARAM_SIZE				0x3000//12K
//#define KEY_PARAM_SIZE				0x5000//20K

//应用参数
#define APP_PARAM_ADDR				(KEY_PARAM_ADDR + KEY_PARAM_SIZE)
//#define APP_PARAM_SIZE				0xA000//40k
#define APP_PARAM_SIZE				0x1000//4k

//EMV文件区
#define EMV_FILE_PARAM_ADDR			(APP_PARAM_ADDR + APP_PARAM_SIZE)
#define EMV_FILE_PARAM_SIZE			0x3800//14k
//#define EMV_FILE_PARAM_SIZE			0xA000//40k

//文件系统区
#define FILE_SYSTEM_ADDR			(EMV_FILE_PARAM_ADDR + EMV_FILE_PARAM_SIZE)
//#define FILE_SYSTEM_SIZE		     0xA000//40k
#define FILE_SYSTEM_SIZE_IN_FLASH		     0x5000//20k

//------------------------------------------------------------------------------
//固件升级标识
#define PARA_DOWNFM_ADDR			SYS_PARAM_ADDR
#define PARA_DOWNFM_SIZE			2

//防拆标识
#define PARA_PCI_BROKEN_ADDR		PARA_DOWNFM_ADDR + PARA_DOWNFM_SIZE
#define PARA_PCI_BROKEN_LEN			5

//客户SN长度
#define PARA_CUSTOM_SN_LEN_ADDR				PARA_PCI_BROKEN_ADDR + PARA_PCI_BROKEN_LEN
#define PARA_CUSTOM_SN_LEN_SIZE				1

//客户SN数据
#define PARA_CUSTOM_SN_ADDR					PARA_CUSTOM_SN_LEN_ADDR + PARA_CUSTOM_SN_LEN_SIZE
#define PARA_CUSTOM_SN_LEN					24

//21号文SN长度
#define PARA_DEVICE_21NO_LEN_ADDR			PARA_CUSTOM_SN_ADDR + PARA_CUSTOM_SN_LEN
#define PARA_DEVICE_21NO_LEN_SIZE			1

//21号文SN数据
#define PARA_DEVICE_21NO_ADDR				PARA_DEVICE_21NO_LEN_ADDR + PARA_DEVICE_21NO_LEN_SIZE
#define PARA_DEVICE_21NO_LEN				64

//21号文密钥长度
#define PARA_DEVICE_21NO_KEY_LEN_ADDR		PARA_DEVICE_21NO_ADDR + PARA_DEVICE_21NO_LEN
#define PARA_DEVICE_21NO_KEY_LEN_SIZE		1

//21号文密钥数据
#define PARA_DEVICE_21NO_KEY_ADDR			PARA_DEVICE_21NO_KEY_LEN_ADDR + PARA_DEVICE_21NO_KEY_LEN_SIZE
#define PARA_DEVICE_21NO_KEY_LEN			64

//设备初始化状态
#define PARA_INIT_STATUS_ADDR		PARA_DEVICE_21NO_KEY_ADDR + PARA_DEVICE_21NO_KEY_LEN
#define PARA_INIT_STATUS_LEN		1

//是否验签固件标识
#define PARA_VERIFY_FW_FLAG_ADDR	PARA_INIT_STATUS_ADDR + PARA_INIT_STATUS_LEN
#define PARA_VERIFY_FW_FLAG_LEN		2

#define PARAM_FW_SIZE_ADDR			PARA_VERIFY_FW_FLAG_ADDR + PARA_VERIFY_FW_FLAG_LEN
#define PARAM_FW_SIZE_LEN			4

#define PARAM_FW_SIGN_HEAD_ADDR		PARAM_FW_SIZE_ADDR + PARAM_FW_SIZE_LEN
#define PARAM_FW_SIGN_HEAD_LEN		1024

/*******************************************abao add 18.04.24****************************************************************/

#define BT_CONFIG_RECORD		PARAM_FW_SIGN_HEAD_ADDR + PARAM_FW_SIGN_HEAD_LEN //标记蓝牙配置abao add 18.07.04

#define BT_CONFIG_RECORD_LEN 	2	

#define BT_DEVICE_NAME			BT_CONFIG_RECORD + BT_CONFIG_RECORD_LEN //存放蓝牙名称 abao add 18.04.24
#define BT_DEVICE_NAME_LEN		16

#define BT_DEVICE_MAC			BT_DEVICE_NAME + BT_DEVICE_NAME_LEN //存放蓝牙MAC地址 abao add 18.07.04
#define BT_DEVICE_MAC_LEN		12

#define BT_DEVICE_PINCODE		BT_DEVICE_MAC + BT_DEVICE_MAC_LEN //存放蓝牙PINCODE配对码 abao add 18.07.04
#define BT_DEVICE_PINCODE_LEN	6

#define gParam_NVDATA		   	BT_DEVICE_PINCODE + BT_DEVICE_PINCODE_LEN
#define gParam_NVDATA_Len	    96

//......
//......
//......
//todo 这里添加,然后更改下一行边界检查
#define SYS_PARAM_ADDR_END		gParam_NVDATA + gParam_NVDATA_Len

//#if (SYS_PARAM_ADDR_END - SYS_PARAM_ADDR >= SYS_PARAM_SIZE)
#if (SYS_PARAM_ADDR + SYS_PARAM_SIZE < SYS_PARAM_ADDR_END)
	#error "SYS_PARAM OVERFLOW"
#endif

/***************************************************密钥区**************************************************************************/

//根密钥
#define ROOT_KEY_HEAD_ADDR			KEY_PARAM_ADDR							// ROOT KEY 头标志
#define ROOT_KEY_HEAD_SIZE			32

#define MKEY_ADDR					ROOT_KEY_HEAD_ADDR + ROOT_KEY_HEAD_SIZE //主密钥
#define MKEY_LEN					16

#define KMS_ADDR					KEY_PARAM_ADDR + 0x100
//#deinf WKEY_ADDR

#define TLS_KEY_PARAM_START			(KEY_PARAM_ADDR + 0x2000)
//..
//..
//..
#define KEY_PARAM_ADDR_END			(DC_PASSWD_INIT_ADDR + DC_PASSWD_LEN)// TODO:


//#if ((KEY_PARAM_ADDR_END - KEY_PARAM_ADDR) >= KEY_PARAM_SIZE)
#if (KEY_PARAM_ADDR + KEY_PARAM_SIZE < KEY_PARAM_ADDR_END)
	#error "KEY_PARAM OVERFLOW"
#endif

/***************************************************应用参数区**************************************************************************/

#define APP_INFO_ADDR				APP_PARAM_ADDR			//应用信息起始地址

#define APP_TERMINAL_ADDR			APP_INFO_ADDR								// 终端号
#define APP_TERMINAL_LEN			20

#define APP_MERCHANT_ADDR			(APP_TERMINAL_ADDR + APP_TERMINAL_LEN)		// 商户号
#define APP_MERCHANT_LEN			20

#define APP_COM_INFO_ADDR			(APP_MERCHANT_ADDR + APP_MERCHANT_LEN)		// 通讯参数
#define APP_COM_INFO_LEN			100

#define APP_TLS_INDEX_ADDR			(APP_COM_INFO_ADDR + APP_COM_INFO_LEN)		// TLS证书索引
#define APP_TLS_INDEX_LEN			5

#define APP_KEY_INDEX_ADDR			(APP_TLS_INDEX_ADDR + APP_TLS_INDEX_LEN)	// 工作密钥索引
#define APP_KEY_INDEX_LEN			5

#define DC_FACTORY_INIT_ADDR		(APP_KEY_INDEX_ADDR + APP_KEY_INDEX_LEN)	// 工厂初始化标识
#define DC_FACTORY_INIT_LEN			5

#define DC_PASSWD_INIT_ADDR			(DC_FACTORY_INIT_ADDR + DC_FACTORY_INIT_LEN)// dual-control密码存储地址
#define DC_PASSWD_LEN				13
#define DC_PASSWD_SIZE				((DC_PASSWD_LEN * 2) + 2)

//......
//......
//......
//todo 这里添加,然后更改下一行边界检查

#define APP_PARAM_ADDR_END			(DC_PASSWD_INIT_ADDR + DC_PASSWD_LEN)

//#if ((APP_PARAM_ADDR_END - APP_PARAM_ADDR) >= APP_PARAM_SIZE)
#if (APP_PARAM_ADDR + APP_PARAM_SIZE < APP_PARAM_ADDR_END)
	#error "APP_PARAM OVERFLOW"
#endif

//#define FILE_SYSTEM_ADDR			0x0106d000	//FLAG_BASE_ADDR + FLAG_BASE_SIZE


void Hal_flashInit(void);
int flash_erase(uint32 uiAddr,uint32 nLen, unsigned char ucDeValue);
int halFlashErase(uint32 uiAddr,uint32 nLen);
bool halFlashRead(unsigned int dwStartAddr, unsigned char * pReadBuff, unsigned int wSize);
bool halFlashWrite(unsigned int dwStartAddr, unsigned char * pWriteBuff, unsigned int wSize);

unsigned long FLASH_ERASE(uint32 uiAddr,uint32 uiLen);
unsigned long FLASH_READ(uint32 uiStart, u8 *pucBuf, uint32 uiReadLen);
unsigned long FLASH_WRITE(uint32 pucTo, u8 *pucFrom, uint32 uiInLen);

#endif

