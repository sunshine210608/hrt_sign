#include <stdlib.h>
#include <stdio.h>
#include "mhscpu.h"
//#include "Hal_Timer.h"
//#include "GlobalVar.h"
//#include "Lcd.h"
//#include "Hal_LCD.h"

#include "yc1021.h"
#include "JSD_blueTooth.h"
#include "Hal_Norflash.h"

#include "debug.h"
//#include "Hal_Keyboard.h"
//#include "app_comm.h"
//#include "app_comm_interface.h"

JSD_BT_INFO BT_info_t;
JSD_BT_INFO BT_info_Backup_t;

u8 gsPinCode[6+1] = {0};
u8 gIsSetPinCodeFlag = 0;
extern uint8 btstatus;
extern volatile int CommucateOUT;//  蓝牙断开
extern uint8 BT_BLE_TYPE;
extern volatile uint32 g_bt_timeout;
extern uchar g_ret;

/***********************************************************
	函数名称:	JSD_BtDevOpen()
	函数功能:	打开蓝牙设备,使蓝牙模块上电，并配置成工作模式。
	入口参数:	无
	出口参数:	无
	返 回 值:	JSD_SUCCESS	0x00	成功
				JSD_ERROR	0x01	失败
	备    注:	
***********************************************************/
uchar JSD_BtDevOpen(void)
{
	return YC_BT_Dev_init();
}
/***********************************************************
	函数名称:	JSD_BtDisconnectLink()
	函数功能:	断开蓝牙设备连接。
	入口参数:	无
	出口参数:	无
	返 回 值:	
	备    注:	
***********************************************************/
void JSD_BtDisconnectLink(void)
{
	YC_DisConnect();
}
/***********************************************************
	函数名称:	uchar DC_BtDevClose(void)
	函数功能:	断开蓝牙设备连接。
	入口参数:	无
	出口参数:	无
	返 回 值:	JSD_SUCCESS	0x00	成功
				JSD_ERROR	0x01	失败
	备    注:	
***********************************************************/
uchar JSD_BtDevClose(void)
{
	YC_DisConnect();
    YC_WAKEUP_PIN_LOW();
	YC_HCI_CMD_ENTER_SLEEP_MODE();

	CommucateOUT = 2;//  蓝牙断开
	BT_BLE_TYPE = 0;
	return JSD_SUCCESS;
}
//此处好用，但功耗不行，不达标 现处于mA级别
uchar JSD_BtDev_Sleep_Mode(void)
{
	YC_DisConnect();
	YC_HCI_CMD_ENTER_SLEEP_MODE();

	CommucateOUT = 2;//  蓝牙断开
	BT_BLE_TYPE = 0;
	return JSD_SUCCESS;
}

/************************************************************************************************
	函数名称:	uchar JSD_SetBTInfo(DC_BT_INFO *info,uchar flag,uchar enforce)
	函数功能:	设置本地蓝牙设备信息。
	入口参数:	flag:
				1.	JSD_BT_SDC_DISCOVERABLE	0x01 	设置可见性
				2.	JSD_BT_SDC_NAME			0x02	设置蓝牙名称
				3.	JSD_BT_SDC_PINCODE		0x04	设置蓝牙PINcode
				enforce:
				1.	CONFIG_ONE_TIME			0x01	配置项仅出厂设置一次，若之前已经配置过，则配置不执行。
				2.	CONFIG_ENFORCE			0x02	强制配置，不论配置是否执行过，强制执行当前设置。
	出口参数:	无	
	返 回 值:	JSD_SUCCESS		0x00	成功
				JSD_ERROR		0x01	失败
				JSD_ERRPARAM 	0x8B	参数错误
				JSD_CONFIG_DONE	0x24	配置项出厂已经设置
	备    注:	
************************************************************************************************/
uchar JSD_SetBTInfo(JSD_BT_INFO *info,uchar flag,uchar enforce)
{
	u8 BT_ConfRecord[2]={0};
	
	//@@@ CONFIG_ONE_TIME模式 检测 @@@ 
	halFlashRead(BT_CONFIG_RECORD,BT_ConfRecord, BT_CONFIG_RECORD_LEN);
	
	//@@@ 已经配置过了 还想进行第一次配置 则直接退出 @@@ 
	if((BT_ConfRecord[0] == 0x55)&&(BT_ConfRecord[1] == 0xAA)&&(enforce == CONFIG_ONE_TIME))
	{	return JSD_CONFIG_DONE;}	

	//@@@ CONFIG_ONE_TIME 第一次 或 CONFIG_ENFORCE 强制配置 @@@ 
	if(enforce == CONFIG_ONE_TIME || enforce == CONFIG_ENFORCE)
	{
		if((flag&0xFF) == 0){return JSD_ERRPARAM;}
					
		//	1.设置蓝牙名称
		if(flag & JSD_BT_SDC_NAME)		
		{	
			if(strlen(info->ucBTName)==NULL){return JSD_ERRPARAM;}
			if(YC_Set_Name(info->ucBTName, strlen(info->ucBTName)) == JSD_SUCCESS)
			{
				//halFlashWrite(BT_DEVICE_NAME,info->ucBTName, BT_DEVICE_NAME_LEN);
				halFlashWrite(BT_DEVICE_NAME,info->ucBTName, strlen(info->ucBTName));
			}
			else {return JSD_ERROR;}
		}
		
		//	2.设置蓝牙 MAC_ADDR地址
		if(flag & JSD_BT_SDC_BT_MAC)	
		{	
			if(info->ucBTMac==NULL){return JSD_ERRPARAM;}	
			if(YC_Set_ADD(info->ucBTMac)==JSD_SUCCESS)
			{	
				halFlashWrite(BT_DEVICE_MAC,info->ucBTMac, BT_DEVICE_MAC_LEN);
			}
			else {return JSD_ERROR;}
		}

		//	3.设置可见性
		if(flag & JSD_BT_SDC_DISCOVERABLE)
		{	
			if(YC_HCI_CMD_SET_VISIBILITY(info->ucBTDiscoveralbe))
				return JSD_ERROR;
		}		
		
		//	4.设置蓝牙 PINcode 文档要求4字节
		if(flag & JSD_BT_SDC_PINCODE)		
		{	
			if(info->ucBTPincode==NULL){return JSD_ERRPARAM;}	

			YC_HCI_CMD_SET_PAIRING_MODE(pincode);

			//Dc_ClearLine(2);
			//Dc_ClearLine(3);
			LcdCleanLine(2);
			LcdCleanLine(3);
			UpdatePincode();
			#if 0
			if(YC_HCI_CMD_SET_PINCODE(info->ucBTPincode,sizeof(info->ucBTPincode))==DC_SUCCESS)
			{
				halFlashWrite(BT_DEVICE_PINCODE,info->ucBTPincode, BT_DEVICE_PINCODE_LEN);
			}
			else {return DC_ERROR;}
			#endif
		}
		//标记已经设置过了
		BT_ConfRecord[0]=0x55;
		BT_ConfRecord[1]=0xAA;
		halFlashWrite(BT_CONFIG_RECORD,BT_ConfRecord, BT_CONFIG_RECORD_LEN);
	}
	return JSD_SUCCESS;
}

uchar DC_GetBTInfo(JSD_BT_INFO *info)
{
	u8 ucBTMac[12];
	u8 ucBtName[20+1] = {0};
	u8 ucBtPincode[4+1] = {0};
	u8 ucFWverion[2] = {0};
	uint16 Version;
	
	//1. 蓝牙名称获取
	halFlashRead(BT_DEVICE_NAME,ucBtName, BT_DEVICE_NAME_LEN);		
	memcpy(info->ucBTName,ucBtName,BT_DEVICE_NAME_LEN);
	
	//2. 蓝牙 MAC_ADDR地址
	halFlashRead(BT_DEVICE_MAC,ucBTMac, BT_DEVICE_MAC_LEN);	
	memcpy(info->ucBTMac,ucBTMac,BT_DEVICE_MAC_LEN);
	
	//3. 获取蓝牙固件版本号
	YC_HCI_CMD_VERSION_REQUEST(&Version);
	ucFWverion[0]=Version&0xFF;
	ucFWverion[1]=(Version&0xFF00)>>8;
	memcpy(info->ucFWVersion,ucFWverion,sizeof(info->ucFWVersion));

	//4. 获取蓝牙可见状态
	info->ucBTDiscoveralbe = (btstatus && HCI_EVENT_STATUS_BT_FUOND_ENALBED);

	//5. 获取配对PINCODE
	halFlashRead(BT_DEVICE_PINCODE,ucBtPincode, BT_DEVICE_PINCODE_LEN);
	memcpy(info->ucBTPincode,ucBtPincode,BT_DEVICE_PINCODE_LEN);
	
	return JSD_SUCCESS;
}

uchar DC_GetBTLinkStatus(int *status)
{

	YC_HCI_CMD_STATUS_REQUEST();//查询当前蓝牙的状态
	//Bt_Detect_ConnectState();

	if((CommucateOUT==0||CommucateOUT==2) && ((btstatus&&HCI_EVENT_STATUS_BT_CONNECTED)||(btstatus&&HCI_EVENT_STATUS_BLE_CONNECTED)))
	{
		*status = JSD_BTCONNECTED;
  	}
	else if((CommucateOUT==1)||((btstatus && 0x30)==0))
	{
		*status = JSD_BTDISCONNECTED;
  	}
 	else
  	{
		*status = JSD_BTPOWERDOWN;
  	}

	return JSD_SUCCESS;
}

u8 DC_ucSetBTParamConfig(u8 cmd, u8 *data)
{
	return JSD_SUCCESS;
}

//???? && ???? && ?? && //????
//!!! ????????????,????? !!!
void DC_BT_demo2(void)
{
	u8 disp_flag = 1;
	u8 keyvalue = 0;
	int nBtStatus = JSD_BTDISCONNECTED;
	u8 ucBtName[BT_DEVICE_NAME_LEN+1]  = "P86_huangxl";
	u8 ucBtMac[BT_DEVICE_MAC_LEN+1] 	= "201810228889";
	u8 ucPinCode[BT_DEVICE_PINCODE_LEN] = {0x31,0x32,0x33,0x34,0x35,0x36};
	u8 *sTempBuf = NULL;
	u16 nLen = 0;
	u16 nReadLen = 0;
	int ret = 0;
	
	sTempBuf = malloc(3*1024);
	if(NULL == sTempBuf)
		return ;
#if 0		
//	DC_BtDevClose();
	ret = DC_BtDevOpen();
	dbg("-----open bt :%d------",ret);
	//DC_BtDisconnectLink();

	memset((u8 *)&BT_info_t, 0 ,sizeof(BT_info_t));
	
	BT_info_t.ucBTDiscoveralbe = DC_BT_DISCOVERABLE;
	memcpy(BT_info_t.ucBTMac, ucBtMac,BT_DEVICE_MAC_LEN);
	memcpy(BT_info_t.ucBTName,(char*)ucBtName,strlen(ucBtName));
	memcpy(BT_info_t.ucBTPincode,ucPinCode,BT_DEVICE_PINCODE_LEN);
#endif
	//DC_Cls();
	//DC_Displays(1,0, DISP_EFFECT_CENTER_HORI,BT_info_t.ucBTName);
	LcdClear();
	LcdPutsc(BT_info_t.ucBTName, 1);

	//DC_SetBTInfo(&BT_info_t,DC_BT_SET_ALL,CONFIG_ENFORCE);
	//DC_GetBTInfo(&BT_info_Backup_t);
	while(1)
	{
		DC_GetBTLinkStatus(&nBtStatus);
		//dbg("nBtStatus:%d",nBtStatus);
		if(nBtStatus == JSD_BTCONNECTED)
		{
			if(disp_flag)
			{
				//DC_ClearNoTitle();
				//DC_Displays(1,0, DISP_EFFECT_CENTER_HORI,"BT Connected");
				LcdClear();
				LcdPutsc("BT Connected", 1);
				disp_flag = 0;
			}
			//接收头
			nLen = 3;//02 xx xx
			nReadLen = JSD_BT_Recieve_Data(sTempBuf,nLen,500);			
			if(nReadLen > 0) 
			{
				hexdumpEx("BT RECV head:",sTempBuf,nReadLen);
				nLen = sTempBuf[1] << 8;
				nLen += sTempBuf[2];
				dbg("nLen:%d",nLen);
				nReadLen = JSD_BT_Recieve_Data(sTempBuf + nReadLen,nLen,30000);	
				hexdumpEx("BT RECV:",sTempBuf,nLen + 3);
				ret = JSD_BT_Send_Data(sTempBuf,nLen + 3);
				dbg("send ret:%d",ret);
			}
			
		}
		else if((nBtStatus == JSD_BTDISCONNECTED) || (nBtStatus == JSD_BTPOWERDOWN))
		{
			if(!disp_flag)
			{
				//DC_ClearNoTitle();
				//DC_Displays(1,0, DISP_EFFECT_CENTER_HORI,"BT Disconnected");
				LcdClear();
				LcdPutsc("BT Disconnected", 1);
				disp_flag = 1;
			}
		}
		//keyvalue = DC_InkeyMs(10);
		//if(keyvalue == KBV_ESC)
		//	goto exit;
		if(IsPressCancelKey() == TRUE)
		{
			goto exit;
		}
	}
	
exit:
	free(sTempBuf);
	sTempBuf = NULL;
	//DC_BtDevClose();
}

void DC_BT_Init(void)
{
	u8 ret = 0;
	u8 ucBtName[BT_DEVICE_NAME_LEN+1] = {0};//  = "P86_leon";
	u8 ucBtMac[BT_DEVICE_MAC_LEN+1] 	= "201810108888";
	//u8 ucPinCode[BT_DEVICE_PINCODE_LEN] = {0x31,0x32,0x33,0x34};
	u8 ucPinCode[BT_DEVICE_PINCODE_LEN] = {0x31,0x31,0x31,0x31};


//	if(DC_GetSN(ucBtName) != 0)
	if (1)
	{
		strcpy(ucBtName,"TEST");
	}
	else
	{
		memcpy(&ucBtMac[4],&ucBtName[7],8);
	}
	//DC_Cls();
	//DC_Displays(0,0, DISP_EFFECT_CENTER_HORI|DISP_EFFECT_HIGHLIGHT,"      欢迎使用       ");	
	//DC_Displays(1,0, DISP_EFFECT_CENTER_HORI,"系统启动中...");
	LcdClear();
	LcdPutsc("      欢迎使用       ", 0);
	LcdPutsc("系统启动中...", 1);
	
	//DC_BtDevClose();
	//ret = YC_Get_Ready();
	//if(ret != YC_SUCCESS)
	memset((u8 *)&BT_info_t, 0 ,sizeof(BT_info_t));
	
	BT_info_t.ucBTDiscoveralbe = JSD_BT_DISCOVERABLE;
	memcpy(BT_info_t.ucBTMac, ucBtMac,BT_DEVICE_MAC_LEN);
	memcpy(BT_info_t.ucBTName,(char*)ucBtName,strlen(ucBtName));
	memcpy(BT_info_t.ucBTPincode,ucPinCode,BT_DEVICE_PINCODE_LEN);
	if(1)
	{
		//Dc_ClearLine(2);
		//DC_Displays(2,0, DISP_EFFECT_CENTER_HORI,"正在初始化蓝牙...");	
		LcdPutsc("正在初始化蓝牙...", 2);
		ret = JSD_BtDevOpen();
		if(ret != 0)
		{
			//Dc_ClearLine(2);
			//DC_Displays(2,0, DISP_EFFECT_CENTER_HORI,"BT Init Failed");	
			LcdClear();
			LcdPutsc("BT Init Failed", 2);
			mdelay(300);
			//PowerOff();
			//while(1);
			return;
		}
	}
	else
	{
		//YC_DisConnect();
		//Dc_ClearLine(2);
		//DC_Displays(2,0, DISP_EFFECT_CENTER_HORI,"BT Init Succeed");	
		//DC_DelayMs(300);
		LcdClear();
		LcdPutsc("BT Init Succeed", 2);
		mdelay(300);
	}
//	Dc_ClearLine(1);
//	DC_Displays(1,0, DISP_EFFECT_CENTER_HORI,ucBtName);
	

	JSD_SetBTInfo(&BT_info_t,JSD_BT_SET_ALL,CONFIG_ENFORCE);	
	dbg("BT INIT OK\r\n");
	
}

void SetBtPincode(void)
{
	u8 sPinCode[6+1] = {0};
	u16 nLen = 0x0606;

	if(gIsSetPinCodeFlag)
	{
		return;
	}
	//DC_ClearNoTitle();
	//DC_Displays(0,0, DISP_EFFECT_NORMAL,"请输入pincode:");
	//if(DC_InputString(1,0,sPinCode,nLen,0,60000) == 0)
	LcdClear();
	//LcdPutsc("请输入pincode:", 2);
	if (emv_inputPort("请输入pincode:", sPinCode) == 0)
	{
		hexdumpEx("sPinCode:",sPinCode,strlen(sPinCode));
		if(YC_HCI_CMD_SET_PINCODE(sPinCode,strlen(sPinCode))==JSD_SUCCESS)
		{
			halFlashWrite(BT_DEVICE_PINCODE,sPinCode, BT_DEVICE_PINCODE_LEN);
		}
	}
	//Dc_ClearLine(0);
	//Dc_ClearLine(1);
	LcdCleanLine(0);
	LcdCleanLine(1);

	gIsSetPinCodeFlag = 1;
		
}
void DispPinCode(void)
{	
	u8 sPinCodeDisBuf[20+1] = {0};

	dbg("sPinCodeAsc:%s,gIsSetPinCodeFlag:%d",gsPinCode,gIsSetPinCodeFlag);

	//断开时显示pincode
	if(CommucateOUT != 0)
	{
		sprintf(sPinCodeDisBuf,"BT pin:%s",gsPinCode);
		LcdCleanLine(4);
		LcdPutsc(sPinCodeDisBuf, 4);
		//Dc_ClearLine(4);
		//DC_Displays(4,0, DISP_EFFECT_CENTER_HORI,sPinCodeDisBuf);
	}	
	else
	{
		if(gIsSetPinCodeFlag)
		{
			dbg("----");
			LcdCleanLine(4);
			//Dc_ClearLine(4);
		}
	}
}
void DispBtName(void)
{	
	u8 sDispBuf[50] = {0};

	sprintf(sDispBuf,"蓝牙:MPOS-%s",BT_info_t.ucBTName+5);
	
	//DC_ClearNoTitle();
	//DC_Displays(2,0, DISP_EFFECT_CENTER_HORI,sDispBuf);
	LcdClear();
	LcdPutsc(sDispBuf, 2);
}
void UpdatePincode(void)
{
	u8 sPinCode[3+1] = {0};
	u8 sPinCodeAsc[6+1] = {0};

	dbg("CommucateOUT:%d,gIsSetPinCodeFlag:%d",CommucateOUT,gIsSetPinCodeFlag);
	
	//连上时不做更新pincode
	if(CommucateOUT == 0)
	{
		dbg("----have setted pin code-----");
		return;
	}	
	else//断开需要更新pincode;
	{
		
		if(!gIsSetPinCodeFlag)
		{
			//DC_GetRandom(sPinCode,sizeof(sPinCode)-1);
			rand_hardware_byte_ex(sPinCode, sizeof(sPinCode)-1);
			BcdToAsc(sPinCodeAsc,sPinCode,sizeof(sPinCode)-1);
			hexdumpEx("sPinCodeAsc:",sPinCodeAsc,sizeof(sPinCodeAsc)-1);
			if(YC_HCI_CMD_SET_PINCODE(sPinCodeAsc,strlen(sPinCodeAsc))==JSD_SUCCESS)
			{
				halFlashWrite(BT_DEVICE_PINCODE,sPinCodeAsc, BT_DEVICE_PINCODE_LEN);
				memcpy(gsPinCode,sPinCodeAsc,sizeof(sPinCodeAsc)-1);
				gIsSetPinCodeFlag = 1;
			}
		}
		else
		{
			//halFlashRead(BT_DEVICE_PINCODE,sPinCode, BT_DEVICE_PINCODE_LEN);		
			
		}		

	}
}
