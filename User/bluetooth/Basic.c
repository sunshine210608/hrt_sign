/**************************************************************************************************
//--------------文件信息---------------------------------------------------------------------------
// 文 件 名:    	BasicLib.c
// 版    本:		Ver1.0
// 创 建 人:		peterzheng
// 创建日期:		2012.12.14
// 文件描述:    		基本库文件，包含基本数据操作，数据格式转换等等
//=================================================================================================
//-----------------修改记录------------------------------------------------------------------------
// 修改内容:
// 当前版本:		Ver1.0
// 修 改 人:
// 修改日期:
// 注    意:
//-------------------------------------------------------------------------------------------------
**************************************************************************************************/
#include "Basic.h"

void memXOR (
						   uint8* dest,
						   uint8 btXOR,
						   uint32 ulLen)
{
	while (ulLen--)
		dest[ulLen] ^= btXOR;
}


uint32 strslen(uint8* src)
{
	uint32 i = 0;

	while(src[i] != 0)
	{
		i++;
	}

	return i+1;
}

uint16 crc16 (  uint8* dest, uint32 len)
{

	uint32 b,s_s,i;
	uint32 j;
	uint16 crc = 0x0000;
	for(j=0;j<len;j++)
	{
		s_s=dest[j];
		for(i=0;i<8;i++)
		{
			b=crc&1;
			if(s_s&1)
				b^=1;
			if(b)
				crc^=0x4002;
			crc>>=1;
			if(b)
				crc|=0x8000;
			s_s>>=1;
		}
	}
	return crc;
}



void Bcd2Bytes(uint8* pbBytes, uint8* pbBcd, uint16 nBcdLen)
{
	uint16 i;
	for (i=0;i<nBcdLen;i++)
	{
		*pbBytes++ = (uint8)(*pbBcd >> 4);
		*pbBytes++ = (uint8)((*pbBcd) & 0xF);
		pbBcd++;
	}
}

void Bytes2Bcd(uint8* pbBcd, uint8* pbBytes, uint16 nByteLen)
{
	uint16 i;

	if (nByteLen & 1)
	{
		*pbBcd++ = *pbBytes++;
		nByteLen--;
	}
	for (i=0;i<nByteLen;i+=2)
	{
		*pbBcd = (uint8)((*pbBytes) << 4);
		pbBytes++;
		*pbBcd++ |= (uint8)(*pbBytes++);
	}
}

uint8 CardNoBCD2Str(uint8 *src,uint8 bSrcLen,uint8 *dst)
{
	uint8 *sign=(uint8*)"0123456789ABCDEF";
	uint16 i=0;
	uint8 bStrlen= 0;
	for( i=0;i<bSrcLen;i++)
	{
		if( (src[i] & 0x0F ) == 0x0F )
		{
			*dst=sign[src[i]>>4];		dst++;bStrlen++;
			break;
		}

		*dst=sign[src[i]>>4];		dst++;bStrlen++;
		*dst=sign[src[i]&15];		dst++; bStrlen++;
	}

	return bStrlen;

}

bool Num2Str(uint32 dwNum, uint16 len, uint8 *dst)
{
	uint8 cTmp;
	uint16 i;

	if(!dst)
		return FALSE;

	for(i=0; i<len; i++)
	{
		cTmp = dwNum%10;
		dwNum /= 10;
		dst[len-i-1] = cTmp + '0';
	}
	dst[len] = 0;//写字符串结束标志

	return TRUE;
}
#if 0
uint8 LRC(uint8 *pbData, uint16 wLen)
{
	uint8 bResult = 0;

	for(; wLen>0; wLen--)
	{
		bResult ^= pbData[wLen-1];
	}

	return bResult;
}
#endif
#if 0
uint8 CharToHex(uint8 ch)
{
	if(ch>='0'&&ch<='9')
		return ch-'0';
	if(ch>='a'&&ch<='f')
		return ch-'a'+10;
	if(ch>='A'&&ch<='F')
		return ch-'A'+10;
	return 0xFF;
}

uint8 StringToHex(uint8* src,uint16* nLen,uint16 *dst)
{
	uint16 i,len=0;
	uint8 ch,cl;
	uint16 vlen = 0;
	while(src[len]!='\0')
		len++;
	vlen=0;
	for(i=0;i<len;i+=2)
	{
		ch=CharToHex(src[i]);
		cl=CharToHex(src[i+1]);
		if(ch==0xFF||cl==0xFF)
		{

			return FALSE;
		}
		dst[i/2]=((ch<<4)|(cl&15));
		vlen++;
	}
	*nLen = vlen;
	return TRUE;
}
#endif
//把字符串转化为BCD码
void StringToBCD(uint8* src,uint8 bsrcLen,uint8* dest)
{
	uint8 i = 0 ;
	for( i = 0 ; i < ((bsrcLen/2)+(bsrcLen%2));i++)
	{
		dest[i] = 0x00;
		dest[i] = src[i*2]<<4;
		if( (i*2 + 1) == bsrcLen )
			dest[i] |= 0x0F;
		else
		   dest[i] |= (src[i*2+1]&0x0F) ;
	}

}

uint32 BCD2Bin(uint8* bSrc, uint8 bLen)
{
	uint32 nBin = 0;
	uint32 nStep = 0;
	uint8 i, j,n,p;

	for(i=0; i<bLen; i++)
	{
		if(i)
		{
			nStep = 1;
			for(j = i; j != 0; j--)
				nStep *= 100;
		}
		else
		{
			nStep = 1;
		}

		n = bSrc[bLen - 1 -i];

		p = (n>>4)*10 + (n&0xF);

		nBin += p * nStep;
	}

	return nBin;
}

//BCD转化为字符串
//注意:bLimit是pbDest目标地址的最大长度
void BCD2Str(uint8* pbSrc, uint8 bLen, uint8* pbDest, uint8 bLimit)
{
	uint8*		sign = (uint8*)"0123456789ABCDEF";
	uint16	i=0;
	uint16	j=0;

	if(!pbSrc || !pbDest || !bLen || !bLimit)
		return;

	for( i=0; i<bLen; i++)
	{
		if(j+2 >= bLimit)
			break;
		pbDest[j++] =sign[(pbSrc[i]>>4)&0xF];
		pbDest[j++] =sign[pbSrc[i]&0xF];
	}

	pbDest[j] = 0;	//pbDest字符串结尾置'\0'
}

// 000000100000 "1000.00"
void CashBCD2Str(uint8 *src,uint8 *dst)
{
	uint8 *sign=(uint8*)"0123456789ABCDEF";
	uint16 i=0;
	bool bFlag = FALSE;//去掉金额中第有效数字前的0
	for( i=0;i<6;i++)
	{
		if( (src[i] ==0x00) && (bFlag == FALSE ))
		{
			if(i== 4)
			{
				{ *dst='0';		dst++;}
				{ *dst='.';		dst++;}
			}
			continue;
		}
		if( bFlag == FALSE )
		{//第一个非零字节的高低四位要进行判断
			if( i== 5)
			{
				*dst=sign[src[i]>>4];		dst++;

				*dst=sign[src[i]&15];		dst++;
			}
			else
			{
				if( src[i] & 0xF0 )
				{*dst=sign[src[i]>>4];		dst++;}

				*dst=sign[src[i]&15];		dst++;
			}

			bFlag = FALSE;


			if( i == 4 )
			{ *dst='.';		dst++;}
			continue;
		}

		*dst=sign[src[i]>>4];		dst++;
		*dst=sign[src[i]&15];		dst++;
		if( i == 4 )
		{ *dst='.';		dst++;}
	}
	if( bFlag == FALSE)//6字节0
	{

		*dst='0';		dst++;
		*dst='0';		dst++;
	}
}
uint16 DEC2BCD(uint8* Pbcd)//define function
{
	uint16 i;
	i  = 	(Pbcd[0]>>4) *1000;
	i +=    ((Pbcd[0]&0xF) *100); 
	
	i += 	((Pbcd[1]>>4) *10);
	i += 	(Pbcd[1]&0xF);   
	return i;
}
uint16 Get16Len(uint8* pLen)
{
	uint16 i;
	i  = pLen[0] *256;
	i += pLen[1];   
	return i;
}

uint16 BCD2DEC(uint16 wLen)
{
    uint8  bNum[4]={0,0,0,0};
	uint8  bHigh=0;
	uint8  bLow=0;
	bNum[0]  = 	(wLen/1000)%10;
	bNum[1]  = 	(wLen/100)%10;
	bNum[2]  = 	(wLen/10)%10;
	bNum[3]  = 	wLen%10;
	bHigh    = 	(bNum[0]<<4)+bNum[1];
	bLow     = 	(bNum[2]<<4)+bNum[3];
	return ((bHigh<<8) +bLow);
}

//void delay(uint32 nCnt)
//{
//	while(nCnt--);
//}
//extern    const unsigned short Crctab_Itu[];
const unsigned short Crctab_Itu[] = //CRC余式表  start value = 0xFFFF
{
        0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
		0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
		0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
		0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
		0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
		0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
		0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
		0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
		0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
		0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
		0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
		0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
		0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
		0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
		0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
		0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
		0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
		0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
		0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
		0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
		0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
		0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
		0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
		0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
		0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
		0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
		0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
		0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
		0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
		0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
		0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
		0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78,
}; 


unsigned short  CheckCRC(unsigned char * pucBuffer, unsigned int lus_CrcLength)
{
    unsigned short lus_CrcRes = 0xffff;    // 初始化
	
    do {
        lus_CrcRes = (lus_CrcRes >> 8) ^ Crctab_Itu[(lus_CrcRes ^ (*pucBuffer)) & 0xff];
        lus_CrcLength--;
        pucBuffer++;
    } while(lus_CrcLength != 0); 
    
    lus_CrcRes = ~lus_CrcRes;

    return 	lus_CrcRes;
   
   
}

/*******************************************************************
函数名称: void BcdToAsc(u8 *Dest,u8 *Src,u32 Len)
函数功能: 将压缩BCD码转换为ascii码
入口参数: 1.ascii码地址; 2.压缩BCD数组地址; 3.Bcd码字节个数
返 回 值: 无
相关调用:
备    注: Dest地址为Len的两倍  
修改信息:         
********************************************************************/
void BcdToAsc(u8 *Dest,u8 *Src,uint32 Len)
{
    uint32 i;
	for(i=0;i<Len;i++)
	{
	    //高Nibble转换
	    if(((*(Src + i) & 0xF0) >> 4) <= 9)
	    {
            *(Dest + 2*i) = ((*(Src + i) & 0xF0) >> 4) + 0x30;
        }
        else
        {
            *(Dest + 2*i)  = ((*(Src + i) & 0xF0) >> 4) + 0x37 + 0x20;   //大写A~F //huangxl +0x20转出是小写字母
        }    
        //低Nibble转换
        if((*(Src + i) & 0x0F) <= 9)
        {
            *(Dest + 2*i + 1) = (*(Src + i) & 0x0F) + 0x30;
        }
        else
        {
            *(Dest + 2*i + 1) = (*(Src + i) & 0x0F) + 0x37 + 0x20;   //大写A~F
        }    
    }
}


/*******************************************************************
函数名称: void AscToBcd(u8 *Dest,u8 *Src,u32 Len)
函数功能: 将ascii码转换为压缩BCD码
入口参数: 1.压缩bcd数组地址; 2.ascii码地址; 3.ascii字节个数
返 回 值: 无
相关调用:
备    注: 末尾不够补0x00,非ascii码填0x00  
修改信息:       
********************************************************************/
void AscToBcd(u8 *Dest,u8 *Src,uint32 Len)
{
    uint32 i;
    u8 high = 0,low = 0;
    for(i = 0; i < Len; i++) 
    {
        //待转bcd码高Nibble
	    if((*(Src + i) >= 0x61) && (*(Src + i) <= 0x66))      //range a~f
	    {
	        high = *(Src + i) - 0x57;
	    }
	    else if((*(Src + i) >= 0x41) && (*(Src + i) <= 0x46))  //range A~F
	    {
	        high = *(Src + i) - 0x37;
	    }
	    else if((*(Src + i) >= 0x30) && (*(Src + i) <= 0x39) || *(Src + i)==0x3d)  //range 0~9
	    {
	        high = *(Src + i) - 0x30;   
	    }
        else
        {
            high = 0x00 ;                                       //其他
        }
        
        //待转bcd码低Nibble
        i++;
        if(i < Len)
        {
	        if((*(Src + i) >= 0x61) && (*(Src + i) <= 0x66))    //range a~f
	        {
	            low = *(Src + i) - 0x57;
            }
            else if((*(Src + i) >= 0x41) && (*(Src + i) <= 0x46)) //range A~F
            {   
                low = *(Src + i) - 0x37;
	    	}
	    	else if((*(Src + i) >= 0x30) && (*(Src + i) <= 0x39) || *(Src + i)==0x3d)  //range 0~9
		    {
		        low = *(Src + i) - 0x30;
	        }
	        else
	        {
	            low = 0x00 ;                                       //其他
		    }
	    }
	    else
	    {
	        i--;                                                //预防255个时溢出出错
	        low = 0x00 ;                                       //如果是奇数个末尾补0x00 
	    }
        *(Dest + i/2) = (high << 4) | low;                      //合并BCD码
    }
}

void delay(uint32 nCnt)
{
	while(nCnt--);
}
