#include <string.h>
#include <time.h>
#include "mhscpu.h"
#include "SysTick.h"
#include "pmu.h"
#include "sensor.h"
#include "tms_cmd.h"
#include "tms_flow.h"
#include "debug.h"
#include "define.h"
#include "sys_littlefs.h"
#include "wifi_at.h"
#include "vposface.h"
#include "pub.h"
#include "mbedssl_cli.h"
#include "des_sec.h"
#include "battery.h"
#include "util.h"
#include "lcdgui.h"
#include "gprs_at.h"
#include "tcpComm.h"
#ifdef APP_LKL
#include "AppGlobal_lkl.h"
#endif
#ifdef APP_SDJ
#include "AppGlobal_sdj.h"
#endif
#if 1//def APP_CJT
#include "AppGlobal_cjt.h"
#endif

//#define USE_COMPRESS
#ifdef USE_COMPRESS
#include "minilzo.h"
#endif

#include "pub.h"


//"JUSTDO-TECH.AMK."
#define USN_FIXED_TEST_VALUE	"123456789012345678901"
//#define SERVER_AUTH_KEY_VALUE	"12345678901234561234567890123456"

#ifdef APP_SDJ
//主密钥对shengdijia分散后的值:
//#define SERVER_AUTH_KEY_VALUE	"EA2C68B3619EEF6DBF074CC173798F16"      //奇偶校验后
#define SERVER_AUTH_KEY_VALUE	"EA2D69B3609FEE6DBE064CC073788E17"
#endif
#ifdef APP_LKL
//主密钥对lakala分散后的值:
#define SERVER_AUTH_KEY_VALUE	"2C3B919E80AD8AE9ABCEFB3E5D259E32"      //奇偶校验后
#endif
#ifdef APP_CJT
//主密钥对lakala分散后的值:
#define SERVER_AUTH_KEY_VALUE 	"9E20764986E3A42CA120138C25CBEFAB"
#define CUSTOMER    "herongtong"
#endif

#define LOCATION_MAC_KEY_VALUE	"12345678901234561234567890654321"

static unsigned char sg_sActiveId[30];
static int  sg_iFirstAct;

int TMS_RemoteUpdate_Flow(void);

int TMS_UpdateLocation_Flow(void);

int TMS_Activate_Flow(int isFirstTimeRequest, unsigned char *serverUIDIn, int serverUIDInLen, 
	unsigned char *serverUIDOut, int *serverUIDOutLen);


int Sys_GetUsn(byte *uSN, int *uSNLen)
{
	byte TerminalSN[32+1];
	
	*uSNLen = 32;
	
	memset(uSN, 0x00, sizeof(TerminalSN));
	
#if 1 //ndef TMS_DEBUG_FIXED_KEY
	SecReadTerminalSN(TerminalSN);
	memcpy(uSN, TerminalSN, strlen(TerminalSN));	
#else
	//AtoHex(TerminalSN, USN_FIXED_TEST_VALUE, strlen(USN_FIXED_TEST_VALUE));
	strcpy(uSN, USN_FIXED_TEST_VALUE);
    //strcpy((char *)uSN, "00007302120001000240");
#endif
	//memcpy(uSN, TerminalSN, OTP_TERMINAL_SN_LEN);

	return 0;
}

//#define CUR_APPLICATION_VERSION	"0.0.01"

int Sys_GetCurAppVer(byte *curAppVer, int *curAppVerLen)
{
	*curAppVerLen = 8;
	memset(curAppVer, 0x00, 8);
	memcpy(curAppVer, APP_VERSION, strlen(APP_VERSION));

	return 0;
}

#define CUR_LOCATION_VALUE	"32,56"

int Sys_GetLocationValue(byte *curLocation, int *curLocationLen)
{
	char szLocation[100];
	int ret;

	*curLocationLen = 32;

	memset(curLocation, 0x00, 32);
	//Jason added for test 2020/1/2
	ret = gprs_at_GetLocationFlow(szLocation);
	if (ret != 0)
	{
		dbg("gprs_at_GetLocationFlow return = %d\n", ret);
		return -1;
	}else
	{
		dbg("szLocation = %s\n", szLocation);
	}
	memcpy(curLocation, szLocation, strlen(szLocation));
	//memcpy(curLocation, CUR_LOCATION_VALUE, strlen(CUR_LOCATION_VALUE));
	//Jason added end
	return 0;
}

#define TMS_HOST_IP		"47.106.13.245"
#define TMS_HOST_PORT	"6666"
#define TMS_HOST_DOMAIN "www.justdo-tech.com"

int TMS_Connect(void)
{
	int ret;
    char ip[30];

	//ret = wifi_at_tcp_connect(TMS_HOST_IP, TMS_HOST_PORT);
	ret=iCommTcpConn(TMS_HOST_IP, TMS_HOST_PORT, 60*1000);
    if(ret)
    {
        //用域名
        ip[0]=0;
        if( iGetHostByName(TMS_HOST_DOMAIN, ip) )
		{
			//vMessage("解析域名失败");
			return -1*COM_PARAMETER;
		}
        if(ip[0])
            ret=iCommTcpConn(ip, TMS_HOST_PORT, 60*1000);
    }
	if (ret != 0)
	{
		err("wifi_at_tcp_connect return %d\n", ret);
		return -1;
	}

	return 0;
}

int TMS_Disconnet(void)
{
	//wifi_at_DisconnTcp();
	iCommTcpDisConn();

	return 0;
}

#define TMS_AUTH_TDES_KEY	"1234567890123456"
#define TMS_LOCATION_MAC_TDES_KEY	"1234567890654321"

int TMS_Matual_Auth()
{
	int ret;
	byte uSN[32] = {0};
	int	uSNLen;
	byte rand[32];
	int randLen;
	byte encRand[32];
	byte decRand[32];
	int encRandLen;
	byte status;
	byte serverAuthKey[16];

	Sys_GetUsn(uSN, &uSNLen);
	randLen = 32;
	rand_hardware_byte_ex(rand, randLen);

	ret = Tms_Cmd_AuthServer(uSN, uSNLen, rand, randLen, CUSTOMER, &status, encRand, &encRandLen);
	if (ret != 0)
	{
        //vMessageVarArg("Tms_Cmd_AuthServer ret:%d", ret);
		err("Tms_Cmd_AuthServer return %d\n", ret);
		return -1;
	}
	if (status != CMD_STATUS_OK)
	{
		err("Tms_Cmd_AuthServer status = [0x%02x]\n", status);
		return -2;
	}
	hexdumpEx("List encRand", encRand, encRandLen);
#ifndef TMS_DEBUG_FIXED_KEY
	SecReadServerAuthKey(serverAuthKey);
#else
	AtoHex(serverAuthKey, SERVER_AUTH_KEY_VALUE, strlen(SERVER_AUTH_KEY_VALUE));
#endif
	ucl_tdes_ecb(encRand, encRandLen, serverAuthKey, decRand, 0);
	//ucl_tdes_ecb(encRand, encRandLen, TMS_AUTH_TDES_KEY, decRand, 0);
	hexdumpEx("List decRand", decRand, encRandLen);
	if (memcmp_ex(decRand, rand, randLen)!= 0)
	{
		err("decRand != rand\n");
		return -3;
	}

	ret = Tms_Cmd_PedAskServerRand(uSN, uSNLen, &status, rand, &randLen);
	if (ret != 0)
	{
		err("Tms_Cmd_PedAskServerRand return %d\n", ret);
		return -4;
	}
	if (status != CMD_STATUS_OK)
	{
		err("Tms_Cmd_PedAskServerRand status = [0x%02x]\n", status);
		return -5;
	}

	
	ucl_tdes_ecb(rand, randLen, serverAuthKey, encRand, 1);
	//ucl_tdes_ecb(rand, randLen, TMS_AUTH_TDES_KEY, encRand, 1);

	ret = Tms_Cmd_PedAskServerAuth(uSN, uSNLen, encRand, encRandLen, &status);
	if (ret != 0)
	{
        //vMessageVarArg("Tms_Cmd_PedAskServerAuth ret:%d", ret);
		err("Tms_Cmd_PedAskServerAuth return %d\n", ret);
		return -6;
	}
	if (status != CMD_STATUS_OK)
	{
		err("Tms_Cmd_PedAskServerAuth status = [0x%02x]\n", status);
		return -7;
	}
	
	return 0;
}

int TMS_RemoteUpdate_Flow(void)
{	
	int ret;
	byte uSN[32] = {0};
	int	uSNLen;
	byte curAppVer[8];
	int curAppVerLen;
	byte status;
	byte updateResult;
	byte serverAppVer[8];
	int serverAppVerLen;
	byte fileID[10];
	int fileIDLen;
	int fileSize;
	int fileOffset;
	byte filePackData[1024];
	//byte filePackData[512];
	short filePackRcvLen;
	lfs_file_t newAppFile;
	lfs_file_t updateFlagFile;
	char szUpdateAppNewPath[128] = {0};
	char szUpdateFlagPath[128] = {0};
	char szBuf[30];

	_vCls();
	vDispCenter(1, "更新程序", 1);
	
	_vDisp(2, (uchar*)"连接服务器...");
	ret = TMS_Connect();
	if (ret != 0)
	{
		strcpy(szBuf, "连接服务器失败");
		err("TMS_Connect return %d\n", ret);
		ret = -1;
		goto EXIT_TMS;
	}

	_vDisp(2, (uchar*)"认证...");
	ret = TMS_Matual_Auth();
	if (ret != 0)
	{
		strcpy(szBuf, "认证失败");
		err("TMS_Matual_Auth return %d\n", ret);
		ret = -2;
		goto EXIT_TMS;
	}
	
	_vDisp(2, (uchar*)"检查版本...");
	strcpy(szBuf, "检查版本失败");
	Sys_GetUsn(uSN, &uSNLen);
	Sys_GetCurAppVer(curAppVer, &curAppVerLen);

	ret = Tms_Cmd_Check_Update(uSN, uSNLen, curAppVer, curAppVerLen, &status,
		&updateResult, serverAppVer, &serverAppVerLen, fileID, &fileIDLen);
	if (ret != 0)
	{
		err("Tms_Cmd_Check_Update return %d\n", ret);
		ret = -1;
		goto EXIT_TMS;
	}
	if (status != CMD_STATUS_OK)
	{
		err("Tms_Cmd_Check_Update status = [0x%02x]\n", status);
		ret = -2;
		goto EXIT_TMS;
	}
	if (updateResult == TMS_CHECK_UPDATE_RESULT_NO_UPDATE)
	{
		strcpy(szBuf, "已是最新版本,无需升级");
		info("TMS No need update\n");
		ret = 1;
		goto EXIT_TMS;
	}

	_vDisp(2, (uchar*)"正在下载...");
	strcpy(szBuf, "下载失败");
	ret = Tms_Cmd_Start_Download(0x10, fileID, fileIDLen, &status, &fileSize);
	if (ret != 0)
	{
		err("Tms_Cmd_Start_Download return %d\n", ret);
		ret = -1;
		goto EXIT_TMS;
	}
	if (status != CMD_STATUS_OK)
	{
		err("Tms_Cmd_Start_Download status = [0x%02x]\n", status);
		ret = -2;
		goto EXIT_TMS;
	}

	sprintf(szUpdateAppNewPath, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_NEW_APP);
	sys_lfs_file_open(&newAppFile, szUpdateAppNewPath, 
		LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
	
	fileOffset = 0;
	dbg("fileSize = %d\n", fileSize);
	
	while(fileOffset < fileSize)
	{
		vDispVarArg(3, "下载: %d/%d", fileOffset, fileSize);
		ret = Tms_Cmd_RcvPackage(fileOffset, sizeof(filePackData), &status,
			filePackData, &filePackRcvLen);
		if (ret != 0)
		{
			err("Tms_Cmd_RcvPackage return %d\n", ret);
			sys_lfs_file_close(&newAppFile);
			ret = -1;
			goto EXIT_TMS;
		}
		if (status != CMD_STATUS_OK)
		{
			err("Tms_Cmd_RcvPackage status = [0x%02x]\n", status);
			sys_lfs_file_close(&newAppFile);
			ret = -2;
			goto EXIT_TMS;
		}

		fileOffset+= filePackRcvLen;
		sys_lfs_file_write(&newAppFile, filePackData, filePackRcvLen);
	}

	sys_lfs_file_close(&newAppFile);
	ret = Tms_Cmd_End_Download(&status);
	if (ret != 0)
	{
		err("Tms_Cmd_End_Download return %d\n", ret);
		ret = -1;
		goto EXIT_TMS;
	}
	if (status != CMD_STATUS_OK)
	{
		err("Tms_Cmd_End_Download status = [0x%02x]\n", status);
		ret = -2;
		goto EXIT_TMS;
	}
	
	sprintf(szUpdateFlagPath, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_APP_FLAG);
	sys_lfs_file_open(&updateFlagFile, szUpdateFlagPath, 
		LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
	sys_lfs_file_write(&updateFlagFile, UPDATE_FLAG_NEED_UPDATE, 
		strlen(UPDATE_FLAG_NEED_UPDATE));
	sys_lfs_file_close(&updateFlagFile);
	
	//查看下载文件
	//sys_lfs_dir_list(DIR_UPDATE);
	sys_lfs_file_open(&updateFlagFile, szUpdateFlagPath, LFS_O_RDONLY);
	sys_lfs_file_read(&updateFlagFile, szBuf, 
		strlen(UPDATE_FLAG_NEED_UPDATE));
	sys_lfs_file_close(&updateFlagFile);
	dbg("read UPDATE_FLAG:%d\n", szBuf[0]);

	ret = 0;
	strcpy(szBuf, "更新成功,需重启POS");
	dbg("Download OK");	
	
	vClearLines(2);
	_vDisp(2, szBuf);

	//if(getBatteryVol()<UPDATE_VOL_PHASE)
    //if(battery_get_voltage()<BATTERY_1_MIN)
    if(_uiCheckBattery()<=1)
	{
		vDispMid(3, "电量不足,不能重启");
		//_vDisp(4, "请先充电,超50%可重启");
        vDispMid(4, "请先充电");
		strcpy(szBuf, " ");
	}else
	{
		if(makeRebootFlag()==0)
		{
			vDispMid(3, "准备重启POS");
			systemReboot();
		}
	}
	
EXIT_TMS:

	TMS_Disconnet();
	vClearLines(2);
	vMessage(szBuf);
	
	return ret;
}

extern void systemReboot(void);
extern int makeUpdateFlag(void);
extern uint uiMemManaPutSysData(void);
void vRebootforUpdate(uchar ucForceInsFlag)
{
	lfs_file_t updateFlagFile;
	char szUpdateFlagPath[128] = {0};
	
	//写更新标志
	sprintf(szUpdateFlagPath, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_APP_FLAG);
	sys_lfs_file_open(&updateFlagFile, szUpdateFlagPath, LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
	sys_lfs_file_write(&updateFlagFile, UPDATE_FLAG_NEED_UPDATE, strlen(UPDATE_FLAG_NEED_UPDATE));
	sys_lfs_file_close(&updateFlagFile);

#if 0//ndef APP_LKL
	gl_SysData.ulTmsDlStart=0;
	uiMemManaPutSysData();
#endif
	makeUpdateFlag();

	vClearLines(3);
	vDispMid(2, "下载成功");

    _vFlushKey();
	//准备重启
	while(1)
	{
		if(_uiCheckBattery()<3)
		{
			vDispMid(3, "电量不足,不能重启");
			//vDispMid(4, "请先充电,超50%可重启");
            vDispMid(4, "请先充电");
			_vDelay(10);
			continue;
		}

		//if(ucForceInsFlag==0)
		//{
		//	_vDisp(3, "");
		//	vDispMid(4, "按任意键立即重启");
		//	if(iGetKeyWithTimeout(15))
		//		break;
		//}
		break;
	}
    
    vDispMid(4, "重启更新时请勿断电");
    vMessageEx("准备重启...", 150);

	systemReboot();
}

//为了减少内存占用，压缩和解压缩必须分段处理，文件前部自定义保存了分段信息
//格式: "mLZO"+解压后文件大小fileSize[4]+段尺寸segSize[2]+段数据长度segLen[2]+段数据seg[2n]+压缩后数据data[]
int iUnCompressMiniLZOFile(char *pszFile)
{
#ifdef USE_COMPRESS
	lfs_file_t AppFile;
	lfs_file_t UnzipAppFile;
    int ret;
    
    unsigned long buf[1024+100], unbuf[1024+100];       //定义4096字节变量空间(segSize),用long保证4字节对齐
    unsigned char tmp[100];
    unsigned short segment[1024]={0};
    char szUnzipFile[100];

    int fileSize, segSize, n;
    unsigned long len;
    int fileLen;
    
    if (lzo_init() != LZO_E_OK)
    {
        err("lzo_init fail.\n");
        vMessage("文件解压初始化失败");
        return -1;
    }
    ret=sys_lfs_file_open(&AppFile, pszFile, LFS_O_RDONLY);
    if(ret)
        return -2;
        
    ret=sys_lfs_file_read(&AppFile, tmp, 12);
    if(ret<12)
    {
        sys_lfs_file_close(&AppFile);
        return -3;
    }
    if(memcmp(tmp, "mLZO", 4)!=0)   //非压缩格式,无需解压
    {
        sys_lfs_file_close(&AppFile);
        return 0;
    }
    fileSize=(tmp[4]<<24) | (tmp[5]<<16) | (tmp[6]<<8) | tmp[7];
    segSize=tmp[8]*256+tmp[9];
    len=tmp[10]*256+tmp[11];
    if(fileSize<=0 || segSize<=0 || len<=0 || len>=sizeof(segment) || segSize>sizeof(unbuf))
    {
        sys_lfs_file_close(&AppFile);
        return -4;
    }
    ret=sys_lfs_file_read(&AppFile, segment, len);
    if(ret!=len)
    {
        sys_lfs_file_close(&AppFile);
        return -5;
    }
    
    sprintf(szUnzipFile, "%s/%s", DIR_UPDATE, FILE_NAME_UNZIP_APP);
    ret=sys_lfs_file_open(&UnzipAppFile, szUnzipFile, LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
    if(ret)
    {
        sys_lfs_file_close(&AppFile);
        return -6;
    }
    
    fileLen=0;
    for(n=0; segment[n]>0; n++)
    {
        if(segment[n]>sizeof(buf))
            break;
        ret=sys_lfs_file_read(&AppFile, buf, segment[n]);
        if(ret!=segment[n])
            break;
        ret = lzo1x_decompress((unsigned char*)buf, segment[n], (unsigned char*)unbuf, &len, NULL);
        if(ret || len>sizeof(unbuf))
            break;
        //写文件
        sys_lfs_file_write(&UnzipAppFile, unbuf, len);
        fileLen+=len;
    }
    
    sys_lfs_file_close(&AppFile);
    sys_lfs_file_close(&UnzipAppFile);
    
    if(fileLen!=fileSize)
        return 1;
    else
    {
        sys_lfs_remove(pszFile);
        sys_lfs_rename(szUnzipFile, pszFile);
        return 0;
    }
#else
    return 1;
#endif
}

int TMS_Activate_Flow(int isFirstTimeRequest, byte *serverUIDIn, int serverUIDInLen, 
	byte *serverUIDOut, int *serverUIDOutLen)
{	
	int ret;
	byte uSN[32] = {0};
	int	uSNLen;
	byte curLocation[32];
	int curLocationLen;
	byte curLocationMac[8];
	byte status;
	byte actResult;
	char szBuf[30];
	byte locationMacKey[16];

	_vCls();
	//vDispCenter(1, "解锁POS", 1);
    vDispCenter(1, "联机处理", 1);
	
	_vDisp(2, (uchar*)"获取位置信息...");
#ifndef VPOS_APP    
	strcpy(szBuf, "获取位置信息失败");
	ret = Sys_GetLocationValue(curLocation, &curLocationLen);
	if (ret != 0)
	{
		err("Sys_GetLocationValue return %d\n", ret);
        
		ret = -1;
		goto EXIT_ACT;       
	}
#else
    memset(curLocation, 0, sizeof(curLocation));
    curLocationLen=32;
    strcpy((char*)curLocation,"113.921443,22.551855");
#endif

	_vDisp(2, (uchar*)"连接服务器...");
	TMS_Connect();

	_vDisp(2, (uchar*)"认证...");
	strcpy(szBuf, "认证失败");
	ret = TMS_Matual_Auth();
	if (ret != 0)
	{
		//err("TMS_Matual_Auth return %d\n", ret);
        sprintf(szBuf, "认证失败:%d", ret);
		ret = -1;
		goto EXIT_ACT;
	}
	
	_vDisp(2, (uchar*)"请求处理...");
	strcpy(szBuf, "处理失败");
	Sys_GetUsn(uSN, &uSNLen);
	//Sys_GetLocationValue(curLocation, &curLocationLen);
#ifndef TMS_DEBUG_FIXED_KEY
	SecReadLocationMacKey(locationMacKey);
#else
	AtoHex(locationMacKey, LOCATION_MAC_KEY_VALUE, strlen(LOCATION_MAC_KEY_VALUE));
#endif

	ucl_tdes_mac(locationMacKey, curLocation, curLocationLen, curLocationMac);
	//ucl_tdes_mac(TMS_LOCATION_MAC_TDES_KEY, curLocation, curLocationLen, curLocationMac);
    
	ret = Tms_Cmd_Ask_Server_Activate(uSN, uSNLen, 
		curLocation, curLocationLen, 
		curLocationMac, 8,
        CUSTOMER, checkIfBPKReset()?2:1,        //请求上送:2-电池触发 1-拆机触发
        serverUIDIn, serverUIDInLen, 
		&status,
		&actResult, 
		serverUIDOut, serverUIDOutLen);
	if (ret != 0)
	{
		err("Tms_Cmd_Check_Update return %d\n", ret);
        sprintf(szBuf, "处理失败:%d", ret);
		ret = -1;
		goto EXIT_ACT;
	}
	if (status != CMD_STATUS_OK)
	{
		err("Tms_Cmd_Check_Update status = [0x%02x]\n", status);
		ret = -2;
		goto EXIT_ACT;
	}
	if (actResult == TMS_ACTIVATE_RESULT_PROCESSING)
	{
		strcpy(szBuf, "请联系售后处理");
		info("TMS_ACTIVATE_RESULT_PROCESSING\n");
		ret = 1;
		//goto EXIT_ACT;
	}else if (actResult == TMS_ACTIVATE_RESULT_APPROVED)
	{
		strcpy(szBuf, "处理成功.");
		info("TMS_ACTIVATE_RESULT_APPROVED\n");
		ret = 0;
		//goto EXIT_ACT;
	}else
	{
		strcpy(szBuf, "处理失败,服务器拒绝");
		err("actResult = [0x%02x]\n", actResult);
		ret = -3;
		//goto EXIT_ACT;
	}
	
EXIT_ACT:

	TMS_Disconnet();
	vClearLines(2);
	vMessage(szBuf);
	
	return ret;	
}

int TMS_UpdateLocation_Flow(void)
{	
	int ret;
	byte uSN[32] = {0};
	int	uSNLen;
	byte curLocation[32];
	int curLocationLen;
	byte curLocationMac[8];
	byte status;
	byte locationMacKey[16];
	char szBuf[30];

	_vCls();
	vDispCenter(1, "上传位置信息", 1);
	
	_vDisp(2, (uchar*)"连接服务器...");
	TMS_Connect();

	_vDisp(2, (uchar*)"获取位置信息...");
	strcpy(szBuf, "获取位置信息失败");
	ret = Sys_GetLocationValue(curLocation, &curLocationLen);
	if (ret != 0)
	{
		err("Sys_GetLocationValue return %d\n", ret);
		ret=-1;
		goto EXIT_LOC;
	}
	
	_vDisp(2, (uchar*)"认证...");
	strcpy(szBuf, "认证失败");
	ret = TMS_Matual_Auth();
	if (ret != 0)
	{
		err("TMS_Matual_Auth return %d\n", ret);
		ret=-1;
		goto EXIT_LOC;
	}
	
	_vDisp(2, (uchar*)"上传位置信息.....");
	strcpy(szBuf, "上传位置失败");
	Sys_GetUsn(uSN, &uSNLen);
	//Sys_GetLocationValue(curLocation, &curLocationLen);

	SecReadLocationMacKey(locationMacKey);
	ucl_tdes_mac(TMS_LOCATION_MAC_TDES_KEY, curLocation, curLocationLen, curLocationMac);	
	//ucl_tdes_mac(locationMacKey, curLocation, curLocationLen, curLocationMac);

	ret = Tms_Cmd_Update_Location(uSN, uSNLen, 
		curLocation, curLocationLen, 
		curLocationMac, 8,
		&status);
	if (ret != 0)
	{
		err("Tms_Cmd_Update_Location return %d\n", ret);
		ret=-1;
		goto EXIT_LOC;
	}
	if (status != CMD_STATUS_OK)
	{
		err("Tms_Cmd_Update_Location status = [0x%02x]\n", status);
		ret=-2;
		goto EXIT_LOC;
	}

	ret=0;
	strcpy(szBuf, "上传位置成功");
	
EXIT_LOC:	
	TMS_Disconnet();
	vClearLines(2);
	vMessage(szBuf);
	
	return ret;
}

#ifndef NO_WIFI
extern uchar ucGetCommInitDisp(void);
//static int iTimeFlag=1;
int wifiInitAndConnAP2(char *wifiName, char *wifiPwd)
{
#if 0	
	unsigned char tmp[1024];
	int ret;
	tick t1;
	char szLocalIp[20], szMac[20];
	
	t1=get_tick();
	wifi_init();
	
	wifi_hardware_poweron();

	dbg("After power on\n");
	ret = wifi_read(tmp, 1024, 5*1000);
	dbg("After power on wifi_read[%d] = [%s]\n", ret, tmp);
	//hexdump(tmp, ret);
    wifi_at_ctsrts();
    
	if (wifi_at_DisableEcho() != 0)
	{
		dbg("wifi_at_DisableEcho error\n");
		ret = -1;
        //return ret;
	}

    //wifi_at_ctsrts();
	//return 0;

	if (wifi_at_SetStationMode() != 0)
	{
		dbg("wifi_at_SetStationMode error\n");
		ret = -2;
        //return ret;
	}
	
	if (wifi_at_ConnectAp() != 0)
	{
		dbg("wifi_at_ConnectAp error\n");
		//return -3;
	}
	
	//wifi_at_setRecvMode(1);
	
	//ret=wifi_at_QueryAPConnStatus(NULL);
	
	ret=wifi_at_get_localIP(szLocalIp, szMac);
	if(ret==0)
		dbg("ip=%s, mac=%s\n", szLocalIp, szMac);
	
	
	dbg("**** wifi start time=%ld ms\n", get_tick()-t1);
	
	/*
	ret=wifi_at_tcp_connect("192.168.2.113", 5566);
	
	strcpy(tmp, "this is a test msg.");
	wifi_at_tcp_send(tmp,strlen(tmp));
	
	ret=wifi_at_DisconnTcp();
	
	ret=wifi_at_DisconnAp();
	*/
	return ret;
#else
	int ret;
	
	//初始化wifi模块
#ifndef FUDAN_CARD_DEMO
	//LcdPutsc("初始化WIFI,请稍候", 0);
    if(ucGetCommInitDisp())
    {
        //LcdClear();
        vClearLines(2);
        vDispMid(3, "初始化WIFI,请稍候");
    }
#else
    LcdPutsc("wifi init, pls wait...", 0);
#endif
	ret=wifi_appLayer_Init();
	if(ret)
		return ret;
	/*
	if(gl_SysInfo.iInitFlag!=PARAM_FLAG || gl_SysInfo.szWifiName[0]==0)
	{
		dbg("\nwifi name is Invalid !!!\n\n");
		vMessage("请先设置wifi参数");
		return 1;
	}
	*/
	if(wifiName && wifiName[0])
	{
		ret=wifi_at_ConnectAp2(wifiName, wifiPwd);
		if(ret)
		{
			dbg("wifi connect fail.\n");
            //vMessage("wifi连接失败");
			return 2;
		}
		
		wifi_at_get_localIP(NULL, NULL);
	}
	return 0;
	
#endif	
	
}


int wifiInitAndConnAP(void)
{
	return wifiInitAndConnAP2(gl_SysInfo.szWifiNameUtf8, gl_SysInfo.szWifiPwd);
}

#endif

void TMS_UnitTest(void)
{
	dbg("Enter TMS_UnitTest()\n");

	//wifiInitAndConnAP();
	
	TMS_RemoteUpdate_Flow();
}

int TMS_RemoteUpdate(void *p)
{
	_vCls();
	vDispCenter(1, "更新程序", 1);
	
    if(_uiCheckBattery()<=1)
	{
		_vDisp(2, "电量不足,不能更新");
		//vMessage("请先充电,超50%可更新");
        vMessage("请先充电");
        
		return 1;
	}
	
	_vDisp(2, (uchar*)"连接服务器...");
	
	return TMS_RemoteUpdate_Flow();
}


int TMS_Activate(void *p)
{
	int iLenIn, iLenOut=0;
	uchar sActiveIdTmp[30]={0};
	int ret;

	_vCls();
	vDispCenter(2, "检测机器状态", 0);
	if (checkIfSensorWorksWell() == TRUE)
	{
		_vCls();
		vDispCenter(3, "POS状态良好,无须激活", 0);
		bPressKey();

		return -1;
	}
	if (checkIfSensorTampering(2000) == TRUE )
	{
		_vCls();
		vDispCenter(3, "请先修复POS", 0);
		bPressKey();

		return -1;
	}else
	{
		dbg("checkIfSensorOccurring return false\n");
	}
    
#if 1
	_vCls();
	//vDispCenter(2, "解锁POS", 0);
    vDispCenter(2, "硬件故障,需联机处理", 0);
	_vDisp(3, (uchar*)"连接服务器...");
	//wifiInitAndConnAP();
	
	if(sg_iFirstAct!=1)
	{
		memset(sg_sActiveId, 0, sizeof(sg_sActiveId));
	}
	iLenIn=strlen((char*)sg_sActiveId);
	
	ret=TMS_Activate_Flow(sg_iFirstAct, sg_sActiveId, iLenIn, 
		sActiveIdTmp, &iLenOut);
	if(ret==1)
	{
		if(iLenOut>0 && iLenOut<sizeof(sActiveIdTmp))
			sActiveIdTmp[iLenOut]=0;
		strcpy((char*)sg_sActiveId, (char*)sActiveIdTmp);
		sg_iFirstAct=1;
	}else
	{
		sg_iFirstAct=0;
	}
#else
    ret=0;
#endif

	//if (ret == 0 || ret == 1)
	if (ret == 0)
	{
		sensorEnable();

		vClearLines(3);
		_vDisp(3, "正在重启POS...");
		systemReboot();
	}
	
	return ret;
}

int TMS_UpdateLocation(void *p)
{
	_vCls();
	vDispCenter(1, "上传位置", 1);
	
	//_vDisp(2, (uchar*)"连接服务器...");
	//wifiInitAndConnAP();
	
	return TMS_UpdateLocation_Flow();
}

int TMS_OPTest(void *p)
{
#if 0    
	int ret;
	unsigned char send[1024];
	unsigned char recv[2048];
	char szSvrIP[30];
	char szSvrPort[30];
	int  iSvrPort;
	
	//服务器地址端口
	GetServerIPAndPort(szSvrIP, szSvrPort);
	iSvrPort = atoi(szSvrPort);
	//strcpy(szSvrIP, "192.168.2.113");
	//iSvrPort=4455;
	
	//客户端证书和key
	char cli_cert[]="-----BEGIN CERTIFICATE-----\r\n"
"MIIDWTCCAkGgAwIBAgIEElgeuDANBgkqhkiG9w0BAQsFADBdMRgwFgYDVQQDEw8x\r\n"
"OTIuMTY4LjE2OS4yNTExDDAKBgNVBAsTA2RldjEMMAoGA1UEChMDanNkMQswCQYD\r\n"
"VQQHEwJTWjELMAkGA1UECBMCR0QxCzAJBgNVBAYTAkNOMB4XDTE5MTIyNzA3MTIx\r\n"
"MFoXDTI5MTIyNDA3MTIxMFowXTEYMBYGA1UEAxMPMTkyLjE2OC4xNjkuMjUxMQww\r\n"
"CgYDVQQLEwNkZXYxDDAKBgNVBAoTA2pzZDELMAkGA1UEBxMCU1oxCzAJBgNVBAgT\r\n"
"AkdEMQswCQYDVQQGEwJDTjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB\r\n"
"AI3PU7ZWCWxBcKf+2W9aY3UJnlc/2YGhiwOpEVp/lGp+MyB2FH5X7GJ7+K38i1MB\r\n"
"hrlSr42tPbNxOA2E8OD2YAD8QX8A4Y7rimJwI+HIi9HH+KuYO/FMlrd644gl6O4Q\r\n"
"Jh+YqpFIvYhh3k1EmtGhbgNfFXIag/IDFoIPaxsSqFmdIOIrB7qJxfDpKLxmarM6\r\n"
"FUGO8mYK0cjBZ1+UPvk/bY2xqzmDL+07PpeXqvMZQRwilh6rcqxTiI69oQZiQ13D\r\n"
"N3ZGI1P7dRUaE94zQ29Rp6GVDv086MmDkHU3q3vaIs2fneeIsoLSDM46weDip6gT\r\n"
"lY0v230eilmB4n0isML2ev0CAwEAAaMhMB8wHQYDVR0OBBYEFK/DYNUdVAOWEIY3\r\n"
"V6hCc3QqKiaXMA0GCSqGSIb3DQEBCwUAA4IBAQAtW0ZJkiYzBO+Ro8hGuQEFvqpn\r\n"
"YXlYVUfLSLMXEyUa6o1XK83bOJnqO/9DPqwjUkrjw6xwbh8TVU9zvYHh1+J+dcZS\r\n"
"wN0MmGUpBRT1Vt/NpHIPkEAopDXBpyxHNZejuhnmemwPlBOwofUa9l7xcinMbgRt\r\n"
"wl8JIOfOnpDdd4rgtYzOc74U+ROECD1Ha0CI9Rjs5WKSMBG3ZpiF4QYvU9+qOO2r\r\n"
"c6sW9NAgF6HNA0uQYDSJoZGsMnEakwMX1wT1f8rnzK3OOLNfeO5S2TL6Cvk6w7Mq\r\n"
"X+6j80yqDbd0yIgTC+johgndFUZ6BuEKHNQTfpaaCs9fQ5EiI3uBxDSgeqe9\r\n"
"-----END CERTIFICATE-----\r\n";
	
	char cli_key[]="-----BEGIN PRIVATE KEY-----\r\n"
"MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCNz1O2VglsQXCn\r\n"
"/tlvWmN1CZ5XP9mBoYsDqRFaf5RqfjMgdhR+V+xie/it/ItTAYa5Uq+NrT2zcTgN\r\n"
"hPDg9mAA/EF/AOGO64picCPhyIvRx/irmDvxTJa3euOIJejuECYfmKqRSL2IYd5N\r\n"
"RJrRoW4DXxVyGoPyAxaCD2sbEqhZnSDiKwe6icXw6Si8ZmqzOhVBjvJmCtHIwWdf\r\n"
"lD75P22Nsas5gy/tOz6Xl6rzGUEcIpYeq3KsU4iOvaEGYkNdwzd2RiNT+3UVGhPe\r\n"
"M0NvUaehlQ79POjJg5B1N6t72iLNn53niLKC0gzOOsHg4qeoE5WNL9t9HopZgeJ9\r\n"
"IrDC9nr9AgMBAAECggEAbNQ6o0j97EWAZnzPc4SDpVMfYEgQ7UoDPGjod3JHfT6i\r\n"
"j/vumFNzhtUrMKMKbUZTlS/eqDTWkDqIUN/AWHTbkoYE4anUcHFU/1yePO1CWFlF\r\n"
"/rJS9kjXtbGqolatnt1n1IG+DMctJVguWVAYHF7t5cp309yDfBxgjqm0gc9cwYAG\r\n"
"icGZFrGcO8i/sljseV32z09BA22nyBEZ1Exkc6MF2dSbfjrsbNrYwJFVL4Lflf+w\r\n"
"/+L8wmMt9ORiQ5k7k65LaGoTQ+kE5Ykltcpu9HKDVlz+oMAKPfWEdT8MfPm3Md9W\r\n"
"wglFHrpFqI1Wy+vtLnphA8aw2qmA2rW5QN2f19Bp5QKBgQDzIFQ5uZkr6zp7kk/L\r\n"
"mRh6Q1JKXA28PFgvgr/1jPThyvQnLHxBxuN0SZREgcnSMIToJ8CN52dFRFtZBHsn\r\n"
"V7kTY/Esm+04vSgJAg2zpnU5ABqFO8Xx+Ty5vJ49+7uMNyRVZwIAXgD7gPLFIYZn\r\n"
"ihoZ1LgcTJdA3aO+iLYEfMniLwKBgQCVUZ1gGC/xDGlO0PW4iQD7oT96zQ2dhfkJ\r\n"
"K5hrfCTfYnIoPSVAwtfDpmkMULlYSTLd1nv9dr8ShfCdyzqYIK5g5KRRaDKYJ1DQ\r\n"
"/1vAMiel0R0u2vJgI+vg/m0Qnlui7t8LHCrGe7xvnBbWNxFSDAeQkXw1IKAmH20F\r\n"
"asRUK2uGkwKBgQCwMtXXn/KqKagQtlCuNR3QPcn9qgkqSnF+vTtxMd6nZPbdDRhg\r\n"
"c0uUk16o54bkldU8itK6BOKLCKdLNDwsnx66NswqkDaz0CKbpKlZcWGzFagittcW\r\n"
"LpMb6N6l/TJGxA/I9QY2TepYW3OV+l3129hesBNeLwPPNtHc3CdNyUJmcQKBgQCF\r\n"
"XihatD/d5WAJ7coL0RL3rcatQIlwsUEGV9ID7xZgD+Y22qZzeZSORAx/23owyPCO\r\n"
"BA0rDu0K9mc8CVGEn2whTxcVPyQxkqw0gGDLAgE3sdeHCjiCdpMwmw9/UHI4zXKa\r\n"
"0cXDErH0Xk5ndzgZOHVpQwjVyxY/9sHBnONO6plX0QKBgQDHkzyyzQkiUXwP/CNJ\r\n"
"uiSXyhOZvnazxzH8NT9qddkmgHbzhI4R5bOf3Z2w7C96XQpqiOm64JX+JzMjQC0n\r\n"
"UqVYNohgQFI24JgdEWpRWz3oZWi+0RfDj/hEAq3mKhJYtek64xgaQs+tj0tFXoOC\r\n"
"eZl4n6P8fjY9K41htH162OuRqA==\r\n"
"-----END PRIVATE KEY-----\r\n";
	
	_vCls();
	vDispCenter(1, "OP测试", 1);
	
	//_vDisp(2, (uchar*)"连接wifi...");
	//wifiInitAndConnAP();
	
	vClearLines(2);
	_vDisp(3, (uchar*)"SSL初始化...");
	
	dbg("cli cert len=[%d]\n", strlen(cli_cert));
	dbg("cli prikey len=[%d]\n", strlen(cli_key));
	ret=iSSL_Init(NULL, cli_cert, cli_key);
	dbg("iSSL_Init ret=%d\n", ret);
	if(ret)
	{
		vMessageVarArg("SSL初始化失败:%d", ret);
		iSSL_Free();
		return ret;
	}
	
	_vDisp(3, (uchar*)"连接服务器...");
	ret=iSSL_Connect(szSvrIP, iSvrPort, 10);
	dbg("iSSL_Connect ret=%d\n", ret);
	if(ret)
	{
		vMessageVarArg("连接服务器失败:%d", ret);
		iSSL_Free();
		return ret;
	}
	
	_vDisp(3, (uchar*)"发送报文...");
	strcpy((char*)send, "GET / HTTP/1.0\r\n\r\n");
	ret=iSSL_Send(send, strlen((char*)send));
	dbg("iSSL_Send ret=%d\n", ret);
	if(ret<=0)
	{
		vMessageVarArg("SSL发送报文失败:%d", ret);
		iSSL_DisConnect();
		iSSL_Free();
		return ret;
	}
	
	_vDisp(3, (uchar*)"接收报文...");
	ret=iSSL_Recv(recv, sizeof(recv)-1, 20*1000);
	dbg("iSSL_Recv ret=%d\n", ret);
	if(ret<=0)
	{
		
		vMessageVarArg("SSL接收报文失败:%d", ret);
		iSSL_DisConnect();
		iSSL_Free();
		return ret;
	}
	recv[ret]=0;
	dbg("recv data=[%.100s]\n", recv);
	
	iSSL_DisConnect();
	iSSL_Free();
	
	vClearLines(2);
	
	_vDisp(2, "服务端回复信息:");
	_vDisp(3, recv);
	vMessage("SSL测试成功");
#endif    
	return 0;
}

extern void gprs_poweroff(void);
extern int gprs_hardware_reset(void);

int gprsInit(void)
{
	int ret;
//	int loop;
    int retry=0;

GPRS_RETRY:
    if(retry==0)
        ret=gprs_at_poweron();
    else
        ret=gprs_hardware_reset();
	if (ret < 0)
	{
		dbg("gprs_at_DisableEcho error\n");
		return -99;
	}
   	
	ret = gprs_at_DisableEcho();
	if (ret < 0)
	{
		dbg("gprs_at_DisableEcho error\n");
		return -1;
	}
	
	ret = gprs_at_CheckSIM();
	if (ret)
	{
		dbg("gprs_at_CheckSIM error\n");
        if(ret==-100)
            return -100;
		return -2;
	}
	/*
	ret = gprs_at_GetSignalQuality();
	if (ret < 0)
	{
		dbg("gprs_at_GetSignalQuality error\n");
		return -2;
	}
	*/
	ret = gprs_at_CheckRegister();
	if (ret < 0)
	{
		dbg("gprs_at_CheckRegister error\n");
        if(retry==0)
        {
            retry=1;
            goto GPRS_RETRY;
        }
		return -3;
	}

	ret = gprs_at_CheckGprsAttachment();
	if (ret < 0)
	{
		dbg("gprs_at_CheckGprsAttachment error\n");
        if(retry==0)
        {
            retry=1;
            goto GPRS_RETRY;
        }
		return -4;
	}

	ret=gprs_at_Config_Cur_Context();
	if (ret < 0)
	{
		dbg("gprs_at_Config_Cur_Context error\n");
		return -5;
	}
	
	ret = gprs_at_SetAPN((char*)gl_SysInfo.szApn, (char*)gl_SysInfo.szApnUser, (char*)gl_SysInfo.szApnPwd);
	if (ret < 0)
	{
		dbg("gprs_at_SetAPN error\n");
		return -6;
	}else if(ret>0)  //要求重启
    {
        if(ret>=998)
        {
            if(ret==999 && retry!=2)
            {
                retry=2;
            }
            goto GPRS_RETRY;
        }else
            return -6;
    }
    
	ret=gprs_at_InitPDP();
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
        if(retry==0)
        {
            retry=1;
            goto GPRS_RETRY;
        }
		return -7;
	}
	ret=gprs_at_SetRecvMode();
	if (ret < 0)
	{
		dbg("gprs_at_SetRecvMode error\n");
		return -8;
	}
	
	ret=gprs_at_SetHead();
	if (ret < 0)
	{
		dbg("gprs_at_SetHead error\n");
		return -9;
	}

	return 0;
}

#if 0
int gprs_at_GetLocationFlow(char *szLocation)
{	
	int ret;

	if(gl_SysInfo.cCommType==VPOSCOMM_GPRS)
	{
		ret = gprs_at_location(szLocation);
		if (ret < 0)
		{
			dbg("gprs_at_location error\n");
			return -6;
		}
		return 0;
	}
	
	//当前为wifi连接
	ret=gprsInit();
	//if(ret==0)
	//	ret=gprs_at_InitPDP();
	if(ret==0)
		ret=gprs_at_location(szLocation);
	gprs_at_CloseGprs();
	return ret;
}
#endif


