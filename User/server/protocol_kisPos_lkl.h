#ifndef __PROTOCOL_KISPOS_LKL_H__
#define __PROTOCOL_KISPOS_LKL_H__

#define PROTOCOL_PACKAGE_LENGTH_MAX	512

#define KISPOS_STX	'$'
#define KISPOS_ETX	'\x0d'

#define STATUS_CODE_SUCCESS		0x00
#define STATUS_CODE_ERR_TIMEOUT	0x01
#define STATUS_CODE_ERR_LRC		0x02
#define STATUS_CODE_ERR_PARA	0x03
#define STATUS_CODE_ERR_OTHER	0xff


//特殊处理定义：可变长的Attr_n型数据元素右靠齐，左补零
//#define VarAttrN_RightJustify

#define UnPackLenErr -1000 //解包后数据长度与实际长度不符

enum E_Attr
{
   Attr_UnUsed,
   Attr_n,
   Attr_z,
   Attr_b,
   Attr_a,
   Attr_ln, /*xiaf add at 202003*/
   Attr_Over
};
enum L_Attr
{
   Attr_fix,
   Attr_var1,
   Attr_var2,
};

typedef struct
{
   enum E_Attr eElementAttr;
   enum L_Attr eLengthAttr;
   unsigned int uiLength;
} FIELD_ATTR;

#define Data0_BitMapLen	2
#define Data0_01Len 19
#define Data0_02Len 19
#define Data0_03Len 6
#define Data0_04Len 12
#define Data0_05Len 19
#define Data0_06Len 6
#define Data0_07Len 12
#define Data0_08Len 19
#define Data0_09Len 6

//域必须与Data0中一致
typedef struct
{
   unsigned char BitMap[2 * Data0_BitMapLen];
   unsigned char Field02[Data0_01Len + 2];
   unsigned char Field02[Data0_02Len + 2];
   unsigned char Field03[Data0_03Len + 2];
   unsigned char Field04[Data0_04Len + 2];
   unsigned char Field05[Data0_04Len + 2];
   unsigned char Field06[Data0_04Len + 2];
   unsigned char Field07[Data0_04Len + 2];
   unsigned char Field08[Data0_04Len + 2];
   unsigned char Field09[Data0_04Len + 2];
} stKisPOSData;

typedef struct _KisPosDataField7_TMK
{
	int TmkType;
	int TmkCount;
	char TmkIndex[2][10];
	int  TmkLength[10];
	char TmkValue[100][10];
	char TmkSerial[100][10];
	
}KisPosDataField7_TMK;

typedef struct _KisPosData 
{
	unsigned char bitMap[2];
	int FieldLenArr[10];
	char Field1_Request[2];
	char Field2_Response[2];
	char Field3_KisPosTerminalSN[100];
	char Field4_NormalPosTerminalSN[100];
	char Field5_VendorID[3];
	char Field6_ApplicationID[10];
	KisPosDataField7_TMK Field7_TMK;
	char Field8_ICCardNO[100];
	char Field9_ICCardExpDate[100];
} KisPosData;

#endif
