#ifndef __TMS_CMD_H__
#define __TMS_CMD_H__

#include "protocol.h"
#include "define.h"

#define CMD_STATUS_OK						0x00
#define CMD_STATUS_FORMAT_ERR				0x02
#define CMD_STATUS_DOWNLOAD_NO_START_ERR	0x03
#define CMD_STATUS_MATUAL_AUTH_ERR			0x04
#define CMD_STATUS_OTHER_ERR				0xff

#define TMS_CHECK_UPDATE_RESULT_NO_UPDATE	0x00
#define TMS_CHECK_UPDATE_RESULT_FORCE_UPDATE 0x02
#define TMS_CHECK_UPDATE_RESULT_UPDATE		 0x01

#define TMS_ACTIVATE_RESULT_APPROVED		0x00
#define TMS_ACTIVATE_RESULT_PROCESSING		0x01
#define TMS_ACTIVATE_RESULT_MAC_ERR			0x02
#define TMS_ACTIVATE_RESULT_REFUSED			0x03

//int Tms_Cmd_AuthServer(byte *uSNIn, int uSNLen, byte *randIn, int randLen, byte*status, byte *encRandOut, int *encRandOutLen);
int Tms_Cmd_AuthServer(byte *uSNIn, int uSNLen, byte *randIn, int randLen, char *pszCustomId, byte*status, byte *encRandOut, int *encRandOutLen);

int Tms_Cmd_PedAskServerRand(byte *uSNIn, int uSNLen, byte *status, byte *randOut, int *randOutLen);

int Tms_Cmd_PedAskServerAuth(byte *uSNIn, int uSNLen, byte *encRandIn, int encRandLen, byte*status);

int Tms_Cmd_Start_Download(byte fileType, byte* fileIDIn, int fileIDLen, 
	byte*status, int *fileSizeOut);

int Tms_Cmd_RcvPackage(int packageOffset, short expRcvLen, byte * status,
	byte *packageDataOut, short *packageLenOut);

int Tms_Cmd_End_Download(byte * status);

int Tms_Cmd_Ask_Server_Activate(byte *uSNIn, int uSNLen, 
	byte *locationIn, int locationLen, byte *locationMac, int locationMacLen, 
    char *pszCustomId, byte tamperFlag,
	byte *refIDIn, int refIDInLen,
	byte *status, byte *actResult,
	byte *refIDOut, int *refIDOutLen);

int Tms_Cmd_Update_Location(byte *uSNIn, int uSNLen, 
	byte *locationIn, int locationLen, byte *locationMac, int locationMacLen, 
	byte *status);

int Tms_Cmd_Check_Update(byte *uSNIn, int uSNLen, byte *curAppVerIn, int curAppVerLen, 
	byte *status, byte *updateResult,
	byte *serverAppVerOut, int *serverAppVerLen, 
	byte *fileIDOut, int *fileIDLen);

#endif

