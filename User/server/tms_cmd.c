#include <string.h>
#include "user_projectconfig.h"
#include "define.h"
#include "tms_cmd.h"
#include "protocol.h"
#include "debug.h"
#include "misce.h"

#define WAIT_RESPONSE_TIMEOUT    (10 * 1000)

int Tms_Cmd_AuthServer(byte *uSNIn, int uSNLen, byte *randIn, int randLen, char *pszCustomId, byte*status, byte *encRandOut, int *encRandOutLen)
{
	byte sndBuf[MAX_BUFFER_SIZE];
	int sndBufLen;
	int sndIndex = 0;
	byte rcvBuf[MAX_BUFFER_SIZE];
	int rcvBufLen;
	int ret;

	sndIndex = 0;
	memcpy(sndBuf + sndIndex, uSNIn, uSNLen);
	sndIndex+= uSNLen;
	memcpy(sndBuf + sndIndex, randIn, randLen);
	sndIndex+= randLen;
    //增加16字节的客户编号
    memset(sndBuf + sndIndex, 0, 16);
    strcpy((char*)sndBuf + sndIndex, pszCustomId);
    sndIndex+=16;
	sndBufLen = sndIndex;

	ret = executecmd_ex(CMDID_PED_AUTH_SERVER, WAIT_RESPONSE_TIMEOUT, sndBuf, sndBufLen, rcvBuf, &rcvBufLen);
	if (ret != 0)
	{
		err("executecmd_ex err = %d\n", ret);
		return -1;
	}

	if (rcvBufLen != 33 && rcvBufLen != 1)
	{
		return -2;
	}else 
	{
		*status = rcvBuf[0];
		if (rcvBufLen == 33)
		{
			memcpy(encRandOut, rcvBuf + 1, 32);
			*encRandOutLen = 32;
		}else
		{
			*encRandOutLen = 0;
		}
		
		return 0;
	}
	
}

int Tms_Cmd_PedAskServerRand(byte *uSNIn, int uSNLen, byte *status, byte *randOut, int *randOutLen)
{
	byte sndBuf[MAX_BUFFER_SIZE];
	int sndBufLen;
	int sndIndex = 0;
	byte rcvBuf[MAX_BUFFER_SIZE];
	int rcvBufLen;
	int ret;

	sndIndex = 0;
	memcpy(sndBuf + sndIndex, uSNIn, uSNLen);
	sndIndex+= uSNLen;
	sndBufLen = sndIndex;

	ret = executecmd_ex(CMDID_PED_ASK_SERVER_RAND, WAIT_RESPONSE_TIMEOUT, sndBuf, sndBufLen, rcvBuf, &rcvBufLen);
	if (ret != 0)
	{
		err("executecmd_ex err = %d\n", ret);
		return -1;
	}

	if (rcvBufLen != 33 && rcvBufLen != 1)
	{
		return -2;
	}else 
	{
		*status = rcvBuf[0];
		if (rcvBufLen == 33)
		{
			memcpy(randOut, rcvBuf + 1, 32);
			*randOutLen = 32;
		}else
		{
			*randOutLen = 0;
		}
		
		return 0;
	}
}


int Tms_Cmd_PedAskServerAuth(byte *uSNIn, int uSNLen, byte *encRandIn, int encRandLen, byte*status)
{
	byte sndBuf[MAX_BUFFER_SIZE];
	int sndBufLen;
	int sndIndex = 0;
	byte rcvBuf[MAX_BUFFER_SIZE];
	int rcvBufLen;
	int ret;

	sndIndex = 0;
	memcpy(sndBuf + sndIndex, uSNIn, uSNLen);
	sndIndex+= uSNLen;
	memcpy(sndBuf + sndIndex, encRandIn, encRandLen);
	sndIndex+= encRandLen;
	sndBufLen = sndIndex;

	ret = executecmd_ex(CMDID_PED_ASK_SERVER_AUTH_ITSELF, WAIT_RESPONSE_TIMEOUT, sndBuf, sndBufLen, rcvBuf, &rcvBufLen);
	if (ret != 0)
	{
		err("executecmd_ex err = %d\n", ret);
		return -1;
	}

	if (rcvBufLen != 1)
	{
		return -2;
	}else 
	{
		*status = rcvBuf[0];
		return 0;
	}
	
}



int Tms_Cmd_Start_Download(byte fileType, byte* fileIDIn, int fileIDLen, 
	byte*status, int *fileSizeOut)
{
	byte sndBuf[MAX_BUFFER_SIZE];
	int sndBufLen;
	int sndIndex = 0;
	byte rcvBuf[MAX_BUFFER_SIZE];
	int rcvBufLen;
	int ret;

	sndIndex = 0;
	memcpy(sndBuf + sndIndex, &fileType, 1);
	sndIndex+= 1;

	memcpy(sndBuf + sndIndex, fileIDIn, fileIDLen);
	sndIndex+= fileIDLen;
	
	sndBufLen = sndIndex;

	ret = executecmd_ex(CMDID_TMS_START_DOWNLOAD, WAIT_RESPONSE_TIMEOUT, sndBuf, sndBufLen, rcvBuf, &rcvBufLen);
	if (ret != 0)
	{
		err("executecmd_ex err = %d\n", ret);
		return -1;
	}

	if (rcvBufLen != 5)
	{
		return -2;
	}else 
	{
		*status = rcvBuf[0];

		Comm_HexToInt_BigEndian(rcvBuf + 1, fileSizeOut);
		//*fileSizeOut = *((int *)(rcvBuf + 1));
		return 0;
	}
	
}


int Tms_Cmd_RcvPackage(int packageOffset, short expRcvLen, byte * status,
	byte *packageDataOut, short *packageLenOut)
{
	byte sndBuf[MAX_BUFFER_SIZE];
	int sndBufLen;
	int sndIndex = 0;
	byte rcvBuf[MAX_BUFFER_SIZE];
	int rcvBufLen;
	int ret;
	byte packageOffsetByte[4];
	byte expRcvLenByte[2];

	sndIndex = 0;
	Comm_IntToHex_BigEndian(packageOffset, packageOffsetByte);
	memcpy(sndBuf + sndIndex, packageOffsetByte, 4);
	sndIndex += 4;

	Comm_ShortToHex_BigEndian(expRcvLen, expRcvLenByte);
	memcpy(sndBuf + sndIndex, expRcvLenByte, 2);
	sndIndex+= 2;
	
	sndBufLen = sndIndex;

	ret = executecmd_ex(CMDID_TMS_RECEIVE_DOWNLOAD, WAIT_RESPONSE_TIMEOUT, sndBuf, sndBufLen, rcvBuf, &rcvBufLen);
	if (ret != 0)
	{
		err("executecmd_ex err = %d\n", ret);
		return -1;
	}

	if (rcvBufLen < 1)
	{
		return -2;
	}else 
	{
		*status = rcvBuf[0];

		memcpy(packageDataOut, rcvBuf + 1, rcvBufLen - 1);
		*packageLenOut = rcvBufLen - 1;
		return 0;
	}
	
}

int Tms_Cmd_End_Download(byte * status)
{
	byte sndBuf[MAX_BUFFER_SIZE];
	int sndBufLen = 0;
	byte rcvBuf[MAX_BUFFER_SIZE];
	int rcvBufLen;
	int ret;

	ret = executecmd_ex(CMDID_TMS_END_DOWNLOAD, WAIT_RESPONSE_TIMEOUT, sndBuf, sndBufLen, rcvBuf, &rcvBufLen);
	if (ret != 0)
	{
		err("executecmd_ex err = %d\n", ret);
		return -1;
	}

	if (rcvBufLen != 1)
	{
		return -2;
	}else 
	{
		*status = rcvBuf[0];

		return 0;
	}
}


int Tms_Cmd_Check_Update(byte *uSNIn, int uSNLen, byte *curAppVerIn, int curAppVerLen, 
	byte *status, byte *updateResult,
	byte *serverAppVerOut, int *serverAppVerLen, 
	byte *fileIDOut, int *fileIDLen)
{
	byte sndBuf[MAX_BUFFER_SIZE];
	int sndBufLen = 0;
	byte rcvBuf[MAX_BUFFER_SIZE];
	int rcvBufLen;
	int ret;
	int sndIndex;

	sndIndex = 0;
	memcpy(sndBuf + sndIndex, uSNIn, uSNLen);
	sndIndex+= uSNLen;
	memcpy(sndBuf + sndIndex, curAppVerIn, curAppVerLen);
	sndIndex+= curAppVerLen;
	sndBufLen = sndIndex;

	ret = executecmd_ex(CMDID_TMS_CHECK_UPDATE, WAIT_RESPONSE_TIMEOUT, sndBuf, sndBufLen, rcvBuf, &rcvBufLen);
	if (ret != 0)
	{
		err("executecmd_ex err = %d\n", ret);
		return -1;
	}

	if (rcvBufLen != 1 && rcvBufLen != 2 && rcvBufLen != 20)
	{
		return -2;
	}else 
	{
		*status = rcvBuf[0];
		if (rcvBufLen == 2)
		{
			*updateResult = rcvBuf[1];
		}
		if (rcvBufLen == 20)
		{
			*updateResult = rcvBuf[1];
		
			memcpy(serverAppVerOut, rcvBuf+2, 8);
			*serverAppVerLen = 8;

			memcpy(fileIDOut, rcvBuf+10, 10);
			*fileIDLen = 10;
		}

		return 0;
	}
}

int Tms_Cmd_Ask_Server_Activate(byte *uSNIn, int uSNLen, 
	byte *locationIn, int locationLen, byte *locationMac, int locationMacLen, 
    char *pszCustomId, byte tamperFlag,
	byte *refIDIn, int refIDInLen,
	byte *status, byte *actResult,
	byte *refIDOut, int *refIDOutLen)
{
	byte sndBuf[MAX_BUFFER_SIZE];
	int sndBufLen = 0;
	byte rcvBuf[MAX_BUFFER_SIZE];
	int rcvBufLen;
	int ret;
	int sndIndex;

	sndIndex = 0;
	memcpy(sndBuf + sndIndex, uSNIn, uSNLen);
	sndIndex+= uSNLen;
	
	memcpy(sndBuf + sndIndex, locationIn, locationLen);
	sndIndex+= locationLen;

	memcpy(sndBuf + sndIndex, locationMac, locationMacLen);
	sndIndex+= locationMacLen;
    
    memset(sndBuf + sndIndex, 0, 16);
    strcpy((char*)sndBuf + sndIndex, pszCustomId);
    sndIndex+=16;
    
    sndBuf[sndIndex++]=tamperFlag;
    
	if (refIDInLen > 0)
	{
		memcpy(sndBuf + sndIndex, refIDIn, refIDInLen);
		sndIndex+= refIDInLen;
	}
	sndBufLen = sndIndex;

	
	ret = executecmd_ex(CMDID_PED_ASK_SERVER_ACTIVATE_ITSELF, WAIT_RESPONSE_TIMEOUT, sndBuf, sndBufLen, rcvBuf, &rcvBufLen);
	if (ret != 0)
	{
		err("executecmd_ex err = %d\n", ret);
		return -1;
	}
	if (rcvBufLen != 1 && rcvBufLen != 2 && rcvBufLen != 22 && rcvBufLen != 22+64)
	{
		return -2;
	}else 
	{
		*status = rcvBuf[0];
        *actResult = rcvBuf[1];
		if (rcvBufLen >= 22)
		{
			memcpy(refIDOut, rcvBuf + 2, 20);
			*refIDOutLen = 20;
		}

		return 0;
	}
	
}


int Tms_Cmd_Update_Location(byte *uSNIn, int uSNLen, 
	byte *locationIn, int locationLen, byte *locationMac, int locationMacLen, 
	byte *status)
{
	byte sndBuf[MAX_BUFFER_SIZE];
	int sndBufLen = 0;
	byte rcvBuf[MAX_BUFFER_SIZE];
	int rcvBufLen;
	int ret;
	int sndIndex;

	sndIndex = 0;
	memcpy(sndBuf + sndIndex, uSNIn, uSNLen);
	sndIndex+= uSNLen;
	
	memcpy(sndBuf + sndIndex, locationIn, locationLen);
	sndIndex+= locationLen;

	memcpy(sndBuf + sndIndex, locationMac, locationMacLen);
	sndIndex+= locationMacLen;
	sndBufLen = sndIndex;

	
	ret = executecmd_ex(CMDID_PED_UPDATE_LOCATION_TO_SERVER, WAIT_RESPONSE_TIMEOUT, 
		sndBuf, sndBufLen, rcvBuf, &rcvBufLen);
	if (ret != 0)
	{
		err("executecmd_ex err = %d\n", ret);
		return -1;
	}
	if (rcvBufLen != 2 || rcvBuf[0]!=0x00)
	{
		return -2;
	}else 
	{
		*status = rcvBuf[1];
		return 0;
	}
	
}

