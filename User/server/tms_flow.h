#ifndef __TMS_FLOW_H__
#define __TMS_FLOW_H__

#define TMS_DEBUG_FIXED_KEY

#define UPDATE_FLAG_NO_UPDATE		"0"
#define UPDATE_FLAG_NEED_UPDATE		"1"
#define UPDATE_FLAG_UPDATE_ALREADY	"2"

void TMS_UnitTest(void);
int TMS_RemoteUpdate(void *p);
int TMS_Activate(void *p);
int TMS_UpdateLocation(void *p);
int TMS_OPTest(void *p);
int iWifiConfig(void *p);
int Sys_GetUsn(unsigned char *uSN, int *uSNLen);
int wifiInitAndConnAP(void);

#endif
