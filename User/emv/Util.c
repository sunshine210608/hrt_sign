#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <ctype.h>
#include "emvDebug.h"
#include "sys_littlefs.h"
#include "Define.h"
#include "Util.h"
#include "emvl2/utils.h"
#include "rtc.h"

void PrintByteArray( const byte *buf, uint32 bufsize)
{
	hexdump((byte *)buf, bufsize);		
}

void PrintItemName(const char *name)
{
	char	line[81];
	int  	name_len;
	int 	max_len = 35;

	name_len = strlen(name);
	if(name_len >= max_len)
	{
		memcpy(line, name, max_len);
	}
	else
	{
		memcpy(line, name, name_len);
		memset(line + name_len, ' ', max_len - name_len);
	}
	
	line[max_len]='\0';
	LOGD("%s:", line);
}

//YYMMDD
void GetCurrentDateAndTime(byte *pDate, byte *pTime)
{
#ifndef CY20_EMVL2_KERNEL
	time_t LTime;
	struct tm *STime;
	int year;
	int month;
	int day;

	time(&LTime);
	STime = localtime(&LTime);
	
	year = STime->tm_year+ 1900;

	assert(year >1950 && year <2050);
	if(year>=2000)
	{
		year -=2000;
	}
	else if(year >1950)
	{
		year -=1900;
	}

	month = STime->tm_mon + 1;
	day = STime->tm_mday;

	pDate[0] = (year/10) * 16 + year%10;
	pDate[1] = (month/10) * 16 + month%10;
	pDate[2] = (day/10) * 16 + day%10;
	
	pTime[0] = STime->tm_hour/10 * 16 +  STime->tm_hour%10;
	pTime[1] = STime->tm_min/10 * 16 + STime->tm_min%10;
	pTime[2] = STime->tm_sec/10 * 16 + STime->tm_min%10;
#else
	struct tm *STime;
	struct tm time;
	int year;
	int month;
	int day;

	rtc_gettime(&time);
	STime = &time;
	
	year = STime->tm_year+ 1900;

	assert(year >1950 && year <2050);
	if(year>=2000)
	{
		year -=2000;
	}
	else if(year >1950)
	{
		year -=1900;
	}

	month = STime->tm_mon + 1;
	day = STime->tm_mday;

	pDate[0] = (year/10) * 16 + year%10;
	pDate[1] = (month/10) * 16 + month%10;
	pDate[2] = (day/10) * 16 + day%10;
	
	pTime[0] = STime->tm_hour/10 * 16 +  STime->tm_hour%10;
	pTime[1] = STime->tm_min/10 * 16 + STime->tm_min%10;
	pTime[2] = STime->tm_sec/10 * 16 + STime->tm_min%10;
#endif
}

//YYMMDD
void SetCurrentDateAndTime(byte *pDate, byte *pTime)
{
	char tempChar[100] = {0};
	char sysDate[100];
	char sysTime[10];

	TranslateHexToChars(pDate, 3, tempChar);
	sprintf(sysDate, "20%s", tempChar);

	TranslateHexToChars(pTime, 3, sysTime);
		
	LOGD("sysDate = %s, sysTime = %s", sysDate, sysTime);
	SetSysDate(sysDate);
	SetSysTime(sysTime);
}

void PrintCharArray(const byte buf[], uint32 len)
{
	hexdump((byte *)buf, len);
}

void Bin2HexStr(const char src[], int len, char *dest)
{
	int i=0;
	const char	*hex ="0123456789ABCDEF";

	for(i=0; i< len; i++)
	{
		dest[2*i] =hex[src[i] >> 4];
		dest[2*i +1] =hex[src[i] &0x0F];
	}

	dest[2*i]='\0';
}

uint8 Char2Hex(unsigned char ch)
{
	if(ch >='0' && ch <='9')
	{
		return (ch -'0');
	}
	else if(ch >='A' &&ch<= 'F')
	{
		return (ch -'A' + 10);
	}
	else if(ch >= 'a' && ch <= 'f')
	{
		return (ch -'a' +10);
	}

	return 0x0F;
}

int32 HexStr2Bin(char *to, uint32 size, const unsigned char *p, uint32 len)
{
	uint32 i;
	
	assert(size *2 >= len);

	if(size*2 < len)
	{
		return STATUS_FAIL;
	}
	
	for (i=0; i<len; i += 2) 
	{
		to[i/2] = (Char2Hex(p[i])<<4) + (Char2Hex(p[i + 1]));
	}

	return STATUS_OK;
}

/*
void dbg(const char *fmt, ...)
{
	va_list vp;

	printf("*Emv2TestApp:\t");

	va_start(vp, fmt);
	vprintf(fmt,vp);
	va_end(vp);
	
	printf("\n");
}
*/
#ifndef CY20_EMVL2_KERNEL
int GetOptValue(const char *opt, char value[], int size)
{
	FILE *fp;
	char file_name[1024];

	assert(size >=1);
	
	sprintf(file_name, "%s/%s.conf", DIR_CONF, opt);
	fp = fopen(file_name, "r");

	if(fp == NULL)
	{
		return STATUS_FAIL;
	}
	
	fgets(value, size, fp);
	fclose(fp);

	return STATUS_OK;
}
#else
int GetOptValue(const char *opt, char value[], int size)
{
	lfs_file_t file;
	int ret;
	char file_name[1024];

	assert(size >=1);
	
	sprintf(file_name, "%s/%s.conf", DIR_CONF, opt);
	ret = sys_lfs_file_open(&file, file_name, LFS_O_RDONLY);

	if (ret != 0)
	{
		return STATUS_FAIL;
	}
	
	sys_lfs_file_fgets(value, size, &file);
	sys_lfs_file_close(&file);

	return STATUS_OK;
}

#endif
int SaveOptValue(const char *opt, const char *value)
{
#ifndef CY20_EMVL2_KERNEL
	FILE *fp;
	char file_name[1024];
	
	sprintf(file_name, "%s/%s.conf", DIR_CONF, opt);
	fp = fopen(file_name, "w");

	if(fp == NULL)
	{
		return STATUS_FAIL;
	}

	fprintf(fp, "%s", value);
	fclose(fp);

	return STATUS_OK;
#else
	lfs_file_t fp;
	char file_name[1024];
	char file_buffer[1024*5];
	int ret;
	
	sprintf(file_name, "%s/%s.conf", DIR_CONF, opt);
	//fp = fopen(file_name, "w");
	ret = sys_lfs_file_open(&fp, file_name, LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
	if(ret != 0)
	{
		return STATUS_FAIL;
	}

	//fprintf(fp, "%s", value);
	//fclose(fp);
	sprintf(file_buffer, "%s", value);
	sys_lfs_file_write(&fp, file_buffer, strlen(file_buffer));
	sys_lfs_file_close(&fp);

	return STATUS_OK;
#endif
}

char *GetConfFilePath(const char *filename)
{
	static char buf[512];
	
	sprintf(buf, "%s/%s", DIR_CONF, filename);

	return buf;
}

int SetSystemDate(int year, int month, int day)
{
#ifndef CY20_EMVL2_KERNEL
	struct tm *ptm_old;
	struct tm tm_new;
	time_t  time_old;
	time_t  time_new;

	time(&time_old);
	ptm_old = localtime(&time_old);

	memcpy(&tm_new, ptm_old, sizeof(struct tm));

	tm_new.tm_year = year - 1900;
	tm_new.tm_mon = month - 1;
	tm_new.tm_mday = day;

	time_new = mktime(&tm_new);

	//stime(&time_new);
	
	system("hwclock -w");

	return STATUS_OK;
#else
	struct tm tm_new;

	rtc_gettime(&tm_new);
	
	tm_new.tm_year = year - 1900;
	tm_new.tm_mon = month - 1;
	tm_new.tm_mday = day;

	rtc_settime(&tm_new);
	
	return STATUS_OK;
#endif
}

void GetSystemDate(int *year, int *month, int *day)
{
#ifndef CY20_EMVL2_KERNEL
	struct tm *tm;
	time_t  time_old;

	if(year==NULL || month==NULL || day==NULL)
	{
		return;
	}
	
	time(&time_old);
	tm = localtime(&time_old);

	*year = tm->tm_year + 1900;
	*month = tm->tm_mon + 1;
	*day = tm->tm_mday;
#else
	struct tm tm_new;

	rtc_gettime(&tm_new);
	*year = tm_new.tm_year + 1900;
	*month = tm_new.tm_mon + 1;
	*day = tm_new.tm_mday;
#endif
}

int SetSystemTime(int hour, int min, int second)
{
#ifndef CY20_EMVL2_KERNEL

	struct tm *ptm_old;
	struct tm tm_new;
	time_t  time_old;
	time_t  time_new;

	time(&time_old);
	ptm_old = localtime(&time_old);

	memcpy(&tm_new, ptm_old, sizeof(struct tm));

	tm_new.tm_hour = hour;
	tm_new.tm_min = min;
	tm_new.tm_sec = second;

	time_new = mktime(&tm_new);

	//stime(&time_new);
	
	system("hwclock -w");
	
#else
	struct tm tm_new;

	rtc_gettime(&tm_new);
	
	tm_new.tm_hour = hour;
	tm_new.tm_min = min;
	tm_new.tm_sec = second;

	rtc_settime(&tm_new);
	
	return STATUS_OK;
#endif
}

void GetSystemTime(int *hour, int *min, int *second)
{
#ifndef CY20_EMVL2_KERNEL
	struct tm *tm;
	time_t  time_old;

	if(hour==NULL || min==NULL || second==NULL)
	{
		return;
	}
	
	time(&time_old);
	tm = localtime(&time_old);

	*hour = tm->tm_hour;
	*min = tm->tm_min;
	*second = tm->tm_sec;
#else
	struct tm tm_new;

	rtc_gettime(&tm_new);

	*hour = tm_new.tm_hour;
	*min = tm_new.tm_min;
	*second = tm_new.tm_sec;
	
#endif
}


//20080101
int SetSysDate(const char *date)
{
	int i;
	int tmpdate[8];
	int year;
	int month;
	int day;
	
	if(strlen(date) != 8)
	{
		return STATUS_FAIL;
	}

	for(i=0; i<8; i++)
	{
		tmpdate[i] = date[i] -'0';
	}

	year = tmpdate[0]*1000 + tmpdate[1]*100 + tmpdate[2]*10 + tmpdate[3];
	month = tmpdate[4]*10 + tmpdate[5];
	day = tmpdate[6]*10 + tmpdate[7];

	LOGD("year:%d month:%d day:%d", year, month, day);
	if(SetSystemDate(year, month, day) != 0)
	{
		LOGE("SetSystemDate ERR");
		return STATUS_FAIL;
	}

	return STATUS_OK;
}

char *GetSysDate()
{
	int year;
	int mon;
	int day;
	static char date[8+1];

	GetSystemDate(&year, &mon, &day);

	sprintf(date, "%d%02d%02d", year, mon, day);
	
	return date;
}

int SetSysTime(const char *time)
{
	int i;
	int tmptime[6];
	int hour;
	int min;
	int second;
	
	if(strlen(time) != 6)
	{
		return STATUS_FAIL;
	}

	for(i=0; i<6; i++)
	{
		tmptime[i] = time[i] -'0';
	}

	hour = tmptime[0]*10 + tmptime[1];
	min = tmptime[2]*10 + tmptime[3];
	second = tmptime[4]*10 + tmptime[5];

	if(SetSystemTime(hour, min, second) != STATUS_OK)
	{
		LOGE("SetSystemTime ERR");
		return STATUS_FAIL;
	}

	return STATUS_OK;

}

char *GetSysTime()
{
	int hour;
	int min;
	int second;
	static char time[6+1];

	GetSystemTime(&hour, &min, &second);

	sprintf(time, "%02d%02d%02d", hour, min, second);

	return time;
}

#ifndef CY20_EMVL2_KERNEL
/*
void hexdump(byte *buf,int len)
{
    int i;
    char data[4096],*p;
    if(len<0 || !buf)
        return;

    if(len>1024)
    {
        LOGW("HEXDUMP reduce len to 1024");
        len = 1024;
    }
    LOGD("HexDump [%d]",len);
    memset(data,0, sizeof(data));
    for(i=0,p=&data[0];i<len;i++){
        //p+=((i%16)*3);
        sprintf(p+(i*3),"%02X ",buf[i]);
    }
    //if(len%16)
    LOGD("%s =", data);
}

void hexdumpEx(char *prompt, byte *buf, int len)
{
	LOGD("%s", prompt);
	hexdump(buf, len);
}
*/
#else

void hexdumpraw(byte *buf,int len)
{
   int i;
	//dbg("HexDump %d Bytes:\n\t",len);
	if(len>5120) {
		dbg("Data too large %d\n", len);
		return;
	}
	dbg("\t");
	for(i=0;i<len;i++){
		dbg("%02X",buf[i]);
		/*
		if((i+1)%16==0){
			if((i+1)==len)
				dbg("\n");
			else
				dbg("\n\t");
		}
		*/
	}
	//if(len%16) dbg("\n");
	dbg("\n");
}


void hexdumpEx(char *prompt, byte *buf, int len)
{
#if 1
	LOGD("%s", prompt);
	hexdumpraw(buf, len);
	//hexdump(buf, len);	
	LOGD("\n");
#endif
}


#endif


#define LOCAL_PC

#ifdef LOCAL_PC
char gszServerIP[20] = "192.168.2.133";
char gszServerPort[10] = "6908";
#else
char gszServerIP[20] = "100.100.100.222";
char gszServerPort[10] = "6908";
#endif

int SetServerIPAndPort(char *szServerIP, char *szServerPort)
{
	strcpy(gszServerIP, szServerIP);
	strcpy(gszServerPort, szServerPort);

    return  0;
}

int GetServerIPAndPort(char *szServerIP, char *szServerPort)
{
	strcpy(szServerIP, gszServerIP);
	strcpy(szServerPort, gszServerPort);

	return 0;
}

#define KERNEL_TYPE_EMV 0
#define KERNEL_TYPE_PBOC 1


//int gKernelType = KERNEL_TYPE_EMV;
int gKernelType = KERNEL_TYPE_PBOC;

void SetKernelType(int KernelType)
{
	gKernelType = KernelType;
	LOGD("gKernelType = %d", gKernelType);
}

int GetKernelType()
{
	return gKernelType;
}
#ifdef VPOS_APP
tti_bool gForceOnline = TRUE;
#else
tti_bool gForceOnline = FALSE;
#endif
void SetForceOnline(tti_bool bForceOnline)
{
	gForceOnline = bForceOnline;
	LOGD("gForceOnline = %d", gForceOnline);
}

tti_bool IsForceOnline()
{
	return gForceOnline;
}

tti_int16 gTransCurrencyCode = 0x0156;


void SetTransCurrencyCode(tti_int16 iTransCurrency)
{
	gTransCurrencyCode = iTransCurrency;
	LOGD("iTransCurrency = %02x\n", iTransCurrency);
}

tti_int16 GetTransCurrencyCode(tti_byte *byTransCurrencyCode)
{
	if (byTransCurrencyCode != NULL)
	{
		if (gTransCurrencyCode == 0x0840)
		{
			byTransCurrencyCode[0] = 0x08;
			byTransCurrencyCode[1] = 0x40;
		}else
		{
			byTransCurrencyCode[0] = 0x01;
			byTransCurrencyCode[1] = 0x56;
		}
	}

	return gTransCurrencyCode;
}

tti_bool gbAutoTrans = FALSE;

void SetAutoTrans(tti_bool bAutoTrans)
{
	gbAutoTrans = bAutoTrans;
}

tti_bool IsAutoTrans(void)
{
	return gbAutoTrans;
}

void TranslateTransTypeToString(char *transType, char *transString)
{
	if (strcmp(transType, "00") == 0)
	{
		strcpy(transString, "商品或服务");
	}else if (strcmp(transType, "01") == 0)
	{
		strcpy(transString, "现金");
	}else if (strcmp(transType, "21") == 0)
	{
		strcpy(transString, "存款");
	}else if (strcmp(transType, "31") == 0)
	{
		strcpy(transString, "查询");
	}else if (strcmp(transType, "40") == 0)
	{
		strcpy(transString, "转账");
	}else if (strcmp(transType, "50") == 0)
	{
		strcpy(transString, "支付");
	}else if (strcmp(transType, "63") == 0)
	{
		strcpy(transString, "管理");
	}else
	{
		strcpy(transString, "未知");
	}
}

int gMalloc_Free_Counter = 0;

void malloc_Counter()
{
	gMalloc_Free_Counter++;
	//dbg("Malloc: gMalloc_Free_Counter = %d\n", gMalloc_Free_Counter);
}

void free_Counter()
{
	gMalloc_Free_Counter--;
	//dbg("Free: gMalloc_Free_Counter = %d\n", gMalloc_Free_Counter);
}

