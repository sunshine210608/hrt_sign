#ifndef __OSTYPES_H__
#define __OSTYPES_H__



#ifdef __cplusplus

    extern "C" {

#endif

typedef unsigned char tti_bool; /* NOTE: modified for the embedded systems only */



typedef unsigned char   tti_byte;

typedef unsigned char   tti_uint8;

typedef char            tti_int8;



typedef unsigned short  tti_uint16;

typedef short           tti_int16;



typedef unsigned long   tti_uint32;

typedef   signed long   tti_int32;



typedef unsigned int  tti_uint; /* use this when the size is not important. */



typedef tti_uint16 tti_unichar; 



typedef struct

{

	tti_uint8 second;	/* 00..59 */

	tti_uint8 minute;	/* 00..59 */

	tti_uint8 hour;	/* 00..23 */

		/* Driver level:

		    b6=0, 0x00..0x23 

		    b6=1, b5=1->PM, b5=0->AM, 0x01..0x12 , e.g. 0x72 stands for 12PM */

} tti_time ;



typedef struct

{

	tti_uint8 dayinweek;		/* 01..07 */

	tti_uint8 dateinmonth;	/* 01..31 */

	tti_uint8 month;	/* 01..12 */

	tti_uint8 year;	/* 00..99 */

} tti_date;



#ifdef TTI_UNICODE

	typedef tti_unichar tti_tchar; /* tti_tchar is transparent char,            */

#else                                /* so that the port between ASCII and Unicode */

    typedef char tti_tchar;         /* can be done in steps.                      */

#endif



#if !defined (NULL)

    #define NULL (0)

#endif


#ifndef bool
typedef unsigned char bool;
#endif

#if !defined(TRUE) && !defined(FALSE)

    #define TRUE  (1)

    #define FALSE (0)

#endif 

    

/* Use this to avoid compiler warnings when a parameter is unused */

#define TTI_UNUSED(A) ((void)(A))



/* Use this to get the length of an array */

#define TTI_ALEN(A) (sizeof(A)/sizeof*(A))



#define TTI_MIN(a,b) ((a)<(b) ? (a) : (b))

#define TTI_MAX(a,b) ((a)>(b) ? (a) : (b))



#define TTI_SWAP16(A) ((((A)>>8)&0x00FF)|(((A)&0x00FF)<<8))

#define TTI_SWAP32(A) ((((A)>>24)&0x000000FF)|(((A)>>8)&0x0000FF00)|\
                       (((A)&0x0000FF00)<<8)|(((A)&0x000000FF)<<24))



#define NUL         0x00

#define SOH         0x01

#define STX         0x02

#define ETX         0x03

#define EOT         0x04

#define ENQ         0x05

#define ACK         0x06

#define BEL         0x07

#define BS          0x08

#define HT          0x09

#define CLF         0x0a

#define VT          0x0b

#define AUF         0x0c

#define CCR         0x0d

#define SO          0x0e

#define SI          0x0f

#define DLE         0x10

#define DC1         0x11

#define DC2         0x12

#define DC3         0x13

#define DC4         0x14

#define NAK         0x15

#define SYN         0x16

#define ETB         0x17

#define CAN         0x18

#define EM          0x19

#define CSUB        0x1a

#define ESC         0x1b

#define FS          0x1c

#define GS          0x1d

#define CRS         0x1e

#define US          0x1f

#define CSP         0x20

#define DEL         0x7f





/*#define DEVELOPMENT_BOARD*/

#ifdef __cplusplus

    }

#endif



#endif  

