#ifndef PARAM_DOWN_H
#define PARAM_DOWN_H

#include "Define.h"
#include "emvl2/tag.h"


void DownloadParam(void);
int LoadTerminalParam(void);


void DownloadTerminalPara(void);

void DownloadAIDPara(void);

void DownloadCA(void);

int AutoTransaction(char *autoAmount, char *autoAmountOther, unsigned char *transactionType);

void GetAutoTestData(char *autoAmount, char *autoAmountOther, unsigned char *autoTransType);

void GetAutoTransData(int *autoAmount, int *autoAmountOther, unsigned char *autoTransType);

int LoadTerminalParaBctc(byte *terminalCountryCodeBin);

int LoadAidParaByAid(tti_tchar * aidStr, TagList *aidParaTagList, Tag3ByteList *aidPara3TagList);






#endif

