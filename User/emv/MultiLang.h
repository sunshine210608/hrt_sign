#ifndef MULTI_LANG_H
#define MULTI_LANG_H

enum {
	LANG_ENGLISH,
	LANG_FRENCH
};

enum{
	MLSID_APPROVED=0,
	MLSID_DECLINED,
	MLSID_APPROVED_1,
	MLSID_DECLINED_2,
};

extern int g_PrefferLang;

void MultiLangDisp(int id, int line);
void MultiLangDispCenter(int id, int line);
void MultiLangDispRight(int id, int line);
#endif

