#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common.h"
#include "Define.h"
#include "Util.h"
#include "ReqDataItem.h"
#include "emvDebug.h"


ReqDataItem_t g_ReqDataList[REQ_ITEM_COUNT];

DataItemAttr_t *GetRequestItemAttr(uint16 index)
{
	static DataItemAttr_t AttrTable[] ={
	//command
	{DTYPE_ASCII,	ETYPE_MUST,	FIX_LEN, 3, 0, "cmd load config"},
	{DTYPE_ASCII,	ETYPE_MUST,	FIX_LEN, 3, 0, "cmd get aid list"},
	{DTYPE_ASCII,	ETYPE_MUST,	FIX_LEN, 3, 0, "cmd init transaction"},
	{DTYPE_ASCII, 	ETYPE_MUST,	FIX_LEN, 3, 0, "cmd emv transaction"},
	{DTYPE_ASCII, 	ETYPE_MUST,	FIX_LEN, 3, 0, "cmd transaction complete"},	
	{DTYPE_ASCII, 	ETYPE_MUST,	FIX_LEN, 3, 0, "cmd end transaction"},
	{DTYPE_ASCII,   ETYPE_MUST, FIX_LEN, 2, 0, "MessageType"},
	
	//load config
	{DTYPE_ASCII,	ETYPE_MUST,	FIX_LEN, 8, 0, 	"IFD Serial Number"}, //9F1E
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 3, 0, 	"Terminal Country Code"},//9F1A
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 2, 0, 	"Terminal Type" },//9F35
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 6, 0, 	"Terminal Capabilities "},//9F33
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 2, 0, 	"Card Data Input Capability"},
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 2, 0, 	"CVM Capability" },	
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 2, 0, 	"Security Capability" },	
	
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN,10, 0, 	"Add Terminal Capabilities"},//9F40
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 4, 0, 	"Add Trans Type Capability"},	
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 2, 0,	"Add Data Input Capability"},	
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 4, 0, 	"Add Data Output Capability"},	
	{DTYPE_NUMERIC, ETYPE_MUST, FIX_LEN, 1, 0,  "Is Support PSE"},
	
	{DTYPE_ASCII,	ETYPE_OPT,	VAR_LEN, 0, 0, 	"candidate aid list"},

	//Get AID List
	{DTYPE_NUMERIC,	ETYPE_MUST, FIX_LEN, 3, 0, 	"timeout"},

	//init transaction
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN,12, 0, 	"Amount Author"},//9F02
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN,12, 0, 	"Amount Other"},//9F03
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 6, 0, 	"Trans Date"},//9A
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 6, 0, 	"Trans Time"},//9F21
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 2, 0, 	"Trans Type"},//9C	
	{DTYPE_NUMERIC, ETYPE_MUST,	FIX_LEN, 2, 0, 	"POS Entry Mode Code" },//9F39
	//Jason modified on 2012.04.23 start
	//{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 5, 0, 	"Trans SEQ Number"},//9F41
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 8, 0, 	"Trans SEQ Number"},//9F41
	//end
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 2, 0, 	"Account Type"},//5F57
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN,12, 0, 	"Floor Limit"},//9F1B
	{DTYPE_NUMERIC, ETYPE_MUST,	FIX_LEN, 2, 0, 	"Target Percentage"},//none
	{DTYPE_NUMERIC, ETYPE_MUST,	FIX_LEN, 2, 0, 	"Max Target Percentage"},//none
	{DTYPE_NUMERIC, ETYPE_MUST,	FIX_LEN,12, 0, 	"Threshold Amount"},//none
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 4, 0, 	"Merchant Category Code"},//9F15
	{DTYPE_ASCII,	ETYPE_MUST,	FIX_LEN, 15,0, 	"Merchant ID"},//9F16
	{DTYPE_ASCII,	ETYPE_MUST,	FIX_LEN, 8, 0, 	"Terminal ID"},//9F1C
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 3, 0, 	"Trans Currency Code"}, //5F2A
	{DTYPE_NUMERIC,	ETYPE_MUST, FIX_LEN, 1, 0, 	"Trans Currency Exp"},//5F36
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 3, 0, 	"Trans Reference Currency Code"},//9F3C
	{DTYPE_NUMERIC,	ETYPE_MUST, FIX_LEN, 1, 0, 	"Trans Reference Currency Exp"},//9F3D
	{DTYPE_NUMERIC, ETYPE_MUST, FIX_LEN, 1, 0,  "Is Support VLP"},	
	{DTYPE_NUMERIC, ETYPE_MUST, FIX_LEN, 1, 0,  "Bypass Mode"},
	{DTYPE_BINARY,	ETYPE_OPT,	VAR_LEN, 0, 0, 	"Term Risk Manage Data"},//9F1D
	{DTYPE_ASCII,	ETYPE_MUST,	VAR_LEN, 0, 0, 	"Merchant Name Location"},//9F4E
	{DTYPE_BINARY,	ETYPE_MUST,	VAR_LEN, 0, 0, 	"Selected AID"},
	{DTYPE_NUMERIC,	ETYPE_MUST, VAR_LEN, 0, 0, 	"Acquirer ID"},//9F01
	{DTYPE_BINARY, 	ETYPE_OPT,	FIX_LEN,10, 0, 	"Default TAC"},//none
	{DTYPE_BINARY, 	ETYPE_OPT,	FIX_LEN,10, 0, 	"Denial TAC"},//none
	{DTYPE_BINARY, 	ETYPE_OPT,	FIX_LEN,10, 0, 	"Online TAC"},//none
	{DTYPE_BINARY, 	ETYPE_OPT,	VAR_LEN, 0,128,	"Default TDOL"},//none
	{DTYPE_BINARY, 	ETYPE_OPT,	VAR_LEN, 0,128,	"Default DDOL"},//none

	//Emv Transaction		
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 1, 0, 	"Card Hot"},
	{DTYPE_NUMERIC,	ETYPE_MUST,	FIX_LEN, 1, 0,	"Force Online"},	
	{DTYPE_NUMERIC, ETYPE_MUST,	FIX_LEN,12, 0, 	"Trans Log Amount"},//none
	{DTYPE_BINARY,	ETYPE_OPT,  VAR_LEN, 0, 0, 	"Pinpad  Secret Key"},
	{DTYPE_ASCII, 	ETYPE_MUST, VAR_LEN, 0, 0, 	"Offline Plain PIN Prompt"},
	{DTYPE_ASCII, 	ETYPE_MUST, VAR_LEN, 0, 0, 	"Offline Encrypt PIN Prompt"},
	{DTYPE_ASCII, 	ETYPE_MUST, VAR_LEN, 0, 0, 	"Online Encrypt PIN Prompt"},
	{DTYPE_ASCII,   ETYPE_OPT, VAR_LEN, 0, 0,  "Last Pin Try"},
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 2, 0, 	"CA Hash Algorithm"},
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 2, 0, 	"CA Public Key Algorithm"},
	{DTYPE_BINARY, 	ETYPE_MUST,	FIX_LEN, 1, 0, 	"CA Exponent"},
	{DTYPE_BINARY, 	ETYPE_MUST,	VAR_LEN, 2,496,	"CA Key Modulus"},
	{DTYPE_BINARY, 	ETYPE_OPT,	FIX_LEN,40, 0, 	"CA Checksum"},
	{DTYPE_NUMERIC, ETYPE_OPT,	FIX_LEN, 8, 0, 	"CA Key Activation Date"},
	{DTYPE_NUMERIC, ETYPE_OPT,	FIX_LEN, 8, 0, 	"CA Key Expiration Date"},
	
	//emv completion	
	{DTYPE_ASCII, 	ETYPE_OPT,	VAR_LEN, 0, 16,	"Issuer Host IP"},
	{DTYPE_ASCII, 	ETYPE_OPT,  VAR_LEN, 0, 0, 	"Issuer Host Port"},
	{DTYPE_NUMERIC, ETYPE_MUST,	FIX_LEN, 1, 0, 	"Response Mode"},
	{DTYPE_ASCII,	ETYPE_MUST,	FIX_LEN, 2, 0, 	"Trans Response Code"},
	{DTYPE_BINARY, 	ETYPE_OPT,  VAR_LEN, 0,128,	"Issuer Auth Data"},
	{DTYPE_BINARY, 	ETYPE_OPT,  VAR_LEN, 0, 0, 	"Issuer Script"}
	};

#ifndef CY20_EMVL2_KERNEL
	assert(index < REQ_ITEM_COUNT);
#endif
	assert(sizeof(AttrTable)/sizeof(AttrTable[0]) == REQ_ITEM_COUNT);
	return &(AttrTable[index]);
}

//When App startup, call this function only one time.
int gfirstTimeInitRequstFlag = 1;
int32 FirstTimeInitRequestItemList()
{
	int i;

	if (gfirstTimeInitRequstFlag != 1)
	{
		return STATUS_OK;
	}
	
	for(i=0; i<REQ_ITEM_COUNT; i++)
	{
		g_ReqDataList[i].index = i;		
		g_ReqDataList[i].data.ptr = NULL;
		g_ReqDataList[i].data.len = 0;
	}

	gfirstTimeInitRequstFlag = 0;
	return STATUS_OK;
}

int32 InitRequestItemList(void)
{
	int i;

	//Jason added on 2020/05/07 to fixed the issue that just malloc but no free
	FirstTimeInitRequestItemList();
	
	for(i=0; i<REQ_ITEM_COUNT; i++)
	{
		if(IsRequestItemExisted(i))
		{
			ResetRequestItem(i);
		}
	}
	//End
	
	for(i=0; i<REQ_ITEM_COUNT; i++)
	{
		g_ReqDataList[i].index = i;		
		g_ReqDataList[i].data.ptr = NULL;
		g_ReqDataList[i].data.len = 0;
	}

	SetRequestItemStr(REQ_CMD_LOAD_CONFIG, 	CMD_LOAD_CONFIG);
	SetRequestItemStr(REQ_CMD_GET_AID_LIST, CMD_GET_AID_LIST);
	SetRequestItemStr(REQ_CMD_INIT_TRANS, 	CMD_INIT_TRANS);
	SetRequestItemStr(REQ_CMD_EMV_TRANS, 	CMD_EMV_TRANS);
	SetRequestItemStr(REQ_CMD_TRANS_COM, 	CMD_EMV_COMPLETE);
	SetRequestItemStr(REQ_CMD_END_TRANS, 	CMD_EMV_END_TRANS);
	return STATUS_OK;
}

//Jason added on 2021/05/10, free the memory when IC card transaction is done to void QR malloc error.
int32 UninitRequestItemList(void)
{
    int i;

    if (gfirstTimeInitRequstFlag == 1)
    {
    //RequestItemList didn't init yet, do nothig
        return STATUS_OK;
    }

    //RequestItemList already init , free them.
    for(i=0; i<REQ_ITEM_COUNT; i++)
    {
        if(IsRequestItemExisted(i))
        {
            ResetRequestItem(i);
        }
    }

    return STATUS_OK;
}

int32 SetRequestItem(uint16 index, const byte buf[], uint16 len)
{
	const DataItemAttr_t *pAttr;
	vec_t	*pData = &(g_ReqDataList[index].data);

#ifndef CY20_EMVL2_KERNEL
	assert(index < REQ_ITEM_COUNT);
#endif

	pAttr = GetRequestItemAttr(index);

	if((pAttr->len_type==FIX_LEN) && (len != pAttr->min_len))
	{
		//LOGD("Set ReqData Fail:%s--Length is NOT match.\n", pAttr->name);
		
		//hexdumpEx("value: ", (byte *)buf, len);

		return STATUS_FAIL;
	}

	if(IsRequestItemExisted(index))
	{
		ResetRequestItem(index);
	}
	
	pData->ptr = (byte *)malloc(len + 1);
	if(pData->ptr == NULL)
	{
		//dbg("malloc error\n");
		return STATUS_FAIL;
	}
	//Line;
	//malloc_Counter();
	
	memcpy(pData->ptr, buf, len);
	pData->ptr[len] = '\0';
	
	pData->len = len;
	
	return STATUS_OK;
}

int32 SetRequestItemStr(uint16 index, const char *str)
{
	return SetRequestItem(index, (byte *)str, strlen(str));
}

int32 ResetRequestItem(uint16 index)
{
#ifndef CY20_EMVL2_KERNEL
	assert(index < REQ_ITEM_COUNT);
#else
	ASSERT_FAILED(index < REQ_ITEM_COUNT);
#endif

	free(g_ReqDataList[index].data.ptr);
	//free_Counter();
	g_ReqDataList[index].data.ptr = NULL;
	g_ReqDataList[index].data.len = 0;

	return STATUS_OK;
}

void ResetRequestItemList()
{
	uint16 i;
	
	for(i=0; i<REQ_ITEM_COUNT; i++)
	{
		ResetRequestItem(i);
	}
}

vec_t *GetRequestItemValue(uint16 index)
{	
	assert(index < REQ_ITEM_COUNT);

	return &(g_ReqDataList[index].data);
}

int32 IsRequestItemExisted(uint16 index)
{		
	 if(g_ReqDataList[index].data.len == 0)
	 {
	 	return FALSE;
	 }
	
	 return TRUE;
}

int32 SetRequestItemInt(uint16 index, uint32 value)
{
	//byte buf[20];
	char buf[20];
	const DataItemAttr_t *pAttr;
	
	pAttr = GetRequestItemAttr(index);

	assert(pAttr->len_type == FIX_LEN);

	if(pAttr->data_type == DTYPE_NUMERIC)
	{		
		sprintf(buf,"%0*d",pAttr->min_len, value);
	}
	else if(pAttr->data_type == DTYPE_BINARY)
	{
		sprintf(buf,"%0*X",pAttr->min_len, value);
	}
	else
	{
		return STATUS_FAIL;
	}

	return SetRequestItem(index, (byte *)buf, pAttr->min_len);
}

void PrintRequestItemList()
{
	uint16 i;
	
	const DataItemAttr_t *pAttr;

	for(i=0; i<REQ_ITEM_COUNT; i++)
	{
		if(IsRequestItemExisted(i))
		{
			pAttr = GetRequestItemAttr(i);
			PrintItemName((char *)(pAttr->name));
			PrintCharArray(g_ReqDataList[i].data.ptr, g_ReqDataList[i].data.len);
		}
	}
}

int IsInquiryTransaction()
{
	assert(IsRequestItemExisted(REQ_TRANS_TYPE));
	
	if(strcmp((char *)(GetRequestItemValue(REQ_TRANS_TYPE)->ptr), TRANS_T_AVAIL_FUND_ENQ) == 0)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

int IsReadLogTransaction()
{
	assert(IsRequestItemExisted(REQ_TRANS_TYPE));
	
	if(strcmp((char *)(GetRequestItemValue(REQ_TRANS_TYPE)->ptr), TRANS_T_READ_LOG) == 0)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

