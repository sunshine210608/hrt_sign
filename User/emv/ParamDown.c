#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "systick.h"
//#include "sha1.h"
#include "EmvAppUi.h"
#include "Util.h"
#include "ReqDataItem.h"
#include "Setup.h"
#include "UtilPrint.h"
#include "DataTrans.h"
#include "emvDebug.h"
#include "emvl2/defines.h"
#include "emvl2/tag.h"
#include "Online.h"
#include "sys_littlefs.h"
#include "sha1.h"
#include "paramDown.h"
#include "misce.h"
#include "receipt.h"
#include "utils.h"
#include "user_projectconfig.h"

#ifdef VPOS_APP
#include "VposFace.h"
#if 0//def APP_LKL
#include "MemMana_lkl.h"
#endif
#if 0//def APP_SDJ
#include "MemMana_sdj.h"
#endif
#ifdef APP_CJT
#include "MemMana_cjt.h"
#endif

#endif

int GetDataFromServer(char *ip, char *port,int timeoutsec, char buf[], int *len)
{
#ifndef CY20_EMVL2_KERNEL
	int i;
	int ret;
	int sock;	
	long val;
	struct sockaddr_in  server;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock < 0)
	{
		LOGE("Error at socket(): %s", strerror(errno));
		return STATUS_FAIL;
	}

	memset(&server, 0x00, sizeof(server));
	server.sin_family =AF_INET;
	server.sin_addr.s_addr =inet_addr(ip);
	server.sin_port =htons((uint16)atoi(port));

	val =fcntl(sock, F_GETFL, 0);
	fcntl(sock, F_SETFL, val|O_NONBLOCK);

	for(i=0; i<timeoutsec; i++)
	{
		ret = connect(sock, (struct sockaddr *) &server, sizeof(server));
		if(ret == 0)
		{
			break;
		} else
		{
            LOGD("connect now :%s", strerror(errno));
            sleep(1);
        }


		//event = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_CANCEL, 1);
		//if(event == KEYBORADEVM_MASK_KEY_CANCEL)
		//{
		//	return STATUS_FAIL;
		//}
	}

	if(ret != 0)
	{
		close(sock);
        LOGE("CONNECT TIMEOUT");
		return STATUS_SERV_CONN_TIMEOUT;
	}

    sleep(1);
    for(i=0; i<10; i++) 
	{
		 sleep(1);
		 
        *len = recv(sock, buf, *len, 0);
        
       
        if (*len > 0) 
		{
            LOGD("Recive Data: len=%d\n", *len);
			hexdump(buf, *len);
            break;
//		PrintCharArray(buf,*len);
        } else 
        {
            LOGE("Recive Data Fail");
            if (i < 10) 
			{
                continue;
            }
            else
			{
				close(sock);
                return STATUS_FAIL;
            }

            //return STATUS_FAIL;
        }
    }
	
	close(sock);
	return STATUS_OK;
		
#else
	return STATUS_OK;
#endif		
}

int32 GetDataLen(const char buf[], int size)
{
	int i;
	int len = 0;

	for(i=0; i<size; i++)
	{
		if(buf[i] == '=')
		{
			break;
		}

		len++;
	}

	return len;
}

typedef struct 
{
	int index;
	const char *header;
}TTermParam;


TTermParam g_TermParamTable[]=
		{
			{REQ_IFD_SERIAL_NUMBER,	"IFD-SN"},
			{REQ_TERM_CTRY_CODE,	"TermCtryCode"},
			{REQ_TERM_CAP_INPUT,	"CapInput"},
			{REQ_TERM_CAP_CVM,		"CapCVM"},
			{REQ_TERM_CAP_SECURITY,	"CapSec"},
			{REQ_TERM_ADD_TRANS_TYPE,"AddCapTransType"},
			{REQ_TERM_ADD_INPUT,	"AddCapInput"},
			{REQ_TERM_ADD_OUTPUT,	"AddCapOutput"},
			{REQ_DENIAL_TAC,		"DenialTAC"},
			{REQ_ONLINE_TAC,		"OnlineTAC"},
			{REQ_DEFAULT_TAC,		"DefaultTAC"},
			{REQ_DDOL,				"DefaultDDOL"},
			{REQ_TDOL,				"DefaultTDOL"},

			{REQ_HOST_IP,			"IssuerHostIP"},
			{REQ_HOST_PORT,			"IssuerHostPort"},


			{REQ_ACQUIRER_ID,		"AcquirerID"},
			{REQ_MERCHANT_ID,		"MerchantID"},
			{REQ_TERM_ID,			"TerminalID"},
			{REQ_MERCHANT_CAT_CODE,	"MerCatCode"},
			{REQ_MERCHANT_NAME_LOCATION,	"MerNameLoc"},
			{REQ_TRANS_CUR_CODE,	"TransCurCode"},
			{REQ_TRANS_CUR_EXP,		"TransCurExp"},
			{REQ_TRANS_REFERENCE_CUR_CODE,	"TransRefCurCode"},
			{REQ_TRANS_REFERENCE_CUR_EXP,	"TransRefCurExp"},
			{REQ_TERM_RISK_MANAGE_DATA,"TransRiskManData"},
			{REQ_FLOOR_LIMIT,	"FloorLimit"},
			{REQ_TAR_PERCENT,	"TarPercent"},
			{REQ_MAX_TAR_PERCENT,	"MaxTarPerc"},
			{REQ_THRESHOLD_AMOUNT,	"ThreAmount"},
			{REQ_TIMEOUT,	"Timeout"},
			{REQ_IS_SUPPORT_VLP, "IsSupportVLP"},
			{REQ_IS_SUPPORT_PSE, "IsSupportPSE"},
			{REQ_BYPASS_MODE, "Bypass Mode"},
			{REQ_TERM_TYPE,	"TermType"}
		};


int CheckOptData(char buf[], int len)
{
	int i;
	char *CurPos;
	char *EndPos;
	int DataLen;

	CurPos = buf;
	EndPos = buf + len;

	for(i=0; i<sizeof(g_TermParamTable)/sizeof(g_TermParamTable[0]); i++)
	{
		DataLen = GetDataLen(CurPos,(int)(EndPos-CurPos));
		CurPos += DataLen + 1;

		if(CurPos > EndPos)
		{
			return STATUS_FAIL;
		}
	}

	return STATUS_OK;
}

#ifndef CY20_EMVL2_KERNEL
int CheckCA()
{
	char RID[1024];
	char Index[1024];
	char Exp[1024];
	char checksum[1024];
	char PK[1024*5];
	char line[1024*10];
	FILE *fp;
	char calchecksum[20];
	char tmp[1024*10];
	int len;
	int IsSuccess =0;

	printf("CheckCA\n");

	fp =fopen(GetConfFilePath(FILE_NAME_CA_LIST), "r");
	if(fp == NULL)
	{
		return STATUS_FAIL;
	}

	while(1)
	{
		if(fgets(line, sizeof(line), fp) ==NULL)
		{
			IsSuccess = 1;
			break;
		}

		if(strlen(line) <= 2)
		{
			continue;
		}

		line[strlen(line)-1]='\0';
		if(sscanf(line, "%[^:]:%[^:]:%[^:]:%[^:]:%[^:]",
			RID, Index, Exp, checksum, PK) != 5)
		{
			break;
		}

		line[0]='\0';
		strcat(line, RID);
		strcat(line, Index);
		strcat(line, PK);
		strcat(line, Exp);

		len = strlen(line);
		if(len%2 !=0)
		{
			break;
		}

		HexStr2Bin(tmp, sizeof(tmp),line, len);
		sha1_csum(tmp, len/2, calchecksum);
		Bin2HexStr(calchecksum, 20, line);

		printf("checksum is [%s], calchecksum[%s]\n",checksum, line);
		//printf();
		if(strcmp(line, checksum) != 0)
		{
			printf("strcmp != 0\n");
			break;
		}
	}

	fclose(fp);
	if(IsSuccess)
	{
		printf("ok\n");
		return STATUS_OK;
	}
	else
	{
		printf("error\n");
		return STATUS_FAIL;
	}
}
#else
int CheckCA()
{
	char RID[1024];
	char Index[1024];
	char Exp[1024];
	char checksum[1024];
	char PK[1024*5];
	char line[1024*6];
	lfs_file_t fp;
	char calchecksum[20];
	char tmp[1024*6];
	int len;
	int IsSuccess =0;
	int ret;

	dbg("CheckCA\n");

	//fp =fopen(GetConfFilePath(FILE_NAME_CA_LIST), "r");
	//if(fp == NULL)
	//{
	//	return STATUS_FAIL;
	//}

	ret = sys_lfs_file_open(&fp, GetConfFilePath(FILE_NAME_CA_LIST), LFS_O_RDONLY);
	if (ret != 0)
	{
		dbg("sys_lfs_file_open error = %d\n", ret);
		return STATUS_FAIL;
	}

	while(1)
	{
		if(sys_lfs_file_fgets(line, sizeof(line), &fp) ==NULL)
		{
			IsSuccess = 1;
			break;
		}

		if(strlen(line) <= 2)
		{
			continue;
		}

		line[strlen(line)-1]='\0';
		if(sscanf(line, "%[^:]:%[^:]:%[^:]:%[^:]:%[^:]",
			RID, Index, Exp, checksum, PK) != 5)
		{
			break;
		}

		line[0]='\0';
		strcat(line, RID);
		strcat(line, Index);
		strcat(line, PK);
		strcat(line, Exp);

		len = strlen(line);
		if(len%2 !=0)
		{
			break;
		}

		HexStr2Bin(tmp, sizeof(tmp),(tti_byte *)line, len);
		sha1_csum((byte *)tmp, len/2, (byte *)calchecksum);
		Bin2HexStr(calchecksum, 20, line);

		dbg("checksum is [%s], calchecksum[%s]\n",checksum, line);
		//printf();
		if(strcmp(line, checksum) != 0)
		{
			dbg("strcmp != 0\n");
			break;
		}
	}

	sys_lfs_file_close(&fp);
	if(IsSuccess)
	{
		dbg("ok\n");
		return STATUS_OK;
	}
	else
	{
		dbg("error\n");
		return STATUS_FAIL;
	}
}

#endif

int SaveParam(const char *buf, int len)
{
#ifndef CY20_EMVL2_KERNEL
	int i;
	FILE *fp;
	int ret;

	char *filelist[]={FILE_NAME_COMMON, FILE_NAME_AID_LIST, FILE_NAME_CA_LIST, FILE_NAME_EXCEPTION};
	char val[sizeof(filelist)/sizeof(filelist[0])][1024*40];

	ret =sscanf(buf, "%[^\n]\n%[^=]=\n%[^=]=\n%[^=]", val[0], val[1], val[2], val[3]);
	LOGD("data:%s\n",buf);
	if(ret != 4)
	{		
		LOGE("data:%s\n",buf);
		LOGE("Field:%d Parse Error\n", ret);
		DisplayValidateInfo("Parse Error");
		return STATUS_FAIL;
	}

	for(i=0; i<sizeof(filelist)/sizeof(filelist[0]); i++)
	{	
		fp = fopen(GetConfFilePath(filelist[i]), "w");
		if(fp == NULL)
		{
			LOGE("fopen error");
			return STATUS_FAIL;
		}

		fprintf(fp,"%s", val[i]);
		fclose(fp);
	}

	if(CheckCA()!= STATUS_OK)
	{
		DisplayValidateInfo("CA Invalid");
		return STATUS_FAIL;
	}

	return STATUS_OK;
#else
	
	int i;
	lfs_file_t file;
	int ret;
	char fileBuffer[1024*6];

	char *filelist[]={FILE_NAME_COMMON, FILE_NAME_AID_LIST, FILE_NAME_CA_LIST, FILE_NAME_EXCEPTION};
	char val[sizeof(filelist)/sizeof(filelist[0])][1024*40];

	ret =sscanf(buf, "%[^\n]\n%[^=]=\n%[^=]=\n%[^=]", val[0], val[1], val[2], val[3]);
	LOGD("data:%s\n",buf);
	if(ret != 4)
	{		
		LOGE("data:%s\n",buf);
		LOGE("Field:%d Parse Error\n", ret);
		DisplayValidateInfo("Parse Error");
		return STATUS_FAIL;
	}

	for(i=0; i<sizeof(filelist)/sizeof(filelist[0]); i++)
	{	
		//fp = fopen(GetConfFilePath(filelist[i]), "w");
		ret = sys_lfs_file_open(&file, GetConfFilePath(filelist[i]), 
			LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
		
		if(ret != 0)
		{
			LOGE("sys_lfs_file_open error");
			return STATUS_FAIL;
		}

		//fprintf(fp,"%s", val[i]);
		//fclose(fp);
		sprintf(fileBuffer, "%s", val[i]);
		sys_lfs_file_write(&file, fileBuffer, strlen(fileBuffer));
		sys_lfs_file_close(&file);
	}

	if(CheckCA()!= STATUS_OK)
	{
		DisplayValidateInfo("CA Invalid");
		return STATUS_FAIL;
	}

	return STATUS_OK;
#endif
}

void DownloadParam()
{
	int i;
	int ret;	
	int timeout;
	//char IPBuf[MAX_OPT_ITEM_VALUE_LEN];
	char buf[MAX_PARAM_DOWN_BUF_LEN];
    char serverIP[100];
    char serverPort[10];
	int len = sizeof(buf);
	char *filelist[]={FILE_NAME_AID_LIST, FILE_NAME_CA_LIST, FILE_NAME_COMMON, FILE_NAME_EXCEPTION};

	for(i=0; i<sizeof(filelist)/sizeof(filelist[0]); i++)
	{
		sprintf(buf, "%s", GetConfFilePath(filelist[i]));
#ifndef CY20_EMVL2_KERNEL
		unlink(buf);
#else
		sys_lfs_remove(buf);
#endif
	}

	ClearScreen();
	DisplayInfoCenter("connect...", 4);

	timeout = 1000;
    GetServerIPAndPort(serverIP, serverPort);
    LOGD("ServerIP = %s, ServerPort = %s", serverIP, serverPort);
    ret = GetDataFromServer(serverIP, serverPort, timeout, buf, &len);
    if(ret != STATUS_OK)
	{
		if(ret == STATUS_SERV_CONN_TIMEOUT)
		{
			DisplayValidateInfo("Conn Timeout");
			LOGE("Conn Timeout");
		}
		else
		{
			DisplayValidateInfo("Download Fail");
			LOGE("Download Fail");
		}

		return;
	}
    LOGD("Jason GetDataFromServer OK");

	if(CheckOptData(buf, len) != STATUS_OK)
	{
		DisplayValidateInfo("Option Invalid");
		LOGE("Option Invalid");
		return ;
	}

	if(SaveParam(buf, len) != STATUS_OK)
	{
		DisplayValidateInfo("Update Fail");
		LOGE("Update Fail");
		return;
	}

	DisplayValidateInfo("Update Successful");
	LOGD("Update Successful");
	//PrintRequestItemList();
}

#if 0
int LoadTerminalParam()
{	
	int i=0;
	char *CurPos;
	char *EndPos;
	int  DataLen;
	char tmpbuf[128];
	char buf[MAX_PARAM_DOWN_BUF_LEN];
	FILE *fp;
	TTermParam *pItem;

	fp = fopen(GetConfFilePath(FILE_NAME_COMMON), "r");
	if(fp == NULL)
	{
		DisplayValidateInfo("Read Config File Fail.");
		return STATUS_FAIL;
	}

	fgets(buf, sizeof(buf), fp);
	fclose(fp);
	CurPos = buf;
	EndPos = buf + strlen(buf);

	for(i=0; i<sizeof(g_TermParamTable)/sizeof(g_TermParamTable[0]); i++)
	{
		pItem =&g_TermParamTable[i];
		DataLen = GetDataLen(CurPos,(int)(EndPos-CurPos));

		if(DataLen != 0)
		{
			if(pItem->index == REQ_FLOOR_LIMIT
				|| pItem->index == REQ_THRESHOLD_AMOUNT)
			{
				memset(tmpbuf, 0x00, sizeof(tmpbuf));
				memcpy(tmpbuf, CurPos, DataLen);
				SetRequestItemInt(pItem->index, atoi(tmpbuf));
			}
			else
			{
				if(SetRequestItem(pItem->index, CurPos, DataLen) != STATUS_OK)
				{
					return STATUS_FAIL;
				}
			}
		}

		CurPos += DataLen + 1;
		if(CurPos > EndPos)
		{
			DisplayValidateInfo("data format error.");
			return STATUS_FAIL;
		}		
	}

	if(CurPos != EndPos)
	{		
		DisplayValidateInfo("data format error.");
		return STATUS_FAIL;
	}
	
	//PrintRequestItemList();	
	
	return STATUS_OK;
}
#else
int LoadTerminalParam()
{
	return STATUS_OK;
}
#endif


#ifndef CY20_EMVL2_KERNEL
void PrintAidList()
{
	FILE *fp;
	char line[1024];

	NJRCBase_Print_FillBuffer("AID List:\n");
	
	fp =fopen(GetConfFilePath(FILE_NAME_AID_LIST), "r");
	if(fp == NULL)
	{
		return;	
	}

	while(!feof(fp))
	{
		if(fgets(line, sizeof(line), fp) == NULL)
		{
			break;
		}

		//TtiOsPrntBuffer("\t");
		//TtiOsPrntBuffer(line);
		NJRCBase_Print_FillBuffer("\t");
		NJRCBase_Print_FillBuffer(line);
	}

	fclose(fp);
}
#else
void PrintAidList()
{
/*    
	lfs_file_t file;
	char line[1024];
	int ret;

	NJRCBase_Print_FillBuffer("AID List:\n");
	
	ret = sys_lfs_file_open(&file, GetConfFilePath(FILE_NAME_AID_LIST), LFS_O_RDONLY);
	if(ret != 0)
	{
		return;	
	}

	while(1)
	{
		if(sys_lfs_file_fgets(line, sizeof(line), &file) == NULL)
		{
			break;
		}

		//TtiOsPrntBuffer("\t");
		//TtiOsPrntBuffer(line);
		NJRCBase_Print_FillBuffer("\t");
		NJRCBase_Print_FillBuffer(line);
	}

	sys_lfs_file_close(&file);
*/
}

#endif

void PrintParam()
{
#if 0    
	int i;
	char buf[1024];
	vec_t *pData;	
	TTermParam *pItem;

	ResetRequestItemList();
	LoadTerminalParam();
	ClearScreen();
	DisplayInfoCenter("Printing...", 4);


	
	//NJRCBase_Print_ClearBuffer();		//TtiOsClearPrintBuffer();

	PrintHeader("Terminal Parameter");

	for(i=0; i<sizeof(g_TermParamTable)/sizeof(g_TermParamTable[0]); i++)
	{
		pItem = &g_TermParamTable[i];

		//Changed by Siken 2011.09.25
		/*
		if(IsRequestItemExisted(pItem->index))
		{
			pData= GetRequestItemValue(pItem->index);
			sprintf(buf, "%s: %s\n",pItem->header, pData->ptr);
		}
		else
		{
			sprintf(buf, "%s:\n", pItem->header);
		}
		*/

		//Change to:
		if(IsRequestItemExisted(pItem->index))
		{
			pData= GetRequestItemValue(pItem->index);
			//bob added 20110925,fuck ...
			if((0 == strncmp(pItem->header, "TermCtryCode", 12)) || 
			   (0 == strncmp(pItem->header, "TransCurCode", 12)) ||
			   (0 == strncmp(pItem->header, "TransRefCurCode", 15)))
			{
				sprintf(buf, "%s: 0%s\n",pItem->header, pData->ptr);
			}
			else
			{
				sprintf(buf, "%s: %s\n",pItem->header, pData->ptr);
			}
		}
		else
		{
			sprintf(buf, "%s:\n", pItem->header);
		}
		//End
		//TtiOsPrntBuffer(buf);
		NJRCBase_Print_FillBuffer(buf);
	    LOGD("System para:%s", buf);
    }

	PrintAidList();
	PrintFooter();
	
	//NJRCBase_Print_StartPrint();		//TtiOsPrint();
	
	//NJRCBase_Print_Cut();	//CutPaper(CUT_FULL);
	//NJRCBase_TCP_Disconnect();
	
	ResetRequestItemList();
#endif    
}

//tti_byte defaultTerminalType[TERMINAL_TYPE_SIZE + 1]="\x22";
tti_byte defaultTerminalType[TERMINAL_TYPE_SIZE + 1]="\x21";        //仅联机
tti_byte defaultTerminalCapabilitesEmv[TERMINAL_CAPABILITIES_SIZE + 1]="\xE0\xF8\xC8";
#ifdef ENABLE_PRINTER
tti_byte defaultTerminalCapabilitesPBOC[TERMINAL_CAPABILITIES_SIZE + 1]="\xE0\xE9\xC8";
//tti_byte defaultTerminalCapabilitesPBOC[TERMINAL_CAPABILITIES_SIZE + 1]="\xE0\xC9\xC8";
#else
//tti_byte defaultTerminalCapabilitesPBOC[TERMINAL_CAPABILITIES_SIZE + 1]="\xE0\xC9\xC8";
tti_byte defaultTerminalCapabilitesPBOC[TERMINAL_CAPABILITIES_SIZE + 1]="\xE0\x49\xC8";     //屏蔽脱机PIN
#endif

#ifdef ENABLE_PRINTER
tti_byte defaultAdditionalTermCapabilites[ADDTIONAL_TERMINAL_CAPABILITIES_SIZE + 1]="\xF0\x00\xF0\xA0\x01";
#else
tti_byte defaultAdditionalTermCapabilites[ADDTIONAL_TERMINAL_CAPABILITIES_SIZE + 1]="\xF0\x00\xF0\x20\x01";
#endif
tti_byte defaultTerminalCountryCode[2 + 1]="\x08\x40";
tti_byte defaultIFDSerialNumber[8 + 1] = "Terminal";


int GetDownloadTerminalRequest(byte *buf, uint16 *bufLen)
{
	TagList tmpTagList;
	byte Date[10], Time[10];
	//byte tmpTLVBuf[1024];
	byte terminalCountryCode[10];
	uint16 tmpTLVBufLen;
	uint16 dataTotalLen;

	GetCurrentDateAndTime(Date, Time);

	InitTagList(&tmpTagList);

	SetTagValue(TAG_TERMINAL_TYPE, defaultTerminalType, TERMINAL_TYPE_SIZE, &tmpTagList);
	if (GetKernelType() == 0)
	{
		SetTagValue(TAG_TERMINAL_CAPABILITIES, defaultTerminalCapabilitesEmv, TERMINAL_CAPABILITIES_SIZE, &tmpTagList);
	}else{
		SetTagValue(TAG_TERMINAL_CAPABILITIES, defaultTerminalCapabilitesPBOC, TERMINAL_CAPABILITIES_SIZE, &tmpTagList);
	}
	SetTagValue(TAG_ADDITIONAL_TERM_CAPABILITY, defaultAdditionalTermCapabilites, ADDTIONAL_TERMINAL_CAPABILITIES_SIZE, 
		&tmpTagList);
	LoadTerminalParaBctc(terminalCountryCode);
	SetTagValue(TAG_TERM_COUNTRY_CODE, terminalCountryCode, 2, 
		&tmpTagList);
	SetTagValue(TAG_IFD_SERIAL_NUMBER, defaultIFDSerialNumber, 8, 
		&tmpTagList);
	SetTagValue(TAG_TRANSACTION_DATE, Date, 3, &tmpTagList);
	SetTagValue(TAG_TRANSACTION_TIME, Time, 3, &tmpTagList);

    TagListBuildTLV(&tmpTagList, buf + 9, &tmpTLVBufLen);

	dataTotalLen = 5 + tmpTLVBufLen;

	//Head
	buf[0] = STX;
	buf[1] = 0xC4;
	buf[2] = dataTotalLen / 256;
	buf[3] = dataTotalLen % 256;

	//Tag 0xDF8104
	buf[4] = 0xDF;
	buf[5] = 0x81;
	buf[6] = 0x04;
	buf[7] = 0x01;
	buf[8] = 0x00;
	
	*bufLen = dataTotalLen + 4;

	FreeTagList(&tmpTagList);
	
	return STATUS_OK;
}

int GetDownloadAidRequest(byte *buf, uint16 *bufLen)
{	
	//Head
	buf[0] = STX;
	buf[1] = 0xC3;
	buf[2] = 00;
	buf[3] = 00;

	*bufLen = 4;
	
	return STATUS_OK;
}

int GetDownloadCaRequest(byte *buf, uint16 *bufLen)
{	
	//Head
	buf[0] = STX;
	buf[1] = 0xC2;
	buf[2] = 00;
	buf[3] = 00;

	*bufLen = 4;
	
	return STATUS_OK;
}

int GetDownloadExcepfileRequest(byte *buf, uint16 *bufLen)
{	
	//Head
	buf[0] = STX;
	buf[1] = 0xC5;
	buf[2] = 00;
	buf[3] = 00;

	*bufLen = 4;
	
	return STATUS_OK;
}

#ifndef CY20_EMVL2_KERNEL
int SaveTerminalPara(char *buf, int len)
{
	FILE *fp;
	int ret;
	char fileName[512];
	byte Date[3];
	byte Time[3];
	byte tmp[10];
	byte tmp2[10];
	TagList tmpTagList;
	Tag3ByteList tmpTag3ByteList;

	LOGD("SaveTerminalPara list buff!!!!");
	hexdump(buf, len);

	InitTagList(&tmpTagList);
	InitTag3ByteList(&tmpTag3ByteList);

	ret = BuildTagListOneLevelBctc(buf, len, &tmpTagList, &tmpTag3ByteList);
	if (ret != STATUS_OK)
	{
		FreeTagList(&tmpTagList);
		LOGE("BuildTagListOneLevelBctc error");
		return STATUS_FAIL;
	}

	LOGD("88888");
	SetCurrentDateAndTime(GetTagValue(&tmpTagList, TAG_TRANSACTION_DATE), 
				GetTagValue(&tmpTagList, TAG_TRANSACTION_TIME));

	LOGD("123456");
	GetCurrentDateAndTime(Date, Time);
	Bin2HexStr(Date, 3, tmp);	
	Bin2HexStr(Time, 3, tmp2);
	LOGD("Date = %s, Time = %s", tmp, tmp2);
	
	sprintf(fileName, "%s", GetConfFilePath(FILE_NAME_TERM_PARA_BCTC));
	
	fp = fopen(fileName, "w");
	if(fp == NULL)
	{
		FreeTagList(&tmpTagList);
		LOGE("fopen error");
		return STATUS_FAIL;
	}

	/*
	if (fwrite(buf, len, 1, fp) != 1)
	{	
		fclose(fp); 
		LOGE("fwrite error");
		return STATUS_FAIL;
	}
	*/
	if (fwrite(GetTagValue(&tmpTagList, TAG_TERM_COUNTRY_CODE), 
		GetTagValueSize(&tmpTagList, TAG_TERM_COUNTRY_CODE), 1, fp) != 1)
	{	
		FreeTagList(&tmpTagList);
		fclose(fp); 
		LOGE("fwrite error");
		return STATUS_FAIL;
	}

	FreeTagList(&tmpTagList);
	fclose(fp);	

	return STATUS_OK;
}
#else
int SaveTerminalPara(char *buf, int len)
{
	lfs_file_t file;
	int ret;
	char fileName[512];
	byte Date[3];
	byte Time[3];
	byte tmp[10];
	byte tmp2[10];
	TagList tmpTagList;
	Tag3ByteList tmpTag3ByteList;

	LOGD("SaveTerminalPara list buff!!!!");
	hexdump((tti_byte *)buf, len);

	InitTagList(&tmpTagList);
	InitTag3ByteList(&tmpTag3ByteList);

	ret = BuildTagListOneLevelBctc((tti_byte *)buf, len, &tmpTagList, &tmpTag3ByteList);
	if (ret != STATUS_OK)
	{
		FreeTagList(&tmpTagList);
		LOGE("BuildTagListOneLevelBctc error");
		return STATUS_FAIL;
	}

	LOGD("Before set sys date time\n");
	GetCurrentDateAndTime(Date, Time);
	Bin2HexStr((char *)Date, 3, (char *)tmp);	
	Bin2HexStr((char *)Time, 3, (char *)tmp2);
	LOGD("Date = %s, Time = %s", tmp, tmp2);

	LOGD("88888");
	SetCurrentDateAndTime(GetTagValue(&tmpTagList, TAG_TRANSACTION_DATE), 
				GetTagValue(&tmpTagList, TAG_TRANSACTION_TIME));

	LOGD("123456");
	GetCurrentDateAndTime(Date, Time);
	Bin2HexStr((char *)Date, 3, (char *)tmp);	
	Bin2HexStr((char *)Time, 3, (char *)tmp2);
	LOGD("Date = %s, Time = %s", tmp, tmp2);
	
	sprintf(fileName, "%s", GetConfFilePath(FILE_NAME_TERM_PARA_BCTC));
	
	ret = sys_lfs_file_open(&file, fileName, LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
	if(ret != 0)
	{
		FreeTagList(&tmpTagList);
		LOGE("sys_lfs_file_open error = %d", ret);
		return STATUS_FAIL;
	}

	/*
	if (fwrite(buf, len, 1, fp) != 1)
	{	
		fclose(fp); 
		LOGE("fwrite error");
		return STATUS_FAIL;
	}
	*/
	ret = sys_lfs_file_write(&file, GetTagValue(&tmpTagList, TAG_TERM_COUNTRY_CODE), 
		GetTagValueSize(&tmpTagList, TAG_TERM_COUNTRY_CODE)) ;
	sys_lfs_file_close(&file);
	if (ret != GetTagValueSize(&tmpTagList, TAG_TERM_COUNTRY_CODE))
	{	
		FreeTagList(&tmpTagList); 
		LOGE("sys_lfs_file_write error");
		return STATUS_FAIL;
	}

	FreeTagList(&tmpTagList);
	return STATUS_OK;
}

#endif

//terminalCountryCodeBin will be filled as like "\x08\x40"
int LoadTerminalParaBctc(byte *terminalCountryCodeBin)
{
#ifndef CY20_EMVL2_KERNEL	
	int fd;
	char buf[1024];
	int len;
	
	fd = open(GetConfFilePath(FILE_NAME_TERM_PARA_BCTC), O_RDONLY);
	if(fd < 0)
	{
		LOGD("open FILE_NAME_TERM_PARA_BCTC error");
		//No Terminal Parameter, need set it to default value.
		if (GetKernelType() == 0)
		{
			memcpy(terminalCountryCodeBin, "\x08\x40", 2);
		}else
		{
			memcpy(terminalCountryCodeBin, "\x01\x56", 2);
		}
	}
	else
	{
		len = sizeof(buf);
		len = read(fd, buf, len);
		if (len <= 0)
		{
			LOGE("read ERR");
			close(fd);
			return -1;
		}

		//hexdumpEx("List download terminalCountryCodeBin", buf, 2);
		memcpy(terminalCountryCodeBin, buf, 2);
		close(fd);
	}

	return 0;
#else
	int ret;
	lfs_file_t file;
	char buf[1024];
	int len;
	
	ret = sys_lfs_file_open(&file, GetConfFilePath(FILE_NAME_TERM_PARA_BCTC), LFS_O_RDONLY);
	if(ret != 0)
	{
		LOGD("open FILE_NAME_TERM_PARA_BCTC error");
		//No Terminal Parameter, need set it to default value.
		if (GetKernelType() == 0)
		{
			memcpy(terminalCountryCodeBin, "\x08\x40", 2);
		}else
		{
			memcpy(terminalCountryCodeBin, "\x01\x56", 2);
		}
	}
	else
	{
		len = sizeof(buf);
		ret = sys_lfs_file_read(&file, buf, len);
		sys_lfs_file_close(&file);
		if (ret <= 0)
		{
			LOGE("sys_lfs_file_read error = %d\n", ret);
			return -1;
		}

		//hexdumpEx("List download terminalCountryCodeBin", buf, 2);
		memcpy(terminalCountryCodeBin, buf, 2);
	}

	return 0;
#endif	
}

extern TagList gAidParameterTagList;
extern Tag3ByteList gAidParaTag3ByteList;


int LoadAidParaByAid(tti_tchar * aidStr, TagList *aidParaTagList, Tag3ByteList *aidPara3TagList)
{
	int index;
	int fd;
	byte buf[1024];
	int tagBinLen;
	char tagAscii[2048];
	char filename[200];
	char selectAid[100];
	int selectAidStrLen;
	int tagAsciiLen;
	int len;
	lfs_file_t file;
	//Tag3ByteList tmpTag3ByteList;

	strcpy(selectAid, aidStr);
	//TranslateHexToChars(aid, aidLen, selectAid);
	selectAidStrLen = strlen(selectAid);

	//Check same length AID at first
#ifdef VPOS_APP
    for(index = 0; index < AID_NUMBER && index<gl_SysInfo.uiAidNum; index ++)
	{
        if(uiMemManaGetAid(index, (uchar*)buf, (unsigned int *)&len))
            break;
#else   	
    for(index = 0; index < AID_NUMBER; index ++)
	{     
		sprintf(filename, "%s%d", GetConfFilePath(FILE_NAME_AID_LIST_PARA_BCTC), index);
#ifndef CY20_EMVL2_KERNEL	
		fd = open(filename, O_RDONLY);
		if(fd <0)
		{
			break;
		}

		len = read(fd, buf, sizeof(buf));
		close(fd);
#else
		fd = sys_lfs_file_open(&file, filename, LFS_O_RDONLY);
		if (fd != 0)
		{
			break;
		}

		len = sys_lfs_file_read(&file, buf, sizeof(buf));
		sys_lfs_file_close(&file);
#endif
		if (len <= 0)
		{
			DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;
		}
#endif

        hexdumpEx("AID record list", buf, len);//chencheng
		FreeTagList(&gAidParameterTagList);
		InitTagList(&gAidParameterTagList);
		InitTag3ByteList(&gAidParaTag3ByteList);
		
		BuildTagListOneLevelBctc(buf, len, &gAidParameterTagList, &gAidParaTag3ByteList);
		//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
		//PrintOutTag3ByteList(&gAidParaTag3ByteList, "Now list AID 3 BYTE tag list");

		tagBinLen = GetTagValueSize(&gAidParameterTagList, TAG_TERMINAL_AID);
		HextoA(tagAscii, GetTagValue(&gAidParameterTagList, TAG_TERMINAL_AID), 
			tagBinLen);
		tagAsciiLen = tagBinLen*2;
		tagAscii[tagAsciiLen] = 0x00;

		LOGD("SelectAID = %s, selectAidStrLen = %d", selectAid, selectAidStrLen);//chencheng
		LOGD("tagAscii = %s, tagAsciiLen = %d", tagAscii, tagAsciiLen);//chencheng
		
		if (tagAsciiLen > selectAidStrLen)
		{
			//Parameter Aid length is longer than select Aid, not match
			FreeTagList(&gAidParameterTagList);
			continue;
		}else if(tagAsciiLen == selectAidStrLen){
			if (memcmp(tagAscii, selectAid, selectAidStrLen) != 0)
			{
				//Parameter Aid length is same with select Aid, but value no match
				FreeTagList(&gAidParameterTagList);
				continue;
			}else
			{
				LOGD("Find the same length parameter AID");
				LOGD("SelectAID = %s", selectAid);
				LOGD("tagAscii = %s", tagAscii);

				//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
				//PrintOutTag3ByteList(&gAidParaTag3ByteList, "Now list AID 3 BYTE tag list");

				return STATUS_OK;
				
			}
		}else
		{
			//Parameter Aid length is short than select Aid
			//Not check now, must check the same length at first
			FreeTagList(&gAidParameterTagList);
			continue;
		}
		
	}


#ifdef VPOS_APP
    for(index = 0; index < AID_NUMBER && index<gl_SysInfo.uiAidNum; index ++)
	{
        if(uiMemManaGetAid(index, (uchar*)buf, (unsigned int *)&len))
            break;
#else
	//Then Check Parameter Aid length is short than select Aid
	for(index = 0; index < AID_NUMBER; index ++)
	{
		sprintf(filename, "%s%d", GetConfFilePath(FILE_NAME_AID_LIST_PARA_BCTC), index);
#ifndef CY20_EMVL2_KERNEL
		fd = open(filename, O_RDONLY);

		if(fd <0)
		{
			break;
		}

		len = read(fd, buf, sizeof(buf));
		close(fd);
#else
		fd = sys_lfs_file_open(&file, filename, LFS_O_RDONLY);
		if (fd != 0)
		{
			break;
		}

		len = sys_lfs_file_read(&file, buf, sizeof(buf));
		sys_lfs_file_close(&file);
#endif		
		if (len <= 0)
		{
			DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;
		}
#endif
		

        hexdumpEx("AID record list", buf, len);//chencheng
		FreeTagList(&gAidParameterTagList);
		InitTagList(&gAidParameterTagList);
		InitTag3ByteList(&gAidParaTag3ByteList);
		
		BuildTagListOneLevelBctc(buf, len, &gAidParameterTagList, &gAidParaTag3ByteList);
		//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
		//PrintOutTag3ByteList(&gAidParaTag3ByteList, "Now list AID 3 BYTE tag list");

		tagBinLen = GetTagValueSize(&gAidParameterTagList, TAG_TERMINAL_AID);
		HextoA(tagAscii, GetTagValue(&gAidParameterTagList, TAG_TERMINAL_AID), 
			tagBinLen);
		tagAsciiLen = tagBinLen*2;
		tagAscii[tagAsciiLen] = 0x00;

		LOGD("SelectAID = %s, selectAidStrLen = %d", selectAid, selectAidStrLen);//chencheng
		LOGD("tagAscii = %s, tagAsciiLen = %d", tagAscii, tagAsciiLen);//chencheng
		
		if (tagAsciiLen > selectAidStrLen)
		{
			//Parameter Aid length is longer than select Aid, not match
			FreeTagList(&gAidParameterTagList);
			continue;
		}else if(tagAsciiLen == selectAidStrLen){
			//Same length, already check, no need check again
			FreeTagList(&gAidParameterTagList);
			continue;
		}
		else
		{
			//Parameter Aid length is short than select Aid
			//Check if Para Aid support part match
			if (memcmp(GetTagValue(&gAidParameterTagList, TAG_AID_PART_MATCH_BCTC), "\x0", 1) == 0)
			{
				//Parameter Aid not support part match, so no need compare, just continue
				FreeTagList(&gAidParameterTagList);
				continue;
			}else
			{
				//Parameter Aid support part match, so need compare the Parameter Aid and select Aid with parameter aid length
				if (memcmp(tagAscii, selectAid, tagAsciiLen) != 0)
				{
					//Parameter Aid not support part match, so no need compare, just continue
					FreeTagList(&gAidParameterTagList);
					continue;
				}else
				{
					//Find the parameter AID
					LOGD("Find the short length parameter AID");
					LOGD("SelectAID = %s", selectAid);
					LOGD("tagAscii = %s", tagAscii);

					//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
					//PrintOutTag3ByteList(&gAidParaTag3ByteList, "Now list AID 3 BYTE tag list");
					return STATUS_OK;
				}
			}
		}
	}

	LOGE("No find the AID Parameter1");
	return STATUS_FAIL;
}


int SaveAid(char *buf, int len, int index)
{
#ifdef VPOS_APP
{
    uint ret;
    ret = uiMemManaPutAid(index, (uchar*)buf, len);
    if(ret)
        return STATUS_FAIL;
    else
        return STATUS_OK;
}
#else    
    
#ifndef CY20_EMVL2_KERNEL		
	FILE *fp;
	int ret;
	char fileName[512];

	sprintf(fileName, "%s%d", GetConfFilePath(FILE_NAME_AID_LIST_PARA_BCTC), index);
	
	fp = fopen(fileName, "w");
	if(fp == NULL)
	{
		LOGE("fopen error");
		return STATUS_FAIL;
	}

	LOGD("sizeof(tti_uint16) is %d", sizeof(tti_uint16));

	LOGD("Save AID List");
	//hexdump(buf, len);
	if (fwrite(buf, len, 1, fp) != 1)
	{
		LOGE("fwrite error");
		return STATUS_FAIL;
	}
	
	fclose(fp);	

	return STATUS_OK;
#else
	lfs_file_t file;
	int ret;
	char fileName[512];

	sprintf(fileName, "%s%d", GetConfFilePath(FILE_NAME_AID_LIST_PARA_BCTC), index);
	
	ret = sys_lfs_file_open(&file, fileName, LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
	if(ret != 0)
	{
		LOGE("sys_lfs_file_open error");
		return STATUS_FAIL;
	}

	LOGD("sizeof(tti_uint16) is %d", sizeof(tti_uint16));

	LOGD("Save AID List");
	//hexdump(buf, len);
	ret = sys_lfs_file_write(&file, buf, len);
	sys_lfs_file_close(&file);
	if (ret != len)
	{
		LOGE("sys_lfs_file_write error");
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
#endif
#endif
}


//------------------------------------------------------------------------------------------
tti_bool isCorrectCABctc(char *buf, int len)
{
	tti_byte tempBuff[BUFFER_SIZE_1K];
	tti_uint16 tempBuffLen;
	tti_byte tagCheckSum[HASH_RESULT_SIZE];
	tti_byte calCheckSum[HASH_RESULT_SIZE];

	TagList tmpTagList;

	InitTagList(&tmpTagList);

	if (BuildTagListOneLevel((tti_byte *)buf, len, &tmpTagList) != STATUS_OK)
	{
		LOGE("BuildTagListOneLevel ERR");
		FreeTagList(&tmpTagList);
		return FALSE;
	}

	PrintOutTagList(&tmpTagList, "CA TagList");
	
	if (TagIsExisted(&tmpTagList, TAG_CA_PUBLIC_KEY_INDEX_TERM) == FALSE||
		TagIsExisted(&tmpTagList, TAG_TERMINAL_AID) == FALSE || 
		TagIsExisted(&tmpTagList, TAG_CA_PUBLIC_KEY_EFFECTIVE_DATE_BCTC) == FALSE ||
		TagIsExisted(&tmpTagList, TAG_CA_PUBLIC_KEY_HASH_ALG_BCTC) == FALSE ||
		TagIsExisted(&tmpTagList, TAG_CA_PUBLIC_KEY_SIGN_ALG_BCTC) == FALSE ||
		TagIsExisted(&tmpTagList, TAG_CA_PUBLIC_KEY_MOD_BCTC) == FALSE ||
		TagIsExisted(&tmpTagList, TAG_CA_PUBLIC_KEY_EXPONENT_BCTC) == FALSE ||
		TagIsExisted(&tmpTagList, TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC) == FALSE ||
		TagIsExisted(&tmpTagList, TAG_CA_PUBLIC_KEY_SN_BCTC) == FALSE)
	{
		LOGD("No TAG");
		FreeTagList(&tmpTagList);
		return FALSE;
	}

	if (GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC) != HASH_RESULT_SIZE)
	{
		LOGD("Hash size error");
		FreeTagList(&tmpTagList);
		return FALSE;
	}
	memcpy(tagCheckSum, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC), HASH_RESULT_SIZE);

	removeTag(&tmpTagList, TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC);
	//removeTag(&tmpTagList, TAG_CA_PUBLIC_KEY_SN_BCTC);
	TagListBuildTLV(&tmpTagList, tempBuff, &tempBuffLen);
		

	sha1_csum(tempBuff, tempBuffLen, calCheckSum);

	LOGD("List calCheckSum");
	hexdump(calCheckSum, HASH_RESULT_SIZE);

	LOGD("List tagCheckSum");
	hexdump(tagCheckSum, HASH_RESULT_SIZE);
	
	if (memcmp(calCheckSum, tagCheckSum, HASH_RESULT_SIZE) != 0)
	{
		LOGD("calCheckSum and calCheckSum with different value");
		FreeTagList(&tmpTagList);
		return FALSE;
	}

	FreeTagList(&tmpTagList);
	return TRUE;
}

#ifndef CY20_EMVL2_KERNEL
int SaveCA(char *buf, int len, int index)
{
	FILE *fp;
	int ret;
	char fileName[512];

	sprintf(fileName, "%s%d", GetConfFilePath(FILE_NAME_CA_LIST_BCTC), index);
	
	fp = fopen(fileName, "w");
	if(fp == NULL)
	{
		LOGE("fopen error");
		return STATUS_FAIL;
	}

	if (fwrite(buf, len, 1, fp) != 1)
	{
		LOGE("fwrite error");
		return STATUS_FAIL;
	}
	
	fclose(fp);	

	return STATUS_OK;
}
#else
int SaveCA(char *buf, int len, int index)
{
//	lfs_file_t file;
//	int ret;
//	char fileName[200];
#ifdef VPOS_APP
{
    uint ret;
    ret = uiMemManaPutCaPubKey(index, (uchar*)buf, len);
    if(ret)
        return STATUS_FAIL;
    else
        return STATUS_OK;
}
#else 
    
    
	sprintf(fileName, "%s%d", GetConfFilePath(FILE_NAME_CA_LIST_BCTC), index);
	
	ret = sys_lfs_file_open(&file, fileName, LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
	if(ret != 0)
	{
		LOGE("sys_lfs_file_open error = %d", ret);
		return STATUS_FAIL;
	}

	ret = sys_lfs_file_write(&file, buf, len);
	sys_lfs_file_close(&file);
	if (ret != len)
	{
		LOGE("sys_lfs_file_write error, ret = %d\n", ret);
		return STATUS_FAIL;
	}

	return STATUS_OK;
#endif
}

#endif

int SaveExcpFile(char *buf, int len, int index)
{
	FILE *fp;
	//int ret;
	char fileName[512];

	sprintf(fileName, "%s%d", GetConfFilePath(FILE_NAME_EXCEPTION_BCTC), index);
	
	fp = fopen(fileName, "w");
	if(fp == NULL)
	{
		LOGE("fopen error");
		return STATUS_FAIL;
	}

	if (fwrite(buf, len, 1, fp) != 1)
	{
		LOGE("fwrite error");
		return STATUS_FAIL;
	}
	
	fclose(fp);	

	return STATUS_OK;
}

#define DOWNLOAD_PARAMETERS_SND_TIMEOUT_MS	(1*1000)
#define DOWNLOAD_PARAMETERS_RCV_TIMEOUT_MS	(1*1000)

void DownloadTerminalPara(void)
{
	char szServerIP[20];
	char szServerPort[10];
	byte requstBuf[BUFFER_SIZE_1K];
	uint16 requestLen;
	byte receiveBuf[BUFFER_SIZE_1K];
	uint16 receiveBufLen;
	char buf[100]; 
	int fd;

	DisplayProcess("下载中");

	sprintf(buf, "%s", GetConfFilePath(FILE_NAME_TERM_PARA_BCTC));
#ifndef CY20_EMVL2_KERNEL
	unlink(buf);
#else
	sys_lfs_remove(buf);
#endif

	GetServerIPAndPort(szServerIP, szServerPort);

	fd = SocketConnect(szServerIP, szServerPort, 10);
	if (fd < 0)
	{
		LOGE("SocketConnect ERROR");
		DisplayValidateInfo("Conn Timeout");
		return; 
	}

	GetDownloadTerminalRequest(requstBuf, &requestLen);

	if (SocketSendData(fd, requstBuf, requestLen, DOWNLOAD_PARAMETERS_SND_TIMEOUT_MS) != requestLen)
	{
		LOGE("SocketSendData ERR");
		DisplayValidateInfo("Send data err");
		SocketDisconnect(fd);
		return;
	}

	if (SocketRcvData(fd, receiveBuf, 4, DOWNLOAD_PARAMETERS_RCV_TIMEOUT_MS) != 4)
	{
		LOGE("receiveBuf 4 byte ERR");
		DisplayValidateInfo("receive data error");
		SocketDisconnect(fd);
		return;
	}

	receiveBufLen = receiveBuf[2] * 256 + receiveBuf[3];
	LOGD("receiveBufLen = %d", receiveBufLen);
	if (SocketRcvData(fd, receiveBuf + 4, receiveBufLen, DOWNLOAD_PARAMETERS_RCV_TIMEOUT_MS) != receiveBufLen)
	{
		LOGE("receiveBuf ERR");
		DisplayValidateInfo("receive data error");
		SocketDisconnect(fd);
		return;
	}

	if (STATUS_OK != SaveTerminalPara((char *)(receiveBuf + 4), receiveBufLen))
	{
		LOGE("SaveAid ERR");
		DisplayValidateInfo("save data error");
		SocketDisconnect(fd);
		return;
	}

	DisplayValidateInfo("下载成功");
	SocketDisconnect(fd);
	return;
}

void DownloadAIDPara(void)
{
	char szServerIP[20];
	char szServerPort[10];
	byte requstBuf[BUFFER_SIZE_1K];
	uint16 requestLen;
	byte receiveBuf[BUFFER_SIZE_1K];
	uint16 receiveBufLen;
	//char buf[100]; 
	int fd;
	int index = 0;
	TagList tmpTagList;
	Tag3ByteList tmpTag3byteList;
	char szDispBuf[30];

	DisplayProcess("下载中");
	
#ifndef CY20_EMVL2_KERNEL
	sprintf(buf, "%s*", GetConfFilePath(FILE_NAME_AID_LIST_PARA_BCTC));
	unlink(buf);
#else
	sys_lfs_filespart_remove(DIR_CONF, FILE_NAME_AID_LIST_PARA_BCTC);
	//sys_lfs_remove_ex(DIR_CONF);
#endif


	GetServerIPAndPort(szServerIP, szServerPort);

	fd = SocketConnect(szServerIP, szServerPort, 10);
	if (fd < 0)
	{
		LOGE("SocketConnect ERROR");
		DisplayValidateInfo("Conn Timeout");
		return; 
	}

	while(1)
	{
		sprintf(szDispBuf, "%s:AID%d", "下载中", index);
		DisplayProcess(szDispBuf);
		
		GetDownloadAidRequest(requstBuf, &requestLen);

		if (SocketSendData(fd, requstBuf, requestLen, DOWNLOAD_PARAMETERS_SND_TIMEOUT_MS) != requestLen)
		{
			LOGE("SocketSendData ERR");
			DisplayValidateInfo("Send data err");
			SocketDisconnect(fd);
			return;
		}

		if (SocketRcvData(fd, receiveBuf, 4, DOWNLOAD_PARAMETERS_RCV_TIMEOUT_MS) != 4)
		{
			LOGE("receiveBuf 4 byte ERR");
			DisplayValidateInfo("receive data error");
			SocketDisconnect(fd);
			return;
		}

		receiveBufLen = receiveBuf[2] * 256 + receiveBuf[3];
		LOGD("receiveBufLen = %d", receiveBufLen);
		if (SocketRcvData(fd, receiveBuf + 4, receiveBufLen, DOWNLOAD_PARAMETERS_RCV_TIMEOUT_MS) != receiveBufLen)
		{
			LOGE("receiveBuf ERR");
			DisplayValidateInfo("receive data error");
			SocketDisconnect(fd);
			return;
		}

		if (receiveBufLen <= 10)
		{
			LOGD("receiveBufLen = %d, no more aid file", receiveBufLen);
			SocketDisconnect(fd);
			DisplayValidateInfo("下载成功");
			return;
		}


		InitTagList(&tmpTagList);
		InitTag3ByteList(&tmpTag3byteList);
		BuildTagListOneLevelBctc(receiveBuf + 4, receiveBufLen, &tmpTagList, &tmpTag3byteList);
		PrintOutTagList(&tmpTagList, "tmpTagList");
		PrintOutTag3ByteList(&tmpTag3byteList, "tmpTag3byteList");
		FreeTagList(&tmpTagList);
		
		if (STATUS_OK != SaveAid((char *)(receiveBuf + 4), receiveBufLen, index))
		{
			LOGE("SaveAid ERR");
			DisplayValidateInfo("save data error");
			SocketDisconnect(fd);
		}
		index ++;
	}

	//SocketDisconnect(fd);
	//return;
}

void DownloadCA(void)
{
	char szServerIP[20];
	char szServerPort[10];
	byte requstBuf[BUFFER_SIZE_1K];
	uint16 requestLen;
	//byte receiveBuf[BUFFER_SIZE_10K];
	byte receiveBuf[BUFFER_SIZE_2K];
	uint16 receiveBufLen;
	//char buf[200]; 
	int fd;
	int index = 0;
	char szDispBuf[30];

	DisplayProcess("下载CA中");
	
#ifndef CY20_EMVL2_KERNEL
	sprintf(buf, "%s*", GetConfFilePath(FILE_NAME_CA_LIST_BCTC));
	unlink(buf);
#else
	sys_lfs_filespart_remove(DIR_CONF, FILE_NAME_CA_LIST_BCTC);
	//sys_lfs_remove_ex(DIR_CONF);
#endif

	GetServerIPAndPort(szServerIP, szServerPort);

	fd = SocketConnect(szServerIP, szServerPort, 10);
	if (fd < 0)
	{
		LOGE("SocketConnect ERROR");
		DisplayValidateInfo("Conn Timeout");
		return; 
	}

	while(1)
	{
		sprintf(szDispBuf, "%s:CA%d", "下载中", index);
		DisplayProcess(szDispBuf);
	
		GetDownloadCaRequest(requstBuf, &requestLen);

		if (SocketSendData(fd, requstBuf, requestLen, DOWNLOAD_PARAMETERS_SND_TIMEOUT_MS) != requestLen)
		{
			LOGE("SocketSendData ERR");
			DisplayValidateInfo("Send data err");
			SocketDisconnect(fd);
			return;
		}

		if (SocketRcvData(fd, receiveBuf, 4, DOWNLOAD_PARAMETERS_RCV_TIMEOUT_MS) != 4)
		{
			LOGE("receiveBuf 4 byte ERR");
			DisplayValidateInfo("receive data error");
			SocketDisconnect(fd);
			return;
		}

		receiveBufLen = receiveBuf[2] * 256 + receiveBuf[3];
		LOGD("receiveBufLen = %d", receiveBufLen);
		if (SocketRcvData(fd, receiveBuf + 4, receiveBufLen, DOWNLOAD_PARAMETERS_RCV_TIMEOUT_MS) != receiveBufLen)
		{
			LOGE("receiveBuf ERR");
			DisplayValidateInfo("receive data error");
			SocketDisconnect(fd);
			return;
		}

		if (receiveBufLen <= 10)
		{
			LOGD("receiveBufLen = %d, no more aid file", receiveBufLen);
			SocketDisconnect(fd);
			DisplayValidateInfo("下载成功");
			return;
		}

		if (isCorrectCABctc((char *)(receiveBuf + 4), receiveBufLen) == FALSE)
		{
			LOGD("INDEX = %d", index);
            LOGE("isCorrectCABctc is FALSE");
			SocketDisconnect(fd);
			DisplayValidateInfo("CA格式错误");
			return;
		}

		if (STATUS_OK != SaveCA((char *)(receiveBuf + 4), receiveBufLen, index))
		{
			LOGE("SaveAid ERR");
			DisplayValidateInfo("SaveAid ERR");
			SocketDisconnect(fd);
			return;
		}
		index++;
	}

//	SocketDisconnect(fd);
//	return;
}


void DownloadExcpFile()
{
	char szServerIP[20];
	char szServerPort[10];
	byte requstBuf[BUFFER_SIZE_1K];
	uint16 requestLen;
	byte receiveBuf[BUFFER_SIZE_10K];
	uint16 receiveBufLen;
	char buf[100]; 
	int fd;
	int index = 0;

	sprintf(buf, "%s*", GetConfFilePath(FILE_NAME_EXCEPTION_BCTC));
#ifndef CY20_EMVL2_KERNEL
	unlink(buf);
#else
	sys_lfs_filespart_remove(DIR_CONF, FILE_NAME_EXCEPTION_BCTC);	
	//sys_lfs_remove_ex(DIR_CONF);
#endif

	GetServerIPAndPort(szServerIP, szServerPort);

	fd = SocketConnect(szServerIP, szServerPort, 10);
	if (fd < 0)
	{
		LOGE("SocketConnect ERROR");
		DisplayValidateInfo("Conn Timeout");
		return; 
	}

	while(1)
	{
		GetDownloadCaRequest(requstBuf, &requestLen);

		if (SocketSendData(fd, requstBuf, requestLen, 10*1000) != requestLen)
		{
			LOGE("SocketSendData ERR");
			DisplayValidateInfo("Send data err");
			SocketDisconnect(fd);
			return;
		}

		if (SocketRcvData(fd, receiveBuf, 4, 10*1000) != 4)
		{
			LOGE("receiveBuf 4 byte ERR");
			DisplayValidateInfo("receive data error");
			SocketDisconnect(fd);
			return;
		}

		receiveBufLen = receiveBuf[2] * 256 + receiveBuf[3];
		LOGD("receiveBufLen = %d", receiveBufLen);
		if (SocketRcvData(fd, receiveBuf + 4, receiveBufLen, 10*1000) != receiveBufLen)
		{
			LOGE("receiveBuf ERR");
			DisplayValidateInfo("receive data error");
			SocketDisconnect(fd);
			return;
		}

		if (receiveBufLen <= 10)
		{
			LOGD("receiveBufLen = %d, no more aid file", receiveBufLen);
			SocketDisconnect(fd);
			DisplayValidateInfo("涓嬭浇鎴愬姛");
			return;
		}

		if (STATUS_OK != SaveExcpFile((char *)(receiveBuf + 4), receiveBufLen, index))
		{
			LOGE("SaveAid ERR");
			DisplayValidateInfo("save data error");
			SocketDisconnect(fd);
		}
		index++;
	}

	//SocketDisconnect(fd);
	//return;
}

char gAutoAmount[20] = {0};
char gAutoAmountOther[20] = {0};
byte gAutoTransType = 0;
int giAutoAmount;
int giAutoAmountOther;

void GetAutoTransData(int *autoAmount, int *autoAmountOther, unsigned char *autoTransType)
{
	*autoAmount = giAutoAmount;
	*autoAmountOther = giAutoAmountOther;
	*autoTransType = gAutoTransType;
}

void GetAutoTestData(char *autoAmount, char *autoAmountOther, unsigned char *autoTransType)
{
	strcpy(autoAmount, gAutoAmount);
    strcpy(autoAmountOther, gAutoAmountOther);
    *autoTransType = gAutoTransType;
}

int32 ParseTransStartResponse(const byte buf[], uint16 size, TagList *pTagList, Tag3ByteList *pTag3ByteList);

int AutoTransaction(char *autoAmount, char *autoAmountOther, unsigned char *transactionType)
{
	char szServerIP[20];
	char szServerPort[10];
  //char filename[100];
	byte requstBuf[BUFFER_SIZE_1K];
	uint16 requestLen;
	byte receiveBuf[BUFFER_SIZE_1K];
	uint16 receiveBufLen;
	byte tmpBuf[BUFFER_SIZE_1K]; 
	tti_int32 tmpLen;
	TagList tmpTagList;
	Tag3ByteList tmpTag3ByteList;
	int fd;
	int amount = 0;
	int amountOther = 0;

	DisplayProcess("后台交互处理中");

    GetServerIPAndPort(szServerIP, szServerPort);

	fd = SocketConnect(szServerIP, szServerPort, 10);
	if (fd < 0)
	{
		LOGE("SocketConnect ERROR");
		DisplayValidateInfo("Conn Timeout");
		return -1;
	}

	GetTransStartRequest(requstBuf, &requestLen);

	if (SocketSendData(fd, requstBuf, requestLen, 10*1000) != requestLen)
	{
		LOGE("SocketSendData ERR");
		DisplayValidateInfo("Send data err");
		SocketDisconnect(fd);
		return -1;
	}

	if (SocketRcvData(fd, receiveBuf, 4, 10*1000) != 4)
	{
		LOGE("receiveBuf 4 byte ERR");
		DisplayValidateInfo("receive data error");
		SocketDisconnect(fd);
		return -1;
	}

	receiveBufLen = receiveBuf[2] * 256 + receiveBuf[3];
	LOGD("receiveBufLen = %d", receiveBufLen);
	if (SocketRcvData(fd, receiveBuf + 4, receiveBufLen, 10*1000) != receiveBufLen)
	{
		LOGE("receiveBuf ERR");
		DisplayValidateInfo("receive data error");
		SocketDisconnect(fd);
		return -1;
	}
	SocketDisconnect(fd);

	InitTagList(&tmpTagList);
	InitTag3ByteList(&tmpTag3ByteList);
	
	if (ParseTransStartResponse(receiveBuf , receiveBufLen + 4, &tmpTagList, &tmpTag3ByteList) != STATUS_OK)
	{
		FreeTagList(&tmpTagList);
		LOGE("ParseTransStartResponse ERR");
		DisplayValidateInfo("receive data error");
		return -1;
	}
	
    PrintOutTagList(&tmpTagList, "tmpTagList list");
    PrintOutTag3ByteList(&tmpTag3ByteList, "tmpTag3ByteList list");

    if (GetTag3ByteValueAndSize(&tmpTag3ByteList, TAG_TRANSACTION_COMMAND_FLAG_BCTC, tmpBuf, &tmpLen) != 0)
	{
		FreeTagList(&tmpTagList);
        REPORT_ERR_LINE();
		LOGE("GetTag3ByteValueAndSize ERR");
		DisplayValidateInfo("receive data error");
		return -1;
	}else{
			if (tmpBuf[0] == 0xff)
			{
				//No need transaction again.
				FreeTagList(&tmpTagList);
				DisplayValidateInfo("Now exit");
				return 0xff;
			}else if (tmpBuf[0] != 0x00)
			{
#ifndef CY20_EMVL2_KERNEL
				sleep(tmpBuf[0]);
#else
				mdelay((tmpBuf[0])*1000);
#endif
				FreeTagList(&tmpTagList);
				return tmpBuf[0];
			}
	}
		
	if (GetTag3ByteValueAndSize(&tmpTag3ByteList, TAG_DOWNLOAD_COMMAND_FLAG_BCTC, tmpBuf, &tmpLen) == 0)
	{
		//hexdumpEx("List TAG_DOWNLOAD_COMMAND_FLAG_BCTC", tmpBuf, tmpLen);
		if (tmpBuf[0]&0x10)
		{
			//ClearTransRecordRaw();
			//ClearBatchTransRecordRaw();
			ClearTransRecord();
		}
		//if (tmpBuf[0]&0x04)
		//{
		//	DownloadExcpFile();
		//}
		if (tmpBuf[0]&0x02)
		{
			DownloadCA();
		}
		if (tmpBuf[0]&0x01)
		{
			DownloadAIDPara();
		}
	}

	if (TagIsExisted(&tmpTagList, TAG_AMOUNT) == TRUE)
	{
		amount = getAmountByTag(&tmpTagList, TAG_AMOUNT);
		sprintf(autoAmount, "%d.%02d", amount/100, amount%100);
		strcpy(gAutoAmount, autoAmount);
		giAutoAmount = amount;
		LOGD("autoAmount = %s, amount = %d\n", autoAmount, amount);
	}
	if (TagIsExisted(&tmpTagList, TAG_AMOUNT_OTHER) == TRUE)
	{
		amountOther = getAmountByTag(&tmpTagList, TAG_AMOUNT_OTHER);
		sprintf(autoAmountOther, "%d.%02d", amountOther/100, amountOther%100);
		strcpy(gAutoAmountOther, autoAmountOther);
		giAutoAmountOther = amountOther;
		LOGD("autoAmountOther = %s, amountOther = %d \n", autoAmountOther, amountOther);
	}
	if (TagIsExisted(&tmpTagList, TAG_TRANSACTION_TYPE) == TRUE)
	{
	
		memcpy(transactionType, GetTagValue(&tmpTagList, TAG_TRANSACTION_TYPE), 1);
		//gTransactionType = transactionType[0];
		gAutoTransType = transactionType[0];
		LOGD("gTransactionType = %d", transactionType[0]);
	}


	FreeTagList(&tmpTagList);
	return 0;
}



