#ifndef _EMVAPP_UI_H_
#define _EMVAPP_UI_H_

#include "OsTypes.h"
#include "evntMask.h"
/************************************************
	#3: Special function of User defining
************************************************/ 
#define	DISPLAY_LEFT_ALIGN		((tti_uint8)101)
#define	DISPLAY_RIGHT_ALIGN		((tti_uint8)102)
#define	DISPLAY_CENTER_ALIGN	((tti_uint8)103)
#define	DISPLAY_IN_NORMAL		((tti_uint8)0)
#define	DISPLAY_IN_REVERSE		((tti_uint8)1)

#define KEY_YES KEYBORADEVM_MASK_KEY_B
#define KEY_NO	KEYBORADEVM_MASK_KEY_C	

#define LCD_ICON_NFCLED_BLUE	0
#define LCD_ICON_NFCLED_YELLOW	1
#define LCD_ICON_NFCLED_GREEN	2
#define LCD_ICON_NFCLED_RED		3

#define NFCLED_DISPLAY_STATUS_ON	0
#define NFCLED_DISPLAY_STATUS_OFF	1
#define NFCLED_DISPLAY_STATUS_NONE	2

#define LCD_ICON_NFCLED_BLUE_X_START   60
#define LCD_ICON_NFCLED_YELLOW_X_START (LCD_ICON_NFCLED_BLUE_X_START + 50)
#define LCD_ICON_NFCLED_GREEN_X_START  (LCD_ICON_NFCLED_YELLOW_X_START + 50)
#define LCD_ICON_NFCLED_RED_X_START    (LCD_ICON_NFCLED_GREEN_X_START + 50)

#ifdef WIN32
#define TCONST
#define TDADA

#else
#define TCONST	const
#define TDADA
#endif

#define ONE_SEC_TIMEOUT	1000 // one second 
void DisplayStringInFormat(tti_tchar *pLine, tti_uint8 uiDisplayLine, 
	tti_uint8 uiDispStartCol, tti_uint8 uiDispEndCol, tti_uint8 uiFormat, tti_uint8 uiDispMode);
	
//void WaitAKeyOrMagicCardInsertDuringTime(TEvntMask *EvntL1, TEvntMask *EvntL2, tti_uint16 uiTimeOut);
TEvntMask WaitAKeyDuringTime(TEvntMask WaitEvnt, tti_uint16 uiTimeOut);
//void TtiOsClearLines(tti_uint8 uiStartLine, tti_uint8 uiEndLine);

void ClearScreen(void);
void DisplayValidateInfo(tti_tchar *pInfo);
void DisplayInfoRight( tti_tchar *pInfo, tti_uint8 line);
void DisplayInfoCenter( tti_tchar *pInfo, tti_uint8 line);
void DisplayInfo(tti_tchar *pInfo, tti_uint8 line);
void WaitForValidate(void);
void Beep(void);
int IsUserCancel(void);
int IsUserSelectOK(void);
void DisplayIcon(int row,int col, int IconID);
int GetAmount(int line, char *buf, int len);
int DisplayNFCLedIcon(int NFCLedIndex, int displayStatus);
int DisplayAllNFCLedIconOff(void);
int DisplayAllNFCLedIconNone(void);
void DisplayNFCLed_UnitTest(void);


#define INPUT_STATUS_OK 	KEYBORADEVM_MASK_KEY_OK
#define INPUT_STATUS_CANCEL	KEYBORADEVM_MASK_KEY_CANCEL

int GetInput(int line, char *buf, int len);
int GetInputNumStr(int line, char *buf, int len);
int IsUserSelectFirstBtn(void);


int DisplayYesNoChoiceInfo(char *info);


int CJAVA_EnterPIN(char *title, char *enterPin, unsigned char autoClose, char * autoPin);

int CJAVA_ShowAidAndWaitSelect(char *title, unsigned char autoClose, int autoChoiceIndex);

int CJAVA_ShowYesNoAndWaitSelect(char *title, unsigned char autoClose, int autoChoiceIndex);

int CJAVA_ShowApproveDeclineAndWaitSelect(char *title, unsigned char autoClose, int autoChoiceIndex);


void CJAVA_ShowLogTextAndWaitConfirm(char *title, char *data, unsigned char autoClose);

void CJAVA_ShowProcess(char *data);

void DisplayProcess(tti_tchar *pInfo);

void SetLogTextAutoClose(tti_bool logTextAutoClose);

#endif
