#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "EmvAppUi.h"
#include "Define.h"
#include "Util.h"
#include "ReqDataItem.h"
#include "ReqBuild.h"
#include "Online.h"
#include "RespDataItem.h"
#include "MultiLang.h"
#include "emvl2/tag.h"
#include "emvDebug.h"

void PrintRequestItemGroup(const uint16 ar[], uint16 count)
{
	uint16 i;
	uint16 index;
	
	const DataItemAttr_t *pAttr;

	for(i=0; i<count; i++)
	{
		index =ar[i];
		
		if(IsRequestItemExisted(index))
		{
			pAttr =GetRequestItemAttr(index);
			PrintItemName((char *)(pAttr->name));
			PrintCharArray(g_ReqDataList[index].data.ptr, g_ReqDataList[index].data.len);
		}
	}
}

int gEmvIsOnlineSuccess;
int32 BuildReqData(const uint16 ReqDataItemIndexAr[], uint16 IndexCount, 
					 byte buf[], uint16 *size)
{
	uint16 i;
	uint16 index;
	const vec_t *pData;
	const DataItemAttr_t *pAttr;
	int CurLength = 0;

#ifndef CY20_EMVL2_KERNEL
	assert(buf != NULL);
	assert(*size >= 255);
	assert(IndexCount < REQ_ITEM_COUNT);
#endif

	//PrintRequestItemGroup(ReqDataItemIndexAr, IndexCount);

	for(i=0; i<IndexCount; i++)
	{	
		index = ReqDataItemIndexAr[i];
		pAttr=GetRequestItemAttr(index);
		
		if((index==REQ_PK_HASH_ALGOR) && !IsRequestItemExisted(REQ_PK_HASH_ALGOR))
		{
			*size = CurLength - 1;

			return STATUS_OK;
		}

		if(IsRequestItemExisted(index))
		{
			pData = GetRequestItemValue(index);
			memcpy(buf + CurLength,pData->ptr, pData->len);
			CurLength += pData->len;
		}
		else
		{
			if(pAttr->exist_type == ETYPE_MUST)
			{
				LOGD("*******%s Must Exist Item do NOT have Value.", pAttr->name);
				return STATUS_FAIL;
			}
		}
		
		if(((pAttr->exist_type == ETYPE_OPT) || (pAttr->len_type == VAR_LEN))
				&& (i != IndexCount-1))
		{
			buf[CurLength]=ITEM_SEPARATOR;
			CurLength += 1;
		}
	}
	
	*size = CurLength;

	return STATUS_OK;
}

int32 BuildLoadConfigReqData(char buf[], uint16 *size)
{	
	uint16 IndexCount;
	uint16 ItemIndex[]=
	{
		REQ_CMD_LOAD_CONFIG,
		REQ_IFD_SERIAL_NUMBER,
		REQ_TERM_CTRY_CODE,		
		REQ_TERM_TYPE,
		REQ_TERM_CAP,
		REQ_TERM_ADD_CAP,		
		REQ_IS_SUPPORT_PSE,
		REQ_AID_LIST
	};
	
#ifndef CY20_EMVL2_KERNEL
	assert(buf != NULL);
	assert(*size >= 255);
#endif

	IndexCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	return BuildReqData(ItemIndex,IndexCount, (byte *)buf, size);
}

int32 BuildGetAidListReqData(char buf[], uint16 *size)
{	
	uint16 IndexCount;
	uint16 ItemIndex[]=
	{
		REQ_CMD_GET_AID_LIST,
		REQ_TIMEOUT
	};
	
#ifndef CY20_EMVL2_KERNEL
	assert(buf != NULL);
	assert(*size >= 255);
#endif

	IndexCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	return BuildReqData(ItemIndex, IndexCount, (byte *)buf, size);
}

#if 0
int32 BuildInitTransReqData(char buf[], uint16 *size)
{
	uint16 IndexCount;
	uint16 ItemIndex[]=
	{
		REQ_CMD_INIT_TRANS,
		REQ_AMOUNT_AUTHOR,
		REQ_AMOUNT_OTHER,
		REQ_TRANS_DATE,
		REQ_TRANS_TIME,
		REQ_TRANS_TYPE,
		REQ_POS_ENTRY_MODE_CODE,
		REQ_TRANS_SEQ_NUMBER,
		REQ_ACCOUNT_TYPE,
		REQ_FLOOR_LIMIT,
		REQ_TAR_PERCENT,
		REQ_MAX_TAR_PERCENT,
		REQ_THRESHOLD_AMOUNT,
		REQ_MERCHANT_CAT_CODE, 
		REQ_MERCHANT_ID,
		REQ_TERM_ID,
		REQ_TRANS_CUR_CODE,
		REQ_TRANS_CUR_EXP,
		REQ_TRANS_REFERENCE_CUR_CODE,
		REQ_TRANS_REFERENCE_CUR_EXP,
		REQ_IS_SUPPORT_VLP,
		REQ_BYPASS_MODE,
		REQ_TERM_RISK_MANAGE_DATA,
		REQ_MERCHANT_NAME_LOCATION,
		REQ_SELECTED_AID,
		REQ_ACQUIRER_ID,
		REQ_DEFAULT_TAC,
		REQ_DENIAL_TAC,
		REQ_ONLINE_TAC,
		REQ_TDOL,
		REQ_DDOL
	};

	assert(buf != NULL);
	assert(*size >= 255);

	IndexCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	return BuildReqData(ItemIndex, IndexCount, buf, size);
}
#else
int32 BuildInitTransReqData(char buf[], uint16 *size)
{
	uint16 IndexCount;
	uint16 ItemIndex[]=
	{
		REQ_CMD_INIT_TRANS,
		REQ_AMOUNT_AUTHOR,
		REQ_AMOUNT_OTHER,
		REQ_TRANS_DATE,
		REQ_TRANS_TIME,
		REQ_TRANS_TYPE,
		REQ_POS_ENTRY_MODE_CODE,
		REQ_TRANS_SEQ_NUMBER,
		REQ_ACCOUNT_TYPE,
	};

#ifndef CY20_EMVL2_KERNEL
	assert(buf != NULL);
	assert(*size >= 255);
#endif

	IndexCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	return BuildReqData(ItemIndex, IndexCount, (byte *)buf, size);
}

#endif
int32 BuildEmvTransReqData(char buf[], uint16 *size)
{
	uint16 IndexCount;
	uint16 ItemIndex[]=
	{
		REQ_CMD_EMV_TRANS,
		REQ_CARD_HOT,
		REQ_FORCE_ONLINE,	
		REQ_TRANS_LOG_AMOUNT,
		REQ_PIN_PAD_SECRET_KEYS,	
		REQ_PIN_PROMPT_OFFLINE_PLAIN,
		REQ_PIN_PROMPT_OFFLINE_ENCRYPT,
		REQ_PIN_PROMPT_ONLINE_ENCRYPT,
		REQ_PIN_LAST_PIN_TRY,
		REQ_PK_HASH_ALGOR,
		REQ_PK_ALGOR,
		REQ_PK_EXPONENT,
		REQ_PK_MODULUS,
		REQ_PK_CHECKSUM,
		REQ_PK_ACTIVE_DATE,
		REQ_PK_EXPIRE_DATE
	};
	int32 ret;

#ifndef CY20_EMVL2_KERNEL
	assert(buf != NULL);
	assert(*size >= 255);
#endif

	IndexCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	dbg("Before BuildReqData() in BuildEmvTransReqData()\n");
	ret = BuildReqData(ItemIndex, IndexCount, (byte *)buf, size);
	dbg("BuildReqData return = %d\n", ret);
	return ret;
}

/*
int32 CommWithHost()
{
	byte SendBuf[MAX_COMM_HOST_SEND_BUF_SIZE];
	byte RecvBuf[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 SendBufLen = sizeof(SendBuf);
	uint16 RecvBufLen = sizeof(RecvBuf);
	
	if(GetAuthRequest(SendBuf, &SendBufLen) != STATUS_OK)
	{
		dbg("Get Auth Request Data Fail.");
		return STATUS_FAIL;
	}
	
	if(SendAndReceive(SendBuf, SendBufLen, RecvBuf, &RecvBufLen) != STATUS_OK)
	{
		dbg("Send And Recvive Fail.");
		return STATUS_FAIL;
	}
	
	if(ParseHostData(RecvBuf, RecvBufLen) != STATUS_OK)
	{
		dbg("Parse Host Data Fail.");
		
		return STATUS_FAIL;
	}

	if(!IsRequestItemExisted(REQ_ARC))
	{
		dbg("Transaction Response Code Not Exsit.");
		
		return STATUS_FAIL;
	}

	return STATUS_OK;
}
*/

extern int iAppSendAndRecive(const byte * SendBuf,uint16 SendBufLen,
						     byte * RecvBuf, uint16 *pRecvBufLen);


int32 CommWithHost()
{
	byte SendBuf[MAX_COMM_HOST_SEND_BUF_SIZE];
	byte RecvBuf[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 SendBufLen = sizeof(SendBuf);
	uint16 RecvBufLen = sizeof(RecvBuf);

	if(GetAuthRequest(SendBuf, &SendBufLen) != STATUS_OK)
	{
        LOGE("Get Auth Request Data Fail.");
		return STATUS_FAIL;
	}
	LOGD("GetAuthRequest SendBuf ");
	hexdump(SendBuf, SendBufLen);

#ifndef VPOS_APP
	if(SendAndReceive(SendBuf, SendBufLen, RecvBuf, &RecvBufLen) != STATUS_OK)
#else	
	if(iAppSendAndRecive(SendBuf+4, SendBufLen-4, RecvBuf, &RecvBufLen))
#endif	
	{
        LOGE("Send And Recvive Fail.");
		return STATUS_FAIL;
	}

	LOGD("ParseHostData RecvBuf");
	hexdump(RecvBuf, RecvBufLen);

	if(ParseHostData(RecvBuf, RecvBufLen) != STATUS_OK)
	{
        LOGE("Parse Host Data Fail.");

		return STATUS_FAIL;
	}

	if(!IsRequestItemExisted(REQ_ARC))
	{
        LOGE("Transaction Response Code Not Exsit.");

		return STATUS_FAIL;
	}

	return STATUS_OK;
}


int32 TransResultCommWithHost(void)
{
	byte SendBuf[MAX_COMM_HOST_SEND_BUF_SIZE];
	byte RecvBuf[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 SendBufLen = sizeof(SendBuf);
	uint16 RecvBufLen = sizeof(RecvBuf);

	DisplayProcess("交易结果报文处理中");

	if(GetTransResultRequest(SendBuf, &SendBufLen) != STATUS_OK)
	{
        LOGE("GetTransactionResultBCTC Fail.");
		DisplayValidateInfo("通讯失败");
		return STATUS_FAIL;
	}
	LOGD("GetTransactionResultBCTC SendBuf ");
	hexdump(SendBuf, SendBufLen);

	if(SendAndReceive(SendBuf, SendBufLen, RecvBuf, &RecvBufLen) != STATUS_OK)
	{
        LOGE("Send And Recvive Fail.");
		DisplayValidateInfo("通讯失败");
		return STATUS_FAIL;
	}

	if(ParseTransResultResponse(RecvBuf, RecvBufLen) != STATUS_OK)
	{
        LOGE("ParseTransResultResponse Fail.");
		DisplayValidateInfo("通讯失败");

		return STATUS_FAIL;
	}

	//DisplayValidateInfo("交易结果报文发送成功");
	return STATUS_OK;
}



int32 BuildEmvCompleteReqData(char buf[], uint16 *size)
{
	int32 status;
	vec_t *pData;
	uint16 IndexCount;
	uint16 ItemIndex[]=
	{
		REQ_CMD_TRANS_COM,
		REQ_RESP_MODE,
		REQ_ARC,
		REQ_ISSUER_AUTH_DATA,
		REQ_SCRIPT
	};

#ifndef CY20_EMVL2_KERNEL
	assert(buf != NULL);
	assert(*size >= 255);
#endif

	if(CommWithHost() != STATUS_OK)
	{
		pData = GetRequestItemValue(REQ_CMD_TRANS_COM);
		memcpy(buf, pData->ptr, pData->len);

		buf[pData->len] = REQ_RESP_MODE_HOST_COMM_FAIL + '0';
		*size = pData->len + 1;

		return STATUS_OK;
	}
	gEmvIsOnlineSuccess =TRUE;
	
	if(!IsRequestItemExisted(REQ_ARC))
	{
		SetRequestItemInt(REQ_RESP_MODE, REQ_RESP_MODE_HOST_DECLINED);
	}
	else if(strcmp((char *)(GetRequestItemValue(REQ_ARC)->ptr), "00") == 0)
	{
		SetRequestItemInt(REQ_RESP_MODE,REQ_RESP_MODE_HOST_APPROVED);
	}
	else if(strcmp((char *)(GetRequestItemValue(REQ_ARC)->ptr), "01") == 0
		|| strcmp((char *)(GetRequestItemValue(REQ_ARC)->ptr), "02") == 0)
	{
#ifndef VPOS_APP        
		//Added by Siken 2011.09.23
		char szMessage[30];
		//End
		
		//DisplayValidateInfo("Call Your Bank");	
		DisplayValidateInfo("请联系发卡行");
		//Added by Siken 2011.09.23
		sprintf(szMessage, "PAN:%s", GetResponseItemValue(RESP_PAN)->ptr);
		DisplayValidateInfo(szMessage);
		sprintf(szMessage, "Amt:%s", GetRequestItemValue(REQ_AMOUNT_AUTHOR)->ptr);
		DisplayValidateInfo(szMessage);
		sprintf(szMessage, "TransDate:%s", GetRequestItemValue(REQ_TRANS_DATE)->ptr);
		DisplayValidateInfo(szMessage);
		sprintf(szMessage, "TransTime:%s", GetRequestItemValue(REQ_TRANS_TIME)->ptr);
		DisplayValidateInfo(szMessage);
		//End

		//Removed by Siken 2011.09.23
		//DisplayValidateInfo(GetResponseItemValue(RESP_PAN)->ptr);
		//End
		
		ClearScreen();
		//Jason Removed on 2012.04.13
		/*
		MultiLangDispRight(MLSID_APPROVED, 1);
		MultiLangDispRight(MLSID_DECLINED, 3);
		*/
		//End
		
		//Jason Added on 2012.04.13
		//MultiLangDispRight(MLSID_APPROVED_1, 1);
		//MultiLangDispRight(MLSID_DECLINED_2, 3);
		//End

		//Jason modified on 2020/01/14
		//if(IsUserSelectFirstBtn())
		if (DisplayYesNoChoiceInfo("接受或者拒绝交易") == 1)
		//End
		{
			SetRequestItemInt(REQ_RESP_MODE, REQ_RESP_MODE_HOST_APPROVED);
		}
		else
		{
			SetRequestItemInt(REQ_RESP_MODE, REQ_RESP_MODE_HOST_DECLINED);
		}
#else
        SetRequestItemInt(REQ_RESP_MODE, REQ_RESP_MODE_HOST_DECLINED);
#endif
	}
	//Jason and bob edit 20110905
	/*
	else
	{
		dbg("ARC is not invalid.");
		SetRequestItemInt(REQ_RESP_MODE, REQ_RESP_MODE_HOST_DECLINED);
	}
	*/
	else if(strcmp((char *)(GetRequestItemValue(REQ_ARC)->ptr), "05") == 0
		|| strcmp((char *)(GetRequestItemValue(REQ_ARC)->ptr), "51") == 0)
	{
		LOGD("ARC is 05 or 51");
		SetRequestItemInt(REQ_RESP_MODE, REQ_RESP_MODE_HOST_DECLINED);
	}else
	{
		LOGD("ARC is not invalid. Let kernel decide TC or AAC");
		SetRequestItemInt(REQ_RESP_MODE, REQ_RESP_MODE_INVALID_ARC);
	}
	
	IndexCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	status =BuildReqData(ItemIndex, IndexCount, (byte *)buf, size);

	if(status != STATUS_OK)
	{
		return status;
	}

	if(!IsRequestItemExisted(REQ_SCRIPT))
	{
		*size -=1;
	}

	return STATUS_OK;
}

int32 BuildEndTransReqData(char buf[], uint16 *size)
{
	uint16 IndexCount;
	uint16 ItemIndex[]=
	{
		REQ_CMD_END_TRANS
	};

#ifndef CY20_EMVL2_KERNEL
	assert(buf != NULL);
	assert(*size >= 255);
#endif

	IndexCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	return BuildReqData(ItemIndex, IndexCount, (byte *)buf, size);
}


