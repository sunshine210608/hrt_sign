#include <string.h>
#include <stdlib.h>
#include <stdio.h>
//#include <netinet/in.h>
//#include <sys/socket.h>
//#include <arpa/inet.h>
//#include <fcntl.h>

//#include "SDKApi.h"
#include "emvDebug.h"
#include "sys_littlefs.h"
#include "Define.h"
#include "ReqDataItem.h"
#include "RespDataItem.h"
#include "UtilPrint.h"
#include "Util.h"
#include "EmvAppUi.h"
#include "Receipt.h"
#include "ParamDown.h"
#include "DataTrans.h"
#include "Transaction.h"
#include "emvl2/defines.h"
#include "online.h"
#include "utils.h"

#ifdef VPOS_APP
#include "global.h"
#include "VposFace.h"
#include "pub.h"
#if 0//def APP_LKL
#include "AppGlobal_lkl.h"
#include "MemMana_lkl.h"
#endif
#if 0//def APP_SDJ
#include "AppGlobal_sdj.h"
#include "MemMana_sdj.h"
#endif
#if 1//def APP_CJT
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#endif

#endif

extern void vSetDispRetInfo(int flag);
extern int iUploadComfirmSaleTrade(void);

void ParseDetailLine(const char *line, char *value)
{
	char header[1024];
	if(sscanf(line, "%[^;];%s", header, value) != 2)
	{
		value[0] ='\0';
	}
}

char *GetValidBatchRecordFileName()
{
#ifndef CY20_EMVL2_KERNEL
	int i;
	FILE *fp;
	static char filefullpath[1024];

	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		sprintf(filefullpath, FMT_TRANS_BATCH_RECORD_FILE_NAME, DIR_BATCH_RECORD, i);

		fp = fopen(filefullpath, "r");
		if(fp == NULL)
		{			
			LOGD("Batch Trans Record File Name: %s\n", filefullpath);
			return filefullpath;
		}

		fclose(fp);
	}

	DisplayValidateInfo("Batch Trans Record Is Full");
	return NULL;
#else
	int i;
	int ret;
	lfs_file_t lfs_file;
	static char filefullpath[256];

	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		sprintf(filefullpath, FMT_TRANS_BATCH_RECORD_FILE_NAME, DIR_BATCH_RECORD, i);

		ret = sys_lfs_file_open(&lfs_file, filefullpath, LFS_O_RDONLY);
		if(ret != 0)
		{			
			LOGD("Trans Record File Name: %s\n", filefullpath);
			return filefullpath;
		}

		sys_lfs_file_close(&lfs_file);
	}

	DisplayValidateInfo("Batch Trans Record Is Full");
	return NULL;
#endif
}

char *GetValidQpbocBatchRecordFileName()
{
#ifndef CY20_EMVL2_KERNEL
	int i;
	FILE *fp;
	static char filefullpath[1024];

	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		sprintf(filefullpath, FMT_TRANS_QPBOC_BATCH_RECORD_FILE_NAME, DIR_QPBOC_BATCH_RECORD, i);

		fp =fopen(filefullpath, "r");
		if(fp == NULL)
		{			
			LOGD("Trans Record File Name: %s\n", filefullpath);
			return filefullpath;
		}

		fclose(fp);
	}

	DisplayValidateInfo("Trans Record Is Full");
	return NULL;
#else
	int i;
	int ret;
	lfs_file_t lfs_file;
	static char filefullpath[256];

	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		sprintf(filefullpath, FMT_TRANS_QPBOC_BATCH_RECORD_FILE_NAME, DIR_QPBOC_BATCH_RECORD, i);

		ret = sys_lfs_file_open(&lfs_file, filefullpath, LFS_O_RDONLY);
		if(ret != 0)
		{			
			LOGD("Trans Record File Name: %s\n", filefullpath);
			return filefullpath;
		}

		sys_lfs_file_close(&lfs_file);
	}

	DisplayValidateInfo("QPBOC Batch Trans Record Is Full");
	return NULL;
#endif
}


char *GetValidRecordFileName()
{
#ifndef CY20_EMVL2_KERNEL
	int i;
	FILE *fp;
	static char filefullpath[1024];

	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		sprintf(filefullpath, FMT_TRANS_RECORD_FILE_NAME, DIR_TRANS_RECORD, i);

		fp =fopen(filefullpath, "r");
		if(fp == NULL)
		{			
			dbg("Trans Record File Name: %s\n", filefullpath);
			return filefullpath;
		}

		fclose(fp);
	}

	DisplayValidateInfo("Trans Record Is Full");
	return NULL;
#else
	int i;
	int ret;
	lfs_file_t lfs_file;
	static char filefullpath[256];

	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		sprintf(filefullpath, FMT_TRANS_RECORD_FILE_NAME, DIR_TRANS_RECORD, i);

		ret = sys_lfs_file_open(&lfs_file, filefullpath, LFS_O_RDONLY);
		if(ret != 0)
		{			
			LOGD("Trans Record File Name: %s\n", filefullpath);
			return filefullpath;
		}

		sys_lfs_file_close(&lfs_file);
	}

	DisplayValidateInfo("Trans Record Is Full");
	return NULL;
#endif
}

int PrintSign()
{
	vec_t *data;

	if(!IsResponseItemExisted(RESP_SIGNATURE))
	{
		return STATUS_OK;
	}
	
	data = GetResponseItemValue(RESP_SIGNATURE);

	if(strcmp((char *)(data->ptr), "1")==0)
	{
		//TtiOsPrntBuffer("\n\n");
		//TtiOsPrntBuffer("Sign:_______________________");
		NJRCBase_Print_FillBuffer("\n\n");
		NJRCBase_Print_FillBuffer("Sign:_______________________");
	}

	return STATUS_OK;
}

int FmtValue(const char *src, char desc[])
{
	int i=0;
	int j=0;
	int len = strlen(src);

	if(len == 0)
	{
		desc[0]='\0';
		return STATUS_OK;
	}
	
	if(len%2==0)
	{
		for(i=0,j=0; i <len; i+=2, j+=3)
		{
			desc[j]   = src[i];
			desc[j+1] = src[i+1];
			desc[j+2] = ' ';
		}

		desc[j]='\0';
	}
	else
	{
		for(i=0,j=0; i <len-1; i+=2, j+=3)
		{
			desc[j]   = src[i];
			desc[j+1] = src[i+1];
			desc[j+2] = ' ';
		}

		desc[j]   = src[i];
		desc[j+1]='\0';
	}

	return STATUS_OK;
}



/**	if change the item order, please update the 
	SetReqItemPANTransLogAmount function's Amount index number;
**/

DetailItem_t g_QpbocDetail[]=
{	
	{SRC_RESP, 	RESP_AID, 		"AID",	0x4F,	"Application Identifier"},
	{SRC_REQ,	REQ_AMOUNT_AUTHOR,	"AmountAuth", 0x9F02, "Amount Authorised(Numeric)"},
	{SRC_REQ,	REQ_AMOUNT_OTHER, 	"AmountOth",	0x9F03,	"Amount,Other(Numeric)"},
	{SRC_RESP,	RESP_PAN,		"PAN",	0x5A,	"Application Primary Account Number"},
	{SRC_RESP,	RESP_PAN_SEQ_NO, "PANSeqNo",	0x5F34, "PAN Sequence Number"},

	//{SRC_RESP, 	RESP_AIP,		"AIP",	0x82,	"Application Interchange Profile"},		
	//{SRC_RESP,  RESP_CID,		"CID",	0x9F27,	"Cryptogram Information Data"},	
	//{SRC_RESP,	RESP_TSI,		"TSI",	0x9B,	"Transaction Status Information"}, 	
	//{SRC_RESP,  RESP_AUC,		"AUC",	0x9F07,	"Application Usage Control"},
	
	{SRC_RESP,	RESP_ATC,		"ATC",	0x9F36, "Application Transaction Counter"},		
	{SRC_REQ,	REQ_TRANS_SEQ_NUMBER, "TransSeqCounter", 0x9F41, "Transaction Sequence Counter"},
	//{SRC_REQ,	REQ_IFD_SERIAL_NUMBER, "IFDSNum",	0x9F1E, 	"Interface Device(IFD)Serial Number"},		
	//{SRC_RESP,	RESP_ISSUER_APP_DATA,		"IAD",	0x9F10,	"Issuer Application Data"},
	//{SRC_REQ,  	REQ_TERM_CAP, "TermCap",	0x9F33,		"Terminal Capabilities"},		
	//{SRC_REQ,	REQ_TRANS_TYPE, "TransType",	0x9C,	"Transaction Type"},
	//{SRC_RESP,	RESP_TVR,		"TVR",	0x95,	"Terminal Verification Results"},
	{SRC_RESP,	RESP_AC,		"AC",	0x9F26,	"Application Cryptogram"},
	//{SRC_RESP,	RESP_SERVICE_CODE, "ServiceCode", 0x5F30,	"Service Code"},
	//dddddddddddddddddddd
	//{SRC_RESP,	RESP_RAND_ONLINE_TRANS_SELECT_NUM, "TransSelRandNum",	0x11, ""},
	//{SRC_RESP,  RESP_UNPREDICTABLE_NUM, "UnpredictNum",	0x9F37, "Unpredictable Number"},
	//{SRC_RESP,	RESP_TRACK2,	"Track2",	0x57,	"Track2 Equivalent Data"},
	//{SRC_REQ,	REQ_ACQUIRER_ID, 	"AcquirerID",	0x9F01,	"Acquirer Identifier"},	
	//{SRC_RESP,  RESP_CARD_EFF_DATE, "AppEffecDate", 0x5F25,	"Application Effective Date"},
	//{SRC_RESP,	RESP_CARD_EXP_DATE, "AppExpDate",	0x5F24,	"Application Expiration Date"},
	//{SRC_REQ, 	REQ_MERCHANT_CAT_CODE, "MerCatCode",0x9F15,	"Merchant Category Code"},
	//{SRC_REQ, 	REQ_MERCHANT_ID, 	"MerchantID",	0x9F16,	"Merchant Identifier"},
	{SRC_RESP, 	RESP_APP_LABEL, 	"App Label",	0x50,	"Application Label"},
	{SRC_REQ, 	REQ_POS_ENTRY_MODE_CODE,	"POSEntryMode" ,0x9F39,	"Point-of-Service Entry Mode"},	
	{SRC_REQ, 	REQ_TERM_CTRY_CODE, "TermCtryCode", 0x9F1A,	"Terminal Country Code"},
	{SRC_REQ, 	REQ_MERCHANT_NAME_LOCATION, "Mechant Name", 0x9F4E,	"Merchant Name"},
	//{SRC_REQ,  	REQ_TERM_ID, 		"TermID", 0x9F1C,	"Terminal Identification"},
	//Jason delete on 20130524
	//{SRC_REQ,	REQ_AMOUNT_AUTHOR, 	"TransAmount", 0x9F02,	"Amount,Authorised"},
	//End
	//{SRC_REQ, 	REQ_TRANS_CUR_CODE, "TransCurCode",	0x5F2A,	"Transaction Currency Code"},
	{SRC_REQ,	REQ_TRANS_DATE, 	"Date",	0x9A,	"Transaction Date"},
	{SRC_REQ,	REQ_TRANS_TIME, 	"Time", 0x9F21,	"Transaction Time"},

	//dddddddddddddddddddddddddd
	//{SRC_REQ,	REQ_ARC,		"AuthRespCode", 0x8A,"Authorisation Response Code"},
	//{SRC_RESP,	RESP_PIN_BLOCK, "PinBlock",	0x00, "Encrypted Pin Block"},
	//{SRC_RESP, RESP_EC_ISSUER_AUTH_CODER, "ECCode", 0x9F74, "EC Issuer Code"},
	//Issuer Script Results
	{SRC_RESP, RESP_EC_ISSUER_AUTH_CODER, "Balance", 0x9F5D, "QPBOC OFFLINE Balance"},
	
	
	
};


/**	if change the item order, please update the 
	SetReqItemPANTransLogAmount function's Amount index number;
**/

DetailItem_t g_Detail[]=
{	
	{SRC_RESP, 	RESP_AID, 		"AID",	0x4F,	"Application Identifier"},
	{SRC_REQ,	REQ_AMOUNT_AUTHOR,	"AmountAuth", 0x9F02, "Amount Authorised(Numeric)"},
	{SRC_REQ,	REQ_AMOUNT_OTHER, 	"AmountOth",	0x9F03,	"Amount,Other(Numeric)"},
	{SRC_RESP,	RESP_PAN,		"PAN",	0x5A,	"Application Primary Account Number"},
	{SRC_RESP,	RESP_PAN_SEQ_NO, "PANSeqNo",	0x5F34, "PAN Sequence Number"},

	{SRC_RESP, 	RESP_AIP,		"AIP",	0x82,	"Application Interchange Profile"},		
	{SRC_RESP,  RESP_CID,		"CID",	0x9F27,	"Cryptogram Information Data"},	
	{SRC_RESP,	RESP_TSI,		"TSI",	0x9B,	"Transaction Status Information"}, 	
	{SRC_RESP,  RESP_AUC,		"AUC",	0x9F07,	"Application Usage Control"},
	
	{SRC_RESP,	RESP_ATC,		"ATC",	0x9F36, "Application Transaction Counter"},		
	{SRC_REQ,	REQ_TRANS_SEQ_NUMBER, "TransSeqCounter", 0x9F41, "Transaction Sequence Counter"},
	//{SRC_REQ,	REQ_IFD_SERIAL_NUMBER, "IFDSNum",	0x9F1E, 	"Interface Device(IFD)Serial Number"},		
	//{SRC_RESP,	RESP_ISSUER_APP_DATA,		"IAD",	0x9F10,	"Issuer Application Data"},
	//{SRC_REQ,  	REQ_TERM_CAP, "TermCap",	0x9F33,		"Terminal Capabilities"},		
	{SRC_REQ,	REQ_TRANS_TYPE, "TransType",	0x9C,	"Transaction Type"},
	{SRC_RESP,	RESP_TVR,		"TVR",	0x95,	"Terminal Verification Results"},
	{SRC_RESP,	RESP_AC,		"AC",	0x9F26,	"Application Cryptogram"},
	//{SRC_RESP,	RESP_SERVICE_CODE, "ServiceCode", 0x5F30,	"Service Code"},
	//dddddddddddddddddddd
	//{SRC_RESP,	RESP_RAND_ONLINE_TRANS_SELECT_NUM, "TransSelRandNum",	0x11, ""},
	{SRC_RESP,  RESP_UNPREDICTABLE_NUM, "UnpredictNum",	0x9F37, "Unpredictable Number"},
	{SRC_RESP,	RESP_TRACK2,	"Track2",	0x57,	"Track2 Equivalent Data"},
	//{SRC_REQ,	REQ_ACQUIRER_ID, 	"AcquirerID",	0x9F01,	"Acquirer Identifier"},	
	//{SRC_RESP,  RESP_CARD_EFF_DATE, "AppEffecDate", 0x5F25,	"Application Effective Date"},
	//{SRC_RESP,	RESP_CARD_EXP_DATE, "AppExpDate",	0x5F24,	"Application Expiration Date"},
	//{SRC_REQ, 	REQ_MERCHANT_CAT_CODE, "MerCatCode",0x9F15,	"Merchant Category Code"},
	//{SRC_REQ, 	REQ_MERCHANT_ID, 	"MerchantID",	0x9F16,	"Merchant Identifier"},
	{SRC_REQ, 	REQ_POS_ENTRY_MODE_CODE,	"POSEntryMode" ,0x9F39,	"Point-of-Service Entry Mode"},	
	{SRC_REQ, 	REQ_TERM_CTRY_CODE, "TermCtryCode", 0x9F1A,	"Terminal Country Code"},	
	//{SRC_REQ,  	REQ_TERM_ID, 		"TermID", 0x9F1C,	"Terminal Identification"},
	//Jason delete on 20130524
	//{SRC_REQ,	REQ_AMOUNT_AUTHOR, 	"TransAmount", 0x9F02,	"Amount,Authorised"},
	//End
	//{SRC_REQ, 	REQ_TRANS_CUR_CODE, "TransCurCode",	0x5F2A,	"Transaction Currency Code"},
	{SRC_REQ,	REQ_TRANS_DATE, 	"Date",	0x9A,	"Transaction Date"},
	{SRC_REQ,	REQ_TRANS_TIME, 	"Time", 0x9F21,	"Transaction Time"},

	//dddddddddddddddddddddddddd
	//{SRC_REQ,	REQ_ARC,		"AuthRespCode", 0x8A,"Authorisation Response Code"},
	//{SRC_RESP,	RESP_PIN_BLOCK, "PinBlock",	0x00, "Encrypted Pin Block"},
	{SRC_RESP, RESP_EC_ISSUER_AUTH_CODER, "ECCode", 0x9F74, "EC Issuer Code"},
	//Issuer Script Results
	{SRC_RESP, RESP_EC_ISSUER_AUTH_CODER, "Balance", 0x9F5D, "QPBOC OFFLINE Balance"},
	
	
	
};
int g_DetailItemCount =sizeof(g_Detail)/sizeof(g_Detail[0]);

int GetDetailIndex(int src, int id )
{
	int i;

	for(i=0; i< sizeof(g_Detail)/sizeof(g_Detail[0]); i++)
	{
		if((g_Detail[i].src == src) && (g_Detail[i].index == id))
		{
			return i-1;
		}
	}

	return -1;
}
#ifdef VPOS_APP
extern int iGetVoidRecIdx(void);
extern void vSetTransRevFlag(int flag);
extern void vGetBankCardTransName(uint uiTransType, uchar ucVoidFlag, char *pszTrName);
int SaveBatchCaptureDetail(void)
{
	byte buf[100];

    byte batchCaptureAuth[sizeof(gl_TransRec.sIcData)];
	uint16 bufLen;

	unsigned short tag;
    
//    vSetTransRevFlag(0);
    if(gl_TransRec.uiTransType==TRANS_TYPE_BALANCE)
    {
        return 0;
    }

     //不允许脱机交易
    if(gl_TransRec.ucUploadFlag!=0xFF)
    {
        vClearLines(2);
        vMessageMul("脱机故障交易,请联系服务商");
        vSetDispRetInfo(0);
        return 0;
    }
	
	// 保存当前交易
    buf[0]= *GetTagValue(&runningTimeEnv.tagList, 0x9C);	// 交易类型
    if(buf[0]==0x31 || buf[0]==0x30)    //查询交易不保存
        return 0;
    
	dbg("transtype:%04X\n", gl_TransRec.uiTransType);
    gl_TransRec.ucVoidFlag=0;        

	translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, TAG_PAN),
		GetTagValueSize(&runningTimeEnv.tagList, TAG_PAN), buf);
	//dbg("pan:[%s]\n", buf);
	strcat(buf, "FFFFFFFFFF");
	vTwoOne(buf, 20, gl_TransRec.sPan);
	
	tag=0x5F34;
	translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
		GetTagValueSize(&runningTimeEnv.tagList, tag), buf);
    dbg("Tag5F34: pan sn %04X=[%s]\n", tag, buf);
	if(buf[0])
		gl_TransRec.ucPanSerNo=atoi(buf);
	else
		gl_TransRec.ucPanSerNo=0xFF;

	tag=0x9F41;
	translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
		GetTagValueSize(&runningTimeEnv.tagList, tag), buf);
	//dbg("ulTTC=%06lu, %04X=[%s]\n", gl_SysData.ulTTC, tag, buf);
	gl_TransRec.ulTTC=atol(buf);

	tag=0x5F20;
	memcpy(gl_TransRec.gl_uc5f20,GetTagValue(&runningTimeEnv.tagList, tag),
		GetTagValueSize(&runningTimeEnv.tagList, tag));
	dbgHex("gl_uc5f20", gl_TransRec.gl_uc5f20,strlen(gl_TransRec.gl_uc5f20));
		
	if(gl_TransRec.ucUploadFlag!=0xFF)
	{
		//date
		tag=0x9A;
		translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
			GetTagValueSize(&runningTimeEnv.tagList, tag), buf);
		//dbg("date %04X=[%s]\n", tag, buf);
		
		//time
		tag=0x9F21;
		translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
			GetTagValueSize(&runningTimeEnv.tagList, tag), buf+6);
		//dbg("time %04X=[%s]\n", tag, buf);
		vTwoOne(buf, 12, gl_TransRec.sDateTime);
	}

	if (gl_ucStandbyCardFlag == 0) 
	{
		tag=0x9F02;
		translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
			GetTagValueSize(&runningTimeEnv.tagList, tag), buf);
	    //dbg("amt %04X=[%s]\n", tag, buf);
		gl_TransRec.ulAmount=atol(buf);
	}
	
	memcpy(gl_TransRec.sReferenceNo, gl_Recv8583.Field37, 12);	// 交易参考号, 联机交易存在
    memcpy(gl_TransRec.sAuthCode, gl_Recv8583.Field38, 6);      // 授权码
	
	if (TagIsExisted(&runningTimeEnv.tagList, 0x9F5D) == TRUE)
    {
		tag=0x9F5D;
        translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
					GetTagValueSize(&runningTimeEnv.tagList, tag), buf);
    	//dbg("bal %04X=[%s]\n", tag, buf);
		gl_TransRec.ulBalance=atol(buf);
    }else if (TagIsExisted(&runningTimeEnv.tagList, 0x9F79) == TRUE)
	{
		tag=0x9F79;
		translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
			GetTagValueSize(&runningTimeEnv.tagList, tag), buf);
		//dbg("bal %04X=[%s]\n", tag, buf);
		gl_TransRec.ulBalance=atol(buf);
	}
    if((gl_TransRec.uiEntryMode%100)/10==5)
        GetBatchCaptureAuth(batchCaptureAuth, &bufLen);
    else
        GetQpbocBatchCaptureAuth(batchCaptureAuth, &bufLen);
	LOGD("bufLen = %d", bufLen);
    dbgHex("SaveBatchCaptureDetail:", batchCaptureAuth, bufLen);

    if(bufLen>7)
    {
        gl_TransRec.uiIcDataLen=bufLen-7;
        if(gl_TransRec.uiIcDataLen>sizeof(gl_TransRec.sIcData))
        {
            LOGE("IcData len err:%d", gl_TransRec.uiIcDataLen);
            gl_TransRec.uiIcDataLen=sizeof(gl_TransRec.sIcData);
        }
        memcpy(gl_TransRec.sIcData, batchCaptureAuth+7, gl_TransRec.uiIcDataLen);        
    }else
        gl_TransRec.uiIcDataLen=0;
	vTwoOne(gl_SysData.szCurOper, 2, gl_TransRec.sOperNo);

/*
    if((gl_TransRec.uiTransType == TRANS_TYPE_SALE 
		|| gl_TransRec.uiTransType == TRANS_TYPE_DAIRY_SALE
		|| gl_TransRec.uiTransType == TRANS_TYPE_PREAUTH) 
		&& gl_TransRec.ucScriptFlag == 1 && gl_TransRec.uiIcDataLen > 0) {
		
	byte tmp[50];
	int tagBinLen;
	TagList tmpTagList,tmpTagList1;
	Tag3ByteList tmpTag3ByteList;
	uint16 tmpTLVBufLen;
	uchar  sIcData[260] = {0};                // IC卡数据, TLV格式
	ushort uiIcDataLen;                 // IC卡数据长度
	st8583			Send8583;							// 发送8583结构
        char szBuf[100];

        InitTagList(&tmpTagList);
        InitTag3ByteList(&tmpTag3ByteList);
        
        BuildTagListOneLevelBctc(gl_TransRec.sIcData, gl_TransRec.uiIcDataLen, &tmpTagList, &tmpTag3ByteList);

        tag=0x9F33;
        if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }

	 tag=0x95;
        if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }
		
        tag=0x9F37;
    	if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }
   
 	tag=0x9F1E;
	if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }

	 tag=0x9F10;
	if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }	
	
 	tag=0x9F26;
         if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }
		 
 	tag=0x9A;
 	if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }
	
 	tag=0x9F36;
 	 if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }
	 
	 tag=0x82;
         if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }
		 
	 tag=0x9F1A;
        if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }
		
  	tag=0xDF31;
        if(GetTagValueSize(&tmpTagList, tag))
        {
            tagBinLen=GetTagValueSize(&tmpTagList, tag);	
	    memcpy(tmp, GetTagValue(&tmpTagList, tag), tagBinLen);
	    SetTagValue(tag, tmp, tagBinLen, &tmpTagList1);			
        }


        TagListBuildTLV(&tmpTagList1, sIcData, &tmpTLVBufLen);
	uiIcDataLen = tmpTLVBufLen;	
	dbgHex("sIcData TAG:", sIcData, uiIcDataLen);
       FreeTagList(&tmpTagList);
       FreeTagList(&tmpTagList1);	   

        //脚本报文
        memset(&Send8583, 0, sizeof(Send8583));
	
        strcpy((char *)Send8583.Msg01, "0620");
	if(gl_Send8583.Field02)	
		 strcpy((char *)Send8583.Field02, gl_Send8583.Field02);
         strcpy((char *)Send8583.Field03, gl_Send8583.Field03);
         strcpy((char *)Send8583.Field04, gl_Send8583.Field04);
			
        vIncTTC();
	 sprintf((char *)Send8583.Field11, "%06lu", gl_SysData.ulTTC);	

	 strcpy((char *)Send8583.Field22, gl_Send8583.Field22);
	 strcpy((char *)Send8583.Field23, gl_Send8583.Field23);

          strcpy((char *)Send8583.Field25, gl_Send8583.Field25);
	  strcpy((char *)Send8583.Field32, gl_Recv8583.Field32);

         strcpy((char *)Send8583.Field37, gl_Recv8583.Field37);
		 
    	strcpy((char *)Send8583.Field41, (char *)gl_SysInfo.szPosId);
    	strcpy((char *)Send8583.Field42, (char *)gl_SysInfo.szMerchId);

	strcpy((char *)Send8583.Field49, gl_Send8583.Field49);	
	
	Send8583.Field55[0] = uiIcDataLen/256;
	Send8583.Field55[1] = uiIcDataLen%256;
	memcpy(Send8583.Field55+2, sIcData, uiIcDataLen); 
	
	 sprintf((char *)Send8583.Field60, "%02u%06lu%.3s%.2s", 0, gl_SysData.ulBatchNo, "951","50");

	vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
        sprintf((char *)Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, gl_TransRec.ulTTC, szBuf);

	 memcpy((char *)Send8583.Field64, "\x00\x08", 2);  
	 
        uiMemManaPut8583(MSG8583_TYPE_SCR, &Send8583);		
    }   
*/
	
    //联机交易,EMV内核返回无需签名且强制签名为0
#ifdef APP_LKL    
    //不管内核返回,防止测试人员提bug
    if(0 && gl_TransRec.ucUploadFlag!=0xFF && runningTimeEnv.commonRunningData.signatureRequired==0 && gl_SysData.ucForceSign==0)
#else
    if(gl_TransRec.ucUploadFlag!=0xFF && runningTimeEnv.commonRunningData.signatureRequired==0)
#endif    
    {
        gl_TransRec.ucTransAttr&=(0xFF-0x02);
        gl_TransRec.ucSignupload=0xFF;
    }

    //预设需签字+需纸签,电签成功后修改为无需纸签
    gl_TransRec.ucTransAttr=(0x02|0x04);	

     //若为联机交易,清冲正
    if(gl_TransRec.ucUploadFlag==0xFF)
    {
        dbg("online trans\n");
        uiMemManaErase8583(MSG8583_TYPE_REV);
        gl_TransRec.ucSignupload=0;
    }else
    {
        dbg("offline trans\n");
        gl_TransRec.ucUploadFlag=1;         //脱机交易,需上送
        gl_TransRec.ucTransAttr&=(0xFF-0x02);   //脱机交易(电子现金)无需签名
        gl_TransRec.ucSignupload=0xFF;
    }
	
    //保存交易记录
    uiMemManaPutTransRec(gl_SysData.uiTransNum++, &gl_TransRec);
    gl_SysData.ulLastTransTTC = gl_TransRec.ulTTC;
    uiMemManaPutSysData();
	
#ifdef APP_LKL
    if(gl_TransRec.uiTransType==TRANS_TYPE_SALE )
    {
        gl_SysInfo.ucServiceFee=2;
        uiMemManaPutSysInfo();
    }
#endif    
    if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID
        || (gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID && gl_TransRec.ulVoidTTC!=0))
    {
        //原交易做取消标志
        stTransRec voidTransRec;
        int  iIndex=iGetVoidRecIdx();

        if(iIndex>=0 && iIndex<gl_SysData.uiTransNum-1)
        {
            uiMemManaGetTransRec(iIndex, &voidTransRec);
            if(voidTransRec.ulTTC==gl_TransRec.ulVoidTTC)
            {
                voidTransRec.ucVoidFlag=1;
                uiMemManaPutTransRec(iIndex, &voidTransRec);
            }
        }
    }
    
    {
        extern void vPrtTransRecInfo(void);
        vPrtTransRecInfo();
    }
#if 0
    vClearLines(2);
	//签字
	{
        extern int iBillSign(int flag, int iRecIdx, void *pRec);
        
        //1. 电子现金消费(脱机)，无需签名
        //2. pboc返回无需签名且强制签名为0，无需签名
        //3. qpboc联机交易满足小额免签免密且强制签名为0，无需签名
	         if(gl_SysInfo.ucSupportESign == 1)
	       	{
	       		if(gl_TransRec.ucSignupload!=0xFF)
	           	 iBillSign(1, -1, NULL);
	         }
	}
#endif
    vGetBankCardTransName(gl_TransRec.uiTransType, 0, buf);

    _vCls();
    vDispCenter(1, buf, 1);
    
    vSetDispRetInfo(0);
#ifdef ENABLE_PRINTER 
    {	
        extern int iPrintTrans(stTransRec *rec, int dup, uchar *pszReference);
        extern void vShowWaitEx(char *pszMessage, long lWaitTime, int keyFlag);
        
        vShowWaitEx("交易成功,打印票据...", -1, 0);
        iPrintTrans(&gl_TransRec, 0, gl_Recv8583.Field48);
        vShowWaitEx(NULL, 0, 0x03);
	}
#else    
    {
        gl_SysInfo.ucDepositFlag = 1;
	gl_SysInfo.ucActivityFlag = 1;		
	if(gl_SysInfo.ucGprsUseFlag  == 1)
		gl_SysInfo.ucGprsPayFlag = 1;
	
        vMessage("交易成功");
    }
#endif
    /*
    //若当前为联机交易,交易完成后上送脱机交易
    if(gl_TransRec.ucUploadFlag==0xFF)      
    {
        extern int iUploadOfflineTrans(void);
        iUploadOfflineTrans();
    }    
    */
    {
        sprintf(buf, "authcode1:[%.6s]\n", gl_TransRec.sAuthCode);
        dbg(buf);
    }
	//日结确认
	dbg("ucSaleType=%d\r\n",gl_TransRec.ucSaleType);
    if(gl_TransRec.ucSaleType==1){
		iUploadComfirmSaleTrade();
	}
    return 0;
}

int iGetSignMsgFd55FromEnv(uchar *psOutData)
{
	//盛迪嘉签名上送报文55域: 9F26,95,4F,5F34,9B,9F36,82,9F37,50
	tti_uint16 tagTLVLen=0;
	tti_uint16 tagList[] = { 
							0x9F26,			// ARQC
							0x95,			// TVR
							0x4F,			// AID
							0x5F34,			// CSN
							0x9B,			// TSI
							0x9F36,			// ATC
							0x82,			// AIP
							0x9F37,			// UNPR NUM
							0x50			// APPLAB									
							};
	

	TagArrsBuildTLV(&(runningTimeEnv.tagList), tagList, sizeof(tagList)/sizeof(tagList[0]), psOutData, &tagTLVLen);
	//_vDelay(20);
	hexdumpEx("signFd55:", psOutData, tagTLVLen);
	//_vDelay(20);

	return tagTLVLen;
}

int SaveQpbocBatchCaptureDetail()
{
    return SaveBatchCaptureDetail();
}

int SaveDetail()
{
    return 0;
}
#else
#ifndef CY20_EMVL2_KERNEL
int SaveBatchCaptureDetail()
{
	FILE *fp;
	byte batchCaptureAuth[MAX_COMM_HOST_SEND_BUF_SIZE];
	uint16 bufLen;
	
	fp = fopen(GetValidBatchRecordFileName(), "w");	
	if(fp == NULL)
	{
		DisplayValidateInfo("Save Detail Fail");
		return -1;
	}

	GetBatchCaptureAuth(batchCaptureAuth, &bufLen);
	LOGD("bufLen = %d", bufLen);

	if (fwrite(batchCaptureAuth, bufLen, 1, fp) != 1)
	{
		fclose(fp);
		LOGE("fwrite error");
		return -1;
	}

	fclose(fp);
	return 0;
}
#else
int SaveBatchCaptureDetail()
{
	lfs_file_t file;
	byte batchCaptureAuth[MAX_COMM_HOST_SEND_BUF_SIZE];
	uint16 bufLen;
	int ret;
	
	ret = sys_lfs_file_open(&file, GetValidBatchRecordFileName(), LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);	
	if(ret != 0)
	{
		DisplayValidateInfo("Save Detail Fail");
		LOGE("sys_lfs_file_open error : %d", ret);
		return -1;
	}

	GetBatchCaptureAuth(batchCaptureAuth, &bufLen);
	LOGD("bufLen = %d", bufLen);

	if ((ret = sys_lfs_file_write(&file, batchCaptureAuth, bufLen)) != bufLen)
	{
		sys_lfs_file_close(&file);
		LOGE("sys_lfs_file_write error, ret = %d", ret);
		return -1;
	}

	sys_lfs_file_close(&file);
	return 0;
}

#endif

#ifndef CY20_EMVL2_KERNEL
int SaveQpbocBatchCaptureDetail()
{
	FILE *fp;
	byte batchCaptureAuth[MAX_COMM_HOST_SEND_BUF_SIZE];
	uint16 bufLen;
	
	fp = fopen(GetValidQpbocBatchRecordFileName(), "w");	
	if(fp == NULL)
	{
		DisplayValidateInfo("Save Detail Fail");
		return -1;
	}

	GetQpbocBatchCaptureAuth(batchCaptureAuth, &bufLen);
	LOGD("bufLen = %d", bufLen);

	if (fwrite(batchCaptureAuth, bufLen, 1, fp) != 1)
	{
		fclose(fp);
		LOGE("fwrite error");
		return -1;
	}

	fclose(fp);
	return 0;
}
#else
int SaveQpbocBatchCaptureDetail()
{
	lfs_file_t file;
	byte batchCaptureAuth[MAX_COMM_HOST_SEND_BUF_SIZE];
	uint16 bufLen;
	int ret;
	
	ret = sys_lfs_file_open(&file, GetValidQpbocBatchRecordFileName(), LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);	
	if(ret != 0)
	{
		DisplayValidateInfo("Save Detail Fail");
		return -1;
	}

	GetQpbocBatchCaptureAuth(batchCaptureAuth, &bufLen);
	LOGD("bufLen = %d", bufLen);

	if (sys_lfs_file_write(&file, batchCaptureAuth, bufLen) != bufLen)
	{
		sys_lfs_file_close(&file);
		LOGE("sys_lfs_file_write error");
		return -1;
	}

	sys_lfs_file_close(&file);
	return 0;
}

#endif

int SaveDetail()
{
#ifndef CY20_EMVL2_KERNEL
	int i;	
	FILE *fp;
	vec_t *data;
	DetailItem_t *pItem;
	int index;
	
	fp = fopen(GetValidRecordFileName(), "w");

	if(fp == NULL)
	{
		DisplayValidateInfo("Save Detail Fail");
		return -1;
	}

	fprintf(fp, "MessageType;08\r\n");
	for(i=0; i<sizeof(g_Detail)/sizeof(g_Detail[0]); i++)
	{
		pItem = &(g_Detail[i]);		

		index = pItem->index;
		
		if(pItem->src == SRC_RESP)
		{
			if(index == RESP_RAND_ONLINE_TRANS_SELECT_NUM
				|| index == RESP_PIN_BLOCK)
			{
				continue;
			}
		}

		if(pItem->src == SRC_REQ)
		{
			if(IsRequestItemExisted(pItem->index))
			{
				data = GetRequestItemValue(pItem->index);
				fprintf(fp, "%s;%s\r\n", pItem->BatchCaptureHeader, data->ptr);
			}
			else
			{
				fprintf(fp, "%s;\r\n", pItem->BatchCaptureHeader);
			}
		}
		else
		{
			if(IsResponseItemExisted(pItem->index))
			{
				data = GetResponseItemValue(pItem->index);				
				fprintf(fp, "%s;%s\r\n",pItem->BatchCaptureHeader, data->ptr);
			}
			else
			{
				fprintf(fp, "%s;\r\n", pItem->BatchCaptureHeader);
			}
		}
	}

	if(IsResponseItemExisted(RESP_SCRIPT_RESULT))
	{
		fprintf(fp, "Script Result;%s\r\n", GetResponseItemValue(RESP_SCRIPT_RESULT)->ptr);
	}
	else
	{
		fprintf(fp,"Script Result;\r\n");
	}

	fclose(fp);

	return STATUS_OK;
#else
#if 1
	int i;	
	int ret;
	lfs_file_t file;
	vec_t *data;
	DetailItem_t *pItem;
	int index;
	char buffer[1024 * 10];
	
	ret = sys_lfs_file_open(&file, GetValidRecordFileName(), LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
	if(ret != 0)
	{
		DisplayValidateInfo("Save Detail Fail");
		dbg("sys_lfs_file_open err: [%d]\n", ret);
		return -1;
	}

	//fprintf(fp, "MessageType;08\r\n");
	sprintf(buffer, "%s", "MessageType;08\r\n");
	//dbg("buffer = %s\n", buffer);
	sys_lfs_file_write(&file, buffer, strlen(buffer));
	
	for(i=0; i<sizeof(g_Detail)/sizeof(g_Detail[0]); i++)
	{
		pItem = &(g_Detail[i]);		

		index = pItem->index;
		
		if(pItem->src == SRC_RESP)
		{
			if(index == RESP_RAND_ONLINE_TRANS_SELECT_NUM
				|| index == RESP_PIN_BLOCK)
			{
				continue;
			}
		}

		if(pItem->src == SRC_REQ)
		{
			if(IsRequestItemExisted(pItem->index))
			{
				data = GetRequestItemValue(pItem->index);
				//fprintf(fp, "%s;%s\r\n", pItem->BatchCaptureHeader, data->ptr);
				sprintf(buffer, "%s;%s\r\n", pItem->BatchCaptureHeader, data->ptr);
				//dbg("buffer = %s\n", buffer);
				sys_lfs_file_write(&file, buffer, strlen(buffer));
			}
			else
			{
				//fprintf(fp, "%s;\r\n", pItem->BatchCaptureHeader);
				sprintf(buffer, "%s;\r\n", pItem->BatchCaptureHeader);
				//dbg("buffer = %s\n", buffer);
				sys_lfs_file_write(&file, buffer, strlen(buffer));
			}
		}
		else
		{
			if(IsResponseItemExisted(pItem->index))
			{
				data = GetResponseItemValue(pItem->index);				
				//fprintf(fp, "%s;%s\r\n",pItem->BatchCaptureHeader, data->ptr);
				sprintf(buffer, "%s;%s\r\n",pItem->BatchCaptureHeader, data->ptr);
				//dbg("buffer = %s\n", buffer);
				sys_lfs_file_write(&file, buffer, strlen(buffer));
			}
			else
			{
				//fprintf(fp, "%s;\r\n", pItem->BatchCaptureHeader);
				sprintf(buffer, "%s;\r\n", pItem->BatchCaptureHeader);
				//dbg("buffer = %s\n", buffer);
				sys_lfs_file_write(&file, buffer, strlen(buffer));
			}
		}
	}

	if(IsResponseItemExisted(RESP_SCRIPT_RESULT))
	{
		//fprintf(fp, "Script Result;%s\r\n", GetResponseItemValue(RESP_SCRIPT_RESULT)->ptr);
		sprintf(buffer, "Script Result;%s\r\n", GetResponseItemValue(RESP_SCRIPT_RESULT)->ptr);
		//dbg("buffer = %s\n", buffer);
		ret = sys_lfs_file_write(&file, buffer, strlen(buffer));
	}
	else
	{
		//fprintf(fp,"Script Result;\r\n");
		sprintf(buffer, "%s", "Script Result;\r\n");
		//dbg("buffer = %s\n", buffer);
		sys_lfs_file_write(&file, buffer, strlen(buffer));
	}

	//fclose(fp);
	sys_lfs_file_close(&file);

	return STATUS_OK;
#else
	int i;	
	int ret;
	lfs_file_t file;
	vec_t *data;
	DetailItem_t *pItem;
	int index;
	char buffer[1024 * 5];
	char totalBuffer[1024 * 15];
	
	ret = sys_lfs_file_open(&file, GetValidRecordFileName(), LFS_O_CREAT | LFS_O_WRONLY | LFS_O_TRUNC);
	if(ret != 0)
	{
		DisplayValidateInfo("Save Detail Fail");
		dbg("sys_lfs_file_open err: [%d]\n", ret);
		return -1;
	}

	//fprintf(fp, "MessageType;08\r\n");
	sprintf(buffer, "%s", "MessageType;08\r\n");
	//dbg("buffer = %s\n", buffer);
	//sys_lfs_file_write(&file, buffer, strlen(buffer));
	strcpy(totalBuffer, buffer);
	
	for(i=0; i<sizeof(g_Detail)/sizeof(g_Detail[0]); i++)
	{
		pItem = &(g_Detail[i]);		

		index = pItem->index;
		
		if(pItem->src == SRC_RESP)
		{
			if(index == RESP_RAND_ONLINE_TRANS_SELECT_NUM
				|| index == RESP_PIN_BLOCK)
			{
				continue;
			}
		}

		if(pItem->src == SRC_REQ)
		{
			if(IsRequestItemExisted(pItem->index))
			{
				data = GetRequestItemValue(pItem->index);
				//fprintf(fp, "%s;%s\r\n", pItem->BatchCaptureHeader, data->ptr);
				sprintf(buffer, "%s;%s\r\n", pItem->BatchCaptureHeader, data->ptr);
				//dbg("buffer = %s\n", buffer);
				//sys_lfs_file_write(&file, buffer, strlen(buffer));
				strcat(totalBuffer, buffer);
			}
			else
			{
				//fprintf(fp, "%s;\r\n", pItem->BatchCaptureHeader);
				sprintf(buffer, "%s;\r\n", pItem->BatchCaptureHeader);
				//dbg("buffer = %s\n", buffer);
				//sys_lfs_file_write(&file, buffer, strlen(buffer));
				strcat(totalBuffer, buffer);
			}
		}
		else
		{
			if(IsResponseItemExisted(pItem->index))
			{
				data = GetResponseItemValue(pItem->index);				
				//fprintf(fp, "%s;%s\r\n",pItem->BatchCaptureHeader, data->ptr);
				sprintf(buffer, "%s;%s\r\n",pItem->BatchCaptureHeader, data->ptr);
				//dbg("buffer = %s\n", buffer);
				//sys_lfs_file_write(&file, buffer, strlen(buffer));
				strcat(totalBuffer, buffer);
			}
			else
			{
				//fprintf(fp, "%s;\r\n", pItem->BatchCaptureHeader);
				sprintf(buffer, "%s;\r\n", pItem->BatchCaptureHeader);
				//dbg("buffer = %s\n", buffer);
				//sys_lfs_file_write(&file, buffer, strlen(buffer));
				strcat(totalBuffer, buffer);
			}
		}
	}

	if(IsResponseItemExisted(RESP_SCRIPT_RESULT))
	{
		//fprintf(fp, "Script Result;%s\r\n", GetResponseItemValue(RESP_SCRIPT_RESULT)->ptr);
		sprintf(buffer, "Script Result;%s\r\n", GetResponseItemValue(RESP_SCRIPT_RESULT)->ptr);
		//dbg("buffer = %s\n", buffer);
		//ret = sys_lfs_file_write(&file, buffer, strlen(buffer));
		strcat(totalBuffer, buffer);
	}
	else
	{
		//fprintf(fp,"Script Result;\r\n");
		sprintf(buffer, "%s", "Script Result;\r\n");
		//dbg("buffer = %s\n", buffer);
		//sys_lfs_file_write(&file, buffer, strlen(buffer));
		strcat(totalBuffer, buffer);
	}

	//fclose(fp);
	sys_lfs_file_write(&file, totalBuffer, strlen(totalBuffer));
	dbg("totalBuffer[%d] = %s\n", strlen(totalBuffer), totalBuffer);
	sys_lfs_file_close(&file);

	return STATUS_OK;
#endif
#endif
}
#endif

void SaveTransDetailNo(int transDetailNo)
{
	
}

int LoadTransDetailsNo(void)
{
	return 0;
}

void SaveBatchDetailNo(int batchDetailNo)
{
	
}

int LoadBatchDetailsNo(void)
{
	return 0;
}

void SaveQpbocDetailNo(int transDetailNo)
{
	
}

int LoadQpbocTransDetailsNo(void)
{
	return 0;
}



int PrintScriptResult(char *Result)
{
	char buf[1024];
	char *p;
	char *CurPos;

	//TtiOsPrntBuffer("ScriptResult(as below):");
	NJRCBase_Print_FillBuffer("ScriptResult(as below):");
	
	if(Result[0] == '\0')
	{
		return STATUS_OK;
	}
	
	CurPos = Result;

	while(1)
	{		
		p = strchr(CurPos, '|');
		
		if(p == NULL)
		{
			strcpy(buf, CurPos);
		}
		else 
		{
			memcpy(buf, CurPos, p-CurPos);
			buf[p-CurPos]='\0';			
		}
		
		//TtiOsPrntBuffer(buf);
		//TtiOsPrntBuffer("\n   "); 
		NJRCBase_Print_FillBuffer(buf);
		NJRCBase_Print_FillBuffer("\n   ");
		
		if(p == NULL)
		{
			break;
		}
		
		CurPos = p + 1;
	}

	return STATUS_OK;
}

tti_bool gbIsPrint = FALSE;

void SetPrintOrNot(tti_bool isPrint)
{
	gbIsPrint = isPrint;
}

tti_int32 qpbocPrintReceipt(RunningTimeEnv *runningTimeEnv)
{
	int i;	
	char buf[1024*6];
	//vec_t *data;
	DetailItem_t *pItem;
	//tti_byte tempByteArray[256];
	//tti_uint16 tempByteLen;
	tti_tchar tempCharArray[256];
	//char szNewPrintData[100];
	int index;
	tti_int16 transCurrencyCode;
	int amount;
	char szAmount[30];
	//char szAmountOther[30];
	char szFormatAmount[30];
	char szFormatBalance[30];

	if (gbIsPrint == FALSE)
	{
		return STATUS_OK;
	}

	DisplayProcess("打印中");

	NJRCBase_Print_Prepare(600);

	NJRCBase_Print_ClearBuffer();
	PrintHeader("            Receipt");

	for(i=0; i<sizeof(g_QpbocDetail)/sizeof(g_QpbocDetail[0]); i++)
	{
		pItem = &(g_QpbocDetail[i]);

		index = pItem->index;
		dbg("index = %d\n", index);
		
		
		if (TagDataIsMissing(&runningTimeEnv->tagList, pItem->tag) == TRUE)
		{
			NJRCBase_Print_FillBuffer("\n");
			continue;
		}else
		{
			//Jason added on 20200422
			if(index == REQ_AMOUNT_AUTHOR && (pItem->src) == SRC_REQ)
			{
				dbg("REQ_AMOUNT_AUTHOR\n");
				transCurrencyCode = GetTransCurrencyCode(NULL);
				if (transCurrencyCode == 0x0840)
				{
					sprintf(buf, "%s: ", "AmountAuth (USD)");
				}else
				{
					sprintf(buf, "%s: ", "AmountAuth (CNY)");
				}
				NJRCBase_Print_FillBuffer(buf);

				amount = EMVL2_GetAmount();
				sprintf(szAmount, "%d", amount);
				memset(szFormatAmount, 0x00, sizeof(szFormatAmount));
				foramtAmount(szAmount, szFormatAmount);
				NJRCBase_Print_FillBuffer(szFormatAmount);
				NJRCBase_Print_FillBuffer("\n");
				continue;
			}	
			else if(index == REQ_AMOUNT_OTHER && (pItem->src) == SRC_REQ)
			{
				dbg("REQ_AMOUNT_OTHER\n");
				transCurrencyCode = GetTransCurrencyCode(NULL);
				if (transCurrencyCode == 0x0840)
				{
					sprintf(buf, "%s: ", "AmountOth (USD)");
				}else
				{
					sprintf(buf, "%s: ", "AmountOth (CNY)");
				}
				NJRCBase_Print_FillBuffer(buf);

				amount = EMVL2_GetAmountCash();
				sprintf(szAmount, "%d", amount);
				memset(szFormatAmount, 0x00, sizeof(szFormatAmount));
				foramtAmount(szAmount, szFormatAmount);
				NJRCBase_Print_FillBuffer(szFormatAmount);
				NJRCBase_Print_FillBuffer("\n");
				continue;
			}	
			else if(index == RESP_EC_ISSUER_AUTH_CODER && (pItem->src) == SRC_RESP)
			{
				dbg("Balance\n");
				sprintf(buf, "%s: ", pItem->ReceiptHeader);
				NJRCBase_Print_FillBuffer(buf);
				memset(szFormatBalance, 0x00, sizeof(szFormatBalance));
				formatAmountByTag(&runningTimeEnv->tagList, pItem->tag, szFormatBalance);
				NJRCBase_Print_FillBuffer(szFormatBalance);
				NJRCBase_Print_FillBuffer("\n");
				continue;
			}
			//End

			sprintf(buf, "%s: ", pItem->ReceiptHeader);
			NJRCBase_Print_FillBuffer(buf);
		
			if (((pItem->src) == SRC_REQ && index == REQ_MERCHANT_NAME_LOCATION)
				||((pItem->src) == SRC_RESP && index == RESP_APP_LABEL))
			{
				dbg("REQ_MERCHANT_NAME_LOCATION or RESP_APP_LABEL\n");
				memset(tempCharArray, 0x00, sizeof(tempCharArray));
				memcpy(tempCharArray, GetTagValue(&runningTimeEnv->tagList, pItem->tag), 
					GetTagValueSize(&runningTimeEnv->tagList, pItem->tag));
				dbg("tempCharArray = %s\n", tempCharArray);
			}else
			{
				TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, pItem->tag), 
					GetTagValueSize(&runningTimeEnv->tagList, pItem->tag), tempCharArray);
			}
			NJRCBase_Print_FillBuffer(tempCharArray);
			NJRCBase_Print_FillBuffer("\n");
		}
	}

	if(runningTimeEnv->qpbocCvmSign == TRUE)
	{
		NJRCBase_Print_FillBuffer("\n\n");
		NJRCBase_Print_FillBuffer("Sign:_______________________");
	}
	
	PrintFooter();
	NJRCBase_Print_StartPrint();		//TtiOsPrint();
	
	return STATUS_OK;	
}


int PrintReceipt()
{
	int i;	
	char buf[1024*6];
	vec_t *data;
	DetailItem_t *pItem;
	int index;
	tti_int16 transCurrencyCode;
	int amount;
	char szAmount[30];
	//char szAmountOther[30];
	char szFormatAmount[30];

	char szNewPrintData[100];

	if (gbIsPrint == FALSE)
	{
		return STATUS_OK;
	}

	DisplayProcess("打印中");

	NJRCBase_Print_Prepare(600);

	NJRCBase_Print_ClearBuffer();
	PrintHeader("            Receipt");

	for(i=0; i<sizeof(g_Detail)/sizeof(g_Detail[0]); i++)
	{
		pItem = &(g_Detail[i]);

		index = pItem->index;
		if(pItem->src == SRC_RESP)
		{
			if(index == RESP_PIN_BLOCK)
			{
				continue;
			}
			if (index == RESP_EC_ISSUER_AUTH_CODER)
			{
				if (IsEcTrans() == FALSE)
				{
					continue;
				}
			}
		}

		//Jason added on 20200422
		if(index == REQ_AMOUNT_AUTHOR)
		{
			transCurrencyCode = GetTransCurrencyCode(NULL);
			if (transCurrencyCode == 0x0840)
			{
				sprintf(buf, "%s: ", "AmountAuth (USD)");
			}else
			{
				sprintf(buf, "%s: ", "AmountAuth (CNY)");
			}
			NJRCBase_Print_FillBuffer(buf);

			amount = EMVL2_GetAmount();
			sprintf(szAmount, "%d", amount);
			memset(szFormatAmount, 0x00, sizeof(szFormatAmount));
			foramtAmount(szAmount, szFormatAmount);
			NJRCBase_Print_FillBuffer(szFormatAmount);
			NJRCBase_Print_FillBuffer("\n");
			continue;
		}	
		else if(index == REQ_AMOUNT_OTHER)
		{
			transCurrencyCode = GetTransCurrencyCode(NULL);
			if (transCurrencyCode == 0x0840)
			{
				sprintf(buf, "%s: ", "AmountOth (USD)");
			}else
			{
				sprintf(buf, "%s: ", "AmountOth (CNY)");
			}
			NJRCBase_Print_FillBuffer(buf);

			amount = EMVL2_GetAmountCash();
			sprintf(szAmount, "%d", amount);
			memset(szFormatAmount, 0x00, sizeof(szFormatAmount));
			foramtAmount(szAmount, szFormatAmount);
			NJRCBase_Print_FillBuffer(szFormatAmount);
			NJRCBase_Print_FillBuffer("\n");
			continue;
		}	
		//End

		sprintf(buf, "%s: ", pItem->ReceiptHeader);

		//TtiOsPrntBuffer(buf);
        //LOGD("%s", buf);
		NJRCBase_Print_FillBuffer(buf);
		
		if(pItem->src == SRC_REQ)
		{
			if(IsRequestItemExisted(pItem->index))
			{
				data = GetRequestItemValue(pItem->index);
				//Changed by Siken 2011.09.25
				//FmtValue(data->ptr, buf);
				if((0 == strncmp(pItem->ReceiptHeader, "TermCtryCode", 12)) || 
				   (0 == strncmp(pItem->ReceiptHeader, "TransCurCode", 12)))
				{
					sprintf(szNewPrintData, "0%s", data->ptr);
					FmtValue(szNewPrintData, buf);
				}
				else
				{
					FmtValue((char *)(data->ptr), buf);
				}
				//End
			}
			else
			{
				buf[0]='\0';
			}
		}
		else
		{
			if(IsResponseItemExisted(pItem->index))
			{
				data = GetResponseItemValue(pItem->index);
				//Changed by Siken 2011.09.25
				//FmtValue(data->ptr, buf);
				if((0 == strncmp(pItem->ReceiptHeader, "ServiceCode", 11)))
				{
					sprintf(szNewPrintData, "0%s", data->ptr);
					FmtValue(szNewPrintData, buf);
				}
				else
				{
					FmtValue((char *)(data->ptr), buf);
				}
				//End
			}
			else
			{
				buf[0]='\0';
			}
		}

		//TtiOsPrntBuffer(buf);	
		//TtiOsPrntBuffer("\n");	
		NJRCBase_Print_FillBuffer(buf);
		NJRCBase_Print_FillBuffer("\n");
		//LOGD("%s\n", buf);
		
	}

	if(IsResponseItemExisted(RESP_SCRIPT_RESULT))
	{
		PrintScriptResult((char *)(GetResponseItemValue(RESP_SCRIPT_RESULT)->ptr));
	}
	
	PrintSign();
	PrintFooter();
	NJRCBase_Print_StartPrint();		//TtiOsPrint();
	
	return STATUS_OK;
}

#ifdef VPOS_APP
int ClearTransRecordRaw()
{
    return 0;
}
int ClearBatchTransRecordRaw()
{
    gl_SysData.uiTransNum=0;
    uiMemManaPutSysData();
    return 0;    
}
int ClearQpbocBatchTransRecordRaw()
{
    gl_SysData.uiTransNum=0;
    uiMemManaPutSysData();
    return 0;
}
#else
int ClearTransRecordRaw()
{
#ifndef CY20_EMVL2_KERNEL
	char buf[MAX_FILE_PATH_LEN + 64];

	sprintf(buf, "rm -f %s/Record*", DIR_TRANS_RECORD);
	if(system(buf) == 0)
	{
		return STATUS_OK;
	}
	else
	{
		LOGE("ClearTransRecordRaw error");
		return STATUS_FAIL;
	}
#else
	sys_lfs_filesindir_remove(DIR_TRANS_RECORD);

	return STATUS_OK;
#endif
}

int ClearBatchTransRecordRaw()
{
#ifndef CY20_EMVL2_KERNEL
	char buf[MAX_FILE_PATH_LEN + 64];

	sprintf(buf, "rm -f %s/BatchRecord*", DIR_TRANS_RECORD);
	if(system(buf) == 0)
	{
		return STATUS_OK;
	}
	else
	{
		LOGE("ClearBatchTransRecordRaw error");
		return STATUS_FAIL;
	}
#else
	sys_lfs_filesindir_remove(DIR_BATCH_RECORD);

	return STATUS_OK;
#endif

}

int ClearQpbocBatchTransRecordRaw()
{
#ifndef CY20_EMVL2_KERNEL
	char buf[MAX_FILE_PATH_LEN + 64];

	sprintf(buf, "rm -f %s/QpbocBatchRecord*", DIR_TRANS_RECORD);
	if(system(buf) == 0)
	{
		return STATUS_OK;
	}
	else
	{
		LOGE("ClearBatchTransRecordRaw error");
		return STATUS_FAIL;
	}
#else
	sys_lfs_filesindir_remove(DIR_QPBOC_BATCH_RECORD);

	return STATUS_OK;
#endif
}
#endif


void ClearTransRecord()
{

	DisplayProcess("清除日志中");
	if(ClearTransRecordRaw() != STATUS_OK)
	{
		DisplayValidateInfo("Clean Fail");
		return;
	}

	if(ClearBatchTransRecordRaw() != STATUS_OK)
	{
		DisplayValidateInfo("Clean Fail");
		return;
	}

	//Line;
	//sys_lfs_ummount();
	//Line;
	//sys_lfs_mount();

	DisplayValidateInfo("清除成功");

}

void ClearQpbocTransRecord()
{
	DisplayProcess("清除日志中");

	ClearQpbocBatchTransRecordRaw();

	DisplayValidateInfo("清除成功");
}

#ifdef VPOS_APP
int IsTransRecordFull()
{
    if(gl_SysData.uiTransNum >= MAX_TRANS_RECORD_NUM)
        return 1;
    return 0;
}

int IsQpbocTransRecordFull()
{
    return IsTransRecordFull();
}
#else
int IsTransRecordFull()
{
#ifndef CY20_EMVL2_KERNEL
	char filename[MAX_FILE_PATH_LEN];
	FILE *fp;
	
	sprintf(filename, FMT_TRANS_RECORD_FILE_NAME,DIR_TRANS_RECORD, MAX_TRANS_RECORD_NUM -1);

	fp = fopen(filename, "r");
	
	if(fp != NULL)
	{
		fclose(fp);
		return 1;
	}
	else
	{
		return 0;
	}
#else
	char filename[MAX_FILE_PATH_LEN];
	lfs_file_t lfs_file;
	int ret;
	
	sprintf(filename, FMT_TRANS_RECORD_FILE_NAME, DIR_TRANS_RECORD, MAX_TRANS_RECORD_NUM -1);

	ret = sys_lfs_file_open(&lfs_file, filename, LFS_O_RDONLY);
	if(ret == 0)
	{
		//File exist
		sys_lfs_file_close(&lfs_file);
		return 1;
	}
	else
	{
		//File not exist
		return 0;
	}
#endif
}

int IsQpbocTransRecordFull()
{
#ifndef CY20_EMVL2_KERNEL
	char filename[MAX_FILE_PATH_LEN];
	FILE *fp;
	
	sprintf(filename, FMT_TRANS_QPBOC_BATCH_RECORD_FILE_NAME,DIR_QPBOC_BATCH_RECORD, MAX_TRANS_RECORD_NUM -1);

	fp = fopen(filename, "r");
	
	if(fp != NULL)
	{
		fclose(fp);
		return 1;
	}
	else
	{
		return 0;
	}
#else
	char filename[MAX_FILE_PATH_LEN];
	lfs_file_t lfs_file;
	int ret;
	
	sprintf(filename, FMT_TRANS_QPBOC_BATCH_RECORD_FILE_NAME, DIR_QPBOC_BATCH_RECORD, MAX_TRANS_RECORD_NUM -1);

	ret = sys_lfs_file_open(&lfs_file, filename, LFS_O_RDONLY);
	if(ret == 0)
	{
		//File exist
		sys_lfs_file_close(&lfs_file);
		return 1;
	}
	else
	{
		//File not exist
		return 0;
	}
#endif
}
#endif

#if 0
void BatchDataCaptureBCTC()
{
	int sock;
	struct sockaddr_in addr;

	int i;
	int fd;
	char filename[MAX_FILE_PATH_LEN];
	char buf[1024*10];
	int len;
	char tmp[10];
	char ip[128];
    char port[10];
	int loop;
	long val;

	//LoadTerminalParam();

	ClearScreen();
	DisplayInfoCenter("Waitiing...", 4);
	
	sock = socket(AF_INET, SOCK_STREAM, 0);
	memset(&addr, 0x00, sizeof(struct sockaddr_in));

	addr.sin_family = AF_INET;

    GetServerIPAndPort(ip, port);
	LOGD("Issuer Batch Data Capture IP: %s, port : %s", ip, port);
	addr.sin_addr.s_addr = inet_addr(ip);
	//addr.sin_port = htons(10755);	
	addr.sin_port = htons(atoi(port));
	val =fcntl(sock, F_GETFL, 0);
	fcntl(sock, F_SETFL, val|O_NONBLOCK);

	loop = 0;
	while(loop < 20)
	{
		if(connect(sock, (struct sockaddr *)(&addr), sizeof(struct sockaddr)) != 0)
		{
			//DisplayValidateInfo("Connect Fail.");
			//return;
            LOGD("connect not OK");
            loop ++;
			continue;
		}else
		{
            LOGD("connect OK");
			break;
		}

	}
	if (loop >= 100)
	{
		DisplayValidateInfo("Connect Fail.");
		return;
	}

	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		sprintf(filename, FMT_TRANS_BATCH_RECORD_FILE_NAME, DIR_BATCH_RECORD, i);
		LOGD("filename = %s", filename);

		fd = open(filename, O_RDONLY);

		if(fd <0)
		{
			break;
		}

		len = sizeof(buf);
		len = read(fd, buf, len);

		if (len <= 0)
		{
			DisplayValidateInfo("Read Record Fail");
			close(sock);
			return;
		}
		
		sprintf(tmp, "%04d",len);
        LOGD("Batch send buf list");
        hexdump(buf, len);
		if(send(sock, buf, len, 0) <0)
		{
			LOGD("Send Fail.\n");
			DisplayValidateInfo("Batch Capture Fail");
			close(sock);
			return;
		}

		recv(sock, buf, 100, 0);
		
		close(fd);
	}

	if(i == 0)
	{
		DisplayValidateInfo("No Trans Record");
	}
	else
	{
		DisplayValidateInfo("Batch Capture Success");		
		ClearTransRecordRaw();
		ClearBatchTransRecordRaw();
	}
	
	close(sock);
}
#else
void BatchDataCaptureBCTC(void)
{	
	int sockfd;
	int i;
	//int fd;
	char filename[MAX_FILE_PATH_LEN];
	char buf[1024*6];
	int len;
	char tmp[10];
	char ip[128];
  char port[10];
	int ret;
	lfs_file_t file;
	
	//LoadTerminalParam();

	DisplayInfoCenter("处理中", 4);
	
    GetServerIPAndPort(ip, port);
	LOGD("Issuer Batch Data Capture IP: %s, port : %s", ip, port);

	sockfd = SocketConnect(ip, port, 10);
	if (sockfd < 0)
	{
		DisplayValidateInfo("Connect Host error");
		SocketDisconnect(sockfd);
		return;
	}
	
	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		DisplayInfoCenter("处理中", 4);
	
		sprintf(filename, FMT_TRANS_BATCH_RECORD_FILE_NAME, DIR_BATCH_RECORD, i);
		LOGD("filename = %s", filename);
#ifndef CY20_EMVL2_KERNEL
		fd = open(filename, O_RDONLY);
		if(fd <0)
		{
			break;
		}

		len = sizeof(buf);
		len = read(fd, buf, len);
		close(fd);
#else
		ret = sys_lfs_file_open(&file, filename, LFS_O_RDONLY);
		if (ret != 0)
		{
			break;
		}
		
		len = sys_lfs_file_read(&file, buf, sizeof(buf));
		sys_lfs_file_close(&file);
#endif
		if (len <= 0)
		{
			DisplayValidateInfo("Read Record Fail");
			SocketDisconnect(sockfd);
			return;
		}
		
		sprintf(tmp, "%04d",len);
        LOGD("Batch send buf list");
        hexdump((byte *)buf, len);

		if (SocketSendData(sockfd, (byte *)buf, len, 10*1000) != len)
		{
			LOGE("Send Fail.\n");
			DisplayValidateInfo("Batch Capture Fail");
			SocketDisconnect(sockfd);
			return;
		}

		 ret = SocketRcvData(sockfd, (byte *)tmp, 7, 10*1000);
     if (ret != 7)
		 {
					DisplayValidateInfo("Batch Capture Fail");
	        LOGE("SocketRcvData received 7 byte error");
			SocketDisconnect(sockfd);
	        return;
    	}

	
	}

	SocketDisconnect(sockfd);

	if(i == 0)
	{
		DisplayValidateInfo("无交易记录");
	}
	else
	{
		DisplayValidateInfo("批上送成功");		
		ClearTransRecordRaw();
		ClearBatchTransRecordRaw();
	}

}
#endif

void QpbocBatchDataCaptureBCTC()
{
#ifndef CY20_EMVL2_KERNEL		
	int sockfd;
	int i;
	int fd;
	char filename[MAX_FILE_PATH_LEN];
	char buf[1024*10];
	int len;
	char tmp[10];
	char ip[128];
    char port[10];
	int ret;
	
	//LoadTerminalParam();

	DisplayInfoCenter("Waitiing...", 4);
	

    GetServerIPAndPort(ip, port);
	LOGD("Issuer Batch Data Capture IP: %s, port : %s", ip, port);

	sockfd = SocketConnect(ip, port, 3);
	if (sockfd < 0)
	{
		DisplayValidateInfo("Connect Host error");
		SocketDisconnect(sockfd);
		return;
	}
	
	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		sprintf(filename, FMT_TRANS_QPBOC_BATCH_RECORD_FILE_NAME, DIR_QPBOC_BATCH_RECORD, i);
		LOGD("filename = %s", filename);

		fd = open(filename, O_RDONLY);

		if(fd <0)
		{
			break;
		}

		len = sizeof(buf);
		len = read(fd, buf, len);

		if (len <= 0)
		{
			DisplayValidateInfo("Read Record Fail");
			close(fd);
			SocketDisconnect(sockfd);
			return;
		}
		
		sprintf(tmp, "%04d",len);
        LOGD("Batch send buf list");
        hexdump(buf, len);

		if (SocketSendData(sockfd, buf, len, 10*1000) != len)
		{
			LOGE("Send Fail.\n");
			DisplayValidateInfo("Batch Capture Fail");
			close(fd);
			SocketDisconnect(sockfd);
			return;
		}

		 ret = SocketRcvData(sockfd, tmp, 7, 10*1000);
    	 if (ret != 7)
		 {
			DisplayValidateInfo("Batch Capture Fail");
	        LOGE("SocketRcvData received 7 byte error");
			close(fd);
			SocketDisconnect(sockfd);
	        return;
    	}

		close(fd);
	}

	SocketDisconnect(sockfd);

	if(i == 0)
	{
		DisplayValidateInfo("No Trans Record");
	}
	else
	{
		DisplayValidateInfo("Batch Capture Success");		
		ClearQpbocBatchTransRecordRaw();
	}
#else
	int sockfd;
	int i;
	char filename[MAX_FILE_PATH_LEN];
	char buf[1024*6];
	int len;
	char tmp[10];
	char ip[128];
    char port[10];
	int ret;
	lfs_file_t lfs_file;
	
	//LoadTerminalParam();

	DisplayInfoCenter("Waitiing...", 4);
	
    GetServerIPAndPort(ip, port);
	LOGD("Issuer Batch Data Capture IP: %s, port : %s", ip, port);

	sockfd = SocketConnect(ip, port, 3);
	if (sockfd < 0)
	{
		DisplayValidateInfo("Connect Host error");
		SocketDisconnect(sockfd);
		return;
	}
	
	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		sprintf(filename, FMT_TRANS_QPBOC_BATCH_RECORD_FILE_NAME, DIR_QPBOC_BATCH_RECORD, i);
		LOGD("filename = %s", filename);

		ret = sys_lfs_file_open(&lfs_file, filename, LFS_O_RDONLY);
		if (ret != 0)
		{
			break;
		}

		len = sizeof(buf);
		len = sys_lfs_file_read(&lfs_file, buf, len);
		sys_lfs_file_close(&lfs_file);
		
		if (len <= 0)
		{
			DisplayValidateInfo("Read Record Fail");
			SocketDisconnect(sockfd);
			return;
		}
		
		sprintf(tmp, "%04d",len);
        LOGD("Batch send buf list");
        hexdump((byte *)buf, len);

		if (SocketSendData(sockfd, (byte *)buf, len, 10*1000) != len)
		{
			LOGE("Send Fail.\n");
			DisplayValidateInfo("Batch Capture Fail");
			SocketDisconnect(sockfd);
			return;
		}

		 ret = SocketRcvData(sockfd, (byte *)tmp, 7, 10*1000);
    	 if (ret != 7)
		 {
			DisplayValidateInfo("Batch Capture Fail");
	        LOGE("SocketRcvData received 7 byte error");
			SocketDisconnect(sockfd);
	        return;
    	}
	}

	SocketDisconnect(sockfd);
	if(i == 0)
	{
		DisplayValidateInfo("No Trans Record");
	}
	else
	{
		DisplayValidateInfo("Batch Capture Success");		
		ClearQpbocBatchTransRecordRaw();
	}

	return;
#endif	
}

