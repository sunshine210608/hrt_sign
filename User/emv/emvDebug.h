#ifndef __EMV_DEBUG_H__
#define __EMV_DEBUG_H__

#include "user_projectconfig.h"

#ifdef ANDROID
#include <android/log.h>
#include "debug.h"

#define  TAG    "SITCEMVL2"
// 定义info信息

#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,TAG,__VA_ARGS__)
// 定义debug信息

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, TAG,__VA_ARGS__)
//#define LOGD(...) __android_log_print(ANDROID_LOG_ERROR, TAG,__VA_ARGS__)
// 定义error信息
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN,TAG,__VA_ARGS__)
//#define LOGW(...)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,TAG,__VA_ARGS__)

#define REPORT_ERR_LINE() LOGE("Error in File:%s, Fuc: %s, line: %d\n", __FILE__, __func__, __LINE__)
#else
#include <stdio.h>
#include "debug.h"
#define LOGI(x...)	dbg(x);dbg("\n")
#define LOGD(x...)	dbg(x);dbg("\n")
#define LOGW(x...)	dbg(x);dbg("\n")
#define LOGE(x...)	dbg(x);dbg("\n")

#define REPORT_ERR_LINE() LOGE("Error in File:%s, Fuc: %s, line: %d\n", __FILE__, __func__, __LINE__)
//#define ReportLine() LOGW("Now File:%s, Fuc: %s, line: %d\n", __FILE__, __func__, __LINE__)


#endif
#endif
