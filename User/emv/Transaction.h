#ifndef INCLUDE_TRANSACTION_H
#define INCLUDE_TRANSACTION_H

#include "OsTypes.h"

//bool IsEcTrans();

void Transaction(const char *TransType);

void EMVL2_Transaction_SetParaFromUI(int TransactionType, int AccountType, int Amount, int AmountCash);

int EMVL2_GetTransType(void);

int EMVL2_GetAccountType(void);

int EMVL2_GetAmount(void);

int EMVL2_GetAmountCash(void);

void SetTransactionResultBCTC(unsigned char setValue);

unsigned char GetTransactionResultBCTC(void);

void ReadLogTransaction(void);

int DoReadBalanceTransaction(void);

int DoReadLogTransaction(void);

void GetSelectAid(char *szSelectAID);

bool IsEcTrans(void);

int WaitForNFC(char *szPrompt, char *szDispAmt);

#endif
