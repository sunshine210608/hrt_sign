#include <string.h>
#include <stdio.h>
#include "mhscpu.h"
#include "ugui.h"
#include "lcd.h"
#include "keyboard.h"
#include "systick.h"
#include "common.h"
#include "lcdgui.h"
#include "rtc.h"
#include "displaybuf.h"
#include "test_menu.h"
#include "watchdog.h"
//#include "sysparam.h"
#include "user_projectconfig.h"
//#define dbg

#include "VposFace.h"
#include "pub.h"
#include "AppGlobal_cjt.h"

#ifdef ST7789
#define MAX_NUM_ONEPAGE     7       //每页菜单项数(每行单项菜单计)
#define LINE_COL_NUM        26      //行宽
#endif

#ifdef ST7567
#define MAX_NUM_ONEPAGE     4       //每页菜单项数(除去标题行后的行数)
#define LINE_COL_NUM        21      //行宽
#endif

#ifdef ST7571
#define MAX_NUM_ONEPAGE     5       //每页菜单项数(每行单项菜单计)
#define LINE_COL_NUM        21      //行宽
#endif

#ifdef LCD_GC9106
#define MAX_NUM_ONEPAGE     4       //每页菜单项数(除去标题行后的行数)
#define LINE_COL_NUM        20      //行宽
#endif

#define MENU_TIMEOUT		60		//60秒无按键则退出到待机界面


//SMenu menu_manual_test;

//SMenu *MAINMENU= &menu_manual_test;

extern void PMUModeCheck(void);
#ifdef APP_SDJ
extern SMenuItem smit_MainOpernHide;
#endif
#ifdef APP_LKL
extern SMenuItem menu_HardwareTestHide;
#endif
char ucExitToMainFlag=0;        //1-退到主界面


void vSetExitToMainFlag(char flag)
{
    ucExitToMainFlag=flag;
}

static void Display_Menu(SMenu *m)
{
	int i;
	char str[32], tmp[32];	
    SMenuItem *si;
	char num= 0;
	int iOnePageNum;
	int mode;

	mode=(m->mode&0x0F);
    //dbg("Display_Menu:m->mode=%02X,mode=%02X\n", m->mode, mode); 
	if(mode!=1 && mode!=2)
		mode=1;
	iOnePageNum=MAX_NUM_ONEPAGE*mode;
	if(iOnePageNum>10)
		iOnePageNum=10;

    for(i=m->cur-1;i<MAX_MENUITEM_NUMBER;i++)
    {
        si = &m->items[i];
        if(si->name==NULL){ 
            //dbg("last one item\n");
            break;
        }

		if(++num >=iOnePageNum) 
			break;
    }
    //dbg("menu num=%d, cur=%d\n",num, m->cur);
    
    //vDispCenter(1, m->name, 1);    
    memset(str, ' ', LINE_COL_NUM);
    str[LINE_COL_NUM]=0;
    memcpy(str+(LINE_COL_NUM-strlen(m->name))/2, m->name, strlen(m->name));
    if(strlen(m->name)+4<=LINE_COL_NUM)
    {
        if(m->cur-1 > 0)
        {
            memcpy(str, "↑", 2);
        }
        if(num>=iOnePageNum && m->items[iOnePageNum+m->cur-1].name)
        {
            memcpy(str+LINE_COL_NUM-2, "↓", 2);
        }
    }
    
    _vDispX(1, (unsigned char*)str, 1);

    
	for(i=0; (i*mode)<num; i++)
	{
		//dbg("%d name:[%s]\n", i+m->cur-1, m->items[i+m->cur-1].name);
		if(mode==1)
		{
			if(i<m->num)
			{
				sprintf(str, "%d.%s\n", (i+1), m->items[i+m->cur-1].name/*,Ir->pass,Ir->failed*/);//m->items[i].result->pass,m->items[i].result->failed
				LcdPutsl(str,i+1);
			}else
				LcdPutsc("                            ",i+1);
		}else
		{
			memset(str, ' ', LINE_COL_NUM);
			str[LINE_COL_NUM]=0;
			if(i*2 < m->num )
			{
                #ifndef ST7789
                sprintf(tmp, "%d.%s", (i*2+1), m->items[i*2+m->cur-1].name/*,Ir->pass,Ir->failed*/);
                #else
				sprintf(tmp, " %d.%s", (i*2+1), m->items[i*2+m->cur-1].name/*,Ir->pass,Ir->failed*/);
                #endif
				memcpy(str, tmp, strlen(tmp));
			}
			if((i*2+1 < m->num) && (i*2+1 < num))
				sprintf(str+LINE_COL_NUM/2+(LINE_COL_NUM>=21?1:0), "%d.%s", (i*2+1+1)==10?0:(i*2+1+1), m->items[i*2+m->cur-1+1].name/*,Ir->pass,Ir->failed*/);
			LcdPutsl(str, i+1);
		}
	}
    // TODO:
	//flush_lcd();
	
}


// Print the menu into a display buffer, which may not be the internal
// display buffer.
int MenuToBuffer(displaybuf_t *db,SMenu *m)
{
	LcdClear();
    //LcdPutsc(m->name,0);
	//vDispCenter(1, m->name, 1);
	Display_Menu(m);
	return 0;
}


// Print the menu on the screen, and return the pointer of menu item selected.
SMenuItem* MenuDisplay(SMenu *m)
{
    uint16_t keypad;
	uint64_t idleTimer;
	//FIXME:
	//int bytes_per_line = 320/16;//pfont->height * (plcd->pixelx / 8);

	//indecates the current selected menu item.
	char cursor = 0;
	int mode;
	
	int iOnePageNum;

	mode=(m->mode & 0x0F);
    //dbg("MenuDisplay:m->mode=%02X,mode=%02X\n", m->mode, mode);
	if(mode!=1 && mode!=2)
		mode=1;
	iOnePageNum=MAX_NUM_ONEPAGE*mode;
	if(iOnePageNum>10)
		iOnePageNum=10;

	if(m->num==0)
	{
		int i;
		for(i=0;i<MAX_MENUITEM_NUMBER && m->items[i].name!=NULL;i++)
			;
		m->num=i;
	}
	
	MenuToBuffer(NULL, m);

	idleTimer=get_tick()+MENU_TIMEOUT*1000;		//60秒
	KeyBoardClean();
	while(1)
	{
		mdelay(10);
        //WDTFeedDog();
    #if 0 //ENABLE_JTAG!=1
        PMUModeCheck();
    #endif
        
		if(get_tick()>idleTimer && gl_ucTimeOutFlag == 1)
		{
			vSetExitToMainFlag(1);
			return NULL;
		}

		if(KeyBoardRead(&keypad))
			continue;
		
		if(keypad == KEY_CANCEL)
		{
			//dbg("cancel\n");
			return NULL;
		}

		//有按键则重设超时
		idleTimer=get_tick()+MENU_TIMEOUT*1000;

        //dbg("key val=0x%x\n",keypad);
        if(keypad == KEY_1) cursor = 1;
        else if(keypad == KEY_2) cursor = 2;
        else if(keypad == KEY_3) cursor = 3;
        else if(keypad == KEY_4) cursor = 4;
        else if(keypad == KEY_5) cursor = 5;
        else if(keypad == KEY_6) cursor = 6;
        else if(keypad == KEY_7) cursor = 7;
        else if(keypad == KEY_8) cursor = 8;
        else if(keypad == KEY_9) cursor = 9;
		else if(keypad == KEY_0) cursor = 10;
		else if(keypad == KEY_F1){
            //dbg("keyup m->num=%d, iOnePageNum=%d, m->cur=%d\n", m->num, iOnePageNum, m->cur);
            if(m->num>iOnePageNum && m->cur>iOnePageNum)
			{
				m->cur-=iOnePageNum;
                m->cur = ((m->cur-1)/iOnePageNum) *iOnePageNum + 1;
                //if(m->cur > m->num)
                //    m->cur = 1;
				MenuToBuffer(NULL, m);	
            }
            #ifdef APP_LKL
            else if(m->mode&0x80)
            {
                return NULL;
            }
            #endif
            continue;
        }
        else if(keypad == KEY_F2){
            //dbg("keydown m->num=%d, iOnePageNum=%d\n", m->num, iOnePageNum);
            if(m->num>iOnePageNum)
			{
                m->cur = ((m->cur-1)/iOnePageNum + 1) *iOnePageNum + 1;
                if(m->cur > m->num)
                    m->cur = 1;
				MenuToBuffer(NULL, m);	
            }
            //dbg("change page to %d\n",m->cur);
            #ifdef APP_LKL
            else if(m->mode&0x40)
            {                
                return &menu_HardwareTestHide;
            }
            #endif
            continue;
        }else
            continue;
        #ifndef FUDAN_CARD_DEMO
		#ifdef APP_SDJ
		if(cursor==10 && (m->mode&0x80))
		{
			return &smit_MainOpernHide;
		}
        #endif
        #endif
        cursor += m->cur - 1;
        if(cursor > m->num)
            continue;
		dbg("cur=%d max=%d\n",cursor,m->num);
        break;
	}
	
	return &(m->items[cursor-1]);
}

extern void updateLastEventTimestamp(void);

int EnterTestMenu(SMenu * Mainmenu)
{
	SMenuItem *item = NULL;
	//SMenu *TopMenu=NULL;
	int num=0;
    //int ret;

	//get numbers of items
	for(item=Mainmenu->items;item->name!=NULL;item++){
		num++;
	}
	Mainmenu->num=num;
    
    Mainmenu->cur=1;
    
	//dbg("items=%d\n",Mainmenu->num);
	//TestResult_Init(Mainmenu,0);
	while(1)
	{
		item = MenuDisplay(Mainmenu);
       if(ucExitToMainFlag)
       {
            if(ucExitToMainFlag==1)
                updateLastEventTimestamp();
           break;
        }
		if(item == NULL){
            if(Mainmenu->topmenu){
                //dbg("Cancel key,return from sub\n");
                //use the top menu.
                Mainmenu = Mainmenu->topmenu;
                //TopMenu  = NULL;
                continue;
            }
            //dbg("Exit Main Menu,but not exit,free result\n");
            //ShowTestResult();
			break;
		}
       // dbg("item:%s\n",item->name);
        if(item->submenu != NULL){
            //dbg("has submenu\n");
            //save the top menu point
            //TopMenu = Mainmenu;
			item->submenu->topmenu=Mainmenu;
            Mainmenu = item->submenu;
            Mainmenu->cur=1;
        }else{
            /*ret = */ item->func((void*)item);
            //dbg("Result = %d\n",ret);
            //RecordResult(item,ret);
        }
        
       if(ucExitToMainFlag)
       {
           if(ucExitToMainFlag==1)
                updateLastEventTimestamp();
            break;
        }
	}

    //TestResult_DeInit();
    return 0;
}

#if 0
int TestManual(void *m)
{
	SMenuItem *item = NULL;
	SMenu *Mainmenu=MAINMENU,*TopMenu=NULL;
	int num=0;
	int ret;

	//get numbers of items
	for(item=Mainmenu->items;item->name!=NULL;item++){
		num++;
	}
	Mainmenu->num=num;
	dbg("items=%d\n",Mainmenu->num);
	//TestResult_Init(Mainmenu,0);
	while(1)
	{	
		item = MenuDisplay(Mainmenu);
        if(ucExitToMainFlag)
            break;
		if(item == NULL){
            if(TopMenu){
                dbg("Cancel key,return from sub\n");
                //use the top menu.
                Mainmenu = TopMenu;
                TopMenu  = NULL;
                continue;
            }
            dbg("Exit Main Menu,but not exit,free result\n");
            //ShowTestResult();
			break;
		}
        dbg("item:%s\n",item->name);
        if(item->submenu != NULL){
            dbg("has submenu\n");
            //save the top menu point
            TopMenu = Mainmenu;
            Mainmenu = item->submenu;
        }else{
		    ret = item->func((void*)item);
            dbg("Result = %d\n",ret);
            //RecordResult(item,ret);
        }
        if(ucExitToMainFlag)
            break;
	}

    //TestResult_DeInit();
    return 0;
}

int selectExitmenu(void){
    uint16_t key;
    LcdClear();
    LcdPutscc("Exit Or Test Next?");
    LcdPutsb(" EXIT         NEXT ");
    KeyBoardClean();

    key = 0;
    while(1){
        KeyBoardRead(&key);
        if(key==KEY_CANCEL)
            return EXIT;
        else if(key == KEY_OK)
            return NG;
    }
}

/**
 * @fn       int EnterAutoTestItems(SMenu * Mainmenu)
 * @brief    auto test all items in Mainmenu
 * 
 * @param[in]          SMenu * Mainmenu  
 * @return         int
 */
int EnterAutoTestItems(SMenu * Mainmenu){
#if 0    
    SMenuItem *item = NULL;
	int i=0;
	int ret;
    //TestResult_Init(Mainmenu,1);

    //register double cancel to exit loop test or skip current test item
    registerKeyboardEvent(KB_EVT_DOUBLECANCEL);
    //only support one level menu
    while(1){	
        LcdClear();
        //Line;
    	item = &Mainmenu->items[i++];
    	if(item->name == NULL){
            //last test item
    		break;
    	}
	    ret = item->func((void*)item);
        //exit test or continue next one item?
        if(ret == EXIT){
            ret = selectExitmenu();
            if(ret==EXIT)
                break;
        }
        //RecordResult(item,ret);
    }
    unregisterKeyboardEvent(KB_EVT_DOUBLECANCEL);
    
    //TestResult_DeInit();
    //Line;
#endif    
    return 0;
}

int lfs_test(void);

/*
void HardwareTestManual(void){
    EnterTestMenu(&menu_manual_test);
}
*/

int waitkey(void)
{
	uint16_t key;
	while(KeyBoardRead(&key))
		;
	return key;
}

int func1_1_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_1_2", 2);
	waitkey();
	return 0;
}
int func1_1_3(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_1_3", 2);
	waitkey();
	return 0;
}
int func1_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_2", 2);
	waitkey();
	return 0;
}
int func1_3(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_3", 2);
	waitkey();
	return 0;
}
int func2_1_1(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_1_1", 2);
	waitkey();
	return 0;
}
int func2_1_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_1_2", 2);
	waitkey();
	return 0;
}
int func2_1_3(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_1_3", 2);
	waitkey();
	return 0;
}
int func2_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_2", 2);
	waitkey();
	return 0;
}
int func2_3(void *p)
{
	LcdClear();
	LcdPutsl("this is func2_3", 2);
	waitkey();
	return 0;
}
int func1_1_1_1(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_1_1_1", 2);
	waitkey();
	return 0;
}

int func1_1_1_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_1_1_2", 2);
	waitkey();
	return 0;
}
int func3(void *p)
{
	LcdClear();
	LcdPutsl("this is func3", 2);
	waitkey();
	return 0;
}


SMenu menu_SubMemu1_1_1 =
{"Sub Menu1_1_1", 0, 1, 0,
	{
		{"Menu1_1_1_1-F", func1_1_1_1},
		{"Menu1_1_1_2-F", func1_1_1_2},
        NULL
	}
};


//二级子菜单1_1
SMenu menu_SubMemu1_1 =
{"Sub Menu1_1", 0, 1, 0,
	{
		{"Menu1_1_1-M", 0, &menu_SubMemu1_1_1},
		{"Menu1_1_2-F", func1_1_2},
        {"Menu1_1_3-F", func1_1_3},
        NULL
	}
};

//一级子菜单1
SMenu menu_SubMemu1 =
{"Sub Menu1", 0, 1, 0,
	{
		{"Menu1_1-M", 0, &menu_SubMemu1_1},
		{"Menu1_2-F", func1_2},
        {"Menu1_3-F", func1_3},
        NULL
	}
};

//二级子菜单2-1
SMenu menu_SubMemu2_1 =
{"Sub Menu1_1", 0, 1, 0,
	{
		{"Menu2_1_1-F", func2_1_1},
		{"Menu2_1_2-F", func2_1_2},
        {"Menu2_1_3-F", func2_1_3},
        NULL
	}
};

//一级子菜单2
SMenu menu_SubMemu2 =
{"Sub Menu2", 0, 1, 0,
	{
		{"Menu2_1-M", 0, &menu_SubMemu2_1},
		{"Menu2_2-F", func2_2},
        {"Menu2_3-F", func2_3},
        NULL
	}
};

//主菜单
SMenu menu_MainMemu =
{"Main Menu", 0, 1, 0,
	{
		{"Menu1-M", 0, &menu_SubMemu1},
		{"Menu2-M", 0, &menu_SubMemu2},
        {"Menu3-F", func3},
        NULL
	}
};

void testmenu(void){
    EnterTestMenu(&menu_MainMemu);
}
#endif
