
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lcdfont.h"
#include "printer.h"
#include "DataTrans.h"
#include "RespDataItem.h"
#include "utilPrint.h"
#include "debug.h"
#include "mhscpu.h"
#include "msr.h"
#include "define.h"
#include "lcdfont.h"
#include "printer.h"
#include "debug.h"
#include "systick.h"
#include "lcdgui.h"

extern unsigned char font_12x20[256][40];
extern const struct font_desc Font_12x20;
extern const struct font_desc Font_12x24;
extern const struct font_desc Font_24x48;
extern const struct font_desc Font_18x36;

#if PROJECT_CY20  == 0
#ifndef CY20_EMVL2_KERNEL
	char gszPrintBuffer[1024*5];
#endif

int PrintHeader(char *info)
{
    return 0;
}
int PrintFooter(void)
{
    return 0;
}
int TestPrinter_normal(void *m) 
{
    return 0;
}
int NJRCBase_Print_ClearBuffer(void)
{
	return 0;
}
int NJRCBase_Print_FillBuffer(char * szBuffer)
{
	return 0;
}
void NJRCBase_Print_Prepare(int depth)
{
    return;
}
int NJRCBase_Print_StartPrint(void)
{
    return 0;
}
#else
int PrintHeader(char *info)
{
	NJRCBase_Print_FillBuffer(info);
	NJRCBase_Print_FillBuffer("\n\n\n");

	return STATUS_OK;
}

int PrintFooter(void)
{	
	NJRCBase_Print_FillBuffer("\n            Approved");
	NJRCBase_Print_FillBuffer("\n\n\n\n");
	NJRCBase_Print_FillBuffer("\n\n\n\n");
	NJRCBase_Print_FillBuffer("\n\n\n\n");
	NJRCBase_Print_FillBuffer("\n\n\n\n");
	
	return STATUS_OK;
}


/**************************************************************************************************************************************/
int NJRCBase_Print_ClearBuffer(void)
{

	return 0;
}

int NJRCBase_Print_FillBuffer(char * szBuffer)
{
	PrintPuts(szBuffer);
	return 0;
}

int NJRCBase_Print_StartPrint(void)
{
	int ret;

	dbg("before PrintWrite\n");
	ret = PrintWrite();
	dbg("PrintWrite ret = %d\n", ret);
	
	PrintClose();
	return 0;
}

int NJRCBase_Print_Cut(void)
{	
	return 0;
}

void print_test(void);


void NJRCBase_Print_Prepare(int depth)
{
    //struct printer_info info;
    //tick  tks,tkn,tkt;
    char date[48];
	//struct font_desc *font_p = (struct font_desc *)&Font_18x36;
    struct font_desc *font_p = (struct font_desc *)&Font_12x24;
    //char ptwid = 384/font_p->width;
    //struct tm gettm;
    
	//print_test();
		
    int ret;    

    memset(date, 0, sizeof(date));
    ret= PrintOpen();
    if (ret < 0) {
        dbg("printer open error %d",ret);
        return;
    }

	PrintSetDepth(depth);

    PrintLoadFont(font_p);

	//PrintPuts("   ");
	//ret = PrintWrite();
	//dbg(" ret = %d\n", ret);
	//PrintClose();
	

}
/**************************************************************************************************************************************/

/********************************************************************************

 *************************Copyright (C), 2020, KingShiDun Inc.**************************

 ********************************************************************************
 * @file     	 : test_printer.c
 * @brief   		  : .C file function description
 * @author       : Jay
 * @version        : 1.0
 * @date          : 2020-01-16
 * 
 * 
 * @note History:
 * @note        : Jay 2020-01-16
 * @note        : 
 *   Modification: Created file

********************************************************************************/




void printoutSpeedInfo(int ptline,int tkt){
    char tmp[32];
    int ret,depth;
    struct font_desc *font_p = (struct font_desc *)&Font_12x20;
    if(PrintOpen()){
        dbg("open print error\n");
        return;
    }
    PrintLoadFont(font_p);
    
    PrintPutsc("12345");
    {
        sprintf(tmp,"%dline,%dms,%dpps\n",ptline/ONELINEDOTSBYTES,tkt,((ptline/ONELINEDOTSBYTES)*2*1000)/tkt);
        LcdPutscc(tmp);dbg(tmp);
        PrintPutsc(tmp);

        sprintf(tmp, "%s:%dmm/s\n","12345", ((ptline/ONELINEDOTSBYTES)*125)/tkt);
        LcdPutscc(tmp);dbg(tmp);
        PrintPutsc(tmp);
    }
    
    ret = PrintGetTemp();
    if (ret > 0) {
        sprintf(tmp, "%s:%d.C\n","12345", ret);
        PrintPuts(tmp);
    }

    ret = PrintGetVoltage();
    if (ret > 0) {
        memset(tmp, 0, sizeof(tmp));
        sprintf(tmp, "%s:%d mv\n", "12345",ret);
        PrintPuts(tmp);
    }
    dbg("battery=%dmv\n",ret);
    memset(tmp, 0, sizeof(tmp));
    sprintf(tmp, "%s %dms\n","12345", tkt);
    PrintPuts(tmp);

    ret= PrintGetDepth(&depth);
    if (ret == 0) {
        memset(tmp, 0, sizeof(tmp));
        sprintf(tmp, "%s:%d us\n","12345", depth);
        dbg(tmp);
        PrintPuts(tmp);
    }

    


    PrintPutsc(" ");
    PrintPutsc(" ");
    PrintPutsc(" ");
    PrintPutsc(" ");
    PrintPutsc(" ");

    dbg("print result\n");
    if (PrintWrite() < 0)
        dbg("printer error");
    dbg("done\n");

    PrintClose();
}

int PTnumber = 0;

int checkPrinterState(void){
    int stat = 0;
    int ln = 4;
    stat = PrintGetStatus();
    if(stat){
        if(stat & PRINTER_NOPAPER_MASK)
            LcdPutsc("12345",ln++);
        if(stat & PRINTER_HIGHTEMP_MASK)
            LcdPutsc("12345",ln++);
        if(stat & PRINTER_80C_60C_MASK)
            LcdPutsc("12345",ln++);
        if(stat & PRINTER_Writing_MASK)
            LcdPutsc("12345",ln++);
        if(stat & PRINTER_BATTERY_MASK)
            LcdPutsc("12345",ln++);  
        if(stat & PRINTER_SENSOR_MASK)
            LcdPutsc("12345",ln++);  
        
        mdelay(1500);
    }
    return stat;
}

int testpapercheck(void*m){
	#if 0
    int stat,laststa=-1;
    int ln = 4;
    char nopaper = 0; //no paper test flag
    char cnt = 0;

    ltp245_init();
    stat = PrintGetStatus();
    
    do{
        CHECKTIMEOUT;
        if(readKeyboardEvent(KB_EVT_DOUBLECANCEL))
            return EXIT;
        
        ln = 4;
        stat = PrintGetStatus();
        if(cnt && !stat){
            if(cnt++>=5)
                return 0;  // test pass
            mdelay(300);dbg("cnt=%d\n",cnt);
        }
        if(stat == laststa) continue;
        
        LcdCleanLine(ln++);
        LcdCleanLine(ln++);
        LcdCleanLine(ln++);
        LcdCleanLine(ln++);
        LcdCleanLine(ln++);
        LcdCleanLine(ln++);
        laststa = stat;

        if(stat) cnt = 0;
        
        ln = 4;
        if(stat == PRINTER_NOPAPER_MASK){
            LcdPutsc(PTNoPaper_Str,ln++);
            LcdPutsc(PTPutpaper_Str,ln++);
            nopaper = 1;
        }else if(stat==0){
            LcdPutsc(PTDetpaper_Str,ln++);
            if(nopaper==0)
                LcdPutsc(PTRmpaper_Str,ln++);
            else{
                cnt = 1;
            }
        }else{
            if(stat & PRINTER_NOPAPER_MASK)
                LcdPutsc(PTNoPaper_Str,ln++);
            if(stat & PRINTER_HIGHTEMP_MASK)
                LcdPutsc(PTHighTemp_Str,ln++);
            if(stat & PRINTER_80C_60C_MASK)
                LcdPutsc(PT6080_Str,ln++);
            if(stat & PRINTER_Writing_MASK)
                LcdPutsc(PTBusy_Str,ln++);
            if(stat & PRINTER_BATTERY_MASK)
                LcdPutsc(PTLowbat_Str,ln++);
            if(stat & PRINTER_SENSOR_MASK)
                LcdPutsc(SensorDete_Str,ln++);  
        }
    }while(1);
		
		#else
			return 0;
		#endif
}

int test_printer_normal(int setdepth) {
    //char tmp[4800];
    //struct printer_info info;
    //tick  tks,tkn,tkt;
    char date[48];
    struct font_desc *font_p = (struct font_desc *)&Font_12x20;
    //char ptwid = 384/font_p->width;
    //struct tm gettm;
    
    int ret,i;    

    memset(date, 0, sizeof(date));
    ret= PrintOpen();
    if (ret < 0) {
        dbg("printer open error %d",ret);
        return -1;
    }

    //check status
    ret = checkPrinterState();
    if(ret){
        PrintClose();
        return -2;
    }
    
    PrintSetDepth(setdepth);

    PrintLoadFont(font_p);

	//return 0;

    ///////////////////////////////////////////////////
//    PrintImage((unsigned char*)gImage_xp, 48*60);
////    //
////    memset(tmp,0,sizeof(tmp));
////    for(i=0,d=1;i<sizeof(tmp);d++){
////        for(j=0;j<10;j++,i+=48){
////            memset(tmp+i,0xff,1*d);
////        }
////    }
////    PrintImage((unsigned char*)tmp, 48*100);
//    PrintClose();
//    return 0;
    ////////////////////////////////////////////////////
    #if 1
    //PrintPutImage((unsigned char*)gImage_jsdlogo,sizeof(gImage_jsdlogo));
    //memset(tmp,0,sizeof(tmp));
    //sprintf(tmp, "Date:%s\n", COMPILE_TIME);
    //PrintPuts(tmp);

	i = 0;
	while(i++ < 1)
    {
    	dbg("i = %d\n", i);
    	PrintPuts("   ");
	}
	ret = PrintWrite();
	dbg(" ret = %d\n", ret);
	PrintClose();
	return 0;
#endif	
	
#if 0  
    sprintf(tmp, "No: PB30#####%08d", PTnumber);
    PTnumber += 1;
    PrintPuts(tmp);
//    PrintPuts(" ");
//    rtc_gettime(&gettm);
//    sprintf(date,"%s:%d %d %d %d:%d:%d\n",DateTime_Str,gettm.tm_year+1900,gettm.tm_mon+1,gettm.tm_mday,gettm.tm_hour,gettm.tm_min,gettm.tm_sec);
//    PrintPuts(date);
    
//    PrintPuts("Cardholder Name: JSD");
//    PrintPuts("Card barance: $518888888");
//    PrintPuts("Issuer ID: 88888888");
//    PrintPuts("Issuer date: 2019-10-23");
//    PrintPuts("Issuer expiry: 2019-10-23");
//    PrintPuts("Issuer ID: 123456789ABCDEF");
//    PrintPuts("Card points: 0123456");
//    PrintPuts(" ");
//#ifndef ZHCN
//    PrintPuts("Wares:");
//    PrintPuts("ITAL PARMESAN     $5.67");
//    PrintPuts("TOMATO PUREE      $6.88");
//    PrintPuts("DRIED PINEAPPLE   $20.16");
//    PrintPuts("SKIMMED MILK      $10.06");
//    PrintPuts("BREADED HAM       $12.00");
//    PrintPuts("COCA-COLA         $11.56");
//    PrintPuts("TERMINAL CY20    $999.99");
//    PrintPuts(" ");
//#endif
    PrintPuts("         TEL:123-456-789");
    PrintPuts("KingShiDun Tech Co,.Ltd.");
    PrintPutsc("www.kingshidun-tech.cn");
//    PrintPuts("                                ");
//    PrintPuts("Start print 3lines white space  ");
//    PrintPuts("                                ");
//    PrintPuts("                                ");
//    PrintPuts("                                ");
    PrintPutsc("Start print Black Block");

 //   #else
    PrintPuts("-");
    PrintPuts("--");
    PrintPuts("---");
    PrintPuts("----");
    PrintPuts("-----");
    PrintPuts("------");
    PrintPuts("-------");
    PrintPuts("--------");
    PrintPuts("--------");
    PrintPuts("-------");
    PrintPuts("------");
    PrintPuts("-----");
    PrintPuts("----");
    PrintPuts("---");
    PrintPuts("--");
    PrintPuts("-");
//    for(i=0;i<10;i++){
//        PrintPuts("-");
//    }
//    for(i=0;i<10;i++){
//        PrintPuts("--------");
//    }
    
    
    
    
    
//    PrintPuts("=");
//    PrintPuts("==");
//    PrintPuts("===");
//    PrintPuts("====");
//    PrintPuts("=====");
//    PrintPuts("======");
//    PrintPuts("=======");
//    #endif
    //print black block
    memset(tmp,0xff,sizeof(tmp));
    for(i=0;i<20;i++)
        PrintPutImage((unsigned char*)tmp,96);
    
    for(i=0x20,j=0;i<127;i++){
        date[j] = i;
        if(j >= ptwid -1){
            PrintPuts(date);
            memset(date,0,sizeof(date));
            j=0;
        }
        else
            j++;
    }
    if(j) PrintPuts(date);
    

    //print chn
    #ifdef ZHCN
    {
        char *zhcndata = "的一是了我不人在他有这个上们来到时大地为子中你说生国年着就那和要她出也得里后自以会家可下而过天去能对小多然于心学么之都好看起发当没成只如事把还用第样道想作种开美总从无情己面最女但现前些所同日手又行意动方期它头经长儿回位分爱老因很给名法间斯知世什两次使身者被";
        PrintPutsc("简体中文汉字打印测试");
        
        for(i=0;i<strlen(zhcndata);i+=ptwid){
            memcpy(date,zhcndata+i,ptwid);
            PrintPuts(date);
        }
    }
    #endif
    PrintPutsc(" ");
    
    ret = PrintGetVoltage();
    dbg("start print %dmv\n",ret);

    tks = get_tick();
    ret = PrintWrite();
    tkn = get_tick();
    tkt = tkn - tks;

//    out:

    PrintClose();
    
    printoutSpeedInfo(ret,tkt);
    return 0;
#endif
}
//
//int test_printer_sp(int setdepth) {
//    char tmp[4800];
//    //struct printer_info info;
//    tick  tks,tkn,tkt;
//    char date[32];
//    struct font_desc *font_p = (struct font_desc *)&font_16x32;
//    long ret;
//    int i,j,d;
//    
//
//    memset(date, 0, sizeof(date));
//    ret= PrintOpen();
//    if (ret < 0) {
//        dbg("printer open error %d",ret);
//        return -1;
//    }
//    PrintSetDepth(setdepth);
//
//    PrintLoadFont(font_p);
//
//    ///////////////////////////////////////////////////
//    PrintImage((unsigned char*)gImage_hz, sizeof(gImage_hz));
////    //
////    memset(tmp,0,sizeof(tmp));
////    for(i=0,d=1;i<sizeof(tmp);d++){
////        for(j=0;j<10;j++,i+=48){
////            memset(tmp+i,0xff,1*d);
////        }
////    }
////    PrintImage((unsigned char*)tmp, 48*100);
//    PrintClose();
//    return 0;
//    ////////////////////////////////////////////////////
//    #if 0
//    PrintPutImage(gImage_jsdlogo,sizeof(gImage_jsdlogo));
//    
//    PrintPutsc("KingShiDun");
//    memset(tmp,0,sizeof(tmp));
//    //sprintf(tmp, "Date:%s\n", COMPILE_TIME);
//    //PrintPuts(tmp);
//    PrintPutsc(" ");
//
//    sprintf(tmp, "No: PB30#####%08d", PTnumber);
//    PTnumber += 1;
//    PrintPuts(tmp);
//    PrintPuts(" ");
//    PrintPuts("Cardholder Name: JSD");
//    PrintPuts("Card barance: $518888888");
//    PrintPuts("Issuer ID: 88888888");
//    PrintPuts("Issuer date: 2019-10-23");
//    PrintPuts("Issuer expiry: 2019-10-23");
//    PrintPuts("Issuer ID: 123456789ABCDEF");
//    PrintPuts("Card points: 0123456");
//    PrintPuts(" ");
//    PrintPuts("Wares:");
//    PrintPuts("ITAL PARMESAN     $5.67");
//    PrintPuts("TOMATO PUREE      $6.88");
//    PrintPuts("DRIED PINEAPPLE   $20.16");
//    PrintPuts("SKIMMED MILK      $10.06");
//    PrintPuts("BREADED HAM       $12.00");
//    PrintPuts("COCA-COLA         $11.56");
//    PrintPuts("TERMINAL CY20    $999.99");
//    PrintPuts(" ");
//    PrintPuts("         TEL:123-456-789");
//    PrintPuts("KingShiDun Tech Co,.Ltd.");
//    PrintPutsc("www.kingshidun-tech.cn");
//    PrintPuts("                                ");
//    PrintPuts("Start print 3lines white space  ");
//    PrintPuts("                                ");
//    PrintPuts("                                ");
//    PrintPuts("                                ");
//    PrintPutsc("Start print Black Block");
//
//    #else
//    PrintPuts("-");
//    PrintPuts("--");
//    PrintPuts("---");
//    PrintPuts("----");
//    PrintPuts("-----");
//    PrintPuts("------");
//    PrintPuts("-------");
//    PrintPuts("--------");
////    PrintPuts("=");
////    PrintPuts("==");
////    PrintPuts("===");
////    PrintPuts("====");
////    PrintPuts("=====");
////    PrintPuts("======");
////    PrintPuts("=======");
//    #endif
//    //print black block
////    memset(tmp,0xff,sizeof(tmp));
////    for(i=0;i<40;i++)
////        PrintPutImage(tmp,96);
//    
//    PrintPuts("                                ");
//    PrintPuts("                                ");
//    PrintPuts("                                ");
//    
//    ret = PrintGetVoltage();
//    dbg("start print %dmv\n",ret);
//
//    tks = get_tick();
//    ret = PrintWrite();
//    tkn = get_tick();
//    tkt = tkn - tks;
//
//    out:
//
//    PrintClose();
//    
//    //printoutSpeedInfo(ret,tkt);
//    return 0;
//}


void test_printer_black(int depth){
    int ret,cnt=4800;
    unsigned char *buffer;
    tick tks,tkt;
    ret= PrintOpen();
    if (ret < 0) {
        dbg("printer open error");
        return ;
    }
    buffer = malloc(cnt);
    
    PrintSetDepth(depth);

    dbg("\nstart print all black\n");
    //2.print all black
    memset(buffer,0xff,cnt);

    tks = get_tick();
    ret=PrintImage(buffer,cnt);
    tkt = get_tick()-tks;
    PrintClose();
    free(buffer);

    printoutSpeedInfo(ret,tkt);
}

int TestPrinter_normal(void *m)
{
    int ret;
	/*
    ret = testpapercheck(m);
    if(ret){
        dbg("exit printer test %d\n",ret);
        return ret;
    }
    */
    
    ret = test_printer_normal(5);
    if(!ret)
        return 0;
    else if(ret == 1)
        return 1;
    return 1;
}

int TestPrinter_StressNormal(void*m){
    int ret = test_printer_normal(5);
    if(ret) return ret;
    mdelay(5000);
    return 0;
}

//int TestPrinter_sp(void *m)
//{
//    if(!test_printer_sp(500))
//        return MJ;
//    return NG;
//}

/*
print out all blank and stop at 65 c
*/
int TestPrinter_hightemp(void *m)
{
    int ret,cnt=4800*4,depth=500;
    unsigned char *buffer;
    tick tks,tkt;
    
    buffer = malloc(cnt);
    dbg("\nstart print all black\n");
    //2.print all black
    memset(buffer,0xff,cnt);

    //LcdPutscc("Test Printer high Temperature");
    do{
        //if(readKeyboardEvent(KB_EVT_DOUBLECANCEL))
        //    break;
        
        //check status
        ret = checkPrinterState();
        if(ret){
            break;
        }
        
        ret=PrintOpen();
        if (ret < 0) {
            dbg("printer open error");
            break ;
        }
        PrintSetDepth(depth);
        
        tks = get_tick();
        ret=PrintImage(buffer,cnt);
        if(ret<0) break;
        tkt = get_tick()-tks;
        PrintClose();
        
        printoutSpeedInfo(ret,tkt);
        
    }while(1);
    PrintClose();
    free(buffer);
    dbg("ret=%d\n",ret);
    if(ret== PRINTER_ERROR_HIGHTEMP ){
        dbg("detect high temp\n");
        LcdPutscc("12345");
        mdelay(1000);
        return 1;
    }
    return 0;
    
}


int TestPrinter_diffVol(void *m)
{
    test_printer_normal(500);
    //confirmTheTest(TEST_IDX, char * msg);
		return 0;
}

#endif
