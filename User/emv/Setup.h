#ifndef INCLUDE_SETUP_H
#define INCLUDE_SETUP_H

#include "Define.h"


void Setup(void);
int GetParamServIP(char buf[], int len);
int GetParamKerServIP(char buf[], int len);
int GetParamLocalIP(char buf[], int len);
int GetParamKerServPort(char buf[], int len);
int GetDownloadParamTimeout(void);
//int IsForceOnline();
#endif

