#ifndef __EMV_TEST_MENU_H__
#define __EMV_TEST_MENU_H__




#define MAX_MENUITEM_NUMBER		15
#define MAX_ITEM_NAME_LEN       16

typedef struct itemresult_t{
	char name[MAX_ITEM_NAME_LEN];
	int pass;
	int failed;
	int total;
    char lastresult;
	char autotest;
} ItemResult;

typedef struct menu_t SMenu;

typedef int (*VFUNC)(void *);
typedef struct menuitem_t {
	char *name;		// menu item's name.
	VFUNC func;		// handle function for this menu item.
	SMenu *submenu;		// submenu for this menu item.
	ItemResult *result;
} SMenuItem;

typedef struct menu_t {
	char *name;		// menu's name.
	int   num;		// menu items' number in this menu.

	// Following two members is used to record menu status, they are used
	// only at runtime.
	int   cur;			/* current selected item.
						 * Range: 1 -- MAX_MENUITEM_NUMBER.
						 *just for display
						 */
	int   mode/*startline*/;	/* Start line to display, indecates the menu item
						 * displayed on the top line of the LCD.
						 * Range: 0 -- MAX_MENUITEM_NUMBER.
						 * just for running
						 */
						/*原为startline未使用,改为mode,取值1-每行单项菜单 2-每行两项菜单 若高4位非0,则表示有隐含菜单*/
	// Menu items
	SMenuItem items[MAX_MENUITEM_NUMBER];
	SMenu *topmenu;
} SMenu;

enum{
    OK,
    NG,
    MJ,  //Manual judgement
    EXIT  // exit menu.user select NG or exit test
};

#define AUTOTEST_MODE       (((SMenuItem*)m)->result->autotest == 1)

int TestManual(void *m);
SMenuItem* MenuDisplay(SMenu *m);
void HardwareTestManual(void);
int EnterAutoTestItems(SMenu * Mainmenu);
int EnterTestMenu(SMenu * Mainmenu);


#endif

