#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "mhscpu.h"
#include "halmain.h"
#include "Util.h"
#include "Define.h"
#include "ReqDataItem.h"
#include "ReqDataSet.h"
#include "Idle.h"
#include "RespParse.h"
#include "emvDebug.h"
#include "sys_littlefs.h"
#include "Ugui.h"
#include "Des_sec.h"
//#include "sysparam.h"

#include "ProjectConfig.h"
#if USE_RTOS == 1
#include <FreeRTOS.h>
#include <task.h>
#include "rtos_tasks.h"
#endif
#include "SysInit.h"
#include "rtc.h"
#include "lcd.h"
#include "keyboard.h"
#include "pmu.h"
#include "common.h"
#include "pinpad.h"
#include "gprs_at.h"
#include "VposFace.h"
#include "sensor.h"
#include "sysTick.h"
#include "comm.h"
#include "utilPrint.h"
#include "pub.h"
#include "customize_emvl2.h"
#include "gprs.h"
#include "gprs_at.h"
#include "tms_flow.h"
#include "task_wdt.h"
#include "vposface.h"
#include "tcpcomm.h"
#include "StartupPic.h"
#include "battery.h"

//CY21/CY80没有单独功能键(功能键与UP键一体),使用_KEY_FN时用_KEY_UP代替
#if defined(PROJECT_CY21) || defined(PROJECT_CY80)
#undef _KEY_FN
#define _KEY_FN _KEY_UP
#endif
extern void MyApplication(void);

void CreateConfDir(void)
{
#ifndef CY20_EMVL2_KERNEL
    FILE *fp;
    char szTmp[10] = {0};
    static char filefullpath[1024];
    int ret;
		char tmp[MAX_FILE_PATH_LEN + 64];
	
	sprintf(tmp, "mkdir -p %s", DIR_CONF);
	ret = system(tmp);
    LOGD("system[%s] return %d\n", tmp, ret);
    sprintf(tmp, "mkdir -p %s", DIR_TRANS_RECORD);
	system(tmp);
    LOGD("system[%s] return %d\n", tmp, ret);

    sprintf(filefullpath, FMT_TRANS_RECORD_FILE_NAME, DIR_TRANS_RECORD, 2);

    fp = fopen("/data/data/com.szsitc.jay.sitc_emvl2/456", "r");
    if (fp == NULL)
    {
        LOGD("system fopen error");
    }else{
        LOGD("system fopen ok");
        ret = fread(szTmp, 4, 1, fp);
        fclose(fp);
        if (ret >= 0)
        {
            LOGD("system fread ok,ret = %d, data = [%s]",ret, szTmp);
        }else
        {
            LOGD("system fread error");
        }
    }
#else
	int ret;
	
	dbg("CreateConfDir()\n");

	ret = sys_lfs_mkdir(DIR_EMVL2);
	if (ret != 0)
	{
		if (ret == LFS_ERR_EXIST)
		{
			LOGD("%s already exist\n", DIR_EMVL2);
		}else
		{
			LOGD("%s sys_lfs_mkdir ERR return = %d\n", DIR_EMVL2, ret);
			return;
		}	//return;
	}

	ret = sys_lfs_mkdir(DIR_CONF);
	if (ret != 0)
	{
		if (ret == LFS_ERR_EXIST)
		{
			LOGD("%s already exist\n", DIR_CONF);
		}else
		{
			LOGD("%s sys_lfs_mkdir ERR return = %d\n", DIR_CONF, ret);
			return;
		}	//return;
	}

	ret = sys_lfs_mkdir(DIR_TRANS_RECORD);
	if (ret != 0)
	{
		if (ret == LFS_ERR_EXIST)
		{
			LOGD("%s already exist\n", DIR_TRANS_RECORD);
		}else
		{
			LOGD("%s sys_lfs_mkdir ERR return = %d\n", DIR_TRANS_RECORD, ret);
			return;
		}	//return;
	}

	ret = sys_lfs_mkdir(DIR_BATCH_RECORD);
	if (ret != 0)
	{
		if (ret == LFS_ERR_EXIST)
		{
			LOGD("%s already exist\n", DIR_BATCH_RECORD);
		}else
		{
			LOGD("%s sys_lfs_mkdir ERR return = %d\n", DIR_BATCH_RECORD, ret);
			return;
		}	//return;
	}

	ret = sys_lfs_mkdir(DIR_QPBOC_BATCH_RECORD);
	if (ret != 0)
	{
		if (ret == LFS_ERR_EXIST)
		{
			LOGD("%s already exist\n", DIR_QPBOC_BATCH_RECORD);
		}else
		{
			LOGD("%s sys_lfs_mkdir ERR return = %d\n", DIR_QPBOC_BATCH_RECORD, ret);
			return;
		}
	}

	ret = sys_lfs_mkdir(DIR_UPDATE);
	if (ret != 0)
	{
		if (ret == LFS_ERR_EXIST)
		{
			LOGD("%s already exist\n", DIR_UPDATE);
		}else
		{
			LOGD("%s sys_lfs_mkdir ERR return = %d\n", DIR_UPDATE, ret);
			return;
		}
	}

	//sys_lfs_dir_list("./");

	return;
#endif
}



extern const UG_FONT FONT_12X20;

void print_test(void)
{
	TestPrinter_normal(NULL);
}

//进入维修菜单:取消键+向下箭头键+功能键+188
//#define FACTORY_REPAIR_PWD	"FF888"
//#define FACTORY_REPAIR_PWD	"DF188"
#define FACTORY_REPAIR_PWD	"2580"
extern void vRepairPos(void);
void vCheckTamper(void)
{
	int sta;
	uint uiKey, len;
	char szRepairPwd[10+1]={0};
	tick tk=0;
	ulong ulTimeOutms=2000;
    int iAutoFlag=-1;

#ifdef NO_SEC_FORDEMO    
    return;
#endif


#ifdef DISABLE_ALLSENSOR
	return;
#endif
	//_vFlushKey();
	while(1)
	{
        //PMUModeCheck();
#ifndef JTAG_DEBUG   
        if (checkIfSensorEnabled() == 0)
#else
		if(0)
#endif
        {
            /*
            _vCls();
            vDispCenter(3, "请先开启Tamper", 0);
            while(1)
            {
                mdelay(10);
            }
            */
            sta=9;
        }else
            sta=GetTamperStatus();
        
		if(sta==0)
			return;
		
		if(tk==0)
		{
			tk=get_tick();
			_vCls();
			vDispCenter(3, "请联系服务提供商", 0);
		}
		//vDispVarArg(5, "TamperSta:%d", sta);

		switch (sta)
		{
			case TAMPER_STATUS_TAMPERING:
				dbg("Tampering status\n");
				//vDispCenter(2, "请先修复POS并重启", 0);
				//vDispCenter(2, "系统故障11", 0);
                vDispCenter(2, "机器故障：10000", 0);
                vDispCenter(3, "(0x00000001)", 0);
                vDispCenter(4, "code:11", 0);
                iAutoFlag=-1;
				break;
			case TAMPER_STATUS_TAMPED:
                if(iAutoFlag==-1)
                {
                    iAutoFlag=checkIfBPKReset()?1:0;
                }
                if(iAutoFlag==1)
                {
                    extern int iAutoRepairPos(void);
                    iAutoRepairPos();
                    iAutoFlag=0;
                }
				dbg("Tampering status\n");
				//vDispCenter(2, "请先修复POS并重启", 0);
				//vDispCenter(2, "系统故障12", 0);	
                vDispCenter(2, "机器故障：10000", 0);
                vDispCenter(3, "(0x00000001)", 0);
                vDispCenter(4, "code:12", 0);
				break;
			case TAMPER_STATUS_CONFIG_ERR:
				dbg("Error Status: Tamper configure error\n");
				//vDispCenter(3, "请重刷LFS系统1", 0);
				//vDispCenter(2, "系统故障13", 0);
                vDispCenter(2, "机器故障：10000", 0);
                vDispCenter(3, "(0x00000002)", 0);
                vDispCenter(4, "code:13", 0);
				break;
			case TAMPER_STATUS_OTHER_ERR:
				dbg("Error Status: Tamper configure error\n");
				//vDispCenter(3, "请重刷LFS系统2", 0);
				//vDispCenter(2, "系统故障14", 0);
                vDispCenter(2, "机器故障：10000", 0);
                vDispCenter(3, "(0x00000004)", 0);
                vDispCenter(4, "code:14", 0);
				break;
            case 9:
                dbg("Error Status: Tamper detection is not running.\n");
                //vDispCenter(2, "系统故障19", 0);
                vDispCenter(2, "机器故障：10000", 0);
                vDispCenter(3, "(0x00000008)", 0);
                vDispCenter(4, "code:19", 0);
                #if  0//PROJECT_CY20==1
                while(1)
                {
                    mdelay(10);
                }
                #endif
				break;
			default:
				break;
		}
		if(sta!=TAMPER_STATUS_TAMPED && sta!=9)
		{
			mdelay(ulTimeOutms);
			continue;
		}
		//若为TAMPER_STATUS_TAMPED则检测是否按键进入激活菜单
        //取消键+2580
		memset(szRepairPwd, 0, sizeof(szRepairPwd));
		uiKey=0;
		tk=get_tick();
        while(get_tick()<tk+ulTimeOutms)
        {
            if(_uiKeyPressed())
			{
				tk=get_tick();
				uiKey=_uiGetKey();
				len=strlen(szRepairPwd);
				if(len==0 && (uiKey==_KEY_ESC || uiKey==_KEY_FN || uiKey==_KEY_DOWN))
					_vDisp(_uiGetVLines(), "...");
				if(uiKey==_KEY_ESC)
				{
					memset(szRepairPwd, 0, sizeof(szRepairPwd));
					continue;
				}
				if(uiKey==_KEY_FN)
					uiKey='F';
                if(uiKey==_KEY_DOWN)
					uiKey='D';
				szRepairPwd[len]=uiKey;
				if(memcmp(szRepairPwd, FACTORY_REPAIR_PWD, strlen(szRepairPwd))!=0)
					break;
				if(strcmp(szRepairPwd, FACTORY_REPAIR_PWD)==0)
				{
                    extern int SecDirCreat(void);
                    SecDirCreat();
					vRepairPos();
					break;
				}
				//if(len>=strlen(FACTORY_REPAIR_PWD))
				//	break;
				continue;
            }else
				mdelay(100);
        }
		_vFlushKey();
		if(uiKey)
			_vDisp(_uiGetVLines(), "");
	}
}

#if 0 //xf20210820
int Enable_AllTamperSensor(void *p)
{	
/*    
	LcdClear();
	
	//showsensorcfg();

	LcdClear();

	if (checkIfSensorEnabled())
	{    //already enabled 
		dbg("already enabled ");
		LcdPutsc("Already enabled", 1);
		mdelay(1000*2);
		return 0;
	}
	
	if(sensorEnableAll())
	{ 
		err("enable sensor error"); 
		LcdPutsc("enable error", 1);
		mdelay(1000*2);
		return -1;
	}
	
	dbg("enable sensor success");
	LcdPutsc("enable success", 1);
	mdelay(1000*2);
*/    
	return 0;
}
#endif
int switchSensorSetting(void)
{
/*    
    //1.disable all sensor
    if(SensorDisableAll())
    {
        return -1;
    }
    //2.enable sensor again
    if(sensorEnable())
    {
        return -2;
    }
*/
    return 0;
}



void tusnKeyTest()
{
	char szTmp[16];
	char szTusnKey[20];
//	unsigned int ver;
	int loop = 0;
	
	//Enable_AllTamperSensor(NULL);

	//switchSensorSetting();

	//ver = mh_crypt_version();
	//DPRINTF("MegaHunt SCPU secure lib version is V%02x.%02x.%02x.%02x\n", ver >> 24, (ver >> 16)&0xFF, (ver>>8)&0xFF, ver & 0xFF);

	

//	while(1)
//	{
		loop++;
		sprintf(szTusnKey, "123456789012345%01d", loop%10);
		SecWriteTusnKey(szTusnKey);
		memset(szTusnKey, 0x00, sizeof(szTusnKey));
		SecReadTusnKey(szTusnKey);
		dbg("szTusnKey = %s\n", szTusnKey);
		
	
		memset(szTmp, 0x00, sizeof(szTmp));
		SecCalTusnCiphertext("12345678901234567890", "123456", szTusnKey, szTmp);
		dbg("szTmp = %s\n", szTmp);

//		mh_SM4_test_with_known_data1();
//		mh_SM4_test_with_known_data2();	
//	}
}

extern void vDispCSQIcon(void);
static void EmvL2Main(void)
{
    
	{
		waitPowerOn();
		KeyBoardOpen();
		KeyBoardClean();
	    //RTC_Set24HoursReboot();
	}
	
	_vPosInit();		//初始化
	vInitInput();		//必须先调用1次

    if(battery_get_voltage()<BATTERY_1_MIN)
    {
        _vCls();
        vDispMid(3, "电池电量低");
        vDispMid(4, "请先充电");
        while(battery_get_voltage()<BATTERY_1_MIN)
        {
            PMUModeCheck();
            mdelay(200);
        }
        _vCls();
    }	

	#ifdef PROJECT_CY21
	{
		extern void vFixCY21ProductCode20230208(void);
		vFixCY21ProductCode20230208();
	}
	#endif
	
#ifdef ST7789
    LCDDisplayPicture((uchar *)gImage_Startup, 320, 61, 0, 85);
#endif
#ifdef ST7571
    LCDDisplayPicture((uchar *)gImage_Startup, 128, 25, 0, 41);
#endif   
#ifdef ST7567
    LCDDisplayPicture((uchar *)gImage_Startup, 110, 20,9, 19);
#endif  
#ifdef LCD_GC9106
    LCDDisplayPicture((uchar *)gImage_Startup, 100, 100,30, 25);
#endif 
//    mdelay(2*1000);
	
#ifdef FUDAN_CARD_DEMO
    Emvl2_InitICCReader();
    vFudanCardApp();
#else     
	//CreateConfDir();
	//SecInit();
#ifdef TEST_TAMP_COMM    
    {
        vTermInit();
        vTestActive();
    }
#endif  
	vCheckTamper();
    CreateConfDir();
	SecInit();
	Emvl2_InitICCReader();

	g_pUserFlushIcon=vDispCSQIcon;
	MyApplication();
#endif    
}

#if USE_RTOS == 1
#ifndef USE_HAL_LIB
static TaskHandle_t  s_handleEmvL2MainTask;
static void EmvL2MainTask(void *p){
    //deap loop here
    while(1){
        EmvL2Main();
        vTaskDelay(1000);
    }
}

/*
this function will be called by start_task()
*/

//#if PROJECT_CY20  != 1
//#define MAIN_USER_TASK_STACK_DEPTH	(1024*8)//((configTOTAL_HEAP_SIZE - 0x5000)/sizeof(int))
//#define MAIN_USER_TASK_STACK_DEPTH	(1024*10)//((configTOTAL_HEAP_SIZE - 0x5000)/sizeof(int))
#define MAIN_USER_TASK_STACK_DEPTH	(0xA000 + 0x5000)/sizeof(int)
//#define MAIN_USER_TASK_STACK_DEPTH	((0x15000)/sizeof(int))
//#else
//#define MAIN_USER_TASK_STACK_DEPTH	((configTOTAL_HEAP_SIZE - 0x10000)/sizeof(int))
//#endif


TaskHandle_t handleWDT;

static void createUsersTasks(void)
{
    //xTaskCreate((TaskFunction_t)EmvL2MainTask,  "EmvL2MainTask", 6000, NULL, 3, (TaskHandle_t *)&s_handleEmvL2MainTask);
	xTaskCreate((TaskFunction_t)EmvL2MainTask,  "EmvL2MainTask", MAIN_USER_TASK_STACK_DEPTH, NULL, 3, (TaskHandle_t *)&s_handleEmvL2MainTask);
	//xTaskCreate((TaskFunction_t)watchdog_task,  "watchdog_task", 1024, NULL, 1, (TaskHandle_t *)&handleWDT);
	xTaskCreate((TaskFunction_t)watchdog_task,  "watchdog_task", 512, NULL, 1, (TaskHandle_t *)&handleWDT);
}

int main(int argc, char *argv[])
{
	System_Init();
#if USE_RTOS == 1
	launchRTOS(createUsersTasks);
#else
	EmvL2Main();
#endif
	return 0;
}
#else
#define MAIN_USER_TASK_STACK_DEPTH	(0xA000 + 0x5000)/sizeof(int)
	
int main(void)
{    
#ifdef USE_HIGH_BAUDRATE
#ifdef RELEASE
	hal_str hal = {(TaskFunction_t)&EmvL2Main, MAIN_USER_TASK_STACK_DEPTH, FALSE, TRUE};    
#else
	hal_str hal = {(TaskFunction_t)&EmvL2Main, MAIN_USER_TASK_STACK_DEPTH, TRUE, TRUE};	  
#endif
#else    
#ifdef RELEASE
	hal_str hal = {(TaskFunction_t)&EmvL2Main, MAIN_USER_TASK_STACK_DEPTH, FALSE};    
#else
	hal_str hal = {(TaskFunction_t)&EmvL2Main, MAIN_USER_TASK_STACK_DEPTH, TRUE};	  
#endif
#endif

	HALInit(&hal);    
	dbg("HAL Version %s\n",GetHALVersion());    
	return 0;
}
#endif
#endif






