#ifndef INCLUDE_RESP_DATA_ITEM_H
#define INCLUDE_RESP_DATA_ITEM_H

#include "Define.h"

extern RespDataItem_t g_RespItemList[RESP_ITEM_COUNT];

int32 InitResponseItemList(void);
int32 UninitResponseItemList(void);
void  PrintResponseItemList(void);
const DataItemAttr_t *GetResponseItemAttr(uint16 index);
int32 SetResponseItem(uint16 index, const byte buf[], uint16 len);
vec_t *GetResponseItemValue(uint16 index);
byte GetRespStatusCode(void);
byte GetRespEmvStatusCode(void);
int32 SetResponseItemStr(uint16 index, const char *str);
int32 IsResponseItemExisted(uint16 index);
int32 ResetResponseItem(uint16 index);
#endif

