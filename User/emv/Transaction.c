#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sysTick.h"
#include "Setup.h"
#include "EmvAppUi.h"
#include "Util.h"
#include "Define.h"
#include "ReqDataItem.h"
#include "RespDataItem.h"
#include "ReqBuild.h"
#include "ReqDataSet.h"
#include "DataTrans.h"
#include "RespParse.h"
#include "Online.h"
#include "ParamDown.h"
#include "Receipt.h"
#include "ReqDataSet.h"
#include "MagCardTrans.h"
#include "MultiLang.h"
#include "AppSelect.h"
#include "protocol.h"
#include "emvDebug.h"
#include "emvl2/workcenter.h"
#include "Transaction.h"
#include "utils.h"
#include "EmvAppUI.h"
#include "user_projectconfig.h"
//#include "systick.h"
#ifdef VPOS_APP
#include "vposface.h"
#include "pub.h"
#ifdef APP_LKL
#include "AppGlobal_lkl.h"
#endif
#ifdef APP_SDJ
#include "AppGlobal_sdj.h"
#endif
#if 1//def APP_CJT
#include "AppGlobal_cjt.h"
#endif
#endif
#include "emvL2.h"
#include "cardfunc.h"

void enableQuickEmv(void);
void disableQuickEmv(void);

extern void vSetDispRetInfo(int flag);
extern int iGetDispRetInfo(void);

extern int gLeftAmount;
extern int gEmvIsOnlineSuccess;

typedef struct
{
	int32 (*SetCmdDataItem)(void);
	int32 (*BuildCmdReqData)(char buf[], uint16 *size);
	int32 (*ParseCmdRespData)(const char buf[], uint16 len);
}SExecCmdFunc;

typedef enum
{
	EExecCmdLoadConfig=0,
	EExecCmdGetAidList,
	EExecCmdInitTrans,
	EExecCmdEmvTrans,
	EExecCmdComplete,
	EExecCmdEndTrans
}EExecCmd;


void ShowTransactionECBalance(void);

int32 CheckRespStatusCode(EExecCmd ExecCmd)
{
	LOGD("Enter CheckRespStatusCode()");
	LOGD("GetRespStatusCode() = %c", GetRespStatusCode());

	switch(GetRespStatusCode())
	{
		case RESP_SC_SUCCESS:
			//ReportLine();
			return STATUS_OK;

		case RESP_SC_FAILURE:
			if(ExecCmd == EExecCmdGetAidList)
			{
				if(g_MagCardTransData.IsHasReadMagCard)
				{
					//DoPartMagCardTransaction();
					DisplayValidateInfo(/*"Terminated��ֹ"*/ "������ֹ");
				}
				else
				{
					//ChipCannotBeReadTrans();
					DisplayValidateInfo(/*"Terminated��ֹ"*/ "������ֹ");
				}
			}
			else if(ExecCmd==EExecCmdInitTrans)
			{
				if(g_CandidateAidCount > 1)
				{
				/*
					ClearScreen();
					DisplayInfoCenter("Select App Fail", 4);
					DisplayInfoCenter("Try Again?", 5);

					if(!IsUserSelectOK())
					{
						return STATUS_USER_CANCEL;
					}
					*/
					//if (DisplayYesNoChoiceInfo("Select App Fail, try again?") == 0)
                    if (DisplayYesNoChoiceInfo("ѡ��Ӧ��ʧ��,����?") == 0)
					{
						return STATUS_USER_CANCEL;
					}
				}
				else
				{
					//ChipCannotBeReadTrans();
					DisplayValidateInfo(/*"Terminated��ֹ"*/ "������ֹ");
				}
			}
			else
			{
				DisplayValidateInfo(/*"Terminated��ֹ"*/ "������ֹ");
			}
			break;

		case RESP_SC_TIMEOUT:
			//DisplayValidateInfo("Timeout,Terminated��ֹ");
			//DisplayValidateInfo(/*"Timeout,��ֹ"*/ "��ʱ,������ֹ");
			break;

		case RESP_SC_CARD_BLOCK:
			//DisplayValidateInfo("Card Blocked,Terminated��ֹ");
			DisplayValidateInfo(/*"Card Blocked,��ֹ"*/ "������,������ֹ");
			break;

		case RESP_SC_MSG_FMT_ERR:
			//DisplayValidateInfo("Msg Fmt Err,Terminated��ֹ");
			DisplayValidateInfo("��������,������ֹ");
			break;

		case RESP_SC_APP_BLOCK:
			//DisplayValidateInfo("Application Blocked,Terminated��ֹ");
			DisplayValidateInfo(/* "App Blocked,��ֹ" */ "��Ӧ������,������ֹ");
			break;

		case RESP_SC_USER_CANCEL:
			//DisplayValidateInfo("User Cancel,Terminated��ֹ");
			//DisplayValidateInfo(/*"User Cancel,��ֹ"*/ "�û�ȡ��,������ֹ");
			break;

		case RESP_SC_NO_APP:
			//DisplayValidateInfo("No Application,Terminated��ֹ");
			DisplayValidateInfo("�޿�Ӧ��,������ֹ");
			break;

		case RESP_SC_APP_NOT_ACCEPTED:
			if(g_CandidateAidCount == 1)
			{
				//DisplayValidateInfo("Terminated,Not Accepted");
				//DisplayValidateInfo("Not Accepted,��ֹ");
			}
			else
			{
			/*
				ClearScreen();
				DisplayInfoCenter("Select App Fail", 4);
				DisplayInfoCenter("Try Again?", 5);

				if(!IsUserSelectOK())
				{
					return STATUS_USER_CANCEL;
				}
				*/
				//if (DisplayYesNoChoiceInfo("Select App Fail,try again?") == 0)
				if (DisplayYesNoChoiceInfo("ѡ��Ӧ��ʧ��,����?") == 0)
				{
					return STATUS_USER_CANCEL;
				}
			}
			break;

		//Jason added on 2012.04.23, started
		case RESP_SC_READ_RECORD_ERR:
			if(g_CandidateAidCount > 1)
			{
			/*
				ClearScreen();
				DisplayInfoCenter("Select App Fail", 4);
				DisplayInfoCenter("Try Again?", 5);

				if(!IsUserSelectOK())
				{
					return STATUS_USER_CANCEL;
				}
			*/
				//if (DisplayYesNoChoiceInfo("Select App Fail,try again?") == 0)
			if (DisplayYesNoChoiceInfo("ѡ��Ӧ��ʧ��,����?") == 0)
			{
					return STATUS_USER_CANCEL;
			}
			}else
			{
				//DisplayValidateInfo(/*"Terminated��ֹ"*/ "������ֹ");
				//DisplayValidateInfoCHN("Terminated", TERMINATED_STRING_CHINESE);
			}
			break;		
		//End

		default:
			//DisplayValidateInfo("Invalid Status Code,Terminated��ֹ");			
			//DisplayValidateInfo(/*"Terminated��ֹ"*/ "������ֹ");
			break;
	}

	return STATUS_FAIL;
}

//#define MAXIM_EMVL2

int32 ExecCommand(EExecCmd ExecCmd)
{	
	static	SExecCmdFunc ExecCmdFuncTable[]=
	{
		{SetLoadConfigItem, BuildLoadConfigReqData,	ParseLoadConfigResp},
		{NULL,				BuildGetAidListReqData,	ParseGetAidListResp},
		{SetInitTransItem,	BuildInitTransReqData,	ParseInitTransResp},
		{SetEmvTransItem,	BuildEmvTransReqData, 	ParseEmvTransResp},
		{NULL,				BuildEmvCompleteReqData,ParseEmvCompleteResp},		
		{NULL,				BuildEndTransReqData,	ParseEndTransResp}
	};
	SExecCmdFunc *pFunc;
	int32 status;
	//byte SendBuf[MAX_SEND_BUF_SIZE] = {0};
	//uint16 SendBufLen = sizeof(SendBuf);
	//byte RecvBuf[MAX_RECV_BUF_SIZE] = {0};
	//uint16 RecvBufLen = sizeof(RecvBuf);

	byte *SendBuf = NULL;
	uint16 SendBufLen = MAX_SEND_BUF_SIZE;
	byte *RecvBuf = NULL;	
	uint16 RecvBufLen = MAX_RECV_BUF_SIZE;

	SendBuf = malloc(MAX_SEND_BUF_SIZE);
	if (SendBuf == NULL)
	{
		err("malloc error");
		return STATUS_FAIL;
	}

	RecvBuf = malloc(MAX_RECV_BUF_SIZE);
	if (RecvBuf == NULL)
	{
		err("malloc error");
        free(SendBuf);
		return STATUS_FAIL;
	}

	dbg("Enter ExecCommand()\n");

	//ClearScreen();

	if(ExecCmd == EExecCmdComplete)
	{
		//DisplayInfoCenter("Online...", 4);
		//DisplayProcess("Online....");
        //DisplayProcess("����������...");
        //_vDisp(3, "����������...");
	}
	else if(ExecCmd == EExecCmdGetAidList)
	{
		//DisplayInfoCenter("Insert Card", 4);
		vDispMid(3, "������,����ο�");
	}else if(ExecCmd == EExecCmdEndTrans)
	{
        #ifndef VPOS_APP
		ClearScreen();
		DisplayProcess("���׽������µ���");
        #endif
	}
	else if(ExecCmd == EExecCmdInitTrans)
	{
		//DisplayInfoCenter("Waiting...", 4);
		//DisplayProcess("��ʼ������");
        vDispMid(3, "������,����ο�");
	}else
	{
		//DisplayProcess("������");
        vDispMid(3, "���ڴ�����...");
	}

	pFunc = &ExecCmdFuncTable[ExecCmd];

	if(pFunc->SetCmdDataItem !=NULL)
	{
		status =pFunc->SetCmdDataItem();

		if(STATUS_USER_CANCEL == status)
		{
			REPORT_ERR_LINE();
			LOGE("Now is STATUS_USER_CANCEL");
			free(SendBuf);
			free(RecvBuf);
			return STATUS_FAIL;
		}
		else if(status !=STATUS_OK)
		{
			REPORT_ERR_LINE();
			free(SendBuf);
			free(RecvBuf);
			return status;
		}
	}

	status =pFunc->BuildCmdReqData((char *)SendBuf, &SendBufLen);
	if(status != STATUS_OK)
	{
		free(SendBuf);
		free(RecvBuf);
		return status;
	}

	LOGD("EMVL2 Send Buf = %s, SendBufLen = %d", SendBuf, SendBufLen);
	status =TransData(SendBuf, SendBufLen, RecvBuf, &RecvBufLen);
    
#if 0
#ifdef MAXIM_EMVL2
	status = emvl2_command(SendBuf, SendBufLen, RecvBuf, &RecvBufLen);
    if(status != 0)
	{
		DisplayValidateInfo("Comm Fail");
		LOGE("emvl2_command ERROR");
		return STATUS_FAIL;
	}
#else
    EMVL2_Cmd(SendBuf, SendBufLen, RecvBuf, &RecvBufLen);
#endif
#endif
    
    #ifdef JTAG_DEBUG
    mdelay(10);
    #endif
    LOGD("EMVL2 RecvBuf Buf = %s, RecvBuf = %d", RecvBuf, RecvBufLen);

	status =pFunc->ParseCmdRespData((char *)RecvBuf, RecvBufLen);
	Line;
	if(status !=STATUS_OK)
	{
		free(SendBuf);
		free(RecvBuf);
		return status;
	}

	free(SendBuf);
	free(RecvBuf);
	//ReportLine();
	return CheckRespStatusCode(ExecCmd);
}

int SaveBatchCaptureDetail(void);

void CheckRespEmvStatusCode(void)
{
	Line;

	ClearScreen();

    LOGD("GetRespEmvStatusCode()=0x%02X", GetRespEmvStatusCode());
	switch(GetRespEmvStatusCode())
	{
		case RESP_EMV_SC_TC:
			//MultiLangDispCenter(MLSID_APPROVED, 4);
        #ifndef VPOS_APP
            DisplayValidateInfo("Approved �ɹ�");
            ShowTransactionECBalance();
			WaitForValidate();
        #endif
			SetTransactionResultBCTC(TRANSACTION_RESULT_APPROVED_BCTC);
			#if 0
			while(1)
			{
			
			if(CheckIsTransRecordFull() == 1)
			{
				LOGD("CheckIsTransRecordFull");
				DisplayValidateInfo("Record is full");
				ClearTransRecord();
				//return;
			}
			SaveDetail();
			SaveBatchCaptureDetail();
			//For test
			SetReqItemTransSeqCounter();
			SetReqItemPANTransLogAmount();
			//END
			}
			#else
			SaveDetail();
			SaveBatchCaptureDetail();
			#endif
			//PrintReceipt();
			break;

		case RESP_EMV_SC_ARQC:
			LOGD("Online...");
			break;

		case RESP_EMV_SC_AAC:			
            //DisplayValidateInfo("Declined �ܾ�");
            if(iGetDispRetInfo())
                DisplayValidateInfo("����ʧ��");
			//ShowTransactionECBalance();
			SetTransactionResultBCTC(TRANSACTION_RESULT_DECLINED_BCTC);
			//WaitForValidate();
			//PrintReceipt();
			break;

		case RESP_EMV_SC_AAR:					
			//DisplayValidateInfo("Reversal��ת");
            //DisplayValidateInfo("���ױ��ܾ�2");
            if(iGetDispRetInfo())
                DisplayValidateInfo("����ʧ��");
			break;

		case RESP_EMV_SC_SERVICE_NOT_ALLOWED:
			SetTransactionResultBCTC(TRANSACTION_RESULT_SERVICE_NOT_AOLLOWED);
			//DisplayValidateInfo("Terminated, Not Accepted");
			//DisplayValidateInfo("Not Accepted,��ֹ");
            if(iGetDispRetInfo())
                DisplayValidateInfo("����ʧ��");
			break;

		case RESP_EMV_SC_SERVICE_NOT_ALLOWED_WITH_TC:
			SetTransactionResultBCTC(TRANSACTION_RESULT_SERVICE_NOT_AOLLOWED);
			//DisplayValidateInfo("Terminated, Not Allowed");
            if(iGetDispRetInfo())
                DisplayValidateInfo("����ʧ��");
			break;	
			
		default:
			//DisplayValidateInfo("Invalid Emv Status Code");
            if(iGetDispRetInfo())
                DisplayValidateInfo("����ʧ��");
			break;
	}

}

#if 0
void ShowLeftAmount()
{
	char buf[16];
	ClearScreen();

	if(gEmvIsOnlineSuccess)
	{
		sprintf(buf, "%d.%d", gLeftAmount/100, gLeftAmount%100);
		DisplayInfoCenter("Left Amount:", 3);
		DisplayInfoCenter(buf, 5);
	}
	else
	{
		DisplayInfoCenter("Inquiry Fail", 4);
	}

	WaitForValidate();
	ClearScreen();
}
#else
void ShowLeftAmount()
{
	char buf[16];
	char displayBuf[100];
	ClearScreen();

	if(gEmvIsOnlineSuccess)
	{
		sprintf(buf, "%d.%d", gLeftAmount/100, gLeftAmount%100);
		sprintf(displayBuf, "Left Amount : %s", buf);
	}
	else
	{
		strcpy(displayBuf, "Inquiry Fail");
	}

	DisplayValidateInfo(buf);
}

#endif
bool IsEcTrans(void)
{
#ifdef VPOS_APP
    return FALSE;
#else 
	char szIsEC[10] = {0};

	if(IsResponseItemExisted(RESP_IS_EC))
	{
		strcpy(szIsEC, (char *)(GetResponseItemValue(RESP_IS_EC)->ptr));
	}else
	{
		return FALSE;
	}

//	printf("szIsEC = [%s]\n", szIsEC);
	if (szIsEC[0] == '0')
	{
		return FALSE;
	}

	return TRUE;
#endif 	
}

void ShowTransactionECBalance()
{
	char szIsEC[10] = {0};
	char szBalance[100] = {0};
	char szFormatBalance[100] = {0};
	char szDisp[100] = {0};
	char szTmp[100] = {0};

	LOGD("ShowTransactionECBalance\n");

	if(IsResponseItemExisted(RESP_IS_EC))
	{
		strcpy(szIsEC, (char *)(GetResponseItemValue(RESP_IS_EC)->ptr));
	}else
	{
		return;
	}

	LOGD("szIsEC = [%s]\n", szIsEC);
	if (szIsEC[0] == '0')
	{
		return;
	}
	
	if(IsResponseItemExisted(RESP_ISSUER_APP_DATA))
	{
	//Jason added on 20170531
		if (GetResponseItemValue(RESP_ISSUER_APP_DATA)->len >= 30)
		{
			strncpy(szBalance, (char *)((GetResponseItemValue(RESP_ISSUER_APP_DATA)->ptr) + 20), 10);
			strcpy(szTmp, (char *)(GetResponseItemValue(RESP_ISSUER_APP_DATA)->ptr));
		}else
		{
			
			LOGD("ISSUER APP DATA LENGTH = [%d] is short than 30 means No Balance\n", GetResponseItemValue(RESP_ISSUER_APP_DATA)->len);
			return;
		}
	//Jason added end
	}else
	{
		return;
	}

	LOGD("szTmp = [%s]\n", szTmp);
	LOGD("szBalance = [%s]\n", szBalance);
	foramtAmount(szBalance, szFormatBalance);
	sprintf(szDisp, "Balance: %s", szFormatBalance);
	//DisplayInfoCenter( szDisp, 6);
	DisplayValidateInfo(szDisp);
}


unsigned char gbyTransactionResult = TRANSACTION_RESULT_NOT_START_BCTC;
char gszSelectAID[100];

unsigned char GetTransactionResultBCTC()
{
	return gbyTransactionResult;
}

void SetTransactionResultBCTC(unsigned char setValue)
{
	gbyTransactionResult = setValue;
}

void GetSelectAid(char *szSelectAID)
{
	strcpy(szSelectAID, gszSelectAID);
}

int32 DoTransaction(const char* TransType)
{
	int AppIndex;
	int32 status;

	dbg("Enter DoTransaction()\n");

	InitRequestItemList();
	InitResponseItemList();

	dbg("Before SetTransactionResultBCTC()\n");
	SetTransactionResultBCTC(TRANSACTION_RESULT_NOT_START_BCTC);

	if(LoadTerminalParam() !=STATUS_OK)
	{
		//DisplayValidateInfo("Param Invalid");
        DisplayValidateInfo("IC��������");
		return STATUS_FAIL;
	}

    //DisplayValidateInfo("Start EMV L2 Transaction");
	SetRequestItemStr(REQ_TRANS_TYPE,TransType);

	dbg("Before ExecCommand()\n");
	if(ExecCommand(EExecCmdLoadConfig) !=STATUS_OK)
	{
        DisplayValidateInfo("IC��������");
		return STATUS_FAIL;
	}

	if(ExecCommand(EExecCmdGetAidList) !=STATUS_OK)
	{
        DisplayValidateInfo("����ʧ��");
		SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
		return STATUS_FAIL;
	}

	SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
	if(g_CandidateAidCount == 0)
	{
        DisplayValidateInfo("��֧�ָÿ�");
		SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
		return STATUS_FAIL;
	}

	status = STATUS_FAIL;
	while(g_CandidateAidCount >0)
	{
		status =GetUserSelectAidIndex(&AppIndex);
		LOGD("GetUserSelectAidIndex() = %d", status);
		if(status == STATUS_USER_CANCEL)
		{
			LOGE("GetUserSelectAidIndex return STATUS_USER_CANCEL");
			//DisplayValidateInfo("Terminated, User cancel");
			//DisplayValidateInfo("��ֹ");
			//DisplayValidateInfoCHN("Terminated", TERMINATED_STRING_CHINESE);
			SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}

		SetRequestItemStr(REQ_SELECTED_AID, g_CandidateAidList[AppIndex].Aid);
		strcpy(gszSelectAID, g_CandidateAidList[AppIndex].Aid);
		
		status =ExecCommand(EExecCmdInitTrans);
		if(status ==STATUS_OK)
		{
			break;
		}
		else if(status == STATUS_USER_CANCEL)
		{
            Line;
			SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
            if(iGetDispRetInfo())
            {
                DisplayValidateInfo("����ʧ��");
            }
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}
		else
		{
            Line;
            LOGD("ExecCommand EExecCmdInitTrans status=%d", status);
			RemoveAid(AppIndex);
		}
	}

	if(status != STATUS_OK)
	{
        Line;
		SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
        if(iGetDispRetInfo())
        {
            DisplayValidateInfo("����ʧ��");
        }
		return STATUS_NEED_SEND_END_TRANS_CMD;
	}

	dbg("Before ExecCommand(EExecCmdEmvTrans)\n");
	if(ExecCommand(EExecCmdEmvTrans) !=STATUS_OK)
	{
        Line;
		dbg("ExecCommand EExecCmdEmvTrans error\n");
		SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
		return STATUS_NEED_SEND_END_TRANS_CMD;
	}
	dbg("After ExecCommand(EExecCmdEmvTrans)\n");

	CheckRespEmvStatusCode();

	if(GetRespEmvStatusCode() !=RESP_EMV_SC_ARQC)
	{
        Line;
#ifndef VPOS_APP        
		if(IsInquiryTransaction())
		{
			ShowLeftAmount();
		}
#endif
		return STATUS_NEED_SEND_END_TRANS_CMD;
	}

	if(ExecCommand(EExecCmdComplete) !=STATUS_OK)
	{
        Line;
		SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
		return STATUS_NEED_SEND_END_TRANS_CMD;
	}

	dbg("Before CheckRespEmvStatusCode\n");
	CheckRespEmvStatusCode();
#ifndef VPOS_APP
	if(IsInquiryTransaction())
	{
		ShowLeftAmount();
	}
#endif    
	return STATUS_NEED_SEND_END_TRANS_CMD;
}

int CheckIsTransRecordFull()
{
#ifndef VPOS_APP    
	if(IsTransRecordFull())
	{
		DisplayValidateInfo("Trans Record Full");
		return 1;
	}
#endif
	return 0;
}

int giTransactionType = -1;
int giAccountType = -1;
int giAmount = -1;
int giAmountCash= -1;


void EMVL2_Transaction_SetParaFromUI(int TransactionType, int AccountType, int Amount, int AmountCash)
{
    giTransactionType = TransactionType;
    giAccountType = AccountType;
    giAmount = Amount;
    giAmountCash = AmountCash;
}

int EMVL2_GetTransType()
{
    return giTransactionType;
}


int EMVL2_GetAccountType()
{
    return giAccountType;
}


int EMVL2_GetAmount()
{
    return giAmount;
}

int EMVL2_GetAmountCash()
{
    return giAmountCash;
}


#define WAIT_EVENT_ICC_MSR	0
#define WAIT_EVENT_MSR_ONLY	1
#define WAIT_EVENT_ICC_ONLY	2

tti_bool IsPressCancelKey(void)
{
	int ret;

	//DisplayProcess("ȡ�����˳�");
	ret = iGetKeyWithTimeout(1);
	if (ret == _KEY_CANCEL)
	{
		dbg("Press Cancel to exit test");	
        return TRUE;
	}
	return FALSE;
}


int WaitForICCOrMSR()
{
	int ret;
	byte status;
	byte data[1024] = {0};
	char trac2kData[256] = {0};
	char pan[256] = {0};
	char serviceCode[100] = {0};
	int datalen;
	int waitType = WAIT_EVENT_ICC_MSR;
	int loop = 0;

	while(loop < 100)
	{
		loop ++;
		if (waitType == WAIT_EVENT_ICC_MSR)
		{
			if(IsPressCancelKey() == TRUE)
			{
				return -1;
			}
        	DisplayProcess("��ˢ����忨");
			ret = wait_msr_icc_present_event(&status, data, &datalen);
			if (ret != 0)
			{
				LOGE("wait_msr_icc_present_event return %d", ret);
				DisplayValidateInfo("Comm fail");
				return -1;
			}
			switch (status)
			{
				case '0':
	                LOGD("MSR SUCCESS, data = %s", data);
					if (strstr((const char *)data, "{2};") != NULL)
					{
						strncpy(trac2kData, strstr((char *)data, "{2};") + 4, strlen(strstr((char *)data, "{2};")) - 4 - 4);
						LOGD("gTrackData2 = %s", trac2kData);
					} else
	                {
	                    LOGD("Swipe card error, try again");
						//DisplayValidateInfo("Swipe card error, try again");
						DisplayValidateInfo("ˢ��ʧ�ܣ�����");
	                    continue;
	                }
					ret = ParseTrack2Data(trac2kData, pan, serviceCode);
					if (ret != STATUS_OK)
					{
						LOGD("ParseTrack2Data error, try again");
						DisplayValidateInfo("ˢ��ʧ��,����");
	                    continue;
					}
					LOGD("pan = %s, serviceCode = %s", pan, serviceCode);
	         	    if(serviceCode[0] == '2' || serviceCode[0] == '6')
					{
						//DisplayValidateInfo("Use Chip Reader, try again");						
						DisplayValidateInfo("ʹ��оƬ��������");
						continue;
					}
					strcpy(g_MagCardTransData.Track1, " ");
					strcpy(g_MagCardTransData.Track2, trac2kData);
					strcpy(g_MagCardTransData.PAN, pan);
					strcpy(g_MagCardTransData.ServiceCode, serviceCode);
	                return 0;

				case '1':
	                LOGD("ICC Power on SUCCESS");
					return 1;

				case '2':
	                LOGD("ICC present but Power on failed");
	                DisplayValidateInfo("ICʧ�ܣ���ˢ������");
					waitType = WAIT_EVENT_MSR_ONLY;
					continue;

				case 'F':
					continue;

				default:
					LOGE("status = %c, unknow value", status);
					return -2;
			}
		}else if(waitType == WAIT_EVENT_MSR_ONLY)
		{
			if(IsPressCancelKey() == TRUE)
			{
				return -1;
			}
		
			DisplayProcess("��ˢ��");
			ret = wait_msr_event(&status, data, &datalen);
			if (ret != 0)
			{
				LOGE("wait_msr_icc_present_event return %d", ret);
				DisplayValidateInfo("Comm fail");
				return -1;
			}
			switch (status)
			{
				case '0':
	                LOGD("MSR SUCCESS, data = %s", data);
					if (strstr((char *)data, "{2};") != NULL)
					{
						strncpy(trac2kData, strstr((char *)data, "{2};") + 4, strlen(strstr((char *)data, "{2};")) - 4 - 4);
						LOGD("gTrackData2 = %s", trac2kData);
					} else
	                {
	                    LOGD("Swipe card error, try again");
						DisplayValidateInfo("Swipe card error, try again");
	                    continue;
	                }
					ret = ParseTrack2Data(trac2kData, pan, serviceCode);
					if (ret != STATUS_OK)
					{
						LOGD("ParseTrack2Data error, try again");
						DisplayValidateInfo("Swipe card error, try again");
	                    continue;
					}
					LOGD("pan = %s, serviceCode = %s", pan, serviceCode);
	         	    
					strcpy(g_MagCardTransData.Track1, " ");
					strcpy(g_MagCardTransData.Track2, trac2kData);
					strcpy(g_MagCardTransData.PAN, pan);
					strcpy(g_MagCardTransData.ServiceCode, serviceCode);
	                return 2;

				case 'F':
					continue;

				default:
					LOGE("status = %c, unknow value", status);
					return -2;
			}
		}
	}

	return 3;
}


uint32 gStart_tick;

uint32 GetNfcStartTick(void)
{
	return gStart_tick;
}

#if 0
int WaitForNFC(char *szPrompt,char *szDispAmt)
{
	int ret;
	byte status;
	int loop = 0;

	if (szPrompt != NULL)
	{
		//NFC_Init();
		//pcd_poweron();
		pcd_antenna_on();
	}
	else
	{
		nfc_close();
		nfc_open();
	}
	ClearScreen();
#ifndef ENABLE_NFCLED
	DisplayNFCLedIcon(LCD_ICON_NFCLED_BLUE, NFCLED_DISPLAY_STATUS_ON);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_YELLOW, NFCLED_DISPLAY_STATUS_OFF);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_GREEN, NFCLED_DISPLAY_STATUS_OFF);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_RED, NFCLED_DISPLAY_STATUS_OFF);
#else
    #ifdef VPOS_APP
    _vSetAllLed(1, 0, 0, 0);
    #endif
#endif    
	if (szPrompt == NULL)
	{
    	DisplayProcess("��ӿ�");
	}else
	{
		DisplayProcess(szPrompt);
	}

	//emv_protocolLoopTest();
	
	vDispCenter(8, szDispAmt, 0);
#ifndef ENABLE_PRINTER
	DisplayNfcLogo();
#endif
	
	while(loop++ < 2000)
	{
		ret = nfc_detect(&status);
		if (ret != 0)
		{
			DisplayAllNFCLedIconNone();
			LOGE("nfc_detect return %d", ret);
			DisplayValidateInfo("Comm fail");
			return -1;
		}
		switch (status)
		{
			case '0':
				//BuzzerOn(50);
				gStart_tick = get_tick();
				dbg("gStart_tick = %d\n", gStart_tick);
				//DisplayNFCLedIcon(LCD_ICON_NFCLED_YELLOW, NFCLED_DISPLAY_STATUS_ON);
                //LOGD("nfc_detect SUCCES");
				//DisplayProcess("������");
				//vDispCenter(7, "������", 0);
				//mdelay(100);
				return 0;

			case 'F':
				continue;

			case '1':
				//DisplayAllNFCLedIconOff();
				//DisplayNFCLedIcon(LCD_ICON_NFCLED_RED, NFCLED_DISPLAY_STATUS_ON);
				//mdelay(500);
				//DisplayAllNFCLedIconNone();
				LOGD("nfc_detect Card Col");
				DisplayValidateInfo("����ͻ");
                return -2;
			
			default:
				DisplayAllNFCLedIconNone();
				LOGE("status = %c, unknow value", status);
				return -3;
		}

//		if(IsPressCancelKey() == TRUE)
//		{
//			return -1;
//		}
	}	

	DisplayAllNFCLedIconNone();
	return 1;
}
#endif

#ifdef FUDAN_CARD_DEMO
int WaitForNFCFudan(char *szPrompt)
{
	int ret;
	byte status;
	int loop = 0;
	byte uid[20];
	int uidLen;
	char szDispTmp[100];
	char szUID[100];

	dbg("Enter WaitForNFCFudan\n");

	
	nfc_close();
	nfc_open();
	
	ClearScreen();
	DisplayNFCLedIcon(LCD_ICON_NFCLED_BLUE, NFCLED_DISPLAY_STATUS_ON);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_YELLOW, NFCLED_DISPLAY_STATUS_OFF);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_GREEN, NFCLED_DISPLAY_STATUS_OFF);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_RED, NFCLED_DISPLAY_STATUS_OFF);
	if (szPrompt == NULL)
	{
    	DisplayProcess("Please tap card");
	}else
	{
		DisplayProcess(szPrompt);
	}

	//emv_protocolLoopTest();
	
	//vDispCenter(8, szDispAmt, 0);
//#if PROJECT_CY21 == 1	
	DisplayNfcLogo();
//#endif
	
	while(loop++ < 2000)
	{
		ret = nfc_fudan_detect(&status, uid, &uidLen);
		if (ret != 0)
		{
			DisplayAllNFCLedIconNone();
			LOGE("nfc_detect return %d", ret);
			DisplayValidateInfo("detect error");
			return -1;
		}
		switch (status)
		{
			case '0':
				BuzzerOn(50);
				//gStart_tick = get_tick();
				//dbg("gStart_tick = %d\n", gStart_tick);
				memset(szUID, 0x00, sizeof(szUID));
				HextoA(szUID, uid, uidLen);
				sprintf(szDispTmp, "UID:%s", szUID);
				ClearScreen();
				vDispCenter(4, "Tap card success", 0);
				vDispCenter(5, szDispTmp, 0);
				mdelay(1500);
				//DisplayNFCLedIcon(LCD_ICON_NFCLED_YELLOW, NFCLED_DISPLAY_STATUS_ON);
                //LOGD("nfc_detect SUCCES");
				//DisplayProcess("������");
				//vDispCenter(7, "������", 0);
				//mdelay(100);
				return 0;

			case 'F':
				continue;

			case '1':
				//DisplayAllNFCLedIconOff();
				//DisplayNFCLedIcon(LCD_ICON_NFCLED_RED, NFCLED_DISPLAY_STATUS_ON);
				//mdelay(500);
				//DisplayAllNFCLedIconNone();
				LOGD("nfc_detect Card Col");
				//DisplayValidateInfo("����ͻ");
				DisplayValidateInfo("Detect error, please retry");
				return -2;
			
			default:
				DisplayAllNFCLedIconNone();
				LOGE("status = %c, unknow value", status);
				return -3;
		}

		if(IsPressCancelKey() == TRUE)
		{
			return -1;
		}
	}	

	DisplayAllNFCLedIconNone();
	return 1;
}
#endif

int32 TransResultCommWithHost(void);

void Transaction(const char *TransType)
{	
	char szTransType[10];
//	int ret;

	LOGD("Transaction");
#ifndef VPOS_APP    
	if(CheckIsTransRecordFull() == 1)
	{
		LOGD("CheckIsTransRecordFull");
        DisplayValidateInfo("Record is full");
		ClearTransRecord();
		return;
	}
#else
    {
//        extern void vSetTransRevFlag(int flag);
//        vSetTransRevFlag(0);
        vSetDispRetInfo(1);
    }     
#endif
	SetTransactionResultBCTC(TRANSACTION_RESULT_NOT_START_BCTC);

	//strcpy(szTransType, TransType);
	switch (EMVL2_GetTransType()){
        case 0:                         //����,Ԥ��Ȩ���
            strcpy(szTransType, TRANS_T_GOOD_SERVICE);
            break;
        case 1:
            strcpy(szTransType, TRANS_T_CASH);
            break;
        case 9:
            strcpy(szTransType, TRANS_T_GOOD_CASHBACK);
            break;
        case 0x03:
            strcpy(szTransType, "03");  //Ԥ��Ȩ
            break;
        case 0x20:          			//���ѳ���,Ԥ��Ȩ����,Ԥ��Ȩ��ɳ���,�˻�
            strcpy(szTransType, "20");  
            break;
		case 0x30:
			strcpy(szTransType, "30");  //�����
            break;
		case 0x31:
            strcpy(szTransType, "31");  //��������
            break;
        default:
            LOGE("giTransactionType = %d, error", EMVL2_GetTransType());
            return;
    }
#ifndef VPOS_APP
	ret = WaitForICCOrMSR();
	//ret = 1;
	switch (ret)
	{
		case 0:
			LOGD("MSR Swipe OK");
			DoFullMagCardTransaction(szTransType);
			return;

		case 1:
            LOGD("ICC POWER ON OK");
			break;

		case 2:
            LOGD("ICC POWER ON Failed");
			DoPartMagCardTransaction(szTransType);
			return;

		case 3:
			LOGD("Timeout");
			DisplayValidateInfo("��ʱ�˳�");
			return;

		default:
            LOGD("Unknow status");
			return;
	}
#endif
	gEmvIsOnlineSuccess =FALSE;
	if(DoTransaction(szTransType) == STATUS_NEED_SEND_END_TRANS_CMD)
	{
		ExecCommand(EExecCmdEndTrans);
	}
    
    //Jason added on 2021/05/10, free the memory when IC card transaction is done to void QR malloc error.
    UninitRequestItemList();
    UninitResponseItemList();
#ifndef VPOS_APP
	TransResultCommWithHost();
#else
    
    //��ʱ����
    {
        extern int iGetTransRevFlag(void);
        extern int iLiJiProcReverse(st8583 *pSnd8583, st8583 *pRcv8583);
        
        if(iGetTransRevFlag())
        {
            iLiJiProcReverse(NULL, NULL);
        }
    }
    
#endif
	//PrintReceipt();
}



int ExecReadLogCmd(char *szAID)
{
	int  status;	
	byte SendBuf[1024] = {0};
	uint16 SendBufLen = sizeof(SendBuf);
	byte RecvBuf[1024] = {0};
	uint16 RecvBufLen = sizeof(RecvBuf);

	//ClearScreen();

	//DisplayInfoCenter("Waiting...", 4);

	LOGD("8888 ");
	sprintf((char *)SendBuf, "EL.%s", szAID);
	SendBufLen = strlen((const char*)SendBuf);
	LOGD("TransData send buf list = %s", SendBuf);
	status = TransData((byte *)SendBuf, SendBufLen, (byte *)RecvBuf, &RecvBufLen);
	if(status !=STATUS_OK)
	{
		LOGD("9999 ");
		DisplayValidateInfo("Comm Fail");
		return STATUS_FAIL;
	}
	LOGD("TransData RecvBuf = %s", RecvBuf);
	if (RecvBuf[3] == '0')
	{
		LOGD("aaaa ");
		return STATUS_OK;
	}else
	{
		LOGD("bbbb ");
		return STATUS_FAIL;	
	}
}

int ExecReadBalanceCmd(char *szAID)
{
	int  status;	
	char SendBuf[MAX_SEND_BUF_SIZE];
	uint16 SendBufLen = sizeof(SendBuf);
	char RecvBuf[MAX_RECV_BUF_SIZE];
	uint16 RecvBufLen = sizeof(RecvBuf);

	//ClearScreen();

	//DisplayInfoCenter("Waiting...", 4);

	sprintf(SendBuf, "EE.%s", szAID);
	SendBufLen = strlen(SendBuf);
	status = TransData((byte *)SendBuf, SendBufLen, (byte *)RecvBuf, &RecvBufLen);
	if (status !=STATUS_OK)
	{
		DisplayValidateInfo("Comm Fail");
		return STATUS_FAIL;
	}
	if (RecvBuf[3] == '0')
	{
		return STATUS_OK;
	}else
	{
		return STATUS_FAIL;	
	}
}

//Jason added on 2012.04.20
int32 DoReadLogTransaction(void)
{	
	int AppIndex;
	int32 status;

	InitRequestItemList();
	InitResponseItemList();

	if(LoadTerminalParam() !=STATUS_OK)
	{
		DisplayValidateInfo("Param Invalid");
		return STATUS_FAIL;
	}

	SetRequestItemStr(REQ_TRANS_TYPE, TRANS_T_READ_LOG);
	SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_SAVINGS);
	if(ExecCommand(EExecCmdLoadConfig) !=STATUS_OK)
	{
		REPORT_ERR_LINE();
		DisplayValidateInfo("Terminated��ֹ");
		return STATUS_FAIL;
	}

	if(ExecCommand(EExecCmdGetAidList) !=STATUS_OK)
	{
		REPORT_ERR_LINE();
		LOGE("EExecCmdGetAidList ERR");
		DisplayValidateInfo("Terminated��ֹ");
		return STATUS_FAIL;
	}
	LOGD("After ExecCommand ");

	if(g_CandidateAidCount == 0)
	{
		LOGD("g_CandidateAidCount == 0 ");
		DisplayValidateInfo("Terminated��ֹ");
		return STATUS_FAIL;
	}

	LOGD("2222 ");
	status = STATUS_FAIL;
	while(g_CandidateAidCount >0)
	{
		status =GetUserSelectAidIndex(&AppIndex);
		if(status == STATUS_USER_CANCEL)
		{
			LOGD("33333 ");
			//DisplayValidateInfo("Terminated��ֹ");
			ExecCommand(EExecCmdEndTrans);
			REPORT_ERR_LINE();
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}
		else if( status !=STATUS_OK)
		{
			ExecCommand(EExecCmdEndTrans);
			//DisplayValidateInfo("Terminated��ֹ");
			REPORT_ERR_LINE();
			LOGD("44444 ");
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}

		LOGD("55555 ");
		//SetRequestItemStr(REQ_SELECTED_AID, g_CandidateAidList[AppIndex].Aid);	
		//status = ExecCommand(EExecCmdInitTrans);
		status = ExecReadLogCmd(g_CandidateAidList[AppIndex].Aid);
		if(status ==STATUS_OK)
		{
			LOGD("7777 ");
			break;
		}
		else if(status == STATUS_USER_CANCEL)
		{
			REPORT_ERR_LINE();
			LOGD("6666 ");
			DisplayValidateInfo("Terminated��ֹ");
			ExecCommand(EExecCmdEndTrans);
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}
		else
		{
			RemoveAid(AppIndex);
		}
	}

	return 0;
}

void ReadLogTransaction(void)
{
	DoReadLogTransaction();
}

//Jason added on 2012.04.24
int32 DoReadBalanceTransaction(void)
{	
	int AppIndex;
	int32 status;

	InitRequestItemList();
	InitResponseItemList();

	if(LoadTerminalParam() !=STATUS_OK)
	{
		DisplayValidateInfo("Param Invalid");
		return STATUS_FAIL;
	}

	SetRequestItemStr(REQ_TRANS_TYPE, TRANS_T_READ_LOG);
	SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_SAVINGS);
	if(ExecCommand(EExecCmdLoadConfig) !=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	if(ExecCommand(EExecCmdGetAidList) !=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	if(g_CandidateAidCount == 0)
	{
		return STATUS_FAIL;
	}

	status = STATUS_FAIL;
	while(g_CandidateAidCount >0)
	{
		status =GetUserSelectAidIndex(&AppIndex);
		if(status == STATUS_USER_CANCEL)
		{
			//DisplayValidateInfo("Terminated��ֹ");
			ExecCommand(EExecCmdEndTrans);
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}
		else if( status !=STATUS_OK)
		{
			ExecCommand(EExecCmdEndTrans);
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}

		//SetRequestItemStr(REQ_SELECTED_AID, g_CandidateAidList[AppIndex].Aid);	
		//status = ExecCommand(EExecCmdInitTrans);
		status = ExecReadBalanceCmd(g_CandidateAidList[AppIndex].Aid);
		if(status ==STATUS_OK)
		{
			//break;
			ExecCommand(EExecCmdEndTrans);
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}
		else if(status == STATUS_USER_CANCEL)
		{
			//DisplayValidateInfo("Terminated��ֹ");
			ExecCommand(EExecCmdEndTrans);
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}
		else
		{
			RemoveAid(AppIndex);
		}
	}

	ExecCommand(EExecCmdEndTrans);
	return STATUS_NEED_SEND_END_TRANS_CMD;
}


void ReadBalanceTransaction()
{
	DoReadBalanceTransaction();
}

int32 DoQuickTransaction(const char* TransType)
{
	int AppIndex;
	int32 status;

	dbg("Enter DoTransaction()\n");

	InitRequestItemList();
	InitResponseItemList();

	dbg("Before SetTransactionResultBCTC()\n");
	SetTransactionResultBCTC(TRANSACTION_RESULT_NOT_START_BCTC);

	if(LoadTerminalParam() !=STATUS_OK)
	{
		//DisplayValidateInfo("Param Invalid");
        DisplayValidateInfo("IC��������");
		return STATUS_FAIL;
	}

    //DisplayValidateInfo("Start EMV L2 Transaction");
	SetRequestItemStr(REQ_TRANS_TYPE,TransType);

	dbg("Before ExecCommand()\n");
	if(ExecCommand(EExecCmdLoadConfig) !=STATUS_OK)
	{
        DisplayValidateInfo("IC��������");
		return STATUS_FAIL;
	}

	if(ExecCommand(EExecCmdGetAidList) !=STATUS_OK)
	{
        DisplayValidateInfo("����ʧ��");
		SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
		return STATUS_FAIL;
	}

	SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
	if(g_CandidateAidCount == 0)
	{
        DisplayValidateInfo("��֧�ָÿ�");
		SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
		return STATUS_FAIL;
	}

	status = STATUS_FAIL;
	while(g_CandidateAidCount >0)
	{
		status =GetUserSelectAidIndex(&AppIndex);
		LOGD("GetUserSelectAidIndex() = %d", status);
		if(status == STATUS_USER_CANCEL)
		{
			LOGE("GetUserSelectAidIndex return STATUS_USER_CANCEL");
			SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}

		SetRequestItemStr(REQ_SELECTED_AID, g_CandidateAidList[AppIndex].Aid);
		strcpy(gszSelectAID, g_CandidateAidList[AppIndex].Aid);
		
		status =ExecCommand(EExecCmdInitTrans);
		if(status ==STATUS_OK)
		{
			break;
		}
		else if(status == STATUS_USER_CANCEL)
		{
			SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
            if(iGetDispRetInfo())
            {
                DisplayValidateInfo("����ʧ��");
            }
			return STATUS_NEED_SEND_END_TRANS_CMD;
		}
		else
		{
			RemoveAid(AppIndex);
		}
	}

	if(status != STATUS_OK)
	{
		SetTransactionResultBCTC(TRANSACTION_RESULT_TERMINATED_BCTC);
        if(iGetDispRetInfo())
        {
            DisplayValidateInfo("����ʧ��");
        }
		return STATUS_NEED_SEND_END_TRANS_CMD;
	}
    
    SetTransactionResultBCTC(TRANSACTION_RESULT_APPROVED_BCTC);
	return STATUS_NEED_SEND_END_TRANS_CMD;
}

static int sg_iQuickTransEndStatus=0;
int iQuickTransaction(const char *TransType)
{	
	char szTransType[10];
//	int ret;

	LOGD("Transaction");

    vSetDispRetInfo(1);
    sg_iQuickTransEndStatus=0;
    
	SetTransactionResultBCTC(TRANSACTION_RESULT_NOT_START_BCTC);
    
	//strcpy(szTransType, TransType);
	switch (EMVL2_GetTransType()){
        case 0:                         //����,Ԥ��Ȩ���
            strcpy(szTransType, TRANS_T_GOOD_SERVICE);
            break;
        case 1:
            strcpy(szTransType, TRANS_T_CASH);
            break;
        case 9:
            strcpy(szTransType, TRANS_T_GOOD_CASHBACK);
            break;
        case 0x03:
            strcpy(szTransType, "03");  //Ԥ��Ȩ
            break;
        case 0x20:          			
            strcpy(szTransType, "20");  //���ѳ���,Ԥ��Ȩ����,Ԥ��Ȩ��ɳ���,�˻�
            break;
		case 0x30:
			strcpy(szTransType, "30");  //�����
            break;
		case 0x31:
            strcpy(szTransType, "31");  //��������
            break;
        default:
            LOGE("giTransactionType = %d, error", EMVL2_GetTransType());
            return -1;
    }

	gEmvIsOnlineSuccess =FALSE;
    /*
	if(DoQuickTransaction(szTransType) == STATUS_NEED_SEND_END_TRANS_CMD)
	{
		ExecCommand(EExecCmdEndTrans);
	}
    */
    sg_iQuickTransEndStatus=DoQuickTransaction(szTransType);
    
    //Jason added on 2021/05/10, free the memory when IC card transaction is done to void QR malloc error.
    UninitRequestItemList();
    UninitResponseItemList();
    
    if(GetTransactionResultBCTC()==TRANSACTION_RESULT_APPROVED_BCTC)
        return 0;
    else
        return 1;
}

void vQuickTransEnd(void)
{
    if(sg_iQuickTransEndStatus==STATUS_NEED_SEND_END_TRANS_CMD)
        ExecCommand(EExecCmdEndTrans);
    sg_iQuickTransEndStatus=0;
    return;
}

//End
