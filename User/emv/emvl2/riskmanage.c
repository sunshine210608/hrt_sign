#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
//#include <unistd.h>
#include "global.h"
#include "riskmanage.h"
#include "dataauth.h"
#include "utils.h"
#include "transflow.h"
#include "misce.h"
#include "ui.h"
#include "utils.h"
#include "sha1.h"
#include "cardfunc.h"
#include "ui.h"
#include "dataauth.h"
#include "emvDebug.h"
#include "des_sec.h"
#include "pinpad.h"
#include "user_projectconfig.h"

#ifdef VPOS_APP
#include "VposFace.h"
#include "pub.h"
#ifdef APP_LKL
#include "AppGlobal_lkl.h"
#endif
#ifdef APP_SDJ
#include "AppGlobal_sdj.h"
#endif
#ifdef APP_CJT
#include "AppGlobal_cjt.h"
#endif


#endif


void EConlineEncipherPin(RunningTimeEnv *runningTimeEnv);

tti_int32 GetPINEnterFromUI(tti_tchar *szDisplayString, tti_tchar *szPINValue);


//------------------------------------------------------------------------------------------
void randomTransSelectionOnline(RunningTimeEnv *runningTimeEnv)
{
	tti_int32 amount, floorAmount;
	tti_uint32 random;
	tti_byte tmp[10];
	DPRINTF("random trans select 123\n");
	//ReportLine();
	
	amount=getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT);
	//floorAmount=getDataByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT);	
	floorAmount=getAmountByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT);

	if (amount>floorAmount)
	{
		//SetTVR(TVR_RANDOM_SELECTED_FOR_ONLINE);
		//////Line;
		DPRINTF("amount >floorAmount 123\n");
		//ReportLine();
	}
	else
	{
		//Jason start edit
		//ReportLine();
		//srand((tti_int32)time(NULL));
		//ReportLine();
		//Jason end edit
		//Jason edit 2013.05.21, start
		//runningTimeEnv->commonRunningData.randomValueForTransSelect=rand()%99+1;
		random = rand_hardware_byte(tmp);
		//runningTimeEnv->commonRunningData.randomValueForTransSelect=rand_hardware_byte(tmp)%99+1;
		runningTimeEnv->commonRunningData.randomValueForTransSelect=(random%99)+1;
		DPRINTF("random = %d, randomValueForTransSelect = %d\n", random, 
			runningTimeEnv->commonRunningData.randomValueForTransSelect);
		//Jason edit 2013.05.21, end
		
		if (amount<runningTimeEnv->commonRunningData.thresholdAmount)
		{
			//////Line;
			DPRINTF("33\n");
			if (runningTimeEnv->commonRunningData.randomValueForTransSelect<=
				runningTimeEnv->commonRunningData.targetPercentage)
			{
				//////Line;
				DPRINTF("44\n");
				//ReportLine();
				SetTVR(TVR_RANDOM_SELECTED_FOR_ONLINE);
			}
		}
		else
		{
			//////Line;
			DPRINTF("77\n");
			if (floorAmount<=runningTimeEnv->commonRunningData.thresholdAmount)
			{
				//////Line;
				DPRINTF("55\n");
				//ReportLine();
				SetTVR(TVR_RANDOM_SELECTED_FOR_ONLINE);
			}
			else if (runningTimeEnv->commonRunningData.randomValueForTransSelect<=
				((runningTimeEnv->commonRunningData.maxTargetPercentage-	runningTimeEnv->commonRunningData.targetPercentage)
				*(amount-runningTimeEnv->commonRunningData.thresholdAmount))/
				(floorAmount-runningTimeEnv->commonRunningData.thresholdAmount)
				+runningTimeEnv->commonRunningData.targetPercentage)
			{
				//////Line;
				//ReportLine();
				DPRINTF("66\n");
				SetTVR(TVR_RANDOM_SELECTED_FOR_ONLINE);
			}
		}
	}
}

//------------------------------------------------------------------------------------------
void CheckVelocity(RunningTimeEnv *runningTimeEnv)
{
	tti_int32  ATC, lastATC, lowerLimit, upperLimit;

	DPRINTF("check velocity\n");
		
	getSpecialDataFromICC(runningTimeEnv, TAG_ATC);
	getSpecialDataFromICC(runningTimeEnv, TAG_LAST_ATC);

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_LAST_ATC))
	{
		lastATC=getDataByTag(&runningTimeEnv->tagList, TAG_LAST_ATC);
		if (lastATC==0)
		{
			SetTVR(TVR_NEW_CARD);
		}
	}

	if ((!TagIsExisted(&runningTimeEnv->tagList, TAG_LAST_ATC))||
		(!TagIsExisted(&runningTimeEnv->tagList, TAG_ATC)))
	{
		SetTVR(TVR_ICC_DATA_MISSING);
		SetTVR(TVR_LOWER_OFFLINE_LIMIT_EXCEEDED);
		SetTVR(TVR_UPPER_OFFLINE_LIMIT_EXCEEDED);
	}
	else
	{
		ATC=getDataByTag(&runningTimeEnv->tagList, TAG_ATC);
		lastATC=getDataByTag(&runningTimeEnv->tagList, TAG_LAST_ATC);
		if (ATC<=lastATC)
		{
			SetTVR(TVR_LOWER_OFFLINE_LIMIT_EXCEEDED);
			SetTVR(TVR_UPPER_OFFLINE_LIMIT_EXCEEDED);
		}
		else
		{
			lowerLimit=getDataByTag(&runningTimeEnv->tagList, TAG_LOWER_OFFLINE_LIMIT);
			upperLimit=getDataByTag(&runningTimeEnv->tagList, TAG_UPPER_OFFLINE_LIMIT);
			if ((ATC-lastATC)>lowerLimit)
			{
				SetTVR(TVR_LOWER_OFFLINE_LIMIT_EXCEEDED);
			}
			if ((ATC-lastATC)>upperLimit)
			{
				SetTVR(TVR_UPPER_OFFLINE_LIMIT_EXCEEDED);
			}
		}
 	}
}

//------------------------------------------------------------------------------------------
tti_bool terminalCanGoOnline(RunningTimeEnv *runningTimeEnv)
{
	tti_byte *pValue;

	pValue=GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_TYPE);
	if (pValue!=NULL)
	{
		switch ((*pValue)&0x0F)
		{
			case 0x03:
			case 0x06:
				return FALSE;
			
			default:
				break;
		}
	}

	return TRUE;
}

//------------------------------------------------------------------------------------------
tti_bool terminalIsOnlineOnly(RunningTimeEnv *runningTimeEnv)
{
	tti_byte *pValue;

	pValue=GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_TYPE);
	if (pValue!=NULL)
	{
		switch ((*pValue)&0x0F)
		{
			case 0x01:
			case 0x04:
				return TRUE;
//				break;
			default:
				break;
		}
	}

	return FALSE;
}

//------------------------------------------------------------------------------------------
void terminalRiskManagement(RunningTimeEnv *runningTimeEnv)
{
	DPRINTF("risk management 123 \n");

	//Jason added on 2012.04.19, started
	if (IsECTrans(runningTimeEnv))
	{
		return;
	}
	//End
	
	SetTSI(TSI_TERMINAL_RISK_PERFORMED);

	//DPRINTF("runningTimeEnv->commonRunningData.transLogAmount = %d\n", 
	//	runningTimeEnv->commonRunningData.transLogAmount);
	DPRINTF("getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT) = %d\n", 
		getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT));
	//DPRINTF("getDataByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT) = %d\n", 
	//	getDataByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT));
	DPRINTF("getAmountByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT) = %d\n", 
		getAmountByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT));
	/*
	if (((runningTimeEnv->commonRunningData.transLogAmount) +
		getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT))>=
		getDataByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT))
	*/
	if (((runningTimeEnv->commonRunningData.transLogAmount) +
		getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT))>=
		getAmountByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT))
	{
		DPRINTF("Set TVR_EXCEEDS_FLOOR_LIMIT\n");
		SetTVR(TVR_EXCEEDS_FLOOR_LIMIT);
	}

	//DPRINTF("Before terminalCanGoOnline\n");
	if (terminalCanGoOnline(runningTimeEnv))
	{
		DPRINTF("Before randomTransSelectionOnline\n");
		randomTransSelectionOnline(runningTimeEnv);
	}else
	{
		DPRINTF("terminalCanGoOnline return false\n");
	}
	ReportLine();

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_LOWER_OFFLINE_LIMIT)&&
		TagIsExisted(&runningTimeEnv->tagList, TAG_UPPER_OFFLINE_LIMIT))
	{
		CheckVelocity(runningTimeEnv);
	}
}

//------------------------------------------------------------------------------------------
tti_bool isCorrectPrior(tti_byte askCid, tti_byte resultCid)
{
	if (askCid==CID_ARQC)
	{
		if ((resultCid!=CID_ARQC)&&(resultCid!=CID_AAR)&&(resultCid!=CID_AAC))
		{
			return FALSE;
		}
	}
	else if (askCid==CID_AAR)
	{
		if ((resultCid!=CID_AAR)&&(resultCid!=CID_AAC))
		{
			return FALSE;
		}
	}
	else if (askCid==CID_AAC)
	{
		if (resultCid!=CID_AAC)
		{
			return FALSE;
		}
	}

	return TRUE;
}

//------------------------------------------------------------------------------------------
static void SetTCHashValue(TagList *tagList)
{
	tti_byte  	TcData[BUFFER_SIZE_256BYTE];
	tti_byte	TcDataLen;
	tti_byte 	TcHashValue[HASH_RESULT_SIZE];
	
	if(TagIsExisted(tagList, TAG_TDOL))
	{
		BuildDOLToStream(tagList, TAG_TDOL, TcData, &TcDataLen);
		sha1_csum(TcData,TcDataLen,TcHashValue);
	}
	else
	{
		sha1_csum(NULL, 0, TcHashValue);
	}
	
	SetTagValue(TAG_TDOL_HASH_VALUE, TcHashValue, HASH_RESULT_SIZE, tagList);
}

void getECBalanceAfterGenerateAC(RunningTimeEnv *runningTimeEnv)
{
	tti_byte szTmp[100] = {0};

	DPRINTF("111111\n");
	if (IsECTrans(runningTimeEnv) == FALSE)
	{
		return;
	}
	DPRINTF("222222\n");
	
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA) == FALSE)
	{
		return;
	}

	DPRINTF("333333\n");
	
	if (GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA) < 15)
	{
		return;
	}

	
	DPRINTF("44444\n");
	strncpy((char *)szTmp, (char *)((GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA)) + 10), 5);
	SetTagValue(TAG_EC_BALANCE_FROM_GEN_AC, szTmp, 5, &runningTimeEnv->tagList);
	DPRINTF("55555\n");
	
}

//Jason added on 20140314
tti_int32 checkGACRspFormat77(RunningTimeEnv *runningTimeEnv)
{	
	tti_byte *pTagValue;		

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA)||
		TagDataIsMissing(&runningTimeEnv->tagList, TAG_ATC))
	{
		return STATUS_FAIL;
	}

	if ((GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA)!=1)||
		(GetTagValueSize(&runningTimeEnv->tagList, TAG_ATC)!=2)||
		(GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA)>32))
	{
		return STATUS_FAIL;
	}
	
	pTagValue=GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA);
	if (pTagValue==NULL)
	{
		return STATUS_FAIL;
	}

	
	switch ((*pTagValue)&0xc0)
	{
		case CID_TC:
		case CID_ARQC:
			if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_DDA_SDAD))
			{
				return STATUS_FAIL;
			}
			break;
		case CID_AAC:
		case CID_AAR:
			if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_CRYPTOGRAM))
			{
				return STATUS_FAIL;
			}
			if ((GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM)!=8))
			{
				return STATUS_FAIL;
			}
			break;
		default:
			return STATUS_FAIL;
	}

	return STATUS_OK;
}
//End

//------------------------------------------------------------------------------------------
tti_int32 generateAC(RunningTimeEnv *runningTimeEnv, tti_byte resultCode, tti_byte cdol)
{
	tti_byte data[BUFFER_SIZE_256BYTE];
	//tti_byte tdolTempBuffer[BUFFER_SIZE_256BYTE], tdolHashValue[HASH_RESULT_SIZE];
	tti_byte dataLen;
	//tti_byte response[BUFFER_SIZE_10K];
	tti_byte response[BUFFER_SIZE_512BYTE];
	tti_uint16 respLen;
	tti_uint16 tag, tagSize, length, lengthSize, dataStartPoint;
	//Jason added on 2014.3.13
	tti_byte cdasign;

	DPRINTF("generate ac\n");
	
	if (!TagIsExisted(&runningTimeEnv->tagList, cdol))
	{
		return STATUS_FAIL;
	}

	Line;

	if (tagIsInDOL(&runningTimeEnv->tagList, cdol, TAG_TDOL_HASH_VALUE))
	{
		SetTCHashValue(&runningTimeEnv->tagList);
		if ((runningTimeEnv->commonRunningData.defaultTDOLUsed)&&
			TagIsExisted(&runningTimeEnv->tagList, TAG_TDOL))
		{
		//Jason removed on 2020/04/28
			//SetTVR(TVR_DEFAULT_TDOL_USED);
		//End
		}
	}

	Line;
	
	BuildDOLToStream(&runningTimeEnv->tagList, cdol, data, &dataLen);
	//Jason added on 2014.03.17
	if (cdol == TAG_CDOL1)
	{
		memcpy(runningTimeEnv->cdol1Stream, data, dataLen);
		runningTimeEnv->cdol1StreamLen = dataLen;
	}else
	{
		memcpy(runningTimeEnv->cdol2Stream, data, dataLen);
		runningTimeEnv->cdol2StreamLen = dataLen;		
	}
	//End

	Line;
	SetTSI(TSI_CARD_RISK_MAMANGE_PERFORMED);

	Line;

	removeTag(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA);
	removeTag(&runningTimeEnv->tagList, TAG_ATC);
	removeTag(&runningTimeEnv->tagList, TAG_CRYPTOGRAM);
	removeTag(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA);
	
	Line;
	
	if (runningTimeEnv->cdaIsNeedPerform == TRUE)
	{		
		removeTag(&runningTimeEnv->tagList, TAG_DDA_SDAD);
		cdasign = 0x10; 
	}else
	{
		cdasign = 0x00;
	}
	//End

	Line;
	if (SmartCardGenerateAC(cid[resultCode], cdasign, data, (tti_byte)dataLen, response, &respLen)==STATUS_OK)
	{
		Line;
		switch (response[0])
		{
			case 0x80:
				Line;
				if (ParseTlvInfo(response, respLen, &tag, &tagSize, &length, &lengthSize)!=STATUS_OK)
				{
					return STATUS_FAIL;
				}

				if ((respLen!=tagSize+length+lengthSize)||(length<1+2+8))
				{
					return STATUS_FAIL;
				}
				dataStartPoint=tagSize+lengthSize;
					
				SetTagValue(TAG_CRYPTOGRAM_INFO_DATA, &response[dataStartPoint], 1, &runningTimeEnv->tagList);
				SetTagValue(TAG_ATC, &response[dataStartPoint+1], 2, &runningTimeEnv->tagList);
				SetTagValue(TAG_CRYPTOGRAM, &response[dataStartPoint+1+2], 8, &runningTimeEnv->tagList);
				if (respLen-2-1-2-8>0)
				{
					SetTagValue(TAG_ISSUER_APP_DATA, &response[dataStartPoint+1+2+8], respLen-2-1-2-8, &runningTimeEnv->tagList);
					if (GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA)>32)
					{
						return STATUS_FAIL;
					}
				}

				//Jason added on 2014.03.17
				runningTimeEnv->cdaIsGacRspTmp77 = FALSE;
				//End
				break;
			case 0x77:
				Line;
				if (BuildTagList(response, respLen, &runningTimeEnv->tagList)!=STATUS_OK)
				{
					ReportLine();
					return STATUS_FAIL;
				}
				Line;
				//Jason modify on 2014.03.14
				/*
				if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA)||
					TagDataIsMissing(&runningTimeEnv->tagList, TAG_ATC)||
					TagDataIsMissing(&runningTimeEnv->tagList, TAG_CRYPTOGRAM))
				{
					return STATUS_FAIL;
				}
				if ((GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA)!=1)||
					(GetTagValueSize(&runningTimeEnv->tagList, TAG_ATC)!=2)||
					(GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM)!=8)||
					(GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA)>32))
				{
					return STATUS_FAIL;
				}
				*/
				//Jason modified on 2017.05.04
				//if (runningTimeEnv->cdaIsNeedPerform == FALSE)
				Line;
				if (runningTimeEnv->cdaIsNeedPerform == FALSE && runningTimeEnv->cdaAlreadyPerform == FALSE)
				//End
				{
					if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA)||
						TagDataIsMissing(&runningTimeEnv->tagList, TAG_ATC)||
						TagDataIsMissing(&runningTimeEnv->tagList, TAG_CRYPTOGRAM))
					{
						ReportLine();
						return STATUS_FAIL;
					}
					if ((GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA)!=1)||
						(GetTagValueSize(&runningTimeEnv->tagList, TAG_ATC)!=2)||
						(GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM)!=8)||
						(GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA)>32))
					{
						ReportLine();
						return STATUS_FAIL;
					}					
				}else
				{
					Line;
					if (checkGACRspFormat77(runningTimeEnv) != STATUS_OK)
					{
						ReportLine();
						return STATUS_FAIL;
					}
				}
				Line;
				runningTimeEnv->cdaIsGacRspTmp77 = TRUE;
				//End
				break;
			default:
				Line;
				return STATUS_FAIL;
		}
	}
	else
	{
		ReportLine();
		return STATUS_FAIL;
	}

	Line;

	return STATUS_OK;	
}

//------------------------------------------------------------------------------------------
tti_int32 firstGenerateAC(RunningTimeEnv *runningTimeEnv, tti_byte resultCode)
{
	tti_byte *pTagValue;
	
	if (generateAC(runningTimeEnv, resultCode, TAG_CDOL1)!=STATUS_OK)
	{
		LOGE("generateAC ERR");
		return STATUS_FAIL;
	}

	pTagValue=GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA);

	if (((*pTagValue)&0xC0)==CID_AAR)
	{
		LOGE("NOT CID_AAR");
		return STATUS_FAIL;
	}

	if (isCorrectPrior(cid[resultCode], (*pTagValue)&0xC0))
	{
		return STATUS_OK;
	}
	else
	{
		LOGE("isCorrectPrior ERR");
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 secondGenerateAC(RunningTimeEnv *runningTimeEnv, tti_byte resultCode)
{
	return generateAC(runningTimeEnv, resultCode, TAG_CDOL2);
}

//------------------------------------------------------------------------------------------
tti_bool actionCodeIsChecked(tti_byte *pTVR, tti_byte *terminalActionCode, tti_byte *issuerActionCode,
	tti_byte *defaultIssuerActionCode)
{
	tti_int32 index;
	tti_byte *pActionCode;

	if (issuerActionCode!=NULL)
	{
		pActionCode=issuerActionCode;
	}
	else
	{
		pActionCode=defaultIssuerActionCode;
	}

	for (index=0; index<TAC_SIZE; index++)
	{
		if ((pTVR[index] & pActionCode[index])||(pTVR[index] & terminalActionCode[index]))
		{
			return TRUE;
		}

	}

	return FALSE;
}

tti_bool IsNeedDoECTerminalActionAnalysis(RunningTimeEnv *runningTimeEnv)
{
	if (IsECTrans(runningTimeEnv) == FALSE)
	{
		return FALSE;
	}

	//Jason modified on 20170516
	//GetECParaFromICC(runningTimeEnv);
	//Jason modified end

	if (runningTimeEnv->commonRunningData.emvResultCode == EMV_RESULT_REJECT)
	{
		return FALSE;
	}
	
	return TRUE;
	
}



tti_uint8 getECTerminalActionAnalysis(RunningTimeEnv *runningTimeEnv)
{
	if (runningTimeEnv->commonRunningData.emvResultCode == EMV_RESULT_REJECT)
	{
		return EMV_RESULT_REJECT;
	}

	if (runningTimeEnv->commonRunningData.emvResultCode == EMV_RESULT_ONLINE)
	{
		if (getAmountByTag(&runningTimeEnv->tagList, TAG_EC_BALANCE)
			- getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT) <
			getAmountByTag(&runningTimeEnv->tagList, TAG_EC_RESET_THRESHOLD))
		{
			EConlineEncipherPin(runningTimeEnv);
		}
		return EMV_RESULT_ONLINE;
	}

	if (runningTimeEnv->commonRunningData.emvResultCode == EMV_RESULT_GOOD_OFFLINE)
	{
		if (getAmountByTag(&runningTimeEnv->tagList, TAG_EC_BALANCE)
			- getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT) <
			getAmountByTag(&runningTimeEnv->tagList, TAG_EC_RESET_THRESHOLD))
		{
			EConlineEncipherPin(runningTimeEnv);
			return EMV_RESULT_ONLINE;	
		}else
		{
			return EMV_RESULT_GOOD_OFFLINE;
		}
		
	}
	return EMV_RESULT_REJECT;
		
		
}


//------------------------------------------------------------------------------------------
tti_uint8 getTerminalActionAnalysis(RunningTimeEnv *runningTimeEnv)
{
	tti_byte *pTVR;
	tti_byte defaultIccDenailActionCode[TAC_SIZE]="\x00\x00\x00\x00\x00";
	tti_byte defaultIccOnlineActionCode[TAC_SIZE]="\xFF\xFF\xFF\xFF\xFF";
	tti_byte defaultIccDefaultActionCode[TAC_SIZE]="\xFF\xFF\xFF\xFF\xFF";

	pTVR=GetTagValue(&runningTimeEnv->tagList, TAG_TVR);
	if (pTVR==NULL)
	{
		DPRINTF("TVR: NO\n");
		return EMV_RESULT_REJECT;
	}
	else
	{
		DPRINTF("TVR: ");
		printByteArray(pTVR, 5);
		DPRINTF("\n");
	}

	DPRINTF("IAC DENIAL:");
	printByteArray(GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DENIAL),
		GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_DENIAL));
	DPRINTF("IAC ONLINE:");
	printByteArray(GetTagValue(&runningTimeEnv->tagList, TAG_IAC_ONLINE),GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_ONLINE));
	DPRINTF("IAC DEFAULT:");
	printByteArray(GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DEFAULT),GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_DEFAULT));
	
	DPRINTF("TAC DENIAL:");
	printByteArray(runningTimeEnv->commonRunningData.denialTerminalActionCode,5);
	DPRINTF("TAC ONLINE:");
	printByteArray(runningTimeEnv->commonRunningData.onlineTerminalActionCode,5);
	DPRINTF("TAC DEFAULT:");
	printByteArray(runningTimeEnv->commonRunningData.defaultTerminalActionCode,5);

	//Jason modified on 2017.05.05
	if(GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_DENIAL) == 0)
	{
		if (actionCodeIsChecked(pTVR, runningTimeEnv->commonRunningData.denialTerminalActionCode,
		NULL, defaultIccDenailActionCode))
		{
			DPRINTF("IAC DENIAL\n");
			return EMV_RESULT_REJECT;
		
		}
	}else
	{
		if (actionCodeIsChecked(pTVR, runningTimeEnv->commonRunningData.denialTerminalActionCode,
			GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DENIAL), defaultIccDenailActionCode))
		{
			DPRINTF("IAC DENIAL\n");	
			return EMV_RESULT_REJECT;
		}
	}

	if (terminalCanGoOnline(runningTimeEnv))
	{
		DPRINTF("terminal can go online\n");
		if (GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_ONLINE) == 0)
		{
			if (actionCodeIsChecked(pTVR, runningTimeEnv->commonRunningData.onlineTerminalActionCode,
				NULL, defaultIccOnlineActionCode))
			{
				DPRINTF("IAC ONLINE\n");
				return EMV_RESULT_ONLINE;
			}
		}else
		{
			if (actionCodeIsChecked(pTVR, runningTimeEnv->commonRunningData.onlineTerminalActionCode,
				GetTagValue(&runningTimeEnv->tagList, TAG_IAC_ONLINE), defaultIccOnlineActionCode))
			{
				DPRINTF("IAC ONLINE\n");
				return EMV_RESULT_ONLINE;
			}
		}
	}
	else
	{
		DPRINTF("terminal can't go online\n");
		if (GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_DEFAULT) == 0)
		{
			if (actionCodeIsChecked(pTVR, runningTimeEnv->commonRunningData.defaultTerminalActionCode,
				NULL, defaultIccDefaultActionCode))
			{
				DPRINTF("IAC DEFAULT\n");
				return EMV_RESULT_REJECT;
			}
		}else
		{
			if (actionCodeIsChecked(pTVR, runningTimeEnv->commonRunningData.defaultTerminalActionCode,
				GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DEFAULT), defaultIccDefaultActionCode))
			{
				DPRINTF("IAC DEFAULT\n");
				return EMV_RESULT_REJECT;
			}
		}
	}
	//Jason modified end, 2017.05.05

	//Jason add start, 20101104
	if (terminalIsOnlineOnly(runningTimeEnv) == TRUE)
	{
		DPRINTF("Online only, request ARQC\n");
		return EMV_RESULT_ONLINE;
	}
	//Jason add end
	
	DPRINTF("EMV_RESULT_GOOD_OFFLINE\n");
	return EMV_RESULT_GOOD_OFFLINE;
}

//------------------------------------------------------------------------------------------
tti_uint8 getSecondTerminalActionAnalysis(RunningTimeEnv *runningTimeEnv)
{
	tti_byte *pTVR;
	tti_byte defaultIccDenailActionCode[TAC_SIZE]="\x00\x00\x00\x00\x00";
	tti_byte defaultIccDefaultActionCode[TAC_SIZE]="\xFF\xFF\xFF\xFF\xFF";

	pTVR=GetTagValue(&runningTimeEnv->tagList, TAG_TVR);
	if (pTVR==NULL)
	{
		return EMV_RESULT_REJECT;
	}
	if (actionCodeIsChecked(pTVR, runningTimeEnv->commonRunningData.denialTerminalActionCode,
		GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DENIAL), defaultIccDenailActionCode))
	{
		return EMV_RESULT_REJECT;
	}

	if (actionCodeIsChecked(pTVR, runningTimeEnv->commonRunningData.defaultTerminalActionCode,
		GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DEFAULT), defaultIccDefaultActionCode))
	{
		return EMV_RESULT_REJECT;
	}

	return EMV_RESULT_GOOD_OFFLINE;
}

//------------------------------------------------------------------------------------------
void checkAppVersion(RunningTimeEnv *runningTimeEnv)
{
	DPRINTF("check app version\n");
	
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_ICC_APP_VERSION_NUMBER)
		&&TagIsExisted(&runningTimeEnv->tagList, TAG_APP_VERSION_NUMBER))
	{
		DPRINTF("Terminal App Version:");
		printByteArray(GetTagValue(&runningTimeEnv->tagList, TAG_APP_VERSION_NUMBER), GetTagValueSize(&runningTimeEnv->tagList, TAG_APP_VERSION_NUMBER));
		DPRINTF("\n");
		DPRINTF("ICC App Version:");
		printByteArray(GetTagValue(&runningTimeEnv->tagList, TAG_ICC_APP_VERSION_NUMBER), GetTagValueSize(&runningTimeEnv->tagList, TAG_ICC_APP_VERSION_NUMBER));
		DPRINTF("\n");
		if (0!=memcmp(GetTagValue(&runningTimeEnv->tagList, TAG_ICC_APP_VERSION_NUMBER), 
			GetTagValue(&runningTimeEnv->tagList, TAG_APP_VERSION_NUMBER), 
			APP_VERSION_NUMBER_SIZE))
		{
			SetTVR(TVR_APP_VERSION_IS_NOT_SAME);
		}
	}
}

//------------------------------------------------------------------------------------------
tti_bool IsDomesticTransaction(RunningTimeEnv *runningTimeEnv)
{
	if(memcmp(GetTagValue(&runningTimeEnv->tagList, TAG_TERM_COUNTRY_CODE) ,
 		GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_COUNTRY_CODE), 2) == 0)
 	{
 		return TRUE;
 	}
 	else
 	{
 		return FALSE;
 	}
}

tti_bool isATM(RunningTimeEnv *runnintTimeEnv)
{
	tti_byte *pTerminalType, *pTerminalCap;

	pTerminalType=GetTagValue(&runnintTimeEnv->tagList, TAG_TERMINAL_TYPE);
	pTerminalCap=GetTagValue(&runnintTimeEnv->tagList, TAG_ADDITIONAL_TERM_CAPABILITY);
	switch (*pTerminalType)
	{
		case 0x14:
		case 0x15:
		case 0x16:
			if (((*pTerminalCap)&0x80)!=0)	//support cash
			{
				return TRUE;
			}
			break;
		default:
			break;
	}

	return FALSE;
}

//------------------------------------------------------------------------------------------
void checkAppUsageControl(RunningTimeEnv *runningTimeEnv)
{
	tti_byte *pAUC, *pTransType;

	DPRINTF("check app usage control\n");
	
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_AUC))
	{
		pAUC=GetTagValue(&runningTimeEnv->tagList, TAG_AUC);

		if (isATM(runningTimeEnv))
		{
			if (!getAppUsageControl(AUC_ATM))
			{
				SetTVR(TVR_SERVICE_NOT_ALLOWED);
			}
		}
		else
		{
			if (!getAppUsageControl(AUC_TERMINAL))
			{
				SetTVR(TVR_SERVICE_NOT_ALLOWED);
			}
		}
		
		if (TagIsExisted(&runningTimeEnv->tagList, TAG_ISSUER_COUNTRY_CODE))
		{
			pTransType=GetTagValue(&runningTimeEnv->tagList, TAG_TRANSACTION_TYPE);
			switch (*pTransType)
			{
				case TRANS_TYPE_GOODS_AND_SERVICE:
					if (IsDomesticTransaction(runningTimeEnv))
					{
						if ((!getAppUsageControl(AUC_DOMESTIC_GOODS))
							&&(!getAppUsageControl(AUC_DOMESTIC_SERVICE)))
						{
							SetTVR(TVR_SERVICE_NOT_ALLOWED);
						}
					}
					else
					{
						if ((!getAppUsageControl(AUC_INTERNATIONAL_GOODS))
							&&(!getAppUsageControl(AUC_INTERNATIONAL_SERVICE)))
						{
							SetTVR(TVR_SERVICE_NOT_ALLOWED);
						}
					}
					break;
				case TRANS_TYPE_CASH:
					if (IsDomesticTransaction(runningTimeEnv))
					{
						if (!getAppUsageControl(AUC_DOMESTIC_CASH))
						{
							SetTVR(TVR_SERVICE_NOT_ALLOWED);
						}
					}
					else
					{
						if (!getAppUsageControl(AUC_INTERNATIONAL_CASH))
						{
							SetTVR(TVR_SERVICE_NOT_ALLOWED);
						}
					}
					break;
				case TRANS_TYPE_CASHBACK:
					if (IsDomesticTransaction(runningTimeEnv))
					{
						if (!getAppUsageControl(AUC_DOMESTIC_CASHBACK))
						{
							SetTVR(TVR_SERVICE_NOT_ALLOWED);
						}
					}
					else
					{
						if (!getAppUsageControl(AUC_INTERNATIONAL_CASHBACK))
						{
							SetTVR(TVR_SERVICE_NOT_ALLOWED);
						}
					}
				break;
				default:
					break;
			}
		}
	}
}

//------------------------------------------------------------------------------------------
tti_int32 CompareDateInBCDFormat(tti_byte *bcdDate1,tti_byte *bcdDate2)
{
	if (((bcdDate1[0]>=0x50)&&(bcdDate2[0]>=0x50))||
		((bcdDate1[0]<0x50)&&(bcdDate2[0]<50)))
	{
		return memcmp(bcdDate1, bcdDate2, DATE_SIZE/2);
	}
	else
	{
		if (bcdDate1[0]>=50)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
}

//------------------------------------------------------------------------------------------
void checkAppBecomeEffective(RunningTimeEnv *runningTimeEnv)
{
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE))
	{
		if (CompareDateInBCDFormat(GetTagValue(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE),
			GetTagValue(&runningTimeEnv->tagList, TAG_TRANSACTION_DATE))>0)
		{
			SetTVR(TVR_APP_NOT_EFFECTIVE);
		}
	}

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE))
	{
		if (CompareDateInBCDFormat(GetTagValue(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE),
			GetTagValue(&runningTimeEnv->tagList, TAG_TRANSACTION_DATE))<0)
		{
			SetTVR(TVR_EXPIRED_APPLICATION);
		}
	}
}

tti_bool checkQpbocAppBecomeExpiration(RunningTimeEnv *runningTimeEnv)
{
	//LOGD("Enter checkQpbocAppBecomeExpiration");

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE))
	{
		//LOGD("TAG_APP_EXPIRATION_DATE exist");
		if (CompareDateInBCDFormat(GetTagValue(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE),
			GetTagValue(&runningTimeEnv->tagList, TAG_TRANSACTION_DATE))<0)
		{
			//LOGD("TAG_APP_EXPIRATION_DATE < TAG_TRANSACTION_DATE");
			return TRUE;
		}
	}
	//else
	//{
		//LOGD("TAG_APP_EXPIRATION_DATE not exist");
	//}

	return FALSE;
}


//------------------------------------------------------------------------------------------
void ProcessingRestrictions(RunningTimeEnv *runningTimeEnv)
{
	DPRINTF("processing restrictions\n");
	checkAppVersion(runningTimeEnv);

	checkAppUsageControl(runningTimeEnv);

	checkAppBecomeEffective(runningTimeEnv);
 }

//------------------------------------------------------------------------------------------
tti_bool IsCurrencyCodeMatch(RunningTimeEnv *runningTimeEnv)
{
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_TRANS_CURRENCY_CODE)
		&&TagIsExisted(&runningTimeEnv->tagList, TAG_APP_CURRENCY_CODE))
	{
		return (memcmp(GetTagValue(&runningTimeEnv->tagList, TAG_TRANS_CURRENCY_CODE),
			GetTagValue(&runningTimeEnv->tagList, TAG_APP_CURRENCY_CODE), 2)==0);
	}
	else
	{
		return FALSE;
	}
}

//------------------------------------------------------------------------------------------
tti_bool isAttendTerminal(tti_byte terminalType)
{
	terminalType&=0x0F;

	if ((terminalType==0x01)||(terminalType==0x02)||(terminalType==0x03))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*/------------------------------------------------------------------------------------------
//bob delete 20101030 from here
tti_bool needPerformCVM(RunningTimeEnv *runningTimeEnv, tti_byte condition)
{
	tti_byte *pTransType, *pTerminalType;
	tti_int32 amount;
	tti_int32 amountX, amountY;
	tti_byte *cvmList;

	printf("holder : check condition\n");
	
	pTransType=GetTagValue(&runningTimeEnv->tagList, TAG_TRANSACTION_TYPE);
	pTerminalType=GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_TYPE);
	amount=getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT);
	cvmList=GetTagValue(&runningTimeEnv->tagList, TAG_CVM);
	amountX=translateByteArrayToInt(cvmList, CVM_AMOUNT_X_SIZE);
	amountY=translateByteArrayToInt(cvmList+CVM_AMOUNT_X_SIZE, CVM_AMOUNT_Y_SIZE);

	switch (condition)
	{
		case CVM_ALWAYS:
			return TRUE;
			break;
		case CVM_IF_SUPPORTED_CVM:
			return TRUE;
			break;
		case CVM_IF_UNATTENDED_CASH:
			return (!isAttendTerminal(*pTerminalType)&&(*pTransType==TRANS_TYPE_CASH));
			break;
		case CVM_IF_NO_MANUAL_CASH_AND_CASHBACK:
			return ((*pTransType!=TRANS_TYPE_CASHBACK)&&(*pTransType!=TRANS_TYPE_CASH));
 			break;
		case CVM_IF_MANUAL_CASH:
			return (isAttendTerminal(*pTerminalType)&&(*pTransType==TRANS_TYPE_CASH));
			break;
		case CVM_IF_PURCHASE_CASHBACK:
			return (*pTransType==TRANS_TYPE_CASHBACK);
			break;
		case CVM_IF_UNDER_AMOUNT_X:
			if (IsCurrencyCodeMatch(runningTimeEnv))
			{
				return (amount<amountX);
			}
				
			break;
		case CVM_IF_OVER_AMOUNT_X:
			if (IsCurrencyCodeMatch(runningTimeEnv))
			{
				return (amount>amountX);
			}
			break;
		case CVM_IF_UNDER_AMOUNT_Y:
			if (IsCurrencyCodeMatch(runningTimeEnv))
			{
				return (amount<amountY);
			}
			break;
		case CVM_IF_OVER_AMOUNT_Y:
			if (IsCurrencyCodeMatch(runningTimeEnv))
			{
				return (amount>amountY);
			}
		default:
			break;
	}
	
	return FALSE;
}
//bob delete end  20101030
//
*/

//************************************************
//bob add from here, modified this funcution because no cvm required should be execute by kernel
tti_bool needPerformCVM(RunningTimeEnv *runningTimeEnv, tti_byte performed, tti_byte condition)
{
	tti_byte *pTransType, *pTerminalType;
	tti_int32 amount;
	tti_int32 amountX, amountY;
	tti_byte *cvmList;

	DPRINTF("holder : check condition\n");
	
	pTransType=GetTagValue(&runningTimeEnv->tagList, TAG_TRANSACTION_TYPE);
	pTerminalType=GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_TYPE);
	amount=getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT);
	cvmList=GetTagValue(&runningTimeEnv->tagList, TAG_CVM);
	amountX=translateByteArrayToInt(cvmList, CVM_AMOUNT_X_SIZE);
	amountY=translateByteArrayToInt(cvmList+CVM_AMOUNT_X_SIZE, CVM_AMOUNT_Y_SIZE);

	switch (condition)
	{
		case CVM_ALWAYS:
			return TRUE;
		//	break;
		case CVM_IF_SUPPORTED_CVM:
			//add some cases 
			//Jason edit 20110828
			if (( (performed&0x3f) == CVM_NO_REQUIRE) && terminalSupport(TERM_CAP_NO_CVM_REQUIRED))
			{
				return TRUE;
			}
			if (( (performed&0x3f) == CVM_OFFLINE_ENCIPHER_PIN) && terminalSupport(TERM_CAP_ENCIPHER_PIN))
			{
				return TRUE;
			}
			if (( (performed&0x3f) == CVM_OFFLINE_PLAIN_PIN) && terminalSupport(TERM_CAP_PLAIN_PIN))
			{
				return TRUE;
			}

			//Jason added 2012. start
			if (( (performed&0x3f) == CVM_CREDENTIAL) && terminalSupport(TERM_CAP_CREDENTIAL))
			{
				return TRUE;
			}
			if (( (performed&0x3f) == CVM_OFFLINE_ENCIPHER_PIN_AND_SIGN) && terminalSupport(TERM_CAP_ENCIPHER_PIN)
					&& terminalSupport(TERM_CAP_SIGNATURE))
			{
				return TRUE;
			}
			if (( (performed&0x3f) == CVM_OFFLINE_PLAIN_PIN_AND_SIGN) && terminalSupport(TERM_CAP_PLAIN_PIN)
					&& terminalSupport(TERM_CAP_SIGNATURE))
			{
				return TRUE;
			}
			if (( (performed&0x3f) == CVM_SIGN) && terminalSupport(TERM_CAP_SIGNATURE))
			{
				return TRUE;
			}
			
			if (( (performed&0x3f) == CVM_ONLINE_ENCIPHER_PIN) && terminalSupport(TERM_CAP_ONLINE_PIN))
			{
				return TRUE;
			}
			
			//End
			if ( (performed&0x3f) == CVM_FAIL_PROCESS)
			{
				return TRUE;
			}
			
			return FALSE;
			
		case CVM_IF_UNATTENDED_CASH:
			return (!isAttendTerminal(*pTerminalType)&&(*pTransType==TRANS_TYPE_CASH));
			
		case CVM_IF_NO_MANUAL_CASH_AND_CASHBACK:
			return ((*pTransType!=TRANS_TYPE_CASHBACK)&&(*pTransType!=TRANS_TYPE_CASH));
 			
		case CVM_IF_MANUAL_CASH:
			return (isAttendTerminal(*pTerminalType)&&(*pTransType==TRANS_TYPE_CASH));
			
		case CVM_IF_PURCHASE_CASHBACK:
			return (*pTransType==TRANS_TYPE_CASHBACK);
			
		case CVM_IF_UNDER_AMOUNT_X:
			if (IsCurrencyCodeMatch(runningTimeEnv))
			{
				return (amount<amountX);
			}
			break;
			
		case CVM_IF_OVER_AMOUNT_X:
			if (IsCurrencyCodeMatch(runningTimeEnv))
			{
				return (amount>amountX);
			}
			break;
			
		case CVM_IF_UNDER_AMOUNT_Y:
			if (IsCurrencyCodeMatch(runningTimeEnv))
			{
				return (amount<amountY);
			}
			break;
			
		case CVM_IF_OVER_AMOUNT_Y:
			if (IsCurrencyCodeMatch(runningTimeEnv))
			{
				return (amount>amountY);
			}
			
		default:
			break;
	}
	
	return FALSE;
}
//bob add end
//***********************************************************/

//------------------------------------------------------------------------------------------
tti_int32 sign(RunningTimeEnv *runningTimeEnv)
{
	DPRINTF("sign\n");

	if (!terminalSupport(TERM_CAP_SIGNATURE))
	{
		return CVM_RESULT_NOT_SUPPORT;
	}
	else
	{
		runningTimeEnv->commonRunningData.signatureRequired=1;
		return CVM_RESULT_UNKNOWN;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 offlinePlainPin(RunningTimeEnv *runningTimeEnv)
{
	tti_tchar pinNumber[PIN_NUMBER_SIZE+1];
	tti_byte formatedPinNumber[8];
	tti_int32 status;
	tti_byte *pPinTryCounter;
	tti_byte retryTime;
	tti_tchar retryTimeDisplay[20+1];
	tti_int32 iRet;
	tti_tchar displayPrompt[100];

	DPRINTF("offline plain pin\n");

	if (!terminalSupport(TERM_CAP_PLAIN_PIN))
	{
		if (!terminalSupport(TERM_CAP_ENCIPHER_PIN))
		{
			SetTVR(TVR_NEEDED_PINPAD_NOT_WORKING);
		}
		return CVM_RESULT_NOT_SUPPORT;
	}
	
	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_PIN_TRY_COUNTER))
	{
		getSpecialDataFromICC(runningTimeEnv, TAG_PIN_TRY_COUNTER);
	}
	
	pPinTryCounter=GetTagValue(&runningTimeEnv->tagList, TAG_PIN_TRY_COUNTER);
	if (pPinTryCounter!=NULL)
	{
		if ((*pPinTryCounter)==0)
		{
			SetTVR(TVR_PIN_TRY_LIMIT_EXCEEDDED);
			return CVM_RESULT_FAIL;
		}
	}

	if (runningTimeEnv->commonRunningData.bypassPinEntry)
	{
		return CVM_RESULT_FAIL;
	}

    ReportLine();

	
	strcpy(displayPrompt, "请输入脱机明文PIN");

    ReportLine();

	if (pPinTryCounter!=NULL)
	{
		if ((*pPinTryCounter) == 1)
		{
			strcpy(displayPrompt, "最后一次脱机明文PIN");
		}
	}

    ReportLine();

	memset(pinNumber, 0, sizeof(pinNumber));
	while (TRUE)
	{
	//Jason edit on 2013.05.23, start
	/*
		if (EditorLine(3, 0, pinNumber, PIN_NUMBER_SIZE)==STATUS_CANCEL)
		{
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				continue;
			}
			SetTVR(TVR_PIN_WAS_NOT_ENTERED);
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
			return CVM_RESULT_FAIL;
		}
	*/
        ReportLine();

		//iRet = EditorLine(3, 0, pinNumber, PIN_NUMBER_SIZE);
		iRet = GetPINEnterFromUI(displayPrompt, pinNumber);
        DPRINTF("GetPINEnterFromUI() = %d, strlen(pinNumber) = %d, pinNumber[0] = 02x%", iRet, strlen(pinNumber), pinNumber[0]);
		if (iRet == STATUS_OK && strlen(pinNumber) == 0)
		{
			//Try to by pass
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				continue;
			}
            DPRINTF("Offline PIN is bypass");
			SetTVR(TVR_PIN_WAS_NOT_ENTERED);
            DPRINTF("Already set TVR_PIN_WAS_NOT_ENTERED");
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
			return CVM_RESULT_FAIL;
		}else if (iRet == STATUS_CANCEL)
		{
			//Cancel key to exit
			//return CVM_RESULT_CANCEL;
			//Changed to bypass
			//Try to by pass
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				continue;
			}
			SetTVR(TVR_PIN_WAS_NOT_ENTERED);
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
			return CVM_RESULT_FAIL;
		}

	//End
		formatPlainPin(pinNumber, formatedPinNumber);
		status=SmartCardVerifyPin(formatedPinNumber, 8, VERIFY_PIN_TYPE_PLAINTEXT, &retryTime);
		if (status==STATUS_OK)
		{
			return CVM_RESULT_SUCCESS;
		}
		if (status==STATUS_PIN_RETRY_LIMIT)
		{
			SetTVR(TVR_PIN_TRY_LIMIT_EXCEEDDED);
			return CVM_RESULT_FAIL;
		}
		if (status==STATUS_ERROR)
		{
			return CVM_RESULT_ERROR;
		}

		//ClearScreen();
		//LCD_DispLine(1, 0, PLAIN_PIN_STRING_CHINESE, strlen(PLAIN_PIN_STRING_CHINESE), 0);		
		//DisplayInfoLeft(runningTimeEnv->enterOfflinePlainPinString, 0);
		if ( retryTime == 1)
		{
			//DisplayInfoLeft(runningTimeEnv->lastPintryString, 2);
			strcpy(displayPrompt, "最后一次脱机明文PIN");
		}
		else
		{
			sprintf(retryTimeDisplay, "(%d)", retryTime);
			//DisplayInfoLeft(retryTimeDisplay, 1);	//Remove PIN retry times 2011.09.23 Siken
		}
	}
}

#define ONLINE_DES_KEY	"12345678"


//Jason added on 2012.04.24, start
//------------------------------------------------------------------------------------------
#if 0
void EConlineEncipherPin(RunningTimeEnv *runningTimeEnv)
{
	tti_tchar pinRequest[BUFFER_SIZE_256BYTE];
	tti_tchar pinResponse[BUFFER_SIZE_256BYTE];
	tti_int32 respLen;
	//tti_int32 panStringLen;
	//tti_tchar panString[BUFFER_SIZE_32BYTE];
	//tti_tchar formatedPAN[BUFFER_SIZE_32BYTE];

	DPRINTF("EC online encipher pin\n");

ecredo:
	ClearScreen();
	//LCD_DispLine(1, 0, ONLINE_PIN_STRING_CHINESE, strlen(ONLINE_PIN_STRING_CHINESE), 0);
	DisplayInfoLeft(runningTimeEnv->enterOnlinePinString, 0);
/*
	translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_PAN), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_PAN), panString);

	panStringLen=strlen(panString);
	panString[panStringLen-1]='\0';
	panStringLen--;

	if (panStringLen>=12)

	{
		strcpy(formatedPAN, panString+panStringLen-12);
	}
	else
	{
		strcpy(formatedPAN, panString);
	}
	FormatString(formatedPAN, 16, '0', MESSAGE_RIGHT_JUSTIFY);
	
	sprintf(pinRequest, "000%s%s    ", formatedPAN, runningTimeEnv->sessionKey);
*/
	strcpy(pinRequest, "000");
	RequestPinDataEntry(pinRequest, strlen(pinRequest), pinResponse, &respLen);

	pinResponse[respLen]='\0';

	if (pinResponse[3]=='0')
	{
		strcpy(runningTimeEnv->commonRunningData.pinBlock, &pinResponse[6]);
		//SetTVR(TVR_ONLINE_PIN_ENTERED);
		return;
	}
	else 
	{
		strcpy(runningTimeEnv->commonRunningData.pinBlock, "");
		if (pinResponse[3]=='3')
		{
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				goto ecredo;
			}
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
			//SetTVR(TVR_PIN_WAS_NOT_ENTERED);
		}
		return;
	}
}
//End
#else
void EConlineEncipherPin(RunningTimeEnv *runningTimeEnv)
{
	//tti_tchar pinRequest[BUFFER_SIZE_256BYTE];
	tti_tchar pinResponse[BUFFER_SIZE_256BYTE];
	//tti_int32 respLen;
	int iRet;
	tti_byte PinBlock[30] = {0};
	tti_byte tmp1[50] = {0};
	tti_byte tmp2[50] = {0};
	
	DPRINTF("EC online encipher pin\n");

ecredo:
	
	iRet = GetPINEnterFromUI("请输入联机PIN", pinResponse);
	if (iRet == STATUS_OK &&strlen(pinResponse) != 0)
	{
		BuildPinBlock( (tti_byte *)pinResponse, strlen(pinResponse), PinBlock);
		memset (tmp1, 0, sizeof(tmp1));
		ucl_desEncrypt(PinBlock, ONLINE_DES_KEY, tmp1);

		HextoA((char *)tmp2, tmp1, 8);
		strcpy(runningTimeEnv->commonRunningData.pinBlock,(char *) tmp2);
		DPRINTF("pinBlock = %s, %s", tmp2, runningTimeEnv->commonRunningData.pinBlock);
		
		strcpy(runningTimeEnv->commonRunningData.pinBlock, pinResponse);

		SetTagValue(TAG_ONLINE_PIN_BLOCK_BCTC, tmp1, 8, 
			&(runningTimeEnv->tagList));
		
		return;
	}
	else 
	{
		strcpy(runningTimeEnv->commonRunningData.pinBlock, "");
		if (iRet == STATUS_OK &&strlen(pinResponse) == 0)
		{
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				goto ecredo;
			}
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
		}else if (iRet == STATUS_CANCEL)
		{
			//Cancel key to exit
			//return CVM_RESULT_CANCEL;
			//Changed to bypass
			//Try to by pass
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				goto ecredo;
			}
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
		}
		return;
	}
}
#endif

tti_int32 GetOnlinePINEnterFromUI(tti_tchar *keyIndex, tti_tchar * szPAN, tti_tchar *szPINBlockOut);


//#ifdef EMVL2_TEST_ENTER_PIN
#ifdef BCTC_EMVL2_TEST
//------------------------------------------------------------------------------------------
tti_int32 onlineEncipherPin(RunningTimeEnv *runningTimeEnv)
{
	//tti_tchar pinRequest[BUFFER_SIZE_256BYTE];
	//tti_tchar pinResponse[BUFFER_SIZE_256BYTE];
	//tti_int32 respLen; 
	tti_int32 panStringLen;
	tti_tchar panString[BUFFER_SIZE_32BYTE];
	tti_tchar formatedPAN[BUFFER_SIZE_32BYTE];
	//Jason modify 2013.05.22
	int iRet;
	tti_tchar pinNumber[PIN_NUMBER_SIZE+1] = {0};
	tti_byte PinBlock[30] = {0};
	
	//for debug
	tti_byte tmp1[50] = {0};
	tti_byte tmp2[50] = {0};
	tti_tchar szDisplayPrompt[50];

	DPRINTF("online encipher pin\n");

	if (!terminalSupport(TERM_CAP_ONLINE_PIN))
	{
		SetTVR(TVR_NEEDED_PINPAD_NOT_WORKING);
		return CVM_RESULT_NOT_SUPPORT;
	}

	if (runningTimeEnv->commonRunningData.bypassPinEntry)
	{
		return CVM_RESULT_FAIL;
	}

//redo:
	strcpy(szDisplayPrompt, "请输入联机PIN");

	translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_PAN), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_PAN), panString);

	panStringLen=strlen(panString);
	panString[panStringLen-1]='\0';
	panStringLen--;

	if (panStringLen>=12)
	{
		strcpy(formatedPAN, panString+panStringLen-12);
	}
	else
	{
		strcpy(formatedPAN, panString);
	}
	FormatString(formatedPAN, 16, '0', MESSAGE_RIGHT_JUSTIFY);

	while (TRUE)
	{
		//iRet = EditorLineForOnlinePIN(3, 0, pinNumber, PIN_NUMBER_SIZE);
		iRet = GetPINEnterFromUI(szDisplayPrompt, pinNumber);
		if (iRet ==STATUS_OK && strlen(pinNumber) == 0)
		{
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				DPRINTF("pinBypassMode not allow\n");
				continue;
			}
			SetTVR(TVR_PIN_WAS_NOT_ENTERED);
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				DPRINTF("pinBypassMode PIN_BYPASS_NEXT_ALLOW\n");
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
			return CVM_RESULT_FAIL;
		}
		if (iRet == STATUS_CANCEL)
		{
			//Cancel key to exit
			//return CVM_RESULT_CANCEL;
			//Changed to bypass
			//Try to by pass
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				continue;
			}
			SetTVR(TVR_PIN_WAS_NOT_ENTERED);
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
			return CVM_RESULT_FAIL;
		}
		if (iRet == STATUS_OK && strlen(pinNumber) != 0)
		{
			break;
		}

		//ClearScreen();
		//DisplayInfoLeft(runningTimeEnv->enterOnlinePinString, 0);
	}

	
	BuildPinBlock( pinNumber, strlen(pinNumber), PinBlock);
	HextoA(tmp1, PinBlock, 8);
	//TtiOsDebugPrintf("PinBlock is %s", tmp1);

	memset (tmp1, 0, sizeof(tmp1));
	//For Z5 just remove for compile, need implenet with UCL.
	ucl_desEncrypt(PinBlock, ONLINE_DES_KEY, tmp1);
	//LOGD("PinBlock list");
	//hexdump(PinBlock, 8);
	//LOGD("tmp1 list");
	//hexdump(tmp1, 8);
	
	HextoA(tmp2, tmp1, 8);
	strcpy(runningTimeEnv->commonRunningData.pinBlock, tmp2);
	DPRINTF("pinBlock = %s, %s", tmp2, runningTimeEnv->commonRunningData.pinBlock);

	SetTagValue(TAG_ONLINE_PIN_BLOCK_BCTC, tmp1, 8, 
		&(runningTimeEnv->tagList));
	SetTVR(TVR_ONLINE_PIN_ENTERED);
	return CVM_RESULT_UNKNOWN;	
}
#else
tti_int32 onlineEncipherPin(RunningTimeEnv *runningTimeEnv)
{
	//tti_int32 panStringLen;
	tti_tchar panString[BUFFER_SIZE_32BYTE + 1];
	//Jason modify 2013.05.22
	int iRet;
	tti_tchar PinBlockStr[33] = {0};
	tti_byte PinBlockHex[16 + 1] = {0};
    char qPbocFlag=0;

	//for debug
	//tti_byte tmp[512];
    
    if(gl_TransRec.uiEntryMode/10==7)
        qPbocFlag=1;
    
    strcpy(runningTimeEnv->commonRunningData.pinBlock, "");
	DPRINTF("online encipher pin !\n");
	//DPRINTF("test 512 GetRandNumber\n");
	//GetRandNumber(tmp, 512);

	if (!terminalSupport(TERM_CAP_ONLINE_PIN))
	{
		SetTVR(TVR_NEEDED_PINPAD_NOT_WORKING);
		return CVM_RESULT_NOT_SUPPORT;
	}

	if (qPbocFlag==0 && runningTimeEnv->commonRunningData.bypassPinEntry)
	{
		return CVM_RESULT_FAIL;
	}

//redo:
    if(TagIsExisted(&runningTimeEnv->tagList, TAG_PAN))
    {
        translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_PAN),
            GetTagValueSize(&runningTimeEnv->tagList, TAG_PAN), panString);
    }else if(TagIsExisted(&runningTimeEnv->tagList, TAG_TRACK2))
    {
        char *p;
        translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_TRACK2), 10, panString);

        p=strchr(panString, 'D');
        if(p)
            *p=0;
        else
            panString[19]=0;
        dbg("get pan:%s\n", panString);
    }
	while (TRUE)
	{
		iRet = GetOnlinePINEnterFromUI("0", panString, PinBlockStr);
		if (iRet ==STATUS_OK && strlen(PinBlockStr) == 0)
		{
			if(qPbocFlag==0)
			{
				//接触卡判断BYPASS
				if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
				{
					DPRINTF("pinBypassMode not allow\n");
					//vMessage("pinBypassMode not allow");
					mdelay(1);
					continue;
				}
				SetTVR(TVR_PIN_WAS_NOT_ENTERED);
				if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
				{
					DPRINTF("pinBypassMode PIN_BYPASS_NEXT_ALLOW\n");
					runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
				}
			}
			return CVM_RESULT_FAIL;
		}
        #ifndef VPOS_APP		//xfdebug
		if (iRet == STATUS_CANCEL)
		{
			//Cancel key to exit
			//return CVM_RESULT_CANCEL;
			//Changed to bypass
			//Try to by pass
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				continue;
			}
			SetTVR(TVR_PIN_WAS_NOT_ENTERED);
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
			return CVM_RESULT_FAIL;
		}
		else if (iRet == STATUS_FAIL)
		{
			return CVM_RESULT_FAIL;
		}
        #else
        if (iRet != STATUS_OK)
            return CVM_RESULT_CANCEL;
        #endif
		if (iRet == STATUS_OK && strlen(PinBlockStr) != 0)
		{
            break;
		}
	}

	strcpy(runningTimeEnv->commonRunningData.pinBlock, PinBlockStr);

	AtoHex(PinBlockHex, PinBlockStr, strlen(PinBlockStr));

	SetTagValue(TAG_ONLINE_PIN_BLOCK_BCTC, PinBlockHex, strlen(PinBlockStr)/2,
		&(runningTimeEnv->tagList));
    
    if(qPbocFlag==0)
        SetTVR(TVR_ONLINE_PIN_ENTERED);
    else
        setCVMResult(0x02, 0x00, 0x00);
    
	return CVM_RESULT_UNKNOWN;
}
#endif

//------------------------------------------------------------------------------------------
tti_int32 offlinePlainPinAndSign(RunningTimeEnv *runningTimeEnv)
{
	tti_int32 status;

	DPRINTF("offline plain pin and sign\n");

	if (!terminalSupport(TERM_CAP_PLAIN_PIN))
	{
		if (!terminalSupport(TERM_CAP_ENCIPHER_PIN))
		{
			SetTVR(TVR_NEEDED_PINPAD_NOT_WORKING);
		}
		return CVM_RESULT_NOT_SUPPORT;
	}

	if (!terminalSupport(TERM_CAP_SIGNATURE))
	{
		return CVM_RESULT_NOT_SUPPORT;
	}

	status=offlinePlainPin(runningTimeEnv);
	if (status==CVM_RESULT_ERROR)
	{
		return CVM_RESULT_ERROR;
	}
	else if (status==CVM_RESULT_FAIL)
	{
		return CVM_RESULT_FAIL;
	}
	else
	{
		return sign(runningTimeEnv);
	}
}

//------------------------------------------------------------------------------------------
tti_int32 GetRandNumber(tti_byte *randArray, tti_int32 randArrayLen)
{
#ifndef CY20_EMVL2_KERNEL
	tti_int32 unPredicatableNumber;
	tti_int32 loop;
	tti_byte tmp[100];

	DPRINTF("GetRandNumber [%d]\n", randArrayLen);
	
	//Jason start edit
	srand((tti_int32)time(NULL));
	//Jason end edit
	for(loop = 0; loop < randArrayLen; loop++)
	{
		//Jason edit 2013.05.21, start
		//unPredicatableNumber = rand_hardware();
		unPredicatableNumber = rand();
		//unPredicatableNumber = rand_hardware_byte(tmp);
		//Jason edit 2013.05.21, end
		
		//printf("unPredicatableNumber is [%ld] \n", unPredicatableNumber);
		randArray[loop] = unPredicatableNumber%255;
	}
	DPRINTF("random number :\n");
	hexdump(randArray, randArrayLen);
	/*
	fd=open("/dev/rng", O_RDONLY);

	if(fd < 0)
	{
		return STATUS_FAIL;
	}

	read(fd, randArray, randArrayLen);
	close(fd);
	*/
	return STATUS_OK;
#else
	rand_hardware_byte_ex(randArray, randArrayLen);

	return STATUS_OK;
#endif
}


#if 0

tti_int32 GetPINEnterFromUI(tti_tchar *szDisplayString, tti_tchar *szPINValue)
{
	giPINEnterStatus = -1;
	memset(gszPINValue, 0, sizeof(gszPINValue));

    ReportLine();
    c_StartEnterPIN(szDisplayString);

	while(1)
	{
		if (giPINEnterStatus == -1)
		{
			//DPRINTF("No enter PIN");
			usleep(1000*100);
		}else {
			DPRINTF("giPINEnterStatus = %d", giPINEnterStatus);
			switch (giPINEnterStatus)
			{
				//Enter PIN
				case 0:
					DPRINTF("Enter PIN");
					strcpy(szPINValue, gszPINValue);
                    DPRINTF("gszPINValue = %s", gszPINValue);
					return STATUS_OK;

				//Cancel
				case 1:
					DPRINTF("Cancel PIN");
					szPINValue[0] = '\0';
					return STATUS_CANCEL;

				//No Enter PIN just enter OK.
				case 2:
					DPRINTF("Just enter OK");
					szPINValue[0] =  '\0';
					return STATUS_OK;

				default:
					return STATUS_FAIL;
			}
		}
	}

    return STATUS_FAIL;
}
#endif

tti_bool gPinAutoClose = FALSE;
tti_tchar gAutoPin[20] = "";


tti_bool IsPinAutoClose()
{
	return gPinAutoClose;
}

tti_tchar * GetAutoPin()
{
	if (gPinAutoClose == FALSE)
	{
		gAutoPin[0] = '\0';
	}
	
	return gAutoPin;
}

void SetPinAutoClose(tti_bool autoClose, char *autoPIN)
{
	gPinAutoClose = autoClose;

	if (autoClose == TRUE)
	{
		strcpy(gAutoPin, autoPIN);
	}else
	{
		gAutoPin[0] = '\0';
	}

	return;
}


int internalPinpadOnlinePin(tti_tchar *keyIndex, tti_tchar * szPAN, tti_tchar *szPINBlockOut)
{
	tti_byte cmd[100], rsp[100];
	tti_tchar panLenStr[10];
	int cmdLen, rspLen;

	memset(cmd, 0x00, sizeof(cmd));
	memcpy(cmd, "91.", 3);
	memcpy(cmd + 3, keyIndex, 1);
	sprintf(panLenStr, "%02d", strlen(szPAN));
	memcpy(cmd + 3 + 1, panLenStr, 2);
	memcpy(cmd + 3 + 1 + 2, szPAN, strlen(szPAN));
	cmdLen = strlen(szPAN) + 6;

	DPRINTF("Before InternalPinpadCmd\n");
	InternalPinpadCmd(cmd, cmdLen, rsp, &rspLen);
	rsp[rspLen] = '\0';
	switch (rsp[3])
	{
		//Enter PIN
		case '0':
			if (strlen((char *)rsp) != 20 && strlen((char *)rsp) != 36)
			{				
				DPRINTF("rsp[%s] len error", rsp);
				return STATUS_FAIL;
			}

			strcpy(szPINBlockOut, (char *)(rsp + 4));
			DPRINTF("szPINBlockOut = %s", szPINBlockOut);
			return STATUS_OK;

		//Timeout
		case '5':
			DPRINTF("Timeout");
			return STATUS_CANCEL;
			
			//Cancel
		case '3':
			DPRINTF("Cancel");
			return STATUS_CANCEL;

		default:
			return STATUS_FAIL;
	}
	
}

bool Check_CardBlackBin(char *cardno)
{
	u16 num,count;
        uchar  ucCardList[19+1];
		
	count = gl_Cardclblack.ulCardNum[0] * 256 + gl_Cardclblack.ulCardNum[1];  
	dbg("count:%d\n",count);	
        dbg("cardno:%s\n",cardno);
        dbg("strlen(cardno):%d\n",strlen(cardno));
	for(num = 0;num < count;num++)
	{
	       memset(ucCardList,0x00,sizeof(ucCardList));
	       memcpy(ucCardList,&gl_Cardclblack.ucCardList[num * 19],19);
	       rtrim(ucCardList);
	       dbg("ucCardList:%s\n",ucCardList);
	       dbg("cardlen:%d\n",strlen(ucCardList));   		   
                  
	       if(strlen(cardno) == strlen(ucCardList) && (!memcmp(cardno,ucCardList,strlen(ucCardList))))
	       	{
	       	        dbg("this is CardBlack\n");		
		   	break; 		
	       	}	
	}
	
	return num < count ? TRUE: FALSE;	
}

tti_int32 GetOnlinePINEnterFromUI(tti_tchar *keyIndex, tti_tchar * szPAN, tti_tchar *szPINBlockOut)
{
	tti_int32 ret;
	//tti_int32 len;
#ifdef VPOS_APP	
    {
        // -1：取消 -3：超时 >=0:密码长度(0为直接按确认键)
//        extern int iInputPinAndEnc(uchar ucPass, unsigned long ulAmount, char *pszPrompt, char *pszPan, char *pszEncPin);
        extern void vSetDispRetInfo(int flag);
        
        int iRet;
        uchar ucNeedPin=1;
        uchar ucPass=gl_SysInfo.ucByPass;

	dbg("gl_ucNFCPinFlag:%d\n",gl_ucNFCPinFlag);	
	dbg("gl_TransRec.uiTransType:%04x\n",gl_TransRec.uiTransType);	
	dbg("gl_TransRec.uiEntryMode:%d\n",gl_TransRec.uiEntryMode);	
	dbg("gl_SysInfo.ulNoPinLimit:%ld\n",gl_SysInfo.ulNoPinLimit);	
	dbg("gl_SysInfo.ucNoPinFlag:%d\n",gl_SysInfo.ucNoPinFlag);	
	dbg("gl_SysInfo.ucCDCVMFlag:%d\n",gl_SysInfo.ucCDCVMFlag);
	dbgHex("gl_uc9f6c",gl_uc9f6c,2);
		
        if(gl_ucNFCPinFlag && (gl_TransRec.uiTransType==TRANS_TYPE_SALE || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH)) 
        {
            ucNeedPin=1;
            ucPass=0;
        }else{   
         //是否小额免密(非接+消费/预授权+支持免密+金额小于指定值)

		 
          if(gl_TransRec.uiEntryMode/10==7 && (gl_TransRec.uiTransType==TRANS_TYPE_DAIRY_SALE||
			gl_TransRec.uiTransType==TRANS_TYPE_SALE || 
			gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH))					
 	{
 	       if(gl_SysInfo.ucCDCVMFlag)  
           	{
	              if (!(gl_uc9f6c[0] & 0x80)  && gl_uc9f6c[1] & 0X80)
	     	      {
	     			ucNeedPin=0;
				gl_ucNFCPinFlag=0;	
	     	       }			
		      else if(gl_TransRec.ulAmount<=gl_SysInfo.ulNoPinLimit && gl_SysInfo.ucNoPinFlag)
		      	{
		      		dbg("NOPIN1:gl_SysInfo.ucNoPinFlag=%d, gl_SysInfo.ulNoPinLimit=%lu\n", gl_SysInfo.ucNoPinFlag, gl_SysInfo.ulNoPinLimit);
	                    	dbg("szPAN1:%s\n",szPAN);
				if(gl_ucOutsideCardFlag == TRUE)	
				{
				        iRet = iGetEnvIcCardClass();
					if(iRet == 1 || iRet ==2)
					{
						ucNeedPin=0;
						gl_ucNFCPinFlag=0;
					}
					else
					{
						ucNeedPin=1;
						gl_ucNFCPinFlag=1;
					}	
				}
				if(Check_CardBlackBin(szPAN))
				{
					ucNeedPin = 1;
					gl_ucNFCPinFlag=1;
				}	
	               		 else
	                	{
	                		ucNeedPin=0;
					gl_ucNFCPinFlag=0;
	               		 }			
		      	}
          	}
	        else 
	        {                      
	        	 if(gl_TransRec.ulAmount<=gl_SysInfo.ulNoPinLimit && gl_SysInfo.ucNoPinFlag)
		      	{
		      		dbg("NOPIN1:gl_SysInfo.ucNoPinFlag=%d, gl_SysInfo.ulNoPinLimit=%lu\n", gl_SysInfo.ucNoPinFlag, gl_SysInfo.ulNoPinLimit);
	                    	dbg("szPAN1:%s\n",szPAN);
				if(gl_ucOutsideCardFlag == TRUE)	
				{
				        iRet = iGetEnvIcCardClass();
					if(iRet == 1 || iRet ==2)
					{
						ucNeedPin=0;
						gl_ucNFCPinFlag=0;
					}
					else
					{
						ucNeedPin=1;
						gl_ucNFCPinFlag=1;
					}	
				}			
				if(Check_CardBlackBin(szPAN))
				{
					ucNeedPin = 1;
					gl_ucNFCPinFlag=1;
				}	
	               		 else
	                	{
	                		ucNeedPin=0;
					gl_ucNFCPinFlag=0;		
	               		 }			
		      	}
	        }
		dbg("ucNeedPin1:%d\n",ucNeedPin);
		gl_SysInfo.ucNeedPin = ucNeedPin;
          }  
            else
			{
                //是否撤销类交易且对应参数为不输入密码
                if((gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID && gl_SysInfo.ucSaleVoidPinFlag==0) ||
                    (gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID && gl_SysInfo.ucAuthVoidPinFlag==0) ||
                    (gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID && gl_SysInfo.ucAuthCompVoidPinFlag==0) ||
                    (gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP && gl_SysInfo.ucAuthCompPinFlag==0) ||
                    gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
                {
                    dbg("NOPIN 2.\n");
                    ucNeedPin=0;
                }else
                {
                    dbg("******************NEEDPIN.......................... 2.\n");
                }
            }
        }
	
        szPINBlockOut[0]=0;
        if(ucNeedPin)
        {
            iRet=iInputPinAndEnc(ucPass, gl_TransRec.ulAmount, "请持卡人输入密码:", szPAN, szPINBlockOut);
            if(iRet<0)
            {
                vSetDispRetInfo(0);
                if(iRet==-1)
                    return STATUS_CANCEL;
                else
                    return STATUS_FAIL;
            }
        }
        return STATUS_OK;
    }
#else   
	DPRINTF("Call GetOnlinePINEnterFromUI()");
	ret = internalPinpadOnlinePin(keyIndex, szPAN, szPINBlockOut);
	return ret;
#endif    
	//DisplayProcess("处理中");
	/*
	switch (ret)
	{
		//Enter PIN
		case 0:
			len = strlen(szPINBlockOut);
			if (strlen(szPINBlockOut) != 20 && strlen(szPINBlockOut) != 36)
			{				
				DPRINTF("szPINBlockOut len error", szPINBlockOut);
				return STATUS_FAIL;
			}

			if (szPINBlockOut[3] != '0')
			{
				DPRINTF("szPINBlockOut status error", szPINBlockOut);
				return STATUS_FAIL;
			}

			strcpy(szPINBlockOut, szPINBlockOut + 4);
		
			DPRINTF("szPINBlockOut = %s", szPINBlockOut);
			
			return STATUS_OK;

		//Timeout
		case 1:
			DPRINTF("Timeout");
			
			return STATUS_CANCEL;

			//Cancel
		case 2:
			DPRINTF("Cancel");
			
			return STATUS_CANCEL;

		case 3:
			DPRINTF("Comm error");
			
			return STATUS_FAIL;

		//No Enter PIN just enter OK.
		case 4:
			DPRINTF("Just enter OK");
			szPINBlockOut[0] =  '\0';
			return STATUS_OK;

		default:
			return STATUS_FAIL;
	}
	*/
}


tti_int32 GetPINEnterFromUI(tti_tchar *szDisplayString, tti_tchar *szPINValue)
{
	tti_int32 ret;
#ifndef CY20_EMVL2_KERNEL
	ret = CJAVA_EnterPIN(szDisplayString, szPINValue, IsPinAutoClose(), GetAutoPin());
#else

#ifndef BCTC_EMVL2_TEST
	int iTmpLen;
	ret = GetPIN(szPINValue, &iTmpLen);
#else
	int iTmpLen;
	ret = GetPINViaPrompt(szDisplayString, szPINValue, &iTmpLen);
#endif
/*
	switch (ret)
	{
		//Enter PIN
		case 0:
			DPRINTF("Enter PIN, szPINValue = %s", szPINValue);
			
			return STATUS_OK;

		//Cancel
		case 1:
			DPRINTF("Cancel PIN");
			
			return STATUS_CANCEL;

		//No Enter PIN just enter OK.
		case 2:
			DPRINTF("Just enter OK");
			szPINValue[0] =  '\0';
			return STATUS_OK;

		default:
			return STATUS_FAIL;
	}
*/
	switch (ret)
	{
		//Enter PIN
		case 0:
			DPRINTF("Enter PIN, szPINValue = %s", szPINValue);			
			return STATUS_OK;

		//Cancel
		case -1:
			DPRINTF("Cancel PIN");
			return STATUS_CANCEL;

		//Timeout.
		case -2:
			DPRINTF("Cancel PIN");
			return STATUS_CANCEL;

		default:
			return STATUS_FAIL;
	}	
#endif

}


//------------------------------------------------------------------------------------------
tti_int32 offlineEncipherPin(RunningTimeEnv *runningTimeEnv)
{
	tti_tchar pinNumber[PIN_NUMBER_SIZE+1];
	tti_byte formatedPinNumber[BUFFER_SIZE_512BYTE];
	tti_byte response[BUFFER_SIZE_256BYTE];
	tti_uint16 respLen;
//	tti_byte randNum;
	tti_byte encipherText[BUFFER_SIZE_512BYTE];
	tti_int32 encipherTextLen, status;
	tti_byte *pPinTryCounter;
	tti_byte retryTime;
	tti_tchar retryLineDisplay[20+1];
	tti_int32 iRet;
	tti_tchar displayPrompt[30];

	DPRINTF("offline Encipher Pin\n");

	if (!terminalSupport(TERM_CAP_ENCIPHER_PIN))
	{
		if (!terminalSupport(TERM_CAP_PLAIN_PIN))
		{
			SetTVR(TVR_NEEDED_PINPAD_NOT_WORKING);
		}
		return CVM_RESULT_NOT_SUPPORT;
	}

    ReportLine();

	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_PIN_TRY_COUNTER))
	{
		getSpecialDataFromICC(runningTimeEnv, TAG_PIN_TRY_COUNTER);
	}

    ReportLine();

	pPinTryCounter=GetTagValue(&runningTimeEnv->tagList, TAG_PIN_TRY_COUNTER);
	if (pPinTryCounter!=NULL)
	{
		if ((*pPinTryCounter)==0)
		{
			SetTVR(TVR_PIN_TRY_LIMIT_EXCEEDDED);
			return CVM_RESULT_FAIL;
		}
	}

    ReportLine();

	if (getPinCertificate(runningTimeEnv)!=STATUS_OK)
	{
		if (getIccCertificate(runningTimeEnv)!=STATUS_OK)
		{
            DPRINTF("getIccCertificate error");
			return CVM_RESULT_FAIL;
		}
		else
		{
			memcpy(&runningTimeEnv->pinPublicKey, &runningTimeEnv->iccPublicKey, 
				sizeof(Certificate));
		}
	}

    ReportLine();

	if (runningTimeEnv->commonRunningData.bypassPinEntry)
	{
		return CVM_RESULT_FAIL;
	}

    ReportLine();

	strcpy(displayPrompt, "请输入IC卡脱机密码");
	if (pPinTryCounter!=NULL)
	{
		if ((*pPinTryCounter) == 1)
		{
			strcpy(displayPrompt, "最后一次IC卡脱机密码");
		}
	}

    ReportLine();
	
	while(TRUE)
	{
        ReportLine();
		iRet = GetPINEnterFromUI(displayPrompt, pinNumber);
		if (iRet == STATUS_OK && strlen(pinNumber) == 0)
		{
			//Try to by pass
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				continue;
			}
			SetTVR(TVR_PIN_WAS_NOT_ENTERED);
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
			return CVM_RESULT_FAIL;
		}else if (iRet == STATUS_CANCEL)
		{
			//Cancel key to exit
			//return CVM_RESULT_CANCEL;
			//Changed to bypass
			//Try to by pass
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NO_ALLOW)
			{
				continue;
			}
			SetTVR(TVR_PIN_WAS_NOT_ENTERED);
			if (runningTimeEnv->commonRunningData.pinBypassMode==PIN_BYPASS_NEXT_ALLOW)
			{
				runningTimeEnv->commonRunningData.bypassPinEntry=TRUE;
			}
			return CVM_RESULT_FAIL;
		}
		
	//End
		
		if (SmartCardGetChallenge(response, &respLen)!=STATUS_OK)
		{
			return CVM_RESULT_FAIL;
		}

		if (respLen!=8)
		{
			return CVM_RESULT_FAIL;
		}


		while(1)
		{
			if (GetRandNumber(formatedPinNumber, runningTimeEnv->pinPublicKey.modLen)!=STATUS_OK)
			{
				return CVM_RESULT_FAIL;
			}
			formatedPinNumber[0]='\x7F';
			formatPlainPin(pinNumber, &formatedPinNumber[1]);
			//For test, jason
			memcpy(&formatedPinNumber[9], response, 8);

			encipherTextLen=sizeof(encipherText);
			memset(encipherText, 0, sizeof(encipherText));
			
			//LOGD("RSA:runningTimeEnv->pinPublicKey.modLen = %02x", runningTimeEnv->pinPublicKey.modLen);
			
			if (RSA(&runningTimeEnv->pinPublicKey, formatedPinNumber, runningTimeEnv->pinPublicKey.modLen, 
				encipherText, &encipherTextLen)!=STATUS_OK)
			{
				return CVM_RESULT_FAIL;
			}

			LOGD("RSA:runningTimeEnv->pinPublicKey.modLen = %02x", runningTimeEnv->pinPublicKey.modLen);
			LOGD("RSA : encipherTextLen = %02x", encipherTextLen);	
			//Jason added for pad on 2017.05.26	
			if (encipherTextLen !=  runningTimeEnv->pinPublicKey.modLen)
			{
				LOGE("RSA : encipherTextLen = %02x, not runningTimeEnv->pinPublicKey.modLen, retry GetRandNumber", encipherTextLen);
				//encipherTextLen = runningTimeEnv->pinPublicKey.modLen;
				
			}else
			{
				LOGE("RSA : encipherTextLen OK, no need retry");
				break;
			}
			//Added end
		}
		
		status=SmartCardVerifyPin(encipherText, encipherTextLen, VERIFY_PIN_TYPE_ENCIPHER, &retryTime);
		if (status==STATUS_ERROR)
		{
			return CVM_RESULT_ERROR;
		} 

		if (status==STATUS_OK)
		{
			return CVM_RESULT_SUCCESS;
		}

		if (status==STATUS_PIN_RETRY_LIMIT)
		{
			SetTVR(TVR_PIN_TRY_LIMIT_EXCEEDDED);
			return CVM_RESULT_FAIL;
		}

		ClearScreen();		
		//LCD_DispLine(1, 0, ENCIPHER_PIN_STRING_CHINESE, strlen(ENCIPHER_PIN_STRING_CHINESE), 0);
		DisplayInfoLeft(runningTimeEnv->enterOfflineEncryptPinString, 0);
		if ( retryTime == 1)
		{
			//DisplayInfoLeft(runningTimeEnv->lastPintryString, 2);
			strcpy(displayPrompt, "最后一次输入脱机密文PIN");
		}
		else
		{			
			sprintf(retryLineDisplay, "(%d)", retryTime);
			//DisplayInfoLeft(retryLineDisplay, 1); //Remove PIN retry times 2012.04.24 Jason
		}
	}
}

//------------------------------------------------------------------------------------------
tti_int32 offlineEncipherPinAndSign(RunningTimeEnv *runningTimeEnv)
{
	tti_int32 status;

	if (!terminalSupport(TERM_CAP_ENCIPHER_PIN))
	{
		if (!terminalSupport(TERM_CAP_PLAIN_PIN))
		{
			SetTVR(TVR_NEEDED_PINPAD_NOT_WORKING);
		}
		return CVM_RESULT_NOT_SUPPORT;
	}

	if (!terminalSupport(TERM_CAP_SIGNATURE))
	{
		return CVM_RESULT_NOT_SUPPORT;
	}

	status=offlineEncipherPin(runningTimeEnv);

	if (status==CVM_RESULT_ERROR)
	{
		return CVM_RESULT_ERROR;
	}
	else if(status!=CVM_RESULT_SUCCESS)
	{
		return CVM_RESULT_FAIL;
	}
	else
	{
		return sign(runningTimeEnv);
	}
}


//------------------------------------------------------------------------------------------
tti_int32 credentialCheck(RunningTimeEnv *runningTimeEnv)
{
	tti_byte tempCreType[10] = {0};
	tti_byte tempCreNumber[100] = {0};

	DPRINTF("credentialCheck\n");

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_CREDENTIAL_TYPE))
	{
		translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_CREDENTIAL_TYPE),
			GetTagValueSize(&runningTimeEnv->tagList, TAG_CREDENTIAL_TYPE), (tti_tchar *)tempCreType);
	}else
	{
		return CVM_RESULT_ERROR;
	}
	
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_CREDENTIAL_NUMBER))
	{
		strncpy((char *)tempCreNumber, (char *)(GetTagValue(&runningTimeEnv->tagList, TAG_CREDENTIAL_NUMBER)),
			GetTagValueSize(&runningTimeEnv->tagList, TAG_CREDENTIAL_NUMBER));
	}else
	{
		return CVM_RESULT_ERROR;
	}

	if (IsCredentialMatch(tempCreType, tempCreNumber) == TRUE)
	{
		return CVM_RESULT_SUCCESS;
	}else
	{
		return CVM_RESULT_FAIL;
	}	
}

//------------------------------------------------------------------------------------------
tti_int32 performCVM(RunningTimeEnv *runningTimeEnv, tti_byte perform)
{
	DPRINTF("holder:handle\n");
	
	switch (perform&0x3F)
	{
		case CVM_OFFLINE_PLAIN_PIN:
			return offlinePlainPin(runningTimeEnv);
			
		case CVM_ONLINE_ENCIPHER_PIN:
			return onlineEncipherPin(runningTimeEnv);
			
		case CVM_OFFLINE_PLAIN_PIN_AND_SIGN:
			return offlinePlainPinAndSign(runningTimeEnv);
			
		case CVM_OFFLINE_ENCIPHER_PIN:
			return offlineEncipherPin(runningTimeEnv);
			
		case CVM_OFFLINE_ENCIPHER_PIN_AND_SIGN:
			return offlineEncipherPinAndSign(runningTimeEnv);
			
		case CVM_SIGN:
			return sign(runningTimeEnv);
			
		case CVM_CREDENTIAL:
			return credentialCheck(runningTimeEnv);
			
		case CVM_NO_REQUIRE:
			if (terminalSupport(TERM_CAP_NO_CVM_REQUIRED))
			{
				return CVM_RESULT_SUCCESS;
			}
			else
			{
				return CVM_RESULT_NOT_SUPPORT;
			}
			
		case CVM_NO_USED:
		default:
			break;
	}
	
	return CVM_RESULT_UNRECOGNISED;
}

//------------------------------------------------------------------------------------------
tti_int32 CardHolderVerifyMethod(RunningTimeEnv *runningTimeEnv)
{
	tti_int16 cvmLen;
	tti_byte *cvmList;
	tti_int32 index;
	tti_int32 cvmResult;

	DPRINTF("holder verify\n");

	setCVMResult(0x3F, 0x00, CVM_RESULT_UNKNOWN);
	if (!needToDo(AIP_CARDHOLDER_VERIFY))
	{
		setCVMResult(0x3F, 0x00, CVM_RESULT_UNKNOWN);
		return STATUS_OK;
	}
	/*
	if (!terminalSupport(TERM_CAP_NO_CVM_REQUIRED))
	{
		setCVMResult(0x3F, 0x00, CVM_RESULT_UNKNOWN);
		return STATUS_OK;
	}
	*/
	
	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_CVM))
	{
		SetTVR(TVR_ICC_DATA_MISSING);
		return STATUS_OK;
	}
	
	cvmList=GetTagValue(&runningTimeEnv->tagList, TAG_CVM);
	cvmLen=GetTagValueSize(&runningTimeEnv->tagList, TAG_CVM);
	cvmList=GetTagValue(&runningTimeEnv->tagList, TAG_CVM);

	cvmList+=CVM_AMOUNT_X_SIZE+CVM_AMOUNT_Y_SIZE;
	cvmLen=cvmLen-(CVM_AMOUNT_X_SIZE+CVM_AMOUNT_Y_SIZE);

	if (cvmLen==0)
	{
		SetTVR(TVR_ICC_DATA_MISSING);
		return STATUS_OK;
	}
	
	setCVMResult(0x3F, 0x00, CVM_RESULT_FAIL);
	SetTSI(TSI_CARDHOLDER_VERFIFY_PERFORMED);
	DPRINTF("CVM List\n");
	printByteArray(cvmList, cvmLen);
	for (index=0; index<cvmLen; index+=2)
	{
		DPRINTF("Action:%02X Condition:%02X\n", cvmList[index], cvmList[index + 1]);
		if (needPerformCVM(runningTimeEnv, cvmList[index], cvmList[index+1]))
		{
			if ((cvmList[index]&0x3F)==CVM_FAIL_PROCESS)
			{
				SetTVR(TVR_CARDHOLDER_VERIFY_NOT_SUCCESS);
				setCVMResult(cvmList[index], cvmList[index+1], CVM_RESULT_FAIL);
				DPRINTF("FAIL CVM\n");
				return STATUS_OK;
			}
			cvmResult=performCVM(runningTimeEnv, cvmList[index]);
			DPRINTF("performCVM() return %d  [%04X]\n", cvmResult, cvmResult);
			if (cvmResult==CVM_RESULT_ERROR)
			{
				return STATUS_ERROR;
			}
			//Jason added on 2013.05.22, start
			else if(cvmResult == CVM_RESULT_CANCEL)
			{
				return STATUS_CANCEL;
			}
			//end
			else if ((cvmResult==CVM_RESULT_SUCCESS)||(cvmResult==CVM_RESULT_UNKNOWN))
			{
				setCVMResult(cvmList[index], cvmList[index+1], cvmResult);
				return STATUS_OK;
			}
			else if (cvmResult==CVM_RESULT_NOT_SUPPORT)
			{
//				continue;
				
				if ((cvmList[index]&0x40)==0)
				{
					break;
				}
				else
				{
					continue;
				}

			}
			else
			{
				if (cvmResult==CVM_RESULT_UNRECOGNISED)
				{
					SetTVR(TVR_UNRECOGNISED_CVM);
				}
				else
				{
					setCVMResult(cvmList[index], cvmList[index+1], CVM_RESULT_FAIL);
				}
				if ((cvmList[index]&0x40)==0)
				{
					break;
				}
			}
		}
		else
		{
			DPRINTF("No need perform\n");
		}
	}

	SetTVR(TVR_CARDHOLDER_VERIFY_NOT_SUCCESS);

	return STATUS_OK;
}
