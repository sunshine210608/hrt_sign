#ifndef _DATA_AUTH_H_
#define _DATA_AUTH_H_



#include "OsTypes.h"
#include "defines.h"



tti_int32 RSA(Certificate *certificate, tti_byte *src, tti_int32 srcLen,

		 tti_byte *result, int *resultLen);



tti_int32 checkRawIssuerCertificate(tti_byte *checkedRawCertificate, tti_int32 len, TagList *tagList);



tti_int32 getIssuerCertificate(RunningTimeEnv *runningTimeEnv);



tti_int32 checkRawSDACertificate(tti_byte *checkedRawSDACertificate, tti_int32 len, 

	RunningTimeEnv *runningTimeEnv);



tti_int32 performSDA(RunningTimeEnv *runningTimeEnv);



tti_int32 checkRawIccCertificate(tti_byte *checkedRawIccCertificate, tti_int32 len, 

	RunningTimeEnv *runningTimeEnv);



tti_int32 checkRawDDACertificate(tti_byte *checkedRawDDACertificate, tti_int32 len, 

	RunningTimeEnv *runningTimeEnv);



tti_int32 performDDA(RunningTimeEnv *runningTimeEnv);



tti_int32 getPinCertificate(RunningTimeEnv *runningTimeEnv);


tti_int32 getIccCertificate(RunningTimeEnv *runningTimeEnv);


tti_int32 performCDA(RunningTimeEnv *runningTimeEnv, tti_bool isFirstGAC);


tti_int32 recoveryKeyCDA(RunningTimeEnv *runningTimeEnv);

tti_int32 checkQpbocRawDDACertificate(tti_byte *rawDDACertificate, tti_int32 len, 
	RunningTimeEnv *runningTimeEnv);



#endif

