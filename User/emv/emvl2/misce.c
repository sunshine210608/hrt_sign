#include <errno.h>
#include <time.h>
//#include <unistd.h>
#include <string.h>
//#include <sys/time.h>
//#include <sys/select.h>
#include <stdio.h>
#include <signal.h>
#include "misce.h"
#include "emvDebug.h"
#include "define.h"

void debug_buf(char *bufname, char *buf)
{
#ifndef RELEASE
	int len = strlen(buf);
	int cnt = 0;

	DPRINTF("Buffer \"%s(%d)\":\n", bufname, len);
	while(cnt < len)
	{
		DPRINTF(" %02x", buf[cnt++]);
		if(cnt % 8 == 0)
			DPRINTF("  ");
		if(cnt % 16 == 0)
			DPRINTF("\n");
		DPRINTF("\n");
	}
#endif
}


int AscByteToHex(unsigned char *Dest, char Src)
{
	if((Dest == NULL) || (Src < '0'))
		return -1;

    //if(Src>='a' && Src<='f')      //支持小写字母
    //    Src-=('a'-'A');
    
	*Dest = Src - '0';
	if((*Dest > 9) && (*Dest < 17))
		return -1;

	if(*Dest >= 17)
	{
		*Dest -= 7;
		if(*Dest > 0x0f)
			return -1;
	}

	return 0;
}

// "FFFF" -> "\xff\xff"
int AtoHex(unsigned char *Dest, char *Src, int SrcLen)
{
	int i;
	unsigned char Tmp1, Tmp2;

	if((Dest == NULL) || (Src == NULL))
		return -1;

	for(i=0; i<SrcLen; i+=2)
	{
		if(AscByteToHex(&Tmp1, Src[i]) != 0)
			return -1;

		if(AscByteToHex(&Tmp2, Src[i+1]) != 0)
			return -1;

		Dest[i/2] = (Tmp1 << 4) | Tmp2;
	}

	return 0;
}

// "\xff\xff" -> "FFFF" 
int HextoA(char *Dest, const unsigned char *Src, int SrcLen)
{
	int i;
	char Tmp;

	if((Dest == NULL) || (Src == NULL))
		return -1;

	for(i=0; i<SrcLen; i++)
	{
		Tmp = (Src[i] >> 4) & 0x0f;
		Dest[i * 2] = Tmp + '0';
		if(Tmp > 9)
			Dest[i * 2] += 7;

		Tmp = Src[i] & 0x0f;
		Dest[i * 2 + 1] = Tmp + '0';
		if (Tmp > 9)
			Dest[i * 2 + 1] += 7;
	}

	return 0;
}

/*
char* Xor(unsigned char *Dest, const unsigned char *Src)
{
	int i;
	
	if((Dest == NULL) || (Src == NULL))
		return NULL;
	
	for(i=0; i<8; i++)
		Dest[i] ^= Src[i];

	return Dest;
}
*/

int memcmp_ex(const void *buffer1, const void *buffer2, unsigned int count)
{
	int iLoop;
	int iFlag = 0;
	int iTmpFlag = 0;
	
	for (iLoop = 0; iLoop < count; iLoop ++)
	{
		//if (iFlag == 0)
		//{
			iTmpFlag = *((unsigned char *)buffer1) - *((unsigned char *)buffer2);
			if (iTmpFlag > 0 &&iFlag == 0)
			{
				iFlag = 1;
			}
			if (iTmpFlag < 0 && iFlag == 0)
			{
				iFlag = -1;
			}else
			{

			}
			
		buffer1 = (unsigned char *)buffer1 + 1;
    buffer2 = (unsigned char *)buffer2 + 1;
	}
	
	return iFlag;
}

// 0x12345678 ->"\x78\x56\x34\x12"
int Comm_IntToHex_LittleEndian(int iSrc, byte *byDest)
{
    byte *pScr;

    pScr = (byte *)(&iSrc);

    *byDest = *pScr;
    *(byDest + 1) = *(pScr + 1);
    *(byDest + 2) = *(pScr + 2);
    *(byDest + 3) = *(pScr + 3);

    return 0;
}

//0x12345678 ->"\x12\x34\x56\x78"
int Comm_IntToHex_BigEndian(int iSrc, byte *byDest)
{
    byte *pScr;

    pScr = (byte*)(&iSrc);

    *byDest = *(pScr + 3);
    *(byDest + 1) = *(pScr + 2);
    *(byDest + 2) = *(pScr + 1);
    *(byDest + 3) = *(pScr + 0);

    return 0;
}

//"\x78\x56\x34\x12"->0x12345678 
int Comm_HexToInt_LittleEndian(byte *pbySrc, int *piDest)
{
    *piDest = *((int *)pbySrc);
    
    return 0;
}

///"\x12\x34\x56\x78"->0x12345678 
int Comm_HexToInt_BigEndian(byte *pbySrc, int *piDest)
{
    byte byScr[4];

    *byScr = *(pbySrc + 3);
    *(byScr + 1) = *(pbySrc + 2);
    *(byScr + 2) = *(pbySrc + 1);
    *(byScr + 3) = *(pbySrc + 0);

    *piDest = *((int*)byScr);

    return 0;
}

// 0x1234 ->"\x34\x12"
int Comm_ShortToHex_LittleEndian(short Src, byte *byDest)
{
    byte *pScr;

    pScr = (byte*)(&Src);

    *byDest = *pScr;
    *(byDest + 1) = *(pScr + 1);

    return 0;
}

//0x1234 ->"\x12\x34"
int Comm_ShortToHex_BigEndian(short Src, byte *byDest)
{
    byte *pScr;

    pScr = (byte *)&Src;

    *byDest = *(pScr + 1);
    *(byDest + 1) = *pScr ;

    return 0;
}

///"\x12\x34"->0x1234
int Comm_HexToShor_BigEndian(byte *pbySrc, short *pDest)
{
    byte byScr[2];

    *byScr = *(pbySrc + 1);
    *(byScr + 1) = *pbySrc;
    
    *pDest = *((short*)byScr);

    return 0;
}

///"\x12\x34"->0x3412
int Comm_HexToShort_LittleEndian(byte *pbySrc, short *pDest)
{
    *pDest = *((short *)pbySrc);

    return 0;
}


//"\xff\x00\x00\x00" ->  "256"
int Comm_HexToStr_LittleEndian(byte *pbySrc, char *szDest)
{
    int iDest;

    Comm_HexToInt_LittleEndian(pbySrc, &iDest);
    //sprintf(szDest, "%d", iDest);
    //itoa(iDest, szDest, 10);

    return 0;
}

//""256"->  "\xff\x00\x00\x00"
int Comm_StrToHex_LittleEndian(char *szSrc, byte *pbyDest)
{
    int iStr;

    iStr = atoi(szSrc);
    Comm_IntToHex_LittleEndian(iStr, pbyDest);
    return 0;
}



/**
 * @fn CONV_Asc2Hex
 * @li 2个ASC转换成一个HEX，带磁道=号分隔符处理
 * @param[out] pucDest	：目标数据指针
 * @param[in]  uiDestLen：目标数据长度
 * @param[in]  pucSrc	：源数据指针
 * @param[in]  uiSrcLen	：源数据长度
 * @retval
*/
void CONV_Asc2Hex(unsigned char * pucDest, unsigned int uiDestLen,
				unsigned char * pucSrc, unsigned int uiSrcLen)
{
	int i;
	unsigned char temp;
	unsigned char L;
	unsigned char H;

	for (i=0; i<uiSrcLen; i+=2)
	{
		if (pucSrc[i] <= '9' || pucSrc[i] == '=')
		{
			H = pucSrc[i]-'0';
		}
		else if (pucSrc[i] >= 'a' && pucSrc[i] <= 'f')
		{
			H = pucSrc[i]-'a'+10;
		}
		else
		{
			H = pucSrc[i]-'A'+10;
		}

		if (pucSrc[i+1] <= '9' || pucSrc[i+1] == '=')
		{
			L = pucSrc[i+1]-'0';
		}
		else if (pucSrc[i+1] >= 'a' && pucSrc[i+1] <= 'f')
		{
			L = pucSrc[i+1]-'a'+10;
		}
		else
		{
			L = pucSrc[i+1]-'A'+10;
		}

		temp = H;
		temp <<= 4;
		temp |= L;
		pucDest[i/2] = temp;
	}
}



/**
* @fn void CONV_Hex2Asc(unsigned char * pucDest, unsigned int uiDestLen,
*				unsigned char * pucSrc, unsigned int uiSrcLen)
* @li HEX转ASCII,比如一个HEX 0x13 转成2个 ASCII 0x31 0x33,
* @param[out] pucDest	：目标数据指针
* @param[in]  uiDestLen	：目标数据长度
* @param[in]  pucSrc	：源数据指针
* @param[in]  uiSrcLen	：源数据长度
* @retval
*/
void CONV_Hex2Asc(unsigned char * pucDest, unsigned int uiDestLen,
				unsigned char * pucSrc, unsigned int uiSrcLen)
{
	int i;
	unsigned char H = 0;
	unsigned char L = 0;
	unsigned char temp = 0;

	for (i=0; i<uiSrcLen; i++)
	{
		temp = 0;
		temp = (pucSrc[i]>>4 & 0x0f);
		if (temp > 9)
		{
			H = temp-10+'A';
		}
		else
		{
			H = temp+'0';
		}
		temp = pucSrc[i] & 0x0f;
		if (temp > 9)
		{
			L = temp-10+'A';
		}
		else
		{
			L = temp+'0';
		}
		pucDest[i*2] = H;
		pucDest[i*2+1] = L;
	}
}



