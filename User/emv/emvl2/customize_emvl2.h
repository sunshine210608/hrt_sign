#ifndef __CUSTOMIZE_EMVL2__
#define __CUSTOMIZE_EMVL2__


#include "OsTypes.h"

tti_int32 SCR_Open(const tti_int8 * pszParam);

tti_bool SCR_Close(tti_int32 hSCR);

tti_bool SCR_Detect(tti_int32 hSCR);

tti_bool SCR_PowerOn(tti_int32 hSCR, tti_byte * pATR, tti_int8* piATRLen);

tti_bool SCR_PowerOff(tti_int32 hSCR);

tti_bool SCR_ExchangeApdu(tti_int32 hSCR, const tti_byte * pApduCmd, const tti_int16 iApduCmdLen, tti_byte * pApduResp, tti_int16 * piApduRespLen);

int SCR_Get_Status(int *piStatus);

int Emvl2_InitICCReader(void);

int EMVL2_Test(char *szArrTagList);


#endif

