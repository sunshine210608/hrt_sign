#ifndef _BUILD_RESPONSE_H_
#define _BUILD_RESPONSE_H_

#include "defines.h"

#define	RESP_RESULT_OK				0
#define	RESP_RESULT_FAULTURE			1
#define	RESP_RESULT_FORMAT_ERROR	5
#define	RESP_RESULT_CANCELED			7

void appendFixMessage(tti_tchar *string, tti_byte fillType, tti_byte fillLength, tti_tchar fillChar,
	tti_tchar *message);


void buildInvalidResponse(tti_tchar *command, tti_byte status, tti_tchar *response);


void buildLoadConfigurationResponse(tti_byte status,  char *response);


void buildGetAidListResponse(tti_byte status, AID_LIST *aidList,  char *response);


void buildInitTransactionResponse(tti_byte status, RunningTimeEnv *runningTimeEnv, char *response);


void buildReadMediaResponse(tti_byte status, RunningTimeEnv *runningTimeEnv,  char *response);


void buildTransactionResponse(tti_byte status, RunningTimeEnv *runningTimeEnv,  char *response);


void buildCompletionResponse(tti_byte status, RunningTimeEnv *runningTimeEnv,  char *response);


void buildEndTransactionResponse(tti_byte status,  char *response);


void buildGetCheckSumResponse(tti_byte status,  char *response);

void buildGetTagResponse(tti_byte status, tti_uint16 tag, 
	RunningTimeEnv *runningTimeEnv,  char *response);


void buildGetBalanceEnquiryResponse(tti_byte status, RunningTimeEnv *runningTimeEnv,  char *response);




#endif

