#include <string.h>
#include "ui.h"
#include "defines.h"
#include "misce.h"
#include "emvDebug.h"
#include "emvAppUi.h"
#include "util.h"
#include "utils.h"
/*
static tti_tchar pTAlphaKeysTbl[][10] = 
{

	"0- +",  // key_0
	"1QZqz",
	"2ABCabc",
	"3DEFdef",
	"4GHIghi",
	"5JKLjkl",
	"6MNOmno",
	"7PRSprs",
	"8TUVtuv",
	"9WXYwxy",
	",*\'\"\\%", // key_*
	"#.!:@=&" // Key_#
};
*/

/*
typedef struct _alpha_state_evnt_table

{

	TEvntMask tValidKeyEvnt;

	tti_tchar *ptchAlpha;

} T_AlphaStateEvntTable;
*/


/*
static T_AlphaStateEvntTable tAlphaStateEvntTable[] =

{

	{KEYBORADEVM_MASK_KEY_0, &pTAlphaKeysTbl[0][0]},

	{KEYBORADEVM_MASK_KEY_1, &pTAlphaKeysTbl[1][0]},

	{KEYBORADEVM_MASK_KEY_2, &pTAlphaKeysTbl[2][0]},

	{KEYBORADEVM_MASK_KEY_3, &pTAlphaKeysTbl[3][0]},

	{KEYBORADEVM_MASK_KEY_4, &pTAlphaKeysTbl[4][0]},

	{KEYBORADEVM_MASK_KEY_5, &pTAlphaKeysTbl[5][0]},

	{KEYBORADEVM_MASK_KEY_6, &pTAlphaKeysTbl[6][0]},

	{KEYBORADEVM_MASK_KEY_7, &pTAlphaKeysTbl[7][0]},

	{KEYBORADEVM_MASK_KEY_8, &pTAlphaKeysTbl[8][0]},

	{KEYBORADEVM_MASK_KEY_9, &pTAlphaKeysTbl[9][0]},

	{KEYBORADEVM_MASK_KEY_ASTERISK, &pTAlphaKeysTbl[10][0]},

	{KEYBORADEVM_MASK_KEY_SP, &pTAlphaKeysTbl[11][0]}

};
*/


tti_tchar GetOneChar(TEvntMask tCurKeyEvnt, tti_int32 index)

{
/*
	tti_uint8 i, j;



	j = sizeof(tAlphaStateEvntTable) / sizeof(tAlphaStateEvntTable[0]);



	for(i = 0; i < j; i++)

	{

		if(tCurKeyEvnt == tAlphaStateEvntTable[i].tValidKeyEvnt)

		{

			index %= strlen(&pTAlphaKeysTbl[i][0]);

			return pTAlphaKeysTbl[i][index];

		}

	}

	

	return (char)0;
*/

	return 0;
}




//------------------------------------------------------------------------------------------

TEvntMask WaitAKeyDuringTimeMS(TEvntMask WaitEvnt, tti_uint16 uiTimeOut)

{
/*
	TEvntMask tEvnt, KeyEvnt;



	tEvnt = TTIOSEVM_MASK_KEYBORAD; // register all key

	KeyEvnt=WaitEvnt;



	EvntMgrDeregLevel1EventInterest(0xffffffff);

	EvntMgrDeregKeyboardInterest(0xffffffff);

	EvntMgrKillTimer (0); 



	// flush mail box

	TtiOSFlushMailbox();

	if(uiTimeOut > 0) // timeout

	{

		tEvnt |= TTIOSEVM_MASK_TIMER;

		EvntMgrSetTimer (0,  uiTimeOut); 

	}

	EvntMgrRegLevel1EventInterest(tEvnt); 

	EvntMgrRegKeyEventInterest(KeyEvnt); 



	EvntMgrWaitForEvent(&tEvnt, &KeyEvnt); 

	// flush mail box

	TtiOSFlushMailbox();

	EvntMgrKillTimer (0); 

	EvntMgrDeregLevel1EventInterest(0xffffffff);

	EvntMgrDeregKeyboardInterest(0xffffffff);



	if(tEvnt & TTIOSEVM_MASK_KEYBORAD)

	{

		return KeyEvnt;

	}

	else 

	{

		return (TEvntMask)0;

	}
*/

	return 0;
}




//------------------------------------------------------------------------------------------
#if 0
void DisplayStringInFormat(tti_tchar *pLine, tti_int32 uiDisplayLine, 

	tti_int32 uiDispStartCol, tti_int32 uiDispEndCol, tti_int32 uiFormat, tti_int32 uiDispMode)

{
/*
	tti_int8 iOffset;

	tti_int8 iCopyMsgLength;

	tti_int8 iFieldLength, iMsgLength;



	iFieldLength=uiDispEndCol-uiDispStartCol+1;

	iMsgLength=strlen(pLine);

	iCopyMsgLength=(iMsgLength<iFieldLength)?iMsgLength:iFieldLength;

	if (iCopyMsgLength<0)

	{

		iCopyMsgLength=0;

	}



	switch (uiFormat)

	{

		case DISPLAY_LEFT_ALIGN:

			iOffset=0;

			break;

		case DISPLAY_RIGHT_ALIGN:

			iOffset=iFieldLength-iCopyMsgLength;

			break;

		case DISPLAY_CENTER_ALIGN:

			iOffset=(iFieldLength-iCopyMsgLength)/2;

			break;

	}

	TtiOsDisplayPartialRow (uiDisplayLine,pLine, uiDispMode,(tti_uint8)(uiDispStartCol+iOffset));

	//Jason
	LcdFlush();
	*/
}
#endif


//------------------------------------------------------------------------------------------
#if 0
void TtiOsClearLines(tti_int32 uiStartLine, tti_int32 uiEndLine)

{
/*
	tti_tchar szEmptyLine[]="                     ";

	tti_int32 uiLine;



	if (uiEndLine>8)

	{

		return;

	}

	for (uiLine=uiStartLine; uiLine<uiEndLine; uiLine++)

	{

		TtiOsDisplayRow (uiLine, szEmptyLine, FALSE);

	}
*/
}
#endif


//------------------------------------------------------------------------------------------
#if 0
void DisplayInfoRight( tti_tchar *pInfo, tti_int32 line)
{
/*
	DisplayStringInFormat(pInfo, line, 0, 15, DISPLAY_RIGHT_ALIGN, DISPLAY_IN_NORMAL);
*/
}
#endif


//------------------------------------------------------------------------------------------
void DisplayInfoLeft( tti_tchar *pInfo, tti_int32 line)
{
/*
	DisplayStringInFormat(pInfo, line, 0, 15, DISPLAY_LEFT_ALIGN, DISPLAY_IN_NORMAL);
*/
}



//------------------------------------------------------------------------------------------
#if 0
void ClearScreen()
{
/*
	TtiOsClearLines(0,8);

	//Jason
	LcdFlush();
*/
}
#endif


//------------------------------------------------------------------------------------------
#if 0
void DisplayValidateInfo(char *pInfo)
{
/*
	ClearScreen();

	

	DisplayInfoRight( pInfo, 4);

	WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_OK, 60);

	

	ClearScreen();
*/
}
#endif

//------------------------------------------------------------------------------------------




tti_int32 EditorLineForOnlinePIN(tti_int32 line, tti_int32 colume, tti_tchar *resultBuffer, tti_int32 bufferSize)
{

	return STATUS_OK;
#if 0
	tti_int32 bufferIndex=-1,keyIndex=-1;

	TEvntMask keyEvent, lastKeyEvent;

	tti_tchar spaceBuffer[21];

	tti_tchar encryptString[21];



	resultBuffer[0]='\0';

	memset(spaceBuffer, '_', bufferSize);

	spaceBuffer[bufferSize]='\0';



	while (TRUE)

	{

		DisplayStringInFormat(spaceBuffer, line, colume, 15, DISPLAY_RIGHT_ALIGN, DISPLAY_IN_NORMAL);

		memset(encryptString, '*', strlen(resultBuffer));

		encryptString[strlen(resultBuffer)]='\0';

		

		DisplayStringInFormat(encryptString, line, colume, 15, DISPLAY_RIGHT_ALIGN, DISPLAY_IN_NORMAL);



		keyEvent=WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_SECURE

			|KEYBORADEVM_MASK_KEY_OK

			|KEYBORADEVM_MASK_KEY_CANCEL|KEYBORADEVM_MASK_KEY_CORRECT, 0);

		switch (keyEvent)

		{

			case KEYBORADEVM_MASK_KEY_OK:
				DPRINTF("resultBuffer len = %d\n", strlen(resultBuffer));
				if (strlen(resultBuffer) < 4 && strlen(resultBuffer) != 0)
				{
					DPRINTF("continue entry\n");
					break;
				}

				return STATUS_OK;

				break;

			case KEYBORADEVM_MASK_KEY_CANCEL:

				memset(resultBuffer, 0, bufferSize);

				return STATUS_CANCEL;

			case KEYBORADEVM_MASK_KEY_CORRECT:

				if (bufferIndex>=0)

				{

					resultBuffer[bufferIndex]=0;

					bufferIndex--;

				}

				keyIndex=-1;

				break;
			/*
			case KEYBORADEVM_MASK_KEY_ALPHA:

				if (keyIndex>=0)

				{

					keyIndex++;

					resultBuffer[bufferIndex]=GetOneChar(lastKeyEvent, keyIndex);

				}

				break;
			*/
			default:

				keyIndex=0;

				if (bufferIndex<bufferSize-1)

				{

					bufferIndex++;

					resultBuffer[bufferIndex]=GetOneChar(keyEvent, keyIndex);

					resultBuffer[bufferIndex+1]='\0';

					lastKeyEvent=keyEvent;

				}

				break;

		}

	}
#endif
}


tti_int32 EditorLine(tti_int32 line, tti_int32 colume, tti_tchar *resultBuffer, tti_int32 bufferSize)
{
	return STATUS_OK;

	

#if 0
	tti_int32 bufferIndex=-1,keyIndex=-1;

	TEvntMask keyEvent, lastKeyEvent;

	tti_tchar spaceBuffer[21];

	tti_tchar encryptString[21];



	resultBuffer[0]='\0';

	memset(spaceBuffer, '_', bufferSize);

	spaceBuffer[bufferSize]='\0';



	while (TRUE)

	{

		DisplayStringInFormat(spaceBuffer, line, colume, 15, DISPLAY_RIGHT_ALIGN, DISPLAY_IN_NORMAL);

		memset(encryptString, '*', strlen(resultBuffer));

		encryptString[strlen(resultBuffer)]='\0';

		

		DisplayStringInFormat(encryptString, line, colume, 15, DISPLAY_RIGHT_ALIGN, DISPLAY_IN_NORMAL);



		keyEvent=WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_SECURE

			|KEYBORADEVM_MASK_KEY_OK

			|KEYBORADEVM_MASK_KEY_CANCEL|KEYBORADEVM_MASK_KEY_CORRECT, 0);

		switch (keyEvent)

		{

			case KEYBORADEVM_MASK_KEY_OK:				
				if (strlen(resultBuffer) < 4 && strlen(resultBuffer) != 0)
				{
					DPRINTF("continue entry\n");
					break;
				}
				
				return STATUS_OK;

				break;

			case KEYBORADEVM_MASK_KEY_CANCEL:

				memset(resultBuffer, 0, bufferSize);

				return STATUS_CANCEL;

			case KEYBORADEVM_MASK_KEY_CORRECT:

				if (bufferIndex>=0)

				{

					resultBuffer[bufferIndex]=0;

					bufferIndex--;

				}

				keyIndex=-1;

				break;

			/*
			case KEYBORADEVM_MASK_KEY_ALPHA:

				if (keyIndex>=0)

				{

					keyIndex++;

					resultBuffer[bufferIndex]=GetOneChar(lastKeyEvent, keyIndex);

				}

				break;
			*/

			default:

				keyIndex=0;

				if (bufferIndex<bufferSize-1)

				{

					bufferIndex++;

					resultBuffer[bufferIndex]=GetOneChar(keyEvent, keyIndex);

					resultBuffer[bufferIndex+1]='\0';

					lastKeyEvent=keyEvent;

				}

				break;

		}

	}
#endif
}

typedef struct
{
	tti_byte szTypeNum[3];
	tti_tchar szTypeNameChinese[20];
}T_CredentinalType;

/*
T_CredentinalType gtCredentinalType[] = 
{
	{"00", "\xc9\xed\xb7\xdd\xd6\xa4"},
	{"01", "\xbe\xfc\xb9\xd9\xd6\xa4"},
	{"02", "\xbb\xa4\xd5\xd5"},
	{"03", "\xc8\xeb\xbe\xb3\xd6\xa4"},
	{"04", "\xc1\xd9\xca\xb1\xc9\xed\xb7\xdd\xd6\xa4"},
	{"05", "\xc6\xe4\xcb\xfc"},
};
*/
	T_CredentinalType gtCredentinalType[] = 
	{
		{"00", "身份证"},
		{"01", "军官证"},
		{"02", "护照"},
		{"03", "入境证"},
		{"04", "临时身份证"},
		{"05", "其他"},
	};



tti_int32 ChangeCredentialTypeNumToChnName(tti_byte *credentialTypeNum, tti_tchar *szTypeNameChinese)
{
	tti_int32 iLoop;

	for (iLoop = 0; iLoop < sizeof(gtCredentinalType)/sizeof(gtCredentinalType[0]); iLoop ++)
	{
		if (strcmp((tti_tchar *)credentialTypeNum, (tti_tchar *)(gtCredentinalType[iLoop].szTypeNum)) == 0)
		{
			strcpy(szTypeNameChinese, gtCredentinalType[iLoop].szTypeNameChinese);
			return 0;
		}
	}
	return -1;
}

tti_bool IsCredentialMatch(tti_byte *credentialType, tti_byte *credentialNumber)
{
	tti_tchar tmp[100] = {0};
	tti_tchar szTypeNameChn[30] = {0};
	tti_tchar szType[30];
	tti_tchar szNumber[30];
	int ret;
	
	ChangeCredentialTypeNumToChnName(credentialType, szTypeNameChn);
	sprintf(tmp, "请出示证件\n类型:%s\n号码:%s", szTypeNameChn, credentialNumber);
	ClearScreen();
	_vDisp(3, "请出示证件");
	sprintf(szType, "类型:%s", szTypeNameChn);
	sprintf(szNumber, "号码:%s", credentialNumber);
	_vDisp(4, szType);
	_vDisp(5, szNumber);
	mdelay(3000);
	//DisplayValidateInfo(tmp);
	

	ret = DisplayYesNoChoiceInfo("证件是否正确");
	if (ret == 1)
		return TRUE;
	else
		return FALSE;
}

void FormatLogDate(char *szSrc, char *szDst)
{
	char szYear[10] = {0};
	char szMoth[10] = {0};
	char szDay[10] = {0};

	memcpy(szYear, szSrc, 2);
	
	memcpy(szMoth, szSrc + 2, 2);

	memcpy(szDay, szSrc + 4, 2);

	sprintf(szDst, "20%s-%s-%s", szYear, szMoth, szDay);		
}

void FormatLogTime(char *szSrc, char *szDst)
{
	char szHour[10] = {0};
	char szMin[10] = {0};
	char szSecond[10] = {0};

	memcpy(szHour, szSrc, 2);
	
	memcpy(szMin, szSrc + 2, 2);

	memcpy(szSecond, szSrc + 4, 2);

	sprintf(szDst, "%s-%s-%s", szHour, szMin, szSecond);		
}


#if 0
//Jason added on 2012.04.17, start
tti_int32 DisplayLog(tti_int32 logIndex, TagList *tagList, tti_bool bIsLast)
{
	tti_byte dispLine[200] = {0};
	tti_byte tmp1[200] = {0};
	tti_byte tmp2[200] = {0};
	tti_byte tmp3[200] = {0};
	

	sprintf(dispLine, "日志 %d", logIndex);
	//DisplayInfoLeft(dispLine, 0);	
	DisplayValidateInfo(dispLine);

	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_TRANSACTION_DATE) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TRANSACTION_DATE), 
			GetTagValueSize(tagList, TAG_TRANSACTION_DATE), tmp1);
		sprintf(dispLine, " 交易日期 :  %s", tmp1);
		//DisplayInfoLeft(dispLine, 1);
		DisplayValidateInfo(dispLine);
	}

	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_TRANSACTION_TIME) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TRANSACTION_TIME), 
			GetTagValueSize(tagList, TAG_TRANSACTION_TIME), tmp1);
		sprintf(dispLine, " 交易时间 :  %s", tmp1);
		//DisplayInfoLeft(dispLine, 2);
		DisplayValidateInfo(dispLine);
	}
	
	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_AMOUNT) == TRUE)
	{
		//castAmountToString(getAmountByTag(tagList, TAG_AMOUNT), tmp1);
		formatAmountByTag(tagList, TAG_AMOUNT, tmp1);
		sprintf(dispLine, "交易金额:%s", tmp1);
		//DisplayInfoLeft(dispLine, 3);	
		DisplayValidateInfo(dispLine);
	}	

	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_AMOUNT_OTHER) == TRUE)
	{
		//castAmountToString(getAmountByTag(tagList, TAG_AMOUNT_OTHER), tmp1);
		formatAmountByTag(tagList, TAG_AMOUNT_OTHER, tmp1);
		sprintf(dispLine, "其他交易金额:%s", tmp1);
		//DisplayInfoLeft(dispLine, 5);
		DisplayValidateInfo(dispLine);
	}	

	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_TERM_COUNTRY_CODE) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TERM_COUNTRY_CODE), 
			GetTagValueSize(tagList, TAG_TERM_COUNTRY_CODE), tmp1);		
		sprintf(dispLine, "国家代码:%s", tmp1);
		//DisplayInfoLeft(dispLine, 7);
		DisplayValidateInfo(dispLine);
	}	

	//keyEvent=WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_OK|KEYBORADEVM_MASK_KEY_CANCEL, 0);	
	//ClearScreen();
	
	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_TRANS_CURRENCY_CODE) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TRANS_CURRENCY_CODE), 
			GetTagValueSize(tagList, TAG_TRANS_CURRENCY_CODE), tmp1);		
		sprintf(dispLine, "交易货币代码:%s", tmp1);
		//DisplayInfoLeft(dispLine, 1);
		DisplayValidateInfo(dispLine);
	}	

	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_TRANSACTION_TYPE) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TRANSACTION_TYPE), 
			GetTagValueSize(tagList, TAG_TRANSACTION_TYPE), tmp1);
		TranslateTransTypeToString(tmp1, tmp2);
		sprintf(dispLine, "交易类型:%s", tmp2);
		DisplayValidateInfo(dispLine);
	}
	memset(tmp2, 0, sizeof(tmp2));
	if (TagIsExisted(tagList, TAG_ATC) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_ATC), 
			GetTagValueSize(tagList, TAG_ATC), tmp2);		
		sprintf(dispLine, "交易计数器:%s", tmp2);
		DisplayValidateInfo(dispLine);
	}
	//sprintf(dispLine, "Type:%s    ATC:%s", tmp1, tmp2);
	//DisplayInfoLeft(dispLine, 2);
	//DisplayValidateInfo(dispLine);
	
	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_MERCHANT_LOCATION) == TRUE)
	{
		memcpy(tmp1, GetTagValue(tagList, TAG_MERCHANT_LOCATION), 
			GetTagValueSize(tagList, TAG_MERCHANT_LOCATION));		
		sprintf(dispLine, "商铺名称:%s", tmp1);
		//DisplayInfoLeft(dispLine, 3);
		DisplayValidateInfo(dispLine);
	}	

	/*
	keyEvent=WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_OK|KEYBORADEVM_MASK_KEY_CANCEL, 0);
	switch (keyEvent)
	{
		case KEYBORADEVM_MASK_KEY_OK:
			return TRUE;

		case KEYBORADEVM_MASK_KEY_CANCEL:
			return FALSE;

		default:
			return FALSE;
	}

	return FALSE;
	*/

	return TRUE;
}
//End
#else
//Jason added on 2012.04.17, start
tti_int32 DisplayLog(tti_int32 logIndex, TagList *tagList, tti_bool bIsLast)
{
	//tti_byte dispLine[200] = {0};
	tti_tchar dispLine[200] = {0};
	char displayText[1024] = {0};
	tti_tchar tmp1[200] = {0};
	tti_tchar tmp2[200] = {0};
	tti_tchar tmp3[200] = {0};
	

	sprintf(dispLine, "日志 %d\n", (int)logIndex);
	strcpy(displayText, dispLine);
	DisplayValidateInfo(dispLine);

	memset(tmp1, 0, sizeof(tmp1));
	memset(tmp2, 0, sizeof(tmp2));
	if (TagIsExisted(tagList, TAG_TRANSACTION_DATE) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TRANSACTION_DATE), 
			GetTagValueSize(tagList, TAG_TRANSACTION_DATE), tmp1);
		FormatLogDate(tmp1, tmp2);
		sprintf(dispLine, "交易日期 : %s\n", tmp2);
		strcat(displayText, dispLine);
		//DisplayInfoLeft(dispLine, 1);
		DisplayValidateInfo(dispLine);
	}

	memset(tmp1, 0, sizeof(tmp1));
	memset(tmp2, 0, sizeof(tmp2));
	if (TagIsExisted(tagList, TAG_TRANSACTION_TIME) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TRANSACTION_TIME), 
			GetTagValueSize(tagList, TAG_TRANSACTION_TIME), tmp1);
		FormatLogTime(tmp1, tmp2);
		sprintf(dispLine, "交易时间:%s\n", tmp2);
		strcat(displayText, dispLine);
		//DisplayInfoLeft(dispLine, 2);
		DisplayValidateInfo(dispLine);
	}
	
	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_AMOUNT) == TRUE)
	{
		//castAmountToString(getAmountByTag(tagList, TAG_AMOUNT), tmp1);
		formatAmountByTag(tagList, TAG_AMOUNT, tmp1);
		sprintf(dispLine, "交易金额:%s\n", tmp1);
		//DisplayInfoLeft(dispLine, 3);
		strcat(displayText, dispLine);
		//DisplayValidateInfo(dispLine);
        DisplayProcess(dispLine);
	}	

	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_AMOUNT_OTHER) == TRUE)
	{
		//castAmountToString(getAmountByTag(tagList, TAG_AMOUNT_OTHER), tmp1);
		formatAmountByTag(tagList, TAG_AMOUNT_OTHER, tmp1);
		sprintf(dispLine, "其他交易金额 :%s\n", tmp1);
		strcat(displayText, dispLine);
		//DisplayInfoLeft(dispLine, 5);
		//DisplayValidateInfo(dispLine);
        DisplayProcess(dispLine);
	}	

	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_TERM_COUNTRY_CODE) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TERM_COUNTRY_CODE), 
			GetTagValueSize(tagList, TAG_TERM_COUNTRY_CODE), tmp1);		
		sprintf(dispLine, "国家代码 :%s\n", tmp1);
		strcat(displayText, dispLine);		
		//DisplayInfoLeft(dispLine, 7);
		DisplayValidateInfo(dispLine);
	}	

	//keyEvent=WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_OK|KEYBORADEVM_MASK_KEY_CANCEL, 0);	
	//ClearScreen();
	
	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_TRANS_CURRENCY_CODE) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TRANS_CURRENCY_CODE), 
			GetTagValueSize(tagList, TAG_TRANS_CURRENCY_CODE), tmp1);		
		sprintf(dispLine, "交易货币代码:%s\n", tmp1);
		strcat(displayText, dispLine);
		//DisplayInfoLeft(dispLine, 1);
		DisplayValidateInfo(dispLine);
	}	

	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_TRANSACTION_TYPE) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_TRANSACTION_TYPE), 
			GetTagValueSize(tagList, TAG_TRANSACTION_TYPE), tmp1);
		TranslateTransTypeToString(tmp1, tmp2);
		sprintf(dispLine, "交易类型:%s\n", tmp2);
		strcat(displayText, dispLine);	
		DisplayValidateInfo(dispLine);
	}
	memset(tmp2, 0, sizeof(tmp2));
	if (TagIsExisted(tagList, TAG_ATC) == TRUE)
	{
		TranslateHexToChars(GetTagValue(tagList, TAG_ATC), 
			GetTagValueSize(tagList, TAG_ATC), tmp2);		
		sprintf(dispLine, "交易计数器:%s\n", tmp2);
		strcat(displayText, dispLine);
		DisplayValidateInfo(dispLine);
	}
	//sprintf(dispLine, "Type:%s    ATC:%s", tmp1, tmp2);
	//DisplayInfoLeft(dispLine, 2);
	//DisplayValidateInfo(dispLine);
	
	memset(tmp1, 0, sizeof(tmp1));
	if (TagIsExisted(tagList, TAG_MERCHANT_LOCATION) == TRUE)
	{
		memcpy(tmp1, GetTagValue(tagList, TAG_MERCHANT_LOCATION), 
			GetTagValueSize(tagList, TAG_MERCHANT_LOCATION));		
		//sprintf(dispLine, "商铺名称:%s\n", tmp1);
		//DisplayInfoLeft(dispLine, 3);
		strcat(displayText, dispLine);
		//DisplayValidateInfo(dispLine);
		sprintf(dispLine, "%s", tmp1);
		ClearScreen();
		_vDisp(4, "商铺名称");
		_vDisp(5, dispLine);
		mdelay(3000);
		ClearScreen();
	}	

	//DisplayValidateInfo(displayText);

	return TRUE;
}
#endif




tti_int32 DisplayCorfim(tti_tchar *szDisplay, int iLine)
{
/*	
	TEvntMask keyEvent, lastKeyEvent;
	
	ClearScreen();

	DisplayInfoLeft(szDisplay, iLine);


	sleep(2);
	keyEvent = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_OK|KEYBORADEVM_MASK_KEY_CANCEL, 0);
	switch (keyEvent)
	{
		case KEYBORADEVM_MASK_KEY_OK:
			return TRUE;

		case KEYBORADEVM_MASK_KEY_CANCEL:
			return FALSE;

		default:
			return FALSE;
	}
	*/
	
		return FALSE;
}

//#include "../../sdk/Include/misce.h"



#define KEY_0		'0'

#define KEY_1		'1'

#define KEY_2		'2'

#define KEY_3		'3'

#define KEY_4		'4'

#define KEY_5		'5'

#define KEY_6		'6'

#define KEY_7		'7'

#define KEY_8		'8'

#define KEY_9		'9'

#define KEY_ALPHA	'A'

#define KEY_CANCEL	'C'

#define KEY_CORR	'R'

#define KEY_OK		'E'

#define KEY_UP		'K'

#define KEY_DOWN	'L'

#define KEY_F1 		'V'

#define KEY_F2 		'W'

#define KEY_F3 		'X'

#define KEY_F4 		'Y'

#define KEY_A1		'G'

#define KEY_A2		'H'

#define KEY_A3		'I'

#define KEY_A4		'J'

#define KEY_SP		   '.'

#define KEY_ASTERISK  'Z'







#define PIN_LEN_MAX		12

#define PIN_LEN_MIN		4



#define INPUT_LEN_MAX	16



typedef struct {

	unsigned long keyEvnt;

	char keyName;

}KEYNAME;


/*
KEYNAME gKeyName[] = 

{

	{KEYBORADEVM_MASK_KEY_0, KEY_0},

	{KEYBORADEVM_MASK_KEY_1, KEY_1},

	{KEYBORADEVM_MASK_KEY_2, KEY_2},

	{KEYBORADEVM_MASK_KEY_3, KEY_3},

	{KEYBORADEVM_MASK_KEY_4, KEY_4},

	{KEYBORADEVM_MASK_KEY_5, KEY_5},

	{KEYBORADEVM_MASK_KEY_6, KEY_6},

	{KEYBORADEVM_MASK_KEY_7, KEY_7},

	{KEYBORADEVM_MASK_KEY_8, KEY_8},

	{KEYBORADEVM_MASK_KEY_9, KEY_9},

	{KEYBORADEVM_MASK_KEY_SP, KEY_SP},

	{KEYBORADEVM_MASK_KEY_ASTERISK, KEY_ASTERISK},

	{KEYBORADEVM_MASK_KEY_ALPHA, KEY_ALPHA},

	{KEYBORADEVM_MASK_KEY_OK, KEY_OK},

	{KEYBORADEVM_MASK_KEY_CANCEL, KEY_CANCEL},

	{KEYBORADEVM_MASK_KEY_CORRECT, KEY_CORR},

	{KEYBORADEVM_MASK_KEY_F1, KEY_F1},

	{KEYBORADEVM_MASK_KEY_F2, KEY_F2},

	{KEYBORADEVM_MASK_KEY_F3, KEY_F3},

	{KEYBORADEVM_MASK_KEY_F4, KEY_F4},

	{KEYBORADEVM_MASK_KEY_A, KEY_A1},

	{KEYBORADEVM_MASK_KEY_B, KEY_A2},

	{KEYBORADEVM_MASK_KEY_C, KEY_A3},

	{KEYBORADEVM_MASK_KEY_D, KEY_A4},

	{KEYBORADEVM_MASK_KEY_UP, KEY_UP},

	{KEYBORADEVM_MASK_KEY_DOWN, KEY_DOWN},

	{0, 0},

};
*/


/* GetKey(): Tries to get a key in specified time. (timeout unit: milliseconds)

 * Return values:

 *    0:	Success.

 *   -1:	Invaild key.

 *   -2:	Time out.

 */

int PINPad_GetKey(tti_byte *KeyOut,  int TimeOut)
{
/*
	TEvntMask tEvnt, KeyEvnt;

	int iLoop = 0;

	printf("PINPad_GetKey\n");

	tEvnt = TTIOSEVM_MASK_KEYBORAD | TTIOSEVM_MASK_TIMER; // register all key

	EvntMgrDeregLevel1EventInterest(0xffffffff);

	EvntMgrDeregKeyboardInterest(0xffffffff);

	TtiOSFlushMailbox(); // flush mail box

	EvntMgrRegLevel1EventInterest(tEvnt); 

	EvntMgrRegKeyEventInterest(0xffffffff); 


	printf("TimeOut = %d\n", TimeOut);
	if(TimeOut > 0) // timeout
	{
		//tEvnt |= TTIOSEVM_MASK_TIMER;

		EvntMgrSetTimer (0,  TimeOut); 
	}else
	{
		EvntMgrSetTimer (0,  0); 

	}



	// ok, now we call a new function of sdk
	 EvntMgrWaitForEvent(&tEvnt, &KeyEvnt); 
	//EventMgrEventWaitKey(&tEvnt, &KeyEvnt);

	
	printf("55555\n");
	
	TtiOSFlushMailbox();

	EvntMgrKillTimer (0); 

	EvntMgrDeregLevel1EventInterest(0xffffffff);

	EvntMgrDeregKeyboardInterest(0xffffffff);


	printf("6666\n");
	
	if(tEvnt & TTIOSEVM_MASK_KEYBORAD)

	{

		while( gKeyName[iLoop].keyEvnt)

		{

			if ( KeyEvnt == gKeyName[iLoop].keyEvnt)

			{

				memcpy( KeyOut, &(gKeyName[iLoop].keyName), 1);

				return 0;

			}

			iLoop ++;

		}

		return -1; //Invaild key.

	}

	return  -2; //time out
*/

	return -2;
}



/* GetKeyboard(): Tries to get a key in specified time. It checks the

 * cancel status. (timeout unit: milliseconds)

 * Return values:

 *    0:	Success.

 *   -1:	Parameter error.

 *   -2:	Time out.

 *   -3:	Operation cancelled.

 */

int GetKeyboard(tti_byte *KeyOut, int TimeOut)
{
/*	
	int iRet;
	
	long int end = GetMilliSeconds() + TimeOut;

	if(KeyOut == NULL)

		return -1;

	TtiOsBeginGetKey();
	//printf("TtiOsBeginGetKey return = %d\n", iRet);

	while(GetMilliSeconds() < end)

	{

		if(PINPad_GetKey(KeyOut, 1200) == 0)

		{

		    TtiOsEndGetKey();

			return 0;

		}

	}

	

	TtiOsEndGetKey();

	return -2; // timeout
*/

	return 0;
}


/*

	This function try to get the 8 byte  pin format data. 

	Return value:

	   0:  Success

	  -1:  Cancel key pressed.

	  -2:  Press Corr when no data

	  -3:  Time out

	  -4:  Cancel command received

	  -5: PIN entry Times out 

*/

//int GetPin(int Line,tti_byte *Prompt, int TimeOut, tti_byte *PinOut, int *PinLenOut)
int GetPin(void)
{
/*
	int iRet, iLoop;

	int iPinLen = 0;

	tti_byte Key;

	tti_byte PinBuff[50] = {0};

	tti_byte DisplayBuf[22] = {0};

	long int EndTime = GetMilliSeconds() + TimeOut * 1000;

	DisplayInfoRight(Prompt, Line);

	//while(GetMilliSeconds() < EndTime)

	while((TimeOut == 0)?1:(GetMilliSeconds() < EndTime))

	{

		// handle key pressing

		iRet = GetKeyboard(&Key, 15000);

		if(iRet == -2)

			break; // timeout



		if(iRet == -3)

		{

			memset( PinBuff, 0, sizeof( PinBuff));

			return -4; // Cancel command received.

		}



		if(Key == KEY_CANCEL)

		{

			memset( PinBuff, 0, sizeof( PinBuff));

			return -1;

		}



		if(Key == KEY_CORR)

		{

			if ( iPinLen == 0)

			{

				memset( PinBuff, 0, sizeof( PinBuff));

				return -2;

			}else

			{

				memset( PinBuff, 0, sizeof( PinBuff));

				iPinLen = 0;

				//LCDCleanOneLine( Line);

				DisplayInfoRight(Prompt, Line);

				continue;

			}

		}



		if(Key == KEY_OK)

		{

			if(iPinLen < PIN_LEN_MIN)

			{

				memset( PinBuff, 0, sizeof( PinBuff));

				iPinLen = 0;

				//LCDCleanOneLine( Line);

				//LCDDispOneLine(Line-1, Prompt, 0, TA_RIGHT);
				DisplayInfoRight(Prompt, Line);
				continue;

			}else

			{

				memcpy( PinOut, PinBuff, iPinLen);

				*PinLenOut = iPinLen;

				memset( PinBuff, 0, sizeof( PinBuff));

				return 0;

			}

		}



		if((Key <= KEY_9) && (Key >= KEY_0))

		{

			if ( iPinLen >= PIN_LEN_MAX)

				continue;



			PinBuff[iPinLen] = Key;

			memset( DisplayBuf, 0, sizeof( DisplayBuf));

			memcpy( DisplayBuf, Prompt, strlen(Prompt));

			for( iLoop = 0;iLoop <= iPinLen; iLoop++)

				DisplayBuf[ strlen(Prompt)+iLoop ] = '*';



			//LCDCleanOneLine( Line);

			//LCDDispOneLine(Line-1, DisplayBuf, 0, TA_RIGHT);
			DisplayInfoRight(DisplayBuf, Line);		
			iPinLen++;

			continue;

		}

	}

	return -3; // timeout
*/

	return 0;
}

#define ERR_OK				'0'
#define ERR_RANGE			'1'
#define ERR_INVALID        	'2'
#define ERR_CANCEL        	'3'
#define ERR_CORR        	'4'
#define ERR_TIMEOUT       	'5'
#define ERR_TIMESOUT     	'6'
#define ERR_DUPKEY        	'7'
#define ERR_KTK_EXIST		'8'


/*
int AscByteToHex(char *Dest, char Src)
{
	if((Dest == NULL) || (Src < '0'))
		return -1;

	*Dest = Src - '0';
	if((*Dest > 9) && (*Dest < 17))
		return -1;

	if(*Dest >= 17)
	{
		*Dest -= 7;
		if(*Dest > 0x0f)
			return -1;
	}

	return 0;
}

int AtoHex(char *Dest, char *Src, int SrcLen)
{
	int i;
	char Tmp1, Tmp2;

	if((Dest == NULL) || (Src == NULL))
		return -1;

	for(i=0; i<SrcLen; i+=2)
	{
		if(AscByteToHex(&Tmp1, Src[i]) != 0)
			return -1;

		if(AscByteToHex(&Tmp2, Src[i+1]) != 0)
			return -1;

		Dest[i/2] = (Tmp1 << 4) | Tmp2;
	}

	return 0;
}

int HextoA(char *Dest, const char *Src, int SrcLen)
{
	int i;
	char Tmp;

	if((Dest == NULL) || (Src == NULL))
		return -1;

	for(i=0; i<SrcLen; i++)
	{
		Tmp = (Src[i] >> 4) & 0x0f;
		Dest[i * 2] = Tmp + '0';
		if(Tmp > 9)
			Dest[i * 2] += 7;

		Tmp = Src[i] & 0x0f;
		Dest[i * 2 + 1] = Tmp + '0';
		if (Tmp > 9)
			Dest[i * 2 + 1] += 7;
	}

	return 0;
}
*/

//Jason removed on 2019.12.25 for HM chip
/*
int BuildPinBlock( tti_byte *InputPin, int PinLen, tti_byte *PinBlock)
{
	tti_byte PinTmp[16];

	if(( PinLen< PIN_LEN_MIN)&&( PinLen>PIN_LEN_MAX))
	{
		return -2;
	}

	if((InputPin == NULL) &&(PinBlock == NULL))
	{
		return -2;
	}

	

	PinTmp[0] = '0';

	if( PinLen < 10)

	{

		PinTmp[1] = PinLen+0x30;

	}else 

	{

		PinTmp[1] = PinLen+0x37;

	}

	memset( PinTmp+2, 'F',14);

	memcpy( PinTmp+2, InputPin, PinLen);

	if (AtoHex(PinBlock, PinTmp, 16))

	{	

		memset( PinTmp, 0, sizeof(PinTmp));

		return -1;

	}

	memset( PinTmp, 0, sizeof( PinTmp));

	return 0;

}
*/
//Jason removed end

#define DES_KEY	"12345678"

void RequestPinDataEntry(tti_byte *InBuff, int InLen, tti_byte *OutBuff, int *OutLen)
{
/*	
	int iRet;
	tti_byte InputPin[20] = {0};
	tti_byte PinBlock[20] = {0};
	int PinLen;
	//for debug
	char tmp1[50] = {0};

	memcpy(OutBuff, "41.", 3);

	OutBuff[3]  = ERR_RANGE;				// assume error

	memcpy( OutBuff + 4, "01", 2);

	*OutLen = 4;
	printf("3333\n");
	iRet = GetPin(2, "PIN=", 50, InputPin, &PinLen);
	printf("4444\n");
	switch( iRet)
	{
		case 0:
			break;

		case -1:

			OutBuff[3] = '3';

			return;



		case -2:

			OutBuff[3] = ERR_CORR;

			return;



		case -3:

			OutBuff[3] = ERR_TIMEOUT;

			return;



		//Cancel from terminal

		case -4:

			//memcpy( OutBuff, "50.0",sizeof(OutBuff));

			*OutLen = 0;

			return;



		default:

			return;

	}

	if(BuildPinBlock( InputPin, PinLen, PinBlock)!= 0)		
	{

		TtiOsDebugPrintf("Building PIN block failed");

		memset(InputPin,0,sizeof(InputPin));

		memset(PinBlock,0,sizeof(PinBlock));

		PinLen = 0;

		return;

	}


	
	
	HextoA(tmp1, PinBlock, 8);
	TtiOsDebugPrintf("PinBlock is %s", tmp1);

	memset (tmp1, 0, sizeof(tmp1));
	TtiOsDesEncrypt(PinBlock, 8, tmp1, DES_KEY);
	
	*OutLen = 22;
	OutBuff[3] = ERR_OK;
	
	HextoA(OutBuff + 6, tmp1, 8);
	return;
*/
}

int WaitForCancel()
{
/*	
	TEvntMask waitEevent;
	tti_uint16 timeout;
	
	waitEevent = WaitAKeyDuringTimeMS(KEYBORADEVM_MASK_KEY_CANCEL, 300);
	if (waitEevent == KEYBORADEVM_MASK_KEY_CANCEL)
	{
		return TRUE;
	}

	return FALSE;
*/
	return FALSE;
}

