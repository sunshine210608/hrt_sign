#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "OsTypes.h"
#include "utils.h"
#include "defines.h"
#include "misce.h"
#include "../protocol.h"
#include "emvDebug.h"
//#include "../Util.h"

static const char gpuiEvenParity [16] = 
{'\x00','\x80', '\x80', '\x00', '\x80', '\x00', '\x00', '\x80',
	'\x80', '\x00', '\x00', '\x80', '\x00', '\x80', '\x80', '\x00'};

tti_int32 dateEachMonth[MONTHS_IN_YEAR]={31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

//------------------------------------------------------------------------------------------
tti_bool isEven(tti_int32 value)
{
	return ((value/2)*2==value);
}

//------------------------------------------------------------------------------------------
#if 0
unsigned char CalcLRC(tti_byte  *message, tti_int32 msgLen)
{
	unsigned char LRC=0;
	tti_int32 index;

	for (index=0; index<msgLen; index++)
	{
		LRC^=message[index];
	}
	
	return LRC;
}
#endif
//------------------------------------------------------------------------------------------
void AddEvenParity (char* szMessage, int iStrLen)
{
	int iStrIndex;

	for (iStrIndex=0; iStrIndex<iStrLen; iStrIndex++)
	{
		szMessage[iStrIndex]|= gpuiEvenParity [szMessage[iStrIndex] & 0x0F] 
			^ gpuiEvenParity [szMessage[iStrIndex]>> 4];
	}
}

//------------------------------------------------------------------------------------------
void FormatString(char* szString, int iFillLength, char cFillChar, int iAlign)
{
	int i,j,iStrLength;

	iStrLength=strlen(szString);
	if (iFillLength<=iStrLength)
	{
		szString[iFillLength]='\0';
		return;
	}
	switch (iAlign)
	{
		case MESSAGE_NONE_JUSTIFY:
			break;
		case MESSAGE_LEFT_JUSTIFY:
			for (i=strlen(szString);i<iFillLength;i++)
			{
				szString[i]=cFillChar;
			}
			szString[iFillLength]='\0';
			break;
		case MESSAGE_RIGHT_JUSTIFY:
			for (i=iFillLength-1,j=iStrLength-1;j>=0;i--,j--)
			{
				szString[i]=szString[j];
			}
			for (i=iFillLength-iStrLength-1;i>=0;i--)
			{
				szString[i]=cFillChar;
			}
			szString[iFillLength]='\0';
			break;
	}
}

//------------------------------------------------------------------------------------------
void RestoreSmartCardInfo(tti_byte *fromBuffer, tti_int32 fromBufferLen, 
	tti_byte *toBuffer, tti_int32 *toBufferLen)
{
	tti_uint16 index;

	*toBufferLen=0;
	for (index=0; index<fromBufferLen; index++)
	{
		switch (index%4)
		{
			case 0:
				toBuffer[*toBufferLen]=(fromBuffer[index]&0x3F)<<2;
				break;
			case 1:
				toBuffer[*toBufferLen]|=(fromBuffer[index]&0x30)>>4;
				(*toBufferLen)++;
				toBuffer[*toBufferLen]=(fromBuffer[index]&0x0f)<<4;
				break;
			case 2:
				toBuffer[*toBufferLen]|=(fromBuffer[index]&0x3C)>>2;
				(*toBufferLen)++;
				toBuffer[*toBufferLen]=(fromBuffer[index]&0x03)<<6;
				break;
			case 3:
				toBuffer[*toBufferLen]|=(fromBuffer[index]&0x3F);
				(*toBufferLen)++;
				break;
		}
	}
}

//--------------------------------------------------------------------------------------------
tti_int32 GetCharPositionInBuffer(tti_tchar* szString, tti_int32 uiStrLen, tti_tchar ch)
{
	tti_int32 iPosition;

	if(NULL == szString)
	{
		return	-1;
	}
	
	for (iPosition=0; iPosition<uiStrLen; iPosition++)
	{
		if (szString[iPosition]==ch)
		{
			return iPosition;
		}
	}

	return -1;
}

//------------------------------------------------------------------------------------------
void GetResponseFromBuffer(tti_byte *buffer, tti_int32 bufferLen, tti_byte *msg, tti_int32 *msgLen)
{
	tti_int32 stxPosition, etxPosition;

	*msgLen=0;
	stxPosition=GetCharPositionInBuffer((tti_tchar *)buffer, bufferLen, STX);
	if (stxPosition>=0)
	{
		etxPosition=GetCharPositionInBuffer((tti_tchar *)buffer, bufferLen, ETX);
		if (etxPosition>stxPosition)
		{
			*msgLen=etxPosition-stxPosition-1;
			memcpy(msg, buffer+stxPosition+1, *msgLen);
		}
	}
}

//------------------------------------------------------------------------------------------
tti_tchar TranslateDigitToHexChar(tti_int8 iTranslatedDigit)
{
	if ((iTranslatedDigit>=0)&&(iTranslatedDigit<=9))
	{
		return '0'+iTranslatedDigit;
	}
	if ((iTranslatedDigit>=10)&&(iTranslatedDigit<=15))
	{
		return 'A'+iTranslatedDigit-10;
	}

	return 0;
}

//------------------------------------------------------------------------------------------
void GetPrimaryAccount(tti_tchar *szInString, tti_tchar *szPrimaryAccount)
{
	tti_int16 iStartPosition;

	if((NULL == szInString) || (NULL == szPrimaryAccount))
	{
		return;
	}
	
	iStartPosition=strlen(szInString)-12-1;
	if (iStartPosition<0)
	{
		iStartPosition=0;
	}
	strncpy(szPrimaryAccount, szInString+iStartPosition, 12);
	szPrimaryAccount[12]='\0';
}

//------------------------------------------------------------------------------------------
void printByteArray(tti_byte *buffer, tti_int32 bufferSize)
{
/*
	int index;
	
	for (index=0; index<bufferSize; index++)
	{
		if(index%16 == 0 && index != 0)
		{
			DPRINTF("\n");
		}
		DPRINTF("%02x ", buffer[index]);
	}
	
	DPRINTF("\n");
*/
    hexdump(buffer, bufferSize);
}

//------------------------------------------------------------------------------------------
void	printCharArray(tti_byte *buffer, tti_int32 bufferSize)
{
	int index;

	for (index=0; index<bufferSize; index++)
	{
		DPRINTF("%c", buffer[index]);
	}

	DPRINTF("\n");
}

//------------------------------------------------------------------------------------------
void TranslateHexToChars(tti_byte *hexArray, tti_int32 hexArraySize, tti_tchar *tempBuff)
{
	tti_int32 index;

	tempBuff[0]='\0';

	if (hexArray!=NULL)
	{
		for (index=0; index<hexArraySize; index++)
		{
			sprintf(tempBuff, "%s%02X", tempBuff, hexArray[index]);
		}
	}
}

//------------------------------------------------------------------------------------------
tti_int32 GetUnFixMessageLength(tti_byte *sourceMsg, tti_int32 sourceMsgLen, 
	tti_byte seperateByte)
{
	tti_int32 counter=0;

	while (counter<sourceMsgLen)
	{
		if (sourceMsg[counter]!=seperateByte)
		{
			counter++;
		}
		else
		{
			break;
		}
	}

	return counter;
}

//------------------------------------------------------------------------------------------
tti_int32 TranslateStringToByteArray(tti_tchar *string, tti_byte *byteArray, tti_int32 *byteArrayLen)
{
	tti_bool isHighHalfByte=TRUE;
	tti_int32 index, stringLen;
	tti_byte halfByte;

	*byteArrayLen=0;
	stringLen=strlen(string);
	
	for (index=0; index<stringLen; index++)
	{
		halfByte=0;
		if ((string[index]>='0')&&(string[index]<='9'))
		{
			halfByte=string[index]-'0';
		}
		else if ((string[index]>='A')&&(string[index]<='F'))
		{
			halfByte=string[index]-'A'+10;
		}
		else	if ((string[index]>='a')&&(string[index]<='f'))
		{
			halfByte=string[index]-'a'+10;
		}
		else
		{
			return STATUS_FAIL;
		}

 		if (isHighHalfByte)
		{
			byteArray[*byteArrayLen]=(tti_byte)(halfByte<<4);
		}
		else
		{
			byteArray[*byteArrayLen]+=halfByte;
			(*byteArrayLen)++;
		}

		isHighHalfByte=!isHighHalfByte;
	}

	if (!isEven(stringLen))
	{
		(*byteArrayLen)++;
	}

	return STATUS_OK;
}

//--------------------------------------------------------------------------------------------
tti_int32 TranslateStringToBCDArray_FillLeftZero_WithLen(tti_tchar *string, tti_byte *byteArray, 
	tti_int32 *byteArrayLen, tti_int32 stringTotalLen)
{
	tti_tchar tempString[BUFFER_SIZE_256BYTE+1];
	tti_tchar tempStringTotal[BUFFER_SIZE_256BYTE+1] = {0};
	
	//tti_int32 len;

	if ((strlen(string)>BUFFER_SIZE_256BYTE)||(!isDigit(string)))
	{
		return STATUS_FAIL;
	}

	if ( stringTotalLen < strlen(string))
	{
		return STATUS_FAIL;
	}else if (stringTotalLen == strlen(string))
	{
		strcpy(tempStringTotal, string);
	}else
	{
		memset(tempStringTotal, '0', stringTotalLen - strlen(string));
		tempStringTotal[stringTotalLen - strlen(string)] = 0x00;
		strcat(tempStringTotal, string);
		DPRINTF("tempStringTotal = %s\n", tempStringTotal);
	}

	if (isEven(strlen(tempStringTotal)))
	{
		strcpy(tempString, tempStringTotal);
	}
	else
	{
		strcpy(tempString, "0");
		strcat(tempString, tempStringTotal);
	}

	return TranslateStringToByteArray(tempString, byteArray, byteArrayLen);
}


//--------------------------------------------------------------------------------------------
tti_int32 TranslateStringToBCDArray_FillLeftZero(tti_tchar *string, tti_byte *byteArray, tti_int32 *byteArrayLen)
{
	tti_tchar tempString[BUFFER_SIZE_256BYTE+1];
	//tti_int32 len;

	if ((strlen(string)>BUFFER_SIZE_256BYTE)||(!isDigit(string)))
	{
		return STATUS_FAIL;
	}

	if (isEven(strlen(string)))
	{
		strcpy(tempString, string);
	}
	else
	{
		strcpy(tempString, "0");
		strcat(tempString, string);
	}

	return TranslateStringToByteArray(tempString, byteArray, byteArrayLen);
}

//--------------------------------------------------------------------------------------------
tti_int32 TranslateStringToBCDArray(tti_tchar *string, tti_byte *byteArray, tti_int32 *byteArrayLen)
{
	tti_tchar tempString[BUFFER_SIZE_256BYTE+1];
	tti_int32 len;

	len=strlen(string);

	if ((len>BUFFER_SIZE_256BYTE)||(!isDigit(string)))
	{
		return STATUS_FAIL;
	}

	strcpy(tempString, string);
	if (!isEven(len))
	{
		strcat(tempString, "F");
	}

	return TranslateStringToByteArray(tempString, byteArray, byteArrayLen);
}

//--------------------------------------------------------------------------------------------
char* AppendChar(char* szString, char ch)
{
	int iLength;

	iLength=strlen(szString);
	szString[iLength]=ch;
	szString[iLength+1]='\0';

	return szString;
}

//--------------------------------------------------------------------------------------------
tti_bool isDigit(char *checkedDigitString)
{
	while ((*checkedDigitString)!='\0')
	{
		if (((*checkedDigitString)<'0') || ((*checkedDigitString)>'9'))
		{
			return FALSE;
		}
		checkedDigitString++;
	}

	return TRUE;
}

//------------------------------------------------------------------------------------------
tti_int32 checkDateFormat(char *checkedDate)
{
	tti_tchar tempString[3];
	tti_int32 year, month, date;

	if (strlen(checkedDate)!=DATE_SIZE)
	{
		return STATUS_FAIL;
	}

	if (!isDigit(checkedDate))
	{
		return STATUS_FAIL;
	}

	strncpy(tempString, checkedDate+2, 2);
	tempString[2]='\0';
	month=atoi(tempString);

	if ((month<1)||(month>12))
	{
		return STATUS_FAIL;
	}

	strncpy(tempString, checkedDate+4, 2);
	tempString[2]='\0';
	date=atoi(tempString);

	if ((date<1) || (date>dateEachMonth[month-1]))
	{
		return STATUS_FAIL;
	}

	if ((date==29)&&(month==2))
	{
		strncpy(tempString, checkedDate, 2);
		tempString[2]='\0';
		year=atoi(tempString);
		if ((year/4)*4!=year)
		{
			return STATUS_FAIL;
		}
	}

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetDate(char *request, tti_byte *date, tti_int32 *dateLen)
{
	char tempDate[DATE_SIZE+1];
	
	if (strlen(request)<DATE_SIZE)
	{
		return STATUS_FAIL;
	}

	strncpy(tempDate, request, DATE_SIZE);
	tempDate[DATE_SIZE]='\0';

	if (checkDateFormat(tempDate)==STATUS_OK)
	{
		TranslateStringToBCDArray(tempDate, date, dateLen);
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 checkLongDateFormat(char *checkedDate)
{
	//tti_tchar tempString[3];
	//tti_byte month, date;
	
	if (strlen(checkedDate)!=LONG_DATE_SIZE)
	{
		return STATUS_FAIL;
	}

	if (!isDigit(checkedDate))
	{
		return STATUS_FAIL;
	}

	return checkDateFormat(checkedDate+2);
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetLongDate(char *request, char *longDate)
{
	char tempDate[DATE_SIZE+1];
	
	if (strlen(request)<DATE_SIZE)
	{
		return STATUS_FAIL;
	}

	strncpy(tempDate, request, DATE_SIZE);
	tempDate[DATE_SIZE]='\0';

	if (checkLongDateFormat(tempDate)==STATUS_OK)
	{
		strcpy(longDate, tempDate);
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 checkTimeFormat(char *checkedTime)
{
	tti_tchar tempString[3];
	tti_byte hour, minute, second;
	
	if (strlen(checkedTime)!=TIME_SIZE)
	{
		return STATUS_FAIL;
	}

	if (!isDigit(checkedTime))
	{
		return STATUS_FAIL;
	}

	strncpy(tempString, checkedTime, 2);
	tempString[2]='\0';
	hour=atoi(tempString);

	//if ((hour<0)||(hour>23))
	if (hour>23)
	{
		return STATUS_FAIL;
	}

	strncpy(tempString, checkedTime+2, 2);
	tempString[2]='\0';
	minute=atoi(tempString);

	//if ((minute<0)||(minute>59))
	if (minute>59)
	{
		return STATUS_FAIL;
	}

	strncpy(tempString, checkedTime+4, 2);
	tempString[2]='\0';
	second=atoi(tempString);

	//if ((second<0)||(second>59))
	if (second>59)
	{
		return STATUS_FAIL;
	}

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetTime(char *request, tti_byte *time, tti_int32 *timeLen)
{
	char tempTime[TIME_SIZE+1];
	
	if (strlen(request)<TIME_SIZE)
	{
		return STATUS_FAIL;
	}

	strncpy(tempTime, request, TIME_SIZE);
	tempTime[TIME_SIZE]='\0';

	if (checkTimeFormat(tempTime)==STATUS_OK)
	{
		TranslateStringToBCDArray(tempTime, time, timeLen);
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetFixedString(char *request, tti_int32 fixedStringSize, char *fixedString)
{
	if (strlen(request)<fixedStringSize)
	{
		return STATUS_FAIL;
	}

	strncpy(fixedString, request, fixedStringSize);
	fixedString[fixedStringSize]='\0';

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_bool charBetweenIn(tti_tchar checkChar, tti_tchar leftChar, tti_tchar rightChar)
{
	return ((checkChar>=leftChar)&&(checkChar<=rightChar));
}

//------------------------------------------------------------------------------------------
tti_bool isHexDigit(tti_tchar *checkedDigitString)
{
	while ((*checkedDigitString)!='\0')
	{
		if (charBetweenIn(*checkedDigitString, '0', '9')
			|| charBetweenIn(*checkedDigitString, 'a', 'f')
			|| charBetweenIn(*checkedDigitString, 'A', 'F'))
		{
			checkedDigitString++;
			continue;
		}

		return FALSE;
	}

	return TRUE;
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetByteArray(char *request, tti_int32 byteArrayLen, tti_byte *byteArray)
{
	tti_tchar tempBuff[BUFFER_SIZE_256BYTE+1];
	tti_int32 tempLen;

	if (strlen(request)<byteArrayLen*2)
	{
		return STATUS_FAIL;
	}

	strncpy(tempBuff, request, byteArrayLen*2);
	tempBuff[byteArrayLen*2]='\0';

	if (!isHexDigit(tempBuff))
	{
		return STATUS_FAIL;
	}
	
	return TranslateStringToByteArray(tempBuff, byteArray, &tempLen);
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetInt32(char *request, int length, tti_int32 *int32Data)
{
	tti_tchar tempBuffer[INT32_SIZE+1];

	if ((length>INT32_SIZE)||(strlen(request)<length))
	{
		return STATUS_FAIL;
	}

	strncpy(tempBuffer, request, length);
	tempBuffer[length]=0;

	if (!isDigit(tempBuffer))
	{
		return STATUS_FAIL;
	}

	*int32Data=atol(tempBuffer);
	
	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetInt16(char *request, int length, tti_int16 *int16Data)
{
	tti_tchar tempBuffer[INT16_SIZE+1];

	if ((length>INT16_SIZE)||(strlen(request)<length))
	{
		return STATUS_FAIL;
	}

	strncpy(tempBuffer, request, length);
	tempBuffer[length]=0;

	if (!isDigit(tempBuffer))
	{
		return STATUS_FAIL;
	}

	*int16Data=atoi(tempBuffer);
	
	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetInt8(char *request, int length, tti_int8 *int8Data)
{
	tti_tchar tempBuffer[INT8_SIZE+1];

	if ((length>INT8_SIZE)||(strlen(request)<length))
	{
		return STATUS_FAIL;
	}

	strncpy(tempBuffer, request, length);
	tempBuffer[length]=0;

	if (!isDigit(tempBuffer))
	{
		return STATUS_FAIL;
	}

	*int8Data=(tti_int8)atoi(tempBuffer);
	
	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetBool(char *request, tti_bool *boolData)
{
	tti_int8 tempValue;

	if (parseGetInt8(request, 1, &tempValue)==STATUS_OK)
	{
		switch (tempValue)
		{
			case 0:
				*boolData=FALSE;
				return STATUS_OK;

			case 1:
				*boolData=TRUE;
				return STATUS_OK;
	
			default:
				break;
		}
	}

	return STATUS_FAIL;
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetAmount(char *request, tti_int32 *amount)
{
	return parseGetInt32(request, AMOUNT_SIZE, amount);
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetCandidateAid(char *request, CANDIDATE_INFO *candidateAidInfo)
{
	tti_int32 tempBufferSize;
	tti_tchar tempBuffer[BUFFER_SIZE_256BYTE+1];
	tti_int32 versionLen;

	if (strlen(request)<5)
	{
		return STATUS_FAIL;
	}
	strncpy(tempBuffer, request, APP_VERSION_NUMBER_SIZE*2);
	tempBuffer[APP_VERSION_NUMBER_SIZE*2]='\0';
	
	if (TranslateStringToByteArray(tempBuffer, candidateAidInfo->verionNumber, &versionLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	request+=versionLen*2;
	
	if (parseGetBool(request, &candidateAidInfo->exactMatch)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	request++;
	
	tempBufferSize=GetUnFixMessageLength((tti_byte *)request, strlen(request), SEPERATOR);
	if ((tempBufferSize<2)||(tempBufferSize>AID_NAME_SIZE*2)||(!isEven(tempBufferSize)))
	{
		return STATUS_FAIL;
	}
	strncpy(tempBuffer, request, tempBufferSize);
	tempBuffer[tempBufferSize]='\0';
	
	TranslateStringToByteArray(tempBuffer, candidateAidInfo->aidName, &candidateAidInfo->aidLength);

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetUnfixedByteArray(char *request,  tti_byte *byteArray, tti_int32 *byteArrayLen)
{
	tti_tchar tempBuff[BUFFER_SIZE_1K+1];
	tti_int32 len;

	len=GetUnFixMessageLength((tti_byte *)request, strlen(request), SEPERATOR);
	
	if ((*byteArrayLen)*2<len)
	{
		*byteArrayLen=0;
		return STATUS_FAIL;
	}
	
	strncpy(tempBuff, request, len);
	tempBuff[len]='\0';

	if (!isHexDigit(tempBuff))
	{
		return STATUS_FAIL;
	}
	
	return TranslateStringToByteArray(tempBuff, byteArray, byteArrayLen);
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetTag(char *request, tti_uint16 *tag)
{
	tti_tchar tempBuff[BUFFER_SIZE_256BYTE];
	tti_byte tempByteBuff[2];
	tti_int32 tempByteBuffLen;

	if (strlen(request)<4)
	{
		ReportLine();
		return STATUS_FAIL;
	}
	
	strncpy(tempBuff, request, 4);
	tempBuff[4]='\0';

	if (TranslateStringToByteArray(tempBuff, tempByteBuff, &tempByteBuffLen)!=STATUS_OK)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	*tag=tempByteBuff[0]*256+tempByteBuff[1];

	return STATUS_OK;	
}

//------------------------------------------------------------------------------------------
tti_int32 getAmountByTag(TagList *tagList, tti_uint16 tag)
{
	tti_byte *pValue;
	tti_uint16 valueSize;
	tti_int32 index;
	tti_int32 result;

	if (!TagIsExisted(tagList, tag))
	{
		return 0;
	}

	pValue=GetTagValue(tagList, tag);
	valueSize=GetTagValueSize(tagList, tag);
	
	result=0;
	for (index=0; index<valueSize; index++)
	{
		result*=100;
		result+=(pValue[index]>>4)*10+(pValue[index]&0x0F);
	}

	return result;
}

#if 0
void foramtAmount(tti_tchar *szSrc, tti_tchar *szDest)
{	
	tti_tchar szTmp[100] = {0};
	tti_tchar szTmp2[100] = {0};
//	tti_tchar szTmp3[100] = {0};
	tti_int32 iIndex;
	tti_int32 iDigitNum;

	strcpy(szTmp, szSrc);

	for (iIndex = 0; iIndex < strlen(szTmp); iIndex++)
	{
		if (szTmp[iIndex] != '0')
		{
			break;
		}
	}
	iDigitNum = strlen(szTmp) - iIndex;
	strncpy(szTmp2, szTmp + iIndex, iDigitNum);
	if (iDigitNum == 2)
	{
		sprintf(szDest, "0.%s", szTmp2);
	}else if (iDigitNum == 1)
	{
		sprintf(szDest, "0.0%s", szTmp2);
	}else if (iDigitNum == 0)
	{
		strcpy(szDest, "0.00");
	}else
	{
		strncpy(szDest, szTmp2, iDigitNum - 2);
		strcat(szDest, ".");
		strcat(szDest, szTmp2 + iDigitNum - 2);
		
	}
	
}
#endif

void foramtAmount(char *szSrc, char *szDest)
{	
	char szTmp[100] = {0};
	char szTmp2[100] = {0};
	int iIndex;
	int iDigitNum;

	strcpy(szTmp, szSrc);

	for (iIndex = 0; iIndex < strlen(szTmp); iIndex++)
	{
		if (szTmp[iIndex] != '0')
		{
			break;
		}
	}
	iDigitNum = strlen(szTmp) - iIndex;
	strncpy(szTmp2, szTmp + iIndex, iDigitNum);
	if (iDigitNum == 2)
	{
		sprintf(szDest, "0.%s", szTmp2);
	}else if (iDigitNum == 1)
	{
		sprintf(szDest, "0.0%s", szTmp2);
	}else if (iDigitNum == 0)
	{
		strcpy(szDest, "0.00");
	}else
	{
		strncpy(szDest, szTmp2, iDigitNum - 2);
		strcat(szDest, ".");
		strcat(szDest, szTmp2 + iDigitNum - 2);
	}
}


tti_int32 formatAmountByTag(TagList *tagList, tti_uint16 tag, tti_tchar *szFormatAmount)
{
//	tti_byte *pValue;
//	tti_uint16 valueSize;
//	tti_int32 index;
//	tti_int32 result;
	tti_tchar szTmp[100] = {0};
//	tti_tchar szTmp2[100] = {0};
//	tti_tchar szTmp3[100] = {0};
//	tti_int32 iIndex;
//	tti_int32 iDigitNum;

	if (!TagIsExisted(tagList, tag))
	{
		return 0;
	}

//	pValue=GetTagValue(tagList, tag);
//	valueSize=GetTagValueSize(tagList, tag);

	TranslateHexToChars(GetTagValue(tagList, tag), 
			GetTagValueSize(tagList, tag), szTmp);		

	foramtAmount(szTmp, szFormatAmount);
	/*
	for (iIndex = 0; iIndex < strlen(szTmp); iIndex++)
	{
		if (szTmp[iIndex] != '0')
		{
			break;
		}
	}
	iDigitNum = strlen(szTmp) - iIndex;
	strncpy(szTmp2, szTmp + iIndex, iDigitNum);
	if (iDigitNum == 2)
	{
		sprintf(szFormatAmount, "0.%s", szTmp2);
	}else if (iDigitNum == 1)
	{
		sprintf(szFormatAmount, "0.0%s", szTmp2);
	}else if (iDigitNum == 0)
	{
		strcpy(szFormatAmount, "0.00");
	}else
	{
		strncpy(szFormatAmount, szTmp2, iDigitNum - 2);
		strcat(szFormatAmount, ".");
		strcat(szFormatAmount, szTmp2 + iDigitNum - 2);
		
	}
	*/
	
	return 0;
}


//------------------------------------------------------------------------------------------
tti_int32 translateByteArrayToInt(tti_byte *byteArray, tti_int32 byteArrayLen)
{
	tti_int32 result, index;

	result=0;

	if (byteArrayLen<=4)
	{
		for (index=0; index<byteArrayLen; index++)
		{
			result<<=8;
			result+=byteArray[index];
		}
	}

	return result;
}

//------------------------------------------------------------------------------------------
tti_int32 getDataByTag(TagList *tagList, tti_uint16 tag)
{
	if (TagIsExisted(tagList, tag))
	{
		return translateByteArrayToInt(GetTagValue(tagList, tag), 
			GetTagValueSize(tagList, tag));
	}
	else
	{
		return 0;
	}
}

//------------------------------------------------------------------------------------------
void formatPlainPin(tti_tchar *pinNumber, tti_byte *formatedPinNumber)
{
	tti_tchar formatPinString[16+1];
	tti_int32 pinNumberLen;

	DPRINTF("raw pin number\n");
	printByteArray((tti_byte *)pinNumber, strlen(pinNumber));
	
	memset(formatPinString, 'F', 16);
	formatPinString[0]='2';
	formatPinString[1]=TranslateDigitToHexChar((tti_uint8)strlen(pinNumber));
	memcpy(&formatPinString[2], pinNumber, strlen(pinNumber));
	formatPinString[16]='\0';

	DPRINTF("prepare format pin string= %d", strlen(formatPinString));
	printByteArray((tti_byte *)formatPinString, strlen(formatPinString));
	
	TranslateStringToByteArray(formatPinString, formatedPinNumber, &pinNumberLen);
	DPRINTF("formatted pin number[%d]", pinNumberLen);
	printByteArray(formatedPinNumber, pinNumberLen);
}

//------------------------------------------------------------------------------------------
void printCertificate(Certificate *certificate)
{
	DPRINTF("Mod len= %d\n", certificate->modLen);
	printByteArray(certificate->modData, certificate->modLen);

	DPRINTF("exp len=%d\n", certificate->expLen);
	printByteArray(certificate->exp, certificate->expLen);
}

//------------------------------------------------------------------------------------------
tti_bool isExpiredDate(tti_byte *shortDate_MMYY, tti_byte *currentDate_YYMMDD)
{
	tti_byte tempShortDate_YYMM[2];
	
	tempShortDate_YYMM[0]=shortDate_MMYY[1];
	tempShortDate_YYMM[1]=shortDate_MMYY[0];
	
	if ((tempShortDate_YYMM[1]<0x01)||
		((tempShortDate_YYMM[1]>=0x0a)&&(tempShortDate_YYMM[1]<0x10))||
		(tempShortDate_YYMM[1]>0x12))
	{
		return FALSE;
	}

	if (((tempShortDate_YYMM[0]>=0x50)&&(currentDate_YYMMDD[0]>=0x50))||
		((tempShortDate_YYMM[0]<0x50)&&(currentDate_YYMMDD[0]<0x50)))
	{
		if (memcmp(tempShortDate_YYMM, currentDate_YYMMDD, 2)<0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		if (tempShortDate_YYMM[0]>=0x50)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

}

//------------------------------------------------------------------------------------------
void translateBcdArrayToString(tti_byte *bcdArray, tti_int32 bcdArrayLen, tti_tchar *string)
{
	tti_int32 index;
	tti_tchar ch;

	for (index=0; index<bcdArrayLen; index++)
	{
		ch=bcdArray[index]>>4;
		if ((ch>=0)&&(ch<=9))
		{
			*string=ch+'0';
			string++;
		}
		else
		{
			*string='\0';
			return;
		}
		
		ch=bcdArray[index]&0x0F;
		if ((ch>=0)&&(ch<=9))
		{
			*string=ch+'0';
			string++;
		}
		else
		{
			*string='\0';
			return;
		}
	}
	
	*string='\0';
}

//------------------------------------------------------------------------------------------
void castInt32ToByteArray(tti_int32 intValue, tti_byte *byteArray)
{
	tti_int32 index;

	for (index=3; index>=0; index--)
	{
		byteArray[index]=(tti_byte)(intValue&0x0FF);
		intValue>>=8;
	}
}

//------------------------------------------------------------------------------------------
void castAmountToBCDArray(tti_int32 intValue, tti_byte *bcdArray)
{
	tti_int32 index;

	for (index=5; index>=0; index--)
	{
		bcdArray[index]=intValue%10;
		intValue/=10;
		bcdArray[index]+=(intValue%10)*16;
		intValue/=10;
	}
}

//------------------------------------------------------------------------------------------
void castAmountToString(tti_int32 intValue, tti_tchar *szString)
{
//	tti_int32 index;
	tti_tchar szTmp[100] = {0};

	sprintf(szTmp, "%03d", intValue);
	memcpy(szString, szTmp, strlen(szTmp) - 2);
	szString[strlen(szTmp) - 2] = '.';
	memcpy(szString + strlen(szTmp) - 2 + 1, szTmp + strlen(szTmp) - 2 , 2);
	szString[strlen(szTmp) + 1] = '\0';
}


//------------------------------------------------------------------------------------------
void castByteToBCDString(tti_byte byteValue, tti_tchar *bcdString)
{
	bcdString[0]=byteValue/10+'0';
	bcdString[1]=byteValue%10+'0';
	bcdString[2]='\0';
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetActionCode(tti_byte *request,  tti_byte *tacCode, tti_int32 *tacCodeLen)
{
	if (parseGetUnfixedByteArray((char *)request, tacCode, tacCodeLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	if (((*tacCodeLen)!=TAC_SIZE)&&((*tacCodeLen)!=0))
	{
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetAccountType(char *request, tti_byte *accountType)
{
	tti_tchar tempString[BUFFER_SIZE_32BYTE];
	tti_int32 tempLen;

	if (parseGetFixedString(request, ACCOUNT_TYPE_SIZE, tempString)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	TranslateStringToBCDArray(tempString, accountType, &tempLen);

	return STATUS_OK;	
}




