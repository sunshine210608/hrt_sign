#ifndef _MISCE_H_
#define _MISCE_H_

#include "user_projectconfig.h"
#include <errno.h>
#include "emvDebug.h"
#include "OsTypes.h"

#define SOH 0x01
#define STX 0x02
#define ETX 0x03
#define EOT 0x04
#define ENQ 0x05
#define ACK 0x06
#define NAK 0x15
#define SYN 0x16
#define ETB 0x17

//#define DEBUG 

#ifndef RELEASE
//#define DPRINTF(format, ...) LOGD(format, ## __VA_ARGS__)
#define DPRINTF(format, ...) LOGD(format, ## __VA_ARGS__)
//#define DPRINTF(format, ...) 
#define derror(str)          DPRINTF("%s, %s, errno=%d\n", (str), strerror(errno),  errno)
#define dwatch(x)            DPRINTF("%s = %d(x%08x)\n", #x, (int)(x), (int)(x))
#define dbuf(x)              debug_buf(#x, (char*)(x))
//#define info(x...)		      DPRINTF(0,"INFO:%s %s() L%d:\n\t",__FILE__,__FUNCTION__,__LINE__);DPRINTF(0,x)
#define ReportLine() LOGW("Now File:%s, Fuc: %s, line: %d\n", __FILE__, __func__, __LINE__)
//#define ReportLine() 


#else
//#define DPRINTF(format, ...)
#define DPRINTF(format, ...)
#define derror(str)
#define dwatch(x)
#define dbuf(x)
#define ReportLine() 
//#define info(x...)
#endif



void debug_buf(char *bufname, char *buf);

// "\xff\xff" -> "FFFF" 
int HextoA(char *Dest, const unsigned char *Src, int SrcLen);

// "FFFF" -> "\xff\xff"
int AtoHex(unsigned char *Dest, char *Src, int SrcLen);

///"\x12\x34\x56\x78"->0x12345678 
int Comm_HexToInt_BigEndian(unsigned char *pbySrc, int *piDest);

//0x1234 ->"\x12\x34"
int Comm_ShortToHex_BigEndian(short Src, unsigned char *byDest);

// 0x12345678 ->"\x78\x56\x34\x12"
int Comm_IntToHex_LittleEndian(int iSrc, unsigned char *byDest);

//0x12345678 ->"\x12\x34\x56\x78"
int Comm_IntToHex_BigEndian(int iSrc, unsigned char *byDest);

//"\x78\x56\x34\x12"->0x12345678 
int Comm_HexToInt_LittleEndian(tti_byte *pbySrc, int *piDest);


int memcmp_ex(const void *buffer1, const void *buffer2, unsigned int count);

void CONV_Asc2Hex(unsigned char * pucDest, unsigned int uiDestLen,
				unsigned char * pucSrc, unsigned int uiSrcLen);

void CONV_Hex2Asc(unsigned char * pucDest, unsigned int uiDestLen,
				unsigned char * pucSrc, unsigned int uiSrcLen);



#endif // _MISCE_H_



