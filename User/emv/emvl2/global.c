#include <string.h>
#include "global.h"

TerminalParam terminalParam;

RunningTimeEnv runningTimeEnv;

AID_LIST aidList;

tti_tchar PSEPreferredLang[PREFERRED_LANG_SIZE+1];

tti_byte cid[]={CID_TC, CID_ARQC, CID_AAC, CID_AAR};

//------------------------------------------------------------------------------------------
void initGlobalData(void)
{
	memset(&terminalParam, 0, sizeof(TerminalParam));
}

//------------------------------------------------------------------------------------------
void	initRunningTimeEnv(RunningTimeEnv *runningTimeEnv)
{
	InitTagList(&runningTimeEnv->tagList);
	memset(runningTimeEnv, 0, sizeof(RunningTimeEnv));
}

//------------------------------------------------------------------------------------------
tti_bool IsSupportIssuerAuth(RunningTimeEnv *runningTimeEnv)
{
	tti_byte *pValue;

	pValue=GetTagValue(&runningTimeEnv->tagList, TAG_AIP);

	if ((pValue==NULL)||GetTagValueSize(&runningTimeEnv->tagList, TAG_AIP)!=2)
	{
		return FALSE;
	}

	return (pValue[0]&Bit3);
}

//------------------------------------------------------------------------------------------
void SetTVR(tti_uint16 tvr)
{
	tti_byte *pValue;
	tti_byte tvrIndex, bit;

	tvrIndex=(tti_byte)(tvr>>8);
	bit=(tti_byte)(tvr&0xff);

	if ((tvrIndex>=1)&&(tvrIndex<=TVR_SIZE))
	{
		pValue=GetTagValue(&runningTimeEnv.tagList, TAG_TVR);
		if (pValue==NULL)
		{
			SetTagValue(TAG_TVR, (tti_byte *)"\x00\x00\x00\x00\x00", TVR_SIZE, &runningTimeEnv.tagList);
			pValue=GetTagValue(&runningTimeEnv.tagList, TAG_TVR);
		}
		pValue[tvrIndex-1]|=bit;
	}
}

void ClearTVR(tti_uint16 tvr)
{
	tti_byte *pValue;
	tti_byte tvrIndex, bit;

	tvrIndex=(tti_byte)(tvr>>8);
	bit=(tti_byte)(tvr&0xff);

	if ((tvrIndex>=1)&&(tvrIndex<=TVR_SIZE))
	{
		pValue=GetTagValue(&runningTimeEnv.tagList, TAG_TVR);
		if (pValue==NULL)
		{
			SetTagValue(TAG_TVR, (tti_byte *)"\x00\x00\x00\x00\x00", TVR_SIZE, &runningTimeEnv.tagList);
			pValue=GetTagValue(&runningTimeEnv.tagList, TAG_TVR);
		}
		pValue[tvrIndex-1]&=bit;
	}
}


//------------------------------------------------------------------------------------------
void SetTSI(tti_uint16 tsi)
{
	tti_byte *pValue;
	tti_byte tsiIndex, bit;

	tsiIndex=(tti_byte)(tsi>>8);
	bit=(tti_byte)(tsi&0xff);

	if ((tsiIndex>=1)&&(tsiIndex<=TSI_SIZE))
	{
		pValue=GetTagValue(&runningTimeEnv.tagList, TAG_TSI);
		if (pValue==NULL)
		{
			SetTagValue(TAG_TSI, (tti_byte *)"\x00\x00", TSI_SIZE, &runningTimeEnv.tagList);
			pValue=GetTagValue(&runningTimeEnv.tagList, TAG_TSI);
		}
		pValue[tsiIndex-1]|=bit;
	}
}



//------------------------------------------------------------------------------------------
tti_bool needToDo(tti_byte condition)
{
	tti_byte *pValue;
	pValue=GetTagValue(&runningTimeEnv.tagList, TAG_AIP);
	
	if (pValue!=NULL)
	{
		return (pValue[0]&condition);
	}
	else
	{
		return FALSE;
	}
}

//------------------------------------------------------------------------------------------
tti_bool terminalSupport(tti_uint16 termCapabilityType)
{
	tti_byte *pValue;
	tti_byte tvrIndex, bit;
	tvrIndex=(tti_byte)(termCapabilityType>>8);

	bit=(tti_byte)(termCapabilityType&0x0ff);

	if ((tvrIndex>=1)&&(tvrIndex<=3))
	{
		pValue=GetTagValue(&runningTimeEnv.tagList, TAG_TERMINAL_CAPABILITIES);
		if (pValue==NULL)
		{
			return FALSE;
		}
		else
		{
			return (pValue[tvrIndex-1]&bit);
		}
	}

		return FALSE;
}



//------------------------------------------------------------------------------------------
tti_bool terminalAddOnSupport(tti_uint16 termAddOnCapabilityType)
{
	tti_byte *pValue;
	tti_byte tvrIndex, bit;

	tvrIndex=(tti_byte)(termAddOnCapabilityType>>8);
	bit=(tti_byte)(termAddOnCapabilityType&0x0ff);

	if ((tvrIndex>=1)&&(tvrIndex<=5))
	{
		pValue=GetTagValue(&runningTimeEnv.tagList, TAG_ADDITIONAL_TERM_CAPABILITY);
		if (pValue==NULL)
		{
			return FALSE;
		}
		else
		{
			return (pValue[tvrIndex-1]&bit);

		}
	}
	
	return FALSE;
}

//------------------------------------------------------------------------------------------
tti_bool getAppUsageControl(tti_uint16 AppUsageControl)
{
	tti_byte *pValue;
	tti_byte aucIndex, bit;

	aucIndex=(tti_byte)(AppUsageControl>>8);

	bit=(tti_byte)(AppUsageControl&0xff);
	if ((aucIndex>=1)&&(aucIndex<=AUC_SIZE))
	{
		pValue=GetTagValue(&runningTimeEnv.tagList, TAG_AUC);
		if (pValue!=NULL)
		{
			return (pValue[aucIndex-1]&bit);
		}
	}

	return FALSE;
}

//------------------------------------------------------------------------------------------
void setCVMResult(tti_byte perform, tti_byte condition, tti_byte result)
{
	tti_byte *pValue;

	if (!TagIsExisted(&runningTimeEnv.tagList, TAG_CVM_RESULTS))
	{
		SetTagValue(TAG_CVM_RESULTS, (tti_byte *)"\x3F\x00\x00", CVM_RESULT_SIZE, &runningTimeEnv.tagList);
	}

	pValue=GetTagValue(&runningTimeEnv.tagList, TAG_CVM_RESULTS);
	pValue[0]=perform;
	pValue[1]=condition;
	pValue[2]=result;
}
