#ifndef __EMV_L2_H__
#define __EMV_L2_H__

#include "OsTypes.h"

typedef tti_bool (*Fn_SCR_PowerOn)(tti_int32 hSCR, tti_byte * pATR, tti_int8 * piATR);
typedef tti_bool (*Fn_SCR_PowerOff)(tti_int32 hSCR);
typedef tti_bool (*Fn_SCR_ExchangeApdu)(tti_int32 hSCR, const tti_byte * pApduCmd, const tti_int16 iApduCmd, tti_byte * pApduResp, tti_int16 * piApduResp);

typedef struct _SCR_CallBacks
{
	tti_int32				m_SCR_Handle;
	Fn_SCR_PowerOn 			m_SCR_PowerOn;
	Fn_SCR_PowerOff 		m_SCR_PowerOff;
	Fn_SCR_ExchangeApdu 	m_SCR_ExchangeApdu;
}SCR_CallBacks;

tti_int32 EMVL2_Init_SmartCard(SCR_CallBacks * pScrCallbacks);

void EMVL2_LoadConfiguration(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void EMVL2_GetAidList(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void EMVL2_InitTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void EMVL2_GetBalanceEnquiry(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void EMVL2_Transaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void EMVL2_Complete(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void EMVL2_GetTag(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void EMVL2_EndTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void EMVL2_GetCheckSum(tti_byte *request, int reqLen, tti_byte *response, int *respLen);



#endif
