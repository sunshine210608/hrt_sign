#ifndef _CARD_FUNC_H_
#define _CARD_FUNC_H_

#include "OsTypes.h"


#include "tag.h"



tti_int32 EmvIOInit(void);



tti_int32 SmartCardSelect(tti_byte *aidName, tti_byte aidNameLen, 

	tti_byte *response, tti_uint16 *respLen);



tti_int32 SmartCardGetprocessOption(tti_byte *gpoData, tti_byte gpoDatalen, 

	tti_byte *response, tti_uint16 *respLen);



tti_int32	SmartCardReadRecord(tti_byte SFI, tti_byte index, tti_byte *response, tti_uint16 *respLen);



tti_int32 ParseGPOResponse(tti_byte *response, tti_uint16 respLen, TagList *tagList);



tti_int32 SmartCardGenerateAC(tti_byte cid, tti_byte cdaSign, tti_byte *data, tti_byte dataLen, 

	tti_byte *response, tti_uint16 *respLen);



tti_int32 SmartCardInternalAuth(tti_byte *data, tti_byte dataLen, tti_byte *response, tti_uint16 *respLen);



tti_int32 SmartCardGetData(tti_uint16 tag, tti_byte *response, tti_uint16 *respLen);



tti_int32 SmartCardExternalAuth(tti_byte *data, tti_byte dataLen);



tti_int32 SmartCardSendScript(tti_byte *data, tti_byte dataLen);



 tti_int32 SmartCardGetChallenge(tti_byte *response, tti_uint16 *responseLen);



tti_int32 SmartCardVerifyPin(tti_byte *data, tti_byte dataLen, tti_byte pinType, tti_byte *retryTime);


tti_int32 SmartCardSelectNext(tti_byte *aidName, tti_byte aidNameLen, 
	tti_byte *response, tti_uint16 *respLen);


tti_int32  EmvIOIsCardPresent(void);



tti_int32 EmvIOSessionEnd(void);

tti_int32 EmvContactlessIOSessionEnd(void);


tti_int32 NfcSmartCardSelect(tti_byte *aidName, tti_byte aidNameLen, 
	tti_byte *response, tti_uint16 *respLen);


tti_int32 NfcGetprocessOption(tti_byte *gpoData, tti_byte gpoDatalen, 
	tti_byte *response, tti_uint16 *respLen);

tti_int32 NfcReadRecord(tti_byte SFI, tti_byte index, tti_byte *response, tti_uint16 *respLen);


#endif

