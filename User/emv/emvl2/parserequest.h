#ifndef _PARSE_REQUEST_H_
#define _PARSE_REQUEST_H_

#include "OsTypes.h"
#include "defines.h"

tti_int32 parseLoadConfiguration(char *request, TerminalParam *param);

tti_int32 parseGetAidList(char *request, tti_int32 *timeOut);

tti_int32 parseInitQuickTransaction(char *request, RunningTimeEnv *runningTimeEnv, AID_LIST *aidList);

tti_int32 parseInitTransaction(char *request, RunningTimeEnv *runningTimeEnv, AID_LIST *aidList, 
	TagList *aidParaTagList, Tag3ByteList *aidParaTag3ByteList);

tti_int32 parseGetBalanceEnquiry(char *request, RunningTimeEnv *runningTimeEnv);

tti_int32 parseTransaction(char *request, RunningTimeEnv *runningTimeEnv);

tti_int32 parseCompletion(char *request, RunningTimeEnv *runningTimeEnv);

tti_int32 parseEndTransaction(char *request, RunningTimeEnv *runningTimeEnv);

tti_int32 parseGetCheckSum(char *request, RunningTimeEnv *runningTimeEnv);

tti_int32 parseReadLogTransaction(char *request, RunningTimeEnv *runningTimeEnv, AID_LIST *aidList);

#endif

