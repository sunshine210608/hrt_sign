#ifndef _WORK_CENTER_H_
#define _WORK_CENTER_H_

#include "OsTypes.h"
#include "defines.h"

void processIncLoadConfiguration(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void processIncGetAidList(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void processIncInitTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void processIncGetBalanceEnquiry(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void processIncTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void processIncComplete(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void processIncGetTag(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void processIncEndTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void processIncGetCheckSum(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void processIncReadECBalance(tti_byte *request, int reqLen, tti_byte *response, int *respLen);

void EMVL2_Cmd(tti_byte *InBuff, int InLen, tti_byte *OutBuff, int *OutLen);

tti_int32 EMVL2_BulidTLVForTagList(tti_uint16 tagList[], tti_int32 tagListCount, tti_byte *TLVData, tti_uint16 *TLVDataLen);

#endif

