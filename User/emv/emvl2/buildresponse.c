#include <string.h>
#include <stdio.h>
#include "defines.h"
#include "global.h"
#include "misce.h"
#include "utils.h"
#include "transflow.h"
#include "emvDebug.h"
//#include "sha1.h"
#if 1//def APP_CJT

#include "AppGlobal_cjt.h"
#endif

//------------------------------------------------------------------------------------------
void appendFixMessage(tti_tchar *string, tti_byte fillType, tti_byte fillLength, tti_tchar fillChar,
	tti_tchar *message)
{
	tti_tchar tempBuff[BUFFER_SIZE_256BYTE];
	tti_int32 strLen;

	memset(tempBuff, fillChar, BUFFER_SIZE_256BYTE);
	strLen=strlen(string);
	
	if (fillLength>strLen)
	{
		if (fillType==MESSAGE_RIGHT_JUSTIFY)
		{
			strncpy(tempBuff+fillLength-strLen, string, strLen);
		}
		else
		{
			strncpy(tempBuff, string, strLen);
		}
	}
	else
	{
		if (fillType==MESSAGE_RIGHT_JUSTIFY)
		{
			strncpy(tempBuff, string+strLen-fillLength, fillLength);
		}
		else
		{
			strncpy(tempBuff, string, fillLength);
		}
	}

	tempBuff[fillLength]='\0';

	strcat(message, tempBuff);
}

//------------------------------------------------------------------------------------------
void buildInvalidResponse(tti_tchar *command, tti_byte status, tti_tchar *response)
{
	sprintf(response, "%s.%d", command, status);
}

//------------------------------------------------------------------------------------------
void buildLoadConfigurationResponse(tti_byte status,  char *response)
{
	sprintf((char *)response, "E0.%d", status);
}

//------------------------------------------------------------------------------------------
void buildGetAidListResponse(tti_byte status, AID_LIST *aidList,  char *response)
{
	tti_int32 index;
	tti_tchar tempBuffer[BUFFER_SIZE_1K];
	
	if (status!=0)
	{
		buildInvalidResponse("E1", status, response);
		return;
	}

	strcpy(response, "E1.0");

	for (index=0; index<aidList->count; index++)
	{
		TranslateHexToChars(aidList->aidInfo[index].aidName, 
			aidList->aidInfo[index].aidLength, tempBuffer);
		strcat(response, tempBuffer);
		AppendChar(response, ';');
		strcat(response, aidList->aidInfo[index].aidLabel);
		AppendChar(response, ';');
		if (aidList->aidInfo[index].mustConform)
		{
			AppendChar(response, '1');
		}
		else
		{
			AppendChar(response, '0');
		}
		AppendChar(response, ';');
		strcat(response, aidList->aidInfo[index].aidPreferLabel);
		AppendChar(response, ';');
		DPRINTF("issuerCodeTableIndex[%d] is %s\n", index, aidList->aidInfo[index].issuerCodeTableIndex);
		strcat(response, aidList->aidInfo[index].issuerCodeTableIndex);
		AppendChar(response, ';');
		strcat(response, aidList->aidInfo[index].fciIssuerData);
		
		if (index!=aidList->count-1)
		{
			AppendChar(response, '|');
		}
	}
}

//------------------------------------------------------------------------------------------
void buildInitTransactionResponse(tti_byte status, RunningTimeEnv *runningTimeEnv, char *response)
{
	tti_tchar tempBuffer[BUFFER_SIZE_1K];
//	tti_byte *tagValue;
//	tti_uint16 tagValueSize;

	if (status!=0)
	{
		sprintf(response, "E2.%d", status);
		return;
	}

	sprintf(response, "E2.0%d", runningTimeEnv->commonRunningData.conditions);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 6, '0', response);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 6, '0', response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_AIP), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_AIP), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 4, '0', response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_AUC), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_AUC), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 4, '0', response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_PAN_SEQU_NUMBER), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_PAN_SEQU_NUMBER), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 2, '0', response);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_SERVICE_CODE), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_SERVICE_CODE), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 3, '0', response);

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_PREFERRED_LANGUAGE))
	{
		strncpy(tempBuffer, (char *)GetTagValue(&runningTimeEnv->tagList, TAG_PREFERRED_LANGUAGE),
			GetTagValueSize(&runningTimeEnv->tagList, TAG_PREFERRED_LANGUAGE));
		tempBuffer[GetTagValueSize(&runningTimeEnv->tagList, TAG_PREFERRED_LANGUAGE)]='\0';
		strcat(response, tempBuffer);
	}
	else
	{
		strcat(response, PSEPreferredLang);
	}
		 
	AppendChar(response, '|');

//FIXME: DON'T NEED TO GIVE VERSION NUMBER OUT
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_APP_VERSION_NUMBER), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_APP_VERSION_NUMBER), tempBuffer);
	strcat(response, tempBuffer);
	AppendChar(response, '|');

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_CA_PUBLIC_KEY_INDEX), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_CA_PUBLIC_KEY_INDEX), tempBuffer);
	strcat(response, tempBuffer);
	AppendChar(response, '|');

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_DF_NAME), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_DF_NAME), tempBuffer);
	strcat(response, tempBuffer);
	AppendChar(response, '|');

	translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_PAN), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_PAN), tempBuffer);
	strcat(response, tempBuffer);
	AppendChar(response, '|');

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_TRACK2), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_TRACK2), tempBuffer);
	strcat(response, tempBuffer);
	AppendChar(response, '|');

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_CARDHOLDER_NAME))
	{
		strncat(response, (char *)GetTagValue(&runningTimeEnv->tagList, TAG_CARDHOLDER_NAME),
			GetTagValueSize(&runningTimeEnv->tagList, TAG_CARDHOLDER_NAME));
	}
	AppendChar(response, '|');

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_APP_LABEL))
	{
		strncat(response, (char *)GetTagValue(&runningTimeEnv->tagList, TAG_APP_LABEL),
			GetTagValueSize(&runningTimeEnv->tagList, TAG_APP_LABEL));
	}
}

//------------------------------------------------------------------------------------------
void buildGetBalanceEnquiryResponse(tti_byte status, RunningTimeEnv *runningTimeEnv,  char *response)
{
	tti_tchar tempBuffer[BUFFER_SIZE_1K];

	if (status!=0)
	{
		sprintf(response, "E8.%d", status);
		return;
	}

	if ((runningTimeEnv->status!=CARD_SESSION_SELECT_APP)||
		(runningTimeEnv->status!=CARD_SESSION_TRANSACTION)||
		!TagIsExisted(&runningTimeEnv->tagList, 0x9F50))
	{
		strcpy(response, "E8.1");
	}
	else
	{
		strcpy(response, "E8.0");
		TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, 0x9F50),
			GetTagValueSize(&runningTimeEnv->tagList, 0x9F50), tempBuffer);
		appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 12, '0', response);
	}
}

//------------------------------------------------------------------------------------------
void buildTransactionResponse(tti_byte status, RunningTimeEnv *runningTimeEnv,  char *response)
{
	//tti_tchar tempBuffer[BUFFER_SIZE_256BYTE];
	
	tti_tchar tempBuffer[BUFFER_SIZE_1K];

	if (status!=0)
	{
		sprintf(response, "E4.%d", status);
		return;
	}

	sprintf(response, "E4.%d%d", status, runningTimeEnv->commonRunningData.emvResultCode);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 16, '0', response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 2, '0', response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_ATC), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ATC), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 4, '0', response);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_TVR), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_TVR), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 10, '0', response);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_TSI), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_TSI), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 4, '0', response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_UNPREDICTABLE_NUMBER), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_UNPREDICTABLE_NUMBER), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 8, '0', response);

	castByteToBCDString((tti_byte)runningTimeEnv->commonRunningData.randomValueForTransSelect,
		tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 2, '0', response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_CVM_RESULTS), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_CVM_RESULTS), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 6, '0', response);

	switch (runningTimeEnv->commonRunningData.IACCodeType)
	{
		case 1:
			TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DENIAL), 
				GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_DENIAL), tempBuffer);
			break;
		case 2:
			TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_IAC_ONLINE), 
				GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_ONLINE), tempBuffer);
			break;
		case 0:
		default:
			TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DEFAULT), 
				GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_DEFAULT), tempBuffer);
			break;
	}
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 10, '0', response);

	sprintf(tempBuffer, "%d%d", runningTimeEnv->commonRunningData.IACCodeType,
		runningTimeEnv->commonRunningData.signatureRequired);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 2, '0', response);

	//Jason added on 2012.04.24 for EC_ISSUER_AUTH_CODER 
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_EC_ISSUER_AUTH_CODER) == TRUE)
	{		
		TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_EC_ISSUER_AUTH_CODER), 
				GetTagValueSize(&runningTimeEnv->tagList, TAG_EC_ISSUER_AUTH_CODER), tempBuffer);
		appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 12, '0', response);
	}else
	{
		memset(tempBuffer, '0', 12);
		tempBuffer[12] = '\x0';
		strcat(response, tempBuffer);	
	}	
	//End
	

	//Jason added on 2012.04.24 for ISEC 
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_EC_BALANCE_FROM_GEN_AC) == TRUE)
	{		
		strcat(response, "1");
	}else
	{
		strcat(response, "0");	
	}	
	//End

	
	//Jason added on 2014.01.13 for  TAG_RESPONSE_CODE
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_RESPONSE_CODE) == TRUE)
	{		
		memcpy(tempBuffer, GetTagValue(&runningTimeEnv->tagList, TAG_RESPONSE_CODE), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_RESPONSE_CODE));
		tempBuffer[GetTagValueSize(&runningTimeEnv->tagList, TAG_RESPONSE_CODE)] = '\0';
		appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, strlen(tempBuffer), '0', response);	
	}else
	{
		//Not set TAG_RESPONSE_CODE yet
		strcat(response, "  ");	
	}	
	//End
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA), tempBuffer);
	strcat(response, tempBuffer);
	AppendChar(response, '|');
	strcat(response, runningTimeEnv->commonRunningData.pinBlock);

	}

//------------------------------------------------------------------------------------------
void buildCompletionResponse(tti_byte status, RunningTimeEnv *runningTimeEnv,  char *response)
{
	tti_tchar tempBuffer[BUFFER_SIZE_4K];
	tti_int32 index;

	if (status!=0)
	{
		sprintf(response, "E5.%d", status);
        DPRINTF("3: response is : %s\n", response);
		return;
	}

	sprintf(response, "E5.%d", status);

	/*
	appendFixMessage(GetTagValue(&runningTimeEnv->tagList, TAG_RESPONSE_CODE), 
		MESSAGE_RIGHT_JUSTIFY, 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_RESPONSE_CODE), '0', response);
	*/
	memcpy(tempBuffer, GetTagValue(&runningTimeEnv->tagList, TAG_RESPONSE_CODE), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_RESPONSE_CODE));
	tempBuffer[GetTagValueSize(&runningTimeEnv->tagList, TAG_RESPONSE_CODE)] = '\0';
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, strlen(tempBuffer), '0', response);	
	DPRINTF("1: response is : %s\n", response);
	
	sprintf(tempBuffer, "%d", runningTimeEnv->commonRunningData.emvResultCode);
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 1, '0', response);	
	DPRINTF("2: response is : %s\n", response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 16, '0', response);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 2, '0', response);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_ATC), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ATC), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 4, '0', response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_TVR), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_TVR), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 10, '0', response);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_TSI), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_TSI), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 4, '0', response);

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_UNPREDICTABLE_NUMBER), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_UNPREDICTABLE_NUMBER), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 8, '0', response);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_CVM_RESULTS), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_CVM_RESULTS), tempBuffer);
	appendFixMessage(tempBuffer, MESSAGE_RIGHT_JUSTIFY, 6, '0', response);

	switch (runningTimeEnv->commonRunningData.IACCodeType)
	{
		case 1:
			TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DENIAL), 
				GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_DENIAL), tempBuffer);
			break;
		case 2:
			TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_IAC_ONLINE), 
				GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_ONLINE), tempBuffer);
			break;
		case 0:
		default:
			TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_IAC_DEFAULT), 
				GetTagValueSize(&runningTimeEnv->tagList, TAG_IAC_DEFAULT), tempBuffer);
			break;
	}
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 10, '0', response);

	sprintf(tempBuffer, "%d", runningTimeEnv->commonRunningData.IACCodeType);
	appendFixMessage(tempBuffer, MESSAGE_LEFT_JUSTIFY, 1, '0', response);

	//Jason added on 2012.04.24 for ISEC
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_EC_BALANCE_FROM_GEN_AC) == TRUE)
	{		
		strcat(response, "1");
	}else
	{
		strcat(response, "0");
	}
	//End

	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA), tempBuffer);
	strcat(response, tempBuffer);

	for (index=0; index<runningTimeEnv->scriptsList.scriptsNumber; index++)
	{
		if (runningTimeEnv->scriptsList.scripts[index].scriptValue[0]==TAG_TEMPERATE_71)
		{
			AppendChar(response, SEPERATOR);
			DPRINTF("TAG_TEMPERATE_71, scriptResult list");
			hexdump(runningTimeEnv->scriptsList.scriptResults[index].scriptResult, 
				runningTimeEnv->scriptsList.scriptResults[index].scriptResultLen);
			TranslateHexToChars(runningTimeEnv->scriptsList.scriptResults[index].scriptResult, 
				runningTimeEnv->scriptsList.scriptResults[index].scriptResultLen, tempBuffer);
			strcat(response, tempBuffer);
		}
	}
	for (index=0; index<runningTimeEnv->scriptsList.scriptsNumber; index++)
	{
		if (runningTimeEnv->scriptsList.scripts[index].scriptValue[0]==TAG_TEMPERATE_72)
		{
			AppendChar(response, SEPERATOR);
			DPRINTF("TAG_TEMPERATE_72, scriptResult list");
			hexdump(runningTimeEnv->scriptsList.scriptResults[index].scriptResult, 
				runningTimeEnv->scriptsList.scriptResults[index].scriptResultLen);
			TranslateHexToChars(runningTimeEnv->scriptsList.scriptResults[index].scriptResult, 
				runningTimeEnv->scriptsList.scriptResults[index].scriptResultLen, tempBuffer);
			strcat(response, tempBuffer);
		}
	}
	DPRINTF("3: response is : %s\n", response);
}

//------------------------------------------------------------------------------------------
void buildGetTagResponse(tti_byte status, tti_uint16 tag, 
	RunningTimeEnv *runningTimeEnv,  char *response)
{
	if (status!=0)
	{
		sprintf(response, "E7.%d", status);
	}
	else
	{
		//DPRINTF("runningTimeEnv->status = %d\n", runningTimeEnv->status);
		if (TagIsExisted(&runningTimeEnv->tagList, tag) == TRUE)
		{
			DPRINTF("TAG [%04x] is exist\n", tag);
		}else
		{
			DPRINTF("TAG [%04x] is not exist\n", tag);
		}
		if (((runningTimeEnv->status!=CARD_SESSION_SELECT_APP)&&
			(runningTimeEnv->status!=CARD_SESSION_TRANSACTION))||
			!TagIsExisted(&runningTimeEnv->tagList, tag))
		{
			strcpy(response, "E7.1");
		}
		else
		{
			strcpy(response, "E7.0");
			TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, tag), 
				GetTagValueSize(&runningTimeEnv->tagList, tag), response+4);
		}
	}
}

//------------------------------------------------------------------------------------------
void buildEndTransactionResponse(tti_byte status, char *response)
{
	sprintf(response, "ED.%d", status);
}

//------------------------------------------------------------------------------------------
void buildGetCheckSumResponse(tti_byte status, char *response)
{
	tti_byte checkSum[20];
	tti_tchar checkSumString[40+1];
	
	sprintf(response, "EV.%d", status);
	
	//sha1_csum(VERSION_NUMBER, strlen(VERSION_NUMBER), checkSum);
	GetVersionCheckSum((tti_tchar *)checkSum);
	TranslateHexToChars(checkSum, 20, checkSumString);
	checkSumString[16]='\0';
	
	strcat(response, checkSumString);
}
