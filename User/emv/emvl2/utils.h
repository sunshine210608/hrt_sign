#ifndef _UTILS_H_
#define _UTILS_H_

#include "OsTypes.h"
#include "defines.h"

//------------------------------------------------------------------------------------------
tti_bool isEven(tti_int32 value);


//------------------------------------------------------------------------------------------
tti_bool isDigit(char *checkedDigitString);


//------------------------------------------------------------------------------------------
unsigned char CalcLRC(tti_byte  *message, tti_int32 msgLen);


//------------------------------------------------------------------------------------------
void AddEvenParity (char* szMessage, int iStrLen);



//------------------------------------------------------------------------------------------

void FormatString(char* szString, int iFillLength, char cFillChar, int iAlign);



//------------------------------------------------------------------------------------------

void RestoreSmartCardInfo(tti_byte *fromBuffer, tti_int32 fromBufferLen, 

	tti_byte *toBuffer, tti_int32 *toBufferLen);



//------------------------------------------------------------------------------------------

void GetResponseFromBuffer(tti_byte *buffer, tti_int32 bufferLen, tti_byte *msg, tti_int32 *msgLen);



//------------------------------------------------------------------------------------------

tti_int32 GetCharPositionInBuffer(tti_tchar* szString, tti_int32 uiStrLen, tti_tchar ch);



//------------------------------------------------------------------------------------------

tti_tchar TranslateDigitToHexChar(tti_int8 iTranslatedDigit);



//------------------------------------------------------------------------------------------

void GetPrimaryAccount(tti_tchar *szInString, tti_tchar *szPrimaryAccount);



//------------------------------------------------------------------------------------------

void printByteArray(tti_byte *buffer, tti_int32 bufferSize);



//------------------------------------------------------------------------------------------

void	printCharArray(tti_byte *buffer, tti_int32 bufferSize);



//------------------------------------------------------------------------------------------
void TranslateHexToChars(tti_byte *hexArray, tti_int32 hexArraySize, tti_tchar *tempBuff);



//------------------------------------------------------------------------------------------
tti_int32 GetUnFixMessageLength(tti_byte *sourceMsg, tti_int32 sourceMsgLen, 
	tti_byte seperateByte);



//------------------------------------------------------------------------------------------
tti_int32 TranslateStringToByteArray(tti_tchar *string, tti_byte *byteArray, tti_int32 *byteArrayLen);



//--------------------------------------------------------------------------------------------
tti_int32 TranslateStringToBCDArray_FillLeftZero(tti_tchar *string, tti_byte *byteArray, tti_int32 *byteArrayLen);



//------------------------------------------------------------------------------------------
tti_int32 TranslateStringToBCDArray(tti_tchar *string, tti_byte *byteArray, tti_int32 *byteArrayLen);



//--------------------------------------------------------------------------------------------
char* AppendChar(char* szString, char ch);



//--------------------------------------------------------------------------------------------
tti_int32 parseGetDate(char *request, tti_byte *date, tti_int32 *dateLen);



//--------------------------------------------------------------------------------------------

tti_int32 parseGetLongDate(char *request, char *longDate);



//------------------------------------------------------------------------------------------
tti_int32 parseGetTime(char *request, tti_byte *time, tti_int32 *timeLen);



//------------------------------------------------------------------------------------------
tti_int32 parseGetFixedString(char *request, tti_int32 fixedStringSize, char *fixedString);



//------------------------------------------------------------------------------------------
tti_int32 parseGetByteArray(char *request, tti_int32 byteArrayLen, tti_byte *byteArray);



//------------------------------------------------------------------------------------------
tti_int32 parseGetInt32(char *request, int length, tti_int32 *int32Data);



//------------------------------------------------------------------------------------------
tti_int32 parseGetInt16(char *request, int length, tti_int16 *int16Data);



//------------------------------------------------------------------------------------------
tti_int32 parseGetInt8(char *request, int length, tti_int8 *int8Data);



//------------------------------------------------------------------------------------------
tti_int32 parseGetBool(char *request, tti_bool *boolData);



//------------------------------------------------------------------------------------------
tti_int32 parseGetAmount(char *request, tti_int32 *amount);



//------------------------------------------------------------------------------------------
tti_int32 parseGetCandidateAid(char *request, CANDIDATE_INFO *candidateAidInfo);



//------------------------------------------------------------------------------------------
tti_int32 parseGetUnfixedByteArray(char *request,  tti_byte *byteArray, tti_int32 *byteArrayLen);



//------------------------------------------------------------------------------------------
tti_int32 getDataByTag(TagList *tagList, tti_uint16 tag);



//------------------------------------------------------------------------------------------
tti_int32 translateByteArrayToInt(tti_byte *byteArray, tti_int32 byteArrayLen);



//------------------------------------------------------------------------------------------

tti_int32 getAmountByTag(TagList *tagList, tti_uint16 tag);



//------------------------------------------------------------------------------------------

void formatPlainPin(tti_tchar *pinNumber, tti_byte *formatedPinNumber);



//------------------------------------------------------------------------------------------

void printCertificate(Certificate *certificate);



//------------------------------------------------------------------------------------------

tti_bool isExpiredDate(tti_byte *shortDate_MMYY, tti_byte *currentDate_YYMMDD);



//------------------------------------------------------------------------------------------

void translateBcdArrayToString(tti_byte *bcdArray, tti_int32 bcdArrayLen, tti_tchar *string);


//------------------------------------------------------------------------------------------
tti_int32 TranslateStringToBCDArray_FillLeftZero_WithLen(tti_tchar *string, tti_byte *byteArray, 
	tti_int32 *byteArrayLen, tti_int32 stringTotalLen);


//------------------------------------------------------------------------------------------

void castInt32ToByteArray(tti_int32 intValue, tti_byte *byteArray);



//------------------------------------------------------------------------------------------

void castAmountToBCDArray(tti_int32 intValue, tti_byte *bcdArray);



//------------------------------------------------------------------------------------------

void castByteToBCDString(tti_byte byteValue, tti_tchar *bcdString);



//------------------------------------------------------------------------------------------

tti_int32 parseGetActionCode(tti_byte *request,  tti_byte *tacCode, tti_int32 *tacCodeLen);



//------------------------------------------------------------------------------------------

tti_int32 parseGetAccountType(char *request, tti_byte *accountType);

tti_int32 checkDateFormat(char *checkedDate);

tti_int32 parseGetTag(char *request, tti_uint16 *tag);

tti_int32 rand_hardware_byte(tti_byte *randhex);

tti_int32 formatAmountByTag(TagList *tagList, tti_uint16 tag, tti_tchar *szFormatAmount);

void foramtAmount(char *szSrc, char *szDest);


#endif

