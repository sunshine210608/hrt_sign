#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "mhscpu.h"
#include "SysTick.h"
//#include <asm/fcntl.h>
#include "tag.h"
#include "transflow.h"
#include "utils.h"
#include "defines.h"
#include "cardfunc.h"
#include "dataauth.h"
#include "riskmanage.h"
#include "ui.h"
#include "global.h"
#include "misce.h"
#include "debug.h"
#include "sha1.h"
#include "../EmvAppUi.h"
#include "emvDebug.h"
#include "des_sec.h"
#include "utils.h"
//#include "../Tag.h"
#include "util.h"
#include "online.h"
#include "Receipt.h"
#include "ReqDataSet.h"
#include "transaction.h"
#include "paramDown.h"
#include "EmvAppUI.h"
#include "user_projectconfig.h"
#include "gprs.h"
#include "gprs_at.h"

#ifdef VPOS_APP
#include "vposface.h"
#include "pub.h"
#if 0
#ifdef APP_LKL
#include "AppGlobal_lkl.h"
#endif
#ifdef APP_SDJ
#include "AppGlobal_sdj.h"
#endif
#endif
#if 1//def APP_CJT

#include "AppGlobal_cjt.h"
#endif


#endif

#define	RESULT_CODE_EXACT_MATCH			1
#define	RESULT_CODE_NOT_EXACH_MATCH		2
#define	RESULT_CODE_NOT_MATCH			3

extern tick get_diff_tick(tick cur_tick, tick prior_tick);
extern uint32 GetNfcStartTick(void);

tti_int32 qpbocParseCryptInfoData(tti_byte *cryptInfoData);

//------------------------------------------------------------------------------------------
void PrintOutCandidateAidList(CANDIDATE_LIST *candidateAidList)
{
	tti_int32 index;

	DPRINTF("candidate aid count=%ld\n\n", candidateAidList->count);
	
	for (index=0; index<candidateAidList->count; index++)
	{
		printByteArray(candidateAidList->candidateAidInfo[index].aidName,
			candidateAidList->candidateAidInfo[index].aidLength);
		DPRINTF("candidate aid version=\n");
		printByteArray(candidateAidList->candidateAidInfo[index].verionNumber, APP_VERSION_NUMBER_SIZE);
	}
}

//------------------------------------------------------------------------------------------
void PrintOutAidList(AID_LIST *aidList)
{
	tti_int32 index;

	DPRINTF("aid count=%ld\n\n", aidList->count);
	
	for (index=0; index<aidList->count; index++)
	{
		DPRINTF("aid label=%s\n", aidList->aidInfo[index].aidLabel);
		DPRINTF("aid name length=%ld\n", aidList->aidInfo[index].aidLength);
		printByteArray(aidList->aidInfo[index].aidName, aidList->aidInfo[index].aidLength);
		//DPRINTF("aid version=\n");
		//printByteArray(aidList->aidInfo[index].verionNumber, APP_VERSION_NUMBER_SIZE);
		DPRINTF("aid priority=%02x\n", aidList->aidInfo[index].priority);
	}
}

//------------------------------------------------------------------------------------------
void RemoveAidFromList(tti_int32 aidIndex, AID_LIST *aidList)
{
	tti_int32 index;

	if (aidIndex<aidList->count)
	{
		for (index=aidIndex; index<aidList->count-1; index++)
		{
			memcpy(&aidList->aidInfo[index], &aidList->aidInfo[index+1], sizeof(AID_INFO));
		}
		aidList->count--;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 prepareTerminalSideData(TerminalParam *terminalParam, TagList *tagList)
{
	//tti_byte tempByteArray[BUFFER_SIZE_256BYTE];
	tti_byte tempByteArray[BUFFER_SIZE_32BYTE];
	tti_int32 tempByteArrayLen;

	DPRINTF("prepare terminal side data\n");

	SetTagValue(TAG_IFD_SERIAL_NUMBER, (tti_byte *)(terminalParam->IFDSerialNumber),
		IFD_SERIAL_NUMBER_SIZE, tagList);
	
	TranslateStringToBCDArray_FillLeftZero(terminalParam->terminalCountryCode, 
		tempByteArray, &tempByteArrayLen);
	SetTagValue(TAG_TERM_COUNTRY_CODE, tempByteArray, tempByteArrayLen, tagList);

	SetTagValue(TAG_TERMINAL_CAPABILITIES, terminalParam->terminalCapabilities, 
		TERMINAL_CAPABILITIES_SIZE, tagList);

	SetTagValue(TAG_ADDITIONAL_TERM_CAPABILITY, terminalParam->addtionalTerminalCapabilities, 
		ADDTIONAL_TERMINAL_CAPABILITIES_SIZE, tagList);

	SetTagValue(TAG_TERMINAL_TYPE, &terminalParam->terminalType, 1, tagList);

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 getSpecialDataFromICC(RunningTimeEnv *runningTimeEnv, tti_uint16 tag)
{
	tti_byte tempData[BUFFER_SIZE_256BYTE];
	tti_uint16 tempDataLen;
	tti_int32 status;

	DPRINTF("get data\n");
	
	status=SmartCardGetData(tag, tempData, &tempDataLen);
	printByteArray(tempData, tempDataLen);
	if (status==STATUS_OK)
	{
		return BuildTagList(tempData, tempDataLen, &runningTimeEnv->tagList);
	}
	else
	{
		return status;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 getSpecialDataFromNFC(RunningTimeEnv *runningTimeEnv, tti_uint16 tag)
{
	tti_byte tempData[BUFFER_SIZE_256BYTE];
	tti_uint16 tempDataLen;
	tti_int32 status;

	DPRINTF("get data\n");
	
	status=NfcCardGetData(tag, tempData, &tempDataLen);
	printByteArray(tempData, tempDataLen);
	if (status==STATUS_OK)
	{
		return BuildTagList(tempData, tempDataLen, &runningTimeEnv->tagList);
	}
	else
	{
		return status;
	}
}

#ifdef VPOS_APP
int sg_iDispResultInfo=1;
void vSetDispRetInfo(int flag)
{
    sg_iDispResultInfo=flag;
}
int iGetDispRetInfo(void)
{
    return sg_iDispResultInfo;
}
#endif

//------------------------------------------------------------------------------------------
tti_int32 prepareRunningEnv(RunningTimeEnv *runningTimeEnv)
{
	tti_int32 unPredicatableNumber = 0;
	tti_byte tempByteArray[BUFFER_SIZE_32BYTE];

	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_TVR))
	{
		SetTagValue(TAG_TVR, "\x00\x00\x00\x00\x00", TVR_SIZE, &runningTimeEnv->tagList);
	}
	
	setCVMResult(CVM_NO_USED, CVM_ALWAYS, CVM_RESULT_UNKNOWN);

	unPredicatableNumber = rand_hardware_byte(tempByteArray);
	DPRINTF("unPredicatableNumber = %04x", unPredicatableNumber);
	DPRINTF("List unPredicatableNumber data");
	hexdump(tempByteArray, 4);
	SetTagValue(TAG_UNPREDICTABLE_NUMBER, tempByteArray, 4, &runningTimeEnv->tagList);
	
	return 0;
}

//------------------------------------------------------------------------------------------
tti_bool cvmFormatIsOK(RunningTimeEnv *runningTimeEnv)
{
	tti_int32 cvmLen;

	if (!TagDataIsMissing(&runningTimeEnv->tagList, TAG_CVM))
	{
		cvmLen=GetTagValueSize(&runningTimeEnv->tagList, TAG_CVM);
		if ((cvmLen<CVM_AMOUNT_X_SIZE+CVM_AMOUNT_Y_SIZE)||(!isEven(cvmLen)))
		{
			return FALSE;
		}
	}

	return TRUE;
}

//------------------------------------------------------------------------------------------
tti_int32 checkQpbocMandatoryData(RunningTimeEnv *runningTimeEnv)
{
//	tti_tchar tempString[BUFFER_SIZE_512BYTE];
	
	DPRINTF("check qpboc mandatory data\n");

/*
	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE))
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE),
		GetTagValueSize(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE), tempString);

	if (checkDateFormat(tempString)!=STATUS_OK)
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE))
	{
		translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE),
			GetTagValueSize(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE), tempString);

		if (checkDateFormat(tempString)!=STATUS_OK)
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;
		}
	}
*/

	return STATUS_OK;
}


//------------------------------------------------------------------------------------------
tti_int32 checkMandatoryData(RunningTimeEnv *runningTimeEnv)
{
	tti_tchar tempString[BUFFER_SIZE_512BYTE];
	
	DPRINTF("check mandatory data\n");
	
	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE)||
		TagDataIsMissing(&runningTimeEnv->tagList, TAG_PAN)||
		TagDataIsMissing(&runningTimeEnv->tagList, TAG_CDOL1)||
		TagDataIsMissing(&runningTimeEnv->tagList, TAG_CDOL2))
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE),
		GetTagValueSize(&runningTimeEnv->tagList, TAG_APP_EXPIRATION_DATE), tempString);

	if (checkDateFormat(tempString)!=STATUS_OK)
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE))
	{
		translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE),
			GetTagValueSize(&runningTimeEnv->tagList, TAG_APP_EFFECTIVE_DATE), tempString);

		if (checkDateFormat(tempString)!=STATUS_OK)
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;
		}
	}

	if (!cvmFormatIsOK(runningTimeEnv))
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_bool aidIsMatch(CANDIDATE_INFO *candidateAid, AID_INFO *aid)
{
	if (candidateAid->exactMatch)
	{
		return ((candidateAid->aidLength==aid->aidLength)
			&&(memcmp(candidateAid->aidName, aid->aidName, candidateAid->aidLength)==0));
	}
	else
	{
		if (candidateAid->aidLength<=aid->aidLength)
		{
			return (memcmp(candidateAid->aidName, aid->aidName, candidateAid->aidLength)==0);
		}
		else
		{
			return FALSE;
		}
	}
}

//------------------------------------------------------------------------------------------
tti_int32 tryAddAidItemByADF(CANDIDATE_LIST *candidateAidList, TagList *tagList, AID_INFO *aid)
{
	tti_uint16 labelLen;
	tti_byte *pValue;
	tti_int32 index;

	DPRINTF("add aid item by adf\n");

	aid->aidLength=GetTagValueSize(tagList, TAG_ADF_NAME);
	memcpy(aid->aidName, GetTagValue(tagList, TAG_ADF_NAME), aid->aidLength);

	if (TagIsExisted(tagList, TAG_PREFERRED_NAME))
	{
		DPRINTF("TAG_PREFERRED_NAME exist\n");
		/*
		labelLen=GetTagValueSize(tagList, TAG_PREFERRED_NAME);
		memcpy(aid->aidLabel, GetTagValue(tagList, TAG_PREFERRED_NAME), labelLen);
		aid->aidLabel[labelLen]='\0';
		*/
		labelLen=GetTagValueSize(tagList, TAG_PREFERRED_NAME);
		memcpy(aid->aidPreferLabel, GetTagValue(tagList, TAG_PREFERRED_NAME), labelLen);
		aid->aidPreferLabel[labelLen]='\0';
		DPRINTF("aid->aidPreferLabel = %s\n", aid->aidPreferLabel);
	}
	else
	{
		strcpy(aid->aidPreferLabel, "");
	}
	
	if (TagIsExisted(tagList, TAG_APP_LABEL))
	{
		labelLen=GetTagValueSize(tagList, TAG_APP_LABEL);
		memcpy(aid->aidLabel, GetTagValue(tagList, TAG_APP_LABEL), labelLen);
		aid->aidLabel[labelLen]='\0';
	}
	else
	{
		strcpy(aid->aidLabel, "");
	}

	if (TagIsExisted(tagList, TAG_ISSUER_CODE_INDEX))
	{
		DPRINTF("exist TAG_ISSUER_CODE_INDEX\n");
		pValue=GetTagValue(tagList, TAG_ISSUER_CODE_INDEX);
		TranslateHexToChars(pValue, 1, aid->issuerCodeTableIndex);
		DPRINTF("aid->issuerCodeTableIndex = %s\n", aid->issuerCodeTableIndex);
	}
	else
	{
		strcpy(aid->issuerCodeTableIndex, "");
	}

	if (TagIsExisted(tagList, TAG_FCI_ISSUER_DATA))
	{
		pValue=GetTagValue(tagList, TAG_FCI_ISSUER_DATA);
		TranslateHexToChars(pValue, GetTagValueSize(tagList, TAG_FCI_ISSUER_DATA),
			aid->fciIssuerData);
	}
	else
	{
		strcpy(aid->fciIssuerData, "");
	}
	
	if (TagIsExisted(tagList, TAG_APP_PRIOR))
	{
		pValue=GetTagValue(tagList, TAG_APP_PRIOR);
		if ((*(pValue)&Bit8)!=0)
		{
			DPRINTF("aid->mustConform=TRUE\n");
			aid->mustConform=TRUE;
		}
		DPRINTF("aid->mustConform=%d\n", aid->mustConform);
		aid->priority=(*pValue)&0x0F;
	}
	else
	{
		DPRINTF("aid->mustConform=FALSE\n");
		aid->mustConform=FALSE;
		aid->priority=0x0F;
	}

	for (index=0; index<candidateAidList->count; index++)
	{
		if (aidIsMatch(&candidateAidList->candidateAidInfo[index], aid))
		{
			memcpy(aid->verionNumber, candidateAidList->candidateAidInfo[index].verionNumber,
				APP_VERSION_NUMBER_SIZE);
			DPRINTF("add aid successfully\n");
			return STATUS_OK;
		}
	}

	return STATUS_FAIL;
}

//------------------------------------------------------------------------------------------
tti_int32 checkFCIProprietaryTemplateInDDF(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
//	tti_int32 status;
	tti_byte *SFI;

	DPRINTF("check fci proprietary template in ddf\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&tempTagList, TAG_SFI))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	SFI=GetTagValue(&tempTagList, TAG_SFI);
	if (((*SFI)<1)||((*SFI)>10))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	FreeTagList(&tempTagList);

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 checkFCITemplateInDDF(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 status;

	DPRINTF("check fci template in ddf\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&tempTagList, TAG_DF_NAME)||
		!TagIsExisted(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}


	status=checkFCIProprietaryTemplateInDDF(GetTagValue(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE),
		GetTagValueSize(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE));

	FreeTagList(&tempTagList);

	return status;
}

//------------------------------------------------------------------------------------------
tti_int32 checkDDFFormat(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 status;

	DPRINTF("chekc ddf format\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&tempTagList, TAG_FCI_TEMPLATE))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	status=checkFCITemplateInDDF(GetTagValue(&tempTagList, TAG_FCI_TEMPLATE),
		GetTagValueSize(&tempTagList, TAG_FCI_TEMPLATE));

	FreeTagList(&tempTagList);

	return status;
}

//------------------------------------------------------------------------------------------
tti_int32 checkFCIIssuerData(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 index;

	DPRINTF("check FCI issuer data\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	for (index=0; index<tempTagList.duplicateItemCount; index++)
	{
		switch (tempTagList.duplicateItem[index])
		{
			case TAG_LOG_ENTRY:
				FreeTagList(&tempTagList);
				return STATUS_FAIL;

			default:
				break;
		}
	}

	FreeTagList(&tempTagList);

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 checkFCIProprietaryTemplateInADF(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 index, status;

	DPRINTF("check fci proprietary template in adf\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	//Jason added 2012.04.18 for EMV4.3 : "tag 0x50 is must" , start 

	//End
	if (!TagDataIsMissing(&tempTagList, TAG_APP_PRIOR))
	{
		if (GetTagValueSize(&tempTagList, TAG_APP_PRIOR)!=1)
		{
			FreeTagList(&tempTagList);
			return STATUS_FAIL;
		}
	}

	for (index=0; index<tempTagList.duplicateItemCount; index++)
	{
		switch (tempTagList.duplicateItem[index])
		{
			case TAG_APP_PRIOR:
			case TAG_PDOL:
				FreeTagList(&tempTagList);
				return STATUS_FAIL;

			default:
				break;
		}
	}

	if (TagIsExisted(&tempTagList, TAG_FCI_ISSUER_DATA))
	{
		status=checkFCIIssuerData(GetTagValue(&tempTagList, TAG_FCI_ISSUER_DATA),
			GetTagValueSize(&tempTagList, TAG_FCI_ISSUER_DATA));
	}
	else
	{
		status=STATUS_OK;
	}

	FreeTagList(&tempTagList);

	return status;
}

//------------------------------------------------------------------------------------------
tti_int32 checkFCITemplateInADF(tti_byte *message, tti_int32 msgLen)
{
    //bob add 20101106 from here
    TagList tempFullTagList;
	//Bob add 20101106 end

	
	TagList tempTagList;
	tti_int32 index, status;

	DPRINTF("check fci template in adf\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (TagDataIsMissing(&tempTagList, TAG_DF_NAME)||
		!TagIsExisted(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagDataIsMissing(&tempTagList, TAG_PDOL))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	for (index=0; index<tempTagList.duplicateItemCount; index++)
	{
		switch (tempTagList.duplicateItem[index])
		{
			case TAG_DF_NAME:
			case TAG_FCI_PROPRIETARY_TEMPLATE:
				FreeTagList(&tempTagList);
				return STATUS_FAIL;
			default:
				break;
		}
	}

	//bob add from here 20101106
	InitTagList(&tempFullTagList);
	if (STATUS_OK == BuildTagList(message, msgLen, &tempFullTagList))
	{
		if( tempFullTagList.duplicateItemCount > 0)
		{
			FreeTagList(&tempFullTagList);
			return STATUS_FAIL;
		}
	}
	FreeTagList(&tempFullTagList);
	//bob add end

	status=checkFCIProprietaryTemplateInADF(GetTagValue(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE),
		GetTagValueSize(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE));

	FreeTagList(&tempTagList);

	return status;
}

//------------------------------------------------------------------------------------------
tti_int32 checkADFFormat(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 status;

	DPRINTF("chekc adf format\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&tempTagList, TAG_FCI_TEMPLATE))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	status=checkFCITemplateInADF(GetTagValue(&tempTagList, TAG_FCI_TEMPLATE),
		GetTagValueSize(&tempTagList, TAG_FCI_TEMPLATE));

	FreeTagList(&tempTagList);

	return status;
}

//------------------------------------------------------------------------------------------
tti_int32 checkADFEntryFormat(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 index, tagLen;

	DPRINTF("chekc adf entry format\n");
	
	InitTagList(&tempTagList);

	printByteArray(message, msgLen);
	
	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&tempTagList, TAG_ADF_NAME)||!TagIsExisted(&tempTagList, TAG_APP_LABEL))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	tagLen=GetTagValueSize(&tempTagList, TAG_ADF_NAME);
	if ((tagLen<5)||(tagLen>16))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagDataIsMissing(&tempTagList, TAG_APP_PRIOR))
	{
		if (GetTagValueSize(&tempTagList, TAG_APP_PRIOR)!=1)
		{
			FreeTagList(&tempTagList);
			return STATUS_FAIL;
		}
	}

	for (index=0; index<tempTagList.duplicateItemCount; index++)
	{
		switch (tempTagList.duplicateItem[index])
		{
			case TAG_ADF_NAME:
			case TAG_APP_LABEL:
			case TAG_APP_PRIOR:
				//Jason added on 2017.03.23
				FreeTagList(&tempTagList);
				return STATUS_FAIL;

			default:
				break;
		}
	}

	FreeTagList(&tempTagList);

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 checkDDFEntryFormat(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 index, len;

	DPRINTF("chekc ddf entry format\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}
	if (!TagIsExisted(&tempTagList, TAG_DDF_NAME))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	len=GetTagValueSize(&tempTagList, TAG_DDF_NAME);
	if ((len<5)||(len>16))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	for (index=0; index<tempTagList.duplicateItemCount; index++)
	{
		switch (tempTagList.duplicateItem[index])
		{
			case TAG_DDF_NAME:
				FreeTagList(&tempTagList);
				return STATUS_FAIL;
				
			default:
				break;
		}
	}

	FreeTagList(&tempTagList);

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 getAidFromDDF(CANDIDATE_LIST *candidateAidList, tti_byte *ddfName, 
tti_byte ddfNameLen, AID_LIST *aidList, tti_tchar *PSEPreferredLang)
{
	tti_byte tempResult[BUFFER_SIZE_512BYTE];
	tti_uint16 tempResultLen;
	tti_int32 status;
	TagList ddfTagList, recordTagList, entryTagList;
	tti_int32 index, recordIndex;
	tti_byte *pSFI;
	tti_byte SFI;
	tti_uint16	tag, tagSize, length, lengthSize;
	tti_int32 tmp_pes_tag9f11_isexist = 0;
	tti_byte tmp_pes_tag9ff11_value[10];
	tti_uint16 tmp_pes_tag9ff11_len;

	status=SmartCardSelect(ddfName, ddfNameLen, tempResult, &tempResultLen);
	if (status!=STATUS_OK)
	{
		if ((status==STATUS_CARD_BLOCK)&&
			((memcmp(PSE_NAME, ddfName, strlen(PSE_NAME))==0)&&(ddfNameLen==strlen(PSE_NAME))))
		{
			return STATUS_CARD_BLOCK;
		}
		else if (status==STATUS_ERROR)
		{
			return STATUS_ERROR;
		}
		else
		{
			return STATUS_FAIL;
		}
	}

	if (checkDDFFormat(tempResult, tempResultLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	InitTagList(&ddfTagList);
	
	if (BuildTagList(tempResult, tempResultLen, &ddfTagList)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	if (memcmp(ddfName, PSE_NAME, ddfNameLen)==0)
	{
		if (TagIsExisted(&ddfTagList, TAG_PREFERRED_LANGUAGE))
		{
			memcpy(PSEPreferredLang, GetTagValue(&ddfTagList, TAG_PREFERRED_LANGUAGE),
				GetTagValueSize(&ddfTagList, TAG_PREFERRED_LANGUAGE));
			PSEPreferredLang[GetTagValueSize(&ddfTagList, TAG_PREFERRED_LANGUAGE)]='\0';
		}

		//Jason added on 20130527		
		if (TagIsExisted(&ddfTagList, TAG_ISSUER_CODE_INDEX))
		{
			DPRINTF("Save PES TAG_ISSUER_CODE_INDEX\n");
			tmp_pes_tag9f11_isexist = 1;
			memcpy(tmp_pes_tag9ff11_value, GetTagValue(&ddfTagList, TAG_ISSUER_CODE_INDEX),
				GetTagValueSize(&ddfTagList, TAG_ISSUER_CODE_INDEX));
			tmp_pes_tag9ff11_len = GetTagValueSize(&ddfTagList, TAG_ISSUER_CODE_INDEX);
		}
		//End
	}
	
	pSFI=GetTagValue(&ddfTagList, TAG_SFI);
	SFI=(*pSFI)<<3;
	FreeTagList(&ddfTagList);
	
	index=1;
	InitTagList(&recordTagList);
	
	while (TRUE)
	{
		status=SmartCardReadRecord(SFI, index, tempResult, &tempResultLen);
		if (status!=STATUS_OK)
		{
			FreeTagList(&recordTagList);
			if (status==STATUS_RECORD_NOT_FOUND)
			{
				return STATUS_OK;
			}
			else
			{
				return STATUS_FAIL;
			}
		}

		if (ParseTlvInfo(tempResult, tempResultLen, &tag, &tagSize, &length, &lengthSize)!=STATUS_OK)
		{
			FreeTagList(&recordTagList);
			return STATUS_FAIL;
		}

		if ((tag!=TAG_TEMPERATE_70)||(tempResultLen!=tagSize+length+lengthSize))
		{
			FreeTagList(&recordTagList);
			return STATUS_FAIL;
		}

		if (BuildTagListOneLevel(tempResult+tagSize+lengthSize, tempResultLen-tagSize-lengthSize,
			&recordTagList)!=STATUS_OK)
		{
			FreeTagList(&recordTagList);
			return STATUS_FAIL;
		}

		InitTagList(&entryTagList);
		
		for (recordIndex=0; recordIndex<recordTagList.ItemCount; recordIndex++)
		{
			if (recordTagList.tagItem[recordIndex].Tag!=TAG_ENTRY_TEMPLATE)
			{
				continue;
/*
				FreeTagList(&recordTagList);
				return STATUS_FAIL;
*/				
			}
		
			if (checkDDFEntryFormat(recordTagList.tagItem[recordIndex].Value, 
					recordTagList.tagItem[recordIndex].Length)==STATUS_OK)
			{
				if (BuildTagList(recordTagList.tagItem[recordIndex].Value, 
					recordTagList.tagItem[recordIndex].Length, &entryTagList)!=STATUS_OK)
				{
					FreeTagList(&recordTagList);
					FreeTagList(&entryTagList);
					return STATUS_FAIL;
				}
				status=getAidFromDDF(candidateAidList, GetTagValue(&entryTagList, TAG_DDF_NAME), 
					GetTagValueSize(&entryTagList, TAG_DDF_NAME), aidList, NULL);
				if (status!=STATUS_OK)
				{
					FreeTagList(&recordTagList);
					FreeTagList(&entryTagList);
					return status;
				}
				if (SmartCardSelect(ddfName, ddfNameLen, tempResult, &tempResultLen)!=STATUS_OK)
				{
					return STATUS_FAIL;
				}
			}
			else if (checkADFEntryFormat(recordTagList.tagItem[recordIndex].Value, 
					recordTagList.tagItem[recordIndex].Length)==STATUS_OK)
			{
				if (BuildTagListInMode(recordTagList.tagItem[recordIndex].Value, 
					recordTagList.tagItem[recordIndex].Length, TAGMODE_RESERVE_ALL_ITEM, &entryTagList)!=STATUS_OK)
				{
					FreeTagList(&recordTagList);
					FreeTagList(&entryTagList);
					return STATUS_FAIL;
				}

				//Jason added on 20130527
				if (memcmp(ddfName, PSE_NAME, ddfNameLen)==0)
				{
					//now is PSE
					if (TagIsExisted(&entryTagList, TAG_ISSUER_CODE_INDEX) == FALSE)
					{
						//Read record no TAG_ISSUER_CODE_INDEX, check pse taglist
						DPRINTF("Read record no TAG_ISSUER_CODE_INDEX, check pes taglist\n");
						if (tmp_pes_tag9f11_isexist == 1)
						{
							//PES has TAG_ISSUER_CODE_INDEX, set to each record.
							DPRINTF("PES has TAG_ISSUER_CODE_INDEX, set to each record\n");
							SetTagValue(TAG_ISSUER_CODE_INDEX, tmp_pes_tag9ff11_value, 
								tmp_pes_tag9ff11_len, &entryTagList);
						}
					}
				}
				//End
				if (tryAddAidItemByADF(candidateAidList, &entryTagList, &aidList->aidInfo[aidList->count])==STATUS_OK)
				{
					(aidList->count)++;
				}
			}
			else
			{
				FreeTagList(&recordTagList);
				FreeTagList(&entryTagList);
				return STATUS_FAIL;
			}

			FreeTagList(&entryTagList);
		}

		FreeTagList(&recordTagList);
		index++;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 getApplicationByPSE(CANDIDATE_LIST *candidateAidList, 
	AID_LIST *aidList, tti_tchar *PSEPreferredLang)
{
	tti_int32 status;

	DPRINTF("select app by pse\n");

	status=getAidFromDDF(candidateAidList, PSE_NAME, (tti_byte)strlen(PSE_NAME), 
		aidList, PSEPreferredLang);
	if (status!=STATUS_OK)
	{
		return status;
	}
	
	if (aidList->count==0)
	{
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 tryAddAidItem(CANDIDATE_INFO *candidateAid, tti_byte *result, 
	tti_byte resultLen, AID_LIST *aidList, tti_int32 status)
{
	TagList tagList;
	tti_uint16 labelLen;
	tti_byte *pValue;
	tti_int32 returnStatus;

	DPRINTF("try add aid item\n");

	InitTagList(&tagList);

	BuildTagListInMode(result, resultLen, TAGMODE_RESERVE_ALL_ITEM, &tagList);

	if (!TagIsExisted(&tagList, TAG_DF_NAME))
	{
		return STATUS_NOT_MATCH;
	}

	memset(&aidList->aidInfo[aidList->count], 0, sizeof(AID_INFO));

	aidList->aidInfo[aidList->count].aidLength=GetTagValueSize(&tagList, TAG_DF_NAME);
	if ((aidList->aidInfo[aidList->count].aidLength<5)||
		(aidList->aidInfo[aidList->count].aidLength>16))
	{
		return STATUS_NOT_MATCH;
	}
	memcpy(aidList->aidInfo[aidList->count].aidName, 
		GetTagValue(&tagList, TAG_DF_NAME), aidList->aidInfo[aidList->count].aidLength);

	if (((candidateAid->aidLength)>(aidList->aidInfo[aidList->count].aidLength))||
		(memcmp(candidateAid->aidName, aidList->aidInfo[aidList->count].aidName, candidateAid->aidLength)!=0))
	{
		FreeTagList(&tagList);
		return STATUS_NOT_NEED_AID;
	}

	if (TagIsExisted(&tagList, TAG_PREFERRED_NAME))
	{
		labelLen=GetTagValueSize(&tagList, TAG_PREFERRED_NAME);
		memcpy(aidList->aidInfo[aidList->count].aidPreferLabel,
			GetTagValue(&tagList, TAG_PREFERRED_NAME), labelLen);
		aidList->aidInfo[aidList->count].aidPreferLabel[labelLen]='\0';
	}
	else
	{
		strcpy(aidList->aidInfo[aidList->count].aidPreferLabel, "");
	}

	if (TagIsExisted(&tagList, TAG_APP_LABEL))
	{
		labelLen=GetTagValueSize(&tagList, TAG_APP_LABEL);
		memcpy(aidList->aidInfo[aidList->count].aidLabel, 
			GetTagValue(&tagList, TAG_APP_LABEL), labelLen);
		aidList->aidInfo[aidList->count].aidLabel[labelLen]='\0';
	}
	else
	{
		strcpy(aidList->aidInfo[aidList->count].aidLabel, "");
	}
	
	if (TagIsExisted(&tagList, TAG_ISSUER_CODE_INDEX))
	{
		pValue=GetTagValue(&tagList, TAG_ISSUER_CODE_INDEX);
		TranslateHexToChars(pValue, 1, aidList->aidInfo[aidList->count].issuerCodeTableIndex);
	}
	else
	{
		strcpy(aidList->aidInfo[aidList->count].issuerCodeTableIndex, "");
	}

	if (TagIsExisted(&tagList, TAG_FCI_ISSUER_DATA))
	{
		pValue=GetTagValue(&tagList, TAG_FCI_ISSUER_DATA);
		TranslateHexToChars(pValue, GetTagValueSize(&tagList, TAG_FCI_ISSUER_DATA),
			aidList->aidInfo[aidList->count].fciIssuerData);
	}
	else
	{
		strcpy(aidList->aidInfo[aidList->count].fciIssuerData, "");
	}
	
	if (!TagDataIsMissing(&tagList, TAG_APP_PRIOR))
	{
		pValue=GetTagValue(&tagList, TAG_APP_PRIOR);
		if ((*(pValue)&Bit8)!=0)
		{
			DPRINTF("aid->mustConform=TRUE\n");
			aidList->aidInfo[aidList->count].mustConform=TRUE;
		}
		DPRINTF("aid->mustConform=%d\n", aidList->aidInfo[aidList->count].mustConform);
		aidList->aidInfo[aidList->count].priority=(*pValue)&0x0F;
	}
	else
	{
		DPRINTF("aid->mustConform=FALSE\n");
		aidList->aidInfo[aidList->count].mustConform=FALSE;
		aidList->aidInfo[aidList->count].priority=0x0F;
	}

	memcpy(aidList->aidInfo[aidList->count].verionNumber, 
		candidateAid->verionNumber, APP_VERSION_NUMBER_SIZE);

	FreeTagList(&tagList);

	if (!aidIsMatch(candidateAid, &aidList->aidInfo[aidList->count]))
	{
		return STATUS_NOT_MATCH;
	}

	if (candidateAid->aidLength==aidList->aidInfo[aidList->count].aidLength)
	{
		returnStatus=STATUS_EXACT_MATCH;
	}
	else
	{
		returnStatus=STATUS_NOT_EXACT_MATCH;
	}
	
	if (status==STATUS_OK)
	{
		(aidList->count)++;
	}

	DPRINTF("tryAddAidItem ok");
	return returnStatus;
}

//------------------------------------------------------------------------------------------
tti_int32 getApplicationByAidList(CANDIDATE_LIST *candidateAidList, AID_LIST *aidList)
{
	tti_int32 index;
	tti_byte response[BUFFER_SIZE_512BYTE];
	tti_uint16 respLen;
	tti_int32 status, addAidStatus;
	tti_bool blockedApp=FALSE;

	DPRINTF("get aid by candidate list\n");
	
	for (index=0; index<candidateAidList->count; index++)
	{
		status=SmartCardSelect(candidateAidList->candidateAidInfo[index].aidName, 
			candidateAidList->candidateAidInfo[index].aidLength, response, &respLen);
		//Jason start delete 20101104 
		/*
		if ((status==STATUS_CARD_BLOCK)||(status==STATUS_ERROR))
		*/
		//Jason end delete 20101104 

		//Jason start add 20101104 
		if ((status==STATUS_CARD_BLOCK && index == 0)||(status==STATUS_ERROR))
		//Jason end add 20101104 
		{
            ReportLine();
            DPRINTF("SmartCardSelect error\n");
            return status;
		}
		if ((status==STATUS_OK)||(status==STATUS_APP_BLOCKED))
		{
			if (status==STATUS_APP_BLOCKED)
			{
				DPRINTF("blockedApp=TRUE\n");
				blockedApp=TRUE;
			}
			//*************************************************************
			if (checkADFFormat(response, respLen)!=STATUS_OK)
			{
                ReportLine();
                DPRINTF("checkADFFormat error\n");
				continue;
			}
			addAidStatus=tryAddAidItem(&candidateAidList->candidateAidInfo[index], 
				response, respLen, aidList, status);
			if ((addAidStatus==STATUS_EXACT_MATCH)||(addAidStatus==STATUS_NOT_NEED_AID)
				||candidateAidList->candidateAidInfo[index].exactMatch)
			{
				continue;
			}
			/*
			//Updated by Harrison 
			if (checkADFFormat(response, respLen)==STATUS_OK)
			{
				addAidStatus=tryAddAidItem(&candidateAidList->candidateAidInfo[index], 
				response, respLen, aidList, status);
			}

			//Update End.*/

			while (TRUE)
			{
				status=SmartCardSelectNext(candidateAidList->candidateAidInfo[index].aidName, 
					candidateAidList->candidateAidInfo[index].aidLength, response, &respLen);
				if ((status!=STATUS_OK)&&(status!=STATUS_APP_BLOCKED))
				{
					break;
				}
				if (tryAddAidItem(&candidateAidList->candidateAidInfo[index], response, 
					respLen, aidList, status)==STATUS_EXACT_MATCH)
				{
					break;
				}
			}
		}
	}

	if (((aidList->count)==0)&&blockedApp)
	{
        ReportLine();
		return STATUS_APP_BLOCKED;
	}
	else
	{
		DPRINTF("getApplicationByAidList return OK\n");
		return STATUS_OK;
	}
}

//------------------------------------------------------------------------------------------
void exchangeAid(AID_INFO *aid1, AID_INFO *aid2)
{
	AID_INFO tempAid;

	memcpy(&tempAid, aid1, sizeof(AID_INFO));
	memcpy(aid1, aid2, sizeof(AID_INFO));
	memcpy(aid2, &tempAid, sizeof(AID_INFO));
}

//------------------------------------------------------------------------------------------
void sortAidList(AID_LIST *aidList)
{
	tti_int32 i, j;

	for (i=0; i<aidList->count-1; i++)
	{
		for (j=i+1; j<aidList->count; j++)
		{
			if (aidList->aidInfo[i].priority>aidList->aidInfo[j].priority)
			{
				exchangeAid(&aidList->aidInfo[i], &aidList->aidInfo[j]);
			}
		}
	}
}

//------------------------------------------------------------------------------------------
tti_int32 checkDuplicateItem(TagList *tagList)
{
	if (tagList->duplicateItemCount>0)
	{
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
}


//add by bob 20101028
//------------------------------------------------------------------------------------------
tti_int32 checkRecordData(tti_byte *pBuf, tti_uint16 *pSize)
{
	tti_uint16 Tag;

	tti_uint16	TagSize;

	tti_uint16 lengthSize;

	tti_uint16	Length;

	tti_byte 	*CurPos, *EndPos;

	tti_uint16   size = *pSize;

	tti_bool    bExistTemplate70 = FALSE;
	tti_byte *buf = NULL;

	*pSize = 0; 
	
	if ((pBuf==NULL)||(size==0))

	{

		return STATUS_OK;

	}

	buf = (tti_byte *)malloc(size);

	if (NULL == buf)
	{
		return STATUS_FAIL;
	}
	else
	{
		//malloc_Counter();
		memcpy(buf, pBuf, size);
	}

	CurPos=buf;

	EndPos=(buf+size);



	while(CurPos < EndPos)

	{
//*
		if ((*CurPos==0x00)||(*CurPos==0xFF))

		{
			//DPRINTF("check record data\n");
			//DPRINTF("(*CurPos==0x00)||(*CurPos==0xFF)\n");
			
			memcpy(&pBuf[*pSize], CurPos, 1);
			*pSize += 1;
			CurPos++;
			continue;

		}
//*/
		//printByteArray(CurPos, (tti_uint16)(EndPos-CurPos));
		if(STATUS_OK != ParseTlvInfo(CurPos, (tti_uint16)(EndPos-CurPos), &Tag, &TagSize, &Length, &lengthSize))

		{
		//Jason added on 20200507
			if (NULL != buf)
			{
				free(buf);
				buf = NULL;
				//free_Counter();
			}
		//End

			return STATUS_FAIL;
		}

		if (TagIsTemplate(Tag))
		{
			memcpy(&pBuf[*pSize], CurPos, TagSize + lengthSize);
			*pSize += (TagSize + lengthSize);
			CurPos += TagSize + lengthSize; //should be template 70 + 1 byte length		
			bExistTemplate70 = TRUE;
		}
		else
		{
			//DPRINTF("Tag value:%04x, TagSize = %d", Tag, TagSize);
			if (((Tag == 0x9f02) && (TagSize == 2)) ||
				((Tag == 0x81) && (TagSize == 1)) ||
				((Tag == 0x9f06) && (TagSize == 2)) ||
				((Tag == 0x9f15) && (TagSize == 2)) ||
				((Tag == 0x9f16) && (TagSize == 2)) ||
				((Tag == 0x9f4E) && (TagSize == 2)) ||
				((Tag == 0x9f39) && (TagSize == 2)) ||
				((Tag == 0x9f1c) && (TagSize == 2)) ||
				((Tag == 0x9f33) && (TagSize == 2)) ||
				((Tag == 0x95) && (TagSize == 1)) ||
				((Tag == 0x9A) && (TagSize == 1)) ||
				((Tag == 0x9C) && (TagSize == 1)) ||
				((Tag == 0x9B) && (TagSize == 1)) ||
				((Tag == 0x5F2A) && (TagSize == 2))||
				((Tag == 0x9F37) && (TagSize == 2)))
			{
				//do nothing
				//DPRINTF("enter this one\n");
				if(bExistTemplate70)
					pBuf[1] -= (TagSize+lengthSize+Length);
			}
			else
			{
				memcpy(&pBuf[*pSize], CurPos, TagSize + lengthSize + Length);
				*pSize += (TagSize + lengthSize + Length);
				//printByteArray(pBuf, *pSize);
			}

			CurPos += TagSize + lengthSize;

			CurPos += Length;
		}

	}

	if (NULL != buf)
	{
		free(buf);
		buf = NULL;
		//free_Counter();
	}

	if(CurPos != EndPos)
	{
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
}


//------------------------------------------------------------------------------------------
tti_int32 checkRecordFormat(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
//	tti_int32 status;

	//DPRINTF("check record format\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

//	if (!TagIsExisted(&tempTagList, TAG_TEMPERATE_70)||(tempTagList.ItemCount!=1))
	if (!TagIsExisted(&tempTagList, TAG_TEMPERATE_70))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	FreeTagList(&tempTagList);
	return STATUS_OK;	
}

//Jason added 2012.04.18, start
tti_bool IsECTrans(RunningTimeEnv *runningTimeEnv)
{
	tti_byte *pECTerminalSupport;

#ifdef VPOS_APP
    return FALSE;
#else
	DPRINTF("Enter IsECTrans\n");
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_EC_ISSUER_AUTH_CODER) == FALSE)
	{
		return FALSE;
	}
	DPRINTF("IsECTrans  11\n");
	
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_EC_TERMINAL_SUPPORT_IND) == FALSE)
	{
		return FALSE;
	}
	DPRINTF("IsECTrans  222\n");
	

	pECTerminalSupport = GetTagValue(&runningTimeEnv->tagList, TAG_EC_TERMINAL_SUPPORT_IND);
	DPRINTF("IsECTrans  333\n");
	
	if (*pECTerminalSupport == 0x00)
	{
		DPRINTF("IsECTrans  444\n");
		return FALSE;
	}else
	{
		DPRINTF("IsECTrans  TRUE\n");
		return TRUE;
	}
#endif 	
}
//End

//Jason added 2012.04.18, start
tti_int32 SetECInd(RunningTimeEnv *runningTimeEnv)
{
	tti_byte *pTransType;	
	
	DPRINTF("SetECInd\n");

	//assume set TAG_EC_TERMINAL_SUPPORT_IND to 0.
	SetTagValue(TAG_EC_TERMINAL_SUPPORT_IND, "\x00", 1, &runningTimeEnv->tagList);	
	
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_PDOL) == FALSE)
	{
		return STATUS_OK;
	}

	if (tagIsInDOL(&runningTimeEnv->tagList, TAG_PDOL, TAG_EC_TERMINAL_SUPPORT_IND) == FALSE)
	{
		return STATUS_OK;
	}
	
	if (tagIsInDOL(&runningTimeEnv->tagList, TAG_PDOL, TAG_AMOUNT) == FALSE)
	{
		return STATUS_OK;
	}

	if (tagIsInDOL(&runningTimeEnv->tagList, TAG_PDOL, TAG_TERM_COUNTRY_CODE) == FALSE)
	{
		return STATUS_OK;
	}
		
	if (tagIsInDOL(&runningTimeEnv->tagList, TAG_PDOL, TAG_TRANS_CURRENCY_CODE) == FALSE)
	{
		return STATUS_OK;
	}

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_TRANSACTION_TYPE) == FALSE)
	{
		return STATUS_OK;
	}

	pTransType = GetTagValue(&runningTimeEnv->tagList, TAG_TRANSACTION_TYPE);
	if(*pTransType != TRANS_TYPE_GOODS_AND_SERVICE)
	{
		DPRINTF("Type is not goods");
		return STATUS_OK;
	}

	
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_EC_TERMINAL_TRANS_LIMIT) == TRUE)
	{
		if (getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT) <
			getAmountByTag(&runningTimeEnv->tagList, TAG_EC_TERMINAL_TRANS_LIMIT))
		{
			SetTagValue(TAG_EC_TERMINAL_SUPPORT_IND, "\x01", 1, &runningTimeEnv->tagList);
		}
	}else if (TagIsExisted(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT) == TRUE)
	{
		DPRINTF("Amount: %d\n", getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT));
		DPRINTF("TAG_FLOOR_LIMIT: %d\n", getDataByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT));
		if (getAmountByTag(&runningTimeEnv->tagList, TAG_AMOUNT) <
			getDataByTag(&runningTimeEnv->tagList, TAG_FLOOR_LIMIT))
		{
			SetTagValue(TAG_EC_TERMINAL_SUPPORT_IND, "\x01", 1, &runningTimeEnv->tagList);
		}
	}

	return STATUS_OK;
}
//End

//Jason added 2012.04.18, start
tti_int32 GetECParaFromICC(RunningTimeEnv *runningTimeEnv)
{	
	if (IsECTrans(runningTimeEnv) == FALSE)
	{
		return STATUS_OK;
	}
	
	getSpecialDataFromICC(runningTimeEnv, TAG_EC_BALANCE);
	getSpecialDataFromICC(runningTimeEnv, TAG_EC_RESET_THRESHOLD);

	return STATUS_OK;
}
//End

//------------------------------------------------------------------------------------------
tti_int32 checkPan(RunningTimeEnv *runningTimeEnv)
{
	tti_tchar tempTrack2[BUFFER_SIZE_512BYTE];
	tti_tchar tempPan[BUFFER_SIZE_512BYTE];
//	tti_int32 panLen;
	
	if (runningTimeEnv->panIsOk)
	{
		return STATUS_OK;
	}
	
	if ((!TagIsExisted(&runningTimeEnv->tagList, TAG_TRACK2))
		||(!TagIsExisted(&runningTimeEnv->tagList, TAG_PAN)))
	{
		return STATUS_OK;  
	}

    //Jason added on 2017.05.05
	if ( GetTagValueSize(&runningTimeEnv->tagList, TAG_PAN) > 10 ||
		GetTagValueSize(&runningTimeEnv->tagList, TAG_TRACK2) > 19)
	{
		ReportLine();
		return STATUS_FAIL;
	}
    //End

	translateBcdArrayToString(GetTagValue(&runningTimeEnv->tagList, TAG_PAN), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_PAN), tempPan);
	
	TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_TRACK2), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_TRACK2), tempTrack2);

	//Jason removed on 2017.05.04, no need compare
	/*
	panLen=strlen(tempPan);
	if (strncmp(tempPan, tempTrack2, panLen)!=0)
	{
		ReportLine();
		return STATUS_FAIL;
	}
	*/
	//End
	
	//Jason removed on 2014.01.13
	/*
	if ((tempTrack2[panLen]!='D')&&(tempTrack2[panLen]!='d'))
	{
		ReportLine();
		return STATUS_FAIL;
	}
	*/
	//End
	runningTimeEnv->panIsOk=TRUE;

	return STATUS_OK;
}

//End

//------------------------------------------------------------------------------------------
tti_int32 checkQpbocPan(RunningTimeEnv *runningTimeEnv)
{
//	tti_tchar tempTrack2[BUFFER_SIZE_512BYTE];
//	tti_tchar tempPan[BUFFER_SIZE_512BYTE];
//	tti_int32 panLen;
	
	if (runningTimeEnv->panIsOk)
	{
		return STATUS_OK;
	}
	
	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_TRACK2))
	{
		return STATUS_OK;  
	}
	
	runningTimeEnv->panIsOk=TRUE;

	return STATUS_OK;
}


//------------------------------------------------------------------------------------------
tti_int32 initTransaction(TerminalParam *terminalParam, RunningTimeEnv *runningTimeEnv)
{
	tti_tchar response[BUFFER_SIZE_512BYTE];
	tti_uint16 respLen;
	tti_byte *AFL;
	tti_int32 AFLLen, index, recordIndex;
	tti_tchar dataList[BUFFER_SIZE_512BYTE];
	tti_byte dataListLen;
	tti_int32 offlineDataRecord;
	tti_uint16 tagLen, tagLenSize;
	tti_int32 status;
	tti_uint16 ingoreFlag = 0;

	DPRINTF("init application\n");
	DPRINTF("initTransaction\n");

	
	prepareTerminalSideData(terminalParam, &runningTimeEnv->tagList);

	if (SmartCardSelect(GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_AID), 
		(tti_byte)GetTagValueSize(&runningTimeEnv->tagList, TAG_TERMINAL_AID), 
		(tti_byte *)response, &respLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	DPRINTF("Select response:\n");
	printByteArray((tti_byte *)response, respLen);
	if (checkADFFormat((tti_byte *)response, respLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	if (BuildTagList((tti_byte *)response, respLen, &runningTimeEnv->tagList)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	ResetDuplicateStatus(&runningTimeEnv->tagList);

	removeTag(&runningTimeEnv->tagList, TAG_ISSUER_COUNTRY_CODE);

	SetECInd(runningTimeEnv);

	//PrintOutTagList(&runningTimeEnv->tagList, "tag list before GPO");
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_PDOL))
	{
		BuildDOLToStream(&runningTimeEnv->tagList, TAG_PDOL, (tti_byte *)(dataList+2), &dataListLen);
		//Jason added on 2014.03.17
		memcpy(runningTimeEnv->pdolStream, dataList+2, dataListLen);
		runningTimeEnv->pdolStreamLen = dataListLen;
		//End
		*dataList='\x83';
		if (dataListLen<128)
		{
			*(dataList+1)=dataListLen;
			dataListLen+=2;
		}
		else
		{
			memmove(dataList+3, dataList+2, dataListLen);
			*(dataList+1)=0x81;
			*(dataList+2)=dataListLen;
			dataListLen+=3;
		}
	}
	else
	{
		memcpy(dataList, "\x83\x00", 2);
		dataListLen=2;
	}
	status=SmartCardGetprocessOption((tti_byte *)dataList, dataListLen, (tti_byte *)response, &respLen);

	if (status!=STATUS_OK)
	{
		return status;
	}

	if (ParseGPOResponse((tti_byte *)response, respLen, &runningTimeEnv->tagList)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_AIP)||
		(TagDataIsMissing(&runningTimeEnv->tagList, TAG_AFL)))
	{
		return STATUS_FAIL;
	}
        
	AFL=GetTagValue(&runningTimeEnv->tagList, TAG_AFL);
	AFLLen=GetTagValueSize(&runningTimeEnv->tagList, TAG_AFL);
	DPRINTF("AFL list\n");
	hexdump(AFL, AFLLen);
	
	if (((AFLLen/4)*4!=AFLLen)||(AFLLen==0))
	{
		return STATUS_FAIL;
	}

	runningTimeEnv->offlineAuthDataLen=0;

	for (index=0; index<AFLLen; index+=4)
	{
		if ((AFL[index+1]==0)||(AFL[index+2]-AFL[index+1]+1<AFL[index+3]))
		{
			return STATUS_FAIL;
		}
		offlineDataRecord=0;
		DPRINTF("index = %d\n", index);
		for (recordIndex=AFL[index+1]; recordIndex<=AFL[index+2]; recordIndex++)
		{
			if (SmartCardReadRecord(AFL[index], (tti_byte)recordIndex, (tti_byte *)response, &respLen)==STATUS_OK)
			{
				if (checkRecordFormat((tti_byte *)response, respLen)!=STATUS_OK)
				{
					DPRINTF("checkRecordFormat error\n");
					if (((AFL[index]>>3)<=10)||((AFL[index]>>3)>30))
					{
						return STATUS_FAIL;
					}
					if (!terminalIsOnlineOnly(runningTimeEnv))
					{
						//Jason added on 2014.03.15
						if (needToDo(AIP_COMBINED_DYNAMIC_DATA_AUTH)&&terminalSupport(TERM_CAP_CDA))
						{
							SetTSI(TSI_OFFLINE_DATA_AUTH_PERFORMED);
							runningTimeEnv->offlineAuthWasDone=TRUE;
							SetTVR(TVR_CDA_FAILED);
						}
						//End
						if (needToDo(AIP_DYNAMIC_DATA_AUTH)&&terminalSupport(TERM_CAP_DDA))
						{
							SetTSI(TSI_OFFLINE_DATA_AUTH_PERFORMED);
							runningTimeEnv->offlineAuthWasDone=TRUE;
							SetTVR(TVR_DDA_FAILED);
						}
						else if (needToDo(AIP_STATIC_DATA_AUTH)&&terminalSupport(TERM_CAP_SDA))
						{
							SetTSI(TSI_OFFLINE_DATA_AUTH_PERFORMED);
							runningTimeEnv->offlineAuthWasDone=TRUE;
							SetTVR(TVR_SDA_FAILED);
							//Jason added on 2014.01.13
							SetTVR(TVR_SDA_SELECTED);
							//end
						}
					}
				}
				else
				{
					//******************************************
					//bob add here 20101028
					//*
					DPRINTF("New checkRecordData\n");
					if (checkRecordData((tti_byte *)response, &respLen)!=STATUS_OK)
					{
						DPRINTF("checkRecordData error\n");
						//return STATUS_FAIL;
						return STATUS_READ_RECORD_ERR;
					}
					DPRINTF("New checkRecordData ok, respLen = [%d]\n", respLen);
					
					//*/
					//*******************************************
					printByteArray((tti_byte *)response, respLen);
					if (BuildTagList((tti_byte *)response, respLen, &runningTimeEnv->tagList)!=STATUS_OK)
					{
						DPRINTF("after new check record data, BuildTagList error\n");
						return STATUS_READ_RECORD_ERR;
					}
				}
				DPRINTF("offlineDataRecord [%d]\n", offlineDataRecord);
				DPRINTF("AFL[index+3] = [%d]\n", AFL[index+3]);
				if (offlineDataRecord<AFL[index+3])
				{
					DPRINTF("Enter set offline data record \n");
					printByteArray((tti_byte *)response, respLen);
					if (response[0]==TAG_TEMPERATE_70)
					{
						DPRINTF("response[0]==TAG_TEMPERATE_70\n");
						if ((AFL[index]>>3)>10)
						{
							DPRINTF("SFI >10\n");
							memcpy(&runningTimeEnv->offlineAuthData[runningTimeEnv->offlineAuthDataLen],
								response, respLen);
							runningTimeEnv->offlineAuthDataLen+=respLen;
						}
						else
						{
							DPRINTF("SFI <= 10\n");
							if (ParseTlvLengthInfo((tti_byte *)(response+1), respLen-1, &tagLen, &tagLenSize)
								!=STATUS_OK)
							{
								//return STATUS_FAIL;
								ReportLine();
								return STATUS_READ_RECORD_ERR;
							}
							DPRINTF("Have offline data\n");
							memcpy(&runningTimeEnv->offlineAuthData[runningTimeEnv->offlineAuthDataLen],
								response+1+tagLenSize, respLen-1-tagLenSize);
							runningTimeEnv->offlineAuthDataLen+=respLen-1-tagLenSize;
						}
					}else
					{
						DPRINTF("response[0]!=TAG_TEMPERATE_70\n");
					}
				}
			}
			else
			{
				//return STATUS_FAIL;
				ReportLine();
				return STATUS_READ_RECORD_ERR;
			}

			if (checkPan(runningTimeEnv)!=STATUS_OK)
			{
				//return STATUS_FAIL;
				ReportLine();
				return STATUS_READ_RECORD_ERR; 
			}
				
			offlineDataRecord++;
		}
	}

	if (checkMandatoryData(runningTimeEnv)!=STATUS_OK)
	{
		
		EmvIOSessionEnd();
		//return STATUS_FAIL;
		ReportLine();
		return STATUS_READ_RECORD_ERR;
	}

	ReportLine();
	if ((!TagIsExisted(&runningTimeEnv->tagList, TAG_TDOL))
		&&(runningTimeEnv->commonRunningData.defaultTDOL.len!=0))
	{
		SetTagValue(TAG_TDOL, runningTimeEnv->commonRunningData.defaultTDOL.value, 
			runningTimeEnv->commonRunningData.defaultTDOL.len, &runningTimeEnv->tagList);
		runningTimeEnv->commonRunningData.defaultTDOLUsed=TRUE;
	}

	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_DDOL))
	{
		SetTagValue(TAG_DDOL, runningTimeEnv->commonRunningData.defaultDDOL.value, 
			runningTimeEnv->commonRunningData.defaultDDOL.len, &runningTimeEnv->tagList);
	}

	DPRINTF("duplicate tag count=%d\n", runningTimeEnv->tagList.duplicateItemCount);

	//Jason modified on 2017.05.05
	ingoreFlag = 1;
	for (index=0; index<runningTimeEnv->tagList.duplicateItemCount; index++)
	{
		DPRINTF("duplicate tag=%x\n", runningTimeEnv->tagList.duplicateItem[index]);
		if (runningTimeEnv->tagList.duplicateItem[index] <= 0x9F7F && 
			runningTimeEnv->tagList.duplicateItem[index] >= 0x9F50)
		{
			//Do nothing
			LOGD("Now is duplicate tag from 0x9F50 to 0x9F7F, you can ingore them");
		}else if (runningTimeEnv->tagList.duplicateItem[index] == 0xDF01 || 
		   	runningTimeEnv->tagList.duplicateItem[index] == 0x80 )
		{
			 //Do nothing
			LOGD("Duplicate tag 0xDF01 or 0x80, you can ingore them");
		 }else
		{
			//The duplicate tag is not from 0x9F50 to 0x9F7F, you can't ingore the duplicate tag check.
			LOGD("Now have duplicate tag not from 0x9F50 to 0x9F7F, you can't ingore it");
			ingoreFlag = 0;
		}
	}

	if (ingoreFlag == 0)
	{
		if (checkDuplicateItem(&runningTimeEnv->tagList)==STATUS_OK)
		{
			return STATUS_OK;
		}
		else
		{
			//return STATUS_FAIL;
			ReportLine();
			return STATUS_READ_RECORD_ERR;
		}
	}else
	{
		ReportLine();
		return STATUS_OK;
	}
	//End
}



//Jason added on 2012.04.17, start
tti_int32 ICCReadLogTransaction(TerminalParam *terminalParam, RunningTimeEnv *runningTimeEnv)
{
	tti_tchar response[BUFFER_SIZE_512BYTE];
	tti_uint16 respLen;
	tti_byte *LogEntry;
	tti_int32 recordIndex;
	//tti_uint16 tagLen, tagLenSize;
	tti_byte bySFI;
	tti_int32 status;

	DPRINTF("init ICCReadLogTransaction\n");
	
	//prepareTerminalSideData(terminalParam, &runningTimeEnv->tagList);
	//removeTag(&runningTimeEnv->tagList, TAG_TERM_COUNTRY_CODE);
	status = SmartCardSelect(GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_AID), 
		(tti_byte)GetTagValueSize(&runningTimeEnv->tagList, TAG_TERMINAL_AID), 
		(tti_byte *)response, &respLen);
	if (status != STATUS_OK && status != STATUS_APP_BLOCKED)
	{
		return STATUS_FAIL;
	}

	DPRINTF("Select response:\n");
	printByteArray((tti_byte *)response, respLen);
	if (checkADFFormat((tti_byte *)response, respLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	DPRINTF("1111\n");
	

	if (BuildTagList((tti_byte *)response, respLen, &runningTimeEnv->tagList)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_LOG_ENTRY) == FALSE)
	{
		DPRINTF("No Log Entry tag\n");
		return STATUS_FAIL;
	}
	LogEntry = GetTagValue(&runningTimeEnv->tagList, TAG_LOG_ENTRY);
	if ( LogEntry[0] < 11 || LogEntry[0] > 20)
	{
		return STATUS_FAIL;
	}

	getSpecialDataFromICC(runningTimeEnv, TAG_LOG_FORMAT);	
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_LOG_FORMAT) == FALSE)
	{
		DPRINTF("No Log format tag\n");
		return STATUS_FAIL;
	}

	

	bySFI = (LogEntry[0])<<3;
	for (recordIndex=1; recordIndex<=LogEntry[1]; recordIndex++)
	{
		DisplayProcess("读取中");
		if (SmartCardReadRecord(bySFI, (tti_byte)recordIndex, (tti_byte *)response, &respLen)==STATUS_OK)
		{
			DPRINTF("Read record ok\n");
			if (ParseDOLStreamToTagList(&runningTimeEnv->tagList, TAG_LOG_FORMAT, (tti_byte *)response, respLen) == STATUS_OK)
			{
				DPRINTF("ParseDOLStreamToTagList ok\n");
				if (recordIndex == LogEntry[1])
				{
					status = DisplayLog(recordIndex, &runningTimeEnv->tagList, TRUE);
				}else
				{
					status = DisplayLog(recordIndex, &runningTimeEnv->tagList, FALSE);
				}
				if (status == STATUS_OK)
				{
					continue;	
				}
			}else
			{
				DPRINTF("ParseDOLStreamToTagList error\n");
				continue;
			}
		}else
		{
			DPRINTF("Read record error\n");
			continue;
		}
	}		
	return STATUS_OK;
}
//End

//Jason added on 2012.04.24, start
tti_int32 ReadECBalance(TerminalParam *terminalParam, RunningTimeEnv *runningTimeEnv)
{
	tti_tchar response[BUFFER_SIZE_512BYTE];
	tti_uint16 respLen;
	//tti_byte *LogEntry;
	//tti_int32 index, recordIndex;
	//tti_uint16 tagLen;
	tti_tchar szDisp[100] = {0};
	tti_tchar szForamtBalance[100] = {0};
	tti_int32 status;

	//prepareTerminalSideData(terminalParam, &runningTimeEnv->tagList);
	//removeTag(&runningTimeEnv->tagList, TAG_TERM_COUNTRY_CODE);
	status = SmartCardSelect(GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_AID), 
		(tti_byte)GetTagValueSize(&runningTimeEnv->tagList, TAG_TERMINAL_AID), 
		(tti_byte *)response, &respLen);
	if (status != STATUS_OK && status != STATUS_APP_BLOCKED)
	{
		return STATUS_FAIL;
	}

	DPRINTF("Select response:\n");
	printByteArray((tti_byte *)response, respLen);
	if (checkADFFormat((tti_byte *)response, respLen)!=STATUS_OK)
	{
		//DisplayCorfim("Read balance error", 5);	
		return STATUS_FAIL;
	}
	DPRINTF("1111\n");
	
	if (BuildTagList((tti_byte *)response, respLen, &runningTimeEnv->tagList)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	getSpecialDataFromICC(runningTimeEnv, TAG_EC_BALANCE);	
	if (TagIsExisted(&runningTimeEnv->tagList, TAG_EC_BALANCE) == FALSE)
	{
		//DisplayCorfim("Read balance error", 5);
		DisplayValidateInfo("Read balance error");
		return STATUS_FAIL;
	}

	formatAmountByTag(&runningTimeEnv->tagList, TAG_EC_BALANCE, szForamtBalance);
	sprintf(szDisp, "余额:%s", szForamtBalance);
	//DisplayCorfim(szDisp, 5);
	DisplayValidateInfo(szDisp);	
	return STATUS_OK;
}
//End

//------------------------------------------------------------------------------------------
tti_int32 getAidList(TerminalParam *terminalParam, tti_int32 timeOut, AID_LIST *aidList)
{

	//tti_bool IsFirstCheckCardPresent = TRUE;
	tti_bool isTimeOut=TRUE;
	tti_int32 index, status;

	//info("1");
	//DPRINTF("Test DPRINTF\n");

	memset(aidList, 0, sizeof(AID_LIST));
	
	EmvIOInit();		//Init EMV IO.
	//SCardTimeOutEx(100);
	DPRINTF("timeOut = %d\n", timeOut);

	for (index=0; index<timeOut*5; index++)
	{
		//DPRINTF("111\n");
		status = EmvIOIsCardPresent();
		if (status == 0)
		{
			DPRINTF("Card present\n");
			isTimeOut=FALSE;
			break;
		}else if (status == -3)
		{
			DPRINTF("Power on error\n");
			return STATUS_FAIL;
		}else
		{
			DPRINTF("Card not present\n");
			return STATUS_FAIL;
		}
	
		/*
		if(EmvIOIsCardPresent())
		{
			isTimeOut=FALSE;
			break;
		}
		*/
	
		//DPRINTF("222\n");
#if 0	
		if (WaitForCancel() == TRUE)
		{
			DPRINTF("User cancel\n");
			return STATUS_CANCEL;				
		}
	
	//	DPRINTF("3333\n");
		if(IsFirstCheckCardPresent)
		{
			IsFirstCheckCardPresent =FALSE;
		}
		else
		{
			//Z5 debug remove
			//msleep(200);
			//Z5 end
			//delayms_ex(200);
		}
		//DPRINTF("4444\n");
#endif 	
	}

	if (isTimeOut)
	{
		return STATUS_TIMEOUT;
	}	
	
	/*
	msleep(300);
	
	SCardTimeOutEx(1000);
	SCardSetOperateMode(CARD_OPERATE_MODE_EMV);
	if (SCardPowerUp()!=SCARD_S_SUCCESS)
	{
		return STATUS_ERROR;
	}
	SCardTimeOut(5000);
	
	EmvIOIsCardPresent();
	*/

	DPRINTF("jason 0007 ~~~~~~\n");
	DPRINTF("1234568888\n");
	info("1");
	//delayms_ex(5000);
	strcpy(PSEPreferredLang, "");
	info("2");

	DPRINTF("terminalParam->supportPSE = %d", terminalParam->supportPSE);
	if (terminalParam->supportPSE)
	{
		info("");
		status=getApplicationByPSE(&terminalParam->CandidateAidList, aidList, PSEPreferredLang);
		info("");
		DPRINTF("status = %d\n", status);
		switch (status)
		{
			case STATUS_CARD_BLOCK:
				EmvIOSessionEnd();
				return STATUS_CARD_BLOCK;
			case STATUS_ERROR:
				EmvIOSessionEnd();
				return STATUS_ERROR;
			case STATUS_FAIL:
				memset(aidList, 0, sizeof(AID_LIST));
				strcpy(PSEPreferredLang, "");
				break;
			default:
				break;
		}
	}

	info("");
	if (aidList->count==0)
	{
		info("");
		status=getApplicationByAidList(&terminalParam->CandidateAidList, aidList);
		info("");
	}

	switch (status)
	{
		case STATUS_CARD_BLOCK:
		case STATUS_ERROR:
		case STATUS_APP_BLOCKED:
			EmvIOSessionEnd();
			return status;
	
		case STATUS_OK:
			sortAidList(aidList);
			if (aidList->count==0)
			{
                ReportLine();
                DPRINTF("aidList->count==0");
				EmvIOSessionEnd();
			}
			DPRINTF("getAidList return ok\n");
			return STATUS_OK;
	
		default:
			return STATUS_FAIL;
	}

}

//------------------------------------------------------------------------------------------
tti_int32 JudgeTransaction(RunningTimeEnv *runningTimeEnv)
{
	tti_byte *pTagValue;
	tti_int32 status;
	int iRet;
	uchar szTmp[100];
	ulong ulAmount;
	
	DPRINTF("judge transaction\n");

	prepareRunningEnv(runningTimeEnv);

	ReportLine();

	if (runningTimeEnv->commonRunningData.forceOnline)
	{
		SetTVR(TVR_FORCE_ONLINE);
	}

	if(runningTimeEnv->commonRunningData.cardHot)
	{
		SetTVR(TVR_CARD_APPEARS_ON_EXECPTION_FILE);
	}

	//Jason moved to here on 2017.05.16
	GetECParaFromICC(runningTimeEnv);
	//End

	//Jason added on 2014.03.17
	runningTimeEnv->cdaIsNeedPerform = FALSE;
	runningTimeEnv->cdaAlreadyPerform = FALSE;
	//runningTimeEnv->cdaIsSecondGacAskAAC=FALSE;
	runningTimeEnv->cdaIsNeedParsedCID = FALSE;		
	//End
	if (!terminalIsOnlineOnly(runningTimeEnv)&&(!runningTimeEnv->offlineAuthWasDone))
	{
		ReportLine();
		//Jason added on 20140314
		if (needToDo(AIP_COMBINED_DYNAMIC_DATA_AUTH)&&terminalSupport(TERM_CAP_CDA))
		{
			ReportLine();
			runningTimeEnv->offlineAuthWasDone=TRUE;
			status = recoveryKeyCDA(runningTimeEnv);
			if (status==STATUS_ERROR)
			{
				ReportLine();
				EmvIOSessionEnd();
				return status;
			}		
			else if (status!=STATUS_OK)
			{
				ReportLine();
				SetTVR(TVR_CDA_FAILED);
				runningTimeEnv->cdaIsNeedPerform=FALSE;
				//Jason added on 2017.05.04
				runningTimeEnv->cdaAlreadyPerform=TRUE;
				//End
			}else
			{
				runningTimeEnv->cdaIsNeedPerform=TRUE;
				//Jason added on 2017.05.04
				runningTimeEnv->cdaAlreadyPerform=TRUE;
				//End
			}
		}
		//End
		else if (needToDo(AIP_DYNAMIC_DATA_AUTH)&&terminalSupport(TERM_CAP_DDA))
		{
			ReportLine();
			runningTimeEnv->offlineAuthWasDone=TRUE;
			status=performDDA(runningTimeEnv);
			if (status==STATUS_ERROR)
			{
				ReportLine();
				EmvIOSessionEnd();
				return status;
			}		
			else if (status!=STATUS_OK)
			{
				ReportLine();
				SetTVR(TVR_DDA_FAILED);
			}
		}
		else if (needToDo(AIP_STATIC_DATA_AUTH)&&terminalSupport(TERM_CAP_SDA))
		{
			ReportLine();
			runningTimeEnv->offlineAuthWasDone=TRUE;
			if (performSDA(runningTimeEnv)!=STATUS_OK)
			{
				ReportLine();
				SetTVR(TVR_SDA_FAILED);
			}
		}else
		{	
			DPRINTF("No SDA and DDA 2\n");
			DPRINTF("needToDo(AIP_DYNAMIC_DATA_AUTH) return %d\n", needToDo(AIP_DYNAMIC_DATA_AUTH));
			DPRINTF("terminalSupport(TERM_CAP_DDA) return %d\n", terminalSupport(TERM_CAP_DDA));
			ReportLine();
		}
	}
	else
	{
		ReportLine();
		DPRINTF("runningTimeEnv->offlineAuthWasDone = %d\n", runningTimeEnv->offlineAuthWasDone);
		DPRINTF("No SDA and DDA\n");
	}

	ReportLine();
	if (!runningTimeEnv->offlineAuthWasDone)
	{
		SetTVR(TVR_OFFLINE_DATA_AUTH_NOT_PERFORM);
	}

	
	

	//Jason and bob edit 20110828
	//if (needToDo(AIP_TERM_RISK_MANAGEMENT))
	//{
		terminalRiskManagement(runningTimeEnv);
	//}

	ProcessingRestrictions(runningTimeEnv);

	//Jason modify on 2013.05.22
	/*
	if (CardHolderVerifyMethod(runningTimeEnv)==STATUS_ERROR)
	{
		EmvIOSessionEnd();
		return STATUS_FAIL;
	}*/
	
       if( gl_ucStandbyCardFlag == 1)
    	{
	       if(iGetAmount(NULL, "请输入金额",NULL, szTmp)<=0)
	      {        	
	      		return 1;
	       }
	        ulAmount = atol(szTmp);
  		  sprintf(szTmp, "金额:%lu.%02lu", ulAmount / 100, ulAmount % 100);
		 gl_TransRec.ulAmount = ulAmount;
	}

	iRet = CardHolderVerifyMethod(runningTimeEnv);
	if (iRet==STATUS_ERROR)
	{
		EmvIOSessionEnd();
		return STATUS_FAIL;
	}else if (iRet == STATUS_CANCEL)
	{
		EmvIOSessionEnd();
		return STATUS_CANCEL;	
	}
	//End

	runningTimeEnv->commonRunningData.emvResultCode=getTerminalActionAnalysis(runningTimeEnv);
	//Jason added on 2012.04.23, started
	if (IsNeedDoECTerminalActionAnalysis(runningTimeEnv) == TRUE)
	{
		runningTimeEnv->commonRunningData.emvResultCode = getECTerminalActionAnalysis(runningTimeEnv);	
	}
	//End
	switch (runningTimeEnv->commonRunningData.emvResultCode)
	{
		case EMV_RESULT_GOOD_OFFLINE:
			SetTagValue(TAG_RESPONSE_CODE, (tti_byte *)("Y1"), RESPONSE_CODE_SIZE, 
				&runningTimeEnv->tagList);
			break;
			
		case EMV_RESULT_REJECT:
			SetTagValue(TAG_RESPONSE_CODE, (tti_byte *)("Z1"), RESPONSE_CODE_SIZE, 
				&runningTimeEnv->tagList);
			//Jason added on 2014.03.12
			if (runningTimeEnv->cdaIsNeedPerform == TRUE)
			{
//Need CDA, but ask AAC.
//Set Set  "Offline Data Auth. Not Performed" to "1" in TVR. 
				runningTimeEnv->cdaIsNeedPerform = FALSE;
				SetTVR(TVR_OFFLINE_DATA_AUTH_NOT_PERFORM);
			}
			//Added end
			break;
	}

	if (firstGenerateAC(runningTimeEnv, runningTimeEnv->commonRunningData.emvResultCode)!=STATUS_OK)
	{
		LOGD("firstGenerateAC ERR");
		EmvIOSessionEnd();
		return STATUS_FAIL;
	}

	//Jason added;
	DPRINTF("Before getECBalanceAfterGenerateAC()\n");
	getECBalanceAfterGenerateAC(runningTimeEnv);
	DPRINTF("After getECBalanceAfterGenerateAC()\n");
	//end
	
	pTagValue=GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA);
	if (pTagValue==NULL)
	{
		EmvIOSessionEnd();
		return STATUS_FAIL;
	}

//refuse if "service not allow"
	if (((*pTagValue)&0x07)==0x01)
	{
		//Changed by Siken 2011.09.24 for "not allowed" displaying
		//runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_SERVICE_NOT_ALLOWED;
		if(((*pTagValue)&0xf0)==0x40)
		{
			runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_SERVICE_NOT_ALLOWED_WITH_TC;
		}
		else
		{
			runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_SERVICE_NOT_ALLOWED;
		}
		//End change
	
		EmvIOSessionEnd();
		return STATUS_OK;
	}

	switch ((*pTagValue)&0xc0)
	{
		case CID_TC:
			runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_GOOD_OFFLINE;
			//Jason added on 2014.03.13
			if (runningTimeEnv->cdaIsNeedPerform == TRUE)
			{
				//When CDA, check if GAC response Tamper '0x77', if not, just terminate 
				if (runningTimeEnv->cdaIsGacRspTmp77 != TRUE)
				{
					EmvIOSessionEnd();
					return STATUS_FAIL;
				}
			
				//Perform CDA, not need do CDA again
				status = performCDA(runningTimeEnv, TRUE);
				runningTimeEnv->cdaIsNeedPerform = FALSE;
				if (status != STATUS_OK)
				{
					SetTVR(TVR_CDA_FAILED);
					runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_REJECT;
					if (runningTimeEnv->cdaIsNeedParsedCID == TRUE)
					{
						//Parsed CID is different with TAG CID, according parsed CID to do 2nd GAC or not 				
						switch ((runningTimeEnv->cdaParsedCID[0])&0xc0)
						{
							case CID_ARQC:
								SetTagValue(TAG_RESPONSE_CODE, (tti_byte *)("Z1"), RESPONSE_CODE_SIZE, 
									&runningTimeEnv->tagList);
								if (secondGenerateAC(runningTimeEnv, 
								runningTimeEnv->commonRunningData.emvResultCode)!=STATUS_OK)
								{
									EmvIOSessionEnd();
									return STATUS_ERROR;
								}
								break;
							default:
								break;
						}	
					}
					//Parsed CID is same with TAG CID, no need do 2nd GAC
				}
			}
			//End
				
			EmvIOSessionEnd();
			break;
		case CID_ARQC:
			if (terminalCanGoOnline(runningTimeEnv))
			{
				runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_ONLINE;

				//Jason added on 2014.03.13
				if (runningTimeEnv->cdaIsNeedPerform == TRUE)
				{
					//When CDA, check if GAC response Tamper '0x77', if not, just terminate 
					if (runningTimeEnv->cdaIsGacRspTmp77 != TRUE)
					{
						EmvIOSessionEnd();
						return STATUS_FAIL;
					}

					//Perform CDA after 1st GAC
					status = performCDA(runningTimeEnv, TRUE);
					if (status != STATUS_OK)
					{
						//CDA failed
						runningTimeEnv->cdaIsNeedPerform=FALSE;
						SetTVR(TVR_CDA_FAILED);

						runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_REJECT;
								
						if (runningTimeEnv->cdaIsNeedParsedCID == TRUE)
						{
							//Parsed CID is different with TAG CID, according parsed CID to do 2nd GAC or not 
							switch ((runningTimeEnv->cdaParsedCID[0])&0xc0)
							{
								case CID_ARQC:
									SetTagValue(TAG_RESPONSE_CODE, (tti_byte *)("Z1"), RESPONSE_CODE_SIZE, 
										&runningTimeEnv->tagList);
								
									if (secondGenerateAC(runningTimeEnv, 
										runningTimeEnv->commonRunningData.emvResultCode)!=STATUS_OK)
									{
										EmvIOSessionEnd();
										return STATUS_ERROR;
									}
									break;
								default:
									break;
							}		
						}else
						{
							//Parsed CID is same with TAG CID, need do 2nd GAC
							SetTagValue(TAG_RESPONSE_CODE, "Z1", RESPONSE_CODE_SIZE, 
									&runningTimeEnv->tagList);
							if (secondGenerateAC(runningTimeEnv, 
								runningTimeEnv->commonRunningData.emvResultCode)!=STATUS_OK)
							{
								EmvIOSessionEnd();
								return STATUS_ERROR;
							}
						}
					}else
					{
						//CDA ok, need do CDA in 2nd GAC
						ClearTVR(CLEAR_TVR_OFFLINE_DATA_AUTH_NOT_PERFORM);
					}
				}
				//End
			}
			else
			{
				runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_COMM_ERROR;
				return TransactionComplete(runningTimeEnv);
			}
			break;
		case CID_AAC:
		case CID_AAR:
			runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_REJECT;
			EmvIOSessionEnd();

			//Jason added on 2014.03.13
			if (runningTimeEnv->cdaIsNeedPerform == TRUE)
			{
			//Ask CDA, but response AAC. not need do CDA again.
				runningTimeEnv->cdaIsNeedPerform = FALSE;
			}
			//End
			break;
		default:
			return STATUS_ERROR;
	}

	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 buildScriptTagList(tti_byte *script, tti_int32 scriptLen, TagList *scriptTagList)
{
	tti_uint16 Tag;
	tti_uint16	TagSize;
	tti_uint16 lengthSize;
	tti_uint16	Length;
	tti_byte *CurPos, *EndPos;
	
	if ((script==NULL)||(scriptLen==0))
	{
		return STATUS_OK;
	}

	CurPos=script;
	EndPos=(script+scriptLen);

	while(CurPos < EndPos)
	{
		if ((*CurPos==0x00)||(*CurPos==0xFF))
		{
			CurPos++;
			continue;
		}
		if(STATUS_OK != ParseTlvInfo(CurPos, (tti_uint16)(EndPos-CurPos), &Tag, &TagSize, &Length, &lengthSize))
		{
			return STATUS_FAIL;
		}
		CurPos += TagSize + lengthSize;

		if (TagIsTemplate(Tag))
		{
			if(STATUS_OK != buildScriptTagList(CurPos, Length, scriptTagList))
			{
				return STATUS_FAIL;
			}
		}
		else
		{
			if ((Tag!=TAG_SCRIPT)&&(Tag!=TAG_SCRIPT_ID))
			{
				return STATUS_FAIL;
			}
			
			if (STATUS_OK!=AppendTagValue(Tag, CurPos, Length, scriptTagList))
			{
				return STATUS_FAIL;
			}
		}
		
		CurPos += Length;
	}
	
	if(CurPos != EndPos)
	{
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 handleScripts(RunningTimeEnv *runningTimeEnv, tti_int32 handlePlace)
{
	tti_uint16 scriptTag;
	tti_int32	index, tagIndex, scriptIndex;
	TagList	scriptTagList;
	//tti_int32	scriptHandleStatus, 
	tti_int32 scriptTotalLen;

	DPRINTF("handle scripts\n");
	
	switch (handlePlace)
	{
		case HANDLE_BEFORE_GC:
			scriptTag=TAG_TEMPERATE_71;
			break;
		case HANDLE_AFTER_GC:
			scriptTag=TAG_TEMPERATE_72;
			break;
		default:
			return STATUS_FAIL;
	}

	InitTagList(&scriptTagList);
	
	scriptTotalLen=0;
	
	for (index=0; index<runningTimeEnv->scriptsList.scriptsNumber; index++)
	{
		//DPRINTF("Script %d\n", index);
		//printByteArray(runningTimeEnv->scriptsList.scripts[index].scriptValue, runningTimeEnv->scriptsList.scripts[index].scriptLen);
		if (runningTimeEnv->scriptsList.scripts[index].scriptValue[0]!=scriptTag)
		{
			DPRINTF("Now need [%02x], but current Scprint is [%02x]\n", scriptTag, 
				runningTimeEnv->scriptsList.scripts[index].scriptValue[0]);
			continue;
		}
		
		SetTSI(TSI_SCRIPT_PROCESS_PERFORMED);
		scriptIndex=0;
		
		scriptTotalLen+=runningTimeEnv->scriptsList.scripts[index].scriptLen;

		if ((buildScriptTagList(runningTimeEnv->scriptsList.scripts[index].scriptValue, 
			runningTimeEnv->scriptsList.scripts[index].scriptLen, &scriptTagList)!=STATUS_OK)
			||(scriptTotalLen>128))
		{
			runningTimeEnv->scriptsList.scriptResults[index].scriptResult[0]=0x00;
			DPRINTF("Analyze the script failed.\n");
		}
		else
		{
			//PrintOutTagList(&scriptTagList, "scriptTagList list");
			runningTimeEnv->scriptsList.scriptResults[index].scriptResult[0]=0x20;
						
			for (tagIndex=0; tagIndex<scriptTagList.ItemCount; tagIndex++)
			{
				DPRINTF("apdu script\n");
			//	gl_TransRec.ucScriptFlag = 1;
				printByteArray(scriptTagList.tagItem[tagIndex].Value, 
						scriptTagList.tagItem[tagIndex].Length);
				if (scriptTagList.tagItem[tagIndex].Tag==TAG_SCRIPT)
				{
					if (scriptIndex<0x0F)
					{
						scriptIndex++;
					}
					if (SmartCardSendScript(scriptTagList.tagItem[tagIndex].Value, 
						scriptTagList.tagItem[tagIndex].Length)!=STATUS_OK)
					{
						DPRINTF("Script Failed.\n");
						runningTimeEnv->scriptsList.scriptResults[index].scriptResult[0]=
							0x10|(tti_byte)scriptIndex;
						break;
						
					}
				}
			}
		}

		if (runningTimeEnv->scriptsList.scriptResults[index].scriptResult[0]!=0x20)
		{
			if (handlePlace==HANDLE_BEFORE_GC)
			{
				SetTVR(TVR_SCRIPT_FAILED_BEFORE_FINAL_GC);
			}
			else
			{
				SetTVR(TVR_SCRIPT_FAILED_AFTER_FINAL_GC);
			}
		}

		if (!TagDataIsMissing(&scriptTagList, TAG_SCRIPT_ID))
		{
			memcpy(&runningTimeEnv->scriptsList.scriptResults[index].scriptResult[1],
				GetTagValue(&scriptTagList, TAG_SCRIPT_ID), 
				GetTagValueSize(&scriptTagList, TAG_SCRIPT_ID));
			runningTimeEnv->scriptsList.scriptResults[index].scriptResultLen=
				GetTagValueSize(&scriptTagList, TAG_SCRIPT_ID)+1;
		}
		else
		{
			memcpy(&runningTimeEnv->scriptsList.scriptResults[index].scriptResult[1], "\x00\x00\x00\x00", 4);
			runningTimeEnv->scriptsList.scriptResults[index].scriptResultLen=4+1;
		}

		FreeTagList(&scriptTagList);
	}

	return STATUS_OK;
}
#ifdef VPOS_APP
extern void vSetTransRevFlag(int flag);
#endif
//------------------------------------------------------------------------------------------
tti_int32 TransactionComplete(RunningTimeEnv *runningTimeEnv)
{
	tti_int32 status;
	tti_byte *pTagValue;
	
	
	DPRINTF("transaction complete\n");
	DPRINTF("Issuer Auth Data:");
	printByteArray(GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_AUTH_DATA) ,GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_AUTH_DATA));

#ifdef APP_CJT          //盛迪嘉DIPOS版,不校验发卡行授权响应密文
    {
        //_vDisp(2, "DiPos, Not Check IssAuthData");
        //_vDelay(200);
       if(_uiTestCard(0) == 1)  //检测到卡就直接交易成功 
       {
	        DPRINTF("****SDJ_DI_POS Ver, Not Check IssuerAuthData***\n");
	        runningTimeEnv->commonRunningData.emvResultCode=STATUS_OK;
	        EmvIOSessionEnd();
	        return STATUS_OK;
	}	
    }
#endif
	if (IsSupportIssuerAuth(runningTimeEnv)&&TagIsExisted(&runningTimeEnv->tagList, TAG_ISSUER_AUTH_DATA)
		&&(GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_AUTH_DATA)>0))
	{
		if (SmartCardExternalAuth(GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_AUTH_DATA),
			GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_AUTH_DATA)) != STATUS_OK)
		{
			SetTVR(TVR_ISSUER_AUTH_FAILED);
            
            #if 0//def VPOS_APP            
            //银联规范：在POS设备上，如果在联机过程中发卡行批准交易，但由于发卡行认证失败卡片拒绝交易，终端则应向后台发送冲正报文
            if(gl_TransRec.ucUploadFlag==0xFF && gl_TransRec.uiTransType!=TRANS_TYPE_BALANCE && gl_TransRec.uiTransType!=TRANS_TYPE_REFUND)
            {
                vSetTransRevFlag(1);
            }
            #endif
		if(gl_SysInfo.ucDoReverseFlag == 1)	
			vSetTransRevFlag(1);
		}
		
		SetTSI(TSI_ISSUER_AUTH_PERFORMED);
	}
	else
	{
		DPRINTF("External No Need perform.\n");
	}

	handleScripts(runningTimeEnv, HANDLE_BEFORE_GC);

	if (runningTimeEnv->commonRunningData.emvResultCode==EMV_RESULT_COMM_ERROR 
		//Jason edit 20110905
		||runningTimeEnv->commonRunningData.emvResultCode==EMV_RESULT_INVALID_ARC
		//End
		)
	{
#ifdef VPOS_APP
        runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_REJECT;
			SetTagValue(TAG_RESPONSE_CODE, "Z3", RESPONSE_CODE_SIZE, 
				&runningTimeEnv->tagList);  
#else
		if (terminalIsOnlineOnly(runningTimeEnv))
		{
			runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_REJECT;
			SetTagValue(TAG_RESPONSE_CODE, "Z3", RESPONSE_CODE_SIZE, 
				&runningTimeEnv->tagList);
		}
		else
		{
			runningTimeEnv->commonRunningData.emvResultCode=getSecondTerminalActionAnalysis(runningTimeEnv);
			DPRINTF("When comm err : runningTimeEnv->commonRunningData.emvResultCode==[%d]\n",
				runningTimeEnv->commonRunningData.emvResultCode);	
			if (runningTimeEnv->commonRunningData.emvResultCode==EMV_RESULT_GOOD_OFFLINE)
			{
				SetTagValue(TAG_RESPONSE_CODE, (tti_byte *)("Y3"), RESPONSE_CODE_SIZE, 
					&runningTimeEnv->tagList);
			}
			else
			{
				SetTagValue(TAG_RESPONSE_CODE, (tti_byte *)("Z3"), RESPONSE_CODE_SIZE, 
					&runningTimeEnv->tagList);
			}
		}
#endif        
	}
	else
	{
		DPRINTF("runningTimeEnv->commonRunningData.emvResultCode==[%d].\n",
				runningTimeEnv->commonRunningData.emvResultCode);
		//GetTagValue(&runningTimeEnv->tagList,TAG_RESPONSE_CODE)
	}
	
	//Jason added on 2014/03/13
	if (runningTimeEnv->cdaIsNeedPerform == TRUE)
	{
		if (runningTimeEnv->commonRunningData.emvResultCode != EMV_RESULT_GOOD_OFFLINE)
		{
            DPRINTF("emvResultCode-->cdaIsNeedPerform FALSE\n");
			runningTimeEnv->cdaIsNeedPerform = FALSE;	
		}
	}
	//End

	pTagValue=GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA);
	if (pTagValue)
        DPRINTF("BF secondGenerateAC, runningTimeEnv TAG_CRYPTOGRAM_INFO_DATA=[%02X]\n", *pTagValue);  	
    
	if (secondGenerateAC(runningTimeEnv, 
		runningTimeEnv->commonRunningData.emvResultCode)!=STATUS_OK)
	{
		if(gl_SysInfo.ucDoReverseFlag == 1)
			vSetTransRevFlag(1);
		EmvIOSessionEnd();
		return STATUS_ERROR;
	}

	pTagValue=GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA);
	if (pTagValue)
        DPRINTF("AF runningTimeEnv TAG_CRYPTOGRAM_INFO_DATA=[%02X]\n", *pTagValue);    
    
	getECBalanceAfterGenerateAC(runningTimeEnv);
	
	handleScripts(runningTimeEnv, HANDLE_AFTER_GC);

	pTagValue=GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA);
	if (pTagValue==NULL)
	{
		EmvIOSessionEnd();
		return STATUS_FAIL;
	}

    DPRINTF("runningTimeEnv TAG_CRYPTOGRAM_INFO_DATA=[%02X]\n", *pTagValue);
	if (((*pTagValue)&0x07)==0x01)
	{
        DPRINTF("pTagValue-->EMV_RESULT_SERVICE_NOT_ALLOWED\n");
		runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_SERVICE_NOT_ALLOWED;
		EmvIOSessionEnd();
		return STATUS_OK;
	}

	if (!isCorrectPrior(cid[runningTimeEnv->commonRunningData.emvResultCode], (*pTagValue)&0xc0))
	{
        DPRINTF("isCorrectPrior-->EMV_RESULT_REJECT\n");
		runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_REJECT;
	}
	else
	{
#if 0//def VPOS_APP     //xfdebug
	runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_GOOD_OFFLINE;
#else
		switch ((*pTagValue)&0xc0)
		{
			case CID_TC:
				runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_GOOD_OFFLINE;
				//Jason added on 2014/03/13
				if (runningTimeEnv->cdaIsNeedPerform == TRUE)
				{
					status = performCDA(runningTimeEnv, FALSE);
					if (status != STATUS_OK)
					{
                        DPRINTF("performCDA-->EMV_RESULT_REJECT\n");
						SetTVR(TVR_CDA_FAILED);
						runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_REJECT;
					}
				}
				//End			
				break;
			case CID_AAC:
			case CID_AAR:
			case CID_ARQC:
                DPRINTF("pTagValue-->EMV_RESULT_REJECT:%02X\n",(*pTagValue)&0xc0);
				runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_REJECT;
				break;
			default:
                DPRINTF("pTagValue default-->STATUS_ERROR:%02X\n",(*pTagValue)&0xc0);
				return STATUS_ERROR;
		}
#endif		
	}

	EmvIOSessionEnd(); 
	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 EndTransaction(RunningTimeEnv *runningTimeEnv)
{
	FreeTagList(&(runningTimeEnv->tagList));
 	EmvIOSessionEnd();

	return STATUS_OK;
}


//Jason added qPBOC logic on 2017.01.12

//------------------------------------------------------------------------------------------
tti_int32 qpbocCheckADFEntryFormat(tti_byte *message, tti_int32 msgLen, AID_INFO *aidInfo)
{
	TagList tempTagList;
	tti_int32 index, tagLen;

	//DPRINTF("chekc qpboc adf entry format\n");
	
	InitTagList(&tempTagList);

	//hexdumpEx("List message", message, msgLen);
	
	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		ReportLine();
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	//PrintOutTagList(&tempTagList, "qpbocCheckADFEntryFormat taglist");

	if (!TagIsExisted(&tempTagList, TAG_ADF_NAME))
	{
		ReportLine();
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	tagLen=GetTagValueSize(&tempTagList, TAG_ADF_NAME);
	if ((tagLen<7)||(tagLen>16))
	{
		ReportLine();
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagDataIsMissing(&tempTagList, TAG_APP_PRIOR))
	{
		if (GetTagValueSize(&tempTagList, TAG_APP_PRIOR)!=1)
		{
			ReportLine();
			FreeTagList(&tempTagList);
			return STATUS_FAIL;
		}
	}

	for (index=0; index<tempTagList.duplicateItemCount; index++)
	{
		switch (tempTagList.duplicateItem[index])
		{
			case TAG_ADF_NAME:
			case TAG_APP_LABEL:
			case TAG_APP_PRIOR:
				ReportLine();
				FreeTagList(&tempTagList);
				return STATUS_FAIL;

			default:
				break;
		}
	}

	memcpy(aidInfo->aidName, GetTagValue(&tempTagList, TAG_ADF_NAME), 
		GetTagValueSize(&tempTagList, TAG_ADF_NAME));
	aidInfo->aidLength = GetTagValueSize(&tempTagList, TAG_ADF_NAME);

	if (!TagDataIsMissing(&tempTagList, TAG_APP_PRIOR))
	{
		memcpy(&(aidInfo->priority), GetTagValue(&tempTagList, TAG_APP_PRIOR), 
			GetTagValueSize(&tempTagList, TAG_APP_PRIOR));
	}else
	{
		memcpy(&(aidInfo->priority), "\xFF", 1);
	}

	if (!TagDataIsMissing(&tempTagList, TAG_APP_LABEL))
	{
		memcpy(aidInfo->aidLabel , GetTagValue(&tempTagList, TAG_APP_LABEL), 
			GetTagValueSize(&tempTagList, TAG_APP_LABEL));
		aidInfo->aidLabel[GetTagValueSize(&tempTagList, TAG_APP_LABEL)] = 0x00;
	}

	FreeTagList(&tempTagList);

	//hexdumpEx("aidInfo->aidName", aidInfo->aidName, aidInfo->aidLength);
	//hexdumpEx("aidInfo->priority", &(aidInfo->priority), 1);
	
	return STATUS_OK;
}

extern AID_LIST aidList;

//------------------------------------------------------------------------------------------
tti_int32 qpbocCheckFCIIssuerData(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 status;
	//tti_int32 tmpLen;
	tti_int32 index;
	AID_LIST tempAidList;

	memset(&tempAidList, 0, sizeof(AID_LIST));
	memset(&aidList, 0, sizeof(AID_LIST));

	//DPRINTF("check qpboc FCI issuer data\n");
	//hexdumpEx("List Tamp BF0C", message, msgLen);
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		REPORT_ERR_LINE();
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	//dwatch(tempTagList.duplicateItemCount);

	if (!TagIsExisted(&tempTagList, TAG_ENTRY_TEMPLATE))
	{
		REPORT_ERR_LINE();
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	tempAidList.count = 0;
	//PrintOutTagList(&tempTagList, "FCI issuer temp tag list");
	for (index = 0; index < tempTagList.ItemCount; index ++)
	{
		//if (tempTagList.tagItem[index].Tag = TAG_ENTRY_TEMPLATE)
		if (tempTagList.tagItem[index].Tag == TAG_ENTRY_TEMPLATE)
		{
			//hexdumpEx("List TAG_ENTRY_TEMPLATE buffer", tempTagList.tagItem[index].Value, 
			//	tempTagList.tagItem[index].Length);
			
			status = qpbocCheckADFEntryFormat(tempTagList.tagItem[index].Value,
				tempTagList.tagItem[index].Length, &(tempAidList.aidInfo[tempAidList.count]));
			if (status != STATUS_OK)
			{
				continue;
			}else
			{
				(tempAidList.count)++;
			}
		}
	}
	
	if (tempAidList.count == 0)
	{
		REPORT_ERR_LINE();
		LOGE("tempAidList.count = 0, No select AID");
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	sortAidList(&tempAidList);

	memcpy(&aidList, &tempAidList, sizeof(AID_LIST));
	//PrintOutAidList(&aidList);
	FreeTagList(&tempTagList);

	return STATUS_OK;
}


//------------------------------------------------------------------------------------------
tti_int32 qpbocCheckFCIProprietaryTemplateInDDF(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 status;

	//DPRINTF("check qpboc fci proprietary template in ddf\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		ReportLine();
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&tempTagList, TAG_FCI_ISSUER_DATA))
	{
		FreeTagList(&tempTagList);
		ReportLine();
		return STATUS_FAIL;
	}

	
	status=qpbocCheckFCIIssuerData(GetTagValue(&tempTagList, TAG_FCI_ISSUER_DATA),
		GetTagValueSize(&tempTagList, TAG_FCI_ISSUER_DATA));
	
	//status = STATUS_OK;
	FreeTagList(&tempTagList);

	return status;
}



//------------------------------------------------------------------------------------------
tti_int32 qpbocCheckFCITemplateInDDF(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 status;

	//DPRINTF("check qpboc fci template in ddf\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&tempTagList, TAG_DF_NAME)||
		!TagIsExisted(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}


	status=qpbocCheckFCIProprietaryTemplateInDDF(GetTagValue(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE),
		GetTagValueSize(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE));

	FreeTagList(&tempTagList);

	return status;
}


//------------------------------------------------------------------------------------------
tti_int32 qpbocCheckDDFFormat(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 status;

	//DPRINTF("chekc qpboc ddf format\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&tempTagList, TAG_FCI_TEMPLATE))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	status=qpbocCheckFCITemplateInDDF(GetTagValue(&tempTagList, TAG_FCI_TEMPLATE),
		GetTagValueSize(&tempTagList, TAG_FCI_TEMPLATE));

	FreeTagList(&tempTagList);

	return status;
}


//------------------------------------------------------------------------------------------
tti_int32 qpbocCheckFCITemplateInADF(tti_byte *message, tti_int32 msgLen)
{
    //bob add 20101106 from here
    //TagList tempFullTagList;
	//Bob add 20101106 end

	
	TagList tempTagList;
	tti_int32 index, status;

	//DPRINTF("check qpboc fci template in adf\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (TagDataIsMissing(&tempTagList, TAG_DF_NAME)||
		!TagIsExisted(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE))
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagDataIsMissing(&tempTagList, TAG_PDOL))
	{
		FreeTagList(&tempTagList);
		//return STATUS_FAIL;
		return STATUS_ADF_NO_PDOL;
	}

	for (index=0; index<tempTagList.duplicateItemCount; index++)
	{
		switch (tempTagList.duplicateItem[index])
		{
			case TAG_DF_NAME:
			case TAG_FCI_PROPRIETARY_TEMPLATE:
				FreeTagList(&tempTagList);
				return STATUS_FAIL;
			default:
				break;
		}
	}

/*
	//bob add from here 20101106
	InitTagList(&tempFullTagList);
	if (STATUS_OK == BuildTagList(message, msgLen, &tempFullTagList))
	{
		if( tempFullTagList.duplicateItemCount > 0)
		{
			FreeTagList(&tempFullTagList);
			return STATUS_FAIL;
		}
	}
	FreeTagList(&tempFullTagList);
	//bob add end

	status=checkFCIProprietaryTemplateInADF(GetTagValue(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE),
		GetTagValueSize(&tempTagList, TAG_FCI_PROPRIETARY_TEMPLATE));
*/

	FreeTagList(&tempTagList);

	status = STATUS_OK;

	return status;
}


//------------------------------------------------------------------------------------------
tti_int32 qpbocCheckADFFormat(tti_byte *message, tti_int32 msgLen)
{
	TagList tempTagList;
	tti_int32 status;

	//DPRINTF("chekc qpboc adf format\n");
	
	InitTagList(&tempTagList);

	if (BuildTagListOneLevel(message, msgLen, &tempTagList)!=STATUS_OK)
	{
		ReportLine();
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&tempTagList, TAG_FCI_TEMPLATE))
	{
		ReportLine();
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}

	status=qpbocCheckFCITemplateInADF(GetTagValue(&tempTagList, TAG_FCI_TEMPLATE),
		GetTagValueSize(&tempTagList, TAG_FCI_TEMPLATE));

	FreeTagList(&tempTagList);

	return status;
}

tti_int32 getTagCIDFromTag10(tti_byte *tag10Value, tti_int32 tag10Len, tti_byte *tagCIDValue, tti_int32 *tagCIDLen)
{
	tti_byte b9F27;
	
	//hexdump(tag57Value, tag57Len);
	//hexdump(tagPanValue, *tagPanLen);

	b9F27 = ((tag10Value[4]) & 0x30);		

	if (b9F27 == 0x20)
	{
		//锟斤拷锟斤拷锟斤拷锟斤拷
		//result=Result.Online;
		b9F27 = 0x80;
	}
	else if(b9F27 == 0x00)
	{
		//锟斤拷锟阶拒撅拷
		//result=Result.Decline;
		b9F27 = 0x00;
	}
	else if(b9F27== 0x10)
	{
		//锟斤拷锟斤拷锟窖伙拷锟斤拷准
		//result=Result.OfflineApproveNoAllow;
		b9F27 = 0x40;
	}
	else{
		//result=Result.Terminal;
	}

	*tagCIDValue = b9F27;
	*tagCIDLen = 1;

	DPRINTF("tagCIDValue = %02x\n",b9F27);
	return 0;
}


tti_int32 getPanFromTag57(tti_byte *tag57Value, tti_int32 tag57Len, tti_byte *tagPanValue, tti_int32 *tagPanLen)
{
	tti_int32 i = 0;
	tti_bool half = FALSE;
	
	for(i = 0; i < tag57Len; i++)
	{
		if((tag57Value[i]& 0xd0) == 0xd0)
		{
			break;
		}
		if((tag57Value[i]&0x0d)== 0x0d)
		{
			half = TRUE;
			break;
		}
	}

	if (i >= tag57Len)
	{
		return -1;
	}

	if (half == TRUE)
	{
		memcpy(tagPanValue, tag57Value, i+1);
		tagPanValue[i] = tagPanValue[i] & 0xf0;
		tagPanValue[i] += 0x0f;
		*tagPanLen = i + 1;
	}
	else
	{
		memcpy(tagPanValue, tag57Value, i);
		*tagPanLen = i;
	}

	//hexdump(tag57Value, tag57Len);
	//hexdump(tagPanValue, *tagPanLen);
	
	return 0;
}

tti_int32 getExpDateFromTag57(tti_byte *tag57Value, tti_int32 tag57Len, tti_byte *tagExpDateValue, tti_int32 *tagExpDateLen)
{
	tti_int32 i = 0;
	tti_bool half = FALSE;
	
	for(i = 0; i < tag57Len; i++)
	{
		if((tag57Value[i]& 0xd0) == 0xd0)
		{
			half = TRUE;
			break;
		}
		if((tag57Value[i]&0x0d)== 0x0d)
		{
			
			break;
		}
	}

	if (i >= tag57Len)
	{
		return -1;
	}

	if (half == TRUE)
	{
		tagExpDateValue[0] = (tag57Value[i] << 4);
		tagExpDateValue[0] += (tag57Value[i+1] >> 4);
		tagExpDateValue[1] = (tag57Value[i+1] << 4);
		tagExpDateValue[1] += (tag57Value[i+2] >> 4);
		*tagExpDateLen = 2;
	}
	else
	{
		memcpy(tagExpDateValue, tag57Value + i + 1, 2);
		*tagExpDateLen = 2;
	}

	//hexdump(tag57Value, tag57Len);
	//hexdump(tagExpDateValue, *tagExpDateLen);
	return 0;
}



//------------------------------------------------------------------------------------------

tti_int32 qpbocSelectPPSE()
{
	tti_byte RApdu[300];
	tti_uint16 RApduLen;
	int status = STATUS_FAIL;
	//TagList tempTagList;
	tti_int32 ret;

	
	dbg("Before NfcSmartCardSelect PPSE: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	ret = NfcSmartCardSelect((tti_byte *)(PPSE_NAME), strlen(PPSE_NAME), RApdu, &RApduLen);
	dbg("After NfcSmartCardSelect PPSE: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	if (ret != STATUS_OK)
	{
		DPRINTF("NfcSmartCardSelect Error\n");
		return status;
	}
	
	status = qpbocCheckDDFFormat(RApdu, RApduLen);
	//DPRINTF("qpbocCheckDDFFormat return %d\n", status);
	if (status != STATUS_OK)
	{
		return status;
	}

	return STATUS_OK;

#if 0
	InitTagList(&tempTagList);
	/*
	if (BuildTagList(RApdu, RApduLen, &tempTagList) != STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}
	*/
	if (BuildTagListInMode(RApdu, RApduLen, TAGMODE_DUPLICATE_ITEM_ALLOWED, &tempTagList) != STATUS_OK)
	{
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}
	
	PrintOutTagList(&tempTagList, "List PPSE_NAME Tags");

	memcpy(aid, GetTagValue(&tempTagList, TAG_ADF_NAME), GetTagValueSize(&tempTagList, TAG_ADF_NAME));
	*aidLen = GetTagValueSize(&tempTagList, TAG_ADF_NAME);

	//For test
	aid[GetTagValueSize(&tempTagList, TAG_ADF_NAME) - 1] = 0x01;
	//End

	memcpy(appLabel, GetTagValue(&tempTagList, TAG_APP_LABEL), GetTagValueSize(&tempTagList, TAG_APP_LABEL));
	*appLabelLen = GetTagValueSize(&tempTagList, TAG_APP_LABEL);
	
	FreeTagList(&tempTagList);
	
	return STATUS_OK;
#endif
}



//------------------------------------------------------------------------------------------
tti_int32 qpbocInitTransaction(tti_byte *aid, tti_int16 aidLen, TagList *tagList)
{
	tti_byte RApdu[300];
	tti_uint16 RApduLen;
	int status = STATUS_FAIL;
	TagList tempTagList;
	tti_tchar dataList[BUFFER_SIZE_512BYTE];
	tti_byte dataListLen;
	tti_byte cryptInfoData[2];
	tti_byte issueAppData[30];
	tti_byte tmp;
	//tti_byte tagPANValue[30];
	//tti_byte tagExpDateValue[10];
	//tti_byte tagCIDValue[10];
	//tti_int32 tagPANLen;
	//tti_int32 tagExpDateLen;
	//tti_int32 tagCIDLen;
	tti_int32 loop;
	tti_int32 ret;
	tti_byte tempByteArr[256];
	tti_byte	*sBuff;
	//tti_uint16 tempByteLen;
	tti_uint16 terminalTagArray[] = {
					 0x9f02,
			  		 0x81,   
			  		 0x9f06,
			  		 0x9f15,
			  		 0x9f16,
			  		 0x9f4E,
			  		 0x9f39,
			  		 0x9f1c,
			  		 0x9f33,
			  	     0x95,
			  		 0x9A,
			  		 0x9C,
			  		 0x9B,
			  		 0x5F2A,
			         0x9F37};

	
	dbg("Before NfcSmartCardSelect: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	ret = NfcSmartCardSelect(aid, aidLen, RApdu, &RApduLen);
	if (ret != STATUS_OK)
	{
		DPRINTF("APDU NfcSmartCardSelect Error, TRY NEXT AID\n");
		return STATUS_FAIL;
	}
	dbg("After NfcSmartCardSelect: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));

	status = qpbocCheckADFFormat(RApdu, RApduLen);
	//DPRINTF("qpbocCheckADFFormat return %d\n", status);
	
	dbg("After qpbocCheckADFFormat: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));

	if (status != STATUS_OK)
	{
		REPORT_ERR_LINE();
		DPRINTF("qpbocCheckADFFormat ERR\n");
		return STATUS_FAIL;
	}

	InitTagList(&tempTagList);
	if (BuildTagList(RApdu, RApduLen, &tempTagList) != STATUS_OK)
	{
		REPORT_ERR_LINE();
		FreeTagList(&tempTagList);
		return STATUS_FAIL;
	}
	
	//DPRINTF("tempTagList count is %d\n", tempTagList.ItemCount);
	//DPRINTF("before Append :tagList count is %d\n", tagList->ItemCount);
	
	appendTagList(&tempTagList, tagList);
	//DPRINTF("After Append :tagList count is %d\n", tagList->ItemCount);
	//PrintOutTagList(tagList, "After global tagList");
	FreeTagList(&tempTagList);
	
	//PrintOutTagList(tagList, "tag list before GPO");
	
	if (TagIsExisted(tagList, TAG_PDOL))
	{
		if (tagIsInDOL(tagList, TAG_PDOL, TAG_TERMINAL_TRANSACTION_ATTRIBUTES) == FALSE)
		{
			//LOGE("TAG_TERMINAL_TRANSACTION_ATTRIBUTES NOT in TAG_DDOL, try next AID");
			//return STATUS_ERROR;
			return STATUS_FAIL;
		}
		
		BuildDOLToStream(tagList, TAG_PDOL, (tti_byte *)(dataList+2), &dataListLen);
		
		*dataList='\x83';
		if (dataListLen<128)
		{
			*(dataList+1)=dataListLen;
			dataListLen+=2;
		}
		else
		{
			memmove(dataList+3, dataList+2, dataListLen);
			*(dataList+1)=0x81;
			*(dataList+2)=dataListLen;
			dataListLen+=3;
		}
	}
	else
	{
		//LOGE("No TAG_PDOL, try next AID");
		//REPORT_ERR_LINE();
		//return STATUS_ERROR;
		return STATUS_FAIL;
	}

	//add by chencheng 
	gl_ucOutsideCardFlag = FALSE;
	
	getSpecialDataFromNFC(&runningTimeEnv, TAG_QPBOC_9F51);	
	sBuff=GetTagValue(&runningTimeEnv.tagList, TAG_QPBOC_9F51);
	if (sBuff!=NULL &&  !memcmp(sBuff, "\x01\x56", 2))
	{
		dbgHex("TAG_QPBOC_9F51",sBuff,GetTagValueSize(&runningTimeEnv.tagList,TAG_QPBOC_9F51));
		gl_ucOutsideCardFlag = FALSE;
	}
        else if(sBuff!=NULL && memcmp(sBuff, "\x01\x56", 2) )
        {
        	gl_ucOutsideCardFlag = TRUE;
        }

		
	getSpecialDataFromNFC(&runningTimeEnv, TAG_QPBOC_DF71);	
	sBuff=GetTagValue(&runningTimeEnv.tagList, TAG_QPBOC_DF71);
	if (sBuff!=NULL  &&  !memcmp(sBuff, "\x01\x56", 2))
	{
		dbgHex("TAG_QPBOC_DF71",sBuff,GetTagValueSize(&runningTimeEnv.tagList,TAG_QPBOC_DF71));
		gl_ucOutsideCardFlag = FALSE;
	}
	else if(sBuff!=NULL && memcmp(sBuff, "\x01\x56", 2) )
        {
        	gl_ucOutsideCardFlag = TRUE;
        }
			
	dbg("Before NfcGetprocessOption: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	status=NfcGetprocessOption((tti_byte *)dataList, dataListLen, RApdu, &RApduLen);
	dbg("After NfcGetprocessOption: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	
	//DPRINTF("NfcGetprocessOption return %d\n", status);
	if (status != STATUS_OK)
	{
		//REPORT_ERR_LINE();
		if (status == STATUS_GPO_NO_ACCEPTED)
		{
			//Don't try next AID
			DPRINTF("APDU NfcGetprocessOption Error but STATUS_GPO_NO_ACCEPTED, Don't TRY NEXT AID\n");
			return STATUS_ERROR;
		}else if (status == STATUS_GPO_POWER_RESET_ERR)
		{
			DPRINTF("GPO return 6986, need restart qtransaction.\n");
			return STATUS_GPO_POWER_RESET_ERR;
		}else if (status == STATUS_ERROR)
		{
			DPRINTF("GPO no response, need restart qtransaction .\n");
			return STATUS_GPO_NO_RESPONSE_ERR;
		}
		
		DPRINTF("APDU NfcGetprocessOption Error, NOT TRY NEXT AID\n");
		return STATUS_ERROR;
	}

	if (ParseGPOResponse(RApdu, RApduLen, &tempTagList)!=STATUS_OK)
	{
		REPORT_ERR_LINE();
		DPRINTF("ParseGPOResponse error, Don't TRY NEXT AID\n");
		return STATUS_ERROR;
	}

	//Check data
	if (TagDataIsMissing(&tempTagList, TAG_AIP)||
		//(TagDataIsMissing(&tempTagList, TAG_TRACK2))|| 
		(TagDataIsMissing(&tempTagList, TAG_ATC))||
		(TagDataIsMissing(&tempTagList, TAG_ISSUER_APP_DATA))||
		(TagDataIsMissing(&tempTagList, TAG_CRYPTOGRAM)))
	{
		REPORT_ERR_LINE();
		FreeTagList(&tempTagList);
		return STATUS_ERROR;
	}

#if 0
	memcpy(tempByteArr, GetTagValue(&tempTagList, TAG_ISSUER_APP_DATA), GetTagValueSize(&tempTagList, TAG_ISSUER_APP_DATA));
	if ((tempByteArr[4] & 0x30) == 0x00)
	{
		//decline
		if (TagDataIsMissing(&tempTagList, TAG_TRACK2))
		{
			REPORT_ERR_LINE();
			FreeTagList(&tempTagList);
			return STATUS_ERROR;
		}
	}else if ((tempByteArr[4] & 0x30) == 0x10)
	{
		//offline approve
		if (TagDataIsMissing(&tempTagList, TAG_AFL))
		{
			REPORT_ERR_LINE();
			FreeTagList(&tempTagList);
			return STATUS_ERROR;
		}
	}else if ((tempByteArr[4] & 0x30) == 0x20)
	{
		//online
		if (TagDataIsMissing(&tempTagList, TAG_TRACK2))
		{
			REPORT_ERR_LINE();
			FreeTagList(&tempTagList);
			return STATUS_ERROR;
		}
	}else
	{
		REPORT_ERR_LINE();
		FreeTagList(&tempTagList);
		return STATUS_ERROR;
	}

	for(loop = 0; loop < sizeof(terminalTagArray)/sizeof(terminalTagArray[0]); loop ++)
	{
		removeTag(&tempTagList, terminalTagArray[loop]);	
	}

	appendTagList(&tempTagList, tagList);
	FreeTagList(&tempTagList); 
#else
	if (TagIsExisted(&tempTagList, TAG_CRYPTOGRAM_INFO_DATA) == FALSE )
	{
		DPRINTF("No TAG_CRYPTOGRAM_INFO_DATA\n");
		//memset(cryptInfoData, 0x00, sizeof(cryptInfoData));
		memcpy(issueAppData, GetTagValue(&tempTagList, TAG_ISSUER_APP_DATA), 
			GetTagValueSize(&tempTagList, TAG_ISSUER_APP_DATA));
		//tmp = issueAppData[4] & 0x30;
		cryptInfoData[0] = (issueAppData[4] & 0x30) << 2;
		//memcpy(cryptInfoData, issueAppData+4, 1);
		//hexdumpEx("List issueAppData", issueAppData, 5);
		//hexdumpEx("List cryptInfoData", cryptInfoData, 1);
		SetTagValue(TAG_CRYPTOGRAM_INFO_DATA, cryptInfoData, 1, &tempTagList);
	}else if(GetTagValueSize(&tempTagList, TAG_CRYPTOGRAM_INFO_DATA) == 0)
	{
		DPRINTF("TAG_CRYPTOGRAM_INFO_DATA size is 0\n");
		memset(cryptInfoData, 0x00, sizeof(cryptInfoData));
		memcpy(issueAppData, GetTagValue(&tempTagList, TAG_ISSUER_APP_DATA), 
			GetTagValueSize(&tempTagList, TAG_ISSUER_APP_DATA));
		tmp = issueAppData[4] & 0x30;
		cryptInfoData[0] = tmp << 2;
		//memcpy(cryptInfoData, issueAppData+4, 1);
		hexdumpEx("List issueAppData", issueAppData, 5);
		hexdumpEx("List cryptInfoData", cryptInfoData, 1);
		SetTagValue(TAG_CRYPTOGRAM_INFO_DATA, cryptInfoData, 1, &tempTagList);
	}
	else
	{
		DPRINTF("TAG_CRYPTOGRAM_INFO_DATA Exist\n");
		memcpy(cryptInfoData, GetTagValue(&tempTagList, TAG_CRYPTOGRAM_INFO_DATA), 
			GetTagValueSize(&tempTagList, TAG_CRYPTOGRAM_INFO_DATA));
	}

	ret = qpbocParseCryptInfoData(cryptInfoData);
	if (ret == QPBOC_CID_STATUS_AAC)
	//if ((tempByteArr[0] & 0xC0) == 0x00)
	{
		//decline
		if (TagDataIsMissing(&tempTagList, TAG_TRACK2))
		{
			REPORT_ERR_LINE();
			FreeTagList(&tempTagList);
			return STATUS_ERROR;
		}
	//}else if ((tempByteArr[0] & 0xC0) == 0x40)
	}else if (ret == QPBOC_CID_STATUS_TC)
	{
		//offline approve
		if (TagDataIsMissing(&tempTagList, TAG_AFL))
		{
			REPORT_ERR_LINE();
			FreeTagList(&tempTagList);
			return STATUS_ERROR;
		}
	//}else if ((tempByteArr[0] & 0xC0) == 0x80)
	}else if (ret == QPBOC_CID_STATUS_ARQC)
	{
		//online
		if (TagDataIsMissing(&tempTagList, TAG_TRACK2))
		{
			REPORT_ERR_LINE();
			FreeTagList(&tempTagList);
			return STATUS_ERROR;
		}
	}else
	{
		REPORT_ERR_LINE();
		FreeTagList(&tempTagList);
		return STATUS_ERROR;
	}

	for(loop = 0; loop < sizeof(terminalTagArray)/sizeof(terminalTagArray[0]); loop ++)
	{
		removeTag(&tempTagList, terminalTagArray[loop]);	
	}

	appendTagList(&tempTagList, tagList);
	FreeTagList(&tempTagList); 
#endif
	//SetTagValue(TAG_ADF_NAME, aid, aidLen, tagList);
	//SetTagValue(TAG_TERMINAL_AID, aid, aidLen, tagList);

	return STATUS_OK;
}

void nfcLedRemoveShow(void)
{
/*
	DisplayNFCLedIcon(LCD_ICON_NFCLED_BLUE, NFCLED_DISPLAY_STATUS_ON);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_YELLOW, NFCLED_DISPLAY_STATUS_ON);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_GREEN, NFCLED_DISPLAY_STATUS_ON);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_RED, NFCLED_DISPLAY_STATUS_OFF);

	BuzzerOn(100);
*/
    #ifdef VPOS_APP
    _vSetAllLed(1, 1, 1, 0);
    #endif 
}

void nfcLedSuccessShow(void)
{
/*
	DisplayNFCLedIcon(LCD_ICON_NFCLED_BLUE, NFCLED_DISPLAY_STATUS_ON);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_YELLOW, NFCLED_DISPLAY_STATUS_ON);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_GREEN, NFCLED_DISPLAY_STATUS_ON);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_RED, NFCLED_DISPLAY_STATUS_OFF);

	BuzzerOn(100);
*/
    #ifdef VPOS_APP
    _vSetAllLed(1, 1, 1, 0);
    #endif     
}

void nfcLedFailedShow(void)
{
/*
	DisplayNFCLedIcon(LCD_ICON_NFCLED_BLUE, NFCLED_DISPLAY_STATUS_OFF);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_YELLOW, NFCLED_DISPLAY_STATUS_OFF);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_GREEN, NFCLED_DISPLAY_STATUS_OFF);
	DisplayNFCLedIcon(LCD_ICON_NFCLED_RED, NFCLED_DISPLAY_STATUS_ON);

	BuzzerOn(100);
*/
    #ifdef VPOS_APP
    _vSetAllLed(0, 0, 0, 1);
    #endif 
}


#define ONLINE_DES_KEY	"12345678"

tti_int32 GetPINEnterFromUI(tti_tchar *szDisplayString, tti_tchar *szPINValue);

tti_int32 qpbocOnlinePin(RunningTimeEnv *runningTimeEnv)
{
	
	int iRet;
	tti_tchar pinNumber[PIN_NUMBER_SIZE+1] = {0};
	tti_byte PinBlock[30] = {0};
	
	//for debug
	tti_byte tmp1[50] = {0};
	//tti_byte tmp2[50] = {0};
	tti_tchar szDisplayPrompt[50];

	DPRINTF("online encipher pin\n");
#ifndef VPOS_APP
	strcpy(szDisplayPrompt, "请输入PIN");

	while (TRUE)
	{
		iRet = GetPINEnterFromUI(szDisplayPrompt, pinNumber);
		if (iRet ==STATUS_OK && strlen(pinNumber) == 0)
		{
			return CVM_RESULT_FAIL;
		}
		if (iRet == STATUS_CANCEL)
		{
			return CVM_RESULT_FAIL;
		}
		if (iRet == STATUS_OK && strlen(pinNumber) != 0)
		{
			break;
		}
	}

	BuildPinBlock((tti_byte*)pinNumber, strlen(pinNumber), PinBlock);
	ucl_desEncrypt(PinBlock, (tti_byte*)(ONLINE_DES_KEY), tmp1);
    SetTagValue(TAG_ONLINE_PIN_BLOCK_BCTC, tmp1, 8, 
		&(runningTimeEnv->tagList));    
#else
    {
 //       extern tti_int32 onlineEncipherPin(RunningTimeEnv *runningTimeEnv);
        
        return onlineEncipherPin(runningTimeEnv);    
    }
#endif

	
	return CVM_RESULT_UNKNOWN;	
}

#if 0
tti_int32 qpbocPerformCvm(RunningTimeEnv *runningTimeEnv)
{
	tti_byte cardAuthData[20];
	tti_byte termTransAttribute[20];

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_TERMINAL_TRANSACTION_ATTRIBUTES) == TRUE)
	{
		LOGE("NO TAG_TERMINAL_TRANSACTION_ATTRIBUTES");
		return STATUS_FAIL;
	}else
	{
		memcpy(termTransAttribute, GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_TRANSACTION_ATTRIBUTES), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_TERMINAL_TRANSACTION_ATTRIBUTES));
	}

	runningTimeEnv->qpbocCvmSign = FALSE;
	
	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES) == TRUE)
	{
		LOGD("TAG_CARD_TRANSACTION_ATTRIBUTES is miss");
		if ((termTransAttribute[0] & 0x02) != 0x00 && (termTransAttribute[1] & 0x40) != 0x00)
		{
			LOGD("NO TAG_CARD_TRANSACTION_ATTRIBUTES, Set runningTimeEnv->qpbocCvmSign = TRUE");
			runningTimeEnv->qpbocCvmSign = TRUE;
		}
		
	}else
	{
		memcpy(cardAuthData, GetTagValue(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES));
		if ((cardAuthData[0] & 0x80) != 0x00)
		{
			//CVM Online PIN
			qpbocOnlinePin(runningTimeEnv);
			
		}else if ((cardAuthData[0] & 0x40) != 0x00)
		{
			//CVM signature
			if ((termTransAttribute[0] & 0x02) != 0x00)
			{
				LOGD("TAG_QPBOC_CARD_AUTH_DATA set bit, Set runningTimeEnv->qpbocCvmSign = TRUE");
				runningTimeEnv->qpbocCvmSign = TRUE;
			}
		}else
		{
			return STATUS_FAIL;
		}
	}

	return STATUS_OK;
}
#else

tti_int32 qpbocParseCryptInfoData(tti_byte *cryptInfoData)
{
	//dbg("cryptInfoData[0] = %02x\n", cryptInfoData[0]);
/*
	if (cryptInfoData[0] == 0x80)
	{
		////Line;
		DPRINTF("QPBOC_CID_STATUS_ARQC");
		return QPBOC_CID_STATUS_ARQC;
	}
*/
	if (((cryptInfoData[0]&0x80) != 0x00) && ((cryptInfoData[0]&0x40) == 0x00))
	{
		//Bit 8 and Bit 7 is '10'
		//Go to online
		////Line;
		//DPRINTF("QPBOC_CID_STATUS_ARQC");
		return QPBOC_CID_STATUS_ARQC;
	}else if (((cryptInfoData[0]&0x80) == 0x00) && ((cryptInfoData[0]&0x40) == 0x00))
	{
		//Bit 8 and Bit 7 is '00'
		//Go to decline
		//DPRINTF("QPBOC_CID_STATUS_AAC");
		////Line;
		return QPBOC_CID_STATUS_AAC;
	}else if (((cryptInfoData[0]&0x80) == 0x00) && ((cryptInfoData[0]&0x40) != 0x00))
	{
		//Bit 8 and Bit 7 is '01'
		//Go to offline approve.
		//DPRINTF("QPBOC_CID_STATUS_TC");
		////Line;
		return QPBOC_CID_STATUS_TC;
	}else
	{
		//Other case, error
		//DPRINTF("QPBOC_CID_STATUS_ERR");
		////Line;
		return QPBOC_CID_STATUS_ERR;
	}
}

tti_int32 qpbocPerformCvm(RunningTimeEnv *runningTimeEnv)
{
	tti_byte cryptInfoData[20];
	tti_byte cardAuthData[20];
	tti_byte cardTransAttribute[20];
	tti_byte termTransAttribute[20];
	tti_int32 ret;

	FUNCIN;

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_TERMINAL_TRANSACTION_ATTRIBUTES) == TRUE)
	{
		LOGE("NO TAG_TERMINAL_TRANSACTION_ATTRIBUTES");
		return STATUS_FAIL;
	}else
	{
		LOGD("TAG_TERMINAL_TRANSACTION_ATTRIBUTES exist");
		memcpy(termTransAttribute, GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_TRANSACTION_ATTRIBUTES), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_TERMINAL_TRANSACTION_ATTRIBUTES));
	}

	hexdumpEx("List termTransAttribute", termTransAttribute, 3);
	runningTimeEnv->qpbocCvmSign = FALSE;

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES) == TRUE)
	{
		//TAG_CARD_TRANSACTION_ATTRIBUTES Not exist
		dbg("TAG_CARD_TRANSACTION_ATTRIBUTES is missed\n");
		dbg("Card no response 9F6C\n");
		if ((termTransAttribute[1] & 0x40) == 0x00)
		{	
			////Line;
			dbg("9F66 don't ask CVM and card no response 9F6C, perform no cvm\n");
			return STATUS_OK;
		}else if ((termTransAttribute[0] & 0x02) != 0x00)
		{
			dbg("Terminal support sign, CVM excute sign\n");
			runningTimeEnv->qpbocCvmSign = TRUE;
		}else if ((termTransAttribute[2] & 0x40) != 0x00 && (termTransAttribute[0] & 0x04) == 0x00)
		{
			dbg("Terminal support CDCVM only, decline\n");
			////Line;
			return STATUS_QPBOC_CVM_DECLINE;
		}else if((termTransAttribute[2] & 0x40) != 0x00 && (termTransAttribute[0] & 0x04) != 0x00)
		{
			dbg("Terminal support CDCVM and Online PIN, goto online PIN\n");
            if (qpbocOnlinePin(runningTimeEnv) == CVM_RESULT_CANCEL)
			{
				return STATUS_CANCEL;
			}
		}
	}else
	{
		//TAG_CARD_TRANSACTION_ATTRIBUTES EXIST
		dbg("TAG_CARD_TRANSACTION_ATTRIBUTES Exist,card response 9f6c\n");
		dbg("Card response 9F6C\n");
		memcpy(cardTransAttribute, GetTagValue(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES));
		hexdumpEx("List cardTransAttribute", cardTransAttribute, 2);
		memset(gl_uc9f6c,0x00,sizeof(gl_uc9f6c));
		memcpy(gl_uc9f6c,cardTransAttribute,2);
#if 0	
        if ((cardTransAttribute[0] & 0x80) != 0x00 && (termTransAttribute[2] & 0x40) != 0x00)
		{
			//Card support Online PIN and Terminal support Online PIN
			dbg("Card support Online PIN and Terminal support Online PIN\n");
            if (qpbocOnlinePin(runningTimeEnv) == CVM_RESULT_CANCEL)
			{
				return STATUS_CANCEL;
			}
			
		}else if ((cardTransAttribute[1] & 0x80) != 0x00)
		{
			//Card support CDCVM
			dbg("Card support CDCVM\n");
			if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA) == TRUE)
			{
				//Card no response TAG Card auth data
				dbg("Card no response Card auth data\n");
				memcpy(cryptInfoData, GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA), 
					GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA));
				hexdumpEx("List cryptInfoData", cryptInfoData, 1);
				ret = qpbocParseCryptInfoData(cryptInfoData);
				switch (ret)
				{
					case QPBOC_CID_STATUS_ARQC:
						dbg("Card support CDCVM, and Card no response TAG Card auth data;CID is ARQC\n");
						return STATUS_OK;
	
					 default:
					 	dbg("Card support CDCVM, and Card no response TAG Card auth data;CID is NOT ARQC\n");
					 	return STATUS_QPBOC_CVM_DECLINE;
				}
			}else
			{
				//Card response TAG CARD auth data
				dbg("Card response Card auth data\n");
				memcpy(cardAuthData, GetTagValue(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA), 
					GetTagValueSize(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA));
				if (cardAuthData[5] == cardTransAttribute[0] 
					&& cardAuthData[6] == cardTransAttribute[1])
				{
					////Line;
					dbg("cardAuthData[5] == cardTransAttribute[0] and cardAuthData[6] == cardTransAttribute[1]\n");
					return STATUS_OK;
				}else
				{
					////Line;
					dbg("cardAuthData != cardTransAttribute\n");
					return STATUS_QPBOC_CVM_DECLINE;
				}
			}
			
		}
		else if ((cardTransAttribute[0] & 0x40) != 0x00 && (termTransAttribute[0] & 0x02) != 0x00)
		{
			////Line;
			dbg("Card ask SIGN and Terminal Support SIGN\n");
			runningTimeEnv->qpbocCvmSign = TRUE;
		}else if ((termTransAttribute[1] & 0x40) != 0x00)
		{	
			////Line;
			dbg("9F66 ask CVM but card no cvm, transaction goto decline\n");
			return STATUS_QPBOC_CVM_DECLINE;
		}
#else
        runningTimeEnv->qpbocCvmSign = TRUE;
        if (qpbocOnlinePin(runningTimeEnv) == CVM_RESULT_CANCEL)
        {
            return STATUS_CANCEL;
        }
#endif        
	}
  
	return STATUS_OK;
}

#endif
tti_int32 qpbocBuildDDOL(RunningTimeEnv *runningTimeEnv)
{
	tti_byte tempByteArray[256];
	tti_uint16 tempByteLen;
	tti_byte appVersionData[100];
	tti_uint16 appVersionLen;
	tti_uint16 fixedAppVersion = 0x0030;
	tti_byte tempDDOLValue[256];
	tti_uint16 tempDDOLSize;
	tti_bool tagCardAuthDataIsExist = FALSE;
	tti_bool tagCardAppVersionIsExist = FALSE;

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA) == TRUE)
	{
		LOGE("NO TAG_QPBOC_CARD_AUTH_DATA");
		tagCardAuthDataIsExist = FALSE;
		//return STATUS_FAIL;
	}else
	{
		memcpy(tempByteArray, GetTagValue(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA));
		tempByteLen = GetTagValueSize(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA);
		tagCardAuthDataIsExist = TRUE;
	}

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_ICC_APP_VERSION_NUMBER) == TRUE)
	{
		LOGE("NO TAG_ICC_APP_VERSION_NUMBER");
		tagCardAppVersionIsExist = FALSE;
		//return STATUS_FAIL;
	}else
	{
		memcpy(appVersionData, GetTagValue(&runningTimeEnv->tagList, TAG_ICC_APP_VERSION_NUMBER), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_ICC_APP_VERSION_NUMBER));
		appVersionLen = GetTagValueSize(&runningTimeEnv->tagList, TAG_ICC_APP_VERSION_NUMBER);
		tagCardAppVersionIsExist = TRUE;
		//hexdumpEx("TAG_ICC_APP_VERSION_NUMBER", appVersionData, appVersionLen);
	}


	if (tagCardAuthDataIsExist == TRUE && tagCardAppVersionIsExist == TRUE)
	{
		if (tempByteArray[0] == 0x00)
		{
			//Format 00
			if (appVersionData[0] * 256 + appVersionData[1] >= fixedAppVersion)
			{
				LOGE("The new version card (>= 0x0030) can't be format 00");
				return STATUS_FAIL;
			}
			tempDDOLSize = 3;
			memcpy(tempDDOLValue, "\x9F\x37\x04", 3);
		}else if (tempByteArray[0] == 0x01)
		{
			//Format 01
			if (appVersionData[0] * 256 + appVersionData[1] < fixedAppVersion)
			{
				//The old version card ( <  0x0030) can't be format 01
				LOGE("The old version card ( <  0x0030) can't be format 01");
				return STATUS_FAIL;
			}
			
			tempDDOLSize = 12;
			memcpy(tempDDOLValue, "\x9F\x37\x04\x9F\x02\x06\x5F\x2A\x02", 9);
			memcpy(tempDDOLValue + 9, "\x9F\x69", 2);
			tempDDOLValue[11] = tempByteLen;
		}else
		{
			REPORT_ERR_LINE();
			LOGE("TAG_QPBOC_CARD_AUTH_DATA byte1 unknow format");
			return STATUS_FAIL;
		}
	}
	else if(tagCardAuthDataIsExist == FALSE && tagCardAppVersionIsExist == TRUE)
	{
		//It must be format 00
		if (appVersionData[0] * 256 + appVersionData[1] >= fixedAppVersion)
		{
			LOGE("The new version card (>= 0x0030) can't be format 00");
			return STATUS_FAIL;
		}
		tempDDOLSize = 3;
		memcpy(tempDDOLValue, "\x9F\x37\x04", 3);
	}else if (tagCardAuthDataIsExist == FALSE && tagCardAppVersionIsExist == FALSE)
	{
		//Default to be format 00
		tempDDOLSize = 3;
		memcpy(tempDDOLValue, "\x9F\x37\x04", 3);
	}else if (tagCardAuthDataIsExist == TRUE && tagCardAppVersionIsExist == FALSE)
	{
		if (tempByteArray[0] == 0x00)
		{
			tempDDOLSize = 3;
			memcpy(tempDDOLValue, "\x9F\x37\x04", 3);
		}else
		{
			LOGE("tagCardAuthDataIsExist == TRUE && tagCardAppVersionIsExist == FALSE, it can't be format 01");
			return STATUS_FAIL;
		}
	}else
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	SetTagValue(TAG_DDOL, tempDDOLValue, tempDDOLSize, &runningTimeEnv->tagList);
	//hexdumpEx("List DDOL", GetTagValue(&runningTimeEnv->tagList, TAG_DDOL), 
	//	GetTagValueSize(&runningTimeEnv->tagList, TAG_DDOL));
	return STATUS_OK;
}

tti_int32 qpbocGetPublicKey(RunningTimeEnv *runningTimeEnv);



tti_int32 qpbocPerformFDDA(RunningTimeEnv *runningTimeEnv)
{
	tti_byte ddaData[BUFFER_SIZE_512BYTE];
	int ddaDataLen;
	tti_byte aip[10];

	DPRINTF("qpboc perform fdda\n");

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_AFL) == FALSE)
	{
		LOGD("No TAG_AFL, no need fdda");
		return STATUS_OK;
	}

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA) == TRUE)
	{
		LOGE("No TAG_QPBOC_CARD_AUTH_DATA");
		//return STATUS_FAIL;
	}else
	{
		if (GetTagValueSize(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA) > 16 || 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_QPBOC_CARD_AUTH_DATA) < 8)
		{
			LOGE("TAG_QPBOC_CARD_AUTH_DATA Length error");
			return STATUS_FAIL;
		}
	}

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_AIP) == TRUE)
	{
		LOGE("No TAG_AIP");
		return STATUS_FAIL;
	}else
	{
		memcpy(aip, GetTagValue(&runningTimeEnv->tagList, TAG_AIP),  
			GetTagValueSize(&runningTimeEnv->tagList, TAG_AIP));
		//hexdumpEx("List AIP", aip, 2);
		LOGD("aip[0] & 0x20 = %d", aip[0] & 0x20);
		if ((aip[0] & 0x20) == 0)
		{
			LOGE("TAG_AIP Byte1 bit 6 is 0, don't support DDA");
			return STATUS_FAIL;
		}
	}

	if (qpbocGetPublicKey(runningTimeEnv) != STATUS_OK)
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	if (getIccCertificate(runningTimeEnv)!=STATUS_OK)		
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_DDA_SDAD))
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	ddaDataLen=sizeof(ddaData);

	if (RSA(&runningTimeEnv->iccPublicKey, GetTagValue(&runningTimeEnv->tagList, TAG_DDA_SDAD),

		GetTagValueSize(&runningTimeEnv->tagList, TAG_DDA_SDAD), ddaData, &ddaDataLen)!=STATUS_OK)

	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	printByteArray(ddaData, ddaDataLen);

	if(qpbocBuildDDOL(runningTimeEnv) != STATUS_OK)
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	if (checkQpbocRawDDACertificate(ddaData, ddaDataLen, runningTimeEnv)!=STATUS_OK)
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	dbg("qpbocPerformFDDA return STATUS_OK\n");
	return STATUS_OK;
}



tti_int32 gqpbocReadRecordRet = STATUS_OK;

tti_int32 qpbocSetReadRecordRet(int ret)
{
	gqpbocReadRecordRet = ret;
    return 0;
}

tti_int32 qpbocGetReadRecordRet(void)
{
	return gqpbocReadRecordRet;
}

tti_int32 gqpbocFddaRet = STATUS_OK;

tti_int32 qpbocSetPerformFddaRet(int ret)
{
	gqpbocFddaRet = ret;
    return 0;
}

tti_int32 qpbocGetPerformFddaRet(void)
{
	return gqpbocFddaRet;
}


tti_int32 qpbocReadRecord(RunningTimeEnv *runningTimeEnv)
{
	//tti_tchar response[BUFFER_SIZE_512BYTE];
	tti_byte response[BUFFER_SIZE_512BYTE];
	tti_uint16 respLen;
	tti_byte *AFL;
	tti_int32 AFLLen, index, recordIndex, duplicateItemIndex;
	tti_int32 offlineDataRecord;
	tti_uint16 tagLen, tagLenSize;
	tti_uint16 ingoreFlag = 0;

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_AFL) == FALSE)
	{
		DPRINTF("No AFL, qpboc no need read record\n");
		return STATUS_OK;
	}

	AFL=GetTagValue(&runningTimeEnv->tagList, TAG_AFL);
	AFLLen=GetTagValueSize(&runningTimeEnv->tagList, TAG_AFL);
	//DPRINTF("AFL list\n");
	//hexdumpEx("AFL list", AFL, AFLLen);
	
	if (((AFLLen/4)*4!=AFLLen)||(AFLLen==0))
	{
		return STATUS_FAIL;
	}

	runningTimeEnv->offlineAuthDataLen=0;

	//Added on 2020/05/19
	//Check all AFI before read record 
	for (index=0; index<AFLLen; index+=4)
	{
		//Jason modified on 2017/06/08
		//if ((AFL[index+1]==0)||(AFL[index+2]-AFL[index+1]+1<AFL[index+3]))
		if ((AFL[index+1]==0)||(AFL[index+2]-AFL[index+1]+1<AFL[index+3]))
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;
		}
		if (((AFL[index]>>3) < 1)||((AFL[index]>>3) > 30))
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;			
		}
		if (AFL[index+1] > AFL[index+2])
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;	
		}
	}
	//End
	for (index=0; index<AFLLen; index+=4)
	{
		//Jason modified on 2017/06/08
		//if ((AFL[index+1]==0)||(AFL[index+2]-AFL[index+1]+1<AFL[index+3]))
		if ((AFL[index+1]==0)||(AFL[index+2]-AFL[index+1]+1<AFL[index+3]))
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;
		}
		if (((AFL[index]>>3) < 1)||((AFL[index]>>3) > 30))
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;			
		}
		if (AFL[index+1] > AFL[index+2])
		{
			REPORT_ERR_LINE();
			return STATUS_FAIL;	
		}
		
		//End
		offlineDataRecord=0;
		//DPRINTF("index = %d\n", index);
		for (recordIndex=AFL[index+1]; recordIndex<=AFL[index+2]; recordIndex++)
		{
			if (NfcReadRecord(AFL[index], (tti_byte)recordIndex, response, &respLen)==STATUS_OK)
			{
				if (checkRecordFormat(response, respLen)!=STATUS_OK)
				{
					//DPRINTF("checkRecordFormat error\n");
					if (((AFL[index]>>3)<=10)||((AFL[index]>>3)>30))
					{
						LOGE("(AFL[index]>>3)<=10)||((AFL[index]>>3)>30");
						return STATUS_FAIL;
					}
					
				}
				else
				{
					//******************************************
					//bob add here 20101028
					//*
					//DPRINTF("New checkRecordData\n");
					if (checkRecordData(response, &respLen)!=STATUS_OK)
					{
						DPRINTF("checkRecordData error\n");
						//return STATUS_FAIL;
						return STATUS_READ_RECORD_ERR;
					}
					//DPRINTF("New checkRecordData ok, respLen = [%d]\n", respLen);
					
					//*/
					//*******************************************
					//printByteArray(response, respLen);
					if (BuildTagList(response, respLen, &runningTimeEnv->tagList)!=STATUS_OK)
					{
						DPRINTF("after new check record data, BuildTagList error\n");
						return STATUS_READ_RECORD_ERR;
					}
				}
				//DPRINTF("offlineDataRecord [%d]\n", offlineDataRecord);
				//DPRINTF("AFL[index+3] = [%d]\n", AFL[index+3]);
				if (offlineDataRecord<AFL[index+3])
				{
					//DPRINTF("Enter set offline data record \n");
					//printByteArray(response, respLen);
					if (response[0]==TAG_TEMPERATE_70)
					{
						//DPRINTF("response[0]==TAG_TEMPERATE_70\n");
						if ((AFL[index]>>3)>10)
						{
							DPRINTF("SFI >10\n");
							memcpy(&runningTimeEnv->offlineAuthData[runningTimeEnv->offlineAuthDataLen],
								response, respLen);
							runningTimeEnv->offlineAuthDataLen+=respLen;
						}
						else
						{
							DPRINTF("SFI <= 10\n");
							if (ParseTlvLengthInfo(response+1, respLen-1, &tagLen, &tagLenSize)
								!=STATUS_OK)
							{
								//return STATUS_FAIL;
								ReportLine();
								return STATUS_READ_RECORD_ERR;
							}
							DPRINTF("Have offline data\n");
							memcpy(&runningTimeEnv->offlineAuthData[runningTimeEnv->offlineAuthDataLen],
								response+1+tagLenSize, respLen-1-tagLenSize);
							runningTimeEnv->offlineAuthDataLen+=respLen-1-tagLenSize;
						}
					}else
					{
						DPRINTF("response[0]!=TAG_TEMPERATE_70\n");
					}
				}

				if (checkQpbocAppBecomeExpiration(runningTimeEnv) == TRUE)
				{
					LOGE("checkQpbocAppBecomeExpiration() = TRUE\n");
					return STATUS_CARD_EXP_ERR;
				}
				
				//Jason modified on 2017.05.05
				//DPRINTF("duplicate tag count=%d\n", runningTimeEnv->tagList.duplicateItemCount);
				
				ingoreFlag = 1;
				for (duplicateItemIndex=0; duplicateItemIndex<runningTimeEnv->tagList.duplicateItemCount; duplicateItemIndex++)
				{
					//DPRINTF("duplicate tag=%x\n", runningTimeEnv->tagList.duplicateItem[duplicateItemIndex]);
					if ((runningTimeEnv->tagList.duplicateItem[duplicateItemIndex] <= 0x9F7F && 
						runningTimeEnv->tagList.duplicateItem[duplicateItemIndex] >= 0x9F50) ||
						runningTimeEnv->tagList.duplicateItem[duplicateItemIndex] == 0x85 ||
						runningTimeEnv->tagList.duplicateItem[duplicateItemIndex] == 0x5F34)
					{
						if (runningTimeEnv->tagList.duplicateItem[duplicateItemIndex] == 0x9F5D ||
							runningTimeEnv->tagList.duplicateItem[duplicateItemIndex] == 0x9F7C)
						{
							//LOGD("Now have duplicate tag 0x9F5D or 0x9F7C, you can't ingore them\n");
							ingoreFlag = 0;
						}else
						{
							//Do nothing
							LOGD("Now duplicate tag from 0x9F50 to 0x9F7F or 0x85 or 0x5F34, you can ingore them\n");
						}
					}else
					{
						//The duplicate tag is not from 0x9F50 to 0x9F7F, you can't ingore the duplicate tag check.
						//LOGD("Now have duplicate tag not from 0x9F50 to 0x9F7F or 0x85 or 0x5F34, you can't ingore them\n");
						ingoreFlag = 0;
					}
				}

				if (ingoreFlag == 0)
				{
					if (checkDuplicateItem(&runningTimeEnv->tagList)==STATUS_OK)
					{
						ReportLine();
						//return STATUS_OK;
					}
					else
					{
						//return STATUS_FAIL;
						ReportLine();
						return STATUS_READ_RECORD_ERR;
					}
				}else
				{
					ReportLine();
					//return STATUS_OK;
				}
			}
			else
			{
			#ifdef QPBOC_RETRY
               		 return STATUS_ERROR;
                	 #else
				//return STATUS_FAIL;
				REPORT_ERR_LINE();
				return STATUS_READ_RECORD_ERR;
			 #endif	
			}
				
			offlineDataRecord++;
		}
	}

	if (checkQpbocMandatoryData(runningTimeEnv)!=STATUS_OK)
	{
		
		EmvIOSessionEnd();
		//return STATUS_FAIL;
		ReportLine();
		return STATUS_READ_RECORD_ERR;
	}	

	return STATUS_OK;

	/*
	DPRINTF("duplicate tag count=%d\n", runningTimeEnv->tagList.duplicateItemCount);

	//Jason modified on 2017.05.05
	ingoreFlag = 1;
	for (index=0; index<runningTimeEnv->tagList.duplicateItemCount; index++)
	{
		DPRINTF("duplicate tag=%x\n", runningTimeEnv->tagList.duplicateItem[index]);
		if ((runningTimeEnv->tagList.duplicateItem[index] <= 0x9F7F && 
			runningTimeEnv->tagList.duplicateItem[index] >= 0x9F50) ||
			runningTimeEnv->tagList.duplicateItem[index] == 0x85)
		{
			//Do nothing
			LOGD("Now duplicate tag from 0x9F50 to 0x9F7F or 0x85, you can ingore them");
		}else
		{
			//The duplicate tag is not from 0x9F50 to 0x9F7F, you can't ingore the duplicate tag check.
			LOGD("Now have duplicate tag not from 0x9F50 to 0x9F7F or 0x85, you can't ingore them");
			ingoreFlag = 0;
		}
	}

	if (ingoreFlag == 0)
	{
		if (checkDuplicateItem(&runningTimeEnv->tagList)==STATUS_OK)
		{
			return STATUS_OK;
		}
		else
		{
			//return STATUS_FAIL;
			ReportLine();
			return STATUS_READ_RECORD_ERR;
		}
	}else
	{
		ReportLine();
		return STATUS_OK;
	}
	*/
}

#if 0
tti_int32 qpbocTerminalAction(RunningTimeEnv *runningTimeEnv)
{
	tti_byte tempByteArr[256];
	//tti_uint16 tempByteLen;
	tti_int32 ret;
	tti_tchar displayInfo[256] = {0};
	tti_tchar transResult[256] = {0};
	tti_tchar tempString[256] = {0};
	tti_tchar tempFormatString[256] = {0};
	tti_tchar balance[256] = {0};

	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_QPBOC_OFFLINE_BALANCE) == FALSE)
	{
		TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_QPBOC_OFFLINE_BALANCE), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_QPBOC_OFFLINE_BALANCE), tempString);
		LOGD("tempString = %s", tempString);
		foramtAmount(tempString, tempFormatString);
		LOGD("tempFormatString = %s", tempFormatString);
		sprintf(balance, "余额:%s", tempFormatString);
		//sprintf(displayInfo, " %s\n 浣欓: %s", hostResult, tempFormatString);	
	}else
	{
		sprintf(balance, "%s", " ");	
	}

	memcpy(tempByteArr, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA), 
		GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA));
	if ((tempByteArr[4] & 0x30) == 0x00)
	//memcpy(tempByteArr, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA), 
	//	GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA));
	//if ((tempByteArr[4] & 0x30) == 0x00)
	//memcpy(tempByteArr, GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA), 
	//	GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA));
	//if ((tempByteArr[0] & 0xC0) == 0x00)
	{
		//Decline
		strcpy(transResult, "拒绝");

		sprintf(displayInfo, "%s,%s,请移卡", transResult, balance);
		nfcLedFailedShow();
		DisplayValidateInfo(displayInfo);
		return STATUS_OK;
	}else if ((tempByteArr[4] & 0x30) == 0x10)
	//}else if ((tempByteArr[0] & 0xC0) == 0x40)
	{
		//Offline
		ret = qpbocReadRecord(runningTimeEnv);
		EmvContactlessIOSessionEnd();
		if (ret != STATUS_OK)
		{
			if (ret == STATUS_CARD_EXP_ERR)
			{
				REPORT_ERR_LINE();
				nfcLedFailedShow();
				strcpy(transResult, "卡过期，拒绝");
			}else
			{
				REPORT_ERR_LINE();
				nfcLedFailedShow();
				strcpy(transResult, "终止");
			}
		}else
		{
			ret = qpbocPerformFDDA(runningTimeEnv);
			if (ret != STATUS_OK)
			{
				memcpy(tempByteArr, GetTagValue(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES), 
					GetTagValueSize(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES));	
				hexdumpEx("TAG_CARD_TRANSACTION_ATTRIBUTES", tempByteArr, 5);
				if (tempByteArr[0] & 0x20)
				{
					//Goto online
					DisplayProcess("联机处理中");
					ret = qpbocAuthCommWithHost();
					if (ret != STATUS_OK)
					{
						ReportLine();
						nfcLedFailedShow();
						strcpy(transResult, "联机拒绝");
						
					}else
					{
						nfcLedSuccessShow();
						strcpy(transResult, "联机批准");
						SaveQpbocBatchCaptureDetail();
						qpbocPrintReceipt(runningTimeEnv);
					}
				}else if (tempByteArr[0] & 0x10)
				{
					//Terminate
					nfcLedFailedShow();
					strcpy(transResult, "FDDA失败，转终止");
				}else
				{
					//Decline
					nfcLedFailedShow();
					strcpy(transResult, "FDDA失败，转脱机拒绝");
				}
			}else
			{
				nfcLedSuccessShow();
				strcpy(transResult, "脱机批准");
				SaveQpbocBatchCaptureDetail();
				qpbocPrintReceipt(runningTimeEnv);
			}
		}
			

		ReportLine();
		sprintf(displayInfo, "%s,%s,请移卡", transResult, balance);
		ReportLine();
		DisplayValidateInfo(displayInfo);
		return STATUS_OK;
	}else if ((tempByteArr[4] & 0x30) == 0x20)
	//}else if ((tempByteArr[0] & 0xC0) == 0x80)
	{
		//Online
//online:
		DisplayProcess("联机处理中");
		ret = qpbocAuthCommWithHost();
		if (ret != STATUS_OK)
		{
			ReportLine();
			nfcLedFailedShow();
			strcpy(transResult, "联机拒绝");
			
		}else
		{
			nfcLedSuccessShow();
			strcpy(transResult, "联机批准");
			SaveQpbocBatchCaptureDetail();
			qpbocPrintReceipt(runningTimeEnv);
		}

		ReportLine();
		sprintf(displayInfo, "%s,%s,请移卡", transResult, balance);
		ReportLine();
		LOGD("displayInfo = %s", displayInfo);
		DisplayValidateInfo(displayInfo);
		ReportLine();
		return STATUS_OK;
	}else
	{
		ReportLine();
		nfcLedFailedShow();
		LOGE("unknow status");
		DisplayValidateInfo("终止,请移卡");
		return STATUS_FAIL;
	}
}
#else
tti_int32 qpbocTerminalAction(RunningTimeEnv *runningTimeEnv)
{
	tti_byte cryptInfoData[20];
	tti_byte cardTransAttr[20];
	tti_byte terminalTransAttr[20];
	tti_byte tempByteArr[50];
	tti_byte issueAppData[10];
	tti_byte tmp;
	//tti_uint16 tempByteLen;
	tti_int32 ret;
	tti_tchar displayInfo[50] = {0};
	tti_tchar transResult[50] = {0};
	tti_tchar tempString[256] = {0};
	tti_tchar tempFormatString[256] = {0};
	tti_tchar balance[50] = {0};

	DPRINTF("Call qpbocTerminalAction\n");
	FUNCIN;
    
	if (TagDataIsMissing(&runningTimeEnv->tagList, TAG_QPBOC_OFFLINE_BALANCE) == FALSE)
	{
		LOGD("TAG_QPBOC_OFFLINE_BALANCE Exist : 9F5D\n");
		TranslateHexToChars(GetTagValue(&runningTimeEnv->tagList, TAG_QPBOC_OFFLINE_BALANCE), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_QPBOC_OFFLINE_BALANCE), tempString);
		LOGD("tempString = %s", tempString);
		foramtAmount(tempString, tempFormatString);
		LOGD("tempFormatString = %s", tempFormatString);
		sprintf(balance, "余额:%s", tempFormatString);
		//sprintf(displayInfo, " %s\n 浣欓: %s", hostResult, tempFormatString);	
	}else
	{
		LOGD("No TAG_QPBOC_OFFLINE_BALANCE :No 9F5D \n");
		sprintf(balance, "%s", " ");	
	}

	if (TagIsExisted(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA) == FALSE)
	{
		DPRINTF("No TAG_CRYPTOGRAM_INFO_DATA\n");
		memset(cryptInfoData, 0x00, sizeof(cryptInfoData));
		memcpy(issueAppData, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA));
		tmp = issueAppData[4] & 0x30;
		cryptInfoData[0] = tmp << 2;
		//memcpy(cryptInfoData, issueAppData+4, 1);
		hexdumpEx("List issueAppData", issueAppData, 5);
		hexdumpEx("List cryptInfoData", cryptInfoData, 1);
		SetTagValue(TAG_CRYPTOGRAM_INFO_DATA, cryptInfoData, 1, &runningTimeEnv->tagList);
	}
	else if (GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA) == 0)
	{	DPRINTF("TAG_CRYPTOGRAM_INFO_DATA SIZE == 0\n");
		memset(cryptInfoData, 0x00, sizeof(cryptInfoData));
		memcpy(issueAppData, GetTagValue(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_ISSUER_APP_DATA));
		tmp = issueAppData[4] & 0x30;
		cryptInfoData[0] = tmp << 2;
		//memcpy(cryptInfoData, issueAppData+4, 1);
		hexdumpEx("List issueAppData", issueAppData, 5);
		hexdumpEx("List cryptInfoData", cryptInfoData, 1);
		SetTagValue(TAG_CRYPTOGRAM_INFO_DATA, cryptInfoData, 1, &runningTimeEnv->tagList);
	}else
	{
		DPRINTF("TAG_CRYPTOGRAM_INFO_DATA Exist and Length is not 0\n");
		memcpy(cryptInfoData, GetTagValue(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA), 
			GetTagValueSize(&runningTimeEnv->tagList, TAG_CRYPTOGRAM_INFO_DATA));
	}

	hexdumpEx("List cryptInfoData", cryptInfoData, 1);
	ret = qpbocParseCryptInfoData(cryptInfoData);
	if (ret == QPBOC_CID_STATUS_AAC)
	{
		//Decline
		////Line;
		strcpy(transResult, "交易失败");
#ifndef VPOS_APP
		sprintf(displayInfo, "%s,%s", transResult, balance);
#else
        sprintf(displayInfo, "%s", transResult);
#endif        
		nfcLedFailedShow();
		DisplayValidateInfo(displayInfo);
		return STATUS_OK;
	}else if (ret == QPBOC_CID_STATUS_TC)
	{
		//Offline
		//ret = qpbocReadRecord(runningTimeEnv);
		ret = qpbocGetReadRecordRet();
		EmvContactlessIOSessionEnd();
		//DisplayProcess("请移卡");
		if (ret != STATUS_OK)
		{
			if (ret == STATUS_CARD_EXP_ERR)
			{
				////Line;
				if (TagIsExisted(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES) == FALSE)
				{
					nfcLedFailedShow();
					dbg("Card exp error and No 9F6C, goto decline\n");
					strcpy(transResult, "卡过期,交易拒绝");
				}else
				{
					memcpy(cardTransAttr, GetTagValue(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES), 
					GetTagValueSize(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES));
					if ((cardTransAttr[0] & 0x08) != 0x00)
					{
						dbg("Card exp error, 9F6C Byte 1 Bit 4 is '1' , goto online\n");
						DisplayProcess("卡过期,交易转联机");
						mdelay(1000);
						goto Online;
						
					}else
					{
						nfcLedFailedShow();
						dbg("Card exp error, 9F6C Byte 1 Bit 4 is '0' , goto decline\n");
						strcpy(transResult, "卡过期,交易拒绝");
					}
				}
			}else
			{
				////Line;
				nfcLedFailedShow();
				strcpy(transResult, "交易终止");
			}
		}else
		{
			//ret = qpbocPerformFDDA(runningTimeEnv);
			ret = qpbocGetPerformFddaRet();
			if (ret != STATUS_OK)
			{
				dbg("qpbocGetPerformFddaRet return not STATUS_OK\n");
				if (TagIsExisted(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES) == FALSE)
				{
					nfcLedFailedShow();
					dbg("FDDA error and No 9f6c, GOTO decline\n");
					strcpy(transResult, "交易拒绝");
					goto DispResult;
				}
				memcpy(cardTransAttr, GetTagValue(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES), 
					GetTagValueSize(&runningTimeEnv->tagList, TAG_CARD_TRANSACTION_ATTRIBUTES));	
				hexdumpEx("TAG_CARD_TRANSACTION_ATTRIBUTES", cardTransAttr, 5);
				memcpy(terminalTransAttr, GetTagValue(&runningTimeEnv->tagList, TAG_TERMINAL_TRANSACTION_ATTRIBUTES), 
					GetTagValueSize(&runningTimeEnv->tagList, TAG_TERMINAL_TRANSACTION_ATTRIBUTES));
				hexdumpEx("TAG_TERMINAL_TRANSACTION_ATTRIBUTES", terminalTransAttr, 1);
				if ( (cardTransAttr[0] & 0x20) != 0x00 && (terminalTransAttr[0] & 0x20) != 0x00)
				{
					//Goto online
Online:				
                    //DisplayProcess("联机处理中");
                    _vDisp(3, "正在处理中...");
					ret = qpbocAuthCommWithHost();
					if (ret != STATUS_OK)
					{
						ReportLine();
						nfcLedFailedShow();
						strcpy(transResult, "交易拒绝");
						
					}else
					{
						nfcLedSuccessShow();
						//LcdPutsc("请移卡", 3);
						strcpy(transResult, "交易成功");
						SaveQpbocBatchCaptureDetail();
						qpbocPrintReceipt(runningTimeEnv);
					}
				}else if ((cardTransAttr[0] & 0x10) != 0x00)
				{
					//Terminate
					nfcLedFailedShow();
					strcpy(transResult, "交易终止");
				}else
				{
					//Decline
					nfcLedFailedShow();
					strcpy(transResult, "交易失败");
				}
			}else
			{
				dbg("qpbocGetPerformFddaRet return STATUS_OK\n");
				nfcLedSuccessShow();
				strcpy(transResult, "交易成功");
				SaveQpbocBatchCaptureDetail();
				qpbocPrintReceipt(runningTimeEnv);
			}
		}
			

DispResult:		

        ReportLine();
#ifndef VPOS_APP            
        sprintf(displayInfo, "%s,%s", transResult, balance);  
        ReportLine();
        DisplayValidateInfo(displayInfo);
#else
        sprintf(displayInfo, "%s", transResult);   
        ReportLine();
        if(iGetDispRetInfo())
            DisplayValidateInfo(displayInfo);
#endif
		return STATUS_OK;
	}else if (ret == QPBOC_CID_STATUS_ARQC)
	{
		
//online:
		//DisplayProcess("联机处理中");
        _vDisp(3, "正在处理中...");
		ret = qpbocAuthCommWithHost();
		if (ret != STATUS_OK)
		{
			ReportLine();
			nfcLedFailedShow();
			strcpy(transResult, "交易拒绝");
		}else
		{
			nfcLedSuccessShow();
			//strcpy(transResult, "联机批准");
            strcpy(transResult, "交易成功");
			SaveQpbocBatchCaptureDetail();
			qpbocPrintReceipt(runningTimeEnv);
		}

		ReportLine();
#ifndef VPOS_APP        
		sprintf(displayInfo, "%s,%s", transResult, balance);
		LOGD("displayInfo = %s", displayInfo);
		DisplayValidateInfo(displayInfo);
		ReportLine();
#else
        strcpy(displayInfo, transResult);
        LOGD("displayInfo = %s", displayInfo);
        
        if(iGetDispRetInfo())
            DisplayValidateInfo(displayInfo);
		ReportLine();
#endif        
		return STATUS_OK;
	}else
	{
		ReportLine();
		nfcLedFailedShow();
		LOGE("unknow status");
        if(iGetDispRetInfo())
            DisplayValidateInfo("交易终止,请移卡");
		return STATUS_FAIL;
	}
}

#endif

tti_int32 qpbocTerminalPreTrans(TagList *aidTagList, 
	Tag3ByteList *aidTag3ByteList, TagList *transactionTagList)
{
	tti_byte temp1ByteArry[256];
	tti_int32 temp1ByteSize;
//	tti_byte temp2ByteArry[256];
//	tti_int32 temp2ByteSize;
	tti_int32 transAmount;
	tti_int32 termTransAmountLimit;
	tti_int32 termCVMAmountLimit;
	tti_int32 termOfflineLimit;
	tti_byte transAttrArr[4] = {0};

	tti_byte transAttrByte1 = 0x26;
	tti_byte transAttrByte2 = 0x00;

	LOGD("qpbocTerminalPreTrans");

	if (TagIsExisted(transactionTagList, TAG_AMOUNT) == TRUE)
	{
		transAmount = getAmountByTag(transactionTagList, TAG_AMOUNT);
		LOGD("transAmount = %d", transAmount);
	}else
	{
		LOGE("No transAmount");
		return STATUS_FAIL;
	}

	if (TagIsExisted(aidTagList, TAG_QPBOC_TRANS_AMOUNT_LIMIT_BCTC) == TRUE)
	{
		termTransAmountLimit = getAmountByTag(aidTagList, TAG_QPBOC_TRANS_AMOUNT_LIMIT_BCTC);
		LOGD("termTransAmountLimit = %d", termTransAmountLimit);
		if (transAmount >= termTransAmountLimit)
		{
			LOGE("transAmount >= termTransAmountLimit, Not QPBOC");
			return STATUS_ERROR;
		}
	}

	if (TagIsExisted(aidTagList, TAG_CVM_FLOOR_LIMIT_AMOUNT_BCTC) == TRUE)
	{
		termCVMAmountLimit = getAmountByTag(aidTagList, TAG_CVM_FLOOR_LIMIT_AMOUNT_BCTC);
		LOGD("termCVMAmountLimit = %d", termCVMAmountLimit);
		if (transAmount >= termCVMAmountLimit)
		{
			LOGD("Set transAttrByte2 bit 7 to 1");
			transAttrByte2 |= 0x40; 
		}
	}

	if (TagIsExisted(aidTagList, TAG_QPBOC_OFFLINE_AMOUNT_LIMIT_BCTC) == TRUE)
	{
		termOfflineLimit = getAmountByTag(aidTagList, TAG_QPBOC_OFFLINE_AMOUNT_LIMIT_BCTC);
		LOGD("termOfflineLimit = %d", termOfflineLimit);
		if (transAmount > termOfflineLimit)
		{
			LOGD("Set transAttrByte2 bit 8 to 1");
			transAttrByte2 |= 0x80; 
		}
	}else
	{
		LOGE("No TAG_QPBOC_OFFLINE_AMOUNT_LIMIT_BCTC");
#ifdef VPOS_APP        
        if (TagIsExisted(aidTagList, TAG_FLOOR_LIMIT) == TRUE)
        {
            termOfflineLimit = getAmountByTag(aidTagList, TAG_FLOOR_LIMIT);
            LOGD("termOfflineLimit = %d", termOfflineLimit);
            if (transAmount > termOfflineLimit)
            {
                LOGD("Set transAttrByte2 bit 8 to 1");
                transAttrByte2 |= 0x80; 
            }
        }else
#endif        
            return STATUS_FAIL;      
	}
    
#ifndef VPOS_APP
	if (GetTag3ByteValueAndSize(aidTag3ByteList, TAG_QPBOC_STATUS_CHECK, temp1ByteArry, &temp1ByteSize) != STATUS_OK)
	{
		LOGE("No TAG_QPBOC_STATUS_CHECK");
		return STATUS_FAIL;
	}   
	hexdumpEx("TAG_QPBOC_STATUS_CHECK", temp1ByteArry, 1);
	if (temp1ByteArry[0] == 0x01 && (transAmount == 100 || transAmount == 0))
	{
		LOGD("TAG_QPBOC_STATUS_CHECK == 1 && (transAmount == 100 || transAmount == 0)");
		LOGD("Set transAttrByte2 bit 8 to 1");
		transAttrByte2 |= 0x80; 
	}
#else
    if(transAmount == 0)
        transAttrByte2 |= 0x80;
    //if(gl_TransRec.uiTransType!=TRANS_TYPE_SALE)      //强制联机
    {
        transAttrByte2 |= 0x80;
    }
#endif
	transAttrArr[0] = transAttrByte1;
	transAttrArr[1] = transAttrByte2;
	//transAttrArr[2] = 0x00;
	transAttrArr[2] = 0x40;
	transAttrArr[3] = 0x80;

	hexdumpEx("List transAttrArr", transAttrArr, 4);
	SetTagValue(TAG_TERMINAL_TRANSACTION_ATTRIBUTES, transAttrArr, 4, transactionTagList);
	return STATUS_OK;
}

tti_int32 qpbocInitTransTagList(TagList *transactionTagList)
{
	tti_uint32 AmountAuthor;
	tti_uint32 AmountOther;
	tti_byte tempByteArray[100] = {0};
	tti_byte byRand[4];
	tti_byte Date[3];
	tti_byte Time[3];
    tti_byte transType;

	AmountAuthor = EMVL2_GetAmount();
	AmountOther = 0;
    transType = EMVL2_GetTransType();

	AmountAuthor += AmountOther;
    LOGD("AmountAuthor = %d, AmountOther = %d", AmountAuthor, AmountOther);

	castAmountToBCDArray(AmountAuthor, tempByteArray);
	SetTagValue(TAG_AMOUNT, tempByteArray, AMOUNT_SIZE_IN_TAG, transactionTagList);

	castInt32ToByteArray(AmountAuthor, tempByteArray);
	SetTagValue(TAG_AMOUNT_BINARY, tempByteArray, 4, transactionTagList);
	
	castAmountToBCDArray(AmountOther, tempByteArray);
	SetTagValue(TAG_AMOUNT_OTHER, tempByteArray, AMOUNT_SIZE_IN_TAG, transactionTagList);

	castInt32ToByteArray(AmountOther, tempByteArray);
	SetTagValue(TAG_AMOUNT_OTHER_BINARY, tempByteArray, 4, transactionTagList);

	rand_hardware_byte(byRand);
	SetTagValue(TAG_UNPREDICTABLE_NUMBER, byRand, 4, transactionTagList);

	//SetTagValue(TAG_TRANSACTION_TYPE, "\x00", 1, transactionTagList);
    SetTagValue(TAG_TRANSACTION_TYPE, &transType, 1, transactionTagList);
    

	SetTagValue(TAG_TVR, "\x00\x00\x00\x00\x00", 5, transactionTagList);
	SetTagValue(TAG_TSI, "\x00\x00", 2, transactionTagList);

	SetTagValue(TAG_POS_ENTRY_MODE, "\x07", 1, transactionTagList);
	
	GetCurrentDateAndTime(Date, Time);
	hexdumpEx("Date", Date, 3);
	hexdumpEx("Time", Time, 3);
	
	SetTagValue(TAG_TRANSACTION_DATE, Date, 3, transactionTagList);
	SetTagValue(TAG_TRANSACTION_TIME, Time, 3, transactionTagList);
	SetTagValue(TAG_CVM_RESULTS, "\x00\x00\x00", CVM_RESULT_SIZE, transactionTagList);

	return STATUS_OK;
}

tti_int32 qpbocInitTermailPara(TagList *pTagList)
{
	tti_byte transCurrCode[10];

	//SetTagValue(TAG_APP_VERSION_NUMBER, "\x00\x30", 2, pTagList);
	SetTagValue(TAG_TERM_COUNTRY_CODE, (tti_byte *)("\x01\x56"), 2, pTagList);
#if PROJECT_CY20 == 1
	dbg("qpbocInitTermailPara : CY20\n");
	//SetTagValue(TAG_TERMINAL_CAPABILITIES, (tti_byte *)("\xE0\xF8\xC8"), 3, pTagList);
    SetTagValue(TAG_TERMINAL_CAPABILITIES, (tti_byte *)("\xE0\xE1\xC8"), 3, pTagList);
	SetTagValue(TAG_ADDITIONAL_TERM_CAPABILITY, (tti_byte *)("\xF0\x00\xF0\xA0\x01"), 5, pTagList);
#else
	dbg("qpbocInitTermailPara : CY21\n");
	SetTagValue(TAG_TERMINAL_CAPABILITIES, (tti_byte *)("\xE0\xD8\xC8"), 3, pTagList);
	SetTagValue(TAG_ADDITIONAL_TERM_CAPABILITY, (tti_byte *)("\xF0\x00\xF0\x20\x01"), 5, pTagList);
#endif
	SetTagValue(TAG_TERMINAL_TYPE, (tti_byte *)("\x22"), 1, pTagList);
	SetTagValue(TAG_IFD_SERIAL_NUMBER, (tti_byte *)("Terminal"), 8, 
		pTagList);
	//SetTagValue(TAG_TRANS_CURRENCY_CODE, "\x01\x56", 2, pTagList);
	GetTransCurrencyCode(transCurrCode);
	SetTagValue(TAG_TRANS_CURRENCY_CODE, transCurrCode, 2, pTagList);
	//GetTransCurrencyCode
	
	return STATUS_OK;
}

extern TagList gAidParameterTagList;
extern Tag3ByteList gAidParaTag3ByteList;

void qpbocInitTagList()
{
   	FreeTagList(&runningTimeEnv.tagList);
	initRunningTimeEnv(&runningTimeEnv);

    FreeTagList(&gAidParameterTagList);
    InitTagList(&gAidParameterTagList);

    InitTag3ByteList(&gAidParaTag3ByteList);
}

#ifdef QPBOC_RETRY
static int sg_iQPbocTryFlag=0;
void vSetQPbocTryFlag(int flag)
{
    sg_iQPbocTryFlag=flag;
}
int iGetQPbocTryFlag(void)
{
    return sg_iQPbocTryFlag;
}
#else
void vSetQPbocTryFlag(int flag)
{
}
int iGetQPbocTryFlag(void)
{
    return 0;
}
#endif

tti_int32 qpbocTransaction(char *szPrompt)
{
	//TagList terminalTagList;
	//TagList aidTagList;
	//Tag3ByteList aidTag3ByteList; 
	//TagList transactionTagList;
  int status;
	int ret=0;
	tti_int32 loop;
	tti_byte tempByteArray[10];
	tti_uint16 tempByteArrayLen;
	tti_tchar displayInfo[50] = {0};
	tti_tchar transResult[50] = {0};
	tti_tchar tempString[256] = {0};
	tti_tchar tempFormatString[256] = {0};
	tti_tchar balance[50] = {0};
	tti_tchar szAmt[50] = {0};
	tti_tchar szDispAmt[50] = {0};
	uchar szTmp[100];
	ulong ulAmount;
		
	//LOGD("Enter qpbocTransaction!");
#ifndef VPOS_APP
	if (IsQpbocTransRecordFull() == 1)
	{
		LOGE("IsQpbocTransRecordFull");
		DisplayValidateInfo("交易日志满");
		ClearQpbocTransRecord();
		return  STATUS_FAIL;
	}
#endif
    
    #ifdef VPOS_APP
    vSetQPbocTryFlag(0);	
    vSetDispRetInfo(1);
    #endif
	qpbocInitTagList();
	qpbocInitTermailPara(&runningTimeEnv.tagList);
	qpbocInitTransTagList(&runningTimeEnv.tagList);
	if (LoadAidParaByAid("A0000003330101", NULL, NULL) != STATUS_OK)
	{
		if (LoadAidParaByAid("A000000333010101", NULL, NULL) != STATUS_OK)
		{      
			 if (LoadAidParaByAid("A000000333010102", NULL, NULL) != STATUS_OK)
			{
				//DisplayValidateInfo("读取参数失败，终止");
	           		 DisplayValidateInfo("IC卡参数错");
				return  STATUS_FAIL;
			}	
		}
	}
	if ((ret = qpbocTerminalPreTrans(&gAidParameterTagList, &gAidParaTag3ByteList, &runningTimeEnv.tagList)) != STATUS_OK)
    {
        LOGD("qpbocTerminalPreTrans error\n");
        EmvContactlessIOSessionEnd();
		if (ret == STATUS_ERROR)
		{
        	//DisplayValidateInfo("金额超限，终止");
            DisplayValidateInfo("金额超限,交易终止");
		}else
		{
			//DisplayValidateInfo("终止");
            DisplayValidateInfo("交易终止");
		}
		return  STATUS_FAIL;
    }
	//PrintOutTagList(&gAidParameterTagList, "gAidParameterTagList");
	appendTagList(&gAidParameterTagList, &runningTimeEnv.tagList);
	//PrintOutTagList(&runningTimeEnv.tagList, "runningTimeEnv.tagList");

	formatAmountByTag(&runningTimeEnv.tagList, TAG_AMOUNT, szAmt);
	sprintf(szDispAmt, "交易金额:%s\n", szAmt);

	qpbocSetTransSeqCounter(tempByteArray, &tempByteArrayLen);
	SetTagValue(TAG_TRANS_SEQ_COUNTER, tempByteArray, tempByteArrayLen, &runningTimeEnv.tagList);

#ifndef VPOS_APP	
	ret = WaitForNFC(szPrompt, szDispAmt);
	switch (ret)
	{
		case 0:
			//LOGD("WaitForNFC OK");
			break;

		case 1:
			LOGD("Timeout");
			//_vDisp(9, "终止");
			DisplayValidateInfo("终止,超时退出");
			return STATUS_OK;

		default:
            LOGD("Unknow status");
			return STATUS_FAIL;
	}
#else
    _vSetAllLed(1, 1, 0, 0);
#endif

	vSetQPbocTryFlag(1);
	//qpbocSetTransSeqCounter(tempByteArray, &tempByteArrayLen);
	//SetTagValue(TAG_TRANS_SEQ_COUNTER, tempByteArray, tempByteArrayLen, &runningTimeEnv.tagList);
	dbg("before qpbocSelectPPSE: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	dbg("Now get_tick = %d\n", get_tick());
	status = qpbocSelectPPSE();
	if (status != 0)
	{
		LOGD("qpbocSelectPPSE error\n");
		EmvContactlessIOSessionEnd();
	if(iGetQPbocTryFlag()==0)	
	{
		nfcLedFailedShow();
		//vDispCenter(2, pInfo, DISP_NORMAL);
		//_vDisp(9, "终止");
		//DisplayValidateInfo("终止,请移卡");
        	DisplayValidateInfo("交易终止,请移卡");
	}	
        return  STATUS_FAIL;
	}

	for (loop = 0; loop < aidList.count; loop ++ )
	{
		
		dbg("before qpbocInitTransaction: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
		status = qpbocInitTransaction(aidList.aidInfo[loop].aidName, aidList.aidInfo[loop].aidLength, 
			&runningTimeEnv.tagList);
		dbg("after qpbocInitTransaction: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	
		if (status != STATUS_OK)
		{
			if (status == STATUS_FAIL)
			{
				//Try next AID
				continue;
			}else if (status == STATUS_GPO_POWER_RESET_ERR)
			{
				//EmvContactlessIOSessionEnd();
				//nfc_close();
#ifdef 	NFC_IC_MH1XXX
				//pcd_antenna_off();
#endif

#ifdef NFC_IC_YC

#endif
				DisplayProcess("请查看手机并重新放置");
				mdelay(950);
				//////Line;
				return qpbocTransaction("请查看手机并重新放置");
			}else if (status == STATUS_GPO_NO_RESPONSE_ERR)
			{
				EmvContactlessIOSessionEnd();
				////Line;
				return qpbocTransaction(NULL);
			}
			else
			{
				//APDU error but not "GPO not accept" , don't try next AID
				LOGD("qpbocInitTransaction error\n");
				EmvContactlessIOSessionEnd();
				 if(iGetQPbocTryFlag()==0)
				{
					nfcLedFailedShow();
					DisplayValidateInfo("交易终止,请移卡");
				 }	
		        return  STATUS_FAIL;
			}
		}else
		{
			SetTagValue(TAG_ADF_NAME, aidList.aidInfo[loop].aidName, aidList.aidInfo[loop].aidLength, 
				&runningTimeEnv.tagList);
			SetTagValue(TAG_TERMINAL_AID, aidList.aidInfo[loop].aidName, aidList.aidInfo[loop].aidLength, 
				&runningTimeEnv.tagList);
			//LOGD("qpbocInitTransaction ok\n");
			break;
		}
	}

	//LOGD("loop = %d, aidList.count = %d", loop, aidList.count);
	if (loop >= aidList.count)
	{
		LOGE("Select all aid but no one sucessful");
		EmvContactlessIOSessionEnd();
		if(iGetQPbocTryFlag()==0)
		{
			nfcLedFailedShow();
			DisplayValidateInfo("交易终止,请移卡");
		}
		return  STATUS_FAIL;
	}

	dbg("before qpbocReadRecord: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	
	ret = qpbocReadRecord(&runningTimeEnv);
	#ifdef QPBOC_RETRY
	if(ret==STATUS_ERROR && iGetQPbocTryFlag())
	{
	        EmvContactlessIOSessionEnd();
	        return  STATUS_FAIL;
	 }
	 #endif
	 vSetQPbocTryFlag(0);
	BuzzerOn(50);    
	dbg("After qpbocReadRecord: %d\n", get_diff_tick(get_tick(), GetNfcStartTick()));
	qpbocSetReadRecordRet(ret);
#if 0    
    if(gl_ucPbocSimpleProc)
    {
        //pboc简易流程返回卡号
        if(ret==0)
        {
            //读卡成功
            
            return  STATUS_OK;
        }else
        {
            //读卡失败
            return  STATUS_FAIL;
        }
    }
#endif    
	//DisplayProcess("请移卡...");
	//mdelay(800);
	
#ifdef VPOS_APP    
    dbg("uiTransType=%04X\n", gl_TransRec.uiTransType);    

       //消费撤销或者预授权完成撤销，比对卡号是否一致
    if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
    {
        extern void vUnpackPan(uchar *psIn, uchar *pszPan);
        extern int iGetEnvCardId(unsigned char *pszCardId, unsigned char *pucCardSeqNo, unsigned char *pszTrack2);
        uchar szCardId1[20], szCardId2[20];
        uchar ucCSN;
        
        szCardId1[0]=0;
        iGetEnvCardId(szCardId1, &ucCSN, NULL);
        vUnpackPan(gl_TransRec.sPan, szCardId2);
        
        dbg("szCardId1=%s, szCardId2=%s, csn1=%02X, csn2=%02x\n", szCardId1, szCardId2, gl_TransRec.ucPanSerNo, ucCSN);
        if(strcmp((char*)szCardId1, (char*)szCardId2))
        {
            vClearLines(2);
            vMessage("卡号与原交易不一致");
            return STATUS_FAIL;
        }
  //      if(gl_TransRec.ucPanSerNo!=ucCSN)
 //       {
  //          vClearLines(2);
  //          vMessage("非原卡");
  //          return STATUS_FAIL;
   //     }
    }	
	
    //快捷流程
    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP
        || gl_TransRec.uiTransType==TRANS_TYPE_REFUND || gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID
        ||gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
    {
        /*
        EmvContactlessIOSessionEnd();
        {
            extern int iGetEnvCardId(unsigned char *pszCardId, unsigned char *pucCardSeqNo, unsigned char *pszTrack2);
            
            uchar szCardId[20+1], ucCardSeqNo;
            uchar szBuf[100];
            uchar szTrack2[50];
            int ret;
            
            szTrack2[0]=0;
            szCardId[0]=0;
            ret=iGetEnvCardId(szCardId, &ucCardSeqNo, szTrack2);
            dbg("qpboc cardid=[%s],CSN=%02X,T2=[%s]\n", szCardId, ucCardSeqNo, szTrack2);
        }
        */
        return STATUS_OK;
    }
#endif
    
	ret = qpbocPerformFDDA(&runningTimeEnv);
	qpbocSetPerformFddaRet(ret);

       if( gl_ucStandbyCardFlag == 2)
    	{
	       if(iGetAmount(NULL, "请输入金额",NULL, szTmp)<=0)
	      {        	
	      		return 1;
	       }
	        ulAmount = atol(szTmp);
  		  sprintf(szTmp, "金额:%lu.%02lu", ulAmount / 100, ulAmount % 100);
		 gl_TransRec.ulAmount = ulAmount;
	}
	   
	ret = qpbocPerformCvm(&runningTimeEnv);
	if (ret == STATUS_QPBOC_CVM_DECLINE)
	{
		////Line;
		EmvContactlessIOSessionEnd();
		nfcLedFailedShow();
 #ifndef VPOS_APP        
		if (TagDataIsMissing(&(runningTimeEnv.tagList), TAG_QPBOC_OFFLINE_BALANCE) == FALSE)
		{
			LOGD("TAG_QPBOC_OFFLINE_BALANCE Exist : 9F5D\n");
			TranslateHexToChars(GetTagValue(&(runningTimeEnv.tagList), TAG_QPBOC_OFFLINE_BALANCE), 
				GetTagValueSize(&(runningTimeEnv.tagList), TAG_QPBOC_OFFLINE_BALANCE), tempString);
			LOGD("tempString = %s", tempString);
			foramtAmount(tempString, tempFormatString);
			LOGD("tempFormatString = %s", tempFormatString);
			sprintf(balance, "余额:%s", tempFormatString);
			//sprintf(displayInfo, " %s\n 浣欓: %s", hostResult, tempFormatString);	
		}else
		{
			LOGD("No TAG_QPBOC_OFFLINE_BALANCE :No 9F5D \n");
			sprintf(balance, "%s", " ");	
		}
       
		sprintf(displayInfo, "拒绝,%s", balance);
#else
        sprintf(displayInfo, "交易失败");
#endif		
		DisplayValidateInfo(displayInfo);
		
		return  STATUS_FAIL;
	}else if(ret == STATUS_CANCEL)
	{
		EmvContactlessIOSessionEnd();
		nfcLedFailedShow();      
        if(iGetDispRetInfo())
            DisplayValidateInfo("交易失败");
		return  STATUS_FAIL;
	}

	qpbocTerminalAction(&runningTimeEnv);

	EmvContactlessIOSessionEnd();
	return STATUS_OK;
}


extern tti_int32				__SCR_Handle;
extern Fn_SCR_PowerOn 			__SCR_PowerOn;
extern Fn_SCR_PowerOff 		__SCR_PowerOff;
extern Fn_SCR_ExchangeApdu 	__SCR_ExchangeApdu;

tti_int32 Init_SmartCard(SCR_CallBacks * pScrCallbacks)
{
	__SCR_Handle = pScrCallbacks->m_SCR_Handle;
	__SCR_PowerOn = pScrCallbacks->m_SCR_PowerOn;
	__SCR_PowerOff = pScrCallbacks->m_SCR_PowerOff;
	__SCR_ExchangeApdu =pScrCallbacks->m_SCR_ExchangeApdu;

	return 0;
}
tti_int32 GetVersionCheckSum(tti_tchar *szCheckSum)
{
	tti_tchar terminalInfo[100] = {0};
	
	if (szCheckSum == NULL)
	{
		return -1;
	}

	strcpy(terminalInfo, "22A0D8C0F000F02001");
	strcat(terminalInfo, VERSION_NUMBER);
	
	sha1_csum((tti_byte *)terminalInfo, strlen(terminalInfo), (tti_byte *)szCheckSum);

	return 0;
}


tti_int32 GetTermCheckSum(tti_tchar *szCheckSum)
{
	tti_tchar terminalInfo[100] = {0};
	
	if (szCheckSum == NULL)
	{
		return -1;
	}

	strcpy(terminalInfo, "55A0D8C0F000F0200D");
	strcat(terminalInfo, VERSION_NUMBER);
	
	sha1_csum((tti_byte *)terminalInfo, strlen(terminalInfo), (tti_byte *)szCheckSum);

	return 0;
}


tti_int32 GetKernelVersion(tti_tchar *szVersion)
{
	if (szVersion == NULL)
	{
		return -1;
	}

	strcpy(szVersion, VERSION_NUMBER);
	return 0;
}


int gprs_at_GetLocationFlow(char *szLocation)
{	
	int ret;
	//char rcvData[1024];
	struct tm rtcTm;
/*
	gprs_poweron();
  	
	if (gprs_at_MoudleIsActive() == 0)
	{
		dbg("Need gprs_modulepin_reset()\n");
		gprs_modulepin_reset();
		if (gprs_at_MoudleIsActive() == 0)
		{
			dbg("Need gprs_modulepin_reset()\n");
			gprs_modulepin_reset();
			if (gprs_at_MoudleIsActive() == 0)
			{
				dbg("gprs_modulepin_reset() can't work\n");
				gprs_lcdDebug("AT", "No response");
				return -9;
			}
		}

		mdelay(5*1000);
	}else
	{
		dbg("No Need gprs_hardware_reset()\n");
	}
*/  
    if(gprs_at_poweron())
        return -9;

	ret = gprs_at_DisableEcho();
	if (ret < 0)
	{
		dbg("gprs_at_DisableEcho error\n");
		return -1;
	}
	
	ret = gprs_at_CheckSIM();
	if (ret < 0)
	{
		dbg("gprs_at_CheckSIM error\n");
		return -1;
	}
	
	ret = gprs_at_GetSignalQuality();
	if (ret < 0)
	{
		dbg("gprs_at_GetSignalQuality error\n");
		return -2;
	}

	ret = gprs_at_CheckRegister();
	if (ret < 0)
	{
		dbg("gprs_at_CheckRegister error\n");
		return -3;
	}

	ret = gprs_at_CheckGprsAttachment();
	if (ret < 0)
	{
		dbg("gprs_at_CheckGprsAttachment error\n");
		return -4;
	}

	ret = gprs_at_SetAPN(NULL, NULL, NULL);
	if (ret < 0)
	{
		dbg("gprs_at_SetAPN error\n");
		return -5;
	}

	ret = gprs_at_InitPDP();
	if (ret < 0)
	{
		dbg("gprs_at_InitPDP error\n");
		return -6;
	}

	ret = gprs_at_get_cclk(&rtcTm);
	if (ret < 0)
	{
		dbg("gprs_at_get_cclk error\n");
		return -6;
	}


	ret = gprs_at_location(szLocation);
	if (ret < 0)
	{
		dbg("gprs_at_location error\n");
		return -6;
	}
	
	ret = gprs_at_CloseGprs();
	if (ret < 0)
	{
		dbg("gprs_at_CloseGprs error\n");
		return -8;
	}

	return 0;
}
