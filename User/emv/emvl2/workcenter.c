	//#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "defines.h"
#include "global.h"
#include "parserequest.h"
#include "buildresponse.h"
#include "workcenter.h"
#include "misce.h"
#include "EMVL2.h"
#include "transflow.h"
#include "utils.h"
#include "debug.h"

tti_bool gQuickEmvFlag = FALSE;

void enableQuickEmv(void)
{
	gQuickEmvFlag = TRUE;
}

void disableQuickEmv(void)
{
	gQuickEmvFlag = FALSE;
}
	
tti_bool IsQuickEmv(void)
{
	return gQuickEmvFlag;
}

//------------------------------------------------------------------------------------------
void processIncLoadConfiguration(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	TerminalParam tempTermParam;
	//char tempreq[BUFFER_SIZE_10K];

	//printf("processIncLoadConfiguration jason 20140404\n");

	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';
	request[reqLen]='\0';

	memset(&tempTermParam, 0, sizeof(TerminalParam));

	//if (parseLoadConfiguration(tempreq, &tempTermParam)==STATUS_OK)
	if (parseLoadConfiguration(request, &tempTermParam)==STATUS_OK)
	{
		memcpy(&terminalParam, &tempTermParam, sizeof(TerminalParam));
		buildLoadConfigurationResponse(INC_STATUS_OK, (char *)response);
	}
	else
	{
		buildLoadConfigurationResponse(INC_STATUS_FORMAT_ERROR, (char *)response);
	}
	*respLen=strlen((char *)response);

	//printf("emv len= %ld, response =%s\n", *respLen, response);

}

//------------------------------------------------------------------------------------------
void processIncGetAidList(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	//char tempreq[BUFFER_SIZE_10K];
	//char tempreq[BUFFER_SIZE_1K];
	tti_int32 status;
	tti_int32 timeOut;

	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';

	//info("0");

	request[reqLen]='\0';

	if (parseGetAidList(request, &timeOut)!=STATUS_OK)
	{
		info("1");
		buildInvalidResponse("E1", INC_STATUS_FORMAT_ERROR, (tti_tchar *)response);
	}
	info("2");

	status=getAidList(&terminalParam, timeOut, &aidList);
	info("3");
    ReportLine();
	
	switch (status)
	{
		case STATUS_OK:
			if (aidList.count==0)
			{
                ReportLine();
				buildInvalidResponse("E1", INC_STATUS_NO_APP, (tti_tchar *)response);
			}
			else
			{
				buildGetAidListResponse(INC_STATUS_OK, &aidList, (tti_tchar *)response);
			}
			break;

		case STATUS_TIMEOUT:
			buildInvalidResponse("E1", INC_STATUS_TIMEOUT, (tti_tchar *)response);
			break;

		case STATUS_CARD_BLOCK:
			buildInvalidResponse("E1", INC_STATUS_CARD_BLOCKED, (tti_tchar *)response);
			break;

		case STATUS_APP_BLOCKED:
			buildInvalidResponse("E1", INC_STATUS_APP_BLOCKED, (tti_tchar *)response);
			break;

		case STATUS_CANCEL:
			buildInvalidResponse("E1", INC_STATUS_CANCELED, (tti_tchar *)response);
			break;
			
		default:
			buildInvalidResponse("E1", INC_STATUS_FAULTURE, (tti_tchar *)response);
			break;
	}

	*respLen=strlen((tti_tchar *)response);

}

extern TagList gAidParameterTagList;
extern Tag3ByteList gAidParaTag3ByteList;

//------------------------------------------------------------------------------------------
void processIncInitTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
//	char tempreq[BUFFER_SIZE_10K];
	tti_int32 status;

	//char *tempreq = NULL;

	//tempreq = malloc(BUFFER_SIZE_10K);
	//if(tempreq == NULL)
	//{
	//	err("malloc error");
	//	return;
	//}

	//DPRINTF("Enter processIncInitTransaction\n");

	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';

	request[reqLen]='\0';

	FreeTagList(&runningTimeEnv.tagList);
	initRunningTimeEnv(&runningTimeEnv);
	//if (parseInitTransaction(tempreq, &runningTimeEnv, &aidList)!=STATUS_OK)	
	//if (parseInitTransaction(request, &runningTimeEnv, &aidList, &gAidParameterTagList, &gAidParaTag3ByteList)!=STATUS_OK)
	if (IsQuickEmv() == TRUE)
	{
		status = parseInitQuickTransaction(request, &runningTimeEnv, &aidList);
	}else
	{
		status = parseInitTransaction(request, &runningTimeEnv, &aidList, &gAidParameterTagList, &gAidParaTag3ByteList);
	}
	if (status != STATUS_OK)	
	{
		FreeTagList(&gAidParameterTagList);
#ifdef VPOS_APP
		//ȷ�Ͽ���?
#endif
		buildInvalidResponse("E2", INC_STATUS_FORMAT_ERROR, (tti_tchar *)response);
	}
	else
	{
		FreeTagList(&gAidParameterTagList);
		switch (initTransaction(&terminalParam, &runningTimeEnv))
		{
			case STATUS_OK:
				runningTimeEnv.status=CARD_SESSION_SELECT_APP;
				buildInitTransactionResponse(INC_STATUS_OK, &runningTimeEnv, (tti_tchar *)response);
				break;

			case STATUS_GPO_NO_ACCEPTED:
				buildInvalidResponse("E2", INC_STATUS_GPO_NO_ACCDEPTED, (tti_tchar *)response);
				break;

			case STATUS_READ_RECORD_ERR:
				//buildInvalidResponse("E2", INC_STATUS_GPO_NO_ACCDEPTED, response);
				DPRINTF("STATUS_READ_RECORD_ERR \n");
				sprintf((tti_tchar *)response, "%s%s", "E2.", "a");
				break;

			case STATUS_FAIL:
			default:
				buildInvalidResponse("E2", INC_STATUS_FAULTURE, (tti_tchar *)response);
				break;
		}
	}

	*respLen=strlen((tti_tchar *)response);
}
//------------------------------------------------------------------------------------------
void processIncGetBalanceEnquiry(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	//char tempreq[BUFFER_SIZE_10K];
	
	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';
	request[reqLen] = '\0';

	if (parseGetBalanceEnquiry(request, &runningTimeEnv)!=STATUS_OK)
	{
		buildGetBalanceEnquiryResponse(INC_STATUS_FORMAT_ERROR, &runningTimeEnv, (tti_tchar *)response);
	}
	buildGetBalanceEnquiryResponse(INC_STATUS_OK, &runningTimeEnv, (tti_tchar *)response);
	*respLen=strlen((tti_tchar *)response);
}

//------------------------------------------------------------------------------------------
void processIncTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	//char tempreq[BUFFER_SIZE_10K];
	int iRet;

	
	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';
	request[reqLen]='\0';

	if (runningTimeEnv.status!=CARD_SESSION_SELECT_APP)
	{
		buildTransactionResponse(INC_STATUS_INVALID_COMM, &runningTimeEnv, (tti_tchar *)response);
	}
	else
	{
		if (parseTransaction((tti_tchar *)request, &runningTimeEnv)!=STATUS_OK)
		{
			buildTransactionResponse(INC_STATUS_FORMAT_ERROR, &runningTimeEnv, (tti_tchar *)response);
		}

		info("22\n");

		//Jason edit on 2013.05.22
		/*
		if (JudgeTransaction(&runningTimeEnv)==STATUS_OK)
		{
			runningTimeEnv.status=CARD_SESSION_TRANSACTION;
			buildTransactionResponse(INC_STATUS_OK, &runningTimeEnv, response);
		}
		else
		{
			buildTransactionResponse(INC_STATUS_FAULTURE, &runningTimeEnv, response);
		}
		*/
	


		iRet = JudgeTransaction(&runningTimeEnv);
		
		DPRINTF("JudgeTransaction return %d\n", iRet);
	
		if (iRet == STATUS_OK)
		{
			runningTimeEnv.status=CARD_SESSION_TRANSACTION;
			buildTransactionResponse(INC_STATUS_OK, &runningTimeEnv, (tti_tchar *)response);
		}
		else if (iRet == STATUS_CANCEL)
		{
			buildTransactionResponse(INC_STATUS_CANCELED, &runningTimeEnv, (tti_tchar *)response);
		}
		else
		{
			buildTransactionResponse(INC_STATUS_FAULTURE, &runningTimeEnv, (tti_tchar *)response);
		}
		//End

	}

	*respLen=strlen((tti_tchar *)response);

}


//------------------------------------------------------------------------------------------
void processIncComplete(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	//char tempreq[BUFFER_SIZE_10K];

	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';
	request[reqLen] = '\0';

	ReportLine();
	
	if (runningTimeEnv.status!=CARD_SESSION_TRANSACTION)
	{
		buildCompletionResponse(INC_STATUS_INVALID_COMM, &runningTimeEnv, (tti_tchar *)response);
	}
	else 
	{
		if (parseCompletion((tti_tchar *)request, &runningTimeEnv)!=STATUS_OK)
		{
			ReportLine();
			buildCompletionResponse(INC_STATUS_FORMAT_ERROR, &runningTimeEnv, (tti_tchar *)response);
		}
		else
		{
			if (TransactionComplete(&runningTimeEnv)==STATUS_OK)
			{
				buildCompletionResponse(INC_STATUS_OK, &runningTimeEnv, (tti_tchar *)response);
			}
			else
			{
				buildCompletionResponse(INC_STATUS_FAULTURE, &runningTimeEnv, (tti_tchar *)response);
			}
		}
	}

//Jason removed on 2017.03.01
	//FreeTagList(&runningTimeEnv.tagList);
//End
	*respLen=strlen((tti_tchar *)response);
}

//------------------------------------------------------------------------------------------
void processIncExecuteAPDU(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	return;
}

//------------------------------------------------------------------------------------------
void processIncGetTag(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	tti_uint16 tag;
	
	request[reqLen]='\0';

	if (parseGetTag((tti_tchar *)request, &tag)!=STATUS_OK)
	{
		//info("Get Tag: Parse Not OK\n");
		buildGetTagResponse(INC_STATUS_FORMAT_ERROR, 0, &runningTimeEnv, (tti_tchar *)response);
	}
	else
	{
		//info("Get Tag: Parse OK\n");
		buildGetTagResponse(INC_STATUS_OK, tag, &runningTimeEnv, (tti_tchar *)response);
	}

	*respLen=strlen((tti_tchar *)response);
}



//------------------------------------------------------------------------------------------
void processIncEndTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	//char tempreq[BUFFER_SIZE_10K];

	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';

	request[reqLen]='\0';

	//if (parseEndTransaction(tempreq, &runningTimeEnv)!=STATUS_OK)
	if (parseEndTransaction(request, &runningTimeEnv)!=STATUS_OK)
	{
		buildEndTransactionResponse(INC_STATUS_FORMAT_ERROR, (tti_tchar *)response);
	}

	EndTransaction(&runningTimeEnv);
	buildEndTransactionResponse(INC_STATUS_OK, (tti_tchar *)response);
	*respLen=strlen((tti_tchar *)response);
}

//------------------------------------------------------------------------------------------
void processIncGetCheckSum(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	//char tempreq[BUFFER_SIZE_10K];
	
	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';

	request[reqLen]='\0';

	//if (parseGetCheckSum(tempreq, &runningTimeEnv)!=STATUS_OK)
	if (parseGetCheckSum(request, &runningTimeEnv)!=STATUS_OK)
	{
		buildInvalidResponse("EV", INC_STATUS_FORMAT_ERROR, (tti_tchar *)response);
	}

	buildGetCheckSumResponse(INC_STATUS_OK, (tti_tchar *)response);
	*respLen=strlen((tti_tchar *)response);
}

void processIncReadLog(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	//char tempreq[BUFFER_SIZE_10K];
	//tti_int32 status;

	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';

	//memcpy(tempreq, request, reqLen);
	//tempreq[reqLen]='\0';

	request[reqLen]='\0';
	
	FreeTagList(&runningTimeEnv.tagList);
	initRunningTimeEnv(&runningTimeEnv);
	if (parseReadLogTransaction(request, &runningTimeEnv, &aidList)!=STATUS_OK)
	{
		buildInvalidResponse("EL", INC_STATUS_FORMAT_ERROR, (tti_tchar *)response);
	}
	else
	{
		switch (ICCReadLogTransaction(&terminalParam, &runningTimeEnv))
		{
			case STATUS_OK:
				//runningTimeEnv.status=CARD_SESSION_SELECT_APP;
				buildInvalidResponse("EL", INC_STATUS_OK, (tti_tchar *)response);
				break;

			case STATUS_FAIL:
			default:
				buildInvalidResponse("EL", INC_STATUS_FAULTURE, (tti_tchar *)response);
				break;
		}
	}
	
	*respLen=strlen((tti_tchar *)response);
}

void processIncReadECBalance(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
//	char tempreq[BUFFER_SIZE_10K];
//	tti_int32 status;

//	DPRINTF("Enter processIncReadLog\n");

//	memcpy(tempreq, request, reqLen);
//	tempreq[reqLen]='\0';

	request[reqLen]='\0';

	FreeTagList(&runningTimeEnv.tagList);
	initRunningTimeEnv(&runningTimeEnv);
	if (parseReadLogTransaction(request, &runningTimeEnv, &aidList)!=STATUS_OK)
	{
		buildInvalidResponse("EE", INC_STATUS_FORMAT_ERROR, (tti_tchar *)response);
	}
	else
	{
		switch (ReadECBalance(&terminalParam, &runningTimeEnv))
		{
			case STATUS_OK:
				//runningTimeEnv.status=CARD_SESSION_SELECT_APP;
				buildInvalidResponse("EE", INC_STATUS_OK, (tti_tchar *)response);
				break;

			case STATUS_FAIL:
			default:
				buildInvalidResponse("EE", INC_STATUS_FAULTURE, (tti_tchar *)response);
				break;
		}
	}
	
	*respLen=strlen((tti_tchar *)response);
}


void processIncPINValueFromUI(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
		return;
}

/*
void ParseSwipCardData(int Indictor,const MSRTRACKPINPAD* ptrack, byte *OutBuff, int *pOutLen)
{
	int iOffSet =0;
	int iTrackFlag1 = 0;
	int iTrackFlag2 = 0;
	int iTrackFlag3 = 0;

	if((Indictor==1) || (Indictor==3) || (Indictor==5))
		iTrackFlag1 = 1;

	if((Indictor==1) || (Indictor==2) || (Indictor==3) || (Indictor==6))
		iTrackFlag2 = 1;

	if((Indictor==2) || (Indictor==3))
		iTrackFlag3 = 1;

	// Copy the track date, removing start/end sentinel and LRC.
		if(iTrackFlag1 == 1)
		{
			memcpy(OutBuff+iOffSet, "{1}", 3);
			iOffSet += 3;
			if(ptrack[0].len > 3)
			{
				memcpy(OutBuff+iOffSet, ptrack[0].data+1, ptrack[0].len-3);
				iOffSet += ptrack[0].len - 3;
			}
		}
		if(iTrackFlag2 == 1)
		{
			memcpy(OutBuff + iOffSet, "{2}", 3);
			iOffSet += 3;
			if(ptrack[1].len > 3)
			{
				memcpy(OutBuff+iOffSet, ptrack[1].data+1, ptrack[1].len-3);
				iOffSet += ptrack[1].len - 3;
			}
		}
		if(iTrackFlag3 == 1)
		{
			memcpy(OutBuff + iOffSet, "{3}", 3);
			iOffSet += 3;
			if(ptrack[2].len > 3)
			{
				memcpy(OutBuff+iOffSet, ptrack[2].data+1, ptrack[2].len-3);
				iOffSet += ptrack[2].len - 3;
			}
		}
		*pOutLen = iOffSet;
}*/

//{E9.} +  {process code} + {card type} + { card status}
//Command	A6



void EMVL2_LoadConfiguration(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	processIncLoadConfiguration(request + 3, reqLen - 3, response, respLen);
}


void EMVL2_GetAidList(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	processIncGetAidList(request + 3, reqLen - 3, response, respLen);
}


void EMVL2_InitTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	processIncInitTransaction(request + 3, reqLen - 3, response, respLen);
}


void EMVL2_GetBalanceEnquiry(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	processIncGetBalanceEnquiry(request + 3, reqLen - 3, response, respLen);
}

void EMVL2_Transaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	processIncTransaction(request + 3, reqLen - 3, response, respLen);
}

void EMVL2_Complete(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	processIncComplete(request + 3, reqLen - 3, response, respLen);
}


void EMVL2_GetTag(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	processIncGetTag(request + 3, reqLen - 3, response, respLen);
}


void EMVL2_EndTransaction(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	processIncEndTransaction(request + 3, reqLen - 3, response, respLen);
}

void EMVL2_GetCheckSum(tti_byte *request, int reqLen, tti_byte *response, int *respLen)
{
	processIncGetCheckSum(request + 3, reqLen - 3, response, respLen);
}

tti_int32 EMVL2_Init_SmartCard(SCR_CallBacks * pScrCallbacks)
{
	return Init_SmartCard(pScrCallbacks);
}

typedef void (*CMDFUNC)(tti_byte *, int, tti_byte *, int *);

typedef struct {
	char *name;
	CMDFUNC func;
} EMVL2_SCommand;


EMVL2_SCommand emvl2_cmdlist[] = {
	
	{"E0.", processIncLoadConfiguration},
	{"E1.", processIncGetAidList},
	{"E2.", processIncInitTransaction},
	{"E4.", processIncTransaction},
	{"E5.", processIncComplete},
	{"E6.", processIncExecuteAPDU},
	{"E7.", processIncGetTag},
	{"E8.", processIncGetBalanceEnquiry},
	{"ED.", processIncEndTransaction},
	{"EV.", processIncGetCheckSum},
	{"EE.", processIncReadECBalance},
	{"EL.", processIncReadLog},		
};

void EMVL2_Cmd(tti_byte *InBuff, int InLen, tti_byte *OutBuff, int *OutLen)
{
	int cnt;
	int commandnumber = sizeof(emvl2_cmdlist) / sizeof(EMVL2_SCommand);

	if (InLen < 3)
	{
		memcpy(OutBuff, "XX.F", 4);
		*OutLen = 4;
		return;
	}

	for (cnt=0; cnt<commandnumber; cnt++)
	{
		if(memcmp(InBuff, emvl2_cmdlist[cnt].name, 3) == 0)
		{
			LOGD("emvl2_cmdlist[cnt].name = [%s]\n", emvl2_cmdlist[cnt].name);
			InLen -= 3;
			emvl2_cmdlist[cnt].func(InBuff+3, InLen, OutBuff, OutLen);
			break;
		}
	}

	if (cnt >= commandnumber)
	{
		memcpy(OutBuff, InBuff, 3);
		OutBuff[3] = 'F';
		*OutLen = 4;
	}
	
	return;
}


    tti_int32 EMVL2_BulidTLVForTagList(tti_uint16 tagList[], tti_int32 tagListCount, tti_byte *TLVData, tti_uint16 *TLVDataLen)
    {
        tti_int32 ret;
        tti_int32 loop;
        tti_uint16 index = 0;
        tti_uint16 tmpLen = 0;


        if ((runningTimeEnv.status!=CARD_SESSION_SELECT_APP)&&
            (runningTimeEnv.status!=CARD_SESSION_TRANSACTION))
        {
            return STATUS_FAIL;
        }

        for (loop = 0; loop < tagListCount; loop++)
        {
            if (TagIsExisted(&(runningTimeEnv.tagList), tagList[loop] != TRUE))
            {
                continue;
            }

            ret = TagBuildTLV(&(runningTimeEnv.tagList),tagList[loop], TLVData + index, &tmpLen);
            if (ret == STATUS_OK)
            {
                index += tmpLen;
            }
        }

        *TLVDataLen = index;

        return STATUS_OK;
    }


