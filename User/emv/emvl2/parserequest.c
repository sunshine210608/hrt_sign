#include <string.h>
#include "parserequest.h"
#include "defines.h"
#include "misce.h"
#include "utils.h"
#include "tag.h"
#include "sha1.h"
#include "emvDebug.h"
#include "Transaction.h"
//#include "util.h"

#define BCTC_EMVL2_DEBUG	

//------------------------------------------------------------------------------------------
tti_int32 parseLoadConfiguration(char *request, TerminalParam *param)
{
	tti_int32 counter;

	counter=0;

	if (parseGetFixedString(request+counter, IFD_SERIAL_NUMBER_SIZE,
		param->IFDSerialNumber)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=IFD_SERIAL_NUMBER_SIZE;

	if (parseGetFixedString(request+counter, COUNTRY_CODE_SIZE, param->terminalCountryCode)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=COUNTRY_CODE_SIZE;

	if (parseGetByteArray(request+counter, 1, &param->terminalType)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=2;

	//Jason modified on 2017.02.23
	//if (parseGetByteArray(request+counter, TERMINAL_CAPABILITIES_SIZE, &param->terminalCapabilities)
	//	!=STATUS_OK)
	if (parseGetByteArray(request+counter, TERMINAL_CAPABILITIES_SIZE, param->terminalCapabilities)
		!=STATUS_OK)
	//End
	{
		return STATUS_FAIL;
	}
	counter+=TERMINAL_CAPABILITIES_SIZE*2;
	
	//Jason modified on 2017.02.23
	//if (parseGetByteArray(request+counter, ADDTIONAL_TERMINAL_CAPABILITIES_SIZE, &param->addtionalTerminalCapabilities)
	//	!=STATUS_OK)
	//Jason modified on 2017.02.23
	if (parseGetByteArray(request+counter, ADDTIONAL_TERMINAL_CAPABILITIES_SIZE, param->addtionalTerminalCapabilities)
		!=STATUS_OK)
	//End
	{
		return STATUS_FAIL;
	}	
	
	counter+=ADDTIONAL_TERMINAL_CAPABILITIES_SIZE*2;

	if (parseGetBool(request+counter, &param->supportPSE)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter++;

	param->CandidateAidList.count=0;

	while (TRUE)
	{
		if (parseGetCandidateAid(request+counter, 
			&param->CandidateAidList.candidateAidInfo[param->CandidateAidList.count])!=STATUS_OK)
		{
			return STATUS_FAIL;
		}

		counter+=param->CandidateAidList.candidateAidInfo[param->CandidateAidList.count].aidLength*2
			+1+APP_VERSION_NUMBER_SIZE*2;

		(param->CandidateAidList.count)++;
		if (param->CandidateAidList.count>=CANDIDATE_AID_NUMBER)
		{
			return STATUS_FAIL;
		}

		if (*(request+counter)==SEPERATOR)
		{
			counter++;
		}
		else
		{
			break;
		}
	}
		
	if (counter==strlen(request))
	{
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetAidList(char *request, tti_int32 *timeOut)
{
	tti_int32 counter;

	counter=0;
	
	if (parseGetInt32(request+counter, 3, timeOut)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=3;

	if (counter==strlen(request))
	{
		return STATUS_OK;
	}
	else
	{
		return STATUS_FAIL;
	}
	
}

//Jason added 2012.04.17, start
//------------------------------------------------------------------------------------------
tti_int32 parseReadLogTransaction(char *request, RunningTimeEnv *runningTimeEnv, AID_LIST *aidList)
{
	tti_int32 counter;
	tti_byte tempByteArray[BUFFER_SIZE_256BYTE];
	tti_int32 tempByteArrayLen;
	tti_int32 index;
	
	counter = 0;

	tempByteArrayLen=AID_NAME_SIZE;
	if (parseGetUnfixedByteArray(request+counter, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("TAG_TERMINAL_AID error\n");
		return STATUS_FAIL;
	}
	SetTagValue(TAG_TERMINAL_AID, tempByteArray, tempByteArrayLen, &runningTimeEnv->tagList);
	counter+=tempByteArrayLen*2+1;

	for (index=0; index<aidList->count; index++)
	{
		if ((memcmp(tempByteArray, aidList->aidInfo[index].aidName, 
			aidList->aidInfo[index].aidLength)==0)&&
			(tempByteArrayLen==aidList->aidInfo[index].aidLength))
		{
			SetTagValue(TAG_APP_VERSION_NUMBER, aidList->aidInfo[index].verionNumber,
				APP_VERSION_NUMBER_SIZE, &runningTimeEnv->tagList);
		}
	}

	return STATUS_OK;		
}
//End

//#ifndef BCTC_EMVL2_DEBUG
//------------------------------------------------------------------------------------------
tti_int32 parseInitQuickTransaction(char *request, RunningTimeEnv *runningTimeEnv, AID_LIST *aidList)
{
	tti_int32 counter;
//	tti_byte tempDolBuff[DOL_SIZE];
//	tti_int32 tempDolBuffLen;
	tti_int32 len, index;
	tti_int32 tempInt32Data;
	tti_byte tempByteArray[BUFFER_SIZE_256BYTE];
	tti_int32 tempByteArrayLen;
	tti_tchar tempCharArray[BUFFER_SIZE_256BYTE+1];

	DPRINTF("Call parseInitTransaction \n");

	counter=0;
	
	if (parseGetAmount(request+counter, &tempInt32Data)!=STATUS_OK)
	{
		DPRINTF("Amount error\n");
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;
	
	castAmountToBCDArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_AMOUNT, tempByteArray, AMOUNT_SIZE_IN_TAG, &runningTimeEnv->tagList);

	castInt32ToByteArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_AMOUNT_BINARY, tempByteArray, 4, &runningTimeEnv->tagList);

	if (parseGetAmount(request+counter, &tempInt32Data)!=STATUS_OK)
	{
		DPRINTF("Other Amount error\n");
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;

	castAmountToBCDArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_AMOUNT_OTHER, tempByteArray, AMOUNT_SIZE_IN_TAG, &runningTimeEnv->tagList);

	castInt32ToByteArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_AMOUNT_OTHER_BINARY, tempByteArray, 4, &runningTimeEnv->tagList);

	if (parseGetDate(request+counter, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("Date error\n");
		return STATUS_FAIL;
	}
	counter+=DATE_SIZE;

	SetTagValue(TAG_TRANSACTION_DATE, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);

	if (parseGetTime(request+counter, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("Time error\n");
		return STATUS_FAIL;
	}
	counter+=TIME_SIZE;

	SetTagValue(TAG_TRANSACTION_TIME, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);
	
	//if (parseGetInt8(request+counter, 2, tempByteArray)!=STATUS_OK)
	if (parseGetByteArray(request+counter, 1, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANSACTION_TYPE error\n");
		return STATUS_FAIL;
	}
	counter+=2;
	
	SetTagValue(TAG_TRANSACTION_TYPE, tempByteArray, 1, &runningTimeEnv->tagList);

	if (parseGetInt8(request+counter, 2, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_POS_ENTRY_MODE error\n");
		return STATUS_FAIL;
	}
	counter+=2;

	SetTagValue(TAG_POS_ENTRY_MODE, tempByteArray, 1, &runningTimeEnv->tagList);

	//Jason modified on 2012.04.23, start
	//if (parseGetFixedString(request+counter, 5, tempCharArray)!=STATUS_OK)
	if (parseGetFixedString(request+counter, 8, tempCharArray)!=STATUS_OK)
	//End
	{
		DPRINTF("TAG_TRANS_SEQ_COUNTER error\n");
		return STATUS_FAIL;
	}
	//Jason modified on 2012.04.23, start
	//counter+=5;
	counter+=8;
	//End
	FormatString(tempCharArray, 8, '0', MESSAGE_RIGHT_JUSTIFY);
	
	if (TranslateStringToBCDArray(tempCharArray, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("TranslateStringToBCDArray error\n");
		return STATUS_FAIL;
	}

	SetTagValue(TAG_TRANS_SEQ_COUNTER, tempByteArray, 4, &runningTimeEnv->tagList);

	if (parseGetAccountType(request+counter, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_ACCOUNT_TYPE error\n");
		return STATUS_FAIL;
	}
	counter+=ACCOUNT_TYPE_SIZE;

	SetTagValue(TAG_ACCOUNT_TYPE, tempByteArray, 1, &runningTimeEnv->tagList);

	if (parseGetAmount(request+counter, &tempInt32Data)!=STATUS_OK)
	{
		DPRINTF("TAG_FLOOR_LIMIT error\n");
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;

	castInt32ToByteArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_FLOOR_LIMIT, tempByteArray, 4, &runningTimeEnv->tagList);

	if (parseGetInt32(request+counter, 2, &runningTimeEnv->commonRunningData.targetPercentage)!=STATUS_OK)
	{
		DPRINTF("targetPercentage error\n");
		return STATUS_FAIL;
	}
	counter+=2;

	if (parseGetInt32(request+counter, 2, &runningTimeEnv->commonRunningData.maxTargetPercentage)!=STATUS_OK)
	{
		DPRINTF("maxTargetPercentage error\n");
		return STATUS_FAIL;
	}
	counter+=2;

	if (parseGetAmount(request+counter, &runningTimeEnv->commonRunningData.thresholdAmount)!=STATUS_OK)
	{
		DPRINTF("thresholdAmount error\n");
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;	

	if (parseGetFixedString(request+counter, MERCHANT_CATEGORY_CODE_SIZE, 
		tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_MERCHANT_CATEGORY_CODE error\n");
		return STATUS_FAIL;
	}
	counter+=MERCHANT_CATEGORY_CODE_SIZE;

	tempByteArrayLen=sizeof(tempByteArray);
	TranslateStringToBCDArray(tempCharArray, tempByteArray, &tempByteArrayLen);
	SetTagValue(TAG_MERCHANT_CATEGORY_CODE, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);
	
	if (parseGetFixedString(request+counter, MERCHANT_ID_SIZE, tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_MERCHANT_ID error\n");
		return STATUS_FAIL;
	}
	counter+=MERCHANT_ID_SIZE;

	SetTagValue(TAG_MERCHANT_ID, tempCharArray, MERCHANT_ID_SIZE, &runningTimeEnv->tagList);
	
	if (parseGetFixedString(request+counter, TERMINAL_ID_SIZE, 	tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TERMINAL_ID error\n");
		return STATUS_FAIL;
	}
	counter+=TERMINAL_ID_SIZE;

	SetTagValue(TAG_TERMINAL_ID, tempCharArray, TERMINAL_ID_SIZE, &runningTimeEnv->tagList);

	if (parseGetFixedString(request+counter, CURRENCY_CODE_SIZE, tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANS_CURRENCY_CODE error\n");
		return STATUS_FAIL;
	}
	counter+=CURRENCY_CODE_SIZE;

	TranslateStringToBCDArray_FillLeftZero(tempCharArray, tempByteArray, &tempByteArrayLen);
	SetTagValue(TAG_TRANS_CURRENCY_CODE, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);

	if (parseGetInt8(request+counter, 1, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANS_CURRENCY_EXP error\n");
		return STATUS_FAIL;
	}
	counter++;

	SetTagValue(TAG_TRANS_CURRENCY_EXP, tempByteArray, 1, &runningTimeEnv->tagList);

	if (parseGetFixedString(request+counter, CURRENCY_CODE_SIZE, tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANS_REF_CURRENCY_CODE error\n");
		return STATUS_FAIL;
	}
	counter+=CURRENCY_CODE_SIZE;

	TranslateStringToBCDArray_FillLeftZero(tempCharArray, tempByteArray, &tempByteArrayLen);
	SetTagValue(TAG_TRANS_REF_CURRENCY_CODE, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);

	if (parseGetInt8(request+counter, 1, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANS_REF_CURRENCY_EXP error\n");
		return STATUS_FAIL;
	}
	counter++;

	SetTagValue(TAG_TRANS_REF_CURRENCY_EXP, tempByteArray, 1, &runningTimeEnv->tagList);

	if (parseGetInt8(request+counter, 1, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_VLP error\n");
		return STATUS_FAIL;
	}
	counter++;

	if (((*tempByteArray)!=0)&&((*tempByteArray)!=1))
	{
		DPRINTF("TAG_VLP value error \n");
		return STATUS_FAIL;
	}

	SetTagValue(TAG_VLP, tempByteArray, 1, &runningTimeEnv->tagList);

	if (parseGetInt32(request+counter, 1, &runningTimeEnv->commonRunningData.pinBypassMode)
		!=STATUS_OK)
	{
		DPRINTF("pinBypassMode error\n");
		return STATUS_FAIL;
	}
	if ((runningTimeEnv->commonRunningData.pinBypassMode!=PIN_BYPASS_NO_ALLOW)&&
		(runningTimeEnv->commonRunningData.pinBypassMode!=PIN_BYPASS_CURR_ONLY)&&
		(runningTimeEnv->commonRunningData.pinBypassMode!=PIN_BYPASS_NEXT_ALLOW))
	{
		DPRINTF("pinBypassMode value error\n");
		return STATUS_FAIL;
	}
	counter++;

	tempByteArrayLen=sizeof(tempByteArray);
	if (parseGetUnfixedByteArray(request+counter, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("TAG_TERMINAL_RISK_DATA error\n");
		return STATUS_FAIL;
	}
	if (tempByteArrayLen>8)
	{
		DPRINTF("TAG_TERMINAL_RISK_DATA value error\n");
		return STATUS_FAIL;
	}
	counter+=tempByteArrayLen*2+1;

	if (tempByteArrayLen>0)
	{
		SetTagValue(TAG_TERMINAL_RISK_DATA, tempByteArray, tempByteArrayLen, 
			&runningTimeEnv->tagList);
	}
	
	len=GetUnFixMessageLength(request+counter, strlen(request)-counter, '|');
	if (len>64)
	{
		DPRINTF("TAG_MERCHANT_LOCATION error\n");
		return STATUS_FAIL;
	}
	strncpy(tempCharArray, request+counter, len);
	tempCharArray[len]='\0';
	counter+=len+1;

	if (len>0)
	{
		SetTagValue(TAG_MERCHANT_LOCATION, tempCharArray, len, &runningTimeEnv->tagList);
	}
	
	tempByteArrayLen=AID_NAME_SIZE;
	if (parseGetUnfixedByteArray(request+counter, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("TAG_TERMINAL_AID error\n");
		return STATUS_FAIL;
	}
	SetTagValue(TAG_TERMINAL_AID, tempByteArray, tempByteArrayLen, &runningTimeEnv->tagList);
	counter+=tempByteArrayLen*2+1;

	for (index=0; index<aidList->count; index++)
	{
		if ((memcmp(tempByteArray, aidList->aidInfo[index].aidName, 
			aidList->aidInfo[index].aidLength)==0)&&
			(tempByteArrayLen==aidList->aidInfo[index].aidLength))
		{
			SetTagValue(TAG_APP_VERSION_NUMBER, aidList->aidInfo[index].verionNumber,
				APP_VERSION_NUMBER_SIZE, &runningTimeEnv->tagList);
		}
	}

	len=GetUnFixMessageLength(request+counter, strlen(request)-counter, '|');
	if ((len<6)||(len>11))
	{
		DPRINTF("TAG_ACQUIRER_ID error\n");
		return STATUS_FAIL;
	}
	strncpy(tempCharArray, request+counter, len);
	tempCharArray[len]='\0';
	counter+=len+1;

	//Jason added on 2013529, start
	TranslateStringToBCDArray_FillLeftZero_WithLen(tempCharArray, tempByteArray, &tempByteArrayLen, 12);
	SetTagValue(TAG_ACQUIRER_ID, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);
	
	//End

	//Jason remove on 2013529, start
	//SetTagValue(TAG_ACQUIRER_ID, tempCharArray, len, &runningTimeEnv->tagList);
	//end
	
	len=TAC_SIZE;
	if (parseGetActionCode(request+counter, 
		runningTimeEnv->commonRunningData.defaultTerminalActionCode, &len)!=STATUS_OK)
	{
		DPRINTF("defaultTerminalActionCode error\n");
		return STATUS_FAIL;
	}
	counter+=len*2+1;

	len=TAC_SIZE;
	if (parseGetActionCode(request+counter, 
		runningTimeEnv->commonRunningData.denialTerminalActionCode, &len)!=STATUS_OK)
	{
		DPRINTF("denialTerminalActionCode error\n");
		return STATUS_FAIL;
	}
	counter+=len*2+1;

	len=TAC_SIZE;
	if (parseGetActionCode(request+counter, 
		runningTimeEnv->commonRunningData.onlineTerminalActionCode, &len)!=STATUS_OK)
	{
		DPRINTF("onlineTerminalActionCode error\n");
		return STATUS_FAIL;
	}
	counter+=len*2+1;

	runningTimeEnv->commonRunningData.defaultTDOL.len=DOL_SIZE;
	if (parseGetUnfixedByteArray(request+counter, runningTimeEnv->commonRunningData.defaultTDOL.value,
		&runningTimeEnv->commonRunningData.defaultTDOL.len)!=STATUS_OK)
	{
		DPRINTF("defaultTDOL error\n");
		return STATUS_FAIL;
	}
	counter+=runningTimeEnv->commonRunningData.defaultTDOL.len*2+1;

	runningTimeEnv->commonRunningData.defaultDDOL.len=DOL_SIZE;
	if (parseGetUnfixedByteArray(request+counter, runningTimeEnv->commonRunningData.defaultDDOL.value,
		&runningTimeEnv->commonRunningData.defaultDDOL.len)!=STATUS_OK)
	{
		DPRINTF("defaultDDOL error\n");
		return STATUS_FAIL;
	}
	counter+=runningTimeEnv->commonRunningData.defaultDDOL.len*2;

	return STATUS_OK;
/*
	if (counter!=strlen(request))
	{
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
*/	
}

//#else

//------------------------------------------------------------------------------------------
tti_int32 parseInitTransaction(char *request, RunningTimeEnv *runningTimeEnv, AID_LIST *aidList, 
	TagList *aidParaTagList, Tag3ByteList *aidParaTag3ByteList)
{
	tti_int32 counter;
//	tti_byte tempDolBuff[DOL_SIZE];
//	tti_int32 tempDolBuffLen;
	tti_int32 index;
	tti_int32 tempInt32Data;
	tti_byte tempByteArray[BUFFER_SIZE_256BYTE];
	tti_int32 tempByteArrayLen;
	tti_tchar tempCharArray[BUFFER_SIZE_256BYTE+1];
	tti_uint16 tagId;

	DPRINTF("Call parseInitTransaction \n");

	counter=0;
	
	if (parseGetAmount(request+counter, &tempInt32Data)!=STATUS_OK)
	{
		DPRINTF("Amount error\n");
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;
	
	castAmountToBCDArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_AMOUNT, tempByteArray, AMOUNT_SIZE_IN_TAG, &runningTimeEnv->tagList);

	castInt32ToByteArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_AMOUNT_BINARY, tempByteArray, 4, &runningTimeEnv->tagList);

	if (parseGetAmount(request+counter, &tempInt32Data)!=STATUS_OK)
	{
		DPRINTF("Other Amount error\n");
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;

	castAmountToBCDArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_AMOUNT_OTHER, tempByteArray, AMOUNT_SIZE_IN_TAG, &runningTimeEnv->tagList);

	castInt32ToByteArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_AMOUNT_OTHER_BINARY, tempByteArray, 4, &runningTimeEnv->tagList);

	if (parseGetDate(request+counter, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("Date error\n");
		return STATUS_FAIL;
	}
	counter+=DATE_SIZE;

	SetTagValue(TAG_TRANSACTION_DATE, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);

	if (parseGetTime(request+counter, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("Time error\n");
		return STATUS_FAIL;
	}
	counter+=TIME_SIZE;

	SetTagValue(TAG_TRANSACTION_TIME, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);
	
	//if (parseGetInt8(request+counter, 2, (tti_int8 *)tempByteArray)!=STATUS_OK)
    if (parseGetByteArray(request+counter, 1, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANSACTION_TYPE error\n");
		return STATUS_FAIL;
	}
	counter+=2;

	SetTagValue(TAG_TRANSACTION_TYPE, tempByteArray, 1, &runningTimeEnv->tagList);

	if (parseGetInt8(request+counter, 2, (tti_int8 *)tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_POS_ENTRY_MODE error\n");
		return STATUS_FAIL;
	}
	counter+=2;

	SetTagValue(TAG_POS_ENTRY_MODE, tempByteArray, 1, &runningTimeEnv->tagList);

	//Jason modified on 2012.04.23, start
	//if (parseGetFixedString(request+counter, 5, tempCharArray)!=STATUS_OK)
	if (parseGetFixedString(request+counter, 8, tempCharArray)!=STATUS_OK)
	//End
	{
		DPRINTF("TAG_TRANS_SEQ_COUNTER error\n");
		return STATUS_FAIL;
	}
	//Jason modified on 2012.04.23, start
	//counter+=5;
	counter+=8;
	//End
	FormatString(tempCharArray, 8, '0', MESSAGE_RIGHT_JUSTIFY);
	
	if (TranslateStringToBCDArray(tempCharArray, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("TranslateStringToBCDArray error\n");
		return STATUS_FAIL;
	}

	SetTagValue(TAG_TRANS_SEQ_COUNTER, tempByteArray, 4, &runningTimeEnv->tagList);

	if (parseGetAccountType(request+counter, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_ACCOUNT_TYPE error\n");
		return STATUS_FAIL;
	}
	counter+=ACCOUNT_TYPE_SIZE;

	SetTagValue(TAG_ACCOUNT_TYPE, tempByteArray, 1, &runningTimeEnv->tagList);

/*
	if (parseGetAmount(request+counter, &tempInt32Data)!=STATUS_OK)
	{
		DPRINTF("TAG_FLOOR_LIMIT error\n");
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;

	castInt32ToByteArray(tempInt32Data, tempByteArray);
	SetTagValue(TAG_FLOOR_LIMIT, tempByteArray, 4, &runningTimeEnv->tagList);
*/
	DPRINTF("List TAG_FLOOR_LIMIT\n");
	hexdump(GetTagValue(aidParaTagList, TAG_FLOOR_LIMIT),
		GetTagValueSize(aidParaTagList, TAG_FLOOR_LIMIT));
	SetTagValue(TAG_FLOOR_LIMIT, GetTagValue(aidParaTagList, TAG_FLOOR_LIMIT), 
		GetTagValueSize(aidParaTagList, TAG_FLOOR_LIMIT), &runningTimeEnv->tagList);

/*
	if (parseGetInt32(request+counter, 2, &runningTimeEnv->commonRunningData.targetPercentage)!=STATUS_OK)
	{
		DPRINTF("targetPercentage error\n");
		return STATUS_FAIL;
	}
	counter+=2;
*/
	TranslateHexToChars(GetTagValue(aidParaTagList, TAG_TARGET_PERCENTAGE_BCTC), 
		GetTagValueSize(aidParaTagList, TAG_TARGET_PERCENTAGE_BCTC), tempCharArray);
	if (strlen(tempCharArray) != 2)
	{
		DPRINTF("LEN != 2");
		DPRINTF("targetPercentage error\n");
		return STATUS_FAIL;
	}
	if (parseGetInt32(tempCharArray, strlen(tempCharArray), &runningTimeEnv->commonRunningData.targetPercentage)!=STATUS_OK)
	{
		DPRINTF("targetPercentage error\n");
		return STATUS_FAIL;
	}

/*
	if (parseGetInt32(request+counter, 2, &runningTimeEnv->commonRunningData.maxTargetPercentage)!=STATUS_OK)
	{
		DPRINTF("maxTargetPercentage error\n");
		return STATUS_FAIL;
	}
	counter+=2;
*/
	
	 
	TranslateHexToChars(GetTagValue(aidParaTagList, TAG_TARGET_MAX_PERCENTAGE_BCTC), 
		GetTagValueSize(aidParaTagList, TAG_TARGET_MAX_PERCENTAGE_BCTC), tempCharArray);
	if (strlen(tempCharArray) != 2)
	{
		DPRINTF("LEN != 2");
		DPRINTF("maxTargetPercentage error\n");
		return STATUS_FAIL;
	}
	if (parseGetInt32(tempCharArray, strlen(tempCharArray), &runningTimeEnv->commonRunningData.maxTargetPercentage)!=STATUS_OK)
	{
		DPRINTF("maxTargetPercentage error\n");
		return STATUS_FAIL;
	}
	
/*
	if (parseGetAmount(request+counter, &runningTimeEnv->commonRunningData.thresholdAmount)!=STATUS_OK)
	{
		DPRINTF("thresholdAmount error\n");
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;	
*/
	tagId = TAG_THRESHOLD_AMOUNT_BCTC;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	runningTimeEnv->commonRunningData.thresholdAmount = getAmountByTag(aidParaTagList, TAG_THRESHOLD_AMOUNT_BCTC);
	

	tagId = TAG_MERCHANT_CATEGORY_CODE;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
#ifndef VPOS_APP        
		return STATUS_FAIL;
#endif
	}else
    {
        SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
            GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);
    }
/*
	if (parseGetFixedString(request+counter, MERCHANT_CATEGORY_CODE_SIZE, 
		tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_MERCHANT_CATEGORY_CODE error\n");
		return STATUS_FAIL;
	}
	counter+=MERCHANT_CATEGORY_CODE_SIZE;

	tempByteArrayLen=sizeof(tempByteArray);
	TranslateStringToBCDArray(tempCharArray, tempByteArray, &tempByteArrayLen);
	SetTagValue(TAG_MERCHANT_CATEGORY_CODE, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);
*/	
	tagId = TAG_MERCHANT_ID;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
		GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);
/*
	if (parseGetFixedString(request+counter, MERCHANT_ID_SIZE, tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_MERCHANT_ID error\n");
		return STATUS_FAIL;
	}
	counter+=MERCHANT_ID_SIZE;

	SetTagValue(TAG_MERCHANT_ID, tempCharArray, MERCHANT_ID_SIZE, &runningTimeEnv->tagList);
*/

	tagId = TAG_TERMINAL_ID;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
		GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);

/*
	if (parseGetFixedString(request+counter, TERMINAL_ID_SIZE, 	tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TERMINAL_ID error\n");
		return STATUS_FAIL;
	}
	counter+=TERMINAL_ID_SIZE;

	SetTagValue(TAG_TERMINAL_ID, tempCharArray, TERMINAL_ID_SIZE, &runningTimeEnv->tagList);
*/

#if 1
	tagId = TAG_TRANS_CURRENCY_CODE;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
		GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);
#else
	tagId = TAG_TRANS_CURRENCY_CODE;
	GetTransCurrencyCode(tempByteArray);
	SetTagValue(tagId, tempByteArray, 2, &runningTimeEnv->tagList);
#endif

/*
	if (parseGetFixedString(request+counter, CURRENCY_CODE_SIZE, tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANS_CURRENCY_CODE error\n");
		return STATUS_FAIL;
	}
	counter+=CURRENCY_CODE_SIZE;

	TranslateStringToBCDArray_FillLeftZero(tempCharArray, tempByteArray, &tempByteArrayLen);
	SetTagValue(TAG_TRANS_CURRENCY_CODE, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);
*/

	tagId = TAG_TRANS_CURRENCY_EXP;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
		GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);
/*
	if (parseGetInt8(request+counter, 1, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANS_CURRENCY_EXP error\n");
		return STATUS_FAIL;
	}
	counter++;

	SetTagValue(TAG_TRANS_CURRENCY_EXP, tempByteArray, 1, &runningTimeEnv->tagList);
*/
	tagId = TAG_TRANS_REF_CURRENCY_CODE;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
		GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);

/*
	if (parseGetFixedString(request+counter, CURRENCY_CODE_SIZE, tempCharArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANS_REF_CURRENCY_CODE error\n");
		return STATUS_FAIL;
	}
	counter+=CURRENCY_CODE_SIZE;

	TranslateStringToBCDArray_FillLeftZero(tempCharArray, tempByteArray, &tempByteArrayLen);
	SetTagValue(TAG_TRANS_REF_CURRENCY_CODE, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);
*/
	tagId = TAG_TRANS_REF_CURRENCY_EXP;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
		GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);

/*
	if (parseGetInt8(request+counter, 1, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_TRANS_REF_CURRENCY_EXP error\n");
		return STATUS_FAIL;
	}
	counter++;

	SetTagValue(TAG_TRANS_REF_CURRENCY_EXP, tempByteArray, 1, &runningTimeEnv->tagList);
*/
	SetTagValue(TAG_VLP, "\x0", 1, &runningTimeEnv->tagList);
/*
	if (parseGetInt8(request+counter, 1, tempByteArray)!=STATUS_OK)
	{
		DPRINTF("TAG_VLP error\n");
		return STATUS_FAIL;
	}
	counter++;

	if (((*tempByteArray)!=0)&&((*tempByteArray)!=1))
	{
		DPRINTF("TAG_VLP value error \n");
		return STATUS_FAIL;
	}

	SetTagValue(TAG_VLP, tempByteArray, 1, &runningTimeEnv->tagList);
*/
	runningTimeEnv->commonRunningData.pinBypassMode = PIN_BYPASS_NEXT_ALLOW;
/*
	if (parseGetInt32(request+counter, 1, &runningTimeEnv->commonRunningData.pinBypassMode)
		!=STATUS_OK)
	{
		DPRINTF("pinBypassMode error\n");
		return STATUS_FAIL;
	}
	if ((runningTimeEnv->commonRunningData.pinBypassMode!=PIN_BYPASS_NO_ALLOW)&&
		(runningTimeEnv->commonRunningData.pinBypassMode!=PIN_BYPASS_CURR_ONLY)&&
		(runningTimeEnv->commonRunningData.pinBypassMode!=PIN_BYPASS_NEXT_ALLOW))
	{
		DPRINTF("pinBypassMode value error\n");
		return STATUS_FAIL;
	}
	counter++;
*/
	tagId = TAG_TERMINAL_RISK_DATA;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
#ifndef VPOS_APP        
		return STATUS_FAIL;
#endif
	}
	if (GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
			GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);
	}
/*
	tempByteArrayLen=sizeof(tempByteArray);
	if (parseGetUnfixedByteArray(request+counter, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("TAG_TERMINAL_RISK_DATA error\n");
		return STATUS_FAIL;
	}
	if (tempByteArrayLen>8)
	{
		DPRINTF("TAG_TERMINAL_RISK_DATA value error\n");
		return STATUS_FAIL;
	}
	counter+=tempByteArrayLen*2+1;

	if (tempByteArrayLen>0)
	{
		SetTagValue(TAG_TERMINAL_RISK_DATA, tempByteArray, tempByteArrayLen, 
			&runningTimeEnv->tagList);
	}
*/
    
	tagId = TAG_MERCHANT_LOCATION;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
#ifndef VPOS_APP        
		return STATUS_FAIL;
#endif
	}
	if (GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
			GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);
	}
 
/*

	len=GetUnFixMessageLength(request+counter, strlen(request)-counter, '|');
	if (len>64)
	{
		DPRINTF("TAG_MERCHANT_LOCATION error\n");
		return STATUS_FAIL;
	}
	strncpy(tempCharArray, request+counter, len);
	tempCharArray[len]='\0';
	counter+=len+1;

	if (len>0)
	{
		SetTagValue(TAG_MERCHANT_LOCATION, tempCharArray, len, &runningTimeEnv->tagList);
	}
*/

/*
	tempByteArrayLen=AID_NAME_SIZE;
	if (parseGetUnfixedByteArray(request+counter, tempByteArray, &tempByteArrayLen)!=STATUS_OK)
	{
		DPRINTF("TAG_TERMINAL_AID error\n");
		return STATUS_FAIL;
	}
	SetTagValue(TAG_TERMINAL_AID, tempByteArray, tempByteArrayLen, &runningTimeEnv->tagList);
	counter+=tempByteArrayLen*2+1;
*/
	GetSelectAid((char*)tempCharArray);
	TranslateStringToByteArray(tempCharArray, tempByteArray, &tempByteArrayLen);
	SetTagValue(TAG_TERMINAL_AID, tempByteArray, tempByteArrayLen, &runningTimeEnv->tagList);
#ifdef VPOS_APP
    SetTagValue(TAG_ADF_NAME, tempByteArray, tempByteArrayLen, &runningTimeEnv->tagList);
#endif
	for (index=0; index<aidList->count; index++)
	{
		if ((memcmp(tempByteArray, aidList->aidInfo[index].aidName, 
			aidList->aidInfo[index].aidLength)==0)&&
			(tempByteArrayLen==aidList->aidInfo[index].aidLength))
		{
			SetTagValue(TAG_APP_VERSION_NUMBER, aidList->aidInfo[index].verionNumber,
				APP_VERSION_NUMBER_SIZE, &runningTimeEnv->tagList);
		}
	}

	
	tagId = TAG_ACQUIRER_ID;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
#ifndef VPOS_APP        
		return STATUS_FAIL;
#endif
	}else
    {
        SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
            GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);
    }

	/*
	len=GetUnFixMessageLength(request+counter, strlen(request)-counter, '|');
	if ((len<6)||(len>11))
	{
		DPRINTF("TAG_ACQUIRER_ID error\n");
		return STATUS_FAIL;
	}
	strncpy(tempCharArray, request+counter, len);
	tempCharArray[len]='\0';
	counter+=len+1;

	//Jason added on 2013529, start
	TranslateStringToBCDArray_FillLeftZero_WithLen(tempCharArray, tempByteArray, &tempByteArrayLen, 12);
	SetTagValue(TAG_ACQUIRER_ID, tempByteArray, tempByteArrayLen, 
		&runningTimeEnv->tagList);
	
	//End

	//Jason remove on 2013529, start
	//SetTagValue(TAG_ACQUIRER_ID, tempCharArray, len, &runningTimeEnv->tagList);
	//end
	*/

	tagId = TAG_TAC_DEFAULT_BCTC;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	if (GetTagValueSize(aidParaTagList, tagId) != TAC_SIZE && 
		GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		DPRINTF("TAG_TAC_DEFAULT_BCTC Length error\n");
		return STATUS_FAIL;
	}else if (GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		memcpy(runningTimeEnv->commonRunningData.defaultTerminalActionCode, 
			GetTagValue(aidParaTagList, tagId), TAC_SIZE);
	}
	
	/*
	len=TAC_SIZE;
	if (parseGetActionCode(request+counter, 
		runningTimeEnv->commonRunningData.defaultTerminalActionCode, &len)!=STATUS_OK)
	{
		DPRINTF("defaultTerminalActionCode error\n");
		return STATUS_FAIL;
	}
	counter+=len*2+1;
	*/

	tagId = TAG_TAC_DENIAL_BCTC;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	if (GetTagValueSize(aidParaTagList, tagId) != TAC_SIZE && 
		GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		DPRINTF("TAG_TAC_DENIAL_BCTC Length error\n");
		return STATUS_FAIL;
	}else if (GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		memcpy(runningTimeEnv->commonRunningData.denialTerminalActionCode, 
			GetTagValue(aidParaTagList, tagId), TAC_SIZE);
	}
	
	/*
	len=TAC_SIZE;
	if (parseGetActionCode(request+counter, 
		runningTimeEnv->commonRunningData.denialTerminalActionCode, &len)!=STATUS_OK)
	{
		DPRINTF("denialTerminalActionCode error\n");
		return STATUS_FAIL;
	}
	counter+=len*2+1;
	*/

	tagId = TAG_TAC_ONLINE_BCTC;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	if (GetTagValueSize(aidParaTagList, tagId) != TAC_SIZE && 
		GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		DPRINTF("TAG_TAC_DENIAL_BCTC Length error\n");
		return STATUS_FAIL;
	}else if (GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		memcpy(runningTimeEnv->commonRunningData.onlineTerminalActionCode, 
			GetTagValue(aidParaTagList, tagId), TAC_SIZE);
	}
	/*
	len=TAC_SIZE;
	if (parseGetActionCode(request+counter, 
		runningTimeEnv->commonRunningData.onlineTerminalActionCode, &len)!=STATUS_OK)
	{
		DPRINTF("onlineTerminalActionCode error\n");
		return STATUS_FAIL;
	}
	counter+=len*2+1;
	*/

	if (GetTag3ByteValueAndSize(aidParaTag3ByteList, TAG_DEFAULT_TDOL_BCTC, tempByteArray, &tempByteArrayLen) != 0)
	{
		DPRINTF("aidParaTagList Not have %04x\n", TAG_DEFAULT_TDOL_BCTC);
#ifndef VPOS_APP        
		return STATUS_FAIL;
#endif
	}

	runningTimeEnv->commonRunningData.defaultTDOL.len = tempByteArrayLen;
	if (tempByteArrayLen != 0)
	{
		memcpy(runningTimeEnv->commonRunningData.defaultTDOL.value, 
			GetTagValue(aidParaTagList, tagId), GetTagValueSize(aidParaTagList, tagId));
	}

	/*
	runningTimeEnv->commonRunningData.defaultTDOL.len=DOL_SIZE;
	if (parseGetUnfixedByteArray(request+counter, runningTimeEnv->commonRunningData.defaultTDOL.value,
		&runningTimeEnv->commonRunningData.defaultTDOL.len)!=STATUS_OK)
	{
		DPRINTF("defaultTDOL error\n");
		return STATUS_FAIL;
	}
	counter+=runningTimeEnv->commonRunningData.defaultTDOL.len*2+1;
	*/

	tagId = TAG_DDOL_DEFAULT_BCTC;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	runningTimeEnv->commonRunningData.defaultDDOL.len = GetTagValueSize(aidParaTagList, tagId);
	if ( GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		memcpy(runningTimeEnv->commonRunningData.defaultDDOL.value, 
			GetTagValue(aidParaTagList, tagId), GetTagValueSize(aidParaTagList, tagId));
	}
	/*
	runningTimeEnv->commonRunningData.defaultDDOL.len=DOL_SIZE;
	if (parseGetUnfixedByteArray(request+counter, runningTimeEnv->commonRunningData.defaultDDOL.value,
		&runningTimeEnv->commonRunningData.defaultDDOL.len)!=STATUS_OK)
	{
		DPRINTF("defaultDDOL error\n");
		return STATUS_FAIL;
	}
	counter+=runningTimeEnv->commonRunningData.defaultDDOL.len*2;
	*/

	tagId = TAG_EC_TERMINAL_TRANS_LIMIT;
	if(TagIsExisted(aidParaTagList, tagId) == FALSE)
	{
		DPRINTF("aidParaTagList Not have %04x\n", tagId);
		return STATUS_FAIL;
	}
	if (GetTagValueSize(aidParaTagList, tagId) != 0)
	{
		SetTagValue(tagId, GetTagValue(aidParaTagList, tagId), 
			GetTagValueSize(aidParaTagList, tagId), &runningTimeEnv->tagList);
	}
	return STATUS_OK;
/*
	if (counter!=strlen(request))
	{
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
*/	
}

//#endif

//------------------------------------------------------------------------------------------
tti_int32 parseGetBalanceEnquiry(char *request, RunningTimeEnv *runningTimeEnv)
{
	if (strlen(request)!=0)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_bool isCorrectCA(RunningTimeEnv *runningTimeEnv)
{
	tti_byte tempBuff[BUFFER_SIZE_1K];
	tti_int32 tempBuffLen;
	tti_byte checkSum[HASH_RESULT_SIZE];
	
	if (!TagIsExisted(&runningTimeEnv->tagList, TAG_CA_PUBLIC_KEY_INDEX)||
		!TagIsExisted(&runningTimeEnv->tagList, TAG_DF_NAME))
	{
		return TRUE;
	}

	if ((runningTimeEnv->caPublicKey.modLen==0)||(runningTimeEnv->caPublicKey.expLen))
	{
		return FALSE;
	}
	
	memcpy(tempBuff, GetTagValue(&runningTimeEnv->tagList, TAG_DF_NAME), 5);
	tempBuffLen=5;
	memcpy(tempBuff+tempBuffLen, GetTagValue(&runningTimeEnv->tagList, TAG_CA_PUBLIC_KEY_INDEX), 1);
	tempBuffLen++;
	memcpy(tempBuff+tempBuffLen, runningTimeEnv->caPublicKey.modData, runningTimeEnv->caPublicKey.modLen);
	tempBuffLen+=runningTimeEnv->caPublicKey.modLen;
	memcpy(tempBuff+tempBuffLen, runningTimeEnv->caPublicKey.exp, runningTimeEnv->caPublicKey.expLen);
	tempBuffLen+=runningTimeEnv->caPublicKey.expLen;

	sha1_csum(tempBuff, tempBuffLen, checkSum);

	return memcmp(checkSum, runningTimeEnv->commonRunningData.checkSum, HASH_RESULT_SIZE);
	
}

#ifndef BCTC_EMVL2_DEBUG
//------------------------------------------------------------------------------------------
tti_int32 parseTransaction(char *request, RunningTimeEnv *runningTimeEnv)
{
	tti_int32 counter;
	//tti_byte tempDolBuff[DOL_SIZE];
	//tti_int32 tempDolBuffLen;
	tti_int32 len;
	//tti_byte tempAmountArray[AMOUNT_SIZE_IN_TAG];
	//tti_tchar tempAmountString[AMOUNT_SIZE_IN_TAG*2+1];
	//tti_int32 tempAmountArrayLen;
	//tti_int32 tempAmount;
	
	counter=0;

	if (parseGetBool(request+counter, &runningTimeEnv->commonRunningData.cardHot)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=BOOL_SIZE;

	if (parseGetBool(request+counter, &runningTimeEnv->commonRunningData.forceOnline)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	//Jason For PBOC test 2017.05.10
	//runningTimeEnv->commonRunningData.forceOnline = TRUE;
	//End
	counter+=BOOL_SIZE;

	if (parseGetAmount(request+counter, &runningTimeEnv->commonRunningData.transLogAmount)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;

	len=GetUnFixMessageLength(request+counter, MAX_DISPLAY_CHAR, '|');
	if ((len!=0)&&(len!=16)&&(len!=32))
	{
		return STATUS_FAIL;
	}
	strncpy(runningTimeEnv->sessionKey, request+counter, len);
	runningTimeEnv->sessionKey[len]='\0';
	if (*(request+counter+len)!='|')
	{
		return STATUS_FAIL;
	}
		
	counter+=len+1;

	len=GetUnFixMessageLength(request+counter, MAX_DISPLAY_CHAR, '|');
	strncpy(runningTimeEnv->enterOfflinePlainPinString, request+counter, len);
	runningTimeEnv->enterOfflinePlainPinString[len]='\0';
	if (*(request+counter+len)!='|')
	{
		return STATUS_FAIL;
	}
	counter+=len+1;

	len=GetUnFixMessageLength(request+counter, MAX_DISPLAY_CHAR, '|');
	strncpy(runningTimeEnv->enterOfflineEncryptPinString, request+counter, len);
	runningTimeEnv->enterOfflineEncryptPinString[len]='\0';
	if (*(request+counter+len)!='|')
	{
		return STATUS_FAIL;
	}
	counter+=len+1;

	len=GetUnFixMessageLength(request+counter, MAX_DISPLAY_CHAR, '|');
	strncpy(runningTimeEnv->enterOnlinePinString, request+counter, len);
	runningTimeEnv->enterOnlinePinString[len]='\0';
	if (*(request+counter+len)!='|')
	{
		return STATUS_FAIL;
	}
	counter+=len+1;
	
	len=GetUnFixMessageLength(request+counter, MAX_DISPLAY_CHAR, '|');
	strncpy(runningTimeEnv->lastPintryString, request+counter, len);
	runningTimeEnv->lastPintryString[len]='\0';

	counter+=len;

	if (counter==strlen(request))
	{
		return STATUS_OK;
	}

	counter++;

	if (parseGetInt8(request+counter, 2, &runningTimeEnv->commonRunningData.hashAlgorithm)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=2;

	if (parseGetInt8(request+counter, 2, &runningTimeEnv->commonRunningData.publicKeyAlgorithm)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=2;

	switch (*(request+counter))
	{
		case '0':
			memcpy(runningTimeEnv->caPublicKey.exp, "\x03", 1);
			runningTimeEnv->caPublicKey.expLen=1;
			break;
		case '1':
			memcpy(runningTimeEnv->caPublicKey.exp, "\x01\x00\x01", 3);
			runningTimeEnv->caPublicKey.expLen=3;
			break;
		default:
			return STATUS_FAIL;
	}
	counter++;

	runningTimeEnv->caPublicKey.modLen=MAX_CA_PUBLIC_KEY_SIZE;
	if (parseGetUnfixedByteArray(request+counter, runningTimeEnv->caPublicKey.modData, 
		&runningTimeEnv->caPublicKey.modLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	counter+=runningTimeEnv->caPublicKey.modLen*2+1;

	printCertificate(&runningTimeEnv->caPublicKey);

	runningTimeEnv->commonRunningData.checkSumLen=HASH_RESULT_SIZE;
	if (parseGetUnfixedByteArray(request+counter, runningTimeEnv->commonRunningData.checkSum,
		&runningTimeEnv->commonRunningData.checkSumLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	if (!isCorrectCA(runningTimeEnv))
	{
		return STATUS_FAIL;
	}

	return STATUS_OK;
	
	/* Jason removed on 2017.02.23
	if (counter!=strlen(request))
	{
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
	*/
}
#else
//------------------------------------------------------------------------------------------
tti_int32 parseTransaction(char *request, RunningTimeEnv *runningTimeEnv)
{
	tti_int32 counter;
	//tti_byte tempDolBuff[DOL_SIZE];
	//tti_int32 tempDolBuffLen;
	tti_int32 len;
	//tti_byte tempAmountArray[AMOUNT_SIZE_IN_TAG];
	//tti_tchar tempAmountString[AMOUNT_SIZE_IN_TAG*2+1];
	//tti_int32 tempAmountArrayLen;
	//tti_int32 tempAmount;
	
	counter=0;

	if (parseGetBool(request+counter, &runningTimeEnv->commonRunningData.cardHot)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=BOOL_SIZE;

	if (parseGetBool(request+counter, &runningTimeEnv->commonRunningData.forceOnline)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	//Jason For PBOC test 2017.05.10
	//runningTimeEnv->commonRunningData.forceOnline = TRUE;
	//End
	counter+=BOOL_SIZE;

	if (parseGetAmount(request+counter, &runningTimeEnv->commonRunningData.transLogAmount)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=AMOUNT_SIZE;

	len=GetUnFixMessageLength((tti_byte*)(request+counter), MAX_DISPLAY_CHAR, '|');
	if ((len!=0)&&(len!=16)&&(len!=32))
	{
		return STATUS_FAIL;
	}
	strncpy(runningTimeEnv->sessionKey, request+counter, len);
	runningTimeEnv->sessionKey[len]='\0';
	if (*(request+counter+len)!='|')
	{
		return STATUS_FAIL;
	}
		
	counter+=len+1;

	len=GetUnFixMessageLength((tti_byte*)(request+counter), MAX_DISPLAY_CHAR, '|');
	strncpy(runningTimeEnv->enterOfflinePlainPinString, request+counter, len);
	runningTimeEnv->enterOfflinePlainPinString[len]='\0';
	if (*(request+counter+len)!='|')
	{
		return STATUS_FAIL;
	}
	counter+=len+1;

	len=GetUnFixMessageLength((tti_byte*)(request+counter), MAX_DISPLAY_CHAR, '|');
	strncpy(runningTimeEnv->enterOfflineEncryptPinString, request+counter, len);
	runningTimeEnv->enterOfflineEncryptPinString[len]='\0';
	if (*(request+counter+len)!='|')
	{
		return STATUS_FAIL;
	}
	counter+=len+1;

	len=GetUnFixMessageLength((tti_byte*)(request+counter), MAX_DISPLAY_CHAR, '|');
	strncpy(runningTimeEnv->enterOnlinePinString, request+counter, len);
	runningTimeEnv->enterOnlinePinString[len]='\0';
	if (*(request+counter+len)!='|')
	{
		return STATUS_FAIL;
	}
	counter+=len+1;
	
	len=GetUnFixMessageLength((tti_byte*)(request+counter), MAX_DISPLAY_CHAR, '|');
	strncpy(runningTimeEnv->lastPintryString, request+counter, len);
	runningTimeEnv->lastPintryString[len]='\0';

	counter+=len;

	if (counter==strlen(request))
	{
		return STATUS_OK;
	}

	counter++;

	if (parseGetInt8(request+counter, 2, (tti_int8 *)(&runningTimeEnv->commonRunningData.hashAlgorithm))!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=2;

	if (parseGetInt8(request+counter, 2, (tti_int8 *)(&runningTimeEnv->commonRunningData.publicKeyAlgorithm))!=STATUS_OK)
	{
		return STATUS_FAIL;
	}
	counter+=2;

	switch (*(request+counter))
	{
		case '0':
			memcpy(runningTimeEnv->caPublicKey.exp, "\x03", 1);
			runningTimeEnv->caPublicKey.expLen=1;
			break;
		case '1':
			memcpy(runningTimeEnv->caPublicKey.exp, "\x01\x00\x01", 3);
			runningTimeEnv->caPublicKey.expLen=3;
			break;
		default:
			return STATUS_FAIL;
	}
	counter++;

	runningTimeEnv->caPublicKey.modLen=MAX_CA_PUBLIC_KEY_SIZE;
	if (parseGetUnfixedByteArray(request+counter, runningTimeEnv->caPublicKey.modData, 
		&runningTimeEnv->caPublicKey.modLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	counter+=runningTimeEnv->caPublicKey.modLen*2+1;

	printCertificate(&runningTimeEnv->caPublicKey);

	runningTimeEnv->commonRunningData.checkSumLen=HASH_RESULT_SIZE;
	if (parseGetUnfixedByteArray(request+counter, runningTimeEnv->commonRunningData.checkSum,
		&runningTimeEnv->commonRunningData.checkSumLen)!=STATUS_OK)
	{
		return STATUS_FAIL;
	}

	return STATUS_OK;
	
	/* Jason removed on 2017.02.23
	if (counter!=strlen(request))
	{
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
	*/
}


#endif

//------------------------------------------------------------------------------------------
tti_int32 parseCompletion(char *request, RunningTimeEnv *runningTimeEnv)
{
	tti_int32 counter;
	char tempByteBuff[BUFFER_SIZE_10K];
	tti_int32 tempByteBuffSize;
	//tti_int32 tempScriptCount;

	counter=0;

	DPRINTF("parseCompletion request = %s, count = %d\n", request, strlen(request));
	if (parseGetInt8(request+counter, 1, (tti_int8 *)(&runningTimeEnv->hostResponseData.responseMode))!=STATUS_OK)
	{
		ReportLine();
		return STATUS_FAIL;
	}
	counter++;

	switch (runningTimeEnv->hostResponseData.responseMode)
	{
		case RESP_MODE_HOST_APPROVED:
		case RESP_MODE_VOICE_APPROVED:
		case RESP_MODE_OTHER_APPROVED:
			runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_GOOD_OFFLINE;
			break;
		case RESP_MODE_HOST_DECLINED:
		case RESP_MODE_VOICE_DECLINED:
			runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_REJECT;
			break;
		//Jason added 20110905
		case RESP_MODE_INVALID_ARC:
			runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_INVALID_ARC;
			break; 
		//End added
		default:
			runningTimeEnv->commonRunningData.emvResultCode=EMV_RESULT_COMM_ERROR;
			if (counter==strlen(request))
			{
				return STATUS_OK;
			}
			else
			{
				ReportLine();
				return STATUS_FAIL;
			}
			//break;
	}
	
	if (parseGetFixedString(request+counter, RESPONSE_CODE_SIZE, tempByteBuff)!=STATUS_OK)
	{
		ReportLine();
		return STATUS_FAIL;
	}
	SetTagValue(TAG_RESPONSE_CODE, (tti_byte *)tempByteBuff, RESPONSE_CODE_SIZE, &runningTimeEnv->tagList);
	counter+=RESPONSE_CODE_SIZE;

	if (counter==strlen(request))
	{
		ReportLine();
		return STATUS_OK;
	}

	tempByteBuffSize=sizeof(tempByteBuff);
	if (parseGetUnfixedByteArray(request+counter, (tti_byte *)tempByteBuff, &tempByteBuffSize)!=STATUS_OK)
	{
		ReportLine();
		return STATUS_FAIL;
	}

	SetTagValue(TAG_ISSUER_AUTH_DATA, (tti_byte *)tempByteBuff, tempByteBuffSize, &runningTimeEnv->tagList);

	counter+=tempByteBuffSize*2;

	if (counter==strlen(request))
	{
		ReportLine();
		return STATUS_OK;
	}

	counter++;

	runningTimeEnv->scriptsList.scriptsNumber=0;
	while (TRUE)
	{
		runningTimeEnv->scriptsList.scripts[runningTimeEnv->scriptsList.scriptsNumber].scriptLen=MAX_SCRIPTS_SIZE;
		if (parseGetUnfixedByteArray(request+counter, runningTimeEnv->scriptsList.scripts[runningTimeEnv->scriptsList.scriptsNumber].scriptValue, 
			&runningTimeEnv->scriptsList.scripts[runningTimeEnv->scriptsList.scriptsNumber].scriptLen)!=STATUS_OK)
		{
			ReportLine();
			return STATUS_FAIL;
		}

		counter+=runningTimeEnv->scriptsList.scripts[runningTimeEnv->scriptsList.scriptsNumber].scriptLen*2;
		runningTimeEnv->scriptsList.scriptsNumber++;
		
		if (*(request+counter)==SEPERATOR)
		{
			counter++;
		}
		else
		{
			break;
		}
	}

	if (counter!=strlen(request))
	{
		ReportLine();
		return STATUS_FAIL;
	}
	else
	{
		return STATUS_OK;
	}
}

//------------------------------------------------------------------------------------------
tti_int32 parseEndTransaction(char *request, RunningTimeEnv *runningTimeEnv)
{
	if (strlen(request)!=0)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}

//------------------------------------------------------------------------------------------
tti_int32 parseGetCheckSum(char *request, RunningTimeEnv *runningTimeEnv)
{
	if (strlen(request)!=0)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}

