#include <stdio.h>
#include <iso7816_3.h>
#include <mhscpu.h>
#include "defines.h"
//#include <memory.h>
/** Other includes */
#include "osTypes.h"
#include "emvl2.h"
/** Local includes */
#include "debug.h"
#include "customize_emvl2.h"
#include "OsTypes.h"
#include "misce.h"
#include "EMVL2.h"
#include "emvDebug.h"
#include "sci.h"
#include "iso7816_3.h"
#ifdef VPOS_APP
#include "vposface.h"
#endif

#ifdef VPOS_APP
tti_int32 SCR_Open(const tti_int8 * pszParam)
{
	return 1;
}

tti_bool SCR_Close(tti_int32 hSCR)
{
	return 1;
}

tti_bool SCR_Detect(tti_int32 hSCR)
{
    if(_uiTestCard(0)==1)
        return TRUE;
    else
        return FALSE;
}


tti_bool SCR_PowerOn(tti_int32 hSCR, tti_byte * pATR, tti_int8* piATRLen)
{
	unsigned int ret;
	unsigned char atr[64];
    
    ret=_uiResetCard(0, atr);
    if(ret==0)
        return FALSE;
    
    //ret>0
    if(pATR)
        memcpy(pATR, atr, ret);
    if(piATRLen)
        *piATRLen=ret;
	return TRUE;
}

tti_bool SCR_PowerOff(tti_int32 hSCR)
{
	_uiCloseCard(0);
	return TRUE;
}
#else
tti_int32 SCR_Open(const tti_int8 * pszParam)
{
	SCI_SetMV(VCC_3000mV);
    
    //2.init device
    iso7816_device_init();
    
	return 1;
}

tti_bool SCR_Close(tti_int32 hSCR)
{
	return 1;
}

tti_bool SCR_Detect(tti_int32 hSCR)
{
	int ret;

	ret = iso7816_detect(SMARTCARD_SLOT);
	if (ret == 0)
	{
		//dbg("iso7816_detect == 0, return TRUE\n");
		return TRUE;
	}else
	{
		//dbg("iso7816_detect != 0, return FALSE\n");
		return FALSE;
	}
}


tti_bool SCR_PowerOn(tti_int32 hSCR, tti_byte * pATR, tti_int8* piATRLen)
{
	int ret;
	//tti_byte status;
	unsigned char atr[64];

    //SCI_SetMV(VCC_3000mV);
    
    //2.init device
#ifndef USE_1903S
    SCI_SetMV(VCC_3000mV);
#endif    
    ret = iso7816_init(SMARTCARD_SLOT, VCC_3000mV | SPD_1X, atr);
    if(ret)
    {
    #ifndef USE_1903S  
        SCI_SetMV(VCC_1800mV);
    #endif
        ret= iso7816_init(SMARTCARD_SLOT, VCC_1800mV | SPD_1X, atr);
    }
    if (ret != 0)
    {
        dbg("iso7816_init failed %d!\n", ret);
       
        return FALSE;
    }

	return TRUE;
}

tti_bool SCR_PowerOff(tti_int32 hSCR)
{
	//int ret;
	//tti_byte status;

	iso7816_close(SMARTCARD_SLOT);

	return TRUE;
}
#endif

tti_bool SCR_ExchangeApdu(tti_int32 hSCR, const tti_byte * pApduCmd, const tti_int16 iApduCmdLen, tti_byte * pApduResp, tti_int16 * piApduRespLen)
{
	int ret;
	//int tmp;
	int lc;
	int le;
  //tti_byte status;
	ST_APDU_RSP     apdu_rsp;
    ST_APDU_REQ     apdu_req;

	//Line;

	if (iApduCmdLen == 4)
	{
		lc = 0;
		le = 0;
	}else
	{
		lc = pApduCmd[4];
		if (lc + 5 == iApduCmdLen)
		{
			//No le
			le = 0;
		}else if (lc + 5 + 1 == iApduCmdLen)
		{
			//le list
			le = pApduCmd[iApduCmdLen - 1];
		}else
		{
			//Length error
			 return FALSE;
		}
	}
	//Line;
	memcpy(apdu_req.cmd, pApduCmd, 4);
	if (lc != 0)
	{
		memcpy(apdu_req.data_in, pApduCmd + 5, lc);
	}
	//Line;
	apdu_req.lc = lc;
	apdu_req.le = le;
    
    ret = iso7816_exchange(SMARTCARD_SLOT, AUTO_GET_RSP, &apdu_req, &apdu_rsp);
	if (0 != ret)
    {
        dbg("Exchange failed %d!\n", ret);
        return FALSE;
    }

	//Line;
	
	memcpy(pApduResp, apdu_rsp.data_out, apdu_rsp.len_out);
	pApduResp[apdu_rsp.len_out] = apdu_rsp.swa;
	pApduResp[apdu_rsp.len_out+1] = apdu_rsp.swb;
	*piApduRespLen = apdu_rsp.len_out+2;

	//Line;
/*
	dbg("apdu_req.lc = %d, apdu_req.le = %d\n", apdu_req.lc, apdu_req.le);
	hexdumpEx("cAPDU command list", apdu_req.cmd, 4);
	hexdumpEx("cAPDU data list", apdu_req.data_in, lc);
	dbg("apdu_rsp.swa = %02x, apdu_rsp.swb = %02x\n", apdu_rsp.swa, apdu_rsp.swb);
	hexdumpEx("apdu_rsp data list", apdu_rsp.data_out, apdu_rsp.len_out);
*/
	return TRUE;
}

int SCR_Get_Status(int *piStatus)
{
	return 0;
}



int Emvl2_InitICCReader(void)
{
	tti_int32 iRet;
	tti_int32 iICCReaderFD = -1;
	SCR_CallBacks tSCR_CallBacks;

#ifdef VPOS_APP
    iICCReaderFD=1;
#else    
	iICCReaderFD = SCR_Open(" ");
	if (iICCReaderFD < 0)
	{
		DPRINTF("Warning: Fail to open ICC Reader.\n");
		return -1;
	}
#endif
	
	tSCR_CallBacks.m_SCR_Handle = iICCReaderFD;
	tSCR_CallBacks.m_SCR_PowerOn = SCR_PowerOn;
	tSCR_CallBacks.m_SCR_PowerOff = SCR_PowerOff;
	tSCR_CallBacks.m_SCR_ExchangeApdu = SCR_ExchangeApdu;
	iRet = EMVL2_Init_SmartCard(&tSCR_CallBacks);
	if (iRet != 0)
	{
		DPRINTF("Warning: Init SmartCard error.\n");
		return -1;
	}

	return 0;
}


int EMVL2_Test(tti_tchar *szTagList)
{
    
	tti_tchar szCmd[1024];
	tti_tchar szAID[100] = {0};
	tti_tchar szRsp[512];
	tti_tchar szTAGArr[][7] = {"5F20", "005A", "5F24", "0057", "9F26", "9F27", "9F10", "9F37", "9F36", "0095", "009A", 
		"009C", "9F02", "5F2A", "0082", "9F1A", "9F03", "9F33", "9F34", "9F35", "9F1E", "0084", "9F09", "9F41", "9F63", "0050", "5F34"};
	int iRspLen;
	int iLoop;
	int iMax;

	Emvl2_InitICCReader();

	strcpy((tti_tchar *)szCmd, "E0.1234567884022A0FFC0E000F020011008c0A0000000031010|00010A0000000999090|00010A0000003330101|00010A000000003101003|00010A000000003101004|00010A000000003101005|00010A000000003101006|00011A000000003101007|00020A0000000041010|00010A0000000651010|00010A000000025010501|00010A122334455|008c0A0000000031010010203040506070809|00010A0000000031010010203040506070809");
	memset(szRsp, 0, sizeof(szRsp));
	EMVL2_LoadConfiguration((tti_byte *)szCmd, strlen(szCmd), (tti_byte *)szRsp, &iRspLen);
	DPRINTF("szRsp is %s\n", szRsp);
	if (szRsp[3] != '0')
	{
		return -1;
	}
	
	strcpy(szCmd, "E1.100");
	memset(szRsp, 0, sizeof(szRsp));
	EMVL2_GetAidList((tti_byte *)szCmd, strlen(szCmd), (tti_byte *)szRsp, &iRspLen);
	DPRINTF("E1 szRsp is %s\n", szRsp);
	if (szRsp[3] != '0')
	{
		SCR_PowerOff(0);
		return -1;
	}

	if (szRsp[3] == '0')
	{
		sscanf(szRsp + 4, "%[^;]", szAID);
		DPRINTF("szAID is [%s]\n", szAID);
	}

	strcpy(szCmd, "E2.0000000011110000000000001612201502080005000000031000000000100000000000000010008888123456789012345123456788402840202|CN|");
	strcat(szCmd, szAID); 
	strcat(szCmd,"|12345678|0000000000|0000000000|0000000000|9F3704|9F3704");
	memset(szRsp, 0, sizeof(szRsp));
	EMVL2_InitTransaction((tti_byte *)szCmd, strlen(szCmd), (tti_byte *)szRsp, &iRspLen);
	DPRINTF("szRsp is %s\n", szRsp);
	if (szRsp[3] != '0')
	{
		SCR_PowerOff(0);
		return -1;
	}

	strcpy(szCmd, "E4.000000000001110000000000000000|Pin:(Plain)|Pin:(Encrypt)|Pin:(Online)|Last Pin Try|Retry Pin|Retry Pin|0|2|01011942B7F2BA5EA307312B63DF77C5243618ACC2002BD7ECB74D821FE7BDC78BF28F49F74190AD9B23B9713B140FFEC1FB429D93F56BDC7ADE4AC075D75532C1E590B21874C7952F29B8C0F0C1CE3AEEDC8DA25343123E71DCF86C6998E15F756E3|251A5F5DE61CF28B5C6E2B5807C0644A01D46FF5|01011999|01012015");
	memset(szRsp, 0, sizeof(szRsp));
	EMVL2_Transaction((tti_byte *)szCmd, strlen(szCmd), (tti_byte *)szRsp, &iRspLen);
	DPRINTF("szRsp is %s\n", szRsp);
	if (szRsp[3] != '0')
	{
		SCR_PowerOff(0);
		return -1;
	}
	iMax = sizeof(szTAGArr)/sizeof(szTAGArr[0]);
	
	for (iLoop = 0;iLoop < iMax;iLoop ++)
	{
		if (iLoop == 24)
		{
			DPRINTF("iLoop = %d\n", iLoop);
		}
		//dbg("szTAGArr[iLoop] is %s\n", szTAGArr[iLoop]);
		strcpy(szCmd, "E7.");
		//info("2");
		strcat(szCmd, szTAGArr[iLoop]);
		//info("3");
		memset(szRsp, 0, sizeof(szRsp));
		//dbg("cmd is %s\n", szCmd);
		EMVL2_GetTag((tti_byte *)szCmd, strlen(szCmd), (tti_byte *)szRsp, &iRspLen);
		//dbg("szRsp is %s\n", szRsp);
		//delayms_ex(1);

		if (iLoop == 0)
		{
			strcpy(szTagList, szTAGArr[iLoop]);
		}else
		{
			strcat(szTagList, szTAGArr[iLoop]);
		}

		if (szRsp[3] == '0')
		{
			sprintf(szTagList, "%s%02X", szTagList, strlen(szRsp + 4)/2);
			strcat(szTagList, szRsp + 4);
		}else
		{
			//Tag no value
			strcat(szTagList, "00");
		}
		strcat(szTagList, ";");
	}
	//Tag 4F
	strcat(szTagList, "004F");
	sprintf(szTagList, "%s%02X", szTagList, strlen(szAID)/2);
	strcat(szTagList, szAID);
	strcat(szTagList, ";");
	
	//dbg("szTagList[%d] is %s\n", strlen(szTagList), szTagList);
	SCR_PowerOff(0);
	return 0;
    
}





