#ifndef _TRANSFLOW_H_
#define _TRANSFLOW_H_

#include "defines.h"
#include "OsTypes.h"
#include "EMVL2.h"

tti_int32	prepareRunningTimeEnv(TerminalParam *terminalParam, RunningTimeEnv *runningTimeEnv);

tti_int32 Init_SmartCard(SCR_CallBacks * pScrCallbacks);

tti_int32 getAidList(TerminalParam *terminalParam, tti_int32 timeOut, AID_LIST *aidList);

tti_int32 initTransaction(TerminalParam *terminalParam, RunningTimeEnv *runningTimeEnv);

tti_int32 JudgeTransaction(RunningTimeEnv *runningTimeEnv);

tti_int32 TransactionComplete(RunningTimeEnv *runningTimeEnv);

tti_int32 getSpecialDataFromICC(RunningTimeEnv *runningEnvTimeEnv, tti_uint16 tag);

tti_int32 EndTransaction(RunningTimeEnv *runningTimeEnv);

tti_int32 GetVersionCheckSum(tti_tchar *szCheckSum);

tti_int32 GetTermCheckSum(tti_tchar *szCheckSum);

tti_int32 GetKernelVersion(tti_tchar *szVersion);

tti_int32 qpbocSelectPPSE(void);

tti_int32 qpbocInitTransaction(tti_byte *aid, tti_int16 aidLen, TagList *tagList);

tti_int32 ReadECBalance(TerminalParam *terminalParam, RunningTimeEnv *runningTimeEnv);

tti_int32 ICCReadLogTransaction(TerminalParam *terminalParam, RunningTimeEnv *runningTimeEnv);

tti_int32 GetECParaFromICC(RunningTimeEnv *runningTimeEnv);

tti_bool IsECTrans(RunningTimeEnv *runningTimeEnv);

tti_int32 qpbocTransaction(char *szPrompt);





#endif


