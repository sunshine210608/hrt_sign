#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "Define.h"
#include "Util.h"
#include "RespDataItem.h"
#include "emvDebug.h"

RespDataItem_t g_RespItemList[RESP_ITEM_COUNT];

const DataItemAttr_t *GetResponseItemAttr(uint16 index)
{
	static const DataItemAttr_t AttrTable[] ={

		//command
		{DTYPE_ASCII,  ETYPE_MUST,FIX_LEN, 3, 0, "command load config"},
		{DTYPE_ASCII,  ETYPE_MUST,FIX_LEN, 3, 0, "command read media"},
		{DTYPE_ASCII,	 ETYPE_MUST,FIX_LEN, 3, 0, "get offline balance enquiry"},
		{DTYPE_ASCII,  ETYPE_MUST,FIX_LEN, 3, 0, "command emv transaction"},
		{DTYPE_ASCII,  ETYPE_MUST,FIX_LEN, 3, 0, "command transaction complete"},		
		{DTYPE_ASCII,	ETYPE_MUST, FIX_LEN, 3, 0, "cmd end transaction"},
		
		//status
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 1, 0, "status"},		
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 1, 0, "EMV Result Code"},

		//read media
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 1, 0, "conditions"},
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 6, 0, "Card Effective Date"},
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 6, 0, "Card Expiry Date"},
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN, 4, 0, "AIP"},
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN, 4, 0, "AUC"},
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 2, 0, "PAN Seq. No."},
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 3, 0, "Service Code"},		
		{DTYPE_ASCII,  ETYPE_MUST, VAR_LEN, 0, 0, "Language Prefer"},
		{DTYPE_BINARY,  ETYPE_OPT,  FIX_LEN, 4, 0, "App version No."},
		{DTYPE_BINARY,  ETYPE_OPT,  FIX_LEN, 2, 0, "CA Public Index"},
		{DTYPE_BINARY,  ETYPE_OPT,  VAR_LEN, 0, 0, "AID"},
		{DTYPE_NUMERIC,  ETYPE_OPT,  VAR_LEN, 0, 0, "PAN"},
		{DTYPE_ASCII,  ETYPE_OPT,  VAR_LEN, 0, 0, "Track 1"},
		{DTYPE_ASCII,  ETYPE_OPT,  VAR_LEN, 0, 0, "Track 2"},
		{DTYPE_ASCII,  ETYPE_OPT,  VAR_LEN, 0, 0, "Cardholder Name"},
		{DTYPE_ASCII,  ETYPE_OPT,  VAR_LEN, 0, 0, "App Label"},

		//emv transaction
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN,16, 0, "Application Cryptogram"},
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN, 2, 0, "Cryptogram Info data"},
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN, 4, 0, "ATC"},
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN,10, 0, "TVR"},
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN, 4, 0, "TSI"},		
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN, 8, 0, "Unpredictable No"},
		{DTYPE_NUMERIC,	 ETYPE_MUST, FIX_LEN, 2, 0, "Rand Num For Online Trans Select"},
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN, 6, 0, "CVR"},
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN,10, 0, "IAC"},
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 1, 0, "IAC Code Type"},
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 1, 0, "Signature Required"},
		//Jason added on 2012.04.24 for EC
		{DTYPE_NUMERIC,  ETYPE_MUST, FIX_LEN, 12, 0, "EC Issuer Authorization Code"},
		//End
		//Jason added on 2012.04.24 for ISEC
		{DTYPE_BINARY,  ETYPE_MUST, FIX_LEN, 1, 0, "IsEC"},
		//End
		
		//Jason delete start, 20101107 
		//{DTYPE_BINARY,  ETYPE_MUST, VAR_LEN, 0,64, "IAD"},
		//Jason delete end
		
		//Jason add start, 20101107
		{DTYPE_BINARY,  ETYPE_OPT, VAR_LEN, 0,64, "IAD"},
		//Jason add end
		{DTYPE_BINARY,  ETYPE_OPT,  VAR_LEN, 0, 0, "PIN Block"},

		

		//emv completion
		
		{DTYPE_ASCII,	 ETYPE_MUST, FIX_LEN, 2, 0, 	"Transaction Response Code"},
		{DTYPE_NUMERIC,  ETYPE_OPT,  VAR_LEN, 1, 0,  	"Script Result"}
	};

		assert(index < RESP_ITEM_COUNT);
		
		return &(AttrTable[index]);
}

int32 gfirstTimeInitResponseFlag = 1;

int32 FirstTimeInitResponseItemList()
{
	uint16 i;

	if (gfirstTimeInitResponseFlag != 1)
	{
		return STATUS_OK;
	}

	for(i=0; i<RESP_ITEM_COUNT; i++)
	{
		g_RespItemList[i].index = i;		
		g_RespItemList[i].data.ptr = NULL;
		g_RespItemList[i].data.len = 0;
	}
	
	gfirstTimeInitResponseFlag = 0;
	
	return STATUS_OK;
}

int32 InitResponseItemList(void)
{
	uint16 i;

	//Jason added 20200507 
	FirstTimeInitResponseItemList();

	for(i=0; i<RESP_ITEM_COUNT; i++)
	{
		if(IsResponseItemExisted(i))
		{
			ResetResponseItem(i);
		}
	}
	//End
		
	for(i=0; i<RESP_ITEM_COUNT; i++)
	{
		g_RespItemList[i].index = i;		
		g_RespItemList[i].data.ptr = NULL;
		g_RespItemList[i].data.len = 0;
	}

	return STATUS_OK;
}

//Jason added on 2021/05/10, free the memory when IC card transaction is done to void QR malloc error.
int32 UninitResponseItemList(void)
{
    uint16 i;

    //Response Item List didn't init yet, no need to free the memory.
    if (gfirstTimeInitResponseFlag == 1)
    {
        return STATUS_OK;
    }

    //Response Item List already init , need to free the memory.
    for(i=0; i<RESP_ITEM_COUNT; i++)
    {
        if(IsResponseItemExisted(i))
        {
            ResetResponseItem(i);
        }
    }

    return STATUS_OK;
}

void PrintResponseItemList()
{
	uint16 i;
	const DataItemAttr_t *pAttr;

	for(i=0; i<RESP_ITEM_COUNT; i++)
	{
		if(IsResponseItemExisted(i))
		{	
			pAttr = GetResponseItemAttr(i);
			LOGD("%s\t:",pAttr->name);
			PrintCharArray(g_RespItemList[i].data.ptr, g_RespItemList[i].data.len);
		}
	}
}

vec_t *GetResponseItemValue(uint16 index)
{
	assert(index < RESP_ITEM_COUNT);
	
	return &(g_RespItemList[index].data);
}

int32 ResetResponseItem(uint16 index)
{
	assert(index < REQ_ITEM_COUNT);

	free(g_RespItemList[index].data.ptr);
	//free_Counter();
	g_RespItemList[index].data.ptr = NULL;
	g_RespItemList[index].data.len = 0;

	return STATUS_OK;
}

int32 SetResponseItem(uint16 index, const byte buf[], uint16 len)
{
	const DataItemAttr_t *pAttr;
	vec_t	*pData = &(g_RespItemList[index].data);

	if(index >= REQ_ITEM_COUNT)
	{
		return STATUS_FAIL;
	}

	if(IsResponseItemExisted(index))
	{
		ResetResponseItem(index);
	}
	pAttr = GetResponseItemAttr(index);

	if(pAttr->len_type==FIX_LEN)
	{
		if(len != pAttr->min_len)
		{
			LOGE("Length NOT match!- Set RespData Fail:%s", pAttr->name);
			return STATUS_FAIL;
		}
	}
	
	pData->ptr = (byte *)malloc(len + 1);
	if(pData->ptr == NULL)
	{
		return STATUS_FAIL;
	}
	//malloc_Counter();
	memcpy(pData->ptr, buf, len);
	pData->ptr[len]='\0';
	
	pData->len = len;
	
	return STATUS_OK;
}

int32 SetResponseItemStr(uint16 index, const char *str)
{
	return SetResponseItem(index, (byte *)str, strlen(str));
}

byte GetRespStatusCode()
{
	return g_RespItemList[RESP_STATUS_CODE].data.ptr[0];
}

byte GetRespEmvStatusCode()
{
	if (g_RespItemList[RESP_EMV_RESULT_CODE].data.ptr == NULL)
	{
		return RESP_EMV_NULL;
	}
	return g_RespItemList[RESP_EMV_RESULT_CODE].data.ptr[0];
}

int32 IsResponseItemExisted(uint16 index)
{
	assert(index < RESP_ITEM_COUNT);
	
	if(g_RespItemList[index].data.len == 0)
	{
		return FALSE;
	}

	return TRUE;
}

