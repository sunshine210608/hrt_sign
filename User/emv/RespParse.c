#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "Define.h"
#include "Util.h"
#include "RespDataItem.h"
#include "RespParse.h"
#include "emvDebug.h"

#ifdef VPOS_APP
#include "vposface.h"
#include "pub.h"
#ifdef APP_LKL
#include "AppGlobal_lkl.h"
#include "func.h"
#endif
#ifdef APP_SDJ
#include "AppGlobal_sdj.h"
#endif
#if 1//def APP_CJT
#include "AppGlobal_cjt.h"
#endif

#endif

SAidInfo g_CandidateAidList[MAX_CANDIDATE_AID_COUNT];
uint16	g_CandidateAidCount;
uint16 GetVarLengthDataItemLength(const char *StartPos, const char *EndPos)
{
	char *CurPos =(char *) StartPos;
	
	assert(StartPos <= EndPos);
	
	if(StartPos >= EndPos)
	{
		return 0;
	}

	while(CurPos <= EndPos)
	{
		if(*CurPos == ITEM_SEPARATOR)
		{
			return ((uint16)(CurPos - StartPos));
		}
		
		CurPos++;
	}

	return (EndPos-StartPos);
}


int32 GetDataItemLength(const DataItemAttr_t *pAttr, const char buf[], uint16 bufsize,uint16 *len)
{	
	if((pAttr->exist_type == ETYPE_MUST) && (pAttr->len_type == FIX_LEN))
	{
		if(pAttr->min_len > bufsize)
		{
			return STATUS_FAIL;
		}
		
		*len = pAttr->min_len;
	}
	else
	{
		*len = GetVarLengthDataItemLength(buf, (const char *)(buf + bufsize));
	}

	return STATUS_OK;
}

int32 ParseResp(const uint16 ItemIndex[], uint16 ItemCount,const char buf[], uint16 len)
{
	uint16 i;
	uint16 DataLen;
	vec_t  *pData;
	const char *CurPos =buf;
	const char *EndPos =buf + len;
	const DataItemAttr_t *pAttr;

	Line;

	assert(ItemCount >=2 );
	
	if(len < 3 + 1)
	{
		return STATUS_FAIL;
	}

	dbg("ItemCount = %d\n", ItemCount);
	for(i=0; i<ItemCount; i++)
	{	
		dbg("i = %d\n", i);
		pAttr = GetResponseItemAttr(ItemIndex[i]);

		if(GetDataItemLength(pAttr, CurPos, (uint16)(EndPos-CurPos), &DataLen) != STATUS_OK)
		{
			LOGE("length wrong.");
			
			return STATUS_FAIL;
		}

		if(CurPos + DataLen > EndPos)
		{
			LOGE("recv data length too short");
			
			return STATUS_FAIL;
		}
		
		//LOGD("Opt:%s:",pAttr->name);
		PrintCharArray((byte *)CurPos, DataLen);

		if(DataLen != 0)
		{
			if(SetResponseItem(ItemIndex[i], (byte *)CurPos, DataLen) != STATUS_OK)
			{
				return STATUS_FAIL;
			}
			
			CurPos += DataLen;

			if(ItemIndex[i] == RESP_ISSUER_APP_DATA && ItemIndex[0] == RESP_CMD_TRANS_COM)
			{
				LOGD("Opt:script result list:");
				PrintCharArray((byte *)CurPos, EndPos-CurPos);
				if(EndPos-CurPos != 0)
				{
					if(SetResponseItem(RESP_SCRIPT_RESULT, (byte *)CurPos, EndPos-CurPos) != STATUS_OK)
					{
						return STATUS_FAIL;
					}

					return STATUS_OK;
				}
			}
			
		}
		else
		{
			if(ItemIndex[i] == RESP_ISSUER_APP_DATA && ItemIndex[0] == RESP_CMD_TRANS_COM)
			{
				LOGD("Opt:script result list:");
				PrintCharArray((byte *)CurPos, EndPos-CurPos);
				if(EndPos-CurPos != 0)
				{
					if(SetResponseItem(RESP_SCRIPT_RESULT, (byte *)CurPos, EndPos-CurPos) != STATUS_OK)
					{
						return STATUS_FAIL;
					}

					return STATUS_OK;
				}
			}
		}
		
		if((pAttr->len_type == VAR_LEN ||pAttr->exist_type == ETYPE_OPT)
				&&(i!=ItemCount-1))
		{
			CurPos += 1;
		}
		
		if(ItemIndex[i] == RESP_STATUS_CODE)
		{
			pData = GetResponseItemValue(ItemIndex[i]);

			if(pData->ptr[0] != RESP_SC_SUCCESS)
			{
				return STATUS_OK;
			}
		}
	}

	if(CurPos != EndPos)
	{
		LOGE("data too much...");
		PrintCharArray((byte *)CurPos, EndPos - CurPos);
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}

int32 ParseLoadConfigResp(const char buf[], uint16 len)
{
	uint16 ItemIndex[]=
	{
		RESP_CMD_LOAD_CONFIG,
		RESP_STATUS_CODE
	};
	
	uint16 ItemCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	
	return ParseResp(ItemIndex, ItemCount, buf, len);
}

typedef enum
{
	eIndexAppInfoAID =0,
	eIndexAppInfoLabel,
	eIndexAppInfoCardHolderConfirm,
	eIndexAppInfoPreferredName,
	eIndexAppInfoIssuerCodeTableIndex,
	eIndexAppInfoIssuerData,
	eIndexAppInfoItemCount
}EIndexAppInfo;

int32 ParseAidInfo(const char buf[], uint16 len)
{
	int i;
	char tmp[1024*2];
	char val[eIndexAppInfoItemCount][256];
	char *p;
	char *CurPos;
	int fieldLen;
	SAidInfo *pAidInfo;
	
	if(g_CandidateAidCount == MAX_CANDIDATE_AID_COUNT)
	{
		dbg("Candidate aid is too many.");
		return STATUS_OK;
	}
	
	PrintCharArray((byte *)buf, len);

	memset(val, 0x00, sizeof(val));
	
	memcpy(tmp, buf, len);
	tmp[len] ='\0';

	p =tmp;
	i=0;
	
	while(1)
	{
		CurPos =p;
		p =strchr(CurPos, ';');

		if(p == NULL)
		{
			break;
		}

		fieldLen = p -CurPos;
		memcpy(val[i], CurPos, fieldLen);
		val[i][fieldLen] = '\0';
		
		p +=1;
		i++;
	}
	
	if(i !=eIndexAppInfoItemCount-1)
	{
		return STATUS_FAIL;
	}

	strcpy(val[i], CurPos);

	for(i=0; i<eIndexAppInfoItemCount; i++)
	{
//		printf("%s\n", val[i]);
	}
	

	pAidInfo=&(g_CandidateAidList[g_CandidateAidCount]);

	if(strlen(val[eIndexAppInfoCardHolderConfirm]) != 1)
	{
		return STATUS_FAIL;
	}
	
	strcpy(pAidInfo->Aid, val[eIndexAppInfoAID]);
	strcpy(pAidInfo->Label, val[eIndexAppInfoLabel]);

	pAidInfo->IsNeedCardholderConfirm = val[eIndexAppInfoCardHolderConfirm][0] - '0';
//	printf("val[eIndexAppInfoCardHolderConfirm] is %s\n", val[eIndexAppInfoCardHolderConfirm]);
//	printf("pAidInfo->IsNeedCardholderConfirm is %c\n", pAidInfo->IsNeedCardholderConfirm);
	strcpy(pAidInfo->PreferName, val[eIndexAppInfoPreferredName]);
	strcpy(pAidInfo->IssuerCodeTableIndex, val[eIndexAppInfoIssuerCodeTableIndex]);
	strcpy(pAidInfo->IssuerData, val[eIndexAppInfoIssuerData]);
	
/*
	if(strlen(pAidInfo->PreferName) !=0)
	{
		strcpy(pAidInfo->DispLabel, pAidInfo->PreferName);
	}
	else 
	*/
	//2011.9.23 Removed comments Siken
	if(strlen(pAidInfo->PreferName) !=0 && strlen(pAidInfo->IssuerCodeTableIndex) != 0)
	{
//		printf("pAidInfo->PreferName = %s \n pAidInfo->IssuerCodeTableIndex == %s\n", 
//			pAidInfo->PreferName, pAidInfo->IssuerCodeTableIndex);
		strcpy(pAidInfo->DispLabel, pAidInfo->PreferName);
	}
	else 
	if(strlen(pAidInfo->Label) != 0)
	{
		strcpy(pAidInfo->DispLabel, pAidInfo->Label);
	}
	else
	{
		strcpy(pAidInfo->DispLabel, pAidInfo->Aid);
	}
	
	g_CandidateAidCount++;	

	return STATUS_OK;
}

int32 ParseCandidateAidList(const char buf[], uint16 len)
{
	int i;
	const char *CurPos;
	const char *EndPos;
	uint16 AidInfoLen;

	LOGD("AID Candidate Info:");
	PrintCharArray((byte *)buf, len);
	CurPos = buf;
	EndPos = buf+len;

	g_CandidateAidCount =0;
	
	AidInfoLen =0;
	while(CurPos <EndPos)
	{
		if(CurPos[0] == '|')
		{
			if(ParseAidInfo(CurPos-AidInfoLen, AidInfoLen) != STATUS_OK)
			{
				return STATUS_FAIL;
			}
			AidInfoLen =0;
		}
		else
		{
			AidInfoLen++;
		}

		CurPos++;
	}

	if(ParseAidInfo(CurPos-AidInfoLen, AidInfoLen) != STATUS_OK)
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	if(g_CandidateAidCount != 1)
	{
		for(i=0; i<g_CandidateAidCount; i++)
		{
			g_CandidateAidList[i].IsNeedCardholderConfirm = TRUE;
		}
	}

	LOGD("ParseCandidateAidList return STATUS_OK");
	return STATUS_OK;
}

#if 0
void TestParseCandidateAidList()
{
	char *buf="AID1;Label1;0;Name1;00;1234|AID2;Label2;0;;;|AID3;Label3;1;Name3;;|AID4;Label4;0;Name4;00;";
	int len =strlen(buf);

	g_CandidateAidCount =0;

	if(ParseCandidateAidList(buf, len) != STATUS_OK)
	{
		printf("Parse Candidate AID List Fail.\n");
	}
	else
	{
		printf("Parse Candidate AID List OK ");
	}
}
#endif

int32 ParseGetAidListResp(const char buf[], uint16 len)
{
#if 0
		char *str="E1.03;aa;1|4;aa;1;dd|5;aa;1;dd;ee";
		strcpy(buf, str);
		len = strlen(str);
#endif

	if(len < 3+1)
	{
		return STATUS_FAIL;
	}
	
	SetResponseItem(RESP_STATUS_CODE, (byte *)(buf +3), 1);

	if(buf[3] == '0')
	{
		return ParseCandidateAidList(buf + 4, len -4);
	}
	else
	{
		return STATUS_OK;
	}
}
int32 ParseInitTransResp(const char buf[],uint16 len)
{
	int32 ret;
	uint16 ItemIndex[]=
	{
		RESP_CMD_GET_AID_LIST,
		RESP_STATUS_CODE,
		RESP_CONDITIONS,
		RESP_CARD_EFF_DATE,
		RESP_CARD_EXP_DATE,
		RESP_AIP,
		RESP_AUC,
		RESP_PAN_SEQ_NO,
		RESP_SERVICE_CODE,
		RESP_PREFER_LANG,
		RESP_APP_VERSION,
		RESP_CA_PK_INDEX,
		RESP_AID,
		RESP_PAN,
		RESP_TRACK2,
		RESP_CARDHOLDER_NAME,
		RESP_APP_LABEL
	};
dbg("\nParseInitTransResp...\n");
	uint16 ItemCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	ret= ParseResp(ItemIndex, ItemCount, buf, len);
	if(ret != STATUS_OK)
	{
		return ret;
	}

	if(IsResponseItemExisted(RESP_SERVICE_CODE)
		&&(strcmp((char *)(GetResponseItemValue(RESP_SERVICE_CODE)->ptr),"000")==0))
	{
		ResetResponseItem(RESP_SERVICE_CODE);
	}

#ifdef VPOS_APP   
    {
        extern void vUnpackPan(uchar *psIn, uchar *pszPan);
        extern int iGetEnvCardId(unsigned char *pszCardId, unsigned char *pucCardSeqNo, unsigned char *pszTrack2);
        extern void vSetDispRetInfo(int flag);
        
        uchar szCardId1[20];
        uchar ucCSN;
        
        szCardId1[0]=0;
        iGetEnvCardId(szCardId1, &ucCSN, NULL);
        
        if(strlen(szCardId1)<10)
            return STATUS_FAIL;
        
        //确认卡号
        vClearLines(2);
        _vDisp(2, "请确认卡号:");
        _vDisp(3, szCardId1);
        vDispCancelAndConfirm(1, 1);
#ifdef _AUTO_TEST
        if(iGetAutoTestFlag())
        {
            iOK(1);
        }else
#endif
        {
            if(iOK(30)!=1)
            {
                vSetDispRetInfo(0);
                return STATUS_FAIL;
            }
        }
        vClearLines(2);
        
        dbg("uiTransType=%04X\n", gl_TransRec.uiTransType);    
        //消费撤销或者预授权完成撤销，比对卡号是否一致
        if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
        {
            uchar szCardId2[20];

            szCardId1[0]=0;
            iGetEnvCardId(szCardId1, &ucCSN, NULL);
            vUnpackPan(gl_TransRec.sPan, szCardId2);
            
            dbg("szCardId1=%s, szCardId2=%s, csn1=%02X, csn2=%02x\n", szCardId1, szCardId2, gl_TransRec.ucPanSerNo, ucCSN);
            if(strcmp((char*)szCardId1, (char*)szCardId2))
            {
                vSetDispRetInfo(0);
                
                vClearLines(2);
                vMessage("卡号与原交易不一致");
                return STATUS_FAIL;
            }
            /*
            if(gl_TransRec.ucPanSerNo!=ucCSN)
            {
                vSetDispRetInfo(0);
                
                vClearLines(2);
                vMessage("非原卡");
                return STATUS_FAIL;
            }
            */
        }
    }
#endif
    
	return STATUS_OK;
}

int32 ParseEmvTransResp(const char buf[],uint16 len)
{
	uint16 ItemIndex[]=
	{
		RESP_CMD_EMV_TRANS,
		RESP_STATUS_CODE,
		RESP_EMV_RESULT_CODE,
		RESP_AC,
		RESP_CID,
		RESP_ATC,
		RESP_TVR,
		RESP_TSI,
		RESP_UNPREDICTABLE_NUM,
		RESP_RAND_ONLINE_TRANS_SELECT_NUM,
		RESP_CVR,
		RESP_IAC,
		RESP_IAC_TYPE,
		RESP_SIGNATURE,
		//Jason added on 2012.04.24 for ISEC
		RESP_EC_ISSUER_AUTH_CODER,
		//End
		//Jason added on 2012.04.24 for ISEC
		RESP_IS_EC,
		//End
		//Jason added on 2014.01.13 for tag 0x8A
		RESP_TRANS_RESP_CODE,
		//End
		RESP_ISSUER_APP_DATA,
		RESP_PIN_BLOCK, 
		};
	
	uint16 ItemCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	
	return ParseResp(ItemIndex, ItemCount, buf, len);
}

int32 ParseEmvCompleteResp(const char buf[],uint16 len)
{
	uint16 ItemIndex[]=
	{
		RESP_CMD_TRANS_COM,
		RESP_STATUS_CODE,		
		RESP_TRANS_RESP_CODE,
		RESP_EMV_RESULT_CODE,
		RESP_AC,
		RESP_CID,
		RESP_ATC,
		RESP_TVR,
		RESP_TSI,
		RESP_UNPREDICTABLE_NUM,
		RESP_CVR,
		RESP_IAC,
		RESP_IAC_TYPE,
		//Jason added on 2012.04.24 for ISEC
		RESP_IS_EC,
		//End
		RESP_ISSUER_APP_DATA,
		//RESP_SCRIPT_RESULT, auto parse
	};
	
	uint16 ItemCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	
	return ParseResp(ItemIndex, ItemCount, buf, len);
}



int32 ParseEndTransResp(const char buf[], uint16 len)
{
	uint16 ItemIndex[]=
	{
		RESP_CMD_END_TRANS,
		RESP_STATUS_CODE
	};
	
	uint16 ItemCount = sizeof(ItemIndex)/sizeof(ItemIndex[0]);
	
	return ParseResp(ItemIndex, ItemCount, buf, len);
}

