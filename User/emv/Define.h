#ifndef INCLUDE_DEFINE_H
#define INCLUDE_DEFINE_H

//#include "user_projectconfig.h"

#define	FRAME_BUFFER_SIZE	512

#ifndef byte
//typedef unsigned char byte;
#define byte unsigned char
#endif

#ifndef uchar
#define uchar unsigned char
#endif

#ifndef u8
#define u8 unsigned char
#endif	

#ifndef u32
#define u32 unsigned int
#endif	

#ifndef u16
#define u16 unsigned short
#endif

//typedef unsigned char uchar;
typedef signed char int8;
typedef unsigned char uint8;
typedef short int16;
typedef unsigned short uint16;
typedef int int32;
typedef unsigned int uint32;

#ifndef bool
	#define bool unsigned char
#endif

#ifndef TRUE
	#define TRUE  1
	#define FALSE 0
#endif

//#define APP_VERSION		"1.0.1"
//#define APP_VERSION		"1.0.0"
#define APP_VERSION			"0.6.1"
#define FIRMWARE_VERSION	"V1.0.0"

//xf add
#define VPOS_APP
//#define LKL_TEST_ENV        //测试环境SIT
//#define LKL_UAT_ENV       //验收环境
//#define LKL_PROD_ENV      //正式环境

#define TMS_PROC

#define SUPPORT_TMS_FUNC
//#define TESTTUSN "000073021HYBL0000005"  //测试异常的SN号
//#define TESTTUSN "000073021HYBL0000002"   //这是我自己测试用的
//#define TESTTUSN "000073021HYBL0000003"    //任涛
//#define TESTTUSN "000073021HYBL0000004"   //赵力 
//#define TESTTUSN "000073021HYBL0000010"     //客户1
//#define TESTTUSN "000073021HYBL0000011"  //客户2
//#define TESTTUSN "000073021HYBL0000012"  //客户3  wifi
//#define TESTTUSN "000073021HYBL0000013"  //客户4 wifi
//#undef SUPPORT_TMS_FUNC
typedef struct
{
	char Aid[64];
	char Label[64];
	char IsNeedCardholderConfirm;
	char PreferName[64];
	char IssuerCodeTableIndex[2+1];
	char IssuerData[256];
	char DispLabel[64];
}SAidInfo;

//Jason modified on 2020.12.9
//#define MAX_CANDIDATE_AID_COUNT 20
#define MAX_CANDIDATE_AID_COUNT 10

#define STATUS_OK 				0
#define STATUS_FAIL 			1
#define STATUS_BUF_TOO_SMALL 	2
#define STATUS_TRANS_TIMEOUT 	3
#define STATUS_USER_CANCEL		4
#define STATUS_SERV_CONN_TIMEOUT 5
#define STATUS_NEED_SEND_END_TRANS_CMD 6
#define STATUS_HOST_FAIL        10
#define ITEM_SEPARATOR '|'

//enum---------------------------------------------------------
typedef enum{
	//command
	REQ_CMD_LOAD_CONFIG,
	REQ_CMD_GET_AID_LIST,
	REQ_CMD_INIT_TRANS,
	REQ_CMD_EMV_TRANS,
	REQ_CMD_TRANS_COM,
	REQ_CMD_END_TRANS,
	REQ_MSG_TYPE,
	
	// load config
	REQ_IFD_SERIAL_NUMBER, 	
	REQ_TERM_CTRY_CODE,	
	REQ_TERM_TYPE,

	REQ_TERM_CAP,
	REQ_TERM_CAP_INPUT,	
	REQ_TERM_CAP_CVM,
	REQ_TERM_CAP_SECURITY,

	REQ_TERM_ADD_CAP,
	REQ_TERM_ADD_TRANS_TYPE,
	REQ_TERM_ADD_INPUT,
	REQ_TERM_ADD_OUTPUT,
	REQ_IS_SUPPORT_PSE,
	REQ_AID_LIST,

	//Get AID List
	REQ_TIMEOUT,

	//init Transaction
	REQ_AMOUNT_AUTHOR,
	REQ_AMOUNT_OTHER,
	REQ_TRANS_DATE,
	REQ_TRANS_TIME,
	REQ_TRANS_TYPE,
	REQ_POS_ENTRY_MODE_CODE,
	REQ_TRANS_SEQ_NUMBER,
	REQ_ACCOUNT_TYPE,
	REQ_FLOOR_LIMIT,
	REQ_TAR_PERCENT,
	REQ_MAX_TAR_PERCENT,
	REQ_THRESHOLD_AMOUNT,
	REQ_MERCHANT_CAT_CODE, 
	REQ_MERCHANT_ID,
	REQ_TERM_ID,
	REQ_TRANS_CUR_CODE,
	REQ_TRANS_CUR_EXP,
	REQ_TRANS_REFERENCE_CUR_CODE,
	REQ_TRANS_REFERENCE_CUR_EXP,
	REQ_IS_SUPPORT_VLP,
	REQ_BYPASS_MODE,
	REQ_TERM_RISK_MANAGE_DATA,
	REQ_MERCHANT_NAME_LOCATION,
	REQ_SELECTED_AID,
	REQ_ACQUIRER_ID,
	REQ_DEFAULT_TAC,
	REQ_DENIAL_TAC,
	REQ_ONLINE_TAC,
	REQ_TDOL,
	REQ_DDOL,
	
	//emv  transaction
	REQ_CARD_HOT,
	REQ_FORCE_ONLINE,	
	REQ_TRANS_LOG_AMOUNT,
	REQ_PIN_PAD_SECRET_KEYS,	
	REQ_PIN_PROMPT_OFFLINE_PLAIN,
	REQ_PIN_PROMPT_OFFLINE_ENCRYPT,
	REQ_PIN_PROMPT_ONLINE_ENCRYPT,
	REQ_PIN_LAST_PIN_TRY,
	REQ_PK_HASH_ALGOR,
	REQ_PK_ALGOR,
	REQ_PK_EXPONENT,
	REQ_PK_MODULUS,
	REQ_PK_CHECKSUM,
	REQ_PK_ACTIVE_DATE,
	REQ_PK_EXPIRE_DATE,

	//emv completion	
	REQ_HOST_IP,
	REQ_HOST_PORT,
	REQ_RESP_MODE,
	REQ_ARC,
	REQ_ISSUER_AUTH_DATA,
	REQ_SCRIPT,
	
	REQ_ITEM_COUNT
}EReqIndex;

typedef enum {
	RESP_CMD_LOAD_CONFIG,
	RESP_CMD_GET_AID_LIST,
	RESP_CMD_INIT_TRANS,
	RESP_CMD_EMV_TRANS,
	RESP_CMD_TRANS_COM,
	RESP_CMD_END_TRANS,
	
	RESP_STATUS_CODE,	
	RESP_EMV_RESULT_CODE,
	
	//read meida
	RESP_CONDITIONS,
	RESP_CARD_EFF_DATE,
	RESP_CARD_EXP_DATE,
	RESP_AIP,
	RESP_AUC,
	RESP_PAN_SEQ_NO,
	RESP_SERVICE_CODE,
	RESP_PREFER_LANG,
	RESP_APP_VERSION,	
	RESP_CA_PK_INDEX,
	RESP_AID,
	RESP_PAN,
	RESP_TRACK1,
	RESP_TRACK2,
	RESP_CARDHOLDER_NAME,
	RESP_APP_LABEL,

	//emv transaction
	RESP_AC,
	RESP_CID,
	RESP_ATC,
	RESP_TVR,
	RESP_TSI,
	RESP_UNPREDICTABLE_NUM,
	RESP_RAND_ONLINE_TRANS_SELECT_NUM,
	RESP_CVR,
	RESP_IAC,
	RESP_IAC_TYPE,
	RESP_SIGNATURE,	
//Jason added for EC
	RESP_EC_ISSUER_AUTH_CODER,
//end
//Jason added for ISEC
 	RESP_IS_EC,
//end
	RESP_ISSUER_APP_DATA,
	RESP_PIN_BLOCK,
	//emv completion
	
	RESP_TRANS_RESP_CODE,
	RESP_SCRIPT_RESULT,

	RESP_ITEM_COUNT
}ERespIndex;


enum {
	DTYPE_BINARY,
	DTYPE_NUMERIC,
	DTYPE_ASCII,
	DTYPE_BOOL,
};

enum{
	ETYPE_OPT,
	ETYPE_MUST
};

//struct------------------------------------------------------
typedef struct
{
	uchar *ptr;
	uint16 len;
}vec_t;

typedef struct
{
	byte	data_type;	
	byte	exist_type;
	byte	len_type;
	uint16 	min_len;
	uint16 	max_len;
	uchar 	*name;	
}DataItemAttr_t;

typedef struct
{
	uint16 index;
	vec_t	data;
}ReqDataItem_t;
typedef ReqDataItem_t RespDataItem_t;

#define	TRANS_T_GOOD_SERVICE		 	"00"	//Good and Services
#define	TRANS_T_CASH		 	"01"	//Cash
#define	TRANS_T_DEBIT_ADJUST 	"02" 	//Debit Adjustment
#define	TRANS_T_GOOD_CASHBACK 	"09"	//Goods and Services with Cash Back
#define	TRANS_T_RETURN			"20"	//Returns
#define	TRANS_T_DEPOSIT 		"21"	//Deposits
#define	TRANS_T_CREDIT_ADJUST	"22"	//Credit Adjustment
#define	TRANS_T_AVAIL_FUND_ENQ	"30"	//Available Fund Enquiry
#define	TRANS_T_BALANCE_ENQ		"31"	//Balance Enquiry
#define	TRANS_T_CARD_VERIFY_ENQ	"37"	//Card Verify Enquiry
#define	TRANS_T_MINI_STATE_ENQ	"38"	//Mini Statement Enquiry
#define	TRANS_T_PIN_CHANGE		"92"	//PIN Change
#define TRANS_T_READ_LOG		"ff"	//Read log

#define FIX_LEN 	0
#define VAR_LEN 	1


#define CMD_LOAD_CONFIG 	"E0."
#define CMD_GET_AID_LIST	"E1."
#define CMD_INIT_TRANS		"E2."
#define CMD_EMV_TRANS  		"E4."
#define CMD_EMV_COMPLETE	"E5."
#define CMD_EMV_END_TRANS	"ED."
#define CMD_EMV_GET_CHECKSUM "EV."

//#define MAX_SEND_BUF_SIZE	1024*10
//#define MAX_RECV_BUF_SIZE	1024*10


//#define MAX_COMM_HOST_SEND_BUF_SIZE 1024*10
//#define MAX_COMM_HOST_RECV_BUF_SIZE 1024*10

#define MAX_SEND_BUF_SIZE	1024*5
#define MAX_RECV_BUF_SIZE	1024*5


#define MAX_COMM_HOST_SEND_BUF_SIZE 1024*2
#define MAX_COMM_HOST_RECV_BUF_SIZE 1024*2


#define MAX_SCRIPT_DATA_BUF_SIZE 1024
#define	RESP_SC_SUCCESS 		'0'
#define RESP_SC_FAILURE			'1'
#define RESP_SC_TIMEOUT			'2'
#define RESP_SC_CARD_BLOCK		'3'
#define RESP_SC_MSG_FMT_ERR 	'5'
#define RESP_SC_APP_BLOCK		'6'
#define RESP_SC_USER_CANCEL		'7'
#define RESP_SC_NO_APP			'8'
#define RESP_SC_APP_NOT_ACCEPTED '9'
#define RESP_SC_READ_RECORD_ERR	 'a'
#define RESP_EMV_SC_TC		'0'
#define RESP_EMV_SC_ARQC 	'1'
#define RESP_EMV_SC_AAC 	'2'
#define RESP_EMV_SC_AAR		'3'
#define RESP_EMV_SC_SERVICE_NOT_ALLOWED '5'

//2011.9.23 Siken added
#define RESP_EMV_SC_SERVICE_NOT_ALLOWED_WITH_TC '7'
#define RESP_EMV_NULL 		'F'

#define TAG_AUTH_CODE 	0x89



#define REQ_RESP_MODE_HOST_COMM_FAIL 	0
#define REQ_RESP_MODE_HOST_APPROVED  	1
#define REQ_RESP_MODE_VOICE_APPROVED	2
#define REQ_RESP_MODE_OTHER_APPROVED  	3
#define REQ_RESP_MODE_HOST_DECLINED		4
#define REQ_RESP_MODE_VOICE_DECLINED	5
//Jason added 20110905
#define REQ_RESP_MODE_INVALID_ARC		6
//End

#define LINE_NUM_AMOUNT_TIP_INFO	1
#define LINE_NUM_AMOUNT				5

#define FILE_NAME_COMMON		"Terminal.conf"
#define FILE_NAME_AID_LIST		"AID.conf"
#define FILE_NAME_CA_LIST		"CA.conf"
#define FILE_NAME_EXCEPTION		"Execption.conf"
#define FILE_TRANS_SEQ_COUNTER	"TransSeqCounter.conf"
//#define DIR_CONF			"/home/common/EMVL2Test"
//#define DIR_TRANS_RECORD	"/home/common/EMVL2Test/TransRecord"
//#define DIR_CONF			"/data/data/com.szsitc.jay.sitc_emvl2"
#define DIR_EMVL2						"/Emvl2"
#define DIR_CONF						"/Emvl2/Conf"
#define DIR_TRANS_RECORD				"/Emvl2/Trans"
#define DIR_BATCH_RECORD				"/Emvl2/Batch"
#define DIR_QPBOC_BATCH_RECORD			"/Emvl2/qpbocBatch"

#define DIR_UPDATE						"/Update"
#define FILE_NAME_UPDATE_NEW_APP		"NewApp"
#define FILE_NAME_UPDATE_APP_FLAG		"UpdateFlag"
#define FILE_NAME_UNZIP_APP		        "UnzipApp"


#define FMT_TRANS_RECORD_FILE_NAME 				"%s/Record%d"
#define FMT_TRANS_BATCH_RECORD_FILE_NAME 		"%s/Record%d"
#define FMT_TRANS_QPBOC_BATCH_RECORD_FILE_NAME 	"%s/Record%d"

#define FMT_TRANS_RECODD_NO_FILE_NAME	"%s/TransNo"
#define FMT_BATCH_RECODD_NO_FILE_NAME	"%s/TransNo"
#define FMT_QPBOC_BATCH_RECODD_NO_FILE_NAME	"%s/TransNo"



#define FILE_NAME_TERM_PARA_BCTC		"BCTCTerminal"
#define FILE_NAME_AID_LIST_PARA_BCTC	"BCTCAID"
#define FILE_NAME_CA_LIST_BCTC			"BCTCCA"
#define FILE_NAME_EXCEPTION_BCTC		"BCTCExecption"





#define MAX_PARAM_DOWN_BUF_LEN	1024*6
#define MAX_OPT_ITEM_VALUE_LEN	1024
#define MAX_FILE_PATH_LEN	256
//#define MAX_FILE_PATH_LEN	1024
//#define MAX_TRANS_RECORD_NUM 1024
//#define MAX_TRANS_RECORD_NUM 400
#define MAX_TRANS_RECORD_NUM 300


enum{
	SRC_RESP,
	SRC_REQ
};


#define ACCOUNT_TYPE_OTHERS 		"00"
#define ACCOUNT_TYPE_SAVINGS		"10"
#define ACCOUNT_TYPE_DEBIT_CHEQUE	"20"
#define ACCOUNT_TYPE_CREDIT 		"30"

//#define ReportLine()	printf("Now File:%s, Fuc:%s, //Line:%d\n", __FILE__, __func__, __LINE__)

//Jason added on 2017.05.01
#define TRANSACTION_RESULT_NOT_START_BCTC   	0
#define TRANSACTION_RESULT_APPROVED_BCTC		1
#define TRANSACTION_RESULT_DECLINED_BCTC		2
#define TRANSACTION_RESULT_TERMINATED_BCTC		3
#define TRANSACTION_RESULT_SERVICE_NOT_AOLLOWED	4
//End
//Jason added on 2020.05.26
#define TRANSACTION_RESULT_Q_OFFLINE_APROVED_BCTC   	0x11
#define TRANSACTION_RESULT_Q_OFFLINE_DECLINE_BCTC		0x12
#define TRANSACTION_RESULT_Q_TERMINATED_BCTC			0x13
#define TRANSACTION_RESULT_Q_ONLINE_APROVED_BCTC		0x14
#define TRANSACTION_RESULT_Q_ONLINE_DECLINE_BCTC		0x15
#define TRANSACTION_RESULT_Q_ONLINE_FAIL_DECLINE_BCTC	0x16
#define TRANSACTION_RESULT_Q_ONLINE_AND_EXPCARD_BCTC	0x18
#define TRANSACTION_RESULT_Q_DECLINE_AND_EXPCARD_BCTC	0x19

//End



//Jason added on 2017.5.13
#define TERMINAL_CAPABILITIES_STRING_EMV_DEFUALT	"E0F8C8"

#define ADD_TERMINAL_CAPABILITIES_STRING_EMV_DEFUALT	"F000F0A001"

#define IFD_SERIAL_NUMBER_DEFAULT						"12345678"
#define TRANS_COUNTRY_CODE_DEFAULT						"0840"
#define IFD_DEFAULT						"Terminal"
#define MERCHANT_ID_DEFAULT				"000000000000000"
#define TERM_COUNTRY_CODE_EMV_DEFAULT				"840"
#define TERM_COUNTRY_CODE_PBOC_DEFAULT				"156"
//#define TERM_TYPE_DEFAULT				"22"
#define TERM_TYPE_DEFAULT				"21"

#define SERVER_BCTC

#define PARAMETER_SERVER_BCTC

//Jason added Start: 2019/11/18
#define CY20_EMVL2_KERNEL
//Jason added End: 2019/11/18

#endif

