#ifndef INCLUDE_REQ_DATA_SET_H
#define INCLUDE_REQ_DATA_SET_H

#include "Define.h"

int32 SetReqCmdDataItem(void);
void SetReqItemTransDateTime(void);
int32 SetLoadConfigItem(void);
int32 SetReadMediaItem(void);
int32 GSetDataItemPublicKey(byte aid[], byte index);
int32 SetEmvTransItem(void);
int32 SeReqItemAmount(void);
int32 SetReqItemTransSeqCounter(void);
int32 SetInitTransItem(void);
int32 SetEmvCompleteItem(void);
int32 qpbocSetTransSeqCounter(byte *transSeqCounter, uint16 *len);

#endif
