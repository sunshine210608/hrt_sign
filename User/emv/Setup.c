#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//#include <arpa/inet.h>

#include "EmvAppUi.h"
#include "Util.h"
#include "Define.h"
#include "ReqDataItem.h"

#define LINE_NUM_TIP		1
#define LINE_NUM_OLD_VALUE 	3
#define LINE_NUM_NEW_VALUE 	5

#ifdef TS700_IP
#define KEY_PREV	KEYBORADEVM_MASK_KEY_F1
#define KEY_NEXT	KEYBORADEVM_MASK_KEY_F2
#define	KEY_EDIT	KEYBORADEVM_MASK_KEY_F3
#define KEY_EXIT	KEYBORADEVM_MASK_KEY_F4
#else
#define KEY_PREV	1
#define KEY_NEXT	2
#define	KEY_EDIT	3
#define KEY_EXIT	4
#endif

typedef struct
{
	char	*tip;
	char	*optname;
}SetupItem_t;

enum{
	IDX_SERV_IP,
	IDX_CONN_TIMEOUT,
	IDX_IS_FORCE_ONLINE,
	IDX_DATE,
	IDX_TIME, 
	IDX_KER_SERV_IP,
	IDX_KER_SERV_PORT,
	IDX_LOCAL_IP,
	IDX_IS_SUPPORT_EC,
};

SetupItem_t g_SetupItemList[]=
{
	{"Config Serv IP"	,"CfgServIP"},
	{"Conn Timeout"		,"CfgServTimeout"},	
	{"Is Force Online",	"CfgIsForceOnline"},
	{"Setup Date"		,""},
	{"Setup Time"		,""},
	{"Conf Kernel Srv IP", "CfgKerServIP"},
	{"Conf Kernel Srv Port", "CfgKerServPort"},
	{"Conf Local IP", "CfgLocalIP"},
	//{"Is Support EC", "CfgIsSupportEC"},
};

extern int ConfigLocalNetwork(char *szLocalIP);

void DisplaySetupOldValue(uint16 index)
{
	SetupItem_t *pItem;
	char line[80];
	
	memset(line, 0x00, sizeof(line));

	pItem = &(g_SetupItemList[index]);

	if(index == IDX_DATE)
	{
		DisplayInfo(GetSysDate(),LINE_NUM_OLD_VALUE);
	}
	else if(index == IDX_TIME)
	{
		DisplayInfo(GetSysTime(), LINE_NUM_OLD_VALUE);
	}
	else 
	{
		if(GetOptValue(pItem->optname, line, sizeof(line)) == STATUS_OK)
		{
			DisplayInfo(line, LINE_NUM_OLD_VALUE);
		}
		else
		{
			DisplayInfo("", LINE_NUM_OLD_VALUE);
		}
	}
}
void DisplaySetupTipInfo(uint16 index)
{
	SetupItem_t *pItem;
	pItem = &(g_SetupItemList[index]);
	
	DisplayInfo(pItem->tip, LINE_NUM_TIP);
}

void ChangeData(uint16 index)
{
#if 0
	int status;
	char 	buf[20];
	uint8 	len = sizeof(buf);
	SetupItem_t *pItem;

	pItem = &(g_SetupItemList[index]);
	
	ClearScreen();
	DisplaySetupTipInfo(index);
	DisplaySetupOldValue(index);
	
	if(GetInputNumStr(LINE_NUM_NEW_VALUE,buf, len) == INPUT_STATUS_OK)
	{
		if(index == IDX_DATE)
		{
		  status =SetSysDate(buf);
		}
		else if(index == IDX_TIME)
		{
			status = SetSysTime(buf);
		}
		else if (index == IDX_LOCAL_IP)
		{
			status = ConfigLocalNetwork(buf);
			if(status != STATUS_OK)
			{			
				DisplayValidateInfo("Config local IP Fail");
				return;
			}
			status =SaveOptValue(pItem->optname, buf);
		}
		else
		{
			status =SaveOptValue(pItem->optname, buf);
		}

		if(status != STATUS_OK)
		{			
			DisplayValidateInfo("Save Config Fail");
		}
	}
#endif
}

void Setup()
{
#if 0
	uint16 i;
	uint32  mask;
	TEvntMask	event;
	uint16 ItemCount;

	ItemCount = sizeof(g_SetupItemList)/sizeof(g_SetupItemList[0]);

	for(i=0; i<ItemCount; i++)
	{
		ClearScreen();
		DisplaySetupTipInfo(i);
		DisplaySetupOldValue(i);
		DisplayInfoRight("  1.Prev",2);		
		DisplayInfoRight("  2.Next",4);
		DisplayInfoRight("  3.Edit",6);
		DisplayInfoRight("  4.Exit",8);
				
		if(i == 0)
		{	if(ItemCount == 1)
			{
				mask =KEY_EDIT | KEY_EXIT;
			}
			else
			{
				mask = KEY_NEXT | KEY_EDIT | KEY_EXIT;
			}
		}
		else if(i == ItemCount - 1)
		{
			mask = KEY_PREV | KEY_EDIT | KEY_EXIT;
		}
		else
		{
			mask = KEY_PREV | KEY_NEXT | KEY_EDIT | KEY_EXIT;	
		}

		event = WaitAKeyDuringTime(mask,60);
		switch(event) 
		{
			case KEY_PREV :
				i -= 2;
				break;
			case KEY_NEXT :
				//do nothing.
				break;
			case KEY_EDIT:
				ChangeData(i);
				i -= 1;
				break;
			case KEY_EXIT:
				return ;
				break;
		}
	}
#endif
}

int GetParamServIP(char buf[], int len)
{	
	if(GetOptValue(g_SetupItemList[IDX_SERV_IP].optname, buf, len) != STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}

int GetParamKerServIP(char buf[], int len)
{	
	if (GetOptValue(g_SetupItemList[IDX_KER_SERV_IP].optname, buf, len) != STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}

int GetParamKerServPort(char buf[], int len)
{	
	if (GetOptValue(g_SetupItemList[IDX_KER_SERV_PORT].optname, buf, len) != STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}

int GetParamLocalIP(char buf[], int len)
{	
	if (GetOptValue(g_SetupItemList[IDX_LOCAL_IP].optname, buf, len) != STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}

int GetDownloadParamTimeout()
{
	char buf[10];
	int timeout;
	
	if(GetOptValue(g_SetupItemList[IDX_CONN_TIMEOUT].optname, buf, sizeof(buf)) == STATUS_OK)
	{
		timeout =atoi(buf);
	}
	else
	{
		 timeout =10;
	}

	if(timeout < 1)
	{
		timeout = 10;
	}

	return timeout;
}

/*
int IsForceOnline()
{
	char buf[10];

	if(GetOptValue(g_SetupItemList[IDX_IS_FORCE_ONLINE].optname, buf, sizeof(buf)) != STATUS_OK)
	{
		return FALSE;
	}

	if(buf[0] == '0')
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}
*/
