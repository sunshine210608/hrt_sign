#ifndef UTIL_PRINT_H
#define UTIL_PRINT_H

int PrintHeader(char *info);
int PrintFooter(void);

int NJRCBase_Print_ClearBuffer(void);
int NJRCBase_Print_FillBuffer(char * szBuffer);
int NJRCBase_Print_StartPrint(void);
int NJRCBase_Print_Cut(void);
void NJRCBase_Print_Prepare(int depth);

int TestPrinter_normal(void *m);


#endif


