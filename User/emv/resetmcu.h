//
// Created by jay on 2016/12/9.
//

#ifndef SITCFIRMWARE_RESETMCU_H
#define SITCFIRMWARE_RESETMCU_H

#define RESET_PIN_DEV   "/sys/devices/security_module/maxrstpin"
#define RESET_PIN_HIGH  "1"
#define RESET_PIN_LOW   "0"

int ResetMCU(void);

#endif //SITCFIRMWARE_RESETMCU_H

