//
// Created by jay on 2017/3/19.
//

#ifndef SITCFIRMWARE_WAKEUPMCU_H
#define SITCFIRMWARE_WAKEUPMCU_H

#define RTS_PIN_DEV   "/sys/devices/security_module/rts3_ctrl"
#define CTS_PIN_DEV   "/sys/devices/security_module/cts3_get"
#define WAKEUP_PIN_DEV  RTS_PIN_DEV

#define WAKEUP_PIN_HIGH  "1"
#define WAKEUP_PIN_LOW   "0"

int WakeUpMCU(void);
int ReadCTS(void);
#endif //SITCFIRMWARE_WAKEUPMCU_H
