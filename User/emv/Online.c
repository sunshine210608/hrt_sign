#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <unistd.h>
//#include <sys/socket.h>
//#include <arpa/inet.h>
//#include <fcntl.h>
//#include <sys/errno.h>
#include "EmvAppUi.h"
//#include "Tag.h"
#include "Define.h"
#include "Util.h"
#include "ReqDataItem.h"
#include "RespDataItem.h"
#include "Receipt.h"
#include "Transaction.h"
#include "emvDebug.h"
#include "Online.h"
#include "emvl2/tag.h"
#include "emvl2/global.h"
#include "emvl2/misce.h"
#include "tcpcomm.h"
#include "user_projectconfig.h"

#include "vposface.h"
#include "pub.h"

#if 0//def APP_LKL
#include "AppGlobal_lkl.h"
//#include "MemMana_lkl.h"
#endif
#if 0//def APP_SDJ
#include "AppGlobal_sdj.h"
#include "trans_sdj.h"
#endif
#if 1//def APP_CJT
#include "AppGlobal_cjt.h"
#include "trans_cjt.h"
#endif
#include "formData.h"

int iAppSendAndRecive(const byte * SendBuf,uint16 SendBufLen,
						     byte * RecvBuf, uint16 *pRecvBufLen);

extern void vShowCommErrMsg(int code);
extern void vSetTransRevFlag(int flag);
extern void  iDownLoadBackBIN(char * date,char * cardno);
extern int iGetEnvIcCardClass(void);
extern void translateBcdArrayToString(tti_byte *bcdArray, tti_int32 bcdArrayLen, tti_tchar *string);

int gLeftAmount=0;

#if 0
int32 SocketSendRecv(const uchar *ip,const uchar *port, 
						const byte SendData[], uint16 SendDataLen,
						byte RecvData[], uint16 *pRecvDataLen)
{
	int ret;
	int sock;
	int val;
	int loop;
	int32 recvlen;
	struct sockaddr_in  server;
	struct timeval tv;
	fd_set rfds;
	fd_set wfds;
	
	sock = socket(AF_INET, SOCK_STREAM,0);
	if(sock < 0)
	{
		LOGE("Error at socket():\n");
		return STATUS_FAIL;
	}
	
	memset(&server,0,sizeof(server));
	server.sin_family= AF_INET;
	server.sin_addr.s_addr =inet_addr(ip);
	server.sin_port =htons((uint16)atoi(port));
	
	LOGD("Server IP: %s\n",ip);
    LOGD("Server Port: %s\n", port);

	val =fcntl(sock, F_GETFL, 0);
	fcntl(sock, F_SETFL, val|O_NONBLOCK);

	loop = 0;
	while(loop < 5)
	{
		loop ++;
		if(connect(sock, (struct sockaddr *) &server, sizeof(server)) != 0)
		{
			if(errno == EINPROGRESS)
			{
				tv.tv_sec =4;
				tv.tv_usec =0;

				FD_ZERO(&rfds);
				FD_ZERO(&wfds);
				
				FD_SET(sock, &rfds);
				FD_SET(sock, &wfds);
				ret =select(sock+1, &rfds, &wfds, NULL,&tv);

				if(ret !=1 || !FD_ISSET(sock,&wfds))
				{
		            LOGE("Connect Fail.\n");
					//return STATUS_FAIL;
					continue;
				}
				break;
			}
			else
			{
		        LOGE("Connect Fail.\n");
				//return STATUS_FAIL;
				continue;
			}
			
		}
	}

    LOGD("Send Data " );
	PrintByteArray(SendData, SendDataLen);
	
	if(send(sock,SendData,SendDataLen,0) != SendDataLen)
	{
        LOGE("Send Fail.\n");
		close(sock);
		return STATUS_FAIL;
	}

	memset(RecvData,0, *pRecvDataLen);

	tv.tv_sec =10;
	tv.tv_usec =0;
	FD_ZERO(&rfds);
	FD_SET(sock, &rfds);

	ret =select(sock+1, &rfds, NULL, NULL,&tv);
	
	if(ret !=1 || !FD_ISSET(sock, &rfds))
	{

        LOGE("Recv Fail.\n");
		close(sock);
		return STATUS_FAIL;
	}

	recvlen = recv(sock,RecvData,*pRecvDataLen,0);
	close(sock);

	if(recvlen <=0)
	{
		LOGD("Recv fail.\n");
		return STATUS_FAIL;
	}
	
	*pRecvDataLen = recvlen;
	
	LOGD("Recive Data From the issuer Host.\nlen=%d", *pRecvDataLen);
	PrintByteArray(RecvData,*pRecvDataLen);

	return STATUS_OK;
}
#else
int32 SocketSendRecv(const uchar *ip,const uchar *port,
                     const byte SendData[], uint16 SendDataLen,
                     byte RecvData[], uint16 *pRecvDataLen)
{

    int32 fd;
    int32 ret;
    uint16 rcvLength;


    fd = SocketConnect((char *)ip, (char *)port, 3);
    if (fd < 0)
    {
        LOGE("SocketConnect error");
        return STATUS_FAIL;
    }

    ret = SocketSendData(fd, (byte *)SendData, SendDataLen, 10*1000);
    if (ret != SendDataLen)
    {
    	SocketDisconnect(fd);
        LOGE("SocketSendData error");
        return STATUS_FAIL;
    }

    ret = SocketRcvData(fd, RecvData, 4, 5*1000);
    if (ret != 4){
		SocketDisconnect(fd);
        LOGE("SocketRcvData first 4 byte error");
        return STATUS_FAIL;
    }

	LOGD("List first 4 bytes");
	hexdump(RecvData, 4);
    rcvLength = RecvData[2] * 256 + RecvData[3];
    ret = SocketRcvData(fd, RecvData + 4, rcvLength, 5*1000);
    if (ret != rcvLength){
        LOGE("SocketRcvData left %d byte error", rcvLength);
		SocketDisconnect(fd);
        return STATUS_FAIL;
    }
	LOGD("List left bytes");
	hexdump(RecvData + 4, rcvLength);

	SocketDisconnect(fd);
    *pRecvDataLen = rcvLength + 4;

    return STATUS_OK;
}
#endif

typedef struct 
{
	uint16 tag;	
	uint8  src;
	uint16 index;
}AuthDataItem_t;

#ifdef SERVER_BCTC
int32 GetAuthRequest(byte *buf, uint16 *bufLen)
{
	tti_uint16 tagTLVLen;
	tti_uint16 tagList[] = { 
				#ifndef VPOS_APP
							TAG_AIP, 
							TAG_TRANS_SEQ_COUNTER, 
							TAG_CRYPTOGRAM, 
							TAG_CRYPTOGRAM_INFO_DATA, 
							TAG_CVM_RESULTS, 
							TAG_IFD_SERIAL_NUMBER, 
							TAG_ISSUER_APP_DATA, 
							TAG_TERMINAL_CAPABILITIES,
							TAG_TERMINAL_TYPE, 
                            TAG_TVR, 
                            TAG_UNPREDICTABLE_NUMBER, 
                            TAG_ACQUIRER_ID,
                            TAG_AMOUNT,
                            TAG_AMOUNT_OTHER,
                            TAG_APP_EFFECTIVE_DATE, 
                            TAG_APP_EXPIRATION_DATE, 
                            TAG_PAN, 
                            TAG_PAN_SEQU_NUMBER,
                            TAG_ONLINE_PIN_BLOCK_BCTC,
                            TAG_MERCHANT_CATEGORY_CODE, 
                            TAG_MERCHANT_ID, 
                            TAG_POS_ENTRY_MODE, 
                            TAG_TERM_COUNTRY_CODE,
                            TAG_TERMINAL_ID, 
                            TAG_TRACK2, 
                            TAG_TRANS_CURRENCY_CODE, 
                            TAG_TRANSACTION_DATE, 
                            TAG_TRANSACTION_TIME,
                            TAG_TRANSACTION_TYPE, 
                            TAG_PAYMENT_ACCOUNT_REF, 
                            TAG_TOKEN_REQUESTOR_ID,
                         	//Jason added on 2020/04/24
                            TAG_ATC,
							//End
				#else
							0x9F26,
							0x9F27,
							0x9F10,
							0x9F37,
							0x9F36,
							0x95,
							0x9A,
							0x9C,
							0x9F02,
							0x5F2A,
							0x82,
							0x9F1A,
							0x9F03,
							0x9F33,
							//0x9F74,
                            //0x8A,
							0x9F34,
							0x9F35,
							0x9F1E,
							0x84,
							0x9F09,
							0x9F41
							/*0x91,
							0x71,
							0x72,
							0xDF31,
							0x9F63*/
				#endif														
							};
	

	TagArrsBuildTLV(&(runningTimeEnv.tagList), tagList, sizeof(tagList)/sizeof(tagList[0]), buf + 4, &tagTLVLen);

	buf[0] = STX;
    buf[1] = 0x42;
    buf[2] = tagTLVLen/256;
    buf[3] = tagTLVLen%256;

	*bufLen = tagTLVLen + 4;

	return STATUS_OK;
}
#else
int32 GetAuthRequest(byte *buf, uint16 *bufLen)
{
	vec_t *pData;
	int32 i;
	/*
	uint16 authTagList[15]=
	{
		0x5a, 0x57, 0x5f2a, 0x5f34, 0x82, 0x95, 0x9a, 0x9c, 0x9f02, 0x9f03, 0x9f10,
			0x9f1a, 0x9f26, 0x9f36, 0x9f37
	};
	*/
	/*
	AuthDataItem_t AuthDataList[]=
	{
		{0x5a, 	SRC_RESP,	RESP_PAN},
		{0x82, 	SRC_RESP,	RESP_AIP},
		{0x95, 	SRC_RESP,	RESP_TVR},
		{0x9f10,SRC_RESP,	RESP_ISSUER_APP_DATA},
		{0x9f26,SRC_RESP,	RESP_AC},
		{0x9f36,SRC_RESP,	RESP_ATC},
		{0x00,	SRC_RESP,	RESP_PIN_BLOCK},		
		{0x9f37,SRC_RESP,	RESP_UNPREDICTABLE_NUM},		
		{0x5f34,SRC_RESP,	RESP_PAN_SEQ_NO},
		{0x9a, 	SRC_REQ,	REQ_TRANS_DATE},
		{0x9c, 	SRC_REQ,	REQ_TRANS_TYPE},		
		{0x57, 	SRC_RESP,	RESP_TRACK2},
		{0x9f1a,SRC_REQ,	REQ_TERM_CTRY_CODE},
		{0x5f2a,SRC_REQ,	REQ_TRANS_CUR_CODE},
		{0x9f02,SRC_REQ,	REQ_AMOUNT_AUTHOR},
		{0x9f03,SRC_REQ,	REQ_AMOUNT_OTHER},
		{0x9F99,SRC_REQ,	REQ_MEG_TYPE},
	
	};
	*/
	
	byte 	tempBuf[1024*10]={0};	
	int		tempBufLen=0;
	byte 	tempValueBuf[255];
	uint8 	tempValueLen;
	byte  	tagValue[1024];
	uint8 	tagValueLen;
	DetailItem_t *pItem;
	int index;
	
	for (i=0; i<g_DetailItemCount; i++)
	{
		pItem = &(g_Detail[i]);

		pData =NULL;
		index =pItem->index;

		if(pItem->src == SRC_REQ)
		{
			if(index == REQ_ARC)
			{
				continue;
			}
		}

		if(pItem->src == SRC_RESP)
		{
			if(index == RESP_PIN_BLOCK && !IsResponseItemExisted(RESP_PIN_BLOCK))
			{
				continue;
			}

			if(index == RESP_RAND_ONLINE_TRANS_SELECT_NUM)
			{
				continue;
			}
			
			if(index == RESP_EC_ISSUER_AUTH_CODER && (IsEcTrans() == FALSE))
			{
				continue;
			}
		}
		
		if (pItem->tag>0xff)
		{
			tempBuf[tempBufLen]=(byte)(pItem->tag>>8);
			tempBufLen += 1;
			tempBuf[tempBufLen]=(byte)(pItem->tag&0xFF);
			tempBufLen += 1;
		}
		else
		{
			tempBuf[tempBufLen]=(byte)pItem->tag;
			tempBufLen += 1;
		}
		
		if(pItem->src == SRC_REQ)
		{
			if(IsRequestItemExisted(pItem->index))
			{
				pData =GetRequestItemValue(pItem->index);
			}
		}
		else
		{
			if(IsResponseItemExisted(pItem->index))
			{
				pData = GetResponseItemValue(pItem->index);
			}
		}

		if(pData)
		{
			if(pData->len%2 != 0)
			{
				//Changed by Siken 2011.09.25
				//Removed:
				//memcpy(tagValue, pData->ptr,  pData->len);
				//tagValue[pData->len] ='F';

				//Added:
				tagValue[0] ='0';
				memcpy(&tagValue[1], pData->ptr,  pData->len);
				//End
				
				tagValueLen = pData->len +1;

				HexStr2Bin(tempValueBuf,sizeof(tempValueBuf), tagValue, tagValueLen);
				tempValueLen =(uint8)tagValueLen/2;
			}
			else
			{
				HexStr2Bin(tempValueBuf,sizeof(tempValueBuf), pData->ptr, pData->len);
				tempValueLen =(uint8)pData->len/2;
			}
			
			tempBuf[tempBufLen]=(uint8)tempValueLen;
			tempBufLen += 1;
			
			memcpy(tempBuf + tempBufLen, tempValueBuf, tempValueLen);
			tempBufLen += tempValueLen;
		}
		else
		{
			tempBuf[tempBufLen] = 0;
			tempBufLen +=1;
		}
	}

	buf[0] = 0x02;
	buf[1] = 0x70;

	if(tempBufLen >255)
	{
		buf[2] = 0x82;
		buf[3] = tempBufLen>>8;
		buf[4] = tempValueLen&0xFF;

		memcpy(buf + 5, tempBuf, tempBufLen);
		*bufLen = 5 + tempBufLen;
	}
	else if(tempBufLen >= 0x80)
	{
		buf[2] = 0x81;
		buf[3] = tempBufLen;
		memcpy(buf + 4, tempBuf, tempBufLen);
		*bufLen = 4 + tempBufLen;
	}
	else
	{
		buf[2] = tempBufLen;
		memcpy(buf + 3, tempBuf, tempBufLen);
		*bufLen = 3 + tempBufLen;
	}

	return STATUS_OK;
}
#endif

int32 GetQpbocAuthRequest(byte *buf, uint16 *bufLen)
{
	tti_uint16 tagTLVLen;
	tti_uint16 tagList[] = {
#ifndef VPOS_APP        
							TAG_TRACK2,
							TAG_PAN,
							TAG_AMOUNT,
							TAG_CARDHOLDER_NAME, 
							TAG_APP_EXPIRATION_DATE, 
							TAG_PAN_SEQU_NUMBER, 
							TAG_AIP,
							TAG_TRANSACTION_DATE,
							TAG_TRANSACTION_TYPE,
							TAG_ISSUER_APP_DATA,
							TAG_TERM_COUNTRY_CODE,
							TAG_CRYPTOGRAM,
							TAG_CRYPTOGRAM_INFO_DATA,
							TAG_ATC,
							TAG_UNPREDICTABLE_NUMBER,
							TAG_QPBOC_OFFLINE_BALANCE,
							TAG_POS_ENTRY_MODE,
							TAG_QPBOC_PRODUCT_INFO,
							TAG_ONLINE_PIN_BLOCK_BCTC,
						//Jason added on 20200512	
							TAG_QPBOC_USER_SPEC_DATA,
							TAG_TOKEN_REQUESTOR_ID,
							TAG_QPBOC_9F25,
							TAG_PAYMENT_ACCOUNT_REF,
							TAG_AMOUNT_OTHER,
							TAG_TERMINAL_CAPABILITIES,
							TAG_TRANS_CURRENCY_CODE,
							TAG_TVR,
						//#if PROJECT_CY21 == 1
							TAG_MERCHANT_LOCATION,
						//#endif
						//End	
            #else
                            0x9F26,
							0x9F27,
							0x9F10,
							0x9F37,
							0x9F36,
							0x95,
							0x9A,
							0x9C,
							0x9F02,
							0x5F2A,
							0x82,
							0x9F1A,
							0x9F03,
							0x9F33,
							//0x9F74,
                            //0x8A,
							0x9F34,
							0x9F35,
							0x9F1E,
							0x84,
							0x9F09,
							0x9F41
							/*0x91,
							0x71,
							0x72,
							0xDF31,
							0x9F63*/
            #endif
							};
	//tti_byte track2Data[100];
	tti_tchar track2Str[256];
	tti_tchar panStr[50];
	tti_tchar panData[100];
	tti_int32 panDataLen;
	tti_int32 loop;
	
	if (TagIsExisted(&(runningTimeEnv.tagList), TAG_PAN) == FALSE)
	{
		memset(panStr, 0x00, sizeof(panStr));
		HextoA(track2Str, GetTagValue(&(runningTimeEnv.tagList), TAG_TRACK2), 
			GetTagValueSize(&(runningTimeEnv.tagList), TAG_TRACK2));
		for (loop = 0; loop < 19; loop ++)
		{
			if (track2Str[loop] == 'D')
			{
				LOGD("Meet 'D' in track 2 data\n");
				break; 
			}
			panStr[loop] = track2Str[loop];
		}
        /*
		if (strlen(panStr) % 2 != 0)
		{
            
			AtoHex(panData, panStr, strlen(panStr) + 1);
			panDataLen = (strlen(panStr) + 1)/2;
		}else
		{
			AtoHex(panData, panStr, strlen(panStr));
			panDataLen = strlen(panStr)/2;
		}
		*/
        if (strlen(panStr) % 2 != 0)
        {
            strcat(panStr, "F");
        }
        AtoHex(panData, panStr, strlen(panStr));
        panDataLen = strlen(panStr)/2;
        
		SetTagValue(TAG_PAN, panData, panDataLen, &(runningTimeEnv.tagList));
	}

	TagArrsBuildTLV(&(runningTimeEnv.tagList), tagList, sizeof(tagList)/sizeof(tagList[0]), buf + 4, &tagTLVLen);

	buf[0] = STX;
    buf[1] = 0x42;
    buf[2] = tagTLVLen/256;
    buf[3] = tagTLVLen%256;

	*bufLen = tagTLVLen + 4;

	return STATUS_OK;
}

int32 ParseqPbocAuthHostData(byte buf[], uint16 size)
{
	uint16 dataLen;
	TagList tmpList;
	int32 ret;
	
	if (size < 4)
	{
		return STATUS_FAIL;
	}

	if (buf[0] != STX || buf[1] != 0x02)
	{
		return STATUS_FAIL;
	}

	dataLen = buf[2] * 256 + buf[3];
	if ((dataLen + 4) != size)
	{
		return STATUS_FAIL;
	}

	InitTagList(&tmpList);

	ret = BuildTagListOneLevel(buf + 4, size - 4, &tmpList);
	if (ret != STATUS_OK)
	{
		FreeTagList(&tmpList);
		LOGE("BuildTagListOneLevel ERR");
		return STATUS_FAIL;
	}
	PrintOutTagList(&tmpList, "Parse Host Data");

    if (TagIsExisted(&tmpList, TAG_RESPONSE_CODE) == FALSE || 
        TagIsDuplicate(&tmpList, TAG_RESPONSE_CODE) == TRUE ||
        GetTagValueSize(&tmpList, TAG_RESPONSE_CODE) != 2)
    {
        //如果没有TAG_RESPONSE_CODE, 但有TAG_ISSUER_AUTH_DATA且长度为10，则TAG_ISSUER_AUTH_DATA后两位为响应码
         if (TagIsExisted(&tmpList, TAG_ISSUER_AUTH_DATA) && GetTagValueSize(&tmpList, TAG_ISSUER_AUTH_DATA) ==10)
        {
            tti_byte *p;
            p=GetTagValue(&tmpList, TAG_ISSUER_AUTH_DATA);
            SetTagValue(TAG_RESPONSE_CODE, p+8, 2, &tmpList);
        }else{
            FreeTagList(&tmpList);
            LOGE("TAG RESPONSE CODE NOT EXIST OR Duplicate or size is not 2");
            return STATUS_FAIL;
        }
    }

	if (memcmp(GetTagValue(&tmpList, TAG_RESPONSE_CODE), "00", 2) != 0)
	{
		FreeTagList(&tmpList);
		LOGE("TAG_RESPONSE_CODE is not '00'");
		return STATUS_FAIL;
	}
	
	FreeTagList(&tmpList);
	return STATUS_OK;
}


int32 qpbocAuthCommWithHost()
{
	byte SendBuf[MAX_COMM_HOST_SEND_BUF_SIZE];
	byte RecvBuf[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 SendBufLen = sizeof(SendBuf);
	uint16 RecvBufLen = sizeof(RecvBuf);

	if(GetQpbocAuthRequest(SendBuf, &SendBufLen) != STATUS_OK)
	{
        LOGE("Get Auth Request Data Fail.");
		return STATUS_FAIL;
	}
	LOGD("qpboc GetAuthRequest SendBuf ");
	hexdump(SendBuf, SendBufLen);

#ifndef VPOS_APP
	if(SendAndReceive(SendBuf, SendBufLen, RecvBuf, &RecvBufLen) != STATUS_OK)
#else	
	if(iAppSendAndRecive(SendBuf+4, SendBufLen-4, RecvBuf, &RecvBufLen))
#endif        
	{
        LOGE("Send And Recvive Fail.");
		return STATUS_FAIL;
	}
	
	LOGD("ParseHostData RecvBuf");
	hexdump(RecvBuf, RecvBufLen);
    
    if(RecvBufLen==4 && (gl_TransRec.uiTransType&0x00FF)!=0x00)      //撤销,查询登记交易可以不返回55域
        return STATUS_OK;

    if(ParseqPbocAuthHostData(RecvBuf, RecvBufLen) != STATUS_OK)
    {
        LOGE("Parse Host Data Fail.");

        return STATUS_FAIL;
    }
    
	return STATUS_OK;
}



//------------------------------------------------------------------------------------------
int32 SendAndReceive(const byte * SendBuf,uint16 SendBufLen,
						     byte * RecvBuf, uint16 *pRecvBufLen)
{
/*
	vec_t *pData;
	uchar IPBuf[20];
	uchar PortBuf[10];
	
	assert(IsRequestItemExisted(REQ_HOST_IP));
	assert(IsRequestItemExisted(REQ_HOST_PORT));
	
	pData = GetRequestItemValue(REQ_HOST_IP);
	sprintf(IPBuf, "%s", pData->ptr);
	
	pData = GetRequestItemValue(REQ_HOST_PORT);
	sprintf(PortBuf,"%s", pData->ptr);
*/
	char IPBuf[20];
	char PortBuf[10];
	int32 ret;
	int32 loop = 0;

	GetServerIPAndPort(IPBuf, PortBuf);

	while(loop < 2)
	{
		ret = SocketSendRecv((byte *)IPBuf, (byte *)PortBuf, SendBuf, SendBufLen, RecvBuf, pRecvBufLen);
		if (ret == STATUS_OK)
		{
			return ret;
		}else
		{
			loop ++;
		}
	}

	return ret;
}

//从runningTimeEnv取卡号
int iGetEnvCardId(unsigned char *pszCardId, unsigned char *pucCardSeqNo, unsigned char *pszTrack2)
{
	char szBuf[100];
	char *p;

	pszCardId[0]=0;
	if(pucCardSeqNo)
		*pucCardSeqNo=0xFF;
	if(TagIsExisted(&(runningTimeEnv.tagList), 0x5A))
	{
		vOneTwo0(GetTagValue(&(runningTimeEnv.tagList), 0x5A), GetTagValueSize(&(runningTimeEnv.tagList), 0x5A), (uchar*)szBuf);
        {
            hexdumpEx("5A", GetTagValue(&(runningTimeEnv.tagList), 0x5A), GetTagValueSize(&(runningTimeEnv.tagList), 0x5A));
            dbg("tag5A len=%d\n", GetTagValueSize(&(runningTimeEnv.tagList), 0x5A));
        }
        
		rtrimF(szBuf);
		strcpy((char*)pszCardId, szBuf);
        dbg("get pan from 0x5A:%s\n", pszCardId);
	}
	
	if((pszTrack2 || pszCardId[0]==0) && TagIsExisted(&(runningTimeEnv.tagList), 0x57))
	{
		vOneTwo0(GetTagValue(&(runningTimeEnv.tagList), 0x57), GetTagValueSize(&(runningTimeEnv.tagList), 0x57), (uchar*)szBuf);

            hexdumpEx("57", GetTagValue(&(runningTimeEnv.tagList), 0x57), GetTagValueSize(&(runningTimeEnv.tagList), 0x57));
            dbg("tag57 len=%d\n", GetTagValueSize(&(runningTimeEnv.tagList), 0x57));



		if(pszCardId[0]==0)
		{
			p=strchr(szBuf, 'D');
			if(p)
			{
				vMemcpy0(pszCardId, (uchar*)szBuf, p-szBuf);
                dbg("get pan from 0x57:%s\n", pszCardId);
			}
		}
		if(pszTrack2)
		{
			rtrimF(szBuf);
			strcpy((char*)pszTrack2, szBuf);
		}
	}
	if(pszCardId[0] && pucCardSeqNo && TagIsExisted(&(runningTimeEnv.tagList), 0x5F34))
	{
		memcpy(pucCardSeqNo, GetTagValue(&(runningTimeEnv.tagList), 0x5F34), 1);
	}
	if(pszCardId[0])
		return 0;

	return 1;
}

int iGetEnvAmount(ulong *pulAmount)
{
    char szBuf[20];
    
	*pulAmount=0L;
	if(TagIsExisted(&(runningTimeEnv.tagList), 0x9F02))
	{
		vOneTwo0(GetTagValue(&(runningTimeEnv.tagList), 0x9F02), GetTagValueSize(&(runningTimeEnv.tagList), 0x9F02), szBuf);
		*pulAmount=atol(szBuf);
		return 0;
	}

	return 1;
}

int iGetEnvDateTime(char *pszDateTime)
{
	pszDateTime[0]=0;
	if(TagIsExisted(&(runningTimeEnv.tagList), 0x9A) && TagIsExisted(&(runningTimeEnv.tagList), 0x9F21))
	{
		strcpy(pszDateTime, "20");
		vOneTwo0(GetTagValue(&(runningTimeEnv.tagList), 0x9A), GetTagValueSize(&(runningTimeEnv.tagList), 0x9A), pszDateTime+2);
		vOneTwo0(GetTagValue(&(runningTimeEnv.tagList), 0x9F21), GetTagValueSize(&(runningTimeEnv.tagList), 0x9F21), pszDateTime+8);

        /*
        tag=0x9A;
		translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
			GetTagValueSize(&runningTimeEnv.tagList, tag), buf);
		
		//time
		tag=0x9F21;
		translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
			GetTagValueSize(&runningTimeEnv.tagList, tag), buf+6);
        */
        
		return 0;
	}
	return 1;
}

//0:国内借记卡 1:国内贷记卡 2:国内准贷记卡3:纯电子现金卡
int iGetEnvIcCardClass(void)
{
    char szAid[32+1]={0};
    int ret=0;
    unsigned short tag;
    
    //判断卡类
    if (TagIsExisted(&runningTimeEnv.tagList, 0x84) == TRUE)
    {
	tag=0x84;
        translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, tag),
					GetTagValueSize(&runningTimeEnv.tagList, tag), szAid);
        
        if(strstr(szAid, "A000000333"))    //银联AID
        {
            //A000000333010101:借记应用
            //A000000333010102:贷记应用
            //A000000333010103:准贷记应用
            //A000000333010106:纯电子现金卡
             if (strcmp(szAid+strlen(szAid)-9, "333010101")==0)
                ret = 0;
            else if (strcmp(szAid+strlen(szAid)-9, "333010102")==0)
                ret = 1;
	   else  if(strcmp(szAid+strlen(szAid)-9, "333010103")==0)
	   	ret = 2;
	   else  if(strcmp(szAid+strlen(szAid)-9, "333010103")==0)
	   	ret = 3;
	   	
        }
    }
    //dbg("aid=[%s], applab=[%s],cardclass:%d\n", szAid, szAppLabel, gl_TransRec.ucCardClass);
    
    return ret;
}


#ifdef APP_LKL
extern void vGenField40(uchar *pszField40);
extern void vSetTransRevFlag(int flag);
extern void vSetDispRetInfo(int flag);
extern int iGetLklTransFeeData(uchar *out);
int iAppSendAndRecive(const byte * SendBuf,uint16 SendBufLen,
						     byte * RecvBuf, uint16 *pRecvBufLen)
{
	uchar szCardId[20+1], ucCardSeqNo;
    uchar szBuf[100];
    uchar szDateTime[14+1], szTrack2[50];
    char *p;
    int iRet;
	ulong ulAmount;
	char szRid[2+1];
    uchar ucFd60MsgCode;
    uchar ucPreAuthType=0;  //预授权类交易

	szTrack2[0]=0;
	iGetEnvCardId(szCardId, &ucCardSeqNo, szTrack2);
	iGetEnvAmount(&ulAmount);
	szDateTime[0]=0;
	iGetEnvDateTime(szDateTime);
    
	vPrtTransRecInfo();

	gl_TransRec.uiEntryMode=gl_TransRec.uiEntryMode/10*10+(strlen(runningTimeEnv.commonRunningData.pinBlock)>=16?1:2);
    dbg("uiEntryMode=%d\n", gl_TransRec.uiEntryMode);

    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
    memset(&Send8583,0,sizeof(gl_Send8583));
	
    switch(gl_TransRec.uiTransType)
    {
        case TRANS_TYPE_SALE:       //消费
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "000000");
            ucFd60MsgCode=22;
            break;
        case TRANS_TYPE_SALEVOID:   //消费撤销
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=23;
            break;
        case TRANS_TYPE_REFUND:     //退货
            strcpy((char *)gl_Send8583.Msg01, "0220");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=25;
            break;
        case TRANS_TYPE_BALANCE:    //查余额
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "300000");
            ucFd60MsgCode=0x01;
            break;
        case TRANS_TYPE_PREAUTH:    //预授权
            strcpy((char *)gl_Send8583.Msg01, "0100");
            strcpy((char *)gl_Send8583.Field03, "030000");
            ucFd60MsgCode=10;
            ucPreAuthType=1;
            break;
        case TRANS_TYPE_PREAUTHVOID:    //预授权撤销
            strcpy((char *)gl_Send8583.Msg01, "0100");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=11;
            ucPreAuthType=1;        
            break;
        case TRANS_TYPE_PREAUTH_COMP:    //预授权完成
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "000000");
            ucFd60MsgCode=20;
            ucPreAuthType=1;
            break;
        case TRANS_TYPE_PREAUTH_COMPVOID:    //预授权完成撤销
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=21;
            ucPreAuthType=1;
            break;
    }
    
    strcpy((char *)gl_Send8583.Field02, szCardId);
    if(gl_TransRec.uiTransType!=TRANS_TYPE_BALANCE)
    	sprintf((char *)gl_Send8583.Field04, "%012lu", ulAmount);
    strcpy((char *)gl_Send8583.Field07, (char *)szDateTime + 4);
    //vIncTTC();
    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
    
    if(TagIsExisted(&(runningTimeEnv.tagList), 0x5F24))
    {
        vOneTwo0(GetTagValue(&(runningTimeEnv.tagList), 0x5F24), GetTagValueSize(&(runningTimeEnv.tagList), 0x5F24), szBuf);
        memcpy((char *)gl_Send8583.Field14, szBuf, 4);
    }
	else
    {
        p=strchr((char*)szTrack2, 'D');
        if(p)
            memcpy((char *)gl_Send8583.Field14, p+1, 4);
    }
    
    sprintf((char *)gl_Send8583.Field22, "%03d", gl_TransRec.uiEntryMode);
	if(ucCardSeqNo!=0xFF)
		sprintf((char *)gl_Send8583.Field23, "%03d", ucCardSeqNo);
    if(ucPreAuthType)
        strcpy((char *)gl_Send8583.Field25, "06");
    else
        strcpy((char *)gl_Send8583.Field25, "00");
    if(szTrack2[0])
        strcpy((char *)gl_Send8583.Field35, (char*)szTrack2);
    /*
    //消费撤销/预授权完成撤销/退货,37域上送原交易参考号
    if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID
		|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
    {
        memcpy(gl_Send8583.Field37, gl_TransRec.sReferenceNo, 12);
    }
    */
    //预授权完成或撤销类交易,需上送原交易授权码
    if((gl_Send8583.Field03[0]=='2' || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP) 
        && gl_TransRec.sAuthCode[0])
    {
        memcpy(gl_Send8583.Field38, gl_TransRec.sAuthCode, 6);
    }
    gl_TransRec.sReferenceNo[0]=0;
    gl_TransRec.sAuthCode[0]=0;
    gl_TransRec.ucUploadFlag=0x00;
    
    //40域(消费，预授权)
    if( gl_TransRec.uiTransType==TRANS_TYPE_SALE || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH)
    {
        vGenField40(gl_Send8583.Field40);
    }
    
    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
    if( gl_TransRec.uiTransType==TRANS_TYPE_SALE)
        iGetLklTransFeeData(gl_Send8583.Field48);
    strcpy((char *)gl_Send8583.Field49, "156");
    
    
    if(strlen(runningTimeEnv.commonRunningData.pinBlock)==16)
    {
        //strcpy((char*)gl_Send8583.Field26, "12");
        
        memcpy(gl_Send8583.Field52, "\x00\x08", 2);
        vTwoOne((uchar*)runningTimeEnv.commonRunningData.pinBlock, 16, gl_Send8583.Field52+2);

        memset(gl_Send8583.Field53, '0', 16);
        memcpy(gl_Send8583.Field53, "26", 2);			//双倍长密钥
		//memcpy(gl_Send8583.Field53, "20", 2);			//单倍长密钥
    }
	gl_Send8583.Field55[0]=SendBufLen/256;
	gl_Send8583.Field55[1]=SendBufLen%256;
	memcpy(gl_Send8583.Field55+2, SendBuf, SendBufLen);

    vGenField57(gl_Send8583.Field57);

	/* if(memcmp(gl_Send8583.Field22, "02", 2)==0)	//02x 正常的磁条卡交易
	{
		szRid[0]=0;
	}else */{
		if(gl_Send8583.Field22[0]=='8')		//80x fallback交易
			strcpy(szRid, "52");
		else
			strcpy(szRid, "50");
	}
    sprintf((char *)gl_Send8583.Field60+2, "%.2s%.4s%.2s", "01", "0000", szRid);
    vFillFieldLen(gl_Send8583.Field60);
    
    if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
    {
        sprintf((char *)gl_Send8583.Field62, "%06lu%06lu%06lu", gl_SysData.ulBatchNo, gl_SysData.ulTTC, gl_TransRec.ulVoidTTC);
    }else if(TRANS_TYPE_REFUND==gl_TransRec.uiTransType)
    {
        //退货,原交易日期MMDD
        vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field62, "%06lu%06lu%06lu%.4s", gl_SysData.ulBatchNo, gl_SysData.ulTTC, 0L, szBuf);
    }else{
        sprintf((char *)gl_Send8583.Field62, "%06lu%06lu", gl_SysData.ulBatchNo, gl_SysData.ulTTC);
    }
    //if(gl_Send8583.Field62[2])
    //    vFillFieldLen(gl_Send8583.Field62);
    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

    vSetTransRevFlag(0); 
	//打8583包-填充http-发送-接收-解http-解8583包
	if(TRANS_TYPE_BALANCE==gl_TransRec.uiTransType || TRANS_TYPE_REFUND==gl_TransRec.uiTransType)
		iRet = iHttpSendRecv8583(0);
	else
    	iRet = iHttpSendRecv8583(1);
    if (iRet)
    {
        //显示错误信息
        vShowCommErrMsg(iRet);
        
        vSetDispRetInfo(0);
        return iRet;
    }

    if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
    {
        //显示服务端错误信息
        TraceHex("DE63", gl_Recv8583.Field63+2,ulStrToLong(gl_Recv8583.Field63,2);
        vShowHostErrMsg(gl_Recv8583.Field39,gl_Recv8583.Field56+2);
        vSetDispRetInfo(0);
        return 1;
    }
#if 0
    //先预设需即时冲正(插卡交易提前拔卡或校验失败)
    if(gl_TransRec.uiEntryMode/10==5 && gl_TransRec.uiTransType!=TRANS_TYPE_BALANCE && gl_TransRec.uiTransType!=TRANS_TYPE_REFUND)
        vSetTransRevFlag(1);       
#endif    
    //撤销类交易重置部分值为初始值
    gl_TransRec.ulBalance=0L;
    gl_TransRec.uiIcDataLen=0;
    gl_TransRec.ucTransAttr=0;
    gl_TransRec.ucUploadFlag=0xFF;
    gl_TransRec.ucSignupload=0x00;
    
    if(strlen(szDateTime)!=14)
		_vGetTime(szDateTime);
	if(gl_Recv8583.Field13[0]&&gl_Recv8583.Field12[0])  //用后台返回时间做交易记录时间
    {
        memcpy(szDateTime+4, gl_Recv8583.Field13, 4);
        memcpy(szDateTime+8, gl_Recv8583.Field12, 6);
    }
    vTwoOne(szDateTime+2, 12, gl_TransRec.sDateTime);

    if(gl_Recv8583.Field15[0])
        vTwoOne(gl_Recv8583.Field15, 4, gl_TransRec.sSettleDate);
    else
        memcpy(gl_TransRec.sSettleDate, "\x00\x00", 2);

	iRet=gl_Recv8583.Field55[0]*256+gl_Recv8583.Field55[1];	
    {
        //添加8A
        int i;
        for(i=2; i<2+iRet-4; i++)
        {
            if(memcmp(gl_Recv8583.Field55+i, "\x8A\x02\x30\x30", 4)==0)
                break;
        }
        if(memcmp(gl_Recv8583.Field55+i, "\x8A\x02\x30\x30", 4)!=0)
        {
            memcpy(gl_Recv8583.Field55+2+iRet, "\x8A\x02\x30\x30", 4);
            iRet+=4;
            gl_Recv8583.Field55[0]=iRet/256;
            gl_Recv8583.Field55[1]=iRet%256;
        }
    }
    RecvBuf[0]=STX;
    RecvBuf[1]=0x02;
	memcpy(RecvBuf+2, gl_Recv8583.Field55, iRet+2);
	*pRecvBufLen=iRet+4;

	if(TRANS_TYPE_BALANCE==gl_TransRec.uiTransType)
	{
		if(strlen(gl_Recv8583.Field54)>=26)
		{
			ulong ulBal;

			ulBal=ulA2L(gl_Recv8583.Field54+14, 12);

			_vCls();
			vDispCenter(1, "余额查询", 1);
			_vDisp(2, "查询成功");
			vDispVarArg(3, "余额:%s%lu.%02lu", (gl_Recv8583.Field54[13]=='C')?"":"-", ulBal/100, ulBal%100);
			vMessage("");
            
            vSetDispRetInfo(0);
		}
	}else
	{		
		//vGetPrtMerchInfo(gl_Recv8583.Field40, &gl_TransRec);
        //48域 广告信息
	}
	return 0;
}
#endif
#if 1//def APP_SDJ
extern void vSetDispRetInfo(int flag);
extern void vGenFiled63CellInfo(uchar *pszFd63);
int iAppSendAndRecive(const byte * SendBuf,uint16 SendBufLen,
						     byte * RecvBuf, uint16 *pRecvBufLen)
{
	uchar szCardId[20+1], ucCardSeqNo;
    uchar szBuf[100];
    uchar szDateTime[14+1], szTrack2[50];
    char *p;
    int iRet;
	ulong ulAmount;
	char szRid[2+1];
    uchar ucFd60MsgCode;
    uchar ucPreAuthType=0;  //预授权类交易
st8583			Send8583;

	szTrack2[0]=0;
	iGetEnvCardId(szCardId, &ucCardSeqNo, szTrack2);
	dbg("gl_ucStandbyCardFlag:%d\n",gl_ucStandbyCardFlag);
	dbg("gl_TransRec.ulAmount:%d\n",gl_TransRec.ulAmount);
	if (gl_ucStandbyCardFlag == 1  ||gl_ucStandbyCardFlag == 2) 
		ulAmount = gl_TransRec.ulAmount;
	else
		iGetEnvAmount(&ulAmount);
	szDateTime[0]=0;
	iGetEnvDateTime(szDateTime);
    
	vPrtTransRecInfo();

	gl_TransRec.uiEntryMode=gl_TransRec.uiEntryMode/10*10+(strlen(runningTimeEnv.commonRunningData.pinBlock)>=16?1:2);
    dbg("uiEntryMode=%d\n", gl_TransRec.uiEntryMode);

    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
    
    switch(gl_TransRec.uiTransType)
    {
        case TRANS_TYPE_SALE:       //消费
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "000000");
            ucFd60MsgCode=22;
            break;
	case TRANS_TYPE_DAIRY_SALE:       //日结消费
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "280000");
	    gl_TransRec.ucSaleType = 1;		
            ucFd60MsgCode=22;
            break;		
        case TRANS_TYPE_SALEVOID:   //消费撤销
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=23;
            break;
        case TRANS_TYPE_REFUND:     //退货
            strcpy((char *)gl_Send8583.Msg01, "0220");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=25;
            break;
        case TRANS_TYPE_BALANCE:    //查余额
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "310000");
            ucFd60MsgCode=0x01;

			
			//strcpy((char *)gl_Send8583.Field26, "12");
			//strcpy((char *)gl_Send8583.Field35, (char*)szTrack2);
            break;
        case TRANS_TYPE_PREAUTH:    //预授权
            strcpy((char *)gl_Send8583.Msg01, "0100");
            strcpy((char *)gl_Send8583.Field03, "030000");
            ucFd60MsgCode=10;
            ucPreAuthType=1;
            break;
        case TRANS_TYPE_PREAUTHVOID:    //预授权撤销
            strcpy((char *)gl_Send8583.Msg01, "0100");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=11;
            ucPreAuthType=1;        
            break;
        case TRANS_TYPE_PREAUTH_COMP:    //预授权完成
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "000000");
            ucFd60MsgCode=20;
            ucPreAuthType=1;
            break;
        case TRANS_TYPE_PREAUTH_COMPVOID:    //预授权完成撤销
            strcpy((char *)gl_Send8583.Msg01, "0200");
            strcpy((char *)gl_Send8583.Field03, "200000");
            ucFd60MsgCode=21;
            ucPreAuthType=1;
            break;
    }
    
    strcpy((char *)gl_Send8583.Field02, szCardId);
    if(gl_TransRec.uiTransType!=TRANS_TYPE_BALANCE)
    	sprintf((char *)gl_Send8583.Field04, "%012lu", ulAmount);

//    strcpy((char *)gl_Send8583.Field07, (char *)szDateTime + 4);
    //vIncTTC();
    sprintf((char *)gl_Send8583.Field11, "%06lu", gl_SysData.ulTTC);
    
    if(TagIsExisted(&(runningTimeEnv.tagList), 0x5F24))
    {
        vOneTwo0(GetTagValue(&(runningTimeEnv.tagList), 0x5F24), GetTagValueSize(&(runningTimeEnv.tagList), 0x5F24), szBuf);
        memcpy((char *)gl_Send8583.Field14, szBuf, 4);
    }else
    {
        p=strchr((char*)szTrack2, 'D');
        if(p)
            memcpy((char *)gl_Send8583.Field14, p+1, 4);
    }
    
    sprintf((char *)gl_Send8583.Field22, "%03d", gl_TransRec.uiEntryMode);
	if(ucCardSeqNo!=0xFF)
		sprintf((char *)gl_Send8583.Field23, "%03d", ucCardSeqNo);
	
    strcpy((char *)gl_Send8583.Field24, "004");	 

    strcpy((char *)gl_Send8583.Field25, "14");
	
    if(szTrack2[0]){
		 if(gl_SysInfo.magEncrypt == 1) 
		{
				AjustTrack2Data(szTrack2);	
				EncryptMagData(szTrack2, strlen(szTrack2), gl_Send8583.Field35);
		 }
		 else if(gl_SysInfo.magEncrypt ==0)
		 	strcpy(gl_Send8583.Field35,szTrack2);
	}
    //strcpy((char *)gl_Send8583.Field35, (char*)szTrack2);


	
    //消费撤销/预授权完成撤销/退货,37域上送原交易参考号
    if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID
		|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
    {
        memcpy(gl_Send8583.Field37, gl_TransRec.sReferenceNo, 12);
    }
    
    //预授权完成或撤销类交易,需上送原交易授权码
    if((gl_Send8583.Field03[0]=='2' || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP) 
        && gl_TransRec.sAuthCode[0])
    {
        memcpy(gl_Send8583.Field38, gl_TransRec.sAuthCode, 6);
    }
    gl_TransRec.sReferenceNo[0]=0;
    gl_TransRec.sAuthCode[0]=0;
    gl_TransRec.ucUploadFlag=0x00;
    
    strcpy((char *)gl_Send8583.Field41, (char *)gl_SysInfo.szPosId);
    strcpy((char *)gl_Send8583.Field42, (char *)gl_SysInfo.szMerchId);
    strcpy((char *)gl_Send8583.Field49, "156");

     if(strlen(runningTimeEnv.commonRunningData.pinBlock) >= 16)
    {
//	    	strcpy((char*)gl_Send8583.Field26, "12");

		if(gl_SysInfo.ucSmFlag == 0)	
	       	{
	       		memcpy(gl_Send8583.Field52, "\x00\x08", 2);
	        	vTwoOne((uchar*)runningTimeEnv.commonRunningData.pinBlock, 16, gl_Send8583.Field52+2);
		}else if(gl_SysInfo.ucSmFlag == 1)
		{
			memcpy(gl_Send8583.Field52, "\x00\x08", 2);
			memset(gl_Send8583.Field52+2, 0x00, 8);         //国密算法,pin密文放在59域,52域填全0
		}
		
	 	memset(gl_Send8583.Field53, '0', 16);
		if(gl_SysInfo.ucSmFlag == 0)		
	        	memcpy(gl_Send8583.Field53, "26", 2);			//双倍长密钥
	        else if(gl_SysInfo.ucSmFlag == 1)
		{
			memcpy(gl_Send8583.Field53, "24", 2);			//双倍长密钥
	        }	
	        	if(gl_SysInfo.magEncrypt == 1)	
        			memcpy(&gl_Send8583.Field53[2], "1", 1);		//磁道加密
       			else if(gl_SysInfo.magEncrypt == 0)	
				memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密
			//memcpy(gl_Send8583.Field53, "20", 2);			//单倍长密钥	
    	}else
    	    {		
		memset(gl_Send8583.Field53, '0', 16);
		if(gl_SysInfo.ucSmFlag == 0)	
			memcpy(gl_Send8583.Field53, "06", 2);			//双倍长密钥
		else if(gl_SysInfo.ucSmFlag == 1)
			memcpy(gl_Send8583.Field53, "04", 2);			//双倍长密钥	
		if(gl_SysInfo.magEncrypt == 1)	
			memcpy(&gl_Send8583.Field53[2], "1", 1);		//磁道加密
		else if(gl_SysInfo.magEncrypt == 0)	
			memcpy(&gl_Send8583.Field53[2], "0", 1);		//磁道加密			
    }

     if(gl_TransRec.uiTransType ==TRANS_TYPE_SALE ||  gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH)	 
    {
    	gl_Send8583.Field55[0]=SendBufLen/256;
	gl_Send8583.Field55[1]=SendBufLen%256;
	memcpy(gl_Send8583.Field55+2, SendBuf, SendBufLen);
     }	

	
//DE59 21号文(消费，预授权)    
    if( gl_TransRec.uiTransType==TRANS_TYPE_SALE || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH || gl_TransRec.uiTransType==TRANS_TYPE_BALANCE)
    { 
	GetPublicField059((char*)runningTimeEnv.commonRunningData.pinBlock,szCardId+strlen(szCardId)-6, gl_Send8583.Field59);	
    }


//DE60
	if(memcmp(gl_Send8583.Field22, "02", 2)==0)	//02x 正常的磁条卡交易
	{
		szRid[0]=0;
	}else {
		if(gl_Send8583.Field22[0]=='8')		//80x fallback交易
			strcpy(szRid, "52");
		else
			strcpy(szRid, "50");
	}
	
   if(gl_SysInfo.ucSmFlag == 0)		
   	sprintf((char *)gl_Send8583.Field61, "%06lu%03s%06lu", gl_SysData.ulBatchNo, gl_SysData.szCurOper , gl_SysData.ulTTC);
   else
   	sprintf((char *)gl_Send8583.Field61, "%06lu%03s%06lu", gl_SysData.ulBatchNo, "SM4" , gl_SysData.ulTTC);
   
    if(gl_TransRec.uiTransType==TRANS_TYPE_SALEVOID)
    {
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
    }
    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTHVOID || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMP
		|| gl_TransRec.uiTransType==TRANS_TYPE_REFUND)
    {
        //sprintf((char *)gl_Send8583.Field61, "%06lu%06lu0000", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC);
        vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", 0L, 0L, szBuf);
    }
    if(gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH_COMPVOID)
    {
		vOneTwo0(gl_TransRec.sDateTime+1, 2, szBuf);
        sprintf((char *)gl_Send8583.Field61, "%06lu%06lu%.4s", gl_SysData.ulBatchNo, gl_TransRec.ulVoidTTC, szBuf);
    }

    memcpy((char *)gl_Send8583.Field64, "\x00\x08", 2);

if(gl_TransRec.uiTransType !=TRANS_TYPE_BALANCE)	
{
	gl_TransRec.ucUploadFlag=0xFF;
    gl_TransRec.ucSignupload=0x00;
       Send8583 = gl_Send8583;

#if 1 
    vClearLines(2);
	//签字
	{
	byte buf[100];
        extern int iBillPreSign(int flag, void *pRec);
	
        //1. 电子现金消费(脱机)，无需签名
        //2. pboc返回无需签名且强制签名为0，无需签名
        //3. qpboc联机交易满足小额免签免密且强制签名为0，无需签名
	         if(gl_SysInfo.ucSupportESign == 1)
	       	{
	       		if(gl_TransRec.ucSignupload!=0xFF)
	           	{
	           		vTwoOne(szDateTime+2, 12, gl_TransRec.sDateTime);
        			gl_TransRec.ulTTC=gl_SysData.ulTTC;
				translateBcdArrayToString(GetTagValue(&runningTimeEnv.tagList, TAG_PAN),
				GetTagValueSize(&runningTimeEnv.tagList, TAG_PAN), buf);
				strcat(buf, "FFFFFFFFFF");
				vTwoOne(buf, 20, gl_TransRec.sPan);	
	           		iBillPreSign(1, &gl_TransRec);
	       		}		
	         }
	}
#endif

    memset(&gl_Send8583, 0, sizeof(gl_Send8583));
    memset(&gl_Recv8583, 0, sizeof(gl_Recv8583));
   gl_Send8583 = Send8583;
}

    _vCls();
    if(gl_TransRec.uiTransType ==TRANS_TYPE_SALE)
   {
   	if(gl_TransRec.ucMobileFlag == 1)
		vDispCenter(1, "手机pay", 1);
	else
	 	vDispCenter(1, "消费", 1);	
    }	
   else if(gl_TransRec.uiTransType ==TRANS_TYPE_BALANCE)		
    	 vDispCenter(1, "查余额", 1);
   
	vSetTransRevFlag(0); 
	gl_TransRec.ucUploadFlag=0x00;
	//打8583包-填充http-发送-接收-解http-解8583包
	if(TRANS_TYPE_BALANCE==gl_TransRec.uiTransType ||TRANS_TYPE_REFUND==gl_TransRec.uiTransType)
		iRet = iHttpSendRecv8583(0);
	else
    	iRet = iHttpSendRecv8583(0);

	dbg("iHttpSendRecv8583:%d func:%s line:%d\r\n",iRet,(u8 const*)__FUNCTION__, __LINE__);
    if (iRet)
    {
	//显示错误信息
        vShowCommErrMsg(iRet);
        
        vSetDispRetInfo(0);
        return iRet;
    }
    if (strcmp((char *)gl_Recv8583.Field39, "00") != 0)
    {
        //显示服务端错误信息
		dbgHex("DE63",gl_Recv8583.Field63,10);
        vShowHostErrMsg(gl_Recv8583.Field39, gl_Recv8583.Field56+2);
        vSetDispRetInfo(0);
	if((strcmp((char *)gl_Recv8583.Field39, "55") ==  0 || strcmp((char *)gl_Recv8583.Field39, "61") ==  0)
		    && gl_TransRec.uiEntryMode/10==7 
		    && (gl_TransRec.uiTransType==TRANS_TYPE_SALE  || gl_TransRec.uiTransType==TRANS_TYPE_PREAUTH) 
		    && !gl_ucNFCPinFlag)
	{
		iDownLoadBackBIN(gl_Recv8583.Field13,gl_Recv8583.Field02);
	}
	
        return 1;
    }
     gl_TransRec.ucUploadFlag=0xFF; 	//设为联机交易
     
    //撤销类交易重置部分值为初始值
    gl_TransRec.ulBalance=0L;
    gl_TransRec.uiIcDataLen=0;
    gl_TransRec.ucTransAttr=0;
    gl_TransRec.ucUploadFlag=0xFF;
    gl_TransRec.ucSignupload=0x00;
    
    if(strlen(szDateTime)!=14)
		_vGetTime(szDateTime);
	if(gl_Recv8583.Field13[0]&&gl_Recv8583.Field12[0])  //用后台返回时间做交易记录时间
    {
        memcpy(szDateTime+4, gl_Recv8583.Field13, 4);
        memcpy(szDateTime+8, gl_Recv8583.Field12, 6);
    }
    vTwoOne(szDateTime+2, 12, gl_TransRec.sDateTime);

    if(gl_Recv8583.Field15[0])
        vTwoOne(gl_Recv8583.Field15, 4, gl_TransRec.sSettleDate);
    else
        memcpy(gl_TransRec.sSettleDate, "\x00\x00", 2);

       if(gl_Recv8583.Field14[0] && memcmp(gl_Recv8583.Field14, "0000", 4)!=0)		
   		 vTwoOne(gl_Recv8583.Field14, 4, gl_TransRec.sCardExpire);
	else
	{
      	  	vTwoOne(gl_Send8583.Field14, 4, gl_TransRec.sCardExpire);
	}	
     
   if(gl_Recv8583.Field27[0]) {	
   	 if(gl_Recv8583.Field27[0] ==  '0')  gl_TransRec.ucCardClass = 0;
    	if(gl_Recv8583.Field27[0] ==  '1')  gl_TransRec.ucCardClass = 1;	
   	 dbg("gl_TransRec.ucCardClass:%d\n",gl_TransRec.ucCardClass );
    }
		
     if(gl_Recv8583.BitMap[5] & 0x10) {
		memcpy(gl_TransRec.issuerBankId,gl_Recv8583.Field44,8);
		gl_TransRec.issuerBankId[8]=0;
		if (strlen(gl_Recv8583.Field44) > 11)
		{
			memcpy(gl_TransRec.recvBankId,gl_Recv8583.Field44+11,8);
			gl_TransRec.recvBankId[8]=0;
		}
	}	
	 
	iRet=gl_Recv8583.Field55[0]*256+gl_Recv8583.Field55[1];	
    {
        //添加8A
        int i;
        for(i=2; i<2+iRet-4; i++)
        {
            if(memcmp(gl_Recv8583.Field55+i, "\x8A\x02\x30\x30", 4)==0)
                break;
        }
        if(memcmp(gl_Recv8583.Field55+i, "\x8A\x02\x30\x30", 4)!=0)
        {
            memcpy(gl_Recv8583.Field55+2+iRet, "\x8A\x02\x30\x30", 4);
            iRet+=4;
            gl_Recv8583.Field55[0]=iRet/256;
            gl_Recv8583.Field55[1]=iRet%256;
        }
    }
    RecvBuf[0]=STX;
    RecvBuf[1]=0x02;
	memcpy(RecvBuf+2, gl_Recv8583.Field55, iRet+2);
	*pRecvBufLen=iRet+4;

	if(TRANS_TYPE_BALANCE==gl_TransRec.uiTransType)
	{
		if(strlen(gl_Recv8583.Field54)>=20)
		{
			ulong ulBal;

			ulBal=ulA2L(gl_Recv8583.Field54+8, 12);
                       
		//	_vCls();
		//	vDispCenter(1, "余额查询", 1);
		//	vDispMid(2, "查询成功");
		        vClearLines(2);	
			vDispVarArg(3, "余额:%s%lu.%02lu", (gl_Recv8583.Field54[7]=='C')?"":"-", ulBal/100, ulBal%100);
		       iGetKeyWithTimeout(3);
            vSetDispRetInfo(0);
		}
	}else
	{		
	//	if(gl_Recv8583.Field43[0])
	//	{
	//		vGetPrtMerchInfo(gl_Recv8583.Field43, &gl_TransRec);
	//	}		
	}



	
	return 0;
}
#endif

#ifdef SERVER_BCTC
int32 ParseHostData(byte buf[], uint16 size)
{
	byte tmpBuf[MAX_COMM_HOST_RECV_BUF_SIZE*2];
	byte tmpScript[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 dataLen;
	TagList tmpList;
	int32 ret;
	int32 loop;
	int32 scriptLen;
	int32 tmpLen;
	byte tmpTagLen[BUFFER_SIZE_32BYTE]; 
	char tmpIAD[BUFFER_SIZE_512BYTE];
	
	if (size < 4)
	{
		return STATUS_FAIL;
	}

	if (buf[0] != STX || buf[1] != 0x02)
	{
		return STATUS_FAIL;
	}

	dataLen = buf[2] * 256 + buf[3];
	if ((dataLen + 4) != size)
	{
		return STATUS_FAIL;
	}

	InitTagList(&tmpList);

	ret = BuildTagListOneLevel(buf + 4, size - 4, &tmpList);
	if (ret != STATUS_OK)
	{
		FreeTagList(&tmpList);
		LOGE("BuildTagListOneLevel ERR");
		return STATUS_FAIL;
	}
	PrintOutTagList(&tmpList, "Parse Host Data");

    if (TagIsExisted(&tmpList, TAG_RESPONSE_CODE) == FALSE || 
        TagIsDuplicate(&tmpList, TAG_RESPONSE_CODE) == TRUE ||
        GetTagValueSize(&tmpList, TAG_RESPONSE_CODE) != 2)
    {
        //如果没有TAG_RESPONSE_CODE, 但有TAG_ISSUER_AUTH_DATA且长度为10，则TAG_ISSUER_AUTH_DATA后两位为响应码
         if (TagIsExisted(&tmpList, TAG_ISSUER_AUTH_DATA) && GetTagValueSize(&tmpList, TAG_ISSUER_AUTH_DATA) ==10)
        {
            tti_byte *p;
            p=GetTagValue(&tmpList, TAG_ISSUER_AUTH_DATA);
            SetTagValue(TAG_RESPONSE_CODE, p+8, 2, &tmpList);
        }else{
            FreeTagList(&tmpList);
            LOGE("TAG RESPONSE CODE NOT EXIST OR Duplicate or size is not 2");
            return STATUS_FAIL;
        }
    }
	SetRequestItem(REQ_ARC, GetTagValue(&tmpList, TAG_RESPONSE_CODE), GetTagValueSize(&tmpList, TAG_RESPONSE_CODE));
    
	if (TagIsExisted(&tmpList, TAG_ISSUER_AUTH_DATA) == FALSE || TagIsDuplicate(&tmpList, TAG_ISSUER_AUTH_DATA) == TRUE)
	{
		//FreeTagList(&tmpList);
		LOGE("TAG TAG_ISSUER_AUTH_DATA NOT EXIST OR Duplicate");
		//return STATUS_FAIL;
	}else{
        Bin2HexStr((char *)(GetTagValue(&tmpList, TAG_ISSUER_AUTH_DATA)), GetTagValueSize(&tmpList, TAG_ISSUER_AUTH_DATA), tmpIAD);
        SetRequestItemStr(REQ_ISSUER_AUTH_DATA, tmpIAD);
    }
    
	scriptLen = 0;
	for (loop = 0; loop < tmpList.ItemCount; loop ++)
	{
		if (tmpList.tagItem[loop].Tag == TAG_TEMPERATE_71 || tmpList.tagItem[loop].Tag == TAG_TEMPERATE_72)
		{
			//TAG
			if(tmpList.tagItem[loop].Tag == TAG_TEMPERATE_71)
			{
				memcpy(tmpScript + scriptLen, "71", 2);
			}else
			{
				memcpy(tmpScript + scriptLen, "72", 2);
			}
			scriptLen += 2;

			//Length
			tmpLen = tmpList.tagItem[loop].Length;
			if (tmpLen < 0x80)
			{
				tmpTagLen[0] = tmpLen;
				Bin2HexStr((char *)tmpTagLen, 1, (char *)tmpBuf);
				memcpy(tmpScript + scriptLen, tmpBuf, 2);
				scriptLen += 2;
			}else if (tmpLen < 0x100)
			{
				tmpTagLen[0] = 0x81;
				tmpTagLen[1] = tmpLen;
				Bin2HexStr((char *)tmpTagLen, 2, (char *)tmpBuf);
				memcpy(tmpScript + scriptLen, tmpBuf, 4);
				scriptLen += 4;	
			}else if (tmpLen < 0x10000)
			{
				tmpTagLen[0] = 0x82;
				tmpTagLen[1] = tmpLen/256;
				tmpTagLen[1] = tmpLen%256;
				Bin2HexStr((char *)tmpTagLen, 3, (char *)tmpBuf);
				memcpy(tmpScript + scriptLen, tmpBuf, 6);
				scriptLen += 6;
			}else{
				FreeTagList(&tmpList);
				return STATUS_FAIL;
			}

			//Value
			Bin2HexStr((char *)(tmpList.tagItem[loop].Value), tmpList.tagItem[loop].Length, (char *)tmpBuf);
			memcpy(tmpScript+scriptLen, tmpBuf, (tmpList.tagItem[loop].Length)*2);
			scriptLen += (tmpList.tagItem[loop].Length)*2;

			//Seperator
			tmpScript[scriptLen] ='|';
			scriptLen +=1;
		}
	}
	if(scriptLen != 0)
	{
		SetRequestItem(REQ_SCRIPT, tmpScript, scriptLen-1);
	}

	if (TagIsExisted(&tmpList, TAG_AUTH_CODE))
	{
		LOGD("TAG_AUTH_CODE EXIST");
		SetTagValue(TAG_AUTH_CODE, GetTagValue(&tmpList, TAG_AUTH_CODE),
			GetTagValueSize(&tmpList, TAG_AUTH_CODE), &(runningTimeEnv.tagList));
	}
	
	FreeTagList(&tmpList);
	return STATUS_OK;
}


#else
int32 ParseHostData(byte buf[], uint16 size)
{
	uint16 Tag;
	uint16 TagSize;
	uint16 LengthSize;
	uint16 Length;
	byte tmpBuf[MAX_COMM_HOST_RECV_BUF_SIZE*2];
	byte tmpScript[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 scriptLen;
	byte *CurPos;
	byte *EndPos;
	int32 ret;
	int32 iTagFlag[3] = {0, 0, 0};
	byte tmpARC[4] = {0};
	byte tmpIAD[500] = {0};

	LOGD("List parse Host data");
	hexdump(buf, size);

	if (STATUS_OK != ParseTlvInfo(buf, size, &Tag, &TagSize, &Length, &LengthSize))
	{
		printf("ParseTlvInfo error\n");
		return STATUS_FAIL;
	}

	if(TagSize + LengthSize + Length != size)
	{
		printf("Size error\n");
		return STATUS_FAIL;
	}

	CurPos =(byte *) (buf + TagSize + LengthSize);
	EndPos = CurPos + Length;

	scriptLen =0;
	while((CurPos < EndPos))
	{
		if(CurPos[0] ==0x66)
		{
			printf("script:");
			PrintByteArray(CurPos +2, CurPos[1]);
			Bin2HexStr(CurPos +2, CurPos[1], tmpBuf);
			memcpy(tmpScript+scriptLen, tmpBuf, CurPos[1]*2);
			scriptLen +=CurPos[1]*2;
			tmpScript[scriptLen] ='|';
			scriptLen +=1;
			
			CurPos +=2 + CurPos[1];
			continue;
		}
		
		ret = ParseTlvInfo(CurPos, (uint16)(EndPos-CurPos), &Tag, &TagSize, &Length, &LengthSize);
		if (ret != STATUS_OK)
		{
			printf("Loop ParseTlvInfo error\n");
			return STATUS_FAIL;
		}
		
		CurPos += TagSize + LengthSize;

		printf("Tag = 0x%04x\n", Tag);

		if(Tag == 0x8A)
		{
			if(Length != 2)			
			{
				return STATUS_FAIL;
			}
			else
			{
				//SetRequestItem(REQ_ARC, CurPos, 2);
				memcpy(tmpARC, CurPos, 2);
				if (tmpARC[0] == 0x03)
				{
					//To void ETX, jason added comments on 2017.04.30
					tmpARC[0] = 0x04;
				}
				if (tmpARC[1] == 0x03)
				{
					//To void ETX, jason added comments on 2017.04.30
					tmpARC[1] = 0x04;
				}
				tmpARC[2] = 0x00;
			}

			if (iTagFlag[0] == 1)
			{
				printf("Exist Tag 8A, error\n");
				return STATUS_FAIL;
			}else
			{
				iTagFlag[0] = 1;
			}
		}
		else if(Tag ==0x91)
		{
			Bin2HexStr(CurPos, Length, tmpBuf);
			strcpy(tmpIAD, tmpBuf);
			//SetRequestItemStr(REQ_ISSUER_AUTH_DATA, tmpBuf);

			if (iTagFlag[1] == 1)
			{
				printf("Exist Tag 91, error\n");
				return STATUS_FAIL;
			}else
			{
				iTagFlag[1] = 1;
			}
		}
		/*
		else if(Tag ==0x11)
		{
			printf("Tag 11, error");
		
			Bin2HexStr(CurPos, Length, tmpBuf);
			gLeftAmount =atoi(tmpBuf);
			if (iTagFlag[2] == 1)
			{
				printf("Exist Tag 11, error\n");
			}else
			{
				iTagFlag[2] = 1;
			}
		}*/
		else
		{
			printf("Other error tag\n");
			return STATUS_FAIL;
	
		}
			
		CurPos += Length;
		
	}

/*
	printf("CurPos = %08x, EndPos = %08x\n", CurPos, EndPos);
	if (CurPos != EndPos)
	{
		printf("CurPos != EndPos \n");
		return STATUS_FAIL;
	}
*/
	//Set value
	SetRequestItem(REQ_ARC, tmpARC, 2);
	SetRequestItemStr(REQ_ISSUER_AUTH_DATA, tmpIAD);

	if(scriptLen != 0)
	{
		SetRequestItem(REQ_SCRIPT, tmpScript, scriptLen-1);
	}
	
	return STATUS_OK;
}
#endif

//Jason delete on 2013.05.23
/*
int32 ParseHostData(const byte buf[], uint16 size)
{
	uint16 Tag;
	uint16 TagSize;
	uint16 LengthSize;
	uint16 Length;
	byte tmpBuf[MAX_COMM_HOST_RECV_BUF_SIZE*2];
	byte tmpScript[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 scriptLen;
	byte *CurPos;
	byte *EndPos;

	if (STATUS_OK != ParseTlvInfo(buf, size, &Tag, &TagSize, &Length, &LengthSize))
	{
		printf("ParseTlvInfo error\n");
		return STATUS_FAIL;
	}

	if(TagSize + LengthSize + Length != size)
	{
		return STATUS_FAIL;
	}

	CurPos =(byte *) (buf + TagSize + LengthSize);
	EndPos = CurPos + Length;

	scriptLen =0;
	while((CurPos < EndPos))
	{
		if(CurPos[0] ==0x66)
		{
			printf("script:");
			PrintByteArray(CurPos +2, CurPos[1]);
			Bin2HexStr(CurPos +2, CurPos[1], tmpBuf);
			memcpy(tmpScript+scriptLen, tmpBuf, CurPos[1]*2);
			scriptLen +=CurPos[1]*2;
			tmpScript[scriptLen] ='|';
			scriptLen +=1;
			
			CurPos +=2 + CurPos[1];
			continue;
		}
		
		ParseTlvInfo(CurPos, (uint16)(EndPos-CurPos), &Tag, &TagSize, &Length, &LengthSize);
		
		CurPos += TagSize + LengthSize;

		if(Tag == 0x8A)
		{
			if(Length != 2)			
			{
				return STATUS_FAIL;
			}
			else
			{
				SetRequestItem(REQ_ARC, CurPos, 2);
			}
		}
		else if(Tag ==0x91)
		{
			Bin2HexStr(CurPos, Length, tmpBuf);
			SetRequestItemStr(REQ_ISSUER_AUTH_DATA, tmpBuf);
		}
		else if(Tag ==0x11)
		{
			Bin2HexStr(CurPos, Length, tmpBuf);
			gLeftAmount =atoi(tmpBuf);
		}
			
		CurPos += Length;
		
	}

	if(scriptLen != 0)
	{
		SetRequestItem(REQ_SCRIPT, tmpScript, scriptLen-1);
	}
	
	return STATUS_OK;
}
*/
//End


/*
int32 GetSriptResultTLV(byte *buf, uint16 *bufLen)
{	
	tti_int32 index;
	tti_int32 totalLen = 0;

	for (index=0; index<runningTimeEnv.scriptsList.scriptsNumber; index++)
		{
			if (runningTimeEnv.scriptsList.scripts[index].scriptValue[0]==TAG_TEMPERATE_71)
			{
				LOGD("TAG_TEMPERATE_71, scriptResult list");
				hexdump(runningTimeEnv.scriptsList.scriptResults[index].scriptResult, 
					runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen);
				buf[totalLen] = TAG_SCRIPT_RESULT_BCTC/256;
				buf[totalLen + 1] = TAG_SCRIPT_RESULT_BCTC%256;
				buf[totalLen + 2] = runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen;
				memcpy(buf + totalLen + 3, runningTimeEnv.scriptsList.scriptResults[index].scriptResult, 
					runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen);
				totalLen += runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen + 3;
			}
		}
		
		for (index=0; index<runningTimeEnv.scriptsList.scriptsNumber; index++)
		{
			if (runningTimeEnv.scriptsList.scripts[index].scriptValue[0]==TAG_TEMPERATE_72)
			{
                LOGD("TAG_TEMPERATE_72, scriptResult list");
				hexdump(runningTimeEnv.scriptsList.scriptResults[index].scriptResult, 
					runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen);
				buf[totalLen] = TAG_SCRIPT_RESULT_BCTC/256;
				buf[totalLen + 1] = TAG_SCRIPT_RESULT_BCTC%256;
				buf[totalLen + 2] = runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen;
				memcpy(buf + totalLen + 3, runningTimeEnv.scriptsList.scriptResults[index].scriptResult, 
					runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen);
				totalLen += runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen + 3;
			}
		}

		*bufLen = totalLen;

		return STATUS_OK;
}*/

int32 GetSriptResultTLV(byte *buf, uint16 *bufLen)
{	
	tti_int32 index;
	tti_int32 totalLen = 0;

	for (index=0; index<runningTimeEnv.scriptsList.scriptsNumber; index++)
		{
			if (runningTimeEnv.scriptsList.scripts[index].scriptValue[0]==TAG_TEMPERATE_71)
			{
				LOGD("TAG_TEMPERATE_71, scriptResult list");
				hexdump(runningTimeEnv.scriptsList.scriptResults[index].scriptResult, 
					runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen);
				//buf[totalLen] = TAG_SCRIPT_RESULT_BCTC/256;
				//buf[totalLen + 1] = TAG_SCRIPT_RESULT_BCTC%256;
				//buf[totalLen + 2] = runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen;
				memcpy(buf + 3 + totalLen , runningTimeEnv.scriptsList.scriptResults[index].scriptResult, 
					runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen);
				totalLen += runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen;
			}
		}
		
		for (index=0; index<runningTimeEnv.scriptsList.scriptsNumber; index++)
		{
			if (runningTimeEnv.scriptsList.scripts[index].scriptValue[0]==TAG_TEMPERATE_72)
			{
                LOGD("TAG_TEMPERATE_72, scriptResult list");
				dbgHex("TAG_TEMPERATE_72, scriptResult list:", runningTimeEnv.scriptsList.scriptResults[index].scriptResult, runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen);
				hexdump(runningTimeEnv.scriptsList.scriptResults[index].scriptResult, 
					runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen);
				//buf[totalLen] = TAG_SCRIPT_RESULT_BCTC/256;
				//buf[totalLen + 1] = TAG_SCRIPT_RESULT_BCTC%256;
				//buf[totalLen + 2] = runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen;
				memcpy(buf + 3 + totalLen, runningTimeEnv.scriptsList.scriptResults[index].scriptResult, 
					runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen);
				totalLen += runningTimeEnv.scriptsList.scriptResults[index].scriptResultLen;
			}
		}

		if (totalLen > 0)
		{
			buf[0] = TAG_SCRIPT_RESULT_BCTC/256;
			buf[1] = TAG_SCRIPT_RESULT_BCTC%256;
			if (totalLen < 128)
			{
				buf[2] = totalLen;
			}else
			{
				LOGE("totalLen = %d, too much", totalLen);
				return STATUS_FAIL;
			}
			*bufLen = totalLen + 3;
		}else
		{
			*bufLen = 0;
		}
		
		return STATUS_OK;
}



int32 GetTransResultRequest(byte *buf, uint16 *bufLen)
{
	tti_uint16 tagTLVLen;
	tti_uint16 tagList[] = {TAG_TVR, TAG_TSI};
	//tti_int32 index;
	tti_int32 totalLen;
	byte transactionResult;

	totalLen = 4;
	transactionResult = GetTransactionResultBCTC();
	if (transactionResult == TRANSACTION_RESULT_NOT_START_BCTC )
	{
		//No need send
        LOGD("TRANSACTION_RESULT_NOT_START_BCTC");
		return STATUS_FAIL;
	}else
	{
		buf[4] = TAG_TRANSACTION_RESULT_BCTC;
		buf[5] = 0x01;
		buf[6] = transactionResult;
		totalLen += 3;
	}

	if (transactionResult != TRANSACTION_RESULT_TERMINATED_BCTC)
	{
		GetSriptResultTLV(buf + totalLen, &tagTLVLen);
		totalLen += tagTLVLen;
	}
	
	if (transactionResult != TRANSACTION_RESULT_TERMINATED_BCTC)
	{
		//PrintOutTagList(&(runningTimeEnv.tagList), "TransResult Request function");
		TagArrsBuildTLV(&(runningTimeEnv.tagList), tagList, sizeof(tagList)/sizeof(tagList[0]), buf + totalLen, &tagTLVLen);
		totalLen += tagTLVLen;
	}else{
		TagArrsBuildTLV(&(runningTimeEnv.tagList), tagList, sizeof(tagList)/sizeof(tagList[0]), buf + totalLen, &tagTLVLen);
		totalLen += tagTLVLen;
	}

	buf[0] = STX;
    buf[1] = 0xc1;
    buf[2] = (totalLen - 4)/256;
    buf[3] = (totalLen - 4)%256;

	*bufLen = totalLen;

	return STATUS_OK;
}





int32 GetQpbocTransResultRequest(byte *buf, uint16 *bufLen)
{
	tti_uint16 tagTLVLen;
	tti_uint16 tagList[] = {TAG_TVR, TAG_TSI};
	//tti_int32 index;
	tti_int32 totalLen;
	tti_tchar tmpStr[100];
	byte transactionResult;

	totalLen = 4;
	transactionResult = GetTransactionResultBCTC();
	if (transactionResult == TRANSACTION_RESULT_NOT_START_BCTC )
	{
		//No need send
        LOGD("TRANSACTION_RESULT_NOT_START_BCTC");
		return STATUS_FAIL;
	}else
	{
		buf[4] = TAG_TRANSACTION_RESULT_BCTC;
		buf[5] = 0x01;
		buf[6] = transactionResult;
		totalLen += 3;
	}

	if (transactionResult != TRANSACTION_RESULT_TERMINATED_BCTC)
	{
		GetSriptResultTLV(buf + totalLen, &tagTLVLen);
		totalLen += tagTLVLen;
	}
	
	if (transactionResult != TRANSACTION_RESULT_TERMINATED_BCTC)
	{
		//PrintOutTagList(&(runningTimeEnv.tagList), "TransResult Request function");
		TagArrsBuildTLV(&(runningTimeEnv.tagList), tagList, sizeof(tagList)/sizeof(tagList[0]), buf + totalLen, &tagTLVLen);
		totalLen += tagTLVLen;
	}else{
		TagArrsBuildTLV(&(runningTimeEnv.tagList), tagList, sizeof(tagList)/sizeof(tagList[0]), buf + totalLen, &tagTLVLen);
		totalLen += tagTLVLen;
	}

	strcpy(tmpStr, "DFC10B0100");
	//memcpy(buf + totalLen, "");

	buf[0] = STX;
    buf[1] = 0xc1;
    buf[2] = (totalLen - 4)/256;
    buf[3] = (totalLen - 4)%256;

	*bufLen = totalLen;

	return STATUS_OK;
}

int32 ParseTransResultResponse(const byte buf[], uint16 size)
{
	if (size != 7)
	{
		return STATUS_FAIL;
	}

	if (buf[0] != STX || buf[1] != 0x81)
	{
		return STATUS_FAIL;
	}

	if (buf[2] != 0x00 || buf[3] != 0x03)
	{
		return STATUS_FAIL;
	}

	if (buf[4] != 0x03 || buf[5] != 0x01 || buf[6] != 0x00)
	{
		return STATUS_FAIL;
	}

	return STATUS_OK;
}

int32 GetTransStartRequest(byte *buf, uint16 *bufLen)
{
	buf[0] = STX;
    buf[1] = 0xc0;
    buf[2] = 0x00;
    buf[3] = 0x00;

	*bufLen = 4;

	return STATUS_OK;
}

int32 ParseTransStartResponse(const byte buf[], uint16 size, TagList *pTagList, Tag3ByteList *pTag3ByteList)
{
	//byte tmpBuf[MAX_COMM_HOST_RECV_BUF_SIZE];
	uint16 dataLen;
	
	int32 ret;
	//int32 loop;
	//int32 scriptLen;
	//int32 tmpLen;
	//byte tmpTagLen[BUFFER_SIZE_32BYTE]; 
	//char tmpIAD[BUFFER_SIZE_512BYTE];
	//TagList tmpTagList;
	//Tag3ByteList tmpTag3byteList;
	
	if (size < 4)
	{
		return STATUS_FAIL;
	}

	if (buf[0] != STX || buf[1] != 0x80)
	{
		REPORT_ERR_LINE();
		return STATUS_FAIL;
	}

	dataLen = buf[2] * 256 + buf[3];
	if ((dataLen + 4) != size)
	{
		return STATUS_FAIL;
	}


	
	ret = BuildTagListOneLevelBctc((tti_byte *)(buf + 4), size - 4, pTagList, pTag3ByteList);
	if (ret != STATUS_OK)
	{
		LOGE("BuildTagListOneLevel ERR");
		return STATUS_FAIL;
	}
	//PrintOutTagList(&tmpTagList, "Parse transaction start Response");

	//PrintOutTagList(&tmpTagList, "Parse transaction start Response");

	return STATUS_OK;	
}



int32 GetQpbocBatchCaptureAuth(byte *buf, uint16 *bufLen)
{
	tti_uint16 totalLen;
	tti_uint16 tagTLVLen;
#ifndef VPOS_APP    
	tti_uint16 tagList[] = {
							TAG_TRACK2,
							TAG_PAN,
							TAG_AMOUNT,
							TAG_CARDHOLDER_NAME, 
							TAG_APP_EXPIRATION_DATE, 
							TAG_PAN_SEQU_NUMBER, 
							TAG_AIP,
							TAG_TRANSACTION_DATE,
							TAG_TRANSACTION_TYPE,
							TAG_ISSUER_APP_DATA,
							TAG_TERM_COUNTRY_CODE,
							TAG_CRYPTOGRAM,
							TAG_CRYPTOGRAM_INFO_DATA,
							TAG_ATC,
							TAG_UNPREDICTABLE_NUMBER,
							TAG_QPBOC_OFFLINE_BALANCE,
							TAG_POS_ENTRY_MODE,
							TAG_QPBOC_PRODUCT_INFO,
							TAG_ONLINE_PIN_BLOCK_BCTC,
							//TAG_QPBOC_9F7C,
                            0x9F74,             //offline trans
						//Jason added on 20200512	
							TAG_QPBOC_USER_SPEC_DATA,
							TAG_TOKEN_REQUESTOR_ID,
							TAG_QPBOC_9F25,
							TAG_PAYMENT_ACCOUNT_REF,
							TAG_AMOUNT_OTHER,
							TAG_TERMINAL_CAPABILITIES,
							TAG_TRANS_CURRENCY_CODE,
							TAG_TVR,
							TAG_TSI,
                            TAG_APP_VERSION_NUMBER,     //0x9F09
                            TAG_ADF_NAME,
							TAG_APP_LABEL,
                            TAG_DF_NAME
						//End
							};
#else
	tti_uint16 tagList[] = {
							0x9F26, 
							0x9F27, 
							0x9F10, 
							0x9F37,
							0x9F36, 
							0x95, 
							0x9A, 
							0x9C, 
							0x9F02, 
							0x5F2A, 
							0x82, 
							0x9F1A, 
							0x9F03, 
                            0x9F33, 
							0x9F34, 
							0x9F35,
                            0x9F1E,
                            0x84,
                            0x9F09,
                            0x9F41,                             
                            0x8A,
                            0x9F74,                    //电子现金交易才有        
                            0x91,
                            0x9B,
                            0x9F21,
                            0x5F34,
                            //0xDF31,
                            //0x9F63,
                            0x4F,
                            0x50,
                            0x9F12                        
							};
#endif
	totalLen = 4;

	
	buf[4] = TAG_TRANSACTION_RESULT_BCTC;
	buf[5] = 0x01;
	buf[6] = 0x01;
	totalLen += 3;

	TagArrsBuildTLV(&(runningTimeEnv.tagList), tagList, sizeof(tagList)/sizeof(tagList[0]), buf + totalLen, &tagTLVLen);
	totalLen += tagTLVLen;

	buf[0] = STX;
    buf[1] = 0x44;
    buf[2] = (totalLen - 4)/256;
    buf[3] = (totalLen - 4)%256;

	*bufLen = totalLen;

	return STATUS_OK;
}



int32 ParseQpbocBatchCaptureResponse(const byte buf[], uint16 size)
{
	if (size != 7)
	{
		return STATUS_FAIL;
	}

	if (buf[0] != STX || buf[1] != 0x04)
	{
		return STATUS_FAIL;
	}

	if (buf[2] != 0x00 || buf[3] != 0x03)
	{
		return STATUS_FAIL;
	}

	if (buf[4] != 0x03 || buf[5] != 0x01 || buf[6] != 0x00)
	{
		return STATUS_FAIL;
	}

	return STATUS_OK;
}


int32 GetBatchCaptureAuth(byte *buf, uint16 *bufLen)
{
	byte transactionResult;
	tti_uint16 totalLen;
	tti_uint16 tagTLVLen;
#ifndef VPOS_APP    
	tti_uint16 tagList[] = {
							TAG_AIP, 
							TAG_TRANS_SEQ_COUNTER, 
							TAG_AUC, 
							TAG_CRYPTOGRAM_INFO_DATA,
							//TAG_CVM,
							TAG_CVM_RESULTS, 
							TAG_IFD_SERIAL_NUMBER, 
							TAG_IAC_DEFAULT, 
							TAG_IAC_DENIAL, 
							TAG_IAC_ONLINE, 
							TAG_ISSUER_APP_DATA, 
							TAG_TERMINAL_CAPABILITIES, 
							TAG_TERMINAL_TYPE, 
							TAG_TVR, 
                            TAG_CRYPTOGRAM, 
							TAG_UNPREDICTABLE_NUMBER, 
							TAG_ACQUIRER_ID,
                            TAG_AMOUNT, 
                            TAG_AMOUNT_OTHER, 
                            TAG_APP_EFFECTIVE_DATE, 
                            TAG_APP_EXPIRATION_DATE, 
                            TAG_PAN, 
                            TAG_PAN_SEQU_NUMBER,
                            TAG_AUTH_CODE, 
                            TAG_RESPONSE_CODE, 
                            TAG_ISSUER_COUNTRY_CODE, 
                            TAG_MERCHANT_CATEGORY_CODE, 
                            TAG_MERCHANT_ID,
                            TAG_POS_ENTRY_MODE, 
                            TAG_TERM_COUNTRY_CODE, 
                            TAG_TERMINAL_ID,
                            TAG_PAYMENT_ACCOUNT_REF,
                            TAG_TOKEN_REQUESTOR_ID,
                            0x9F74,             //offline trans
							//Jason added on 2020/04/24
							TAG_ATC,
							//End
							//Jason added on 2020/04/28
							TAG_TRANSACTION_DATE,
							TAG_TRANSACTION_TIME,
							TAG_TRANSACTION_TYPE,
							TAG_TRANS_CURRENCY_CODE,
							TAG_TSI,
                            //TAG_APP_VERSION_NUMBER,     //0x9F09
                            TAG_ADF_NAME,
							TAG_APP_LABEL,
                            TAG_DF_NAME
							//End
							};
#else
	tti_uint16 tagList[] = {
							0x9F26, 
							0x9F27, 
							0x9F10, 
							0x9F37,
							0x9F36, 
							0x95, 
							0x9A, 
							0x9C, 
							0x9F02, 
							0x5F2A, 
							0x82, 
							0x9F1A, 
							0x9F03, 
                            0x9F33, 
							0x9F34, 
							0x9F35,
                            0x9F1E,
                            0x84,
                            0x9F09,
                            0x9F41,                             
                            0x8A,
                            0x9F74,                            
                            0x91,
                            0x9B,
                            0x9F21,
                            0x5F34,
                            //0xDF31,
                            //0x9F63,
                            0x4F,
                            0x50,
                            0x9F12                        
							};
#endif	
	totalLen = 4;

	transactionResult = GetTransactionResultBCTC();
	if (transactionResult == TRANSACTION_RESULT_NOT_START_BCTC 
		|| transactionResult == TRANSACTION_RESULT_TERMINATED_BCTC)
	{
		//No need send
        LOGE("TRANSACTION_RESULT_NOT_START_BCTC || TRANSACTION_RESULT_TERMINATED_BCTC");
		return STATUS_FAIL;
	}else
	{
		buf[4] = TAG_TRANSACTION_RESULT_BCTC;
		buf[5] = 0x01;
		buf[6] = transactionResult;
		totalLen += 3;
	}

	TagArrsBuildTLV(&(runningTimeEnv.tagList), tagList, sizeof(tagList)/sizeof(tagList[0]), buf + totalLen, &tagTLVLen);
	totalLen += tagTLVLen;
      
	GetSriptResultTLV(buf + totalLen, &tagTLVLen);
	totalLen += tagTLVLen;

	buf[0] = STX;
    buf[1] = 0x44;
    buf[2] = (totalLen - 4)/256;
    buf[3] = (totalLen - 4)%256;

	*bufLen = totalLen;

	return STATUS_OK;
}

int32 ParseBatchCaptureResponse(const byte buf[], uint16 size)
{
	if (size != 7)
	{
		return STATUS_FAIL;
	}

	if (buf[0] != STX || buf[1] != 0x04)
	{
		return STATUS_FAIL;
	}

	if (buf[2] != 0x00 || buf[3] != 0x03)
	{
		return STATUS_FAIL;
	}

	if (buf[4] != 0x03 || buf[5] != 0x01 || buf[6] != 0x00)
	{
		return STATUS_FAIL;
	}

	return STATUS_OK;
}

#ifndef CY20_EMVL2_KERNEL
static struct timeval start_time;
#endif

void InitSeconds()
{
#ifndef CY20_EMVL2_KERNEL	
	gettimeofday(&start_time, NULL);
	return;
#else
	return;
#endif	
}

long int GetMilliSeconds()
{
#ifndef CY20_EMVL2_KERNEL	
	struct timeval tv;
	long int sec, usec;

	gettimeofday(&tv, NULL);
	sec = tv.tv_sec - start_time.tv_sec;
	usec = tv.tv_usec - start_time.tv_usec;
	if(usec < 0)
	{
		sec--;
		usec += 1000000;
	}

	return (sec * 1000 + usec / 1000); // millisecond.
#else
	return 0; 
#endif	
}


/*
* Return Value
-1 -- 	error
>0 --	socket fd
*/
int SocketConnect(char *ip, char *port, int timeoutSecond)
{
#ifndef CY20_EMVL2_KERNEL	
	struct sockaddr_in server_addr;
//	struct hostent *host;
//	int iResolveTimes;
	int sockfd;
	int portnumber = 0;
	int	ret;

	if(ip == NULL)
	{
		LOGE("IP is (NULL)");
		return -1;
	}

	if((portnumber= atoi(port)) < 0)
	{
		LOGE("Port number [%d] parameter value error", port);
		return -1;
	}


	LOGD("Try to connect [%s:%s] ...", ip, port);
	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		LOGE("Calling socket error: [%s]", strerror(errno));
		return -1;
	}

	bzero (&server_addr, sizeof (server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(portnumber);
	server_addr.sin_addr.s_addr = inet_addr(ip);
//	server_addr.sin_addr=*((struct in_addr *)host->h_addr);

	fcntl (sockfd, F_SETFL, O_NONBLOCK);	//set to non-block
	ret = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr));

	//Modify by Louis, check non-block connection. [2008-08-06]
	if(ret == 0)
	{
		LOGD("Host socket connection established");
		return sockfd;	//connection completed immediately
	}
	else
	{
		if(EINPROGRESS == errno)	//connection is processing
		{
			//compare to the NMSStudy Project about judgement non-block connection
			int	iStatus = -1;
			struct timeval	tv;
			fd_set	writefds;
			int	error = 0;
			int	len = sizeof(error);

			if( timeoutSecond < 0)
			{
				timeoutSecond = 10;
			}
			
			tv.tv_sec = timeoutSecond;				//10 seconds as maximum
			tv.tv_usec = 0;
			FD_ZERO(&writefds);
			FD_SET(sockfd, &writefds);

			if((iStatus = select(sockfd+1, NULL, &writefds, NULL, &tv)) > 0)
			{
				// when socket status changed(characters become available)
				// check it option to see if there is a socket error.
				if (FD_ISSET(sockfd, &writefds))
				{
					getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &len);
				}
				else
				{
					LOGE("Select error: sockfd not set");
					close(sockfd);
					return -1;
				}

				if(error == 0)
				{
					LOGE("Host socket connection established");
					return sockfd;
				}
				else
				{
					LOGE("Get socket opt error: [%s]", strerror(error));
					close(sockfd);
					return -1;
				}
			}
			else
			{
				//timeout or error happen
				if(iStatus == 0)
				{
					LOGE("Select error: [Connection timeout (%d) seconds]", timeoutSecond);
				}
				else
				{
					LOGE("Select error: [%s]", strerror(errno));
				}
				close(sockfd);
				return -1;
			}
		}
	}

	//LOGE("Calling connect error: %s", strerror(errno));
	close(sockfd);
	return -1;
	
#else
	int	ret;
	
	ret = iCommTcpConn(ip, port, 5*1000);
	if (ret != 0)
	{
		LOGD("iCommTcpConn return %d\n", ret);
		return -1;
	}
	return 1;
#endif	
}


int SocketSendData(int fd, unsigned char *buf, int len, int timeoutMs)
{
#ifndef CY20_EMVL2_KERNEL	
	struct timeval to;
	fd_set fds;
	int cnt, ret;
	long int done;
	long int timeleft = timeoutMs;

	cnt = 0;
	done = GetMilliSeconds() + timeoutMs;

	while(timeleft >= 0)
	{
		to.tv_sec = timeleft/1000;
		to.tv_usec = (timeleft%1000)*1000;
		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		ret = select(fd+1, NULL, &fds, NULL, &to);
		if(ret < 0)
		{
			if(errno != EINTR)
			{
				LOGE("Twrite->select");
				return -1;
			}
		}

		else if(FD_ISSET(fd, &fds))
		{
		//	DPRINTF("Before write\n");
			ret = write(fd, buf+cnt, len-cnt);
		//	DPRINTF("After write\n");
			if(ret < 0)
			{
				LOGE("Twrite->write");
				return -2;
			}
			cnt += ret;
			if(cnt >= len)
				break;
		}
		timeleft = done - GetMilliSeconds();
	}

	return cnt;
#else
	int ret;

	ret = iCommTcpSend(buf, len);
	//if (ret < len)
	//{
	//	LOGE("wifi_at_tcp_send return = %d\n", ret);
	//	return -1;
	//}
	return ret;
#endif	
}

int SocketRcvData(int fd, unsigned char *buf, int len, int timeout)
{
#ifndef CY20_EMVL2_KERNEL	
	struct timeval to;
	fd_set fds;
	int cnt, ret;
	long int done;
	long int timeleft = timeout;

	cnt = 0;
	done = GetMilliSeconds() + timeout;
	while(timeleft >= 0)
	{
		to.tv_sec = timeleft/1000;
		to.tv_usec = (timeleft%1000)*1000;
		FD_ZERO(&fds);
		FD_SET(fd, &fds);
		ret = select(fd+1, &fds, NULL, NULL, &to);
		if(ret < 0)
		{
			if(errno != EINTR)
			{
				//derror("Tread->select");
				return -1;
			}
		}
		else if(FD_ISSET(fd, &fds))
		{
			ret = read(fd, buf+cnt, len-cnt);
			if(ret < 0)
			{
				//derror("Tread->read");
				return -2;
			}else if(ret == 0)
			{
				return -2;
			}
			//PrintHexStr(buf + cnt, ret);
			cnt += ret;
			if(cnt >= len)
				break;
		}

		timeleft = done - GetMilliSeconds();
	}

	return cnt;
#else
	int ret;	
	
	ret = iCommTcpRecv(buf,len,timeout);

	return ret;
#endif	
}


void SocketDisconnect(int32 socketID)
{
#ifndef CY20_EMVL2_KERNEL	

	close(socketID);
#else
	iCommTcpDisConn();
#endif
}



