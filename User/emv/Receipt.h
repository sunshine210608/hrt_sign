#ifndef RECEIPT_H
#define RECEIPT_H

#include "defines.h"

typedef struct 
{
	int src;
	int index;
	const char *ReceiptHeader;
	//uint16 tag;	
	unsigned short tag;
	const char *BatchCaptureHeader;
}DetailItem_t;

extern int g_DetailItemCount;
extern 	DetailItem_t g_Detail[];

int PrintReceipt(void);
void ClearTransRecord(void);
int SaveDetail(void);
int SaveQpbocBatchCaptureDetail(void);

int IsTransRecordFull(void);

void QpbocBatchDataCaptureBCTC(void);

int GetDetailIndex(int src, int id);
void ParseDetailLine(const char *line, char *value);

void BatchDataCaptureBCTC(void);

void SetPrintOrNot(unsigned char isPrint);

tti_int32 qpbocPrintReceipt(RunningTimeEnv *runningTimeEnv);

int IsQpbocTransRecordFull(void);

#endif

