#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "sysTick.h"
//#include <fcntl.h>

//#include "SDKApi.h"
#include "Define.h"
#include "Util.h"
#include "EmvAppUi.h"
#include "ReqDataItem.h"
#include "RespDataItem.h"
#include "Receipt.h"
#include "MultiLang.h"
#include "Setup.h"
#include "Transaction.h"
#include "emvDebug.h"
#include "emvl2/tag.h"
#include "emvl2/defines.h"
#include "emvl2/misce.h"
#include "sys_littlefs.h"
#include "paramDown.h"
#include "utils.h"
#include "user_projectconfig.h"

#ifdef VPOS_APP
#include "VposFace.h"
#include "pub.h"
#if 0//def APP_LKL
#include "AppGlobal_lkl.h"
#include "MemMana_lkl.h"
#endif
#if 0//def APP_SDJ
#include "AppGlobal_sdj.h"
#include "MemMana_sdj.h"
#endif
#if 1//def APP_CJT
#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#endif
#endif
#include "func.h"

void SetReqItemTransDateTime()
{
	byte Date[3];
	byte Time[3];
	byte tmp[10];

	GetCurrentDateAndTime(Date, Time);

	Bin2HexStr((const char *)Date, 3, (char *)tmp);
	SetRequestItem(REQ_TRANS_DATE, tmp, 6);	

	Bin2HexStr((const char *)Time, 3, (char *)tmp);
	SetRequestItem(REQ_TRANS_TIME, tmp, 6);
}

#ifdef VPOS_APP
int32 SetReqItemTransSeqCounter()
{
    dbg("SetReqItemTransSeqCounter vIncTTC.\n");
    dbg("1. ttc=%06lu\n", gl_SysData.ulTTC);
    vIncTTC();
    dbg("2. ttc=%06lu\n", gl_SysData.ulTTC);
    if(SetRequestItemInt(REQ_TRANS_SEQ_NUMBER, gl_SysData.ulTTC) != STATUS_OK)
	{
        return STATUS_FAIL;
	}
	return STATUS_OK;
}

int32 qpbocSetTransSeqCounter(byte *transSeqCounter, uint16 *len)
{
	byte tempByteArray[10];
	int32 tempByteArrayLen;
	char tempCharArray[20] = {0};
	//lfs_file_t fp;
	//char line[16];
	//int ret;
    
    dbg("qpbocSetTransSeqCounter vIncTTC.\n");
    //dbg("1. ttc=%06lu\n", gl_SysData.ulTTC);
    vIncTTC();
    //dbg("2. ttc=%06lu\n", gl_SysData.ulTTC);
	sprintf(tempCharArray, "%08lu", gl_SysData.ulTTC);
	TranslateStringToBCDArray(tempCharArray, tempByteArray, &tempByteArrayLen);
	//SetTagValue(TAG_TRANS_SEQ_COUNTER, tempByteArray, 4, &runningTimeEnv.tagList);
	memcpy(transSeqCounter, tempByteArray, tempByteArrayLen);
	*len = tempByteArrayLen;

	return STATUS_OK;
}
#else
int32 SetReqItemTransSeqCounter()
{	
#ifndef CY20_EMVL2_KERNEL
	uint16 TransSeqCounter;
	FILE *fp;
	char line[16];
	
	fp = fopen(GetConfFilePath(FILE_TRANS_SEQ_COUNTER), "r+");

	if(fp == NULL)
	{
		fp = fopen(GetConfFilePath(FILE_TRANS_SEQ_COUNTER), "w");
		strcpy(line, "00000");
	}
	else
	{
		fgets(line, sizeof(line), fp);	
	}

	TransSeqCounter = atoi(line);

	if(TransSeqCounter == 65535)
	{
		TransSeqCounter = 1;
	}
	else
	{
		TransSeqCounter += 1;
	}

	fseek(fp, 0, SEEK_SET);
	//fprintf(fp, "%05d",TransSeqCounter);
	fprintf(fp, "%08d",TransSeqCounter);
	fclose(fp);

	if(SetRequestItemInt(REQ_TRANS_SEQ_NUMBER, TransSeqCounter) != STATUS_OK)
	{
			return STATUS_FAIL;
	}

	return STATUS_OK;

#else
	uint16 TransSeqCounter;
	lfs_file_t fp;
	char line[16];
	int ret;
	void *pTmp = NULL;

	FUNCIN;

	memset(line, 0x00, sizeof(line));
	//fp = fopen(GetConfFilePath(FILE_TRANS_SEQ_COUNTER), "r+");
	ret = sys_lfs_file_open(&fp, GetConfFilePath(FILE_TRANS_SEQ_COUNTER), LFS_O_RDWR);
	if (ret != 0)
	{
		//Line;
		dbg("sys_lfs_file_open ret = %d\n", ret);
		ret = sys_lfs_file_open(&fp, GetConfFilePath(FILE_TRANS_SEQ_COUNTER), LFS_O_RDWR | LFS_O_CREAT);
		dbg("sys_lfs_file_open LFS_O_CREAT ret = %d\n", ret);
		strcpy(line, "00000");
		if (ret != 0)
		{
			pTmp = malloc(4096);
			if (pTmp!= NULL)
			{
				//malloc_Counter();
				dbg("pTmp malloc ok\n");
				free(pTmp);
				//free_Counter();
				pTmp = NULL;
			}else
			{
				dbg("pTmp malloc error\n");
			}
		}
	}else
	{
		//Line;
		sys_lfs_file_fgets(line, sizeof(line), &fp);
	}

	TransSeqCounter = atoi(line);
	dbg("TransSeqCounter = %d\n", TransSeqCounter);

	if(TransSeqCounter == 65535)
	{
		TransSeqCounter = 1;
	}
	else
	{
		TransSeqCounter += 1;
	}

	//fseek(fp, 0, SEEK_SET);
	//fprintf(fp, "%05d",TransSeqCounter);
	//fprintf(fp, "%08d",TransSeqCounter);
	//fclose(fp);

	sys_lfs_file_seek(&fp, 0, LFS_SEEK_SET);
	sprintf(line, "%08d", TransSeqCounter);
	//sys_lfs_file_write(&fp, line, 8);
	sys_lfs_file_write(&fp, line, 9);
	sys_lfs_file_close(&fp);

	if(SetRequestItemInt(REQ_TRANS_SEQ_NUMBER, TransSeqCounter) != STATUS_OK)
	{
		return STATUS_FAIL;
	}

	return STATUS_OK;
#endif
}


int32 qpbocSetTransSeqCounter(byte *transSeqCounter, uint16 *len)
{	
#ifndef CY20_EMVL2_KERNEL
	uint16 TransSeqCounter;
	byte tempByteArray[10];
	int32 tempByteArrayLen;
	char tempCharArray[20] = {0};
	FILE *fp;
	char line[16];
	
	fp = fopen(GetConfFilePath(FILE_TRANS_SEQ_COUNTER), "r+");

	if(fp == NULL)
	{
		fp = fopen(GetConfFilePath(FILE_TRANS_SEQ_COUNTER), "w");
		strcpy(line, "00000");
	}
	else
	{
		fgets(line, sizeof(line), fp);	
	}

	TransSeqCounter = atoi(line);
	sprintf(tempCharArray, "%08d", TransSeqCounter);
	TranslateStringToBCDArray(tempCharArray, tempByteArray, &tempByteArrayLen);
	//SetTagValue(TAG_TRANS_SEQ_COUNTER, tempByteArray, 4, &runningTimeEnv.tagList);
	memcpy(transSeqCounter, tempByteArray, tempByteArrayLen);
	*len = tempByteArrayLen;

	if(TransSeqCounter == 65535)
	{
		TransSeqCounter = 1;
	}
	else
	{
		TransSeqCounter += 1;
	}

	fseek(fp, 0, SEEK_SET);
	//fprintf(fp, "%05d",TransSeqCounter);
	fprintf(fp, "%08d", TransSeqCounter);
	fclose(fp);

	return STATUS_OK;

#else
	uint16 TransSeqCounter;
	byte tempByteArray[10];
	int32 tempByteArrayLen;
	char tempCharArray[20] = {0};
	lfs_file_t fp;
	char line[16];
	int ret;

	//fp = fopen(GetConfFilePath(FILE_TRANS_SEQ_COUNTER), "r+");
	ret = sys_lfs_file_open(&fp, GetConfFilePath(FILE_TRANS_SEQ_COUNTER), LFS_O_RDWR);
	if (ret != 0)
	{
		sys_lfs_file_open(&fp, GetConfFilePath(FILE_TRANS_SEQ_COUNTER), LFS_O_RDWR | LFS_O_CREAT);
		strcpy(line, "00000");
	}else
	{
		sys_lfs_file_fgets(line, sizeof(line), &fp);
	}

	TransSeqCounter = atoi(line);
	sprintf(tempCharArray, "%08d", TransSeqCounter);
	TranslateStringToBCDArray(tempCharArray, tempByteArray, &tempByteArrayLen);
	//SetTagValue(TAG_TRANS_SEQ_COUNTER, tempByteArray, 4, &runningTimeEnv.tagList);
	memcpy(transSeqCounter, tempByteArray, tempByteArrayLen);
	*len = tempByteArrayLen;

	if(TransSeqCounter == 65535)
	{
		TransSeqCounter = 1;
	}
	else
	{
		TransSeqCounter += 1;
	}
	
	sys_lfs_file_seek(&fp, 0, LFS_SEEK_SET);
	sprintf(line, "%08d", TransSeqCounter);
	sys_lfs_file_write(&fp, line, 8);
	sys_lfs_file_close(&fp);

	return STATUS_OK;
#endif
}
#endif

int32 SetReqItemTermCountryCode()
{	
	//int fd;
	byte tmpByteArry[10];
	tti_tchar tmpCharArry[20];
	//int len;
	//TagList tagList;
#ifndef VPOS_APP
	LoadTerminalParaBctc(tmpByteArry);
#else	
    memcpy(tmpByteArry, "\x01\x56", 2);		//xfdebug
#endif	
	TranslateHexToChars(tmpByteArry, 2, tmpCharArry);

	LOGD("REQ_TERM_CTRY_CODE = %s", tmpCharArry + 1);
	SetRequestItemStr(REQ_TERM_CTRY_CODE, tmpCharArry + 1);

	return STATUS_OK;
}


int32 SetReqItemIsSupportPSE()
{
	if(SetRequestItem(REQ_IS_SUPPORT_PSE, "1", 1) != STATUS_OK)
	{
		LOGE("SetReqItemIsSupportPSE ERR");
		return STATUS_FAIL;
	}

	return STATUS_OK;
}


int SetReqItemAccountType()
{
#if 0
	TEvntMask event;
	TEvntMask ActiveMask;

	//Jason added on 2012.04.20, start
	if(IsReadLogTransaction())
	{
		SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_SAVINGS);
		return STATUS_OK;
	}
	//End

	ClearScreen();
	DisplayInfoRight("1.Savings", 2);
	DisplayInfoRight("2.Cheque/debit", 4);
	DisplayInfoRight("3.Credit", 6);

	//Changed by Siken 2011.09.24
	//DisplayInfoRight("Others", 8);
	DisplayInfoRight("4.Default", 8);
	//End

//Chnaged by Jason 2012.02.24
/*	
	ActiveMask = KEYBORADEVM_MASK_KEY_F1
				|KEYBORADEVM_MASK_KEY_F2
				|KEYBORADEVM_MASK_KEY_F3
				|KEYBORADEVM_MASK_KEY_F4
				|KEYBORADEVM_MASK_KEY_CANCEL;

*/	
	ActiveMask = KEYBORADEVM_MASK_KEY_1
				|KEYBORADEVM_MASK_KEY_2
				|KEYBORADEVM_MASK_KEY_3
				|KEYBORADEVM_MASK_KEY_4
				|KEYBORADEVM_MASK_KEY_CANCEL;
//End

	event=WaitAKeyDuringTime(ActiveMask, 0);
	switch (event)
	{
		
		case KEYBORADEVM_MASK_KEY_1:
			SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_SAVINGS);
			break;
		
		case KEYBORADEVM_MASK_KEY_2:
			SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_DEBIT_CHEQUE);			
			break;
		case KEYBORADEVM_MASK_KEY_3:
			SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_CREDIT);
			break;
		case KEYBORADEVM_MASK_KEY_4:
			SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_OTHERS);
			break;
		default:
			return STATUS_USER_CANCEL;
			break;
	}
#endif
    //For test

	if(IsReadLogTransaction())
	{
		SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_SAVINGS);
		return STATUS_OK;
	}

	switch (EMVL2_GetAccountType())
	{
		case 0:
			SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_OTHERS);
			break;

		case 1:
			SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_SAVINGS);
			break;

		case 2:
			SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_DEBIT_CHEQUE);
			break;

		case 3:
			SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_CREDIT);
			break;

		default:
			return STATUS_USER_CANCEL;
	}
	return STATUS_OK;
}


/*
int SetReqItemAccountType()
{
	TEvntMask event;
	TEvntMask ActiveMask;

	ClearScreen();
	DisplayInfoRight("Credit", 2);

	ActiveMask = KEYBORADEVM_MASK_KEY_F1;
	event=WaitAKeyDuringTime(ActiveMask, 0);
	switch (event)
	{
		case KEYBORADEVM_MASK_KEY_F1:
			SetRequestItemStr(REQ_ACCOUNT_TYPE, ACCOUNT_TYPE_CREDIT);
			break;

		default:
			return STATUS_USER_CANCEL;
			break;
	}

	return STATUS_OK;
}*/

int32 SeReqItemAmount()
{
	//byte buf[9+1];
	uint32 AmountAuthor;
	uint32 AmountOther;
	
	assert(IsRequestItemExisted(REQ_TRANS_TYPE));
	assert(!IsRequestItemExisted(REQ_AMOUNT_AUTHOR));
	assert(!IsRequestItemExisted(REQ_AMOUNT_OTHER));

	//ClearScreen();	

	if(IsInquiryTransaction())
	{
		SetRequestItemInt(REQ_AMOUNT_AUTHOR, 0);
		SetRequestItemInt(REQ_AMOUNT_OTHER, 0);
		return STATUS_OK;
	}

	//Jason added on 2012.04.20, start
	if(IsReadLogTransaction())
	{
		SetRequestItemInt(REQ_AMOUNT_AUTHOR, 0);
		SetRequestItemInt(REQ_AMOUNT_OTHER, 0);
		return STATUS_OK;
	}
	
	AmountAuthor = EMVL2_GetAmount();
	AmountOther = EMVL2_GetAmountCash();

	AmountAuthor += AmountOther;
    LOGD("AmountAuthor = %d, AmountOther = %d", AmountAuthor, AmountOther);

	SetRequestItemInt(REQ_AMOUNT_AUTHOR, AmountAuthor);
	SetRequestItemInt(REQ_AMOUNT_OTHER, AmountOther);

	//ClearScreen();

	return STATUS_OK;
}



#ifndef PARAMETER_SERVER_BCTC
int32 SetReqItemTermCap()
{
	byte tmp[20];

	assert(IsRequestItemExisted(REQ_TERM_CAP_INPUT));
	assert(IsRequestItemExisted(REQ_TERM_CAP_CVM));
	assert(IsRequestItemExisted(REQ_TERM_CAP_SECURITY));
	
	memcpy(tmp, GetRequestItemValue(REQ_TERM_CAP_INPUT)->ptr, 2);
	memcpy(tmp + 2, GetRequestItemValue(REQ_TERM_CAP_CVM)->ptr, 2);
	memcpy(tmp + 4, GetRequestItemValue(REQ_TERM_CAP_SECURITY)->ptr, 2);

	return SetRequestItem(REQ_TERM_CAP, tmp, 6);
}
#else

#ifdef ENABLE_PRINTER
#define TERMINAL_CAPABILITIES_STRING_PBOC_DEFUALT	"E0E9C8"
#define ADD_TERMINAL_CAPABILITIES_STRING_PBOC_DEFUALT	"F000F0A001"
#else
//#define TERMINAL_CAPABILITIES_STRING_PBOC_DEFUALT	"E0C9C8"
#define TERMINAL_CAPABILITIES_STRING_PBOC_DEFUALT	"E049C8"            //屏蔽脱机PIN
#define ADD_TERMINAL_CAPABILITIES_STRING_PBOC_DEFUALT	"F000F02001"
#endif

int32 SetReqItemTermCap()
{
	byte tmp[20];

	switch(GetKernelType())
	{
		case 0:
			memcpy(tmp, TERMINAL_CAPABILITIES_STRING_EMV_DEFUALT, 6);
			break;

		case 1:
		default:
			memcpy(tmp, TERMINAL_CAPABILITIES_STRING_PBOC_DEFUALT, 6);
			break;		
	}
	
	SetRequestItem(REQ_TERM_CAP_INPUT, tmp, 2);
	SetRequestItem(REQ_TERM_CAP_CVM, tmp + 2, 2);
	SetRequestItem(REQ_TERM_CAP_SECURITY, tmp + 4, 2);
	return SetRequestItem(REQ_TERM_CAP, tmp, 6);
}	

#endif

#ifdef PARAMETER_SERVER_BCTC
int32 SetReqItemTermAddCap()
{
	byte tmp[20];

	switch(GetKernelType())
	{
		case 0:
			memcpy(tmp, ADD_TERMINAL_CAPABILITIES_STRING_EMV_DEFUALT, 10);
			break;

		case 1:
		default:
			memcpy(tmp, ADD_TERMINAL_CAPABILITIES_STRING_PBOC_DEFUALT, 10);
			break;		
	}

	SetRequestItem(REQ_TERM_ADD_TRANS_TYPE, tmp, 4);
	SetRequestItem(REQ_TERM_ADD_INPUT, tmp + 4, 2);
	SetRequestItem(REQ_TERM_ADD_OUTPUT, tmp + 6, 4);
	return SetRequestItem(REQ_TERM_ADD_CAP, tmp, 10);
}

#else
int32 SetReqItemTermAddCap()
{
	byte tmp[20];

	assert(IsRequestItemExisted(REQ_TERM_ADD_TRANS_TYPE));
	assert(IsRequestItemExisted(REQ_TERM_ADD_INPUT));
	assert(IsRequestItemExisted(REQ_TERM_ADD_OUTPUT));
	
	memcpy(tmp, GetRequestItemValue(REQ_TERM_ADD_TRANS_TYPE)->ptr, 4);
	memcpy(tmp + 4, GetRequestItemValue(REQ_TERM_ADD_INPUT)->ptr, 2);
	memcpy(tmp + 6, GetRequestItemValue(REQ_TERM_ADD_OUTPUT)->ptr, 4);

	return SetRequestItem(REQ_TERM_ADD_CAP, tmp, 10);
}

#endif

#ifdef VPOS_APP
int32 SetReqItemAidList()
{	
	int index;
	byte buf[500];
	int tagBinLen;
	char tagAscii[2048];
	byte aidlist[1024*6];
	int SumLen = 0;
	uint len;
	TagList tmpTagList;
	Tag3ByteList tmpTag3ByteList;
	uint uiRet;
    
	for(index = 0; index < AID_NUMBER; index ++)
	{
        uiRet=uiMemManaGetAid(index, buf, &len);
        if (uiRet)
		{
			DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;
		}
        if(len==0)
            break;
		
        //hexdumpEx("AID record list", buf, len);

		InitTagList(&tmpTagList);
		InitTag3ByteList(&tmpTag3ByteList);
		
		BuildTagListOneLevelBctc(buf, len, &tmpTagList, &tmpTag3ByteList);
		//PrintOutTagList(&tmpTagList, "Now list AID tag list");

		ReportLine();
		tagBinLen = GetTagValueSize(&tmpTagList, TAG_APP_VERSION_NUMBER);
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_APP_VERSION_NUMBER), 
			tagBinLen);		
		memcpy(aidlist + SumLen, tagAscii, tagBinLen*2);
		SumLen += (tagBinLen*2);

		ReportLine();
		if (memcmp(GetTagValue(&tmpTagList, TAG_AID_PART_MATCH_BCTC), "\x01", 1) == 0)
		{
			//Support part matach, so need set exact match to not support
			memcpy(aidlist + SumLen, "0", 1);
		}else
		{
			//Not support part matach, so need set exact match to support
			memcpy(aidlist + SumLen, "1", 1);
		}
		SumLen += 1;

		ReportLine();
		tagBinLen = GetTagValueSize(&tmpTagList, TAG_TERMINAL_AID);
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_TERMINAL_AID), 
			tagBinLen);		
		memcpy(aidlist + SumLen, tagAscii, tagBinLen*2);
		SumLen += (tagBinLen*2);

        ReportLine();
		FreeTagList(&tmpTagList);

		aidlist[SumLen] =ITEM_SEPARATOR;
		SumLen +=1;
	}

	if (SumLen == 0)
	{
		LOGE("No AID record, please download them at first");
		return STATUS_FAIL;
	}
	
	if(SetRequestItem(REQ_AID_LIST, aidlist, SumLen -1) != STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}
#else
#ifndef PARAMETER_SERVER_BCTC
int32 SetReqItemAidList()
{
	int i;
	FILE *fp;
	char line[1024];
	char aidlist[1024*10];
	char val[3][512];
	int SumLen;
	int len;
	
	fp = fopen(GetConfFilePath(FILE_NAME_AID_LIST), "r");

	if(fp == NULL)
	{
		return STATUS_FAIL;
	}

	SumLen = 0;
	while(!feof(fp))
	{
		if(fgets(line,sizeof(line), fp)== NULL)
		{
			break;
		}

		if(strlen(line) <5)
		{
			continue;
		}

		if(sscanf(line, "%[^:]:%[^:]:%s", val[0], val[1], val[2]) != 3)
		{
			fclose(fp);
			return STATUS_FAIL;
		}

		for(i=0; i<3; i++)
		{
			len = strlen(val[i]);
			memcpy(aidlist + SumLen, val[i],len);
			SumLen += len;
		}

		aidlist[SumLen] =ITEM_SEPARATOR;
		SumLen +=1;

	}
	
	fclose(fp);
	
	if(SetRequestItem(REQ_AID_LIST, aidlist, SumLen -1) != STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
}
#else
int32 SetReqItemAidList()
{	
	int index;
	int fd;
	byte buf[1024];
	int tagBinLen;
	char tagAscii[2048];
	char filename[200];
	char aidlist[1024*10];
	int SumLen = 0;
	int len;
	TagList tmpTagList;
	Tag3ByteList tmpTag3ByteList;
	lfs_file_t file;

	for(index = 0; index < AID_NUMBER; index ++)
	{
		sprintf(filename, "%s%d", GetConfFilePath(FILE_NAME_AID_LIST_PARA_BCTC), index);
#ifndef CY20_EMVL2_KERNEL
		fd = open(filename, O_RDONLY);

		if(fd <0)
		{
			break;
		}

		len = read(fd, buf, sizeof(buf));
		close(fd);

#else
	   fd = sys_lfs_file_open(&file, filename, LFS_O_RDONLY);
	   if (fd != 0)
	   {
			break;
	   }
	   len = sys_lfs_file_read(&file, buf, sizeof(buf));
	   sys_lfs_file_close(&file);
	   
#endif
		if (len <= 0)
		{
			DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;
		}


        //hexdumpEx("AID record list", buf, len);

		InitTagList(&tmpTagList);
		InitTag3ByteList(&tmpTag3ByteList);
		
		BuildTagListOneLevelBctc(buf, len, &tmpTagList, &tmpTag3ByteList);
		//PrintOutTagList(&tmpTagList, "Now list AID tag list");

		//ReportLine();
		tagBinLen = GetTagValueSize(&tmpTagList, TAG_APP_VERSION_NUMBER);
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_APP_VERSION_NUMBER), 
			tagBinLen);		
		memcpy(aidlist + SumLen, tagAscii, tagBinLen*2);
		SumLen += (tagBinLen*2);

		//ReportLine();
		if (memcmp(GetTagValue(&tmpTagList, TAG_AID_PART_MATCH_BCTC), "\x01", 1) == 0)
		{
			//Support part matach, so need set exact match to not support
			memcpy(aidlist + SumLen, "0", 1);
		}else
		{
			//Not support part matach, so need set exact match to support
			memcpy(aidlist + SumLen, "1", 1);
		}
		SumLen += 1;

		//ReportLine();
		tagBinLen = GetTagValueSize(&tmpTagList, TAG_TERMINAL_AID);
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_TERMINAL_AID), 
			tagBinLen);		
		memcpy(aidlist + SumLen, tagAscii, tagBinLen*2);
		SumLen += (tagBinLen*2);

        //ReportLine();
		FreeTagList(&tmpTagList);

		aidlist[SumLen] =ITEM_SEPARATOR;
		SumLen +=1;
	}
		
	if (SumLen == 0)
	{
		LOGE("No AID record, please download them at first");
		return STATUS_FAIL;
	}

	LOGE("AID SumLen = %d", SumLen);
	LOGE("aidlist = %s", aidlist);
	
	if(SetRequestItem(REQ_AID_LIST, (byte *)aidlist, SumLen -1) != STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	return STATUS_OK;
	
}

#endif
#endif

int32 SetLoadConfigItem()
{		
 	if(SetReqItemAccountType() == STATUS_USER_CANCEL)
 	{
 		ReportLine();
 		return STATUS_USER_CANCEL;
 	}

	if(SeReqItemAmount() == STATUS_USER_CANCEL)
	{
		return STATUS_USER_CANCEL;
	}
	
	SetReqItemTermCap();
	SetReqItemTermAddCap();	
	SetReqItemTransSeqCounter();
	SetReqItemIsSupportPSE();
	if(SetReqItemAidList() != STATUS_OK)
	{
		return STATUS_FAIL;
	}
	
	SetRequestItemStr(REQ_IFD_SERIAL_NUMBER, IFD_DEFAULT);
	SetRequestItemStr(REQ_MERCHANT_ID, MERCHANT_ID_DEFAULT);
	LOGD("SetRequestItemStr REQ_TERM_CTRY_CODE");
	//SetRequestItemStr(REQ_TERM_CTRY_CODE, TERM_COUNTRY_CODE_EMV_DEFAULT);
	SetReqItemTermCountryCode();
	LOGD("After SetRequestItemStr REQ_TERM_CTRY_CODE");
	SetRequestItemStr(REQ_TERM_TYPE, TERM_TYPE_DEFAULT);
	SetRequestItemStr(REQ_TIMEOUT, "100");
	
	return STATUS_OK;
}

#ifdef VPOS_APP
TagList gAidParameterTagList;
Tag3ByteList gAidParaTag3ByteList;

int32 SetInitTransItem()
{
	int index;
	byte buf[500];
	tti_int32 tagBinLen;
	char tagAscii[2048];
	char selectAid[100];
	int selectAidStrLen;
	int tagAsciiLen;
	uint len, ret;
	//lfs_file_t file;
	//Tag3ByteList tmpTag3ByteList;

	SetReqItemTransDateTime();

	SetRequestItemStr(REQ_POS_ENTRY_MODE_CODE, "05");

	GetSelectAid(selectAid);

	selectAidStrLen = strlen(selectAid);

	//Check same length AID at first
	for(index = 0; index < AID_NUMBER; index ++)
	{
		ret = uiMemManaGetAid(index, buf, &len);
		if (ret)
		{
			DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;
		}
        if(len==0)
            break;

        //hexdumpEx("AID record list", buf, len);

		InitTagList(&gAidParameterTagList);
		InitTag3ByteList(&gAidParaTag3ByteList);
		
		BuildTagListOneLevelBctc(buf, len, &gAidParameterTagList, &gAidParaTag3ByteList);
		//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
		//PrintOutTag3ByteList(&gAidParaTag3ByteList, "Now list AID 3 BYTE tag list");

		tagBinLen = GetTagValueSize(&gAidParameterTagList, TAG_TERMINAL_AID);
		HextoA(tagAscii, GetTagValue(&gAidParameterTagList, TAG_TERMINAL_AID), 
			tagBinLen);
		tagAsciiLen = tagBinLen*2;
		tagAscii[tagAsciiLen] = 0x00;

		LOGD("SelectAID = %s, selectAidStrLen = %d", selectAid, selectAidStrLen);
		LOGD("tagAscii = %s, tagAsciiLen = %d", tagAscii, tagAsciiLen);
		
		if (tagAsciiLen > selectAidStrLen)
		{
			//Parameter Aid length is longer than select Aid, not match
			FreeTagList(&gAidParameterTagList);
			continue;
		}else if(tagAsciiLen == selectAidStrLen){
			if (memcmp(tagAscii, selectAid, selectAidStrLen) != 0)
			{
				//Parameter Aid length is same with select Aid, but value no match
				FreeTagList(&gAidParameterTagList);
				continue;
			}else
			{
				LOGD("Find the same length parameter AID");
				LOGD("SelectAID = %s", selectAid);
				LOGD("tagAscii = %s", tagAscii);
                
                if(TagIsExisted(&gAidParameterTagList, TAG_MERCHANT_ID) == FALSE)
                {
                    SetTagValue(TAG_MERCHANT_ID, gl_SysInfo.szMerchId, strlen((char*)gl_SysInfo.szMerchId), &gAidParameterTagList);  //商户号
                    SetTagValue(TAG_TERMINAL_ID, gl_SysInfo.szPosId, strlen((char*)gl_SysInfo.szPosId), &gAidParameterTagList);  //终端号
                    SetTagValue(TAG_MERCHANT_LOCATION, gl_SysInfo.szMerchName, strlen((char*)gl_SysInfo.szMerchName), &gAidParameterTagList);  //商户名
                }
                
                if(TagIsExisted(&gAidParameterTagList, TAG_TRANS_CURRENCY_CODE) == FALSE)
                {
                    SetTagValue(TAG_TRANS_CURRENCY_CODE, (byte*)"\x01\x56", 2, &gAidParameterTagList);
                    SetTagValue(TAG_TRANS_CURRENCY_EXP, (byte*)"\x02", 1, &gAidParameterTagList);
                }
                
                if(TagIsExisted(&gAidParameterTagList, TAG_TRANS_REF_CURRENCY_CODE) == FALSE)
                {
                    SetTagValue(TAG_TRANS_REF_CURRENCY_CODE, (byte*)"\x01\x56", 2, &gAidParameterTagList);
                    SetTagValue(TAG_TRANS_REF_CURRENCY_EXP, (byte*)"\x02", 1, &gAidParameterTagList);
                }
                
                if (GetTag3ByteValueAndSize(&gAidParaTag3ByteList, TAG_DEFAULT_TDOL_BCTC, buf, &tagBinLen) != 0)
                {
                    gAidParaTag3ByteList.tag3ByteItem[gAidParaTag3ByteList.ItemCount].Tag=TAG_DEFAULT_TDOL_BCTC;
                    gAidParaTag3ByteList.tag3ByteItem[gAidParaTag3ByteList.ItemCount].Length = 0x03;
                    memcpy(gAidParaTag3ByteList.tag3ByteItem[gAidParaTag3ByteList.ItemCount].Value, "\x9F\x37\x04", 3);
                    gAidParaTag3ByteList.ItemCount++;
                }
                
                PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
                
				return STATUS_OK;
				
			}
		}else
		{
			//Parameter Aid length is short than select Aid
			//Not check now, must check the same length at first
			FreeTagList(&gAidParameterTagList);
			continue;
		}
		/*
		else
		{
			//Parameter Aid length is short than select Aid
			//Check if Para Aid support part match
			if (memcmp(GetTagValue(&gAidParameterTagList, TAG_AID_PART_MATCH_BCTC), "\x0", 1) == 0)
			{
				//Parameter Aid not support part match, so no need compare, just continue
				FreeTagList(&gAidParameterTagList);
				continue;
			}else
			{
				//Parameter Aid support part match, so need compare the Parameter Aid and select Aid with parameter aid length
				if (memcmp(tagAscii, selectAid, tagAsciiLen) != 0)
				{
					//Parameter Aid not support part match, so no need compare, just continue
					FreeTagList(&gAidParameterTagList);
					continue;
				}else
				{
					//Find the parameter AID
					LOGD("Find the short length parameter AID");
					LOGD("SelectAID = %s", selectAid);
					LOGD("tagAscii = %s", tagAscii);

					PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
					return STATUS_OK;
				}
			}
		}
		*/	
	}

	//Then Check Parameter Aid length is short than select Aid
	for(index = 0; index < AID_NUMBER; index ++)
	{
        ret = uiMemManaGetAid(index, buf, &len);
		if (ret)
		{
			DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;
		}
        if(len==0)
            break;

        //hexdumpEx("AID record list", buf, len);

		InitTagList(&gAidParameterTagList);
		InitTag3ByteList(&gAidParaTag3ByteList);
		
		BuildTagListOneLevelBctc(buf, len, &gAidParameterTagList, &gAidParaTag3ByteList);
		//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
		//PrintOutTag3ByteList(&gAidParaTag3ByteList, "Now list AID 3 BYTE tag list");

		tagBinLen = GetTagValueSize(&gAidParameterTagList, TAG_TERMINAL_AID);
		HextoA(tagAscii, GetTagValue(&gAidParameterTagList, TAG_TERMINAL_AID), 
			tagBinLen);
		tagAsciiLen = tagBinLen*2;
		tagAscii[tagAsciiLen] = 0x00;

		LOGD("SelectAID = %s, selectAidStrLen = %d", selectAid, selectAidStrLen);
		LOGD("tagAscii = %s, tagAsciiLen = %d", tagAscii, tagAsciiLen);
		
		if (tagAsciiLen > selectAidStrLen)
		{
			//Parameter Aid length is longer than select Aid, not match
			FreeTagList(&gAidParameterTagList);
			continue;
		}else if(tagAsciiLen == selectAidStrLen){
			//Same length, already check, no need check again
			FreeTagList(&gAidParameterTagList);
			continue;
		}
		else
		{
			//Parameter Aid length is short than select Aid
			//Check if Para Aid support part match
			if (memcmp(GetTagValue(&gAidParameterTagList, TAG_AID_PART_MATCH_BCTC), "\x0", 1) == 0)
			{
				//Parameter Aid not support part match, so no need compare, just continue
				FreeTagList(&gAidParameterTagList);
				continue;
			}else
			{
				//Parameter Aid support part match, so need compare the Parameter Aid and select Aid with parameter aid length
				if (memcmp(tagAscii, selectAid, tagAsciiLen) != 0)
				{
					//Parameter Aid not support part match, so no need compare, just continue
					FreeTagList(&gAidParameterTagList);
					continue;
				}else
				{
					//Find the parameter AID
					LOGD("Find the short length parameter AID");
					LOGD("SelectAID = %s", selectAid);
					LOGD("tagAscii = %s", tagAscii);

                    if(TagIsExisted(&gAidParameterTagList, TAG_MERCHANT_ID) == FALSE)
                    {
                        SetTagValue(TAG_MERCHANT_ID, gl_SysInfo.szMerchId, strlen((char*)gl_SysInfo.szMerchId), &gAidParameterTagList);  //商户号
                        SetTagValue(TAG_TERMINAL_ID, gl_SysInfo.szPosId, strlen((char*)gl_SysInfo.szPosId), &gAidParameterTagList);  //终端号
                        SetTagValue(TAG_MERCHANT_LOCATION, gl_SysInfo.szMerchName, strlen((char*)gl_SysInfo.szMerchName), &gAidParameterTagList);  //商户名
                    }
                    
                    if(TagIsExisted(&gAidParameterTagList, TAG_TRANS_CURRENCY_CODE) == FALSE)
                    {
                        SetTagValue(TAG_TRANS_CURRENCY_CODE, (byte*)"\x01\x56", 2, &gAidParameterTagList);
                        SetTagValue(TAG_TRANS_CURRENCY_EXP, (byte*)"x02", 1, &gAidParameterTagList);
                    }     

                    if(TagIsExisted(&gAidParameterTagList, TAG_TRANS_REF_CURRENCY_CODE) == FALSE)
                    {
                        SetTagValue(TAG_TRANS_REF_CURRENCY_CODE, (byte*)"\x01\x56", 2, &gAidParameterTagList);
                        SetTagValue(TAG_TRANS_REF_CURRENCY_EXP, (byte*)"\x02", 1, &gAidParameterTagList);
                    }
                    
                    if (GetTag3ByteValueAndSize(&gAidParaTag3ByteList, TAG_DEFAULT_TDOL_BCTC, buf, &tagBinLen) != 0)
                    {
                        gAidParaTag3ByteList.tag3ByteItem[gAidParaTag3ByteList.ItemCount].Tag=TAG_DEFAULT_TDOL_BCTC;
                        gAidParaTag3ByteList.tag3ByteItem[gAidParaTag3ByteList.ItemCount].Length = 0x03;
                        memcpy(gAidParaTag3ByteList.tag3ByteItem[gAidParaTag3ByteList.ItemCount].Value, "\x9F\x37\x04", 3);
                        gAidParaTag3ByteList.ItemCount++;
                    }
                    
					PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
					return STATUS_OK;
				}
			}
		}
	}

	LOGE("No find the AID Parameter2");
	return STATUS_FAIL;	
}

#else

#ifndef PARAMETER_SERVER_BCTC
int32 SetInitTransItem()
{
	SetReqItemTransDateTime();

	SetRequestItemStr(REQ_POS_ENTRY_MODE_CODE, "05");

	return STATUS_OK;
}
#else

TagList gAidParameterTagList;
Tag3ByteList gAidParaTag3ByteList;


int32 SetInitTransItem()
{
	int index;
	int fd;
	byte buf[1024];
	int tagBinLen;
	char tagAscii[2048];
	char filename[200];
	char selectAid[100];
	int selectAidStrLen;
	int tagAsciiLen;
	int len;
	lfs_file_t file;
	//Tag3ByteList tmpTag3ByteList;

	SetReqItemTransDateTime();

	SetRequestItemStr(REQ_POS_ENTRY_MODE_CODE, "05");

	GetSelectAid(selectAid);

	selectAidStrLen = strlen(selectAid);

	//Check same length AID at first
	for(index = 0; index < AID_NUMBER; index ++)
	{
		sprintf(filename, "%s%d", GetConfFilePath(FILE_NAME_AID_LIST_PARA_BCTC), index);
#ifndef CY20_EMVL2_KERNEL	
		fd = open(filename, O_RDONLY);

		if(fd <0)
		{
			break;
		}

		len = sizeof(buf);
		len = read(fd, buf, len);
		close(fd);
#else
	   fd = sys_lfs_file_open(&file, filename, LFS_O_RDONLY);
	   if (fd != 0)
	   {
			break;
	   }
	   len = sys_lfs_file_read(&file, buf, sizeof(buf));
	   sys_lfs_file_close(&file);
	   
#endif
		
		if (len <= 0)
		{
			DisplayValidateInfo("Read Record Fail");	
			return STATUS_FAIL;
		}


        //hexdumpEx("AID record list", buf, len);

		InitTagList(&gAidParameterTagList);
		InitTag3ByteList(&gAidParaTag3ByteList);
		
		BuildTagListOneLevelBctc(buf, len, &gAidParameterTagList, &gAidParaTag3ByteList);
		//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
		//PrintOutTag3ByteList(&gAidParaTag3ByteList, "Now list AID 3 BYTE tag list");

		tagBinLen = GetTagValueSize(&gAidParameterTagList, TAG_TERMINAL_AID);
		HextoA(tagAscii, GetTagValue(&gAidParameterTagList, TAG_TERMINAL_AID), 
			tagBinLen);
		tagAsciiLen = tagBinLen*2;
		tagAscii[tagAsciiLen] = 0x00;

		LOGD("SelectAID = %s, selectAidStrLen = %d", selectAid, selectAidStrLen);
		LOGD("tagAscii = %s, tagAsciiLen = %d", tagAscii, tagAsciiLen);
		
		if (tagAsciiLen > selectAidStrLen)
		{
			//Parameter Aid length is longer than select Aid, not match
			FreeTagList(&gAidParameterTagList);
			continue;
		}else if(tagAsciiLen == selectAidStrLen){
			if (memcmp(tagAscii, selectAid, selectAidStrLen) != 0)
			{
				//Parameter Aid length is same with select Aid, but value no match
				FreeTagList(&gAidParameterTagList);
				continue;
			}else
			{
				LOGD("Find the same length parameter AID");
				LOGD("SelectAID = %s", selectAid);
				LOGD("tagAscii = %s", tagAscii);

				//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");

				return STATUS_OK;
				
			}
		}else
		{
			//Parameter Aid length is short than select Aid
			//Not check now, must check the same length at first
			FreeTagList(&gAidParameterTagList);
			continue;
		}
		/*
		else
		{
			//Parameter Aid length is short than select Aid
			//Check if Para Aid support part match
			if (memcmp(GetTagValue(&gAidParameterTagList, TAG_AID_PART_MATCH_BCTC), "\x0", 1) == 0)
			{
				//Parameter Aid not support part match, so no need compare, just continue
				FreeTagList(&gAidParameterTagList);
				continue;
			}else
			{
				//Parameter Aid support part match, so need compare the Parameter Aid and select Aid with parameter aid length
				if (memcmp(tagAscii, selectAid, tagAsciiLen) != 0)
				{
					//Parameter Aid not support part match, so no need compare, just continue
					FreeTagList(&gAidParameterTagList);
					continue;
				}else
				{
					//Find the parameter AID
					LOGD("Find the short length parameter AID");
					LOGD("SelectAID = %s", selectAid);
					LOGD("tagAscii = %s", tagAscii);

					PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
					return STATUS_OK;
				}
			}
		}
		*/	
	}

	//Then Check Parameter Aid length is short than select Aid
	for(index = 0; index < AID_NUMBER; index ++)
	{
		sprintf(filename, "%s%d", GetConfFilePath(FILE_NAME_AID_LIST_PARA_BCTC), index);
#ifndef CY20_EMVL2_KERNEL
		fd = open(filename, O_RDONLY);

		if(fd <0)
		{
			break;
		}

		len = sizeof(buf);
		len = read(fd, buf, len);
		close(fd);
#else
	   fd = sys_lfs_file_open(&file, filename, LFS_O_RDONLY);
	   if (fd != 0)
	   {
			break;
	   }
	   len = sys_lfs_file_read(&file, buf, sizeof(buf));
	   sys_lfs_file_close(&file);
			   
#endif

		if (len <= 0)
		{
			DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;
		}

        //hexdumpEx("AID record list", buf, len);

		InitTagList(&gAidParameterTagList);
		InitTag3ByteList(&gAidParaTag3ByteList);
		
		BuildTagListOneLevelBctc(buf, len, &gAidParameterTagList, &gAidParaTag3ByteList);
		//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
		//PrintOutTag3ByteList(&gAidParaTag3ByteList, "Now list AID 3 BYTE tag list");

		tagBinLen = GetTagValueSize(&gAidParameterTagList, TAG_TERMINAL_AID);
		HextoA(tagAscii, GetTagValue(&gAidParameterTagList, TAG_TERMINAL_AID), 
			tagBinLen);
		tagAsciiLen = tagBinLen*2;
		tagAscii[tagAsciiLen] = 0x00;

		LOGD("SelectAID = %s, selectAidStrLen = %d", selectAid, selectAidStrLen);
		LOGD("tagAscii = %s, tagAsciiLen = %d", tagAscii, tagAsciiLen);
		
		if (tagAsciiLen > selectAidStrLen)
		{
			//Parameter Aid length is longer than select Aid, not match
			FreeTagList(&gAidParameterTagList);
			continue;
		}else if(tagAsciiLen == selectAidStrLen){
			//Same length, already check, no need check again
			FreeTagList(&gAidParameterTagList);
			continue;
		}
		else
		{
			//Parameter Aid length is short than select Aid
			//Check if Para Aid support part match
			if (memcmp(GetTagValue(&gAidParameterTagList, TAG_AID_PART_MATCH_BCTC), "\x0", 1) == 0)
			{
				//Parameter Aid not support part match, so no need compare, just continue
				FreeTagList(&gAidParameterTagList);
				continue;
			}else
			{
				//Parameter Aid support part match, so need compare the Parameter Aid and select Aid with parameter aid length
				if (memcmp(tagAscii, selectAid, tagAsciiLen) != 0)
				{
					//Parameter Aid not support part match, so no need compare, just continue
					FreeTagList(&gAidParameterTagList);
					continue;
				}else
				{
					//Find the parameter AID
					LOGD("Find the short length parameter AID");
					LOGD("SelectAID = %s", selectAid);
					LOGD("tagAscii = %s", tagAscii);

					//PrintOutTagList(&gAidParameterTagList, "Now list AID tag list");
					return STATUS_OK;
				}
			}
		}
	}

	LOGE("No find the AID Parameter3");
	return STATUS_FAIL;	
}
#endif
#endif

#ifndef PARAMETER_SERVER_BCTC
int32 SetReqItemPublicKey(byte aid[], byte index[])
{
	int i;
	FILE *fp;
	char line[1024 * 10];
	char val[5][1024 *10];
	int arr[]={REQ_PK_EXPONENT,REQ_PK_CHECKSUM, REQ_PK_MODULUS};

	fp = fopen(GetConfFilePath(FILE_NAME_CA_LIST), "r");

	if(fp == NULL)
	{
		return STATUS_OK;
	}

//	printf("fdddddddddddd:AID:%s index=%s", aid, index);

	while(!feof(fp))
	{
		if(fgets(line, sizeof(line), fp) == NULL)
		{
			break;
		}

		if(strlen(line) < 10)
		{
			continue;
		}

		//RID:INDEX:EXP:CHECKSUM:PK
		if(sscanf(line, "%[^:]:%[^:]:%[^:]:%[^:]:%s", val[0], 
						val[1], val[2], val[3], val[4]) != 5)
		{
			return STATUS_FAIL;
		}

//		printf("aid=%s index=%s\n", val[0], val[1]);
		if((memcmp(aid, val[0], 10) != 0)
			||(memcmp(index, val[1], 2) != 0))
		{
			continue;
		}

	//	printf("has found...\n");
		
		if(strcmp(val[2], "03") == 0)
		{
			strcpy(val[2],"0");
		}
		else
		{
			strcpy(val[2],"1");
		}
		
		for(i=0; i<3; i++)
		{
			if(SetRequestItem(arr[i], val[i+2], strlen(val[i+2])) != STATUS_OK)
			{
				fclose(fp);
				return STATUS_FAIL;
			}
		}

		SetRequestItem(REQ_PK_ALGOR, "01", 2);
		SetRequestItem(REQ_PK_HASH_ALGOR, "01", 2);
		SetRequestItem(REQ_PK_ACTIVE_DATE, "01011999", 8);
		SetRequestItem(REQ_PK_EXPIRE_DATE, "01012015", 8);
	}
	
	fclose(fp);

	return STATUS_OK;
}
#else
int32 SetReqItemPublicKey(byte aid[], byte index[])
{
	
	int iIndex = 0;
	int ret;
	byte buf[1024*5];
	char tagAscii[2048];
	char tagAidAscii[100];
	char tagCAIndexAscii[100];
	//int tagAsciiLen;
	//int tagBinLen;
	TagList tmpTagList;
    
#ifdef VPOS_APP
    uint len;
    //int i;
#else    
    int len;
    int fd;
    char filename[200];
#ifndef CY20_EMVL2_KERNEL	
#else
	lfs_file_t file;
#endif
#endif

	hexdumpEx("List AID", aid, 10);
	hexdumpEx("List index", index, 2);
	
	while(1)
	{
#ifdef VPOS_APP	
        ret = uiMemManaGetCaPubKey(iIndex, buf, &len);
        iIndex++;
        if(ret)
        {
			REPORT_ERR_LINE();
			//DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;            
        }
        if(len==0)
            break;
#else        
		sprintf(filename, "%s%d", GetConfFilePath(FILE_NAME_CA_LIST_BCTC), iIndex);
        iIndex++;

#ifndef CY20_EMVL2_KERNEL	
		fd = open(filename, O_RDONLY);
		if(fd <0)
		{
			break;
		}

		len = read(fd, buf, sizeof(buf));
		close(fd);
#else
		fd = sys_lfs_file_open(&file, filename, LFS_O_RDONLY);
		if (fd != 0)
		{
			break;
		}

		len = sys_lfs_file_read(&file, buf, sizeof(buf));
		sys_lfs_file_close(&file);
#endif

		if (len <= 0)
		{
			REPORT_ERR_LINE();
			//DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;
		}
#endif
        //dbg("get pubkey %d\n", iIndex-1);
        //dbgHex("pubkey", buf, len);
        
		InitTagList(&tmpTagList);
		
		ret = BuildTagListOneLevel(buf, len, &tmpTagList);
		if (ret != STATUS_OK)
		{
			REPORT_ERR_LINE();
			//DisplayValidateInfo("Read Record Fail");
			FreeTagList(&tmpTagList);
			return STATUS_FAIL;
		}
		//PrintOutTagList(&tmpTagList, "Now list AID tag list");

		HextoA(tagAidAscii, GetTagValue(&tmpTagList, TAG_TERMINAL_AID), 
			GetTagValueSize(&tmpTagList, TAG_TERMINAL_AID));

		HextoA(tagCAIndexAscii, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_INDEX_TERM), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_INDEX_TERM));

		if (memcmp(tagAidAscii, aid, 10) != 0 ||
			memcmp(tagCAIndexAscii, index, 2) != 0)
		{
			FreeTagList(&tmpTagList);
            mdelay(1);
			continue;
		}

		LOGD("Find the CA");
		hexdumpEx("tagAidAscii", (byte *)tagAidAscii, 10);
		hexdumpEx("tagCAIndexAscii", (byte *)tagCAIndexAscii, 2);
		
		memset(tagAscii, 0, sizeof(tagAscii));
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_SIGN_ALG_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_SIGN_ALG_BCTC));	
		SetRequestItem(REQ_PK_ALGOR, (byte *)tagAscii, strlen(tagAscii));

		memset(tagAscii, 0, sizeof(tagAscii));
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_HASH_ALG_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_HASH_ALG_BCTC));	
		SetRequestItem(REQ_PK_HASH_ALGOR, (byte *)tagAscii, 2);

		memset(tagAscii, 0, sizeof(tagAscii));
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_MOD_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_MOD_BCTC));	
		SetRequestItem(REQ_PK_MODULUS, (byte *)tagAscii, strlen(tagAscii));

		memset(tagAscii, 0, sizeof(tagAscii));
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_EXPONENT_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_EXPONENT_BCTC));	
		LOGD("tagAscii is %s", tagAscii);
		if (strcmp(tagAscii, "010001") == 0)
		{
			SetRequestItem(REQ_PK_EXPONENT, (byte *)("1"), 1);
		}else if (strcmp(tagAscii, "03") == 0)
		{
			SetRequestItem(REQ_PK_EXPONENT, (byte *)("0"), 1);
		}else if (strcmp(tagAscii, "01") == 0)
		{
			SetRequestItem(REQ_PK_EXPONENT, (byte *)("2"), 1);
		}else
		{
			LOGE("REQ_PK_EXPONENT err");
			FreeTagList(&tmpTagList);
			return STATUS_FAIL;	
		}

		memset(tagAscii, 0, sizeof(tagAscii));
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC));	
		SetRequestItem(REQ_PK_CHECKSUM, (byte *)tagAscii, strlen(tagAscii));
		
		SetRequestItem(REQ_PK_ACTIVE_DATE, (byte *)("01011999"), 8);
		SetRequestItem(REQ_PK_EXPIRE_DATE, (byte *)("01012015"), 8);

		FreeTagList(&tmpTagList);
		return STATUS_OK;	
	}

	//DisplayValidateInfo("读取CA参数失败");
	LOGE("No find the CA Parameter FILE");
	return STATUS_FAIL;
}

#endif


tti_int32 qpbocGetPublicKey(RunningTimeEnv *runningTimeEnv)
{
	int iIndex = 0;
	int ret;
    tti_byte buf[1024*5];	
	char tagAscii[2048];
	char tagAidAscii[100];
	char tagCAIndexAscii[100];
	//int tagAsciiLen;
	//int tagBinLen;
    tti_byte tempByteArray[1024];
	//tti_uint16 tempByteLen;
	TagList tmpTagList;
#ifdef VPOS_APP
    uint len;
#else    
    int len;
	int fd;
	char filename[200];    
#ifndef CY20_EMVL2_KERNEL	
#else
	lfs_file_t file;
#endif
#endif

	if (TagDataIsMissing(&(runningTimeEnv->tagList), TAG_CA_PUBLIC_KEY_INDEX) == TRUE)
	{
		LOGE("TAG_CA_PUBLIC_KEY_INDEX_TERM TagDataIsMissing == TRUE");
		return STATUS_FAIL;
	}
	
	while(1)
	{
#ifdef VPOS_APP
        ret = uiMemManaGetCaPubKey(iIndex, buf, &len);
        iIndex++;
        if(ret)
        {
			REPORT_ERR_LINE();
			//DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;            
        }
        if(len==0)
            break;
#else		
		sprintf(filename, "%s%d", GetConfFilePath(FILE_NAME_CA_LIST_BCTC), iIndex);
        iIndex++;
#ifndef CY20_EMVL2_KERNEL	
		fd = open(filename, O_RDONLY);
		if(fd <0)
		{
			break;
		}

		len = read(fd, buf, sizeof(buf));
		close(fd);
#else
		fd = sys_lfs_file_open(&file, filename, LFS_O_RDONLY);
		if (fd != 0)
		{
			break;
		}

		len = sys_lfs_file_read(&file, buf, sizeof(buf));
		sys_lfs_file_close(&file);
#endif
		if (len <= 0)
		{
			REPORT_ERR_LINE();
			//DisplayValidateInfo("Read Record Fail");
			return STATUS_FAIL;
		}
#endif
        
		InitTagList(&tmpTagList);
		
		ret = BuildTagListOneLevel(buf, len, &tmpTagList);
		if (ret != STATUS_OK)
		{
			REPORT_ERR_LINE();
			//DisplayValidateInfo("Read Record Fail");
			FreeTagList(&tmpTagList);
			return STATUS_FAIL;
		}
		//PrintOutTagList(&tmpTagList, "Now list AID tag list");

		HextoA(tagAidAscii, GetTagValue(&tmpTagList, TAG_TERMINAL_AID), 
			GetTagValueSize(&tmpTagList, TAG_TERMINAL_AID));

		HextoA(tagCAIndexAscii, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_INDEX),
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_INDEX));

		if (memcmp(GetTagValue(&tmpTagList, TAG_TERMINAL_AID), GetTagValue(&(runningTimeEnv->tagList), TAG_TERMINAL_AID), 5) != 0 ||
			memcmp(GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_INDEX_TERM), GetTagValue(&(runningTimeEnv->tagList), TAG_CA_PUBLIC_KEY_INDEX), 1) != 0)
		{
			FreeTagList(&tmpTagList);
            mdelay(1);
			continue;
		}

		LOGD("Find the CA");
		hexdumpEx("tagAidAscii", (byte *)tagAidAscii, 10);
		hexdumpEx("tagCAIndexAscii", (byte *)tagCAIndexAscii, 2);

		
		memcpy(tempByteArray, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_HASH_ALG_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_HASH_ALG_BCTC));
		runningTimeEnv->commonRunningData.hashAlgorithm = tempByteArray[0];

		memcpy(tempByteArray, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_HASH_ALG_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_HASH_ALG_BCTC));
		runningTimeEnv->commonRunningData.hashAlgorithm = tempByteArray[0];
		
		memcpy(runningTimeEnv->caPublicKey.modData,  GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_MOD_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_MOD_BCTC));
		runningTimeEnv->caPublicKey.modLen = GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_MOD_BCTC);

		memcpy(runningTimeEnv->commonRunningData.checkSum,  GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC));
		runningTimeEnv->commonRunningData.checkSumLen = GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_CHECKSUM_BCTC);
		

		memset(tagAscii, 0, sizeof(tagAscii));
		HextoA(tagAscii, GetTagValue(&tmpTagList, TAG_CA_PUBLIC_KEY_EXPONENT_BCTC), 
			GetTagValueSize(&tmpTagList, TAG_CA_PUBLIC_KEY_EXPONENT_BCTC));	
		LOGD("tagAscii is %s", tagAscii);
		if (strcmp(tagAscii, "010001") == 0)
		{
			//SetRequestItem(REQ_PK_EXPONENT, "1", 1);
			memcpy(runningTimeEnv->caPublicKey.exp, "\x01\x00\x01", 3);
			runningTimeEnv->caPublicKey.expLen=3;
		}else if (strcmp(tagAscii, "03") == 0)
		{
			//SetRequestItem(REQ_PK_EXPONENT, "0", 1);
			memcpy(runningTimeEnv->caPublicKey.exp, "\x03", 1);
			runningTimeEnv->caPublicKey.expLen=1;
		}else
		{
			LOGE("REQ_PK_EXPONENT err");
			FreeTagList(&tmpTagList);
			return STATUS_FAIL;	
		}

		FreeTagList(&tmpTagList);

		printCertificate(&(runningTimeEnv->caPublicKey));
		return STATUS_OK;	
	}

	//DisplayValidateInfo("读取CA参数失败");
	LOGE("No find the CA Parameter FILE");
	return STATUS_FAIL;
}


void SetPrefferLangByUserSel()
{
#if 0
	TEvntMask event;

	Beep();
	ClearScreen();
	DisplayInfoRight("English", 2);
	//DisplayInfoRight("French", 4);//Jason delete

	event =WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_F1
	//						|KEYBORADEVM_MASK_KEY_F2	//Jason delete
								,60);
	switch(event)
	{
		case KEYBORADEVM_MASK_KEY_F1:
			g_PrefferLang =LANG_ENGLISH;
			break;

		//Jason delete	
		/*case KEYBORADEVM_MASK_KEY_F2:
			g_PrefferLang =LANG_FRENCH;
			break;
		*/
		
		default:
			g_PrefferLang =LANG_ENGLISH;
			break;
	}

	ClearScreen();
#endif
}

//Jason start edit
/*
void SetPrefferLang()
{
	int i;
	int len;
	char *p;

	char tmp[3];

	if(IsResponseItemExisted(RESP_PREFER_LANG))	
	{
		len =GetResponseItemValue(RESP_PREFER_LANG)->len;
		p =GetResponseItemValue(RESP_PREFER_LANG)->ptr;

		if(len >8)
		{
			len =8;
		}

		for(i=0; i<len; i+=2)
		{
			tmp[0] =p[i];
			tmp[1] =p[i+1];
			tmp[2] ='\0';

			if(strcasecmp(tmp, "en") ==0)
			{
				g_PrefferLang =LANG_ENGLISH;
				break;
			}
			else if(strcasecmp(tmp, "fr") ==0)
			{
				g_PrefferLang =LANG_FRENCH;
				break;
			}
		}

		if(i >= len)
		{
			SetPrefferLangByUserSel();
		}
	}
	else
	{
		SetPrefferLangByUserSel();
	}
}*/
//Jason End edit

//Jason start edit

int g_PrefferLang;

void SetPrefferLang()
{	
	g_PrefferLang =LANG_ENGLISH;
	return;
/*
	int i;
	int len;
	char *p;

	char tmp[3];

	if(IsResponseItemExisted(RESP_PREFER_LANG))	
	{
		len =GetResponseItemValue(RESP_PREFER_LANG)->len;
		p =GetResponseItemValue(RESP_PREFER_LANG)->ptr;

		if(len >8)
		{
			len =8;
		}

		for(i=0; i<len; i+=2)
		{
			tmp[0] =p[i];
			tmp[1] =p[i+1];
			tmp[2] ='\0';

			if(strcasecmp(tmp, "en") ==0)
			{
				g_PrefferLang =LANG_ENGLISH;
				break;
			}
		}

		if(i >= len)
		{
			SetPrefferLangByUserSel();
		}
	}
	else
	{
		SetPrefferLangByUserSel();
	}*/
}
//Jason end edit

void SetReqItemEnterPinHint()
{
	SetPrefferLang();
	
	switch(g_PrefferLang)
	{
		case LANG_ENGLISH:
			SetRequestItemStr(REQ_PIN_PROMPT_OFFLINE_PLAIN,	"Pin:(Plain)");
			SetRequestItemStr(REQ_PIN_PROMPT_OFFLINE_ENCRYPT, "Pin:(Encrypt)");
			SetRequestItemStr(REQ_PIN_PROMPT_ONLINE_ENCRYPT, "Pin:(Online)");
			SetRequestItemStr(REQ_PIN_LAST_PIN_TRY, "Last Pin Try");
			break;
		case LANG_FRENCH:
			SetRequestItemStr(REQ_PIN_PROMPT_OFFLINE_PLAIN,	"NIP:(Plain)");
			SetRequestItemStr(REQ_PIN_PROMPT_OFFLINE_ENCRYPT, "NIP:(Encrypt)");
			SetRequestItemStr(REQ_PIN_PROMPT_ONLINE_ENCRYPT, "NIP:(Online)");			
			SetRequestItemStr(REQ_PIN_LAST_PIN_TRY, "Last NIP Try");
			break;
		default:
			dbg("Preffer Lang Function Is Wrong!");
			break;
	}

	
}
void SetReqItemCardHot(void)
{
#if 1	
	//FILE *fp;
	//char line[128];
	//char PAN[128];
	//char PANSeqNo[128];

	//Added by Siken 2011.09.29
	//bool bPANInException = FALSE;
	
	SetRequestItemInt(REQ_CARD_HOT, 0);

	//Not support
	return;
#endif
	
#if 0	
	PAN[0] ='\0';
	PANSeqNo[0] ='\0';
	
	if(IsResponseItemExisted(RESP_PAN))
	{
		strcpy(PAN, GetResponseItemValue(RESP_PAN)->ptr);
	}

	if(IsResponseItemExisted(RESP_PAN_SEQ_NO))
	{
		strcpy(PANSeqNo, GetResponseItemValue(RESP_PAN_SEQ_NO)->ptr);		
	}
	
	fp = fopen(GetConfFilePath(FILE_NAME_EXCEPTION), "r");
	if(fp == NULL)
	{
		return;
	}

	while(!feof(fp))
	{
		if(fgets(line, sizeof(line), fp) == NULL)	
		{
			break;
		}
		
		if(strlen(line) <=1)
		{
			continue;
		}
		
		line[strlen(line) -1] = '\0';

		//Changed by Siken 2011.09.29
		/*
		if(strcmp(line, PAN) == 0
			|| strcmp(line, PANSeqNo) == 0)
		{
			SetRequestItemInt(REQ_CARD_HOT, 1);
		}
		*/
		if(FALSE == bPANInException)
		{
			if(strcmp(line, PAN) == 0)
			{
				bPANInException = TRUE;
			}
		}
		else 
		{
			if(strcmp(line, PANSeqNo) == 0)
			{
				SetRequestItemInt(REQ_CARD_HOT, 1);
			}
			bPANInException = FALSE;
		}
	}
	
	fclose(fp);

#endif
}

#ifdef VPOS_APP
void SetReqItemPANTransLogAmount()
{	
	unsigned TransLogAmount =0;
    stTransRec transRec;
    
	char BufPan[10];
	char Pan[40];
    int i;

	SetRequestItemInt(REQ_TRANS_LOG_AMOUNT, 0);
	if(IsResponseItemExisted(RESP_PAN))
	{
		strncpy(Pan, (char*)GetResponseItemValue(RESP_PAN)->ptr, 19);
        Pan[19]=0;
	}
	else
	{
		return;
	}
    
    strcat(Pan, "FFFFFFFFFF");
    vTwoOne((uchar*)Pan, 20, (uchar*)BufPan);
	for(i=0; i<MAX_TRANS_RECORD_NUM && i<gl_SysData.uiTransNum; i++)
	{		
        uiMemManaGetTransRec(i, &transRec);

        if( memcmp(transRec.sPan, BufPan, 10)==0 )
        {
            TransLogAmount +=transRec.ulAmount;
        }
	}
	SetRequestItemInt(REQ_TRANS_LOG_AMOUNT, TransLogAmount);
}
#else
#ifndef CY20_EMVL2_KERNEL
void SetReqItemPANTransLogAmount()
{
	unsigned int Amount;	
	unsigned TransLogAmount =0;
	
	int ForMaxCnt;
	int AmountIndex;
	int PanIndex;
	
	int LineIndex;
	char BufAmount[1024];
	char BufPan[1024];
	char Pan[1024];

	int i;
	FILE *fp;
	char line[1024];
	char filename[MAX_FILE_PATH_LEN];

	SetRequestItemInt(REQ_TRANS_LOG_AMOUNT, 0);
	if(IsResponseItemExisted(RESP_PAN))
	{
		strcpy(Pan, GetResponseItemValue(RESP_PAN)->ptr);
	}
	else
	{
		return;
	}

	AmountIndex =2;
	PanIndex = 4;

//	assert(g_Detail[AmountIndex].index == REQ_AMOUNT_AUTHOR);
//	assert(g_Detail[PanIndex].index == RESP_PAN);
	
	if(AmountIndex>PanIndex)
	{
		ForMaxCnt = AmountIndex+1;
	}
	else
	{
		ForMaxCnt = PanIndex+1;
	}

	fp = NULL;
	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		if(fp != NULL)
		{
			fclose(fp);
		}
		
		sprintf(filename,FMT_TRANS_RECORD_FILE_NAME,DIR_TRANS_RECORD, i);

		fp =fopen(filename, "r");
		if(fp == NULL)
		{
			break;
		}

		BufAmount[0] ='\0';
		BufPan[0] ='\0';

		for(LineIndex =0; LineIndex <ForMaxCnt; LineIndex++)
		{
			if(fgets(line, sizeof(line) , fp) == NULL)
			{
				break;
			}

			if(LineIndex ==AmountIndex)
			{
				ParseDetailLine(line, BufAmount);
			}
			else if(LineIndex ==PanIndex)
			{
				ParseDetailLine(line, BufPan);
			}
		}

		if(BufAmount[0] !='\0' && BufPan[0] !='\0')
		{
			if(strcmp(Pan, BufPan) == 0)
			{
				Amount = atoi(BufAmount);
				TransLogAmount +=Amount;
			}
		}
	}

	if(fp != NULL)
	{
		fclose(fp);
	}

	SetRequestItemInt(REQ_TRANS_LOG_AMOUNT, TransLogAmount);
}
#else
void SetReqItemPANTransLogAmount()
{
#if 1
	unsigned int Amount;	
	unsigned TransLogAmount =0;
	
	int ForMaxCnt;
	int AmountIndex;
	int PanIndex;
	
	int LineIndex;
	char BufAmount[1024];
	char BufPan[1024];
	char Pan[1024];

	int i;
	lfs_file_t file;
	int ret = -1;
	char line[1024];
	char filename[MAX_FILE_PATH_LEN];

	SetRequestItemInt(REQ_TRANS_LOG_AMOUNT, 0);
	if(IsResponseItemExisted(RESP_PAN))
	{
		strcpy(Pan, (char *)(GetResponseItemValue(RESP_PAN)->ptr));
	}
	else
	{
		return;
	}

	AmountIndex =2;
	PanIndex = 4;

//	assert(g_Detail[AmountIndex].index == REQ_AMOUNT_AUTHOR);
//	assert(g_Detail[PanIndex].index == RESP_PAN);
	
	if(AmountIndex>PanIndex)
	{
		ForMaxCnt = AmountIndex+1;
	}
	else
	{
		ForMaxCnt = PanIndex+1;
	}

	for(i=0; i<MAX_TRANS_RECORD_NUM; i++)
	{
		if(ret == 0)
		{
			sys_lfs_file_close(&file);
			ret = -1;
		}
		
		sprintf(filename,FMT_TRANS_RECORD_FILE_NAME,DIR_TRANS_RECORD, i);

		ret = sys_lfs_file_open(&file, filename, LFS_O_RDONLY);
		if(ret != 0)
		{
			break;
		}

		BufAmount[0] ='\0';
		BufPan[0] ='\0';

		for(LineIndex =0; LineIndex <ForMaxCnt; LineIndex++)
		{
			if(sys_lfs_file_fgets(line, sizeof(line), &file) == NULL)
			{
				break;
			}

			if(LineIndex ==AmountIndex)
			{
				ParseDetailLine(line, BufAmount);
			}
			else if(LineIndex ==PanIndex)
			{
				ParseDetailLine(line, BufPan);
			}
		}

		if(BufAmount[0] !='\0' && BufPan[0] !='\0')
		{
			if(strcmp(Pan, BufPan) == 0)
			{
				Amount = atoi(BufAmount);
				TransLogAmount +=Amount;
			}
		}
	}

	if(ret == 0)
	{
		sys_lfs_file_close(&file);
		ret = -1;
	}
	
	SetRequestItemInt(REQ_TRANS_LOG_AMOUNT, TransLogAmount);
#else
	SetRequestItemInt(REQ_TRANS_LOG_AMOUNT, 10);
#endif
}
#endif
#endif

int32 SetEmvTransItem()
{	
	byte rid[10];
	byte index[2];
	vec_t *pData;

	SetReqItemCardHot();
	SetReqItemPANTransLogAmount();
	SetReqItemEnterPinHint();
	
	//ClearScreen();
	//DisplayInfoCenter("Waiting...", 4);
	if(IsForceOnline())
	{
		SetRequestItemStr(REQ_FORCE_ONLINE, "1");
	}
	else
	{
		SetRequestItemStr(REQ_FORCE_ONLINE, "0");
	}
	SetRequestItemStr(REQ_PIN_PAD_SECRET_KEYS, "0000000000000000");
	if(IsResponseItemExisted(RESP_AID) && IsResponseItemExisted(RESP_CA_PK_INDEX))
	{
		pData = GetResponseItemValue(RESP_AID);		
		if(pData->len <10)
		{
			return STATUS_FAIL;
		}
		memcpy(rid, pData->ptr, 10);
		
		pData =GetResponseItemValue(RESP_CA_PK_INDEX);
		if(pData->len != 2)
		{
			return STATUS_FAIL;
		}
		memcpy(index,pData->ptr, 2);

		SetReqItemPublicKey(rid, index);
	}
	
	return STATUS_OK;
}
