#ifndef INCLUDE_RESP_PARSE_H
#define INCLUDE_RESP_PARSE_H

#include "Define.h"

extern SAidInfo g_CandidateAidList[MAX_CANDIDATE_AID_COUNT];
extern uint16	g_CandidateAidCount;

int32 ParseLoadConfigResp(const char buf[], uint16 len);
int32 ParseGetAidListResp(const char buf[],uint16 len);
int32 ParseInitTransResp(const char buf[],uint16 len);
int32 ParseEmvTransResp(const char buf[],uint16 len);
int32 ParseEmvCompleteResp(const char buf[],uint16 len);
int32 ParseEndTransResp(const char buf[], uint16 len);
#endif

