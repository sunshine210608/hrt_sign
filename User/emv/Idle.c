#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "mhscpu.h"
#include "sysTick.h"
#include "Define.h"
#include "Util.h"
#include "EmvAppUi.h"
#include "Setup.h"
#include "Transaction.h"
#include "ReqDataItem.h"
#include "ReqDataSet.h"
#include "ParamDown.h"
#include "Receipt.h"
#include "ParamPrint.h"
#include "DataTrans.h"
#include "MagCardTrans.h"
#include "emvDebug.h"
#include "test_menu.h"
#include "EvntMask.h"
#include "VposFace.h"
#include "pub.h"
#include "tms_flow.h"
#include "LcdGui.h"
#include "comm.h"
#include "transaction.h"
#include "Receipt.h"
#include "tms_flow.h"
#include "sensor.h"
#include "test_menu.h"
#include "ProjectConfig.h"
#include "Pinpad.h"
#include "tms_Flow.h"
#include "user_projectconfig.h"


#define MENU_1_TIMEOUT		(50*1000) 
#define MENU_OTHER_TIMEOUT	50

#ifndef DISABLE_QRDECODE
#define DISABLE_QRDECODE
#endif

extern int mCommConfig(void *p);
extern int mViewTermInfo1(void *p);
extern int enterdownloadmode(void *p);
int emv_enable_sensor(void *p);

//一级管理子菜单
SMenu menu_repairSubMemu =
{"维护功能", 0, 1, 0,
	{
		{"在线解锁", TMS_Activate},
#if 0
		{"直接激活", emv_enable_sensor},
#endif        
#if 0//def JTAG_DEBUG        
		{"下载密钥", emv_InjectKeyFun},
#endif		
		{"通讯设置", mCommConfig},
        {"终端信息", mViewTermInfo1},
#ifdef USE_1903S
        {"下载模式", enterdownloadmode, NULL},
#endif       
		//{"服务器设置", emv_SetupServerIP},
		//{"下载程序", TMS_RemoteUpdate},
		//{"About", emv_showAppVersion},
        NULL
	}
};

int iAutoRepairPos(void)
{
    extern void vTermInit(void);
    uchar tusn[30];
    
    vTermInit();    //装载参数文件
    
    _uiGetSerialNo(tusn);
    if(tusn[0]==0)
        return 1;
    
    TMS_Activate(NULL);
    return 0;
}

void vRepairPos(void)
{
    extern void vTermInit(void);
    extern void vSetExitToMainFlag(char flag);
    
    vTermInit();    //装载参数文件
    while(1)
    {
        vSetExitToMainFlag(0);
        EnterTestMenu(&menu_repairSubMemu);
        mdelay(10);
    }
}
extern void systemReboot(void);
int emv_enable_sensor(void *p)
{
    _vCls();
	sensorEnable();
	_vDisp(3, "正在重启POS...");
    systemReboot();
	return 0;
}

#ifdef FUDAN_CARD_DEMO
int emv_inputCode(char *szPrompt)
{
	int ret;
	char szTmp[20];
	int amt;

	LcdClear();
	LcdPutsc(szPrompt, 2);

	//带小数点输入金额
	//_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射为小数点键
	_vDisp(3, (const unsigned char *)szPrompt);
	strcpy(szTmp, "1");
	ret=iInput(INPUT_NORMAL, 4, 8, (unsigned char *)szTmp, 8, 2, 8, 30);
	if(ret>0)
	{
		//vDispVarArg(5, "You input is [%s]", szTmp);
		amt = atoi(szTmp);
	}else
	{
		dbg("iInput return = %d\n", ret);
		//vDispVarArg(5, "input fail: [%d]", ret);
		amt = -1;
	}
	//_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	
	return amt;
}

int emv_inputPassword(char *szPrompt)
{
	int ret;
	char szTmp[20];
	int amt;

	LcdClear();
	LcdPutsc(szPrompt, 2);

	//带小数点输入金额
	//_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射为小数点键
	_vDisp(3, (const unsigned char *)szPrompt);
	strcpy(szTmp, "1");
	ret=iInput(INPUT_PIN, 4, 12, (byte *)szTmp, 12, 4, 12, 30);
	if(ret>0)
	{
		//vDispVarArg(5, "You input is [%s]", szTmp);
		amt = atoi(szTmp);
	}else
	{
		dbg("iInput return = %d\n", ret);
		//vDispVarArg(5, "input fail: [%d]", ret);
		amt = -1;
	}
	//_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	
	return amt;
}
int demo_transactionMenu(void *p)
{
	int ret;

	ret = WaitForNFCFudan("Please Tap card");

	if (ret == 0)
	{
		ret = emv_inputCode("Please enter Code");
		if (ret >= 0)
		{
			ret = emv_inputPassword("Please enter Password");
		}
	}
	
	return 0;
}

SMenu menu_DemoMemu =
{"eCash Menu", 0, 1, 0,
	{
		{"eCash Card", demo_transactionMenu},
		{"WIFI Setting", iWifiConfig},
        NULL
	}
};

void vFudanCardApp(void)
{
	extern void vTermInit(void);
	vTermInit();
    while(1)
    {
            mdelay(10);
			vSetExitToMainFlag(0);
            EnterTestMenu(&menu_DemoMemu);
    }
}
#endif

#if 0
void IsNeedExit(void);

bool Emvl2_IsAccountSelect = FALSE;

void Emvl2_Set_AccountSelectType(bool IsAccountSelect )
{
	Emvl2_IsAccountSelect = IsAccountSelect;
}

bool Emvl2_IsAccountNeedSelect()
{
	return Emvl2_IsAccountSelect;
}

int emv_func1_2(void *p)
{
	LcdClear();
	LcdPutsl("this is func1_2", 2);
	//waitkey();
	return 0;
}

int emv_InjectKeyFun(void *p)
{
	char send[100];
	byte response[100];
	int responseLen;

	LcdClear();
	LcdPutsc("注入密钥", 0);

#ifdef WATCHDOG_ENABLE
	enableWDT(10);
#endif
	strcpy(send, "A0.");
	InternalPinpadCmd((byte *)send, strlen(send), response, &responseLen);
#ifdef WATCHDOG_ENABLE
	disableWDT();
#endif

#ifdef WATCHDOG_ENABLE
	enableWDT(155);
#endif
	endlessloop();
#ifdef WATCHDOG_ENABLE
	disableWDT();
#endif	
	return 0;
}


int emv_UptsCmdTestFun(void *p)
{
	char send[100];
	byte response[100];
	int responseLen;

	LcdClear();
	LcdPutsc("UPTS 指令测试", 0);

#ifdef WATCHDOG_ENABLE
	enableWDT(10);
#endif
#ifdef WATCHDOG_ENABLE
	disableWDT();
#endif

#ifdef WATCHDOG_ENABLE
	enableWDT(155);
#endif
	endlessloop();
#ifdef WATCHDOG_ENABLE
	disableWDT();
#endif	
	return 0;
}


int emv_inputAmount(char *szPrompt)
{
	int ret;
	char szTmp[20];
	int amt;

	LcdClear();
	LcdPutsc(szPrompt, 2);

	//带小数点输入金额
	//_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射为小数点键
	_vDisp(3, (const unsigned char *)szPrompt);
	strcpy(szTmp, "1");
	ret=iInput(INPUT_MONEY|INPUT_INITIAL, 4, 8, (unsigned char *)szTmp, 8, 2, 8, 30);
	if(ret>0)
	{
		//vDispVarArg(5, "You input is [%s]", szTmp);
		amt = atoi(szTmp);
	}else
	{
		dbg("iInput return = %d\n", ret);
		//vDispVarArg(5, "input fail: [%d]", ret);
		amt = -1;
	}
	//_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	
	return amt;
}


int emv_inputAutoAmount(char *szPrompt, int iAutoAmount)
{
	int ret;
	char szTmp[20];
	int amt;

	LcdClear();
	LcdPutsc(szPrompt, 2);

	//带小数点输入金额
	_vDisp(3, (const unsigned char *)szPrompt);
	sprintf(szTmp, "%d", iAutoAmount);
	ret=iInput(INPUT_MONEY|INPUT_INITIAL, 4, 8, (unsigned char*)szTmp, 8, 2, 8, 2);
	if(ret>0)
	{
		//vDispVarArg(5, "You input is [%s]", szTmp);
		amt = atoi(szTmp);
	}else
	{
		dbg("iInput return = %d\n", ret);
		//vDispVarArg(5, "input fail: [%d]", ret);
		amt = -1;
	}
	
	return amt;
}

int emv_inputIP(char *szPrompt, char *szIP)
{
	int ret;
	char szTmp[20];

	LcdClear();
	LcdPutsc(szPrompt, 2);

	_vDisp(3, (const unsigned char*)szPrompt);
	_uiMapKey(_KEY_FN, _KEY_DOT);			//功能键映射成点
	ret=iInput(INPUT_ALL_CHAR, 4, 6, (unsigned char *)szTmp, 15, 2, 15, 10);
	_uiMapKey(_KEY_FN, _KEY_FN);			//恢复原值
	if(ret>0)
	{
		strcpy(szIP, szTmp);
		return 0;
	}else
	{
		dbg("iInput return = %d\n", ret);
		//vDispVarArg(5, "input fail: [%d]", ret);
		return -1;
	}
	
}

int emv_inputPort(char *szPrompt, char *szPort)
{
	int ret;
	char szTmp[20];

	LcdClear();
	LcdPutsc(szPrompt, 2);

	_vDisp(3, (const unsigned char*)szPrompt);
	ret=iInput(INPUT_NORMAL, 4, 6, (unsigned char*)szTmp, 5, 2, 5, 1);
	if(ret>0)
	{
		strcpy(szPort, szTmp);
		return 0;
	}else
	{
		dbg("iInput return = %d\n", ret);
		//vDispVarArg(5, "input fail: [%d]", ret);
		return -1;
	}	
}

int emv_transactionMenu(void *p)
{
	TEvntMask pressedKeyEvt;
	char szTransType[10];
	char szAccountType[10];
	char szPrompt[30];
	int iTransType;
	int iAccountType;
	int amount;
	int amountOther;
	
	LcdClear();
	LcdPutsc("交易类型", 0);
	
	LcdPutsl("1.物品", 1);
	LcdPutsl("2.服务", 2);
	LcdPutsl("3.现金", 3);
	LcdPutsl("4.返现", 4);

	pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
										KEYBORADEVM_MASK_KEY_2 |
										KEYBORADEVM_MASK_KEY_3 |
										KEYBORADEVM_MASK_KEY_4 |
										KEYBORADEVM_MASK_KEY_CANCEL, 0);
	switch(pressedKeyEvt)
	{
		case KEYBORADEVM_MASK_KEY_1:
			strcpy(szTransType, TRANS_T_GOOD_SERVICE);
			iTransType = 0x00;
			break;
			
		case KEYBORADEVM_MASK_KEY_2:
			strcpy(szTransType, TRANS_T_GOOD_SERVICE);
			iTransType = 0x00;
			break;
			
		case KEYBORADEVM_MASK_KEY_3:
			strcpy(szTransType, TRANS_T_CASH);
			iTransType = 0x01;
			break;
			
		case KEYBORADEVM_MASK_KEY_4:
			strcpy(szTransType, TRANS_T_GOOD_CASHBACK);
			iTransType = 0x09;
			break;

		case KEYBORADEVM_MASK_KEY_CANCEL:
			return -1;
	}

	if (Emvl2_IsAccountNeedSelect() == TRUE)
	{
	LcdClear();
	LcdPutsc("账号类型", 0);
	
	LcdPutsl("1.默认账户", 1);
	LcdPutsl("2.储蓄账户", 2);
	LcdPutsl("3.借记账户", 3);
	LcdPutsl("4.信用账户", 4);

	pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
										KEYBORADEVM_MASK_KEY_2 |
										KEYBORADEVM_MASK_KEY_3 |
										KEYBORADEVM_MASK_KEY_4 |
										KEYBORADEVM_MASK_KEY_CANCEL, 0);
	switch(pressedKeyEvt)
	{
		case KEYBORADEVM_MASK_KEY_1:
			strcpy(szAccountType, ACCOUNT_TYPE_OTHERS);
			iAccountType = 0;
			break;
			
		case KEYBORADEVM_MASK_KEY_2:
			strcpy(szAccountType, ACCOUNT_TYPE_SAVINGS);
			iAccountType = 1;
			break;
			
		case KEYBORADEVM_MASK_KEY_3:
			strcpy(szAccountType, ACCOUNT_TYPE_DEBIT_CHEQUE);
			iAccountType = 2;
			break;
			
		case KEYBORADEVM_MASK_KEY_4:
			strcpy(szAccountType, ACCOUNT_TYPE_CREDIT);
			iAccountType = 3;
			break;

		case KEYBORADEVM_MASK_KEY_CANCEL:
			return -1;
	}
	}
	else
	{
		iAccountType = 0;
	}
	LcdClear();

	if (GetTransCurrencyCode(NULL) == 0x0840)
	{
		strcpy(szPrompt, "请输入金额 (USD)");
	}else
	{
		strcpy(szPrompt, "请输入金额 (CNY)");
	}

	amount = emv_inputAmount(szPrompt);
	if (amount < 0)
	{
		return -1;
	}

	if (strcmp(szTransType, TRANS_T_GOOD_CASHBACK) == 0)
	{
		if (GetTransCurrencyCode(NULL) == 0x0840)
		{
			strcpy(szPrompt, "请输入其他金额 (USD)");
		}else
		{
			strcpy(szPrompt, "请输入其他金额 (CNY)");
		}
		amountOther = emv_inputAmount(szPrompt);
		if (amountOther < 0)
		{
			return -1;
		}
	}else
	{
		amountOther = 0;
	}

	dbg("amount = %d, amountOther = %d\n", amount, amountOther);
	EMVL2_Transaction_SetParaFromUI(iTransType, iAccountType, amount, amountOther);

	Transaction(" ");
	return 0;
}




int emv_autoTransactionMenu(void *p)
{
	int ret;
	char autoAmount[20];
	char autoAmountOther[20];
	char szPrompt[30];
	unsigned char autoTransType;
	int iAutoAmount;
	int iAutoAmountOther;
	int iTransType;
	int iAccountType;
	
	while(1)
	{
		DisplayProcess("取消键退出");
		ret = iGetKeyWithTimeout(1);
		if (ret == _KEY_CANCEL)
		{
			dbg("Press Cancel to exit test");	
            return 0;
		}
		
		ret = AutoTransaction(autoAmount, autoAmountOther, &autoTransType);
		if (ret == 0xff)
		{
        	dbg("Now exit auto test");	
            return 0;
        }else if (ret != 0) 
        {
         	continue;
        }

		GetAutoTransData(&iAutoAmount, &iAutoAmountOther, &autoTransType);

        if (autoTransType == 0x09) {
            //Cash back type is 0x09, the index is 3
            iTransType = 3;
        } else if (autoTransType == 0x01) {
            //Cash type is 0x01, the index is 2
            iTransType = 2;
        } else if (autoTransType == 0x00) {
            //Good and servce type is 0x00, the index is also 0;
            iTransType = 0;
        } else {
            //other type, set the index to 0;
            iTransType = 0;
        }

		iAccountType = 0;

		LcdClear();
		LcdPutsc("交易类型", 0);
		
		LcdPutsl("1.物品", 1);
		LcdPutsl("2.服务", 2);
		LcdPutsl("3.现金", 3);
		LcdPutsl("4.返现", 4);

		mdelay(300);

		if (Emvl2_IsAccountNeedSelect() == TRUE)
		{
			LcdClear();
			LcdPutsc("账号类型", 0);
			
			LcdPutsl("1.默认账户", 1);
			LcdPutsl("2.储蓄账户", 2);
			LcdPutsl("3.借记账户", 3);
			LcdPutsl("4.信用账户", 4);

			mdelay(300);
		}	

		if (GetTransCurrencyCode(NULL) == 0x0840)
		{
			strcpy(szPrompt, "请输入金额 (USD)");
		}else
		{
			strcpy(szPrompt, "请输入金额 (CNY)");
		}
		emv_inputAutoAmount(szPrompt, iAutoAmount);
		if (iTransType == 3)
		{
			if (GetTransCurrencyCode(NULL) == 0x0840)
			{
				strcpy(szPrompt, "请输入其他金额 (USD)");
			}else
			{
				strcpy(szPrompt, "请输入其他金额 (CNY)");
			}
			emv_inputAutoAmount(szPrompt, iAutoAmountOther);
		}
		
		EMVL2_Transaction_SetParaFromUI(autoTransType, iAccountType, iAutoAmount, iAutoAmountOther);
		SetAutoTrans(TRUE);
		Transaction(" ");
		//qpbocTransaction(NULL);

		DisplayAllNFCLedIconNone();
		SetAutoTrans(FALSE);
	}
}

int emv_downloadCaFun(void *p)
{
	DownloadCA();
	return 0;
}

int emv_downloadTerminalFun(void *p)
{
	DownloadTerminalPara();
	return 0;
}

int emv_downloadAidFun(void *p)
{
	DownloadAIDPara();
	return 0;
}

int emv_CheckSumFun(void *p)
{
	LcdClear();
#if PROJECT_CY20 == 1
	LcdPutsc("内核CheckSum: CCA5B387", 2);
	LcdPutsc("配置CheckSum: DE7523FC", 4);
#else
	LcdPutsc("内核CheckSum: D245F4AC", 2);
	LcdPutsc("配置CheckSum: E35A77EF", 4);
#endif
	bPressKey();
	return 0;
}

int emv_ReadLogFun(void *p)
{
	LcdClear();

	DoReadLogTransaction();
	return 0;
}

int emv_ReadBalanceFun(void *p)
{
	LcdClear();

	DoReadBalanceTransaction();
	return 0;
}

int emv_BatchDataCaptureFun(void *p)
{
	LcdClear();

	//BatchDataCaptureBCTC();
	QpbocBatchDataCaptureBCTC();
	return 0;
}

int emv_SetupServerIP(void *p)
{
	char szIP[30];
	char szPort[20];
	int iRet;

	LcdClear();
	LcdPutsc("设置服务器", 0);

	iRet = emv_inputIP("请输入服务器IP地址", szIP);
	if (iRet < 0)
	{
		return iRet;
	}

	iRet = emv_inputIP("请输入服务器Port", szPort);
	if (iRet < 0)
	{
		return iRet;
	}

	SetServerIPAndPort(szIP, szPort);

	return 0;
}

int test_qrcode(void *m);


int emv_showAppVersion(void *p)
{
	char szDisplayStr[30];
	char szTerminalSN[50];
	int len;

	LcdClear();
	LcdPutsc("固件版本号", 0);

	strcpy(szDisplayStr, FIRMWARE_VERSION);
	LcdPutsc(szDisplayStr, 1);

	LcdPutsc("应用版本号", 3);

	strcpy(szDisplayStr, APP_VERSION);
	LcdPutsc(szDisplayStr, 4);

	LcdPutsc("机器序列号", 6); 
	memset(szTerminalSN, 0x00, sizeof(szTerminalSN));
	Sys_GetUsn((byte *)szTerminalSN, &len);
	//strcpy(szDisplayStr, APP_VERSION);
	LcdPutsc(szTerminalSN, 7);

	//sensorEnable();

	//DisplayNFCLed_UnitTest();

	//TrackDataEncTest();

	//SecOtp_UnitTest();
	/*
	LcdPutsl("确认或者取消键", 8);
	bOK();

	if(makeRebootFlag()==0)
	{
		//LcdClear();

		//LcdPutsl("RebootFlag OK", 1);
		//mdelay(1000*11);
		//systemReboot();
		
	}else
	{
		LcdClear();
		LcdPutsl("RebootFlag REROR", 1);
		LcdPutsl("重启出现错误", 2);
		bPressKey();
	}
	*/

	/*
	sys_lfs_dir_list("./");

	sys_lfs_dir_list("Emvl2/BCTCCA");

	sys_lfs_dir_list("Emvl2/BCTCCA");
	
	sys_lfs_dir_list("Security");

	sys_lfs_dir_list("system");

	sys_lfs_dir_list("OTP");
	*/
	
	bPressKey();

	return 0;
}


int emv_BatchClearFun(void *p)
{
	//ClearTransRecord();
	ClearQpbocTransRecord();
	return 0;
}



int emv_SetAccountTypeFun(void *p)
{
	TEvntMask pressedKeyEvt;

	LcdClear();
	LcdPutsc("选择账户类型", 0);

	LcdPutsl("1.关闭账户类型选择", 2);
	LcdPutsl("2.开启账户类型选择", 4);

	pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
										KEYBORADEVM_MASK_KEY_2 |
										KEYBORADEVM_MASK_KEY_CANCEL, 0);

	switch(pressedKeyEvt)
	{
		case KEYBORADEVM_MASK_KEY_1:
			Emvl2_Set_AccountSelectType(FALSE);
			break;
			
		case KEYBORADEVM_MASK_KEY_2:
			Emvl2_Set_AccountSelectType(TRUE);
			break;

		default:
			break;
	}

	return 0;
}


int emv_SetForceOnlineFun(void *p)
{
	TEvntMask pressedKeyEvt;

	LcdClear();
	LcdPutsc("强制联机", 0);

	
	
	LcdPutsl("1.关闭强制联机", 2);
	LcdPutsl("2.开启强制联机", 4);

	pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
										KEYBORADEVM_MASK_KEY_2 |
										KEYBORADEVM_MASK_KEY_CANCEL, 0);

	switch(pressedKeyEvt)
	{
		case KEYBORADEVM_MASK_KEY_1:
			SetForceOnline(FALSE);
			break;
			
		case KEYBORADEVM_MASK_KEY_2:
			SetForceOnline(TRUE);
			break;

		default:
			break;
	}

	return 0;
}

int emv_SetCurrencyCodeFun(void *p)
{
	TEvntMask pressedKeyEvt;

	LcdClear();
	LcdPutsc("设置当前货币代码", 0);

	
	
	LcdPutsl("1.0840", 2);
	LcdPutsl("2.0156", 4);

	pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
										KEYBORADEVM_MASK_KEY_2 |
										KEYBORADEVM_MASK_KEY_CANCEL, 0);

	switch(pressedKeyEvt)
	{
		case KEYBORADEVM_MASK_KEY_1:
			SetTransCurrencyCode(0x0840);
			break;
			
		case KEYBORADEVM_MASK_KEY_2:
			SetTransCurrencyCode(0x0156);
			break;

		default:
			break;
	}

	return 0;
}


int wdttest(void *p)
{
#ifdef WATCHDOG_ENABLE
    int i=0;
	
    watchdog_init();
    while(1)
	{
        dbg("%d\n",i++);
		mdelay(1000);
    }
#endif

	return 0;
}




int emv_SetPrintFun(void *p)
{
	TEvntMask pressedKeyEvt;

	LcdClear();
	LcdPutsc("打印设置", 0);

	LcdPutsl("1.关闭打印", 2);
	LcdPutsl("2.开启打印", 4);

	pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
										KEYBORADEVM_MASK_KEY_2 |
										KEYBORADEVM_MASK_KEY_CANCEL, 0);

	switch(pressedKeyEvt)
	{
		case KEYBORADEVM_MASK_KEY_1:
			SetPrintOrNot(FALSE);
			break;
			
		case KEYBORADEVM_MASK_KEY_2:
			SetPrintOrNot(TRUE);
			break;

		default:
			break;
	}

	return 0;
}



int emv_qpbocFlow(void *p)
{
	TEvntMask pressedKeyEvt;
	char szTransType[10];
	char szAccountType[10];
	char szPrompt[30];
	int iTransType;
	int iAccountType;
	int amount;
	int amountOther;
	
	LcdClear();
	LcdPutsc("交易类型", 0);
	
	LcdPutsl("1.物品", 1);
	LcdPutsl("2.服务", 2);
	LcdPutsl("3.现金", 3);
	LcdPutsl("4.返现", 4);

	pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
										KEYBORADEVM_MASK_KEY_2 |
										KEYBORADEVM_MASK_KEY_3 |
										KEYBORADEVM_MASK_KEY_4 |
										KEYBORADEVM_MASK_KEY_CANCEL, 0);
	switch(pressedKeyEvt)
	{
		case KEYBORADEVM_MASK_KEY_1:
			strcpy(szTransType, TRANS_T_GOOD_SERVICE);
			iTransType = 0x00;
			break;
			
		case KEYBORADEVM_MASK_KEY_2:
			strcpy(szTransType, TRANS_T_GOOD_SERVICE);
			iTransType = 0x00;
			break;
			
		case KEYBORADEVM_MASK_KEY_3:
			strcpy(szTransType, TRANS_T_CASH);
			iTransType = 0x01;
			break;
			
		case KEYBORADEVM_MASK_KEY_4:
			strcpy(szTransType, TRANS_T_GOOD_CASHBACK);
			iTransType = 0x09;
			break;

		case KEYBORADEVM_MASK_KEY_CANCEL:
			return -1;
	}

	if (Emvl2_IsAccountNeedSelect() == TRUE)
	{
	LcdClear();
	LcdPutsc("账号类型", 0);
	
	LcdPutsl("1.默认账户", 1);
	LcdPutsl("2.储蓄账户", 2);
	LcdPutsl("3.借记账户", 3);
	LcdPutsl("4.信用账户", 4);

	pressedKeyEvt = WaitAKeyDuringTime(KEYBORADEVM_MASK_KEY_1 | 
										KEYBORADEVM_MASK_KEY_2 |
										KEYBORADEVM_MASK_KEY_3 |
										KEYBORADEVM_MASK_KEY_4 |
										KEYBORADEVM_MASK_KEY_CANCEL, 0);
	switch(pressedKeyEvt)
	{
		case KEYBORADEVM_MASK_KEY_1:
			strcpy(szAccountType, ACCOUNT_TYPE_OTHERS);
			iAccountType = 0;
			break;
			
		case KEYBORADEVM_MASK_KEY_2:
			strcpy(szAccountType, ACCOUNT_TYPE_SAVINGS);
			iAccountType = 1;
			break;
			
		case KEYBORADEVM_MASK_KEY_3:
			strcpy(szAccountType, ACCOUNT_TYPE_DEBIT_CHEQUE);
			iAccountType = 2;
			break;
			
		case KEYBORADEVM_MASK_KEY_4:
			strcpy(szAccountType, ACCOUNT_TYPE_CREDIT);
			iAccountType = 3;
			break;

		case KEYBORADEVM_MASK_KEY_CANCEL:
			return -1;
	}
	}
	else
	{
		iAccountType = 0;
	}
	LcdClear();
	if (GetTransCurrencyCode(NULL) == 0x0840)
	{
		strcpy(szPrompt, "请输入金额 (USD)");
	}else
	{
		strcpy(szPrompt, "请输入金额 (CNY)");
	}
	amount = emv_inputAmount(szPrompt);

	if (amount < 0)
	{
		return -1;
	}

	if (strcmp(szTransType, TRANS_T_GOOD_CASHBACK) == 0)
	{
		if (GetTransCurrencyCode(NULL) == 0x0840)
		{
			strcpy(szPrompt, "请输入其他金额 (USD)");
		}else
		{
			strcpy(szPrompt, "请输入其他金额 (CNY)");
		}
		amountOther = emv_inputAmount(szPrompt);
		if (amountOther < 0)
		{
			return -1;
		}
	}else
	{
		amountOther = 0;
	}

	dbg("amount = %d, amountOther = %d\n", amount, amountOther);
	EMVL2_Transaction_SetParaFromUI(iTransType, iAccountType, amount, amountOther);

	qpbocTransaction(NULL);
	NFCClose();

	DisplayAllNFCLedIconNone();
	
	return 0;
}


//一级日志子菜单
SMenu menu_readLogSubMemu =
{"读日志/余额", 0, 1, 0,
	{
		{"交易日志", emv_ReadLogFun},
		{"余额", emv_ReadBalanceFun},
        NULL
	}
};

//一级功能设置子菜单
SMenu menu_settingsSubMemu =
{"功能设置", 0, 1, 0,
	{
		{"选择账户类型", emv_SetAccountTypeFun},
		//{"内核切换", emv_func1_2},
		{"强制联机",     emv_SetForceOnlineFun},
		//{"设置自动测试T", emv_func1_2},
#ifdef ENABLE_PRINTER
		{"打印设置",     emv_SetPrintFun},
#endif
		{"货币代码设置", emv_SetCurrencyCodeFun},
		{"Checksum",     emv_CheckSumFun},
        NULL
	}
};

//一级其他功能设置子菜单
SMenu menu_othrSettingsSubMemu =
{"其他功能", 0, 1, 0,
	{
		{"更新公钥",     emv_downloadCaFun},
		{"更新AID",      emv_downloadAidFun},
		{"更新终端参数", emv_downloadTerminalFun},
		{"批上送",       emv_BatchDataCaptureFun},
		{"服务器设置",   emv_SetupServerIP},
        NULL
	}
};

//一级管理子菜单
SMenu menu_managementSubMemu =
{"管理功能", 0, 1, 0,
	{
		{"下载程序", TMS_RemoteUpdate},
#ifdef JTAG_DEBUG
		{"在线解锁", TMS_Activate},
		{"Test watchdog", wdttest},
#endif
		{"下载密钥", emv_InjectKeyFun},
		{"OP测试",   TMS_OPTest},
		{"上传位置", TMS_UpdateLocation},
		{"WIFI设置", iWifiConfig},
		{"About",    emv_showAppVersion},
		{"UPTS指令测试", emv_UptsCmdTestFun},
#ifndef DISABLE_QRDECODE
		{"扫码测试", test_qrcode},
#endif
        NULL
	}
};




//主菜单
SMenu menu_EmvMainMemu =
{"主菜单", 0, 1, 0,
	{
		{"交易", emv_transactionMenu},
		{"读日志/余额", 0, &menu_readLogSubMemu},
    	{"功能设置", 0, &menu_settingsSubMemu},
    	{"其他功能", 0, &menu_othrSettingsSubMemu},
    	{"清空日志", emv_BatchClearFun},
    	{"自动测试", emv_autoTransactionMenu},
		{"管理", 	 0, &menu_managementSubMemu},
		{"非接", emv_qpbocFlow},
        NULL
	}
};

//TAMPER的主菜单
SMenu menu_TamperingMemu =
{"设备被攻击，请联系售后", 0, 1, 0,
	{
		{"维护", 	 0, &menu_repairSubMemu},
        NULL
	}
};

void lcdDebugSensor(void)
{
	LcdClear();
	if (checkIfSensorEnabled() != 1)
	{
		LcdPutsc("SensorEnabled()!= 1", 1);
	}
	if (checkBPKSensorFlag() != 0)
	{
		LcdPutsc("BPKSenseorFlag()!= 0", 2);
	}
	bPressKey();
}


#if 0
//------------------------------------------------------------------------------------------
void Idle(void)
{
#if 0
	dbg("Enter Idle");
	while(1)
	{
		//PMUModeCheck();
		if (checkIfSensorEnabled() == 0)
		{
			_vCls();
			vDispCenter(3, "请先开启Tamper", 0);
			while(1)
			{
				mdelay(10);
			}
		}
	
		switch (GetTamperStatus())
		{
			case TAMPER_STATUS_TAMPERING:
				dbg("Tampering status\n");
				_vCls();
				vDispCenter(3, "请先修复POS并重启", 0);

				while(1)
				{
					mdelay(10);
				}
				//break;

			case TAMPER_STATUS_CONFIG_ERR:
				dbg("Error Status: Tamper configure error\n");
				_vCls();
				vDispCenter(3, "请重刷LFS系统1", 0);

				while(1)
				{
					mdelay(10);
				}
			case TAMPER_STATUS_OTHER_ERR:
				dbg("Error Status: Tamper configure error\n");
				_vCls();
				vDispCenter(3, "请重刷LFS系统2", 0);

				while(1)
				{
					mdelay(10);
				}

			case TAMPER_STATUS_OK:
				mdelay(10);
				EnterTestMenu(&menu_EmvMainMemu);
				break;

			case TAMPER_STATUS_TAMPED:
				mdelay(10);
				EnterTestMenu(&menu_TamperingMemu);
				break;
		}
	
	}
#else
while(1)
{
		mdelay(10);
		EnterTestMenu(&menu_EmvMainMemu);
}

#endif
/*
	while(1)
	{
		if(checkIfSensorWorksWell() == TRUE)
		{
				mdelay(10);
				EnterTestMenu(&menu_EmvMainMemu);
		
		}else
		{
			if (checkIfSensorTampering(2000) == TRUE)
			{
				_vCls();
				vDispCenter(3, "请先修复POS并重启", 0);

				while(1)
				{
					mdelay(10);
				}
			}else
			{
				//lcdDebugSensor();
				mdelay(10);
		#ifdef 	ENABLE_JTAG
				EnterTestMenu(&menu_EmvMainMemu);
		#else
				EnterTestMenu(&menu_TamperingMemu);
		#endif
			}
		}
	}
*/
	
}
#endif

#endif


