#ifndef _APP_SELECT_H
#define _APP_SELECT_H

#include "Define.h"
#include "OsTypes.h"

int32 GetUserSelectAidIndex(int *pIndex);
void RemoveAid(int index);

int GetUserAIDCount(void);

int GetUserAIDLable(int Index, char *Label);



int c_getUserAIDSelect(void);


void SetSelectAidAutoClose(tti_bool bSelectAidAutoClose, tti_int32 iAutoIndex);

#endif

