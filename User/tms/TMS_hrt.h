#ifndef _TMS_H_
#define _TMS_H_
#include "Tms.h"

#define TMS_STAT_NOTASK         0   //无任务
#define TMS_STAT_PROCESS        1   //任务处理中
#define TMS_STAT_TASKEND        2   //任务结束,无需上送结果
#define TMS_STAT_UPRESULT       3   //任务结束,需立即上送结果
#define TMS_STAT_BOOTRESULT     4   //任务结束,需重启后上送结果(检查版本判断任务为成功或失败)

#define TMS_RESULT_SUCC         0   //任务成功
#define TMS_RESULT_NOPROC       1   //任务不需处理
#define TMS_RESULT_PROCSSING    2   //任务处理中
#define TMS_RESULT_CANCEL       3   //用户取消
#define TMS_RESULT_BREAK        4   //任务中断
#define TMS_RESULT_MD5ERR       5   //MD5校验错
#define TMS_RESULT_DOWNFAIL     6   //下载失败
#define TMS_RESULT_PARAMERR     7   //参数解析错
#define TMS_RESULT_PROCFAIL     8   //下载后处理失败
#define TMS_RESULT_SPACEERR     9   //空间不足
#define TMS_RESULT_OTHERFAIL    10  //其它错


//终端注册
int iTmsTermReg(void);

//终端注销
int iTmsTermUnReg(void);

//心跳上送
int iTmsHeartBeat(char cDispFlag);

//任务查询
int iTmsTaskQuery(void);

//执行tms指令
int iExecTmsTask(void);

//任务执行结果上送
int iTmsTaskResultUploadOne(char *psTaskId);

int iTmsDownloadParam(uchar *psTaskId);

int iTmsDownloadApp(JTMS_TASK *taskInfo);

void vDelSysUpdateFile(char force);

#endif
