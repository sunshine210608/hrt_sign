#ifndef JSD_TMS_H
#define JSD_TMS_H

typedef enum
{
	TMS_QUERY_TASK = 0x00,		//检查更新
	TMS_CHECK_TASKINFO,			//检查任务状态
	TMS_UPDATE_FILE,			//更新文件
	TMS_UPDATE_PARAM,			//更新参数
	TMS_UPLOAD_TASK_STATUS,		//上送结果
	TMS_FINISH,					//任务结束

} KTMS_EVENT_T;

typedef enum
{
	TASK_FILE = 0x01,				//任务类型为更新文件
	TASK_PARAM = 0x02,				//任务类型为更新参数
	TASK_OTHER,
} TASK_TYPE_T;

//-1:下载失败(文件不存在) -2:MD5校验失败 -5:更新失败  -6:用户取消更新 2:下载完成 3:更新成功
#define TASK_DOWNLOAD_FAIL 		-1
#define TASK_CHECK_MD5_FAIL 	-2
#define TASK_UPDATE_FAIL		-5
#define TASK_CANCEL				-6
#define TASK_DOWNLOAD_SUCCESS	2
#define TASK_UPDATE_SUCCESS		3

#define TMS_VER "1.0.0"
#define ROUTER_ADDR "tms-server-web/device"

//TMS系统参数
typedef struct
{
	//uchar szTmsCAName[30 + 1];   		//JTMS ssl 服务器CA证书文件
	//uchar szTMSCertName[30 + 1]; 		//JTMS ssl 终端证书文件名
	//uchar szTmsPrivName[30 + 1]; 		//JTMS ssl 终端私钥文件名
	//uchar bTmsTwoWayAuth;				//JTMS服务器是否开启双向认证
	char szTmsDnsName[50+1];	  			//JTMS服务器DNS域名
	char szTmsIp[15+1];		   		//JTMS服务器15位IP地址
	char szTmsPort[5+1];		   		//JTMS服务器5位端口号
	//uint uiJTMSSSLProtocol;	 			//JTMS系统SSL协议版本
	char bSupportDns;					//JTMS服务器是否开启域名解析
} JTMS_PARAM_T;

typedef struct _JTMS_TASK
{
	char  szTmsTaskId[8+1];
	char  ucTaskType;					//任务类型：1 文件更新、2配置更新
	char  szResSize[8+1];				//需要下载文件大小
	char  szResUrl[100];             	//需要下载文件URL
	ulong  ulTmsDlStart;				//app下载起始位(断点续传)
	//ulong  ulTmsNewLen;				//待下载app长度
	uchar  sNewAppMd5[16];              //待下载的app md5值
	char  szNewVer[20+1];				//待更新的版本,更新前读取文件内版本号得到
	char  taskStatus;
	char  ucTmsUpFlag;					//Tms任务结果上送: 0-无需通知 1-需通知结果(上电更新后检查版本)
    char  sFileHead[12];               //文件头信息,用于计算md5,非应用程序的一部分(拉卡拉有此部分,其它系统无)
    char  ucRestartFlag;                // 0-不需重启 1-需重启
    short iResult;						//-1:下载失败(文件不存在) -2:MD5校验失败 -5:更新失败  -6:用户取消更新 2:下载完成 3:更新成功
    uchar ucInstallType;					// 1 提示升级  0 强制升级
}JTMS_TASK;

#endif

