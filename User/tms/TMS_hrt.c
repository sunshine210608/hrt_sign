#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "VposFace.h"
#include "sys_littlefs.h"
#include "Pub.h"

#include "define.h"

#include "debug.h"
#include "tcpcomm.h"
#include "myHttp.h"

#include "AppGlobal_cjt.h"
#include "MemMana_cjt.h"
#include "TMS_hrt.h"
#include "cJSON.h"
#include "Tms.h"

#include "mbedtls/md5.h"

#define UPDATE_FLAG_NO_UPDATE		"0"
#define UPDATE_FLAG_NEED_UPDATE		"1"
#define UPDATE_FLAG_UPDATE_ALREADY	"2"

#define TMS_STAT_NOTASK         0   //无任务
#define TMS_STAT_PROCESS        1   //任务处理中
#define TMS_STAT_TASKEND        2   //任务结束,无需上送结果
#define TMS_STAT_UPRESULT       3   //任务结束,需立即上送结果
#define TMS_STAT_BOOTRESULT     4   //任务结束,需重启后上送结果(检查版本判断任务为成功或失败)

#define TMS_TASK_QUERY      11
#define TMS_TASK_DLAPP      12
#define TMS_TASK_DLPARAM    13
#define TMS_TASK_UPRESULT   14

#ifdef USE_LTE_4G
#define TMS_DL_PACKSIZE     8000
#else
#define TMS_DL_PACKSIZE     4000
#endif
#define RCV_MAX_LEN         TMS_DL_PACKSIZE+100

extern uint _uiCheckBattery(void);
extern void vSetTcpTAMode(int mode);
extern int gprs_at_setbaud(uint32_t baud);
extern void vDispCancelAndConfirm(char cancel, char confirm);

#define ROUTER_ADDR "tms-server-web/device"


//支持断点续传(写文件会比较慢)
#define BREAKPOINT_TRANS

//下载时先存为临时文件
//#define TMS_APP_USETMPFILE

//extern void vPrtParam(void);
//extern int SaveTMK(byte *TMK);

extern int iGetJsonString(cJSON *cjson, char *pszName, char *pszVal, int size);
extern void vRebootforUpdate(uchar ucForceInsFlag);
int iDownloadParam(uchar *pszParamVer);

#if 0//def GPRS_AT_DEBUG_SERIAL
extern void UsbDbgTxt(char *pszFormat, ...);
#define dbg UsbDbgTxt
#endif

int vGenSyncParamMsg(char *pszOut, int size)
{
	int ret = 1;
	char szBuf[64];
    char *p=0;
	cJSON *root=0,*config=0,*baseInfo=0;

	if(pszOut==NULL || size<=0)
        return 1;
    pszOut[0]=0;

	root = cJSON_CreateObject();
	do{
		if(root==NULL)break;

		config = cJSON_CreateObject();
		if(config==NULL)break;

		baseInfo = cJSON_CreateObject();
		if(baseInfo==NULL)break;		

		cJSON_AddItemToObject(root,"baseInfo",baseInfo);
		cJSON_AddItemToObject(root,"config",config);
        _uiGetSerialNo((uchar*)szBuf);
		cJSON_AddStringToObject(root,"terminalNo",szBuf);
//============================form baseInfo========================================
        _pszGetAppVer(szBuf);
		cJSON_AddStringToObject(baseInfo,"versioncode",szBuf);			
		cJSON_AddStringToObject(baseInfo,"tmsversion" ,TMS_VER);	
		memset(szBuf,0,sizeof(szBuf));
		_vGetTime((uchar*)szBuf);
		cJSON_AddStringToObject(baseInfo,"requesttime",szBuf);	
		
		memset(szBuf,0,sizeof(szBuf));
		iGetIMEI(szBuf);
		cJSON_AddStringToObject(baseInfo,"imei",szBuf);

		memset(szBuf,0,sizeof(szBuf));
		gprs_at_IMSI(szBuf);
		cJSON_AddStringToObject(baseInfo,"imsi",szBuf);		

		memset(szBuf,0,sizeof(szBuf));
		iGetICCID(szBuf);
		cJSON_AddStringToObject(baseInfo,"iccid",szBuf);	

		memset(szBuf,0,sizeof(szBuf));
		gprs_at_GetCellInfo(szBuf);
		cJSON_AddStringToObject(baseInfo,"loaction",szBuf);		
//=================================end=========================================

//============================form config========================================
		cJSON_AddBoolToObject(config,"support4G",true);			
		cJSON_AddBoolToObject(config,"supportBle",false); 
		cJSON_AddBoolToObject(config,"supportWifi",false); 
		cJSON_AddBoolToObject(config,"supportScan",false); 
//=================================end=========================================

	}while(0);

	if(root!=NULL && baseInfo!=NULL)
    {
        ret=2;
		p = cJSON_PrintUnformatted(root);
		if(p!=NULL)
        {
            if(strlen(p)>=size)
            {
                dbg("err:vGenSyncParamMsg->buf Param size err.\n");
            }else
            {
                strcpy(pszOut, p);
                ret=0;
            }
            free(p);
            p=0;
		}
	}
    
	if(root!=NULL){
		cJSON_Delete(root);
		root = NULL;
	}

	return ret;
}

int vFormTaskInfoMsg(char *pszOut, int size, JTMS_TASK *taskinfo)
{
	int ret = 1;
	char szBuf[64];
    char *p=0;
	cJSON *root=0,*config=0,*baseInfo=0;

	if(pszOut==NULL || size<=0)
        return 1;
    pszOut[0]=0;

	root = cJSON_CreateObject();
	do{
		if(root==NULL)break;

		config = cJSON_CreateObject();
		if(config==NULL)break;

		baseInfo = cJSON_CreateObject();
		if(baseInfo==NULL)break;		

		cJSON_AddItemToObject(root,"baseInfo",baseInfo);
		cJSON_AddItemToObject(root,"config",config);
        
        _uiGetSerialNo((uchar*)szBuf);
		cJSON_AddStringToObject(root,"terminalNo",	szBuf);
        
		cJSON_AddStringToObject(root,"taskNo",		taskinfo->szTmsTaskId);
        sprintf(szBuf, "%d", taskinfo->iResult);
		cJSON_AddStringToObject(root,"status", szBuf);
//============================form baseInfo========================================
        _pszGetAppVer(szBuf);
		cJSON_AddStringToObject(baseInfo,"versioncode", szBuf);			
		cJSON_AddStringToObject(baseInfo,"tmsversion", TMS_VER);	

		_vGetTime((uchar*)szBuf);
		cJSON_AddStringToObject(baseInfo,"requesttime",szBuf);		

		iGetIMEI(szBuf);
		cJSON_AddStringToObject(baseInfo,"imei",szBuf);

		gprs_at_IMSI(szBuf);
		cJSON_AddStringToObject(baseInfo,"imsi", szBuf);		

		iGetICCID(szBuf);
		cJSON_AddStringToObject(baseInfo,"iccid", szBuf);	

		gprs_at_GetCellInfo(szBuf);
		cJSON_AddStringToObject(baseInfo,"loaction", szBuf);		
//=================================end=========================================

//============================form config========================================
		cJSON_AddBoolToObject(config,"support4G",true);			
		cJSON_AddBoolToObject(config,"supportBle",false); 
		cJSON_AddBoolToObject(config,"supportWifi",false); 
		cJSON_AddBoolToObject(config,"supportScan",false); 
//=================================end=========================================

	}while(0);
    
    if(root!=NULL && baseInfo!=NULL)
    {
        ret=2;
		p = cJSON_PrintUnformatted(root);
		if(p!=NULL)
        {
            if(strlen(p)>=size)
            {
                dbg("err:vFormTaskInfoMsg->buf Param size err.\n");
            }else
            {
                strcpy(pszOut, p);
                ret=0;
            }
            free(p);
            p=0;
		}
	}
    
	if(root!=NULL){
		cJSON_Delete(root);
		root = NULL;
	}

	return ret;
}

int iTmsUploadTaskResult(char *pszParam, int iParamSize, JTMS_TASK *taskInfo)
{
	char snd[1024], rcv[2048];
    char szUrl[256+1];
    char tmp[100];
	cJSON *recvObj=0,*data=0;
	int  iRet;
	int  status = 0;
	
	vDispCenter(1, "任务执行结果上送", 1);
	vClearLines(2);

    dbg("up result:%d\n", taskInfo->iResult);
    
	vFormTaskInfoMsg(snd, sizeof(snd), taskInfo);

	if(gl_SysInfo.ucTmsDomainOrIp){
		sprintf(szUrl,"http://%s:%d/%s/taskStatus.do", gl_SysInfo.szTmsHost, gl_SysInfo.uiTmsHostPort, ROUTER_ADDR);
	}else{
		sprintf(szUrl,"http://%s:%d/%s/taskStatus.do", gl_SysInfo.szTmsIP1, gl_SysInfo.uiTmsHostPort, ROUTER_ADDR);
	}	
	
    iRet = iHttpSendRecv(2, szUrl, NULL, 1, snd, strlen(snd), rcv, sizeof(rcv), 30);
	iCommTcpDisConn();
	if (iRet)
	{
		return -111;    //上送状态失败
	}
    
	//dbg("recv:\r\n%s\r\n",rcv);
    
    iRet=-2;
    do{
        recvObj = cJSON_Parse(rcv);
        if(recvObj==NULL)
            break;
        
        #ifdef FOR_JSD_TEST
        {
            char *p;
            p = cJSON_PrintUnformatted(recvObj);
            if(p)
            {
                dbg("recvObj:[%s]\n",p);
                free(p);
                p=0;
            }
        }
        #endif
        
        data = cJSON_GetObjectItem(recvObj,"status");
        if(data!=NULL)
            status = data->valueint;
        else
            break;
        iGetJsonString(recvObj, "message", tmp, sizeof(tmp));
        
        dbg("status:%d\r\n",status);
        dbg("message:[%s]\r\n",tmp);
        if(status != 200 || strcmp(tmp,"OK"))
        {
            iRet = -3;
            break;
        }
        
        iRet=0;
        data = cJSON_GetObjectItem(recvObj,"data");
        if(data==NULL)
            break;
        
        memset(taskInfo, 0, sizeof(JTMS_TASK));     //重置任务结构体
        iGetJsonString(data, "taskNo", taskInfo->szTmsTaskId, sizeof(taskInfo->szTmsTaskId));
        
        if(iGetJsonString(data, "type", tmp, 10)==0)
        {
            if(strcmp("1",tmp)==0)
            {
                taskInfo->ucTaskType = TASK_FILE;
                iRet=1;
            }else if(strcmp("2",tmp)==0)
            {
                taskInfo->ucTaskType = TASK_PARAM;
                iRet=2;
            }
        }

        //参数列表(json格式)
        if(taskInfo->ucTaskType == TASK_PARAM && pszParam && iParamSize>0)
        {
            iGetJsonString(data, "config", pszParam, iParamSize);
        }
        
        if(taskInfo->ucTaskType == TASK_FILE)
        {
            iGetJsonString(data, "downloadUrl", taskInfo->szResUrl, sizeof(taskInfo->szResUrl));
            iGetJsonString(data, "size", taskInfo->szResSize, sizeof(taskInfo->szResSize));
            if(iGetJsonString(data, "hashKey", tmp, sizeof(tmp))==0 && strlen(tmp)==32)
                vTwoOne((uchar*)tmp, 32, taskInfo->sNewAppMd5);
            iGetJsonString(data, "version", taskInfo->szNewVer, sizeof(taskInfo->szNewVer));
        }
    }while(0);
	
    if(recvObj!=NULL){
		cJSON_Delete(recvObj);
		recvObj = 0;
	}

	return iRet;
}

//ret: <0查询失败 0无任务 1文件任务 2参数任务
int iJTmsTaskQuery(char *pszParam, int iParamSize, JTMS_TASK *taskInfo)
{
	char snd[1024], rcv[2048];
    char szUrl[256+1];
    char tmp[100];
	cJSON *recvObj=0,*data=0;
	int  iRet;
	int  status = 0;
    
	_vCls();
	vDispCenter(1, "TMS远程更新", 1);
	//vClearLines(2);
    
	vGenSyncParamMsg(snd, sizeof(snd));
	
	if(gl_SysInfo.ucTmsDomainOrIp){
		sprintf(szUrl,"%s:%d/%s/syncParam.do", gl_SysInfo.szTmsHost, gl_SysInfo.uiTmsHostPort, ROUTER_ADDR);
	}else{
		sprintf(szUrl,"%s:%d/%s/syncParam.do", gl_SysInfo.szTmsIP1, gl_SysInfo.uiTmsHostPort,ROUTER_ADDR);
	}
    
	iRet = iHttpSendRecv(2, szUrl, NULL, 1, snd, strlen(snd), rcv, sizeof(rcv), 30);
	if (iRet)
	{
		return -1;
	}
	//dbg("recv:\r\n%s\r\n",rcv);
    
    iRet=-2;
    do{
        recvObj = cJSON_Parse(rcv);
        if(recvObj==NULL)
            break;
        
        #ifdef FOR_JSD_TEST
        {
            char *p;
            p = cJSON_PrintUnformatted(recvObj);
            if(p)
            {
                dbg("recvObj:[%s]\n",p);
                free(p);
                p=0;
            }
        }
        #endif
        
        data = cJSON_GetObjectItem(recvObj,"status");
        if(data!=NULL)
            status = data->valueint;
        else
            break;
        iGetJsonString(recvObj, "message", tmp, sizeof(tmp));
        
        dbg("status:%d\r\n",status);
        dbg("message:[%s]\r\n",tmp);
        if(status != 200 || strcmp(tmp,"OK"))
        {
            iRet = -3;
            break;
        }
        
        iRet=0;
        data = cJSON_GetObjectItem(recvObj,"data");
        if(data==NULL)
            break;
        
        iGetJsonString(data, "taskNo", taskInfo->szTmsTaskId, sizeof(taskInfo->szTmsTaskId));
        rtrim(taskInfo->szTmsTaskId);
        if(taskInfo->szTmsTaskId[00]==0)
            break;
        
        if(iGetJsonString(data, "type", tmp, 10)==0)
        {
            if(strcmp("1",tmp)==0)
            {
                taskInfo->ucTaskType = TASK_FILE;
                iRet=1;
            }else if(strcmp("2",tmp)==0)
            {
                taskInfo->ucTaskType = TASK_PARAM;
                iRet=2;
            }
        }

        //参数列表(json格式)
        if(taskInfo->ucTaskType == TASK_PARAM && pszParam && iParamSize>0)
        {
            iGetJsonString(data, "config", pszParam, iParamSize);
        }
        
        if(taskInfo->ucTaskType == TASK_FILE)
        {
            iGetJsonString(data, "downloadUrl", taskInfo->szResUrl, sizeof(taskInfo->szResUrl));
            iGetJsonString(data, "size", taskInfo->szResSize, sizeof(taskInfo->szResSize));
            if(iGetJsonString(data, "hashKey", tmp, sizeof(tmp))==0 && strlen(tmp)==32)
                vTwoOne((uchar*)tmp, 32, taskInfo->sNewAppMd5);
            iGetJsonString(data, "version", taskInfo->szNewVer, sizeof(taskInfo->szNewVer));
	        if(iGetJsonString(data, "installType", tmp, 10)==0)
	        {
	            if(strcmp("0",tmp)==0)
	            {
	                taskInfo->ucInstallType = 0;		//强制
	            }else if(strcmp("1",tmp)==0)
	            {
	                taskInfo->ucInstallType = 0x01;	//提示
	            }
	        }
        }
    }while(0);
	
    if(recvObj!=NULL){
        cJSON_Delete(recvObj);
        recvObj = 0;
    }
    
    #if 0
    //保存任务信息
    if(iRet>0)
    {
        
        //若已有断点续传且文件大小, md5, url都一致, 则保留原断点续传信息
        if(gptKtms->taskInfo.ulTmsDlStart && taskInfo->ucTaskType == TASK_FILE)
        {
            if( memcmp(gptKtms->taskInfo.sNewAppMd5, taskInfo->sNewAppMd5, 16)==0 
                /* || memcmp(gptKtms->taskInfo.sResUrl, taskInfo->sNewAppMd5, 16)==0 */)
            {
                taskInfo->ulTmsDlStart=gptKtms->taskInfo.ulTmsDlStart;
            }
                
            
            memcpy(&gptKtms->taskInfo, &taskInfo, sizeof(taskInfo));
        }
    
        memcpy(&gptKtms->taskInfo, &taskInfo, sizeof(taskInfo));
        if(taskInfo->ucTaskType == TASK_FILE && gptKtms->cbFuncTable.cbSaveTaskInfo)      //app任务
            gptKtms->cbFuncTable.cbSaveTaskInfo(taskInfo);
        
    }
    #endif
    
	dbg("ucTaskType:%d\r\n",	taskInfo->ucTaskType);
	dbg("taskid:%s\r\n", 		taskInfo->szTmsTaskId);
	dbg("url:%s\r\n", 			taskInfo->szResUrl);
	dbg("size:%s\r\n", 			taskInfo->szResSize);
	dbgHex("md5", 			    taskInfo->sNewAppMd5, sizeof(taskInfo->sNewAppMd5));
	
	return iRet;
}

//查询更新
int iTmsQueryUpdate(void)
{   
	return 0;
}

int iTmsProc(int cmd, int dispflag)
{
    //uchar szParam[2048];      //海科无需下载和解析参数
    JTMS_TASK TaskInfo;
    int iRet;
    int bNeedRestart=0;
    char  szParam[2048+1] = {0};				//需要下载参数
    
    if(cmd==0)
        cmd=TMS_TASK_QUERY;
    
    if(gl_SysInfo.iCommType!=VPOSCOMM_GPRS && gl_SysInfo.iCommType!=VPOSCOMM_WIFI)
    {
        vMessage("请先设置通讯参数");
        return 1;
    }
    
    memset(&TaskInfo, 0, sizeof(TaskInfo));
    do
    {
        switch(cmd)
        {
            case TMS_TASK_QUERY:
            {
                iRet=iJTmsTaskQuery(szParam, sizeof(szParam), &TaskInfo);
                if(iRet<=0)
                {
                    if(iRet==0 && dispflag==1)
                    {
                        vClearLines(2);
                        vMessage("最新版本,无需更新");
                    }
                    cmd=0;
                }else if(iRet==1)
                    cmd=TMS_TASK_DLAPP;
                else if(iRet==2)
                    cmd=TMS_TASK_DLPARAM;
                dispflag=0;
                break;
            }
            case TMS_TASK_DLAPP:
            {
                iRet=iTmsDownloadApp(&TaskInfo);
                if(iRet<=0 && TaskInfo.taskStatus==TMS_STAT_UPRESULT)
                {
                    if(TaskInfo.iResult == TASK_CANCEL)	
			cmd = 0;	
		   else			
                   {
                   	cmd=TMS_TASK_UPRESULT;
                    	if(iRet==0 && TaskInfo.ucRestartFlag==1)
                        	bNeedRestart=1;
		   }			
                }else
                {
                    cmd=0;
                }
                break;
            }
            case TMS_TASK_DLPARAM:
            {

                //不处理参数,直接置为成功
                TaskInfo.iResult=TMS_RESULT_SUCC;
                cmd=TMS_TASK_UPRESULT;
                break;
            }
            case TMS_TASK_UPRESULT:
            {
                iRet=iTmsUploadTaskResult(NULL, 0, &TaskInfo);
                if(iRet<=0)
                    cmd=0;
                else if(iRet==1)
                    cmd=TMS_TASK_DLAPP;
                else if(iRet==2)
                    cmd=TMS_TASK_DLPARAM;
                
                if(bNeedRestart==1) //之前有待重启事件,优先执行重启
                {
                    cmd=0;
                    vDispCenter(1, "TMS远程更新", 1);
                    //vClearLines(2);
                    vRebootforUpdate(1);
                }
                break;
            }
            default:
            {
                cmd=0;
                break;
            }
        }
    }while(cmd);
    return 0;
}

//终端注销
int iTmsTermUnReg(void)
{
	return 1;
}

//心跳上送
int iTmsHeartBeat(char cDispFlag)
{

    return 0;
}

//更新应用参数
int iDownloadParam(uchar *pszParam)
{
	return 0;
}

//保存下载点,便于断电重启后续传
void vSaveDownLen(ulong ulDownloadLen, uchar *psHead, uint uiHeadLen)
{
    gl_SysData.ulTmsDlStart=ulDownloadLen;
#ifdef APP_LKL    
    if(psHead && uiHeadLen && uiHeadLen<=sizeof(gl_SysData.sFileHead))
        memcpy(gl_SysData.sFileHead, psHead, uiHeadLen);
#endif    
    uiMemManaPutSysData();
}


//ret: <0失败,需上送状态 0-成功,需上送状态 >0未成功,不需上送状态
int iTmsDownloadApp(JTMS_TASK *taskInfo)
{
    int iRet;
	char szBuf[100];
	char szAppFile[50];
    //char szUpdateFileName[50];
#ifdef APP_LKL    
    uchar sFileHead[20];
#endif
	uint uiLenTmp;
	uint uiFileLen, uiDownloadLen;
    uint uiLklFileHeadLen=0;
    ulong ulAppTaskId;
    
	dbg("call iTmsDownloadApp()\r\n");
    
    dbg("tms type:[%d]\n", taskInfo->ucTaskType);
    dbg("tms file size:[%s]\n", taskInfo->szResSize);
	dbg("tms file taskid:[%s]\n", taskInfo->szTmsTaskId);
    dbg("tms file NewVer:[%s]\n", taskInfo->szNewVer);
	dbgHex("tms file md5", taskInfo->sNewAppMd5, sizeof(taskInfo->sNewAppMd5));

    if(taskInfo->ucTaskType!=TASK_FILE)
    {
        dbg("tms type err.\n");        
        return 99;
    }
    
	//vDispCenter(1, "TMS下载程序", 1);
    vDispCenter(1, "TMS远程更新", 1);
	vClearLines(2);
    
    //比较新版本号
    rtrim(taskInfo->szNewVer);
    dbg("app currver:[%s], newver[%s]", _pszGetAppVer(NULL), taskInfo->szNewVer);
    _pszGetAppVer(szBuf);
    if(taskInfo->szNewVer[0])
    {
        iRet=strcmp(taskInfo->szNewVer, szBuf);
        if(iRet<=0)
        {
            if(iRet==0)
                vMessage("版本相同,无需更新");
            else
                vMessage("POS版本较新,无需更新");
            
            //当做成功来处理
            taskInfo->iResult=TASK_DOWNLOAD_SUCCESS;
            taskInfo->taskStatus=TMS_STAT_UPRESULT;
            return 0;
        }
    }
    
    //检查是否为断点续传任务
    ulAppTaskId=ulA2L((uchar*)taskInfo->szTmsTaskId, strlen(taskInfo->szTmsTaskId));
    uiFileLen=ulA2L((uchar*)taskInfo->szResSize, strlen(taskInfo->szResSize));
        
#ifdef BREAKPOINT_TRANS
    if( gl_SysData.ulTmsTaskId!=ulAppTaskId || 
            uiFileLen!=gl_SysData.ulTmsNewLen || 
            memcmp(gl_SysData.sNewAppMd5, taskInfo->sNewAppMd5, 16) ||
            strcmp((char*)gl_SysData.szTmsNewVer, taskInfo->szNewVer) )
#endif
    {
        //下载任务有所不同			
        gl_SysData.ulTmsTaskId=ulAppTaskId;
        gl_SysData.ulTmsNewLen=uiFileLen;			
        strcpy((char*)gl_SysData.szTmsNewVer, taskInfo->szNewVer);
        memcpy(gl_SysData.sNewAppMd5, taskInfo->sNewAppMd5, 16);
        //strncpy((char*)gl_SysData.szResUrl, taskInfo->szResUrl, sizeof(gl_SysData.szResUrl)-1);
        
        gl_SysData.ulTmsDlStart=0;
        uiMemManaPutSysData();
    }
    
    if(gl_SysData.ulTmsDlStart==0)
    {
       if( taskInfo->ucInstallType == 0x01)
	{
		//非强制安装,提示
	        _vDisp(2, "是否下载新版本?");
	        vDispVarArg(3, "新版本号:%s", gl_SysData.szTmsNewVer);
	        vDispVarArg(4, "程序大小:%lu", uiFileLen /*sg_TMSTasks[n].sResSize*/);
	        vDispCancelAndConfirm(1, 1);
	        if(iOK(15)==0)
	        {      
	            //用户取消
	            taskInfo->iResult=TASK_CANCEL;
	            taskInfo->taskStatus=TMS_STAT_UPRESULT;
	            return -1;
	        }
	        vClearLines(2);
       	}
	else if( taskInfo->ucInstallType == 0)
  	{	  //强制安装,提示
		_vDisp(2, "下载新版本");
		vDispVarArg(3, "新版本号:%s", gl_SysData.szTmsNewVer);
		vDispVarArg(4, "程序大小:%lu", uiFileLen /*sg_TMSTasks[n].sResSize*/);
		iGetKeyWithTimeout(2);
		vClearLines(2);
  	}
    }
    
    if(gl_SysData.uiTransNum)
    {   
        vDispCenter(2, "有未结算的交易",0 );
        vDispCenter(3, "需先结算现有交易", 0);
        iGetKeyWithTimeout(2);
       iOfflineSettle();
        if(gl_SysData.uiTransNum)
        {
            //未结算不上送状态,等待下次处理tms任务
            return 1;
        }
        //vDispCenter(1, "TMS下载程序", 1);
        vDispCenter(1, "TMS远程更新", 1);
        vClearLines(2);
    }else
        gl_SysData.ucPosLoginFlag=0;

#if 0   //使用临时文件    
    vDelSysUpdateFile(1);
    sprintf(szAppFile, "%s/%s", DIR_UPDATE, UPDATE_APP_TMPFILE_NAME);   //临时文件
#else    
    sprintf(szAppFile, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_NEW_APP);  //直接使用更新文件
#endif    
    dbg("szAppFile:%s\r\n", szAppFile);
    
	//判断空间是否足够
    {
        ulong ulFreeSpace;
        lfs_file_t file;
        
        //检查文件大小
        if(gl_SysData.ulTmsDlStart)
        {
            if(sys_lfs_file_open(&file, szAppFile,  LFS_O_RDONLY)==0)
            {
                uiLenTmp=sys_lfs_file_size(&file);
                sys_lfs_file_close(&file);
                #if 0
                vDispVarArg(2, "start:%lu", gl_SysData.ulTmsDlStart);
                vDispVarArg(3, "filelen:%lu", uiLenTmp);
                _uiGetKey();
                #endif
                
                if(uiLenTmp<gl_SysData.ulTmsDlStart || 
                    //uiLenTmp>gl_SysData.ulTmsDlStart+8000 || //有可能下载到最后一包数据,低于8000的情况存在
                    uiLenTmp>uiFileLen)
                {           
                    //sys_lfs_remove(szAppFile);
                    gl_SysData.ulTmsDlStart=0;
                }
            }else
            {
                gl_SysData.ulTmsDlStart=0;
            }
        }
        
        if(gl_SysData.ulTmsDlStart==0)
            sys_lfs_remove(szAppFile);
        
		_uiGetPosSpace(NULL, NULL, &ulFreeSpace);
        dbg("uiFileLen=[%d], ulTmsDlStart=[%d], left=%d, ulFreeSpace=%d\n",
                    uiFileLen, gl_SysData.ulTmsDlStart, (uiFileLen-gl_SysData.ulTmsDlStart)/1024 + 10, ulFreeSpace);
		if((uiFileLen-gl_SysData.ulTmsDlStart)/1024 + 10 > ulFreeSpace)
		{
            vClearLines(2);
            vMessage("Pos空间不足,无法更新");
            
            //状态上送下载失败
            taskInfo->iResult=TASK_DOWNLOAD_FAIL;            
            taskInfo->taskStatus=TMS_STAT_UPRESULT;
            return -2;
		}
    }
    
	//判断电量是否足够
	if(_uiCheckBattery()<3)
	{
        vClearLines(2);
        vDispCenter(3, "更新程序,电量低", 0);
        vDispCenter(4, "请先充电", 0);
        while(_uiCheckBattery()<3)
        {
            _vDelay(10);
            if(_uiKeyPressed() && _uiGetKey()==_KEY_ESC)
            {
                //vClearLines(2);
                vDispCenter(5, "退出程序更新?", 0);
                if(iOK(15)==1)
                {
                    //电量不足不上送状态,等待下次处理tms任务
                    return 3;
                }
                _vDisp(5, "");
            }
        }
	}
    
    if(gl_SysData.ulTmsDlStart)
	{        
		//sprintf(szBuf, "已下%u/%lu", gl_SysData.ulTmsDlStart+uiLklFileHeadLen, uiFileLen);
        sprintf(szBuf, "已下载:%3lu%%", (gl_SysData.ulTmsDlStart+uiLklFileHeadLen)*100/uiFileLen);
		iRet=iSelectOne(szBuf, "  1.断点续传", "  2.重新下载", "", 1, 30, 0);
		if(iRet==2)
		{
			gl_SysData.ulTmsDlStart=0;
		}
	}

    if(gl_SysData.ulTmsDlStart!=uiFileLen)
    {
        uiDownloadLen = gl_SysData.ulTmsDlStart;
        uiLenTmp=uiDownloadLen;
        if(gl_SysInfo.iCommType == VPOSCOMM_GPRS) 
    	{
	    #ifdef USE_HIGH_BAUDRATE
	        if(gprs_at_setbaud(HIGH_BAUDRATE)==0)
	        {
	            iRet = iHttpDownloadFileTA((uchar*)taskInfo->szResUrl, (uchar*)szAppFile, &uiFileLen, &uiDownloadLen, 1, NULL, 0, vSaveDownLen);
	            vSetTcpTAMode(0);  //关透传模式
	            gprs_at_setbaud(LOW_BAUDRATE);
	        }else
	    #else
	        {
	            iRet = iHttpDownloadFile((uchar*)taskInfo->szResUrl, szAppFile, &uiFileLen, &uiDownloadLen, 1, NULL, 0, vSaveDownLen);
	        }
	    #endif
	        dbg("DownloadFile ret:%d, downloadlen:%d, filelen:%d\n", iRet, uiDownloadLen, uiFileLen);
    	}else if(gl_SysInfo.iCommType == VPOSCOMM_WIFI)
    	{
    		iRet = iHttpDownloadFile((uchar*)taskInfo->szResUrl, szAppFile, &uiFileLen, &uiDownloadLen, 1, NULL, 0, vSaveDownLen);
		dbg("DownloadFile ret:%d, downloadlen:%d, filelen:%d\n", iRet, uiDownloadLen, uiFileLen);	
    	}

        vClearLines(2);
        if (iRet < 0 || uiDownloadLen==uiLenTmp)
        {
            vMessage("下载文件失败");
            //vSetTaskStatus(TASK_DOWNLOAD_FAIL);
            
            //还未实际下载到数据,不上送状态        
            return 4;
        }

        if(iRet && uiDownloadLen!=uiFileLen)
        {        
            //回调函数已保存，此处不做重复保存
            //gl_SysData.ulTmsDlStart=uiDownloadLen;
            //uiMemManaPutSysData();
            
            //下载中断,不需上送状态
            _vDisp(2, "下载中断");
            if(uiDownloadLen)
                uiDownloadLen+=uiLklFileHeadLen;
            sprintf(szBuf, "已下%u/%lu", uiDownloadLen, uiFileLen);
            vMessage(szBuf);
            return 5;
        }
    }
    
	vDispMid(2, "校验文件md5...");
	//检查md5值
	memset(szBuf, 0, sizeof(szBuf));
	iMd5File(szAppFile, NULL, 0, szBuf);
	if(memcmp(szBuf, taskInfo->sNewAppMd5, 16)!=0)
	{
        gl_SysData.ulTmsDlStart=0;
        uiMemManaPutSysData();
        sys_lfs_remove(szAppFile);
        
        taskInfo->iResult=TASK_CHECK_MD5_FAIL;
        taskInfo->taskStatus=TMS_STAT_UPRESULT;
        
        dbg("md5 check fail!!!\n");
        dbgHex("cal  md5", (uchar*)szBuf, 16);
        dbgHex("file md5", taskInfo->sNewAppMd5, 16);
        
        vClearLines(2);
        vMessage("下载文件Md5校验失败");
		return -9;
	}

	//vSetTaskStatus(TASK_DOWNLOAD_SUCCESS);    
    taskInfo->iResult=TASK_DOWNLOAD_SUCCESS;
    taskInfo->taskStatus=TMS_STAT_UPRESULT;
    taskInfo->ucRestartFlag=1;
    
    //非重启后上送状态,可以不保存
#if 0
    //gl_SysData.ulTmsDlStart=0;
    gl_SysData.ucTmsDownloadFlag=2;
	gl_SysData.iTaskResult=TMS_RESULT_SUCC;
	uiMemManaPutSysData();
#endif
    
    //dbgLTxt("gl_SysData.sNewVer=[%.8s]\n", gl_SysData.sNewVer);
    #if 0
	//更新
    sprintf(szUpdateFileName, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_NEW_APP);
    sys_lfs_rename(szAppFile, szUpdateFileName);
	vRebootforUpdate(1);
    #endif

	return 0;
}

extern void systemReboot(void);
extern int makeUpdateFlag(void);
void vUpDateAppfile(void)
{
	int ret;
	uchar sRcvTmp[1500];
	uchar szBuf[100];

	lfs_file_t newAppFile;
	lfs_file_t updateFlagFile;
	char szUpdateAppNewPath[128] = {0};
	char szUpdateFlagPath[128] = {0};

	_vDisp(1, "检查文件md5");

	sprintf(szUpdateAppNewPath, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_NEW_APP);
	#if 0
    {
		mbedtls_md5_context ctx;

		int ret, len = 0;
		unsigned char md[16];
		int n=0;
		
		ret=sys_lfs_file_open(&newAppFile, szUpdateAppNewPath, LFS_O_RDONLY);
		if(ret)
		{
			_vDisp(2, szUpdateAppNewPath);
			vMessageEx("打开文件失败", 10*100);
			return;
		}
		
		mbedtls_md5_init(&ctx);
		mbedtls_md5_starts_ret( &ctx );
		while ((len = sys_lfs_file_read(&newAppFile, sRcvTmp, 1024)) > 0)
		{
			mbedtls_md5_update_ret (&ctx, sRcvTmp, len);
			n+=len;
		}
		mbedtls_md5_finish_ret(&ctx, md);
		mbedtls_md5_free( &ctx );

		sys_lfs_file_close(&newAppFile);

		vOneTwo0(md, 8, sRcvTmp);
		vOneTwo0(md+8, 8, sRcvTmp+50);
		vDispVarArg(5, "[%s]", szUpdateAppNewPath);
		_vDisp(2, sRcvTmp);
		_vDisp(3, sRcvTmp+50);
		vDispVarArg(4, "file len:%d", n);
		_uiGetKey();
	}
    #endif
    
	//_vCls();
	sprintf(szUpdateFlagPath, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_APP_FLAG);
	vDispVarArg(5, "[%s]", szUpdateFlagPath);
	
	//先读原值
	ret=sys_lfs_file_open(&updateFlagFile, szUpdateFlagPath, LFS_O_RDONLY);
	if(ret==0)
	{
		ret=sys_lfs_file_read(&updateFlagFile, szBuf, strlen(UPDATE_FLAG_NEED_UPDATE));
		sys_lfs_file_close(&updateFlagFile);
		vDispVarArg(6, "r UPDATE_FLAG:%02X\n", szBuf[0]);
	}
	
	ret=sys_lfs_file_open(&updateFlagFile, szUpdateFlagPath, LFS_O_CREAT | LFS_O_TRUNC | LFS_O_WRONLY);
	if(ret)
	{
		vMessage("open updatefile err");
		return;
	}
	ret=sys_lfs_file_write(&updateFlagFile, UPDATE_FLAG_NEED_UPDATE, strlen(UPDATE_FLAG_NEED_UPDATE));
	sys_lfs_file_close(&updateFlagFile);

	strcpy(szBuf, UPDATE_FLAG_NEED_UPDATE);
	vDispVarArg(7, "w UPDATE_FLAG:%02X\n", szBuf[0]);

	szBuf[0]=0xFF;
	sys_lfs_file_open(&updateFlagFile, szUpdateFlagPath, LFS_O_RDONLY);
	ret=sys_lfs_file_read(&updateFlagFile, szBuf, strlen(UPDATE_FLAG_NEED_UPDATE));
	sys_lfs_file_close(&updateFlagFile);
	dbg("read UPDATE_FLAG:%d\n", szBuf[0]);
	vDispVarArg(8, "r UPDATE_FLAG:%02X\n", szBuf[0]);
	if(_uiGetKey()==_KEY_ESC)
		return;
	vMessage("更新程序...");
	if(makeUpdateFlag()==0)
	{
		vClearLines(3);
		_vDisp(3, "正在重启POS...");
		systemReboot();
	}
    return;
}


int iTmsUpdateEnd(ulong ulParamTaskId, char *pszParamVer, ulong ulAppTaskId, char *pszAppVer)
{

	return 0;
}

int mTmsClearDownload(void *m)
{
    char szUpdateAppNewPath[128];
    _vCls();
    
    _vDisp(2, "清应用更新已下载部分?");
    
    if(iOK(15)==1)
    {
        gl_SysData.ucTmsDownloadFlag=0;
        gl_SysData.ulTmsTaskId=0;
        gl_SysData.ulTmsDlStart=0;
        gl_SysData.iTaskResult=0;
        uiMemManaPutSysData();
        
        sprintf(szUpdateAppNewPath, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_NEW_APP);
        sys_lfs_remove(szUpdateAppNewPath);		//删除文件
        vMessage("已清除");
    }
    
	return 0;
}

void vDelSysUpdateFile(char force)
{
    char szAppName[50], szFlagName[50];
    char buf[10];
    lfs_file_t file;
    
    sprintf(szAppName,  "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_NEW_APP);
    sprintf(szFlagName, "%s/%s", DIR_UPDATE, FILE_NAME_UPDATE_APP_FLAG);
    
    if(force==0)
    {
        buf[0]=0;
        if(sys_lfs_file_open(&file, szFlagName, LFS_O_RDONLY)==0)
        {
            sys_lfs_file_read(&file, buf, 1);
            sys_lfs_file_close(&file);
        }
        if(buf[0]=='2' || buf[0]=='3')
            force=1;
    }
    
    if(force)
    {
        dbg("del new app file.\n");
        sys_lfs_remove(szAppName);        
        sys_lfs_remove(szFlagName);
    }
}

